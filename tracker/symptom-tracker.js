
if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('service-worker.js');
  });
}


document.addEventListener("DOMContentLoaded", () => {
  // Get the form and the list of symptoms
  const form = document.querySelector("form");
  const symptomList = document.querySelector("ul");

  // Handle form submissions
  form.addEventListener("submit", event => {
    // Prevent the form from refreshing the page
    event.preventDefault();

    // Get all checked symptoms and their selected severity
    const selectedSymptoms = Array.from(form.elements.symptom).filter(
      symptom => symptom.checked
    );
    const severities = Array.from(form.elements.severity).map(
      severity => severity.value
    );

    // Add a list item for each selected symptom
    symptomList.innerHTML = "";
    selectedSymptoms.forEach((symptom, index) => {
      const severity = severities[index];
      const listItem = document.createElement("li");
      listItem.textContent = `${symptom.value} (${severity})`;
      symptomList.appendChild(listItem);
    });
  });
});

