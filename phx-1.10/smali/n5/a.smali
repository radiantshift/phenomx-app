.class public abstract Ln5/a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ln5/g$b;


# instance fields
.field private final f:Ln5/g$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln5/g$c<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ln5/g$c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/g$c<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ln5/a;->f:Ln5/g$c;

    return-void
.end method


# virtual methods
.method public b(Ln5/g$c;)Ln5/g$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ln5/g$b;",
            ">(",
            "Ln5/g$c<",
            "TE;>;)TE;"
        }
    .end annotation

    invoke-static {p0, p1}, Ln5/g$b$a;->b(Ln5/g$b;Ln5/g$c;)Ln5/g$b;

    move-result-object p1

    return-object p1
.end method

.method public c(Ln5/g;)Ln5/g;
    .locals 0

    invoke-static {p0, p1}, Ln5/g$b$a;->d(Ln5/g$b;Ln5/g;)Ln5/g;

    move-result-object p1

    return-object p1
.end method

.method public getKey()Ln5/g$c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ln5/g$c<",
            "*>;"
        }
    .end annotation

    iget-object v0, p0, Ln5/a;->f:Ln5/g$c;

    return-object v0
.end method

.method public i(Ln5/g$c;)Ln5/g;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/g$c<",
            "*>;)",
            "Ln5/g;"
        }
    .end annotation

    invoke-static {p0, p1}, Ln5/g$b$a;->c(Ln5/g$b;Ln5/g$c;)Ln5/g;

    move-result-object p1

    return-object p1
.end method

.method public o(Ljava/lang/Object;Lu5/p;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Lu5/p<",
            "-TR;-",
            "Ln5/g$b;",
            "+TR;>;)TR;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Ln5/g$b$a;->a(Ln5/g$b;Ljava/lang/Object;Lu5/p;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
