.class public abstract Ln5/b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ln5/g$c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<B::",
        "Ln5/g$b;",
        "E::TB;>",
        "Ljava/lang/Object;",
        "Ln5/g$c<",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final f:Lu5/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lu5/l<",
            "Ln5/g$b;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final g:Ln5/g$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln5/g$c<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ln5/g$c;Lu5/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/g$c<",
            "TB;>;",
            "Lu5/l<",
            "-",
            "Ln5/g$b;",
            "+TE;>;)V"
        }
    .end annotation

    const-string v0, "baseKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "safeCast"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Ln5/b;->f:Lu5/l;

    instance-of p2, p1, Ln5/b;

    if-eqz p2, :cond_0

    check-cast p1, Ln5/b;

    iget-object p1, p1, Ln5/b;->g:Ln5/g$c;

    :cond_0
    iput-object p1, p0, Ln5/b;->g:Ln5/g$c;

    return-void
.end method


# virtual methods
.method public final a(Ln5/g$c;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/g$c<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    if-eq p1, p0, :cond_1

    iget-object v0, p0, Ln5/b;->g:Ln5/g$c;

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final b(Ln5/g$b;)Ln5/g$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/g$b;",
            ")TE;"
        }
    .end annotation

    const-string v0, "element"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Ln5/b;->f:Lu5/l;

    invoke-interface {v0, p1}, Lu5/l;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ln5/g$b;

    return-object p1
.end method
