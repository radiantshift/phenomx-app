.class public final Lz4/a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lz4/a$b;,
        Lz4/a$c;,
        Lz4/a$e;,
        Lz4/a$d;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lz4/c;

.field private final b:Ljava/lang/String;

.field private final c:Lz4/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lz4/i<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final d:Lz4/c$c;


# direct methods
.method public constructor <init>(Lz4/c;Ljava/lang/String;Lz4/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lz4/c;",
            "Ljava/lang/String;",
            "Lz4/i<",
            "TT;>;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;Lz4/c$c;)V

    return-void
.end method

.method public constructor <init>(Lz4/c;Ljava/lang/String;Lz4/i;Lz4/c$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lz4/c;",
            "Ljava/lang/String;",
            "Lz4/i<",
            "TT;>;",
            "Lz4/c$c;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lz4/a;->a:Lz4/c;

    iput-object p2, p0, Lz4/a;->b:Ljava/lang/String;

    iput-object p3, p0, Lz4/a;->c:Lz4/i;

    iput-object p4, p0, Lz4/a;->d:Lz4/c$c;

    return-void
.end method

.method static synthetic a(Lz4/a;)Lz4/i;
    .locals 0

    iget-object p0, p0, Lz4/a;->c:Lz4/i;

    return-object p0
.end method

.method static synthetic b(Lz4/a;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lz4/a;->b:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public c(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lz4/a;->d(Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public d(Ljava/lang/Object;Lz4/a$e;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lz4/a$e<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lz4/a;->a:Lz4/c;

    iget-object v1, p0, Lz4/a;->b:Ljava/lang/String;

    iget-object v2, p0, Lz4/a;->c:Lz4/i;

    invoke-interface {v2, p1}, Lz4/i;->a(Ljava/lang/Object;)Ljava/nio/ByteBuffer;

    move-result-object p1

    const/4 v2, 0x0

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v3, Lz4/a$c;

    invoke-direct {v3, p0, p2, v2}, Lz4/a$c;-><init>(Lz4/a;Lz4/a$e;Lz4/a$a;)V

    move-object v2, v3

    :goto_0
    invoke-interface {v0, v1, p1, v2}, Lz4/c;->e(Ljava/lang/String;Ljava/nio/ByteBuffer;Lz4/c$b;)V

    return-void
.end method

.method public e(Lz4/a$d;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lz4/a$d<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lz4/a;->d:Lz4/c$c;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lz4/a;->a:Lz4/c;

    iget-object v2, p0, Lz4/a;->b:Ljava/lang/String;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v3, Lz4/a$b;

    invoke-direct {v3, p0, p1, v1}, Lz4/a$b;-><init>(Lz4/a;Lz4/a$d;Lz4/a$a;)V

    move-object v1, v3

    :goto_0
    iget-object p1, p0, Lz4/a;->d:Lz4/c$c;

    invoke-interface {v0, v2, v1, p1}, Lz4/c;->b(Ljava/lang/String;Lz4/c$a;Lz4/c$c;)V

    goto :goto_2

    :cond_1
    iget-object v0, p0, Lz4/a;->a:Lz4/c;

    iget-object v2, p0, Lz4/a;->b:Ljava/lang/String;

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    new-instance v3, Lz4/a$b;

    invoke-direct {v3, p0, p1, v1}, Lz4/a$b;-><init>(Lz4/a;Lz4/a$d;Lz4/a$a;)V

    move-object v1, v3

    :goto_1
    invoke-interface {v0, v2, v1}, Lz4/c;->d(Ljava/lang/String;Lz4/c$a;)V

    :goto_2
    return-void
.end method
