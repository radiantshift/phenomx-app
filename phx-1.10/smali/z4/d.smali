.class public final Lz4/d;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lz4/d$c;,
        Lz4/d$b;,
        Lz4/d$d;
    }
.end annotation


# instance fields
.field private final a:Lz4/c;

.field private final b:Ljava/lang/String;

.field private final c:Lz4/l;

.field private final d:Lz4/c$c;


# direct methods
.method public constructor <init>(Lz4/c;Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lz4/s;->b:Lz4/s;

    invoke-direct {p0, p1, p2, v0}, Lz4/d;-><init>(Lz4/c;Ljava/lang/String;Lz4/l;)V

    return-void
.end method

.method public constructor <init>(Lz4/c;Ljava/lang/String;Lz4/l;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lz4/d;-><init>(Lz4/c;Ljava/lang/String;Lz4/l;Lz4/c$c;)V

    return-void
.end method

.method public constructor <init>(Lz4/c;Ljava/lang/String;Lz4/l;Lz4/c$c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lz4/d;->a:Lz4/c;

    iput-object p2, p0, Lz4/d;->b:Ljava/lang/String;

    iput-object p3, p0, Lz4/d;->c:Lz4/l;

    iput-object p4, p0, Lz4/d;->d:Lz4/c$c;

    return-void
.end method

.method static synthetic a(Lz4/d;)Lz4/l;
    .locals 0

    iget-object p0, p0, Lz4/d;->c:Lz4/l;

    return-object p0
.end method

.method static synthetic b(Lz4/d;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lz4/d;->b:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lz4/d;)Lz4/c;
    .locals 0

    iget-object p0, p0, Lz4/d;->a:Lz4/c;

    return-object p0
.end method


# virtual methods
.method public d(Lz4/d$d;)V
    .locals 3

    iget-object v0, p0, Lz4/d;->d:Lz4/c$c;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lz4/d;->a:Lz4/c;

    iget-object v2, p0, Lz4/d;->b:Ljava/lang/String;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lz4/d$c;

    invoke-direct {v1, p0, p1}, Lz4/d$c;-><init>(Lz4/d;Lz4/d$d;)V

    :goto_0
    iget-object p1, p0, Lz4/d;->d:Lz4/c$c;

    invoke-interface {v0, v2, v1, p1}, Lz4/c;->b(Ljava/lang/String;Lz4/c$a;Lz4/c$c;)V

    goto :goto_2

    :cond_1
    iget-object v0, p0, Lz4/d;->a:Lz4/c;

    iget-object v2, p0, Lz4/d;->b:Ljava/lang/String;

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    new-instance v1, Lz4/d$c;

    invoke-direct {v1, p0, p1}, Lz4/d$c;-><init>(Lz4/d;Lz4/d$d;)V

    :goto_1
    invoke-interface {v0, v2, v1}, Lz4/c;->d(Ljava/lang/String;Lz4/c$a;)V

    :goto_2
    return-void
.end method
