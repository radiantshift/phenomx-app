.class public final Lf6/c;
.super Lkotlin/coroutines/jvm/internal/d;
.source ""

# interfaces
.implements Le6/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lkotlin/coroutines/jvm/internal/d;",
        "Le6/c<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final f:Le6/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Le6/c<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final g:Ln5/g;

.field public final h:I

.field private i:Ln5/g;

.field private j:Ln5/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln5/d<",
            "-",
            "Lk5/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Le6/c;Ln5/g;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le6/c<",
            "-TT;>;",
            "Ln5/g;",
            ")V"
        }
    .end annotation

    sget-object v0, Lf6/b;->f:Lf6/b;

    sget-object v1, Ln5/h;->f:Ln5/h;

    invoke-direct {p0, v0, v1}, Lkotlin/coroutines/jvm/internal/d;-><init>(Ln5/d;Ln5/g;)V

    iput-object p1, p0, Lf6/c;->f:Le6/c;

    iput-object p2, p0, Lf6/c;->g:Ln5/g;

    const/4 p1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    sget-object v0, Lf6/c$a;->f:Lf6/c$a;

    invoke-interface {p2, p1, v0}, Ln5/g;->o(Ljava/lang/Object;Lu5/p;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    iput p1, p0, Lf6/c;->h:I

    return-void
.end method

.method private final a(Ln5/g;Ln5/g;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/g;",
            "Ln5/g;",
            "TT;)V"
        }
    .end annotation

    instance-of v0, p2, Lf6/a;

    if-eqz v0, :cond_0

    check-cast p2, Lf6/a;

    invoke-direct {p0, p2, p3}, Lf6/c;->c(Lf6/a;Ljava/lang/Object;)V

    :cond_0
    invoke-static {p0, p1}, Lf6/e;->a(Lf6/c;Ln5/g;)V

    return-void
.end method

.method private final b(Ln5/d;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/d<",
            "-",
            "Lk5/s;",
            ">;TT;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-interface {p1}, Ln5/d;->getContext()Ln5/g;

    move-result-object v0

    invoke-static {v0}, Lb6/u1;->f(Ln5/g;)V

    iget-object v1, p0, Lf6/c;->i:Ln5/g;

    if-eq v1, v0, :cond_0

    invoke-direct {p0, v0, v1, p2}, Lf6/c;->a(Ln5/g;Ln5/g;Ljava/lang/Object;)V

    iput-object v0, p0, Lf6/c;->i:Ln5/g;

    :cond_0
    iput-object p1, p0, Lf6/c;->j:Ln5/d;

    invoke-static {}, Lf6/d;->a()Lu5/q;

    move-result-object p1

    iget-object v0, p0, Lf6/c;->f:Le6/c;

    invoke-interface {p1, v0, p2, p0}, Lu5/q;->d(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {}, Lo5/b;->c()Ljava/lang/Object;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/jvm/internal/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_1

    const/4 p2, 0x0

    iput-object p2, p0, Lf6/c;->j:Ln5/d;

    :cond_1
    return-object p1
.end method

.method private final c(Lf6/a;Ljava/lang/Object;)V
    .locals 3

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\n            Flow exception transparency is violated:\n                Previous \'emit\' call has thrown exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lf6/a;->f:Ljava/lang/Throwable;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", but then emission attempt of value \'"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "\' has been detected.\n                Emissions from \'catch\' blocks are prohibited in order to avoid unspecified behaviour, \'Flow.catch\' operator can be used instead.\n                For a more detailed explanation, please refer to Flow documentation.\n            "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, La6/d;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public emit(Ljava/lang/Object;Ln5/d;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ln5/d<",
            "-",
            "Lk5/s;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p2, p1}, Lf6/c;->b(Ln5/d;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lo5/b;->c()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    invoke-static {p2}, Lkotlin/coroutines/jvm/internal/h;->c(Ln5/d;)V

    :cond_0
    invoke-static {}, Lo5/b;->c()Ljava/lang/Object;

    move-result-object p2

    if-ne p1, p2, :cond_1

    return-object p1

    :cond_1
    sget-object p1, Lk5/s;->a:Lk5/s;

    return-object p1

    :catchall_0
    move-exception p1

    new-instance v0, Lf6/a;

    invoke-interface {p2}, Ln5/d;->getContext()Ln5/g;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lf6/a;-><init>(Ljava/lang/Throwable;Ln5/g;)V

    iput-object v0, p0, Lf6/c;->i:Ln5/g;

    throw p1
.end method

.method public getCallerFrame()Lkotlin/coroutines/jvm/internal/e;
    .locals 2

    iget-object v0, p0, Lf6/c;->j:Ln5/d;

    instance-of v1, v0, Lkotlin/coroutines/jvm/internal/e;

    if-eqz v1, :cond_0

    check-cast v0, Lkotlin/coroutines/jvm/internal/e;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getContext()Ln5/g;
    .locals 1

    iget-object v0, p0, Lf6/c;->i:Ln5/g;

    if-nez v0, :cond_0

    sget-object v0, Ln5/h;->f:Ln5/h;

    :cond_0
    return-object v0
.end method

.method public getStackTraceElement()Ljava/lang/StackTraceElement;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    invoke-static {p1}, Lk5/m;->b(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lf6/a;

    invoke-virtual {p0}, Lf6/c;->getContext()Ln5/g;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lf6/a;-><init>(Ljava/lang/Throwable;Ln5/g;)V

    iput-object v1, p0, Lf6/c;->i:Ln5/g;

    :cond_0
    iget-object v0, p0, Lf6/c;->j:Ln5/d;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Ln5/d;->resumeWith(Ljava/lang/Object;)V

    :cond_1
    invoke-static {}, Lo5/b;->c()Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public releaseIntercepted()V
    .locals 0

    invoke-super {p0}, Lkotlin/coroutines/jvm/internal/d;->releaseIntercepted()V

    return-void
.end method
