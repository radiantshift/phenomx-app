.class final synthetic Lf6/d$a;
.super Lkotlin/jvm/internal/h;
.source ""

# interfaces
.implements Lu5/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf6/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1000
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/h;",
        "Lu5/q<",
        "Le6/c<",
        "-",
        "Ljava/lang/Object;",
        ">;",
        "Ljava/lang/Object;",
        "Ln5/d<",
        "-",
        "Lk5/s;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final f:Lf6/d$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf6/d$a;

    invoke-direct {v0}, Lf6/d$a;-><init>()V

    sput-object v0, Lf6/d$a;->f:Lf6/d$a;

    return-void
.end method

.method constructor <init>()V
    .locals 6

    const-class v2, Le6/c;

    const/4 v1, 0x3

    const-string v3, "emit"

    const-string v4, "emit(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lkotlin/jvm/internal/h;-><init>(ILjava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final b(Le6/c;Ljava/lang/Object;Ln5/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le6/c<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            "Ln5/d<",
            "-",
            "Lk5/s;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-interface {p1, p2, p3}, Le6/c;->emit(Ljava/lang/Object;Ln5/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic d(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Le6/c;

    check-cast p3, Ln5/d;

    invoke-virtual {p0, p1, p2, p3}, Lf6/d$a;->b(Le6/c;Ljava/lang/Object;Ln5/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
