.class public Lp/l;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:Lp/a$b;

.field public static final B:Lp/a$b;

.field public static final C:Lp/a$d;

.field public static final D:Lp/a$b;

.field public static final E:Lp/a$b;

.field public static final F:Lp/a$b;

.field public static final G:Lp/a$b;

.field public static final H:Lp/a$e;

.field public static final I:Lp/a$e;

.field public static final J:Lp/a$h;

.field public static final K:Lp/a$h;

.field public static final L:Lp/a$g;

.field public static final M:Lp/i$a;

.field public static final N:Lp/a$h;

.field public static final O:Lp/a$i;

.field public static final P:Lp/a$d;

.field public static final Q:Lp/a$d;

.field public static final R:Lp/a$d;

.field public static final S:Lp/a$h;

.field public static final T:Lp/a$d;

.field public static final U:Lp/a$d;

.field public static final V:Lp/a$d;

.field public static final W:Lp/a$d;

.field public static final X:Lp/a$d;

.field public static final Y:Lp/a$d;

.field public static final Z:Lp/a$d;

.field public static final a:Lp/a$b;

.field public static final a0:Lp/a$d;

.field public static final b:Lp/a$b;

.field public static final c:Lp/a$e;

.field public static final d:Lp/a$c;

.field public static final e:Lp/a$f;

.field public static final f:Lp/a$f;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final g:Lp/a$f;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final h:Lp/a$f;

.field public static final i:Lp/a$f;

.field public static final j:Lp/a$f;

.field public static final k:Lp/a$c;

.field public static final l:Lp/a$c;

.field public static final m:Lp/a$c;

.field public static final n:Lp/a$c;

.field public static final o:Lp/a$c;

.field public static final p:Lp/a$c;

.field public static final q:Lp/a$b;

.field public static final r:Lp/a$b;

.field public static final s:Lp/a$c;

.field public static final t:Lp/a$f;

.field public static final u:Lp/a$c;

.field public static final v:Lp/a$b;

.field public static final w:Lp/a$b;

.field public static final x:Lp/a$f;

.field public static final y:Lp/a$f;

.field public static final z:Lp/a$f;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lp/a$b;

    const-string v1, "VISUAL_STATE_CALLBACK"

    invoke-direct {v0, v1, v1}, Lp/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->a:Lp/a$b;

    new-instance v0, Lp/a$b;

    const-string v1, "OFF_SCREEN_PRERASTER"

    invoke-direct {v0, v1, v1}, Lp/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->b:Lp/a$b;

    new-instance v0, Lp/a$e;

    const-string v1, "SAFE_BROWSING_ENABLE"

    invoke-direct {v0, v1, v1}, Lp/a$e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->c:Lp/a$e;

    new-instance v0, Lp/a$c;

    const-string v1, "DISABLED_ACTION_MODE_MENU_ITEMS"

    invoke-direct {v0, v1, v1}, Lp/a$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->d:Lp/a$c;

    new-instance v0, Lp/a$f;

    const-string v1, "START_SAFE_BROWSING"

    invoke-direct {v0, v1, v1}, Lp/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->e:Lp/a$f;

    new-instance v0, Lp/a$f;

    const-string v1, "SAFE_BROWSING_WHITELIST"

    invoke-direct {v0, v1, v1}, Lp/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->f:Lp/a$f;

    new-instance v0, Lp/a$f;

    const-string v2, "SAFE_BROWSING_ALLOWLIST"

    invoke-direct {v0, v1, v2}, Lp/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->g:Lp/a$f;

    new-instance v0, Lp/a$f;

    invoke-direct {v0, v2, v1}, Lp/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->h:Lp/a$f;

    new-instance v0, Lp/a$f;

    invoke-direct {v0, v2, v2}, Lp/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->i:Lp/a$f;

    new-instance v0, Lp/a$f;

    const-string v1, "SAFE_BROWSING_PRIVACY_POLICY_URL"

    invoke-direct {v0, v1, v1}, Lp/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->j:Lp/a$f;

    new-instance v0, Lp/a$c;

    const-string v1, "SERVICE_WORKER_BASIC_USAGE"

    invoke-direct {v0, v1, v1}, Lp/a$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->k:Lp/a$c;

    new-instance v0, Lp/a$c;

    const-string v1, "SERVICE_WORKER_CACHE_MODE"

    invoke-direct {v0, v1, v1}, Lp/a$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->l:Lp/a$c;

    new-instance v0, Lp/a$c;

    const-string v1, "SERVICE_WORKER_CONTENT_ACCESS"

    invoke-direct {v0, v1, v1}, Lp/a$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->m:Lp/a$c;

    new-instance v0, Lp/a$c;

    const-string v1, "SERVICE_WORKER_FILE_ACCESS"

    invoke-direct {v0, v1, v1}, Lp/a$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->n:Lp/a$c;

    new-instance v0, Lp/a$c;

    const-string v1, "SERVICE_WORKER_BLOCK_NETWORK_LOADS"

    invoke-direct {v0, v1, v1}, Lp/a$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->o:Lp/a$c;

    new-instance v0, Lp/a$c;

    const-string v1, "SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST"

    invoke-direct {v0, v1, v1}, Lp/a$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->p:Lp/a$c;

    new-instance v0, Lp/a$b;

    const-string v1, "RECEIVE_WEB_RESOURCE_ERROR"

    invoke-direct {v0, v1, v1}, Lp/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->q:Lp/a$b;

    new-instance v0, Lp/a$b;

    const-string v1, "RECEIVE_HTTP_ERROR"

    invoke-direct {v0, v1, v1}, Lp/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->r:Lp/a$b;

    new-instance v0, Lp/a$c;

    const-string v1, "SHOULD_OVERRIDE_WITH_REDIRECTS"

    invoke-direct {v0, v1, v1}, Lp/a$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->s:Lp/a$c;

    new-instance v0, Lp/a$f;

    const-string v1, "SAFE_BROWSING_HIT"

    invoke-direct {v0, v1, v1}, Lp/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->t:Lp/a$f;

    new-instance v0, Lp/a$c;

    const-string v1, "WEB_RESOURCE_REQUEST_IS_REDIRECT"

    invoke-direct {v0, v1, v1}, Lp/a$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->u:Lp/a$c;

    new-instance v0, Lp/a$b;

    const-string v1, "WEB_RESOURCE_ERROR_GET_DESCRIPTION"

    invoke-direct {v0, v1, v1}, Lp/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->v:Lp/a$b;

    new-instance v0, Lp/a$b;

    const-string v1, "WEB_RESOURCE_ERROR_GET_CODE"

    invoke-direct {v0, v1, v1}, Lp/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->w:Lp/a$b;

    new-instance v0, Lp/a$f;

    const-string v1, "SAFE_BROWSING_RESPONSE_BACK_TO_SAFETY"

    invoke-direct {v0, v1, v1}, Lp/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->x:Lp/a$f;

    new-instance v0, Lp/a$f;

    const-string v1, "SAFE_BROWSING_RESPONSE_PROCEED"

    const-string v2, "SAFE_BROWSING_RESPONSE_PROCEED"

    invoke-direct {v0, v1, v2}, Lp/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->y:Lp/a$f;

    new-instance v0, Lp/a$f;

    const-string v1, "SAFE_BROWSING_RESPONSE_SHOW_INTERSTITIAL"

    const-string v2, "SAFE_BROWSING_RESPONSE_SHOW_INTERSTITIAL"

    invoke-direct {v0, v1, v2}, Lp/a$f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->z:Lp/a$f;

    new-instance v0, Lp/a$b;

    const-string v1, "WEB_MESSAGE_PORT_POST_MESSAGE"

    const-string v2, "WEB_MESSAGE_PORT_POST_MESSAGE"

    invoke-direct {v0, v1, v2}, Lp/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->A:Lp/a$b;

    new-instance v0, Lp/a$b;

    const-string v1, "WEB_MESSAGE_PORT_CLOSE"

    const-string v2, "WEB_MESSAGE_PORT_CLOSE"

    invoke-direct {v0, v1, v2}, Lp/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->B:Lp/a$b;

    new-instance v0, Lp/a$d;

    const-string v1, "WEB_MESSAGE_GET_MESSAGE_PAYLOAD"

    const-string v2, "WEB_MESSAGE_GET_MESSAGE_PAYLOAD"

    invoke-direct {v0, v1, v2}, Lp/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->C:Lp/a$d;

    new-instance v0, Lp/a$b;

    const-string v1, "WEB_MESSAGE_PORT_SET_MESSAGE_CALLBACK"

    const-string v2, "WEB_MESSAGE_PORT_SET_MESSAGE_CALLBACK"

    invoke-direct {v0, v1, v2}, Lp/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->D:Lp/a$b;

    new-instance v0, Lp/a$b;

    const-string v1, "CREATE_WEB_MESSAGE_CHANNEL"

    const-string v2, "CREATE_WEB_MESSAGE_CHANNEL"

    invoke-direct {v0, v1, v2}, Lp/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->E:Lp/a$b;

    new-instance v0, Lp/a$b;

    const-string v1, "POST_WEB_MESSAGE"

    const-string v2, "POST_WEB_MESSAGE"

    invoke-direct {v0, v1, v2}, Lp/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->F:Lp/a$b;

    new-instance v0, Lp/a$b;

    const-string v1, "WEB_MESSAGE_CALLBACK_ON_MESSAGE"

    const-string v2, "WEB_MESSAGE_CALLBACK_ON_MESSAGE"

    invoke-direct {v0, v1, v2}, Lp/a$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->G:Lp/a$b;

    new-instance v0, Lp/a$e;

    const-string v1, "GET_WEB_VIEW_CLIENT"

    const-string v2, "GET_WEB_VIEW_CLIENT"

    invoke-direct {v0, v1, v2}, Lp/a$e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->H:Lp/a$e;

    new-instance v0, Lp/a$e;

    const-string v1, "GET_WEB_CHROME_CLIENT"

    const-string v2, "GET_WEB_CHROME_CLIENT"

    invoke-direct {v0, v1, v2}, Lp/a$e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->I:Lp/a$e;

    new-instance v0, Lp/a$h;

    const-string v1, "GET_WEB_VIEW_RENDERER"

    const-string v2, "GET_WEB_VIEW_RENDERER"

    invoke-direct {v0, v1, v2}, Lp/a$h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->J:Lp/a$h;

    new-instance v0, Lp/a$h;

    const-string v1, "WEB_VIEW_RENDERER_TERMINATE"

    const-string v2, "WEB_VIEW_RENDERER_TERMINATE"

    invoke-direct {v0, v1, v2}, Lp/a$h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->K:Lp/a$h;

    new-instance v0, Lp/a$g;

    const-string v1, "TRACING_CONTROLLER_BASIC_USAGE"

    const-string v2, "TRACING_CONTROLLER_BASIC_USAGE"

    invoke-direct {v0, v1, v2}, Lp/a$g;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->L:Lp/a$g;

    new-instance v0, Lp/i$a;

    const-string v1, "STARTUP_FEATURE_SET_DATA_DIRECTORY_SUFFIX"

    const-string v2, "STARTUP_FEATURE_SET_DATA_DIRECTORY_SUFFIX"

    invoke-direct {v0, v1, v2}, Lp/i$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->M:Lp/i$a;

    new-instance v0, Lp/a$h;

    const-string v1, "WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE"

    const-string v2, "WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE"

    invoke-direct {v0, v1, v2}, Lp/a$h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->N:Lp/a$h;

    new-instance v0, Lp/l$a;

    const-string v1, "ALGORITHMIC_DARKENING"

    const-string v2, "ALGORITHMIC_DARKENING"

    invoke-direct {v0, v1, v2}, Lp/l$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->O:Lp/a$i;

    new-instance v0, Lp/a$d;

    const-string v1, "PROXY_OVERRIDE"

    const-string v2, "PROXY_OVERRIDE:3"

    invoke-direct {v0, v1, v2}, Lp/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->P:Lp/a$d;

    new-instance v0, Lp/a$d;

    const-string v1, "SUPPRESS_ERROR_PAGE"

    const-string v2, "SUPPRESS_ERROR_PAGE"

    invoke-direct {v0, v1, v2}, Lp/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->Q:Lp/a$d;

    new-instance v0, Lp/a$d;

    const-string v1, "MULTI_PROCESS"

    const-string v2, "MULTI_PROCESS_QUERY"

    invoke-direct {v0, v1, v2}, Lp/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->R:Lp/a$d;

    new-instance v0, Lp/a$h;

    const-string v1, "FORCE_DARK"

    const-string v2, "FORCE_DARK"

    invoke-direct {v0, v1, v2}, Lp/a$h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->S:Lp/a$h;

    new-instance v0, Lp/a$d;

    const-string v1, "FORCE_DARK_STRATEGY"

    const-string v2, "FORCE_DARK_BEHAVIOR"

    invoke-direct {v0, v1, v2}, Lp/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->T:Lp/a$d;

    new-instance v0, Lp/a$d;

    const-string v1, "WEB_MESSAGE_LISTENER"

    const-string v2, "WEB_MESSAGE_LISTENER"

    invoke-direct {v0, v1, v2}, Lp/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->U:Lp/a$d;

    new-instance v0, Lp/a$d;

    const-string v1, "DOCUMENT_START_SCRIPT"

    const-string v2, "DOCUMENT_START_SCRIPT:1"

    invoke-direct {v0, v1, v2}, Lp/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->V:Lp/a$d;

    new-instance v0, Lp/a$d;

    const-string v1, "PROXY_OVERRIDE_REVERSE_BYPASS"

    const-string v2, "PROXY_OVERRIDE_REVERSE_BYPASS"

    invoke-direct {v0, v1, v2}, Lp/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->W:Lp/a$d;

    new-instance v0, Lp/a$d;

    const-string v1, "GET_VARIATIONS_HEADER"

    const-string v2, "GET_VARIATIONS_HEADER"

    invoke-direct {v0, v1, v2}, Lp/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->X:Lp/a$d;

    new-instance v0, Lp/a$d;

    const-string v1, "ENTERPRISE_AUTHENTICATION_APP_LINK_POLICY"

    const-string v2, "ENTERPRISE_AUTHENTICATION_APP_LINK_POLICY"

    invoke-direct {v0, v1, v2}, Lp/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->Y:Lp/a$d;

    new-instance v0, Lp/a$d;

    const-string v1, "GET_COOKIE_INFO"

    const-string v2, "GET_COOKIE_INFO"

    invoke-direct {v0, v1, v2}, Lp/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->Z:Lp/a$d;

    new-instance v0, Lp/a$d;

    const-string v1, "REQUESTED_WITH_HEADER_ALLOW_LIST"

    const-string v2, "REQUESTED_WITH_HEADER_ALLOW_LIST"

    invoke-direct {v0, v1, v2}, Lp/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lp/l;->a0:Lp/a$d;

    return-void
.end method

.method public static a()Ljava/lang/UnsupportedOperationException;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not supported by the current version of the framework and the current WebView APK"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    invoke-static {}, Lp/a;->e()Ljava/util/Set;

    move-result-object v0

    invoke-static {p0, v0}, Lp/l;->c(Ljava/lang/String;Ljava/util/Collection;)Z

    move-result p0

    return p0
.end method

.method public static c(Ljava/lang/String;Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lp/f;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "TT;>;)Z"
        }
    .end annotation

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lp/f;

    invoke-interface {v1}, Lp/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_4

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lp/f;

    invoke-interface {p1}, Lp/f;->b()Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p0, 0x1

    return p0

    :cond_3
    const/4 p0, 0x0

    return p0

    :cond_4
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown feature "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
