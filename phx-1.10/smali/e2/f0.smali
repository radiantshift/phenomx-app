.class public final Le2/f0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Le2/t;


# instance fields
.field private final f:Le2/d;

.field private g:Z

.field private h:J

.field private i:J

.field private j:Lh0/f3;


# direct methods
.method public constructor <init>(Le2/d;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le2/f0;->f:Le2/d;

    sget-object p1, Lh0/f3;->i:Lh0/f3;

    iput-object p1, p0, Le2/f0;->j:Lh0/f3;

    return-void
.end method


# virtual methods
.method public A()J
    .locals 7

    iget-wide v0, p0, Le2/f0;->h:J

    iget-boolean v2, p0, Le2/f0;->g:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Le2/f0;->f:Le2/d;

    invoke-interface {v2}, Le2/d;->d()J

    move-result-wide v2

    iget-wide v4, p0, Le2/f0;->i:J

    sub-long/2addr v2, v4

    iget-object v4, p0, Le2/f0;->j:Lh0/f3;

    iget v5, v4, Lh0/f3;->f:F

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    invoke-static {v2, v3}, Le2/n0;->C0(J)J

    move-result-wide v2

    goto :goto_0

    :cond_0
    invoke-virtual {v4, v2, v3}, Lh0/f3;->b(J)J

    move-result-wide v2

    :goto_0
    add-long/2addr v0, v2

    :cond_1
    return-wide v0
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, Le2/f0;->h:J

    iget-boolean p1, p0, Le2/f0;->g:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Le2/f0;->f:Le2/d;

    invoke-interface {p1}, Le2/d;->d()J

    move-result-wide p1

    iput-wide p1, p0, Le2/f0;->i:J

    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    iget-boolean v0, p0, Le2/f0;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Le2/f0;->f:Le2/d;

    invoke-interface {v0}, Le2/d;->d()J

    move-result-wide v0

    iput-wide v0, p0, Le2/f0;->i:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Le2/f0;->g:Z

    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    iget-boolean v0, p0, Le2/f0;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Le2/f0;->A()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Le2/f0;->a(J)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Le2/f0;->g:Z

    :cond_0
    return-void
.end method

.method public g()Lh0/f3;
    .locals 1

    iget-object v0, p0, Le2/f0;->j:Lh0/f3;

    return-object v0
.end method

.method public h(Lh0/f3;)V
    .locals 2

    iget-boolean v0, p0, Le2/f0;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Le2/f0;->A()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Le2/f0;->a(J)V

    :cond_0
    iput-object p1, p0, Le2/f0;->j:Lh0/f3;

    return-void
.end method
