.class final Le2/e0$c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ld2/h0$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le2/e0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ld2/h0$b<",
        "Ld2/h0$e;",
        ">;"
    }
.end annotation


# instance fields
.field private final f:Le2/e0$b;


# direct methods
.method public constructor <init>(Le2/e0$b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le2/e0$c;->f:Le2/e0$b;

    return-void
.end method


# virtual methods
.method public l(Ld2/h0$e;JJLjava/io/IOException;I)Ld2/h0$c;
    .locals 0

    iget-object p1, p0, Le2/e0$c;->f:Le2/e0$b;

    if-eqz p1, :cond_0

    invoke-interface {p1, p6}, Le2/e0$b;->b(Ljava/io/IOException;)V

    :cond_0
    sget-object p1, Ld2/h0;->f:Ld2/h0$c;

    return-object p1
.end method

.method public m(Ld2/h0$e;JJZ)V
    .locals 0

    return-void
.end method

.method public t(Ld2/h0$e;JJ)V
    .locals 0

    iget-object p1, p0, Le2/e0$c;->f:Le2/e0$b;

    if-eqz p1, :cond_1

    invoke-static {}, Le2/e0;->k()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Le2/e0$c;->f:Le2/e0$b;

    new-instance p2, Ljava/io/IOException;

    new-instance p3, Ljava/util/ConcurrentModificationException;

    invoke-direct {p3}, Ljava/util/ConcurrentModificationException;-><init>()V

    invoke-direct {p2, p3}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {p1, p2}, Le2/e0$b;->b(Ljava/io/IOException;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Le2/e0$c;->f:Le2/e0$b;

    invoke-interface {p1}, Le2/e0$b;->a()V

    :cond_1
    :goto_0
    return-void
.end method
