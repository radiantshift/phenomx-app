.class public Li0/p1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Li0/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Li0/p1$a;
    }
.end annotation


# instance fields
.field private final f:Le2/d;

.field private final g:Lh0/c4$b;

.field private final h:Lh0/c4$d;

.field private final i:Li0/p1$a;

.field private final j:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Li0/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Le2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Le2/q<",
            "Li0/c;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lh0/g3;

.field private m:Le2/n;

.field private n:Z


# direct methods
.method public constructor <init>(Le2/d;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le2/d;

    iput-object v0, p0, Li0/p1;->f:Le2/d;

    new-instance v0, Le2/q;

    invoke-static {}, Le2/n0;->Q()Landroid/os/Looper;

    move-result-object v1

    sget-object v2, Li0/j1;->a:Li0/j1;

    invoke-direct {v0, v1, p1, v2}, Le2/q;-><init>(Landroid/os/Looper;Le2/d;Le2/q$b;)V

    iput-object v0, p0, Li0/p1;->k:Le2/q;

    new-instance p1, Lh0/c4$b;

    invoke-direct {p1}, Lh0/c4$b;-><init>()V

    iput-object p1, p0, Li0/p1;->g:Lh0/c4$b;

    new-instance v0, Lh0/c4$d;

    invoke-direct {v0}, Lh0/c4$d;-><init>()V

    iput-object v0, p0, Li0/p1;->h:Lh0/c4$d;

    new-instance v0, Li0/p1$a;

    invoke-direct {v0, p1}, Li0/p1$a;-><init>(Lh0/c4$b;)V

    iput-object v0, p0, Li0/p1;->i:Li0/p1$a;

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Li0/p1;->j:Landroid/util/SparseArray;

    return-void
.end method

.method public static synthetic A0(Li0/c$a;Lk0/e;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->O2(Li0/c$a;Lk0/e;Li0/c;)V

    return-void
.end method

.method public static synthetic A1(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-static {p0, p1}, Li0/p1;->M1(Li0/c$a;Li0/c;)V

    return-void
.end method

.method private static synthetic A2(Li0/c$a;ZILi0/c;)V
    .locals 0

    invoke-interface {p3, p0, p1, p2}, Li0/c;->g(Li0/c$a;ZI)V

    return-void
.end method

.method public static synthetic B0(Li0/c$a;Lh0/o;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->b2(Li0/c$a;Lh0/o;Li0/c;)V

    return-void
.end method

.method public static synthetic B1(Li0/c$a;Ljava/lang/String;JJLi0/c;)V
    .locals 0

    invoke-static/range {p0 .. p6}, Li0/p1;->M2(Li0/c$a;Ljava/lang/String;JJLi0/c;)V

    return-void
.end method

.method private static synthetic B2(Li0/c$a;ILh0/g3$e;Lh0/g3$e;Li0/c;)V
    .locals 0

    invoke-interface {p4, p0, p1}, Li0/c;->p0(Li0/c$a;I)V

    invoke-interface {p4, p0, p2, p3, p1}, Li0/c;->I(Li0/c$a;Lh0/g3$e;Lh0/g3$e;I)V

    return-void
.end method

.method public static synthetic C0(Li0/c$a;ZLi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->F2(Li0/c$a;ZLi0/c;)V

    return-void
.end method

.method public static synthetic C1(Li0/c$a;Lh0/c3;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->z2(Li0/c$a;Lh0/c3;Li0/c;)V

    return-void
.end method

.method private static synthetic C2(Li0/c$a;Ljava/lang/Object;JLi0/c;)V
    .locals 0

    invoke-interface {p4, p0, p1, p2, p3}, Li0/c;->p(Li0/c$a;Ljava/lang/Object;J)V

    return-void
.end method

.method public static synthetic D0(Li0/c$a;FLi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->T2(Li0/c$a;FLi0/c;)V

    return-void
.end method

.method private static synthetic D2(Li0/c$a;ILi0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->e(Li0/c$a;I)V

    return-void
.end method

.method public static synthetic E0(Li0/c$a;ZLi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->G2(Li0/c$a;ZLi0/c;)V

    return-void
.end method

.method private static synthetic E2(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-interface {p1, p0}, Li0/c;->h(Li0/c$a;)V

    return-void
.end method

.method public static synthetic F0(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-static {p0, p1}, Li0/p1;->j2(Li0/c$a;Li0/c;)V

    return-void
.end method

.method private F1(Lj1/x$b;)Li0/c$a;
    .locals 3

    iget-object v0, p0, Li0/p1;->l:Lh0/g3;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    move-object v1, v0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Li0/p1;->i:Li0/p1$a;

    invoke-virtual {v1, p1}, Li0/p1$a;->f(Lj1/x$b;)Lh0/c4;

    move-result-object v1

    :goto_0
    if-eqz p1, :cond_2

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    iget-object v0, p1, Lj1/v;->a:Ljava/lang/Object;

    iget-object v2, p0, Li0/p1;->g:Lh0/c4$b;

    invoke-virtual {v1, v0, v2}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v0

    iget v0, v0, Lh0/c4$b;->h:I

    invoke-virtual {p0, v1, v0, p1}, Li0/p1;->E1(Lh0/c4;ILj1/x$b;)Li0/c$a;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_1
    iget-object p1, p0, Li0/p1;->l:Lh0/g3;

    invoke-interface {p1}, Lh0/g3;->E()I

    move-result p1

    iget-object v1, p0, Li0/p1;->l:Lh0/g3;

    invoke-interface {v1}, Lh0/g3;->N()Lh0/c4;

    move-result-object v1

    invoke-virtual {v1}, Lh0/c4;->t()I

    move-result v2

    if-ge p1, v2, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_4

    goto :goto_3

    :cond_4
    sget-object v1, Lh0/c4;->f:Lh0/c4;

    :goto_3
    invoke-virtual {p0, v1, p1, v0}, Li0/p1;->E1(Lh0/c4;ILj1/x$b;)Li0/c$a;

    move-result-object p1

    return-object p1
.end method

.method private static synthetic F2(Li0/c$a;ZLi0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->N(Li0/c$a;Z)V

    return-void
.end method

.method public static synthetic G0(Li0/c$a;Ljava/util/List;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->Z1(Li0/c$a;Ljava/util/List;Li0/c;)V

    return-void
.end method

.method private G1()Li0/c$a;
    .locals 1

    iget-object v0, p0, Li0/p1;->i:Li0/p1$a;

    invoke-virtual {v0}, Li0/p1$a;->e()Lj1/x$b;

    move-result-object v0

    invoke-direct {p0, v0}, Li0/p1;->F1(Lj1/x$b;)Li0/c$a;

    move-result-object v0

    return-object v0
.end method

.method private static synthetic G2(Li0/c$a;ZLi0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->J(Li0/c$a;Z)V

    return-void
.end method

.method public static synthetic H0(Li0/c$a;Lh0/f3;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->v2(Li0/c$a;Lh0/f3;Li0/c;)V

    return-void
.end method

.method private H1(ILj1/x$b;)Li0/c$a;
    .locals 3

    iget-object v0, p0, Li0/p1;->l:Lh0/g3;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    iget-object v2, p0, Li0/p1;->i:Li0/p1$a;

    invoke-virtual {v2, p2}, Li0/p1$a;->f(Lj1/x$b;)Lh0/c4;

    move-result-object v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Li0/p1;->F1(Lj1/x$b;)Li0/c$a;

    move-result-object p1

    goto :goto_1

    :cond_1
    sget-object v0, Lh0/c4;->f:Lh0/c4;

    invoke-virtual {p0, v0, p1, p2}, Li0/p1;->E1(Lh0/c4;ILj1/x$b;)Li0/c$a;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_2
    iget-object p2, p0, Li0/p1;->l:Lh0/g3;

    invoke-interface {p2}, Lh0/g3;->N()Lh0/c4;

    move-result-object p2

    invoke-virtual {p2}, Lh0/c4;->t()I

    move-result v2

    if-ge p1, v2, :cond_3

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    sget-object p2, Lh0/c4;->f:Lh0/c4;

    :goto_3
    const/4 v0, 0x0

    invoke-virtual {p0, p2, p1, v0}, Li0/p1;->E1(Lh0/c4;ILj1/x$b;)Li0/c$a;

    move-result-object p1

    return-object p1
.end method

.method private static synthetic H2(Li0/c$a;IILi0/c;)V
    .locals 0

    invoke-interface {p3, p0, p1, p2}, Li0/c;->Q(Li0/c$a;II)V

    return-void
.end method

.method public static synthetic I0(Li0/c$a;Lh0/z1;ILi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Li0/p1;->r2(Li0/c$a;Lh0/z1;ILi0/c;)V

    return-void
.end method

.method private I1()Li0/c$a;
    .locals 1

    iget-object v0, p0, Li0/p1;->i:Li0/p1$a;

    invoke-virtual {v0}, Li0/p1$a;->g()Lj1/x$b;

    move-result-object v0

    invoke-direct {p0, v0}, Li0/p1;->F1(Lj1/x$b;)Li0/c$a;

    move-result-object v0

    return-object v0
.end method

.method private static synthetic I2(Li0/c$a;ILi0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->t(Li0/c$a;I)V

    return-void
.end method

.method public static synthetic J0(Li0/c$a;Lh0/c3;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->y2(Li0/c$a;Lh0/c3;Li0/c;)V

    return-void
.end method

.method private J1()Li0/c$a;
    .locals 1

    iget-object v0, p0, Li0/p1;->i:Li0/p1$a;

    invoke-virtual {v0}, Li0/p1$a;->h()Lj1/x$b;

    move-result-object v0

    invoke-direct {p0, v0}, Li0/p1;->F1(Lj1/x$b;)Li0/c$a;

    move-result-object v0

    return-object v0
.end method

.method private static synthetic J2(Li0/c$a;Lh0/h4;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->R(Li0/c$a;Lh0/h4;)V

    return-void
.end method

.method public static synthetic K0(Li0/c$a;Ljava/lang/Exception;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->L2(Li0/c$a;Ljava/lang/Exception;Li0/c;)V

    return-void
.end method

.method private K1(Lh0/c3;)Li0/c$a;
    .locals 1

    instance-of v0, p1, Lh0/q;

    if-eqz v0, :cond_0

    check-cast p1, Lh0/q;

    iget-object p1, p1, Lh0/q;->s:Lj1/v;

    if-eqz p1, :cond_0

    new-instance v0, Lj1/x$b;

    invoke-direct {v0, p1}, Lj1/x$b;-><init>(Lj1/v;)V

    invoke-direct {p0, v0}, Li0/p1;->F1(Lj1/x$b;)Li0/c$a;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object p1

    return-object p1
.end method

.method private static synthetic K2(Li0/c$a;Lj1/t;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->f0(Li0/c$a;Lj1/t;)V

    return-void
.end method

.method public static synthetic L0(Li0/c$a;Lj1/q;Lj1/t;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Li0/p1;->o2(Li0/c$a;Lj1/q;Lj1/t;Li0/c;)V

    return-void
.end method

.method private static synthetic L1(Li0/c;Le2/l;)V
    .locals 0

    return-void
.end method

.method private static synthetic L2(Li0/c$a;Ljava/lang/Exception;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->s0(Li0/c$a;Ljava/lang/Exception;)V

    return-void
.end method

.method public static synthetic M0(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-static {p0, p1}, Li0/p1;->U2(Li0/c$a;Li0/c;)V

    return-void
.end method

.method private static synthetic M1(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-interface {p1, p0}, Li0/c;->u(Li0/c$a;)V

    return-void
.end method

.method private static synthetic M2(Li0/c$a;Ljava/lang/String;JJLi0/c;)V
    .locals 7

    invoke-interface {p6, p0, p1, p2, p3}, Li0/c;->y(Li0/c$a;Ljava/lang/String;J)V

    move-object v0, p6

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p4

    move-wide v5, p2

    invoke-interface/range {v0 .. v6}, Li0/c;->D(Li0/c$a;Ljava/lang/String;JJ)V

    const/4 v3, 0x2

    move-object v1, p6

    move-object v2, p0

    move-object v4, p1

    invoke-interface/range {v1 .. v6}, Li0/c;->j0(Li0/c$a;ILjava/lang/String;J)V

    return-void
.end method

.method public static synthetic N0(Li0/c$a;IILi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Li0/p1;->H2(Li0/c$a;IILi0/c;)V

    return-void
.end method

.method private static synthetic N1(Li0/c$a;Lj0/e;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->h0(Li0/c$a;Lj0/e;)V

    return-void
.end method

.method private static synthetic N2(Li0/c$a;Ljava/lang/String;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->v(Li0/c$a;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic O0(Li0/c$a;Lj1/t;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->K2(Li0/c$a;Lj1/t;Li0/c;)V

    return-void
.end method

.method private static synthetic O1(Li0/c$a;Ljava/lang/Exception;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->X(Li0/c$a;Ljava/lang/Exception;)V

    return-void
.end method

.method private static synthetic O2(Li0/c$a;Lk0/e;Li0/c;)V
    .locals 1

    invoke-interface {p2, p0, p1}, Li0/c;->E(Li0/c$a;Lk0/e;)V

    const/4 v0, 0x2

    invoke-interface {p2, p0, v0, p1}, Li0/c;->j(Li0/c$a;ILk0/e;)V

    return-void
.end method

.method public static synthetic P0(Li0/c$a;Lz0/a;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->t2(Li0/c$a;Lz0/a;Li0/c;)V

    return-void
.end method

.method private static synthetic P1(Li0/c$a;Ljava/lang/String;JJLi0/c;)V
    .locals 7

    invoke-interface {p6, p0, p1, p2, p3}, Li0/c;->b(Li0/c$a;Ljava/lang/String;J)V

    move-object v0, p6

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p4

    move-wide v5, p2

    invoke-interface/range {v0 .. v6}, Li0/c;->q0(Li0/c$a;Ljava/lang/String;JJ)V

    const/4 v3, 0x1

    move-object v1, p6

    move-object v2, p0

    move-object v4, p1

    invoke-interface/range {v1 .. v6}, Li0/c;->j0(Li0/c$a;ILjava/lang/String;J)V

    return-void
.end method

.method private static synthetic P2(Li0/c$a;Lk0/e;Li0/c;)V
    .locals 1

    invoke-interface {p2, p0, p1}, Li0/c;->d0(Li0/c$a;Lk0/e;)V

    const/4 v0, 0x2

    invoke-interface {p2, p0, v0, p1}, Li0/c;->l(Li0/c$a;ILk0/e;)V

    return-void
.end method

.method public static synthetic Q0(Li0/c$a;ILi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->h2(Li0/c$a;ILi0/c;)V

    return-void
.end method

.method private static synthetic Q1(Li0/c$a;Ljava/lang/String;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->M(Li0/c$a;Ljava/lang/String;)V

    return-void
.end method

.method private static synthetic Q2(Li0/c$a;JILi0/c;)V
    .locals 0

    invoke-interface {p4, p0, p1, p2, p3}, Li0/c;->n0(Li0/c$a;JI)V

    return-void
.end method

.method public static synthetic R0(Li0/c$a;Lh0/r1;Lk0/i;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Li0/p1;->T1(Li0/c$a;Lh0/r1;Lk0/i;Li0/c;)V

    return-void
.end method

.method private static synthetic R1(Li0/c$a;Lk0/e;Li0/c;)V
    .locals 1

    invoke-interface {p2, p0, p1}, Li0/c;->i0(Li0/c$a;Lk0/e;)V

    const/4 v0, 0x1

    invoke-interface {p2, p0, v0, p1}, Li0/c;->j(Li0/c$a;ILk0/e;)V

    return-void
.end method

.method private static synthetic R2(Li0/c$a;Lh0/r1;Lk0/i;Li0/c;)V
    .locals 0

    invoke-interface {p3, p0, p1}, Li0/c;->u0(Li0/c$a;Lh0/r1;)V

    invoke-interface {p3, p0, p1, p2}, Li0/c;->q(Li0/c$a;Lh0/r1;Lk0/i;)V

    const/4 p2, 0x2

    invoke-interface {p3, p0, p2, p1}, Li0/c;->i(Li0/c$a;ILh0/r1;)V

    return-void
.end method

.method public static synthetic S0(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-static {p0, p1}, Li0/p1;->e2(Li0/c$a;Li0/c;)V

    return-void
.end method

.method private static synthetic S1(Li0/c$a;Lk0/e;Li0/c;)V
    .locals 1

    invoke-interface {p2, p0, p1}, Li0/c;->m(Li0/c$a;Lk0/e;)V

    const/4 v0, 0x1

    invoke-interface {p2, p0, v0, p1}, Li0/c;->l(Li0/c$a;ILk0/e;)V

    return-void
.end method

.method private static synthetic S2(Li0/c$a;Lf2/z;Li0/c;)V
    .locals 6

    invoke-interface {p2, p0, p1}, Li0/c;->A(Li0/c$a;Lf2/z;)V

    iget v2, p1, Lf2/z;->f:I

    iget v3, p1, Lf2/z;->g:I

    iget v4, p1, Lf2/z;->h:I

    iget v5, p1, Lf2/z;->i:F

    move-object v0, p2

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Li0/c;->b0(Li0/c$a;IIIF)V

    return-void
.end method

.method public static synthetic T0(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-static {p0, p1}, Li0/p1;->E2(Li0/c$a;Li0/c;)V

    return-void
.end method

.method private static synthetic T1(Li0/c$a;Lh0/r1;Lk0/i;Li0/c;)V
    .locals 0

    invoke-interface {p3, p0, p1}, Li0/c;->k0(Li0/c$a;Lh0/r1;)V

    invoke-interface {p3, p0, p1, p2}, Li0/c;->S(Li0/c$a;Lh0/r1;Lk0/i;)V

    const/4 p2, 0x1

    invoke-interface {p3, p0, p2, p1}, Li0/c;->i(Li0/c$a;ILh0/r1;)V

    return-void
.end method

.method private static synthetic T2(Li0/c$a;FLi0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->F(Li0/c$a;F)V

    return-void
.end method

.method public static synthetic U0(Li0/c$a;Lj1/q;Lj1/t;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Li0/p1;->n2(Li0/c$a;Lj1/q;Lj1/t;Li0/c;)V

    return-void
.end method

.method private static synthetic U1(Li0/c$a;JLi0/c;)V
    .locals 0

    invoke-interface {p3, p0, p1, p2}, Li0/c;->f(Li0/c$a;J)V

    return-void
.end method

.method private static synthetic U2(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-interface {p1, p0}, Li0/c;->x(Li0/c$a;)V

    return-void
.end method

.method public static synthetic V0(Li0/c$a;IJJLi0/c;)V
    .locals 0

    invoke-static/range {p0 .. p6}, Li0/p1;->Y1(Li0/c$a;IJJLi0/c;)V

    return-void
.end method

.method private static synthetic V1(Li0/c$a;Ljava/lang/Exception;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->K(Li0/c$a;Ljava/lang/Exception;)V

    return-void
.end method

.method private synthetic V2(Lh0/g3;Li0/c;Le2/l;)V
    .locals 2

    new-instance v0, Li0/c$b;

    iget-object v1, p0, Li0/p1;->j:Landroid/util/SparseArray;

    invoke-direct {v0, p3, v1}, Li0/c$b;-><init>(Le2/l;Landroid/util/SparseArray;)V

    invoke-interface {p2, p1, v0}, Li0/c;->y0(Lh0/g3;Li0/c$b;)V

    return-void
.end method

.method public static synthetic W0(Li0/c$a;ILh0/g3$e;Lh0/g3$e;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Li0/p1;->B2(Li0/c$a;ILh0/g3$e;Lh0/g3$e;Li0/c;)V

    return-void
.end method

.method private static synthetic W1(Li0/c$a;IJJLi0/c;)V
    .locals 7

    move-object v0, p6

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-interface/range {v0 .. v6}, Li0/c;->r(Li0/c$a;IJJ)V

    return-void
.end method

.method private W2()V
    .locals 3

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/o;

    invoke-direct {v1, v0}, Li0/o;-><init>(Li0/c$a;)V

    const/16 v2, 0x404

    invoke-virtual {p0, v0, v2, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    iget-object v0, p0, Li0/p1;->k:Le2/q;

    invoke-virtual {v0}, Le2/q;->j()V

    return-void
.end method

.method public static synthetic X0(Li0/c$a;Lj0/e;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->N1(Li0/c$a;Lj0/e;Li0/c;)V

    return-void
.end method

.method private static synthetic X1(Li0/c$a;Lh0/g3$b;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->z(Li0/c$a;Lh0/g3$b;)V

    return-void
.end method

.method public static synthetic Y0(Li0/c$a;Lj1/q;Lj1/t;Ljava/io/IOException;ZLi0/c;)V
    .locals 0

    invoke-static/range {p0 .. p5}, Li0/p1;->p2(Li0/c$a;Lj1/q;Lj1/t;Ljava/io/IOException;ZLi0/c;)V

    return-void
.end method

.method private static synthetic Y1(Li0/c$a;IJJLi0/c;)V
    .locals 7

    move-object v0, p6

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-interface/range {v0 .. v6}, Li0/c;->Z(Li0/c$a;IJJ)V

    return-void
.end method

.method public static synthetic Z0(Li0/c$a;Ls1/e;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->a2(Li0/c$a;Ls1/e;Li0/c;)V

    return-void
.end method

.method private static synthetic Z1(Li0/c$a;Ljava/util/List;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->P(Li0/c$a;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic a1(Li0/c$a;Lh0/e2;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->s2(Li0/c$a;Lh0/e2;Li0/c;)V

    return-void
.end method

.method private static synthetic a2(Li0/c$a;Ls1/e;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->a(Li0/c$a;Ls1/e;)V

    return-void
.end method

.method public static synthetic b1(Li0/c$a;IJLi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Li0/p1;->k2(Li0/c$a;IJLi0/c;)V

    return-void
.end method

.method private static synthetic b2(Li0/c$a;Lh0/o;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->w(Li0/c$a;Lh0/o;)V

    return-void
.end method

.method public static synthetic c1(Li0/c$a;Ljava/lang/Exception;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->V1(Li0/c$a;Ljava/lang/Exception;Li0/c;)V

    return-void
.end method

.method private static synthetic c2(Li0/c$a;IZLi0/c;)V
    .locals 0

    invoke-interface {p3, p0, p1, p2}, Li0/c;->r0(Li0/c$a;IZ)V

    return-void
.end method

.method public static synthetic d1(Li0/c$a;Ljava/lang/String;JJLi0/c;)V
    .locals 0

    invoke-static/range {p0 .. p6}, Li0/p1;->P1(Li0/c$a;Ljava/lang/String;JJLi0/c;)V

    return-void
.end method

.method private static synthetic d2(Li0/c$a;Lj1/t;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->e0(Li0/c$a;Lj1/t;)V

    return-void
.end method

.method public static synthetic e1(Li0/c$a;Lh0/g3$b;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->X1(Li0/c$a;Lh0/g3$b;Li0/c;)V

    return-void
.end method

.method private static synthetic e2(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-interface {p1, p0}, Li0/c;->s(Li0/c$a;)V

    return-void
.end method

.method public static synthetic f1(Li0/c$a;Ljava/lang/String;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->N2(Li0/c$a;Ljava/lang/String;Li0/c;)V

    return-void
.end method

.method private static synthetic f2(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-interface {p1, p0}, Li0/c;->g0(Li0/c$a;)V

    return-void
.end method

.method public static synthetic g1(Li0/c$a;IJJLi0/c;)V
    .locals 0

    invoke-static/range {p0 .. p6}, Li0/p1;->W1(Li0/c$a;IJJLi0/c;)V

    return-void
.end method

.method private static synthetic g2(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-interface {p1, p0}, Li0/c;->c0(Li0/c$a;)V

    return-void
.end method

.method public static synthetic h1(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-static {p0, p1}, Li0/p1;->f2(Li0/c$a;Li0/c;)V

    return-void
.end method

.method private static synthetic h2(Li0/c$a;ILi0/c;)V
    .locals 0

    invoke-interface {p2, p0}, Li0/c;->G(Li0/c$a;)V

    invoke-interface {p2, p0, p1}, Li0/c;->O(Li0/c$a;I)V

    return-void
.end method

.method public static synthetic i1(Li0/c$a;ZILi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Li0/p1;->u2(Li0/c$a;ZILi0/c;)V

    return-void
.end method

.method private static synthetic i2(Li0/c$a;Ljava/lang/Exception;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->V(Li0/c$a;Ljava/lang/Exception;)V

    return-void
.end method

.method public static synthetic j1(Li0/c$a;ILi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->w2(Li0/c$a;ILi0/c;)V

    return-void
.end method

.method private static synthetic j2(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-interface {p1, p0}, Li0/c;->C(Li0/c$a;)V

    return-void
.end method

.method public static synthetic k1(Li0/c$a;Lh0/h4;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->J2(Li0/c$a;Lh0/h4;Li0/c;)V

    return-void
.end method

.method private static synthetic k2(Li0/c$a;IJLi0/c;)V
    .locals 0

    invoke-interface {p4, p0, p1, p2, p3}, Li0/c;->a0(Li0/c$a;IJ)V

    return-void
.end method

.method public static synthetic l1(Li0/c$a;Lk0/e;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->R1(Li0/c$a;Lk0/e;Li0/c;)V

    return-void
.end method

.method private static synthetic l2(Li0/c$a;ZLi0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->v0(Li0/c$a;Z)V

    invoke-interface {p2, p0, p1}, Li0/c;->d(Li0/c$a;Z)V

    return-void
.end method

.method public static synthetic m1(Li0/c$a;ILi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->x2(Li0/c$a;ILi0/c;)V

    return-void
.end method

.method private static synthetic m2(Li0/c$a;ZLi0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->W(Li0/c$a;Z)V

    return-void
.end method

.method public static synthetic n1(Li0/c$a;ZILi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Li0/p1;->A2(Li0/c$a;ZILi0/c;)V

    return-void
.end method

.method private static synthetic n2(Li0/c$a;Lj1/q;Lj1/t;Li0/c;)V
    .locals 0

    invoke-interface {p3, p0, p1, p2}, Li0/c;->Y(Li0/c$a;Lj1/q;Lj1/t;)V

    return-void
.end method

.method public static synthetic o1(Li0/c$a;Lj1/q;Lj1/t;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Li0/p1;->q2(Li0/c$a;Lj1/q;Lj1/t;Li0/c;)V

    return-void
.end method

.method private static synthetic o2(Li0/c$a;Lj1/q;Lj1/t;Li0/c;)V
    .locals 0

    invoke-interface {p3, p0, p1, p2}, Li0/c;->o0(Li0/c$a;Lj1/q;Lj1/t;)V

    return-void
.end method

.method public static synthetic p1(Li0/c$a;Lk0/e;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->S1(Li0/c$a;Lk0/e;Li0/c;)V

    return-void
.end method

.method private static synthetic p2(Li0/c$a;Lj1/q;Lj1/t;Ljava/io/IOException;ZLi0/c;)V
    .locals 6

    move-object v0, p5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Li0/c;->B(Li0/c$a;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V

    return-void
.end method

.method public static synthetic q1(Li0/c$a;ILi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->I2(Li0/c$a;ILi0/c;)V

    return-void
.end method

.method private static synthetic q2(Li0/c$a;Lj1/q;Lj1/t;Li0/c;)V
    .locals 0

    invoke-interface {p3, p0, p1, p2}, Li0/c;->w0(Li0/c$a;Lj1/q;Lj1/t;)V

    return-void
.end method

.method public static synthetic r0(Li0/c$a;Ljava/lang/Exception;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->O1(Li0/c$a;Ljava/lang/Exception;Li0/c;)V

    return-void
.end method

.method public static synthetic r1(Li0/c$a;ZLi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->l2(Li0/c$a;ZLi0/c;)V

    return-void
.end method

.method private static synthetic r2(Li0/c$a;Lh0/z1;ILi0/c;)V
    .locals 0

    invoke-interface {p3, p0, p1, p2}, Li0/c;->c(Li0/c$a;Lh0/z1;I)V

    return-void
.end method

.method public static synthetic s0(Li0/c$a;IZLi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Li0/p1;->c2(Li0/c$a;IZLi0/c;)V

    return-void
.end method

.method public static synthetic s1(Li0/p1;Lh0/g3;Li0/c;Le2/l;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Li0/p1;->V2(Lh0/g3;Li0/c;Le2/l;)V

    return-void
.end method

.method private static synthetic s2(Li0/c$a;Lh0/e2;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->L(Li0/c$a;Lh0/e2;)V

    return-void
.end method

.method public static synthetic t0(Li0/c$a;ZLi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->m2(Li0/c$a;ZLi0/c;)V

    return-void
.end method

.method public static synthetic t1(Li0/c;Le2/l;)V
    .locals 0

    invoke-static {p0, p1}, Li0/p1;->L1(Li0/c;Le2/l;)V

    return-void
.end method

.method private static synthetic t2(Li0/c$a;Lz0/a;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->H(Li0/c$a;Lz0/a;)V

    return-void
.end method

.method public static synthetic u0(Li0/c$a;Lh0/r1;Lk0/i;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Li0/p1;->R2(Li0/c$a;Lh0/r1;Lk0/i;Li0/c;)V

    return-void
.end method

.method public static synthetic u1(Li0/c$a;Li0/c;)V
    .locals 0

    invoke-static {p0, p1}, Li0/p1;->g2(Li0/c$a;Li0/c;)V

    return-void
.end method

.method private static synthetic u2(Li0/c$a;ZILi0/c;)V
    .locals 0

    invoke-interface {p3, p0, p1, p2}, Li0/c;->T(Li0/c$a;ZI)V

    return-void
.end method

.method public static synthetic v0(Li0/p1;)V
    .locals 0

    invoke-direct {p0}, Li0/p1;->W2()V

    return-void
.end method

.method public static synthetic v1(Li0/c$a;JLi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Li0/p1;->U1(Li0/c$a;JLi0/c;)V

    return-void
.end method

.method private static synthetic v2(Li0/c$a;Lh0/f3;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->z0(Li0/c$a;Lh0/f3;)V

    return-void
.end method

.method public static synthetic w0(Li0/c$a;Lf2/z;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->S2(Li0/c$a;Lf2/z;Li0/c;)V

    return-void
.end method

.method public static synthetic w1(Li0/c$a;Lk0/e;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->P2(Li0/c$a;Lk0/e;Li0/c;)V

    return-void
.end method

.method private static synthetic w2(Li0/c$a;ILi0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->U(Li0/c$a;I)V

    return-void
.end method

.method public static synthetic x0(Li0/c$a;ILi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->D2(Li0/c$a;ILi0/c;)V

    return-void
.end method

.method public static synthetic x1(Li0/c$a;Ljava/lang/String;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->Q1(Li0/c$a;Ljava/lang/String;Li0/c;)V

    return-void
.end method

.method private static synthetic x2(Li0/c$a;ILi0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->k(Li0/c$a;I)V

    return-void
.end method

.method public static synthetic y0(Li0/c$a;Lj1/t;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->d2(Li0/c$a;Lj1/t;Li0/c;)V

    return-void
.end method

.method public static synthetic y1(Li0/c$a;Ljava/lang/Object;JLi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Li0/p1;->C2(Li0/c$a;Ljava/lang/Object;JLi0/c;)V

    return-void
.end method

.method private static synthetic y2(Li0/c$a;Lh0/c3;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->n(Li0/c$a;Lh0/c3;)V

    return-void
.end method

.method public static synthetic z0(Li0/c$a;JILi0/c;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Li0/p1;->Q2(Li0/c$a;JILi0/c;)V

    return-void
.end method

.method public static synthetic z1(Li0/c$a;Ljava/lang/Exception;Li0/c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Li0/p1;->i2(Li0/c$a;Ljava/lang/Exception;Li0/c;)V

    return-void
.end method

.method private static synthetic z2(Li0/c$a;Lh0/c3;Li0/c;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Li0/c;->t0(Li0/c$a;Lh0/c3;)V

    return-void
.end method


# virtual methods
.method public final A(I)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/f;

    invoke-direct {v1, v0, p1}, Li0/f;-><init>(Li0/c$a;I)V

    const/4 p1, 0x6

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final B(ZI)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/h1;

    invoke-direct {v1, v0, p1, p2}, Li0/h1;-><init>(Li0/c$a;ZI)V

    const/4 p1, -0x1

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final C(ILj1/x$b;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Li0/p1;->H1(ILj1/x$b;)Li0/c$a;

    move-result-object p1

    new-instance p2, Li0/o1;

    invoke-direct {p2, p1, p3}, Li0/o1;-><init>(Li0/c$a;I)V

    const/16 p3, 0x3fe

    invoke-virtual {p0, p1, p3, p2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public D(Z)V
    .locals 0

    return-void
.end method

.method protected final D1()Li0/c$a;
    .locals 1

    iget-object v0, p0, Li0/p1;->i:Li0/p1$a;

    invoke-virtual {v0}, Li0/p1$a;->d()Lj1/x$b;

    move-result-object v0

    invoke-direct {p0, v0}, Li0/p1;->F1(Lj1/x$b;)Li0/c$a;

    move-result-object v0

    return-object v0
.end method

.method public E(I)V
    .locals 0

    return-void
.end method

.method protected final E1(Lh0/c4;ILj1/x$b;)Li0/c$a;
    .locals 17
    .annotation runtime Lorg/checkerframework/checker/nullness/qual/RequiresNonNull;
        value = {
            "player"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    invoke-virtual/range {p1 .. p1}, Lh0/c4;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    move-object v6, v1

    goto :goto_0

    :cond_0
    move-object/from16 v6, p3

    :goto_0
    iget-object v1, v0, Li0/p1;->f:Le2/d;

    invoke-interface {v1}, Le2/d;->d()J

    move-result-wide v2

    iget-object v1, v0, Li0/p1;->l:Lh0/g3;

    invoke-interface {v1}, Lh0/g3;->N()Lh0/c4;

    move-result-object v1

    invoke-virtual {v4, v1}, Lh0/c4;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz v1, :cond_1

    iget-object v1, v0, Li0/p1;->l:Lh0/g3;

    invoke-interface {v1}, Lh0/g3;->E()I

    move-result v1

    if-ne v5, v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    const-wide/16 v9, 0x0

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lj1/v;->b()Z

    move-result v11

    if-eqz v11, :cond_3

    if-eqz v1, :cond_2

    iget-object v1, v0, Li0/p1;->l:Lh0/g3;

    invoke-interface {v1}, Lh0/g3;->D()I

    move-result v1

    iget v11, v6, Lj1/v;->b:I

    if-ne v1, v11, :cond_2

    iget-object v1, v0, Li0/p1;->l:Lh0/g3;

    invoke-interface {v1}, Lh0/g3;->I()I

    move-result v1

    iget v11, v6, Lj1/v;->c:I

    if-ne v1, v11, :cond_2

    goto :goto_2

    :cond_2
    const/4 v7, 0x0

    :goto_2
    if-eqz v7, :cond_6

    iget-object v1, v0, Li0/p1;->l:Lh0/g3;

    invoke-interface {v1}, Lh0/g3;->S()J

    move-result-wide v9

    goto :goto_3

    :cond_3
    if-eqz v1, :cond_4

    iget-object v1, v0, Li0/p1;->l:Lh0/g3;

    invoke-interface {v1}, Lh0/g3;->n()J

    move-result-wide v7

    goto :goto_4

    :cond_4
    invoke-virtual/range {p1 .. p1}, Lh0/c4;->u()Z

    move-result v1

    if-eqz v1, :cond_5

    goto :goto_3

    :cond_5
    iget-object v1, v0, Li0/p1;->h:Lh0/c4$d;

    invoke-virtual {v4, v5, v1}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    move-result-object v1

    invoke-virtual {v1}, Lh0/c4$d;->d()J

    move-result-wide v9

    :cond_6
    :goto_3
    move-wide v7, v9

    :goto_4
    iget-object v1, v0, Li0/p1;->i:Li0/p1$a;

    invoke-virtual {v1}, Li0/p1$a;->d()Lj1/x$b;

    move-result-object v11

    new-instance v16, Li0/c$a;

    iget-object v1, v0, Li0/p1;->l:Lh0/g3;

    invoke-interface {v1}, Lh0/g3;->N()Lh0/c4;

    move-result-object v9

    iget-object v1, v0, Li0/p1;->l:Lh0/g3;

    invoke-interface {v1}, Lh0/g3;->E()I

    move-result v10

    iget-object v1, v0, Li0/p1;->l:Lh0/g3;

    invoke-interface {v1}, Lh0/g3;->S()J

    move-result-wide v12

    iget-object v1, v0, Li0/p1;->l:Lh0/g3;

    invoke-interface {v1}, Lh0/g3;->p()J

    move-result-wide v14

    move-object/from16 v1, v16

    move-object/from16 v4, p1

    move/from16 v5, p2

    invoke-direct/range {v1 .. v15}, Li0/c$a;-><init>(JLh0/c4;ILj1/x$b;JLh0/c4;ILj1/x$b;JJ)V

    return-object v16
.end method

.method public final F(ILj1/x$b;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Li0/p1;->H1(ILj1/x$b;)Li0/c$a;

    move-result-object p1

    new-instance p2, Li0/g1;

    invoke-direct {p2, p1}, Li0/g1;-><init>(Li0/c$a;)V

    const/16 v0, 0x401

    invoke-virtual {p0, p1, v0, p2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public G(Lh0/g3;Landroid/os/Looper;)V
    .locals 2

    iget-object v0, p0, Li0/p1;->l:Lh0/g3;

    if-eqz v0, :cond_1

    iget-object v0, p0, Li0/p1;->i:Li0/p1$a;

    invoke-static {v0}, Li0/p1$a;->a(Li0/p1$a;)Lp2/q;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Le2/a;->f(Z)V

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/g3;

    iput-object v0, p0, Li0/p1;->l:Lh0/g3;

    iget-object v0, p0, Li0/p1;->f:Le2/d;

    const/4 v1, 0x0

    invoke-interface {v0, p2, v1}, Le2/d;->b(Landroid/os/Looper;Landroid/os/Handler$Callback;)Le2/n;

    move-result-object v0

    iput-object v0, p0, Li0/p1;->m:Le2/n;

    iget-object v0, p0, Li0/p1;->k:Le2/q;

    new-instance v1, Li0/i1;

    invoke-direct {v1, p0, p1}, Li0/i1;-><init>(Li0/p1;Lh0/g3;)V

    invoke-virtual {v0, p2, v1}, Le2/q;->e(Landroid/os/Looper;Le2/q$b;)Le2/q;

    move-result-object p1

    iput-object p1, p0, Li0/p1;->k:Le2/q;

    return-void
.end method

.method public final H(ILj1/x$b;Lj1/q;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Li0/p1;->H1(ILj1/x$b;)Li0/c$a;

    move-result-object p1

    new-instance p2, Li0/e0;

    invoke-direct {p2, p1, p3, p4}, Li0/e0;-><init>(Li0/c$a;Lj1/q;Lj1/t;)V

    const/16 p3, 0x3ea

    invoke-virtual {p0, p1, p3, p2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final I(ILj1/x$b;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Li0/p1;->H1(ILj1/x$b;)Li0/c$a;

    move-result-object p1

    new-instance p2, Li0/h0;

    invoke-direct {p2, p1, p3}, Li0/h0;-><init>(Li0/c$a;Lj1/t;)V

    const/16 p3, 0x3ec

    invoke-virtual {p0, p1, p3, p2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public J(Lh0/c3;)V
    .locals 2

    invoke-direct {p0, p1}, Li0/p1;->K1(Lh0/c3;)Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/x;

    invoke-direct {v1, v0, p1}, Li0/x;-><init>(Li0/c$a;Lh0/c3;)V

    const/16 p1, 0xa

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final K(ILj1/x$b;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Li0/p1;->H1(ILj1/x$b;)Li0/c$a;

    move-result-object p1

    new-instance p2, Li0/i0;

    invoke-direct {p2, p1, p3}, Li0/i0;-><init>(Li0/c$a;Lj1/t;)V

    const/16 p3, 0x3ed

    invoke-virtual {p0, p1, p3, p2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final L(ILj1/x$b;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Li0/p1;->H1(ILj1/x$b;)Li0/c$a;

    move-result-object p1

    new-instance p2, Li0/d;

    invoke-direct {p2, p1}, Li0/d;-><init>(Li0/c$a;)V

    const/16 v0, 0x403

    invoke-virtual {p0, p1, v0, p2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final M(Z)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/e1;

    invoke-direct {v1, v0, p1}, Li0/e1;-><init>(Li0/c$a;Z)V

    const/4 p1, 0x3

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final N(Lh0/g3$e;Lh0/g3$e;I)V
    .locals 3

    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Li0/p1;->n:Z

    :cond_0
    iget-object v0, p0, Li0/p1;->i:Li0/p1$a;

    iget-object v1, p0, Li0/p1;->l:Lh0/g3;

    invoke-static {v1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lh0/g3;

    invoke-virtual {v0, v1}, Li0/p1$a;->j(Lh0/g3;)V

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    const/16 v1, 0xb

    new-instance v2, Li0/l;

    invoke-direct {v2, v0, p3, p1, p2}, Li0/l;-><init>(Li0/c$a;ILh0/g3$e;Lh0/g3$e;)V

    invoke-virtual {p0, v0, v1, v2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final O(Lh0/c4;I)V
    .locals 1

    iget-object p1, p0, Li0/p1;->i:Li0/p1$a;

    iget-object v0, p0, Li0/p1;->l:Lh0/g3;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/g3;

    invoke-virtual {p1, v0}, Li0/p1$a;->l(Lh0/g3;)V

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object p1

    new-instance v0, Li0/g;

    invoke-direct {v0, p1, p2}, Li0/g;-><init>(Li0/c$a;I)V

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2, v0}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public P()V
    .locals 0

    return-void
.end method

.method public final Q(ILj1/x$b;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V
    .locals 6

    invoke-direct {p0, p1, p2}, Li0/p1;->H1(ILj1/x$b;)Li0/c$a;

    move-result-object p1

    new-instance p2, Li0/g0;

    move-object v0, p2

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Li0/g0;-><init>(Li0/c$a;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V

    const/16 p3, 0x3eb

    invoke-virtual {p0, p1, p3, p2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final R()V
    .locals 3

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/k0;

    invoke-direct {v1, v0}, Li0/k0;-><init>(Li0/c$a;)V

    const/4 v2, -0x1

    invoke-virtual {p0, v0, v2, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final S(Ljava/util/List;Lj1/x$b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lj1/x$b;",
            ">;",
            "Lj1/x$b;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Li0/p1;->i:Li0/p1$a;

    iget-object v1, p0, Li0/p1;->l:Lh0/g3;

    invoke-static {v1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lh0/g3;

    invoke-virtual {v0, p1, p2, v1}, Li0/p1$a;->k(Ljava/util/List;Lj1/x$b;Lh0/g3;)V

    return-void
.end method

.method public T(Lh0/o;)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/r;

    invoke-direct {v1, v0, p1}, Li0/r;-><init>(Li0/c$a;Lh0/o;)V

    const/16 p1, 0x1d

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final U(ILj1/x$b;Lj1/q;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Li0/p1;->H1(ILj1/x$b;)Li0/c$a;

    move-result-object p1

    new-instance p2, Li0/f0;

    invoke-direct {p2, p1, p3, p4}, Li0/f0;-><init>(Li0/c$a;Lj1/q;Lj1/t;)V

    const/16 p3, 0x3e8

    invoke-virtual {p0, p1, p3, p2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final V(Lh0/z1;I)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/u;

    invoke-direct {v1, v0, p1, p2}, Li0/u;-><init>(Li0/c$a;Lh0/z1;I)V

    const/4 p1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final W(F)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/m1;

    invoke-direct {v1, v0, p1}, Li0/m1;-><init>(Li0/c$a;F)V

    const/16 p1, 0x16

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final X(Lh0/c3;)V
    .locals 2

    invoke-direct {p0, p1}, Li0/p1;->K1(Lh0/c3;)Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/w;

    invoke-direct {v1, v0, p1}, Li0/w;-><init>(Li0/c$a;Lh0/c3;)V

    const/16 p1, 0xa

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method protected final X2(Li0/c$a;ILe2/q$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li0/c$a;",
            "I",
            "Le2/q$a<",
            "Li0/c;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Li0/p1;->j:Landroid/util/SparseArray;

    invoke-virtual {v0, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object p1, p0, Li0/p1;->k:Le2/q;

    invoke-virtual {p1, p2, p3}, Le2/q;->k(ILe2/q$a;)V

    return-void
.end method

.method public final Y(I)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/e;

    invoke-direct {v1, v0, p1}, Li0/e;-><init>(Li0/c$a;I)V

    const/4 p1, 0x4

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final Z(ZI)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/f1;

    invoke-direct {v1, v0, p1, p2}, Li0/f1;-><init>(Li0/c$a;ZI)V

    const/4 p1, 0x5

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, Li0/p1;->m:Le2/n;

    invoke-static {v0}, Le2/a;->h(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le2/n;

    new-instance v1, Li0/k1;

    invoke-direct {v1, p0}, Li0/k1;-><init>(Li0/p1;)V

    invoke-interface {v0, v1}, Le2/n;->k(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a0(Lj0/e;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/c0;

    invoke-direct {v1, v0, p1}, Li0/c0;-><init>(Li0/c$a;Lj0/e;)V

    const/16 p1, 0x14

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final b(Z)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/d1;

    invoke-direct {v1, v0, p1}, Li0/d1;-><init>(Li0/c$a;Z)V

    const/16 p1, 0x17

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final b0(IJJ)V
    .locals 9

    invoke-direct {p0}, Li0/p1;->G1()Li0/c$a;

    move-result-object v7

    new-instance v8, Li0/j;

    move-object v0, v8

    move-object v1, v7

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-direct/range {v0 .. v6}, Li0/j;-><init>(Li0/c$a;IJJ)V

    const/16 p1, 0x3ee

    invoke-virtual {p0, v7, p1, v8}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final c(Ljava/lang/Exception;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/m0;

    invoke-direct {v1, v0, p1}, Li0/m0;-><init>(Li0/c$a;Ljava/lang/Exception;)V

    const/16 p1, 0x3f6

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public c0(Li0/c;)V
    .locals 1

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Li0/p1;->k:Le2/q;

    invoke-virtual {v0, p1}, Le2/q;->c(Ljava/lang/Object;)V

    return-void
.end method

.method public final d(Lk0/e;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->I1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/w0;

    invoke-direct {v1, v0, p1}, Li0/w0;-><init>(Li0/c$a;Lk0/e;)V

    const/16 p1, 0x3f5

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final d0(ILj1/x$b;Lj1/q;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Li0/p1;->H1(ILj1/x$b;)Li0/c$a;

    move-result-object p1

    new-instance p2, Li0/d0;

    invoke-direct {p2, p1, p3, p4}, Li0/d0;-><init>(Li0/c$a;Lj1/q;Lj1/t;)V

    const/16 p3, 0x3e9

    invoke-virtual {p0, p1, p3, p2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/p0;

    invoke-direct {v1, v0, p1}, Li0/p0;-><init>(Li0/c$a;Ljava/lang/String;)V

    const/16 p1, 0x3fb

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final e0(ILj1/x$b;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Li0/p1;->H1(ILj1/x$b;)Li0/c$a;

    move-result-object p1

    new-instance p2, Li0/v0;

    invoke-direct {p2, p1}, Li0/v0;-><init>(Li0/c$a;)V

    const/16 v0, 0x402

    invoke-virtual {p0, p1, v0, p2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final f(Ljava/lang/Object;J)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/o0;

    invoke-direct {v1, v0, p1, p2, p3}, Li0/o0;-><init>(Li0/c$a;Ljava/lang/Object;J)V

    const/16 p1, 0x1a

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final f0()V
    .locals 3

    iget-boolean v0, p0, Li0/p1;->n:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Li0/p1;->n:Z

    const/4 v1, -0x1

    new-instance v2, Li0/l1;

    invoke-direct {v2, v0}, Li0/l1;-><init>(Li0/c$a;)V

    invoke-virtual {p0, v0, v1, v2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    :cond_0
    return-void
.end method

.method public final g(Ljava/lang/String;JJ)V
    .locals 9

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v7

    new-instance v8, Li0/s0;

    move-object v0, v8

    move-object v1, v7

    move-object v2, p1

    move-wide v3, p4

    move-wide v5, p2

    invoke-direct/range {v0 .. v6}, Li0/s0;-><init>(Li0/c$a;Ljava/lang/String;JJ)V

    const/16 p1, 0x3f8

    invoke-virtual {p0, v7, p1, v8}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public synthetic g0(ILj1/x$b;)V
    .locals 0

    invoke-static {p0, p1, p2}, Ll0/p;->a(Ll0/w;ILj1/x$b;)V

    return-void
.end method

.method public final h(Lh0/f3;)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/y;

    invoke-direct {v1, v0, p1}, Li0/y;-><init>(Li0/c$a;Lh0/f3;)V

    const/16 p1, 0xc

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final h0(ILj1/x$b;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Li0/p1;->H1(ILj1/x$b;)Li0/c$a;

    move-result-object p1

    new-instance p2, Li0/z;

    invoke-direct {p2, p1}, Li0/z;-><init>(Li0/c$a;)V

    const/16 v0, 0x3ff

    invoke-virtual {p0, p1, v0, p2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final i(I)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/n1;

    invoke-direct {v1, v0, p1}, Li0/n1;-><init>(Li0/c$a;I)V

    const/16 p1, 0x8

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final i0(Z)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/c1;

    invoke-direct {v1, v0, p1}, Li0/c1;-><init>(Li0/c$a;Z)V

    const/16 p1, 0x9

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public j(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ls1/b;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/t0;

    invoke-direct {v1, v0, p1}, Li0/t0;-><init>(Li0/c$a;Ljava/util/List;)V

    const/16 p1, 0x1b

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public j0(Lh0/g3$b;)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/a0;

    invoke-direct {v1, v0, p1}, Li0/a0;-><init>(Li0/c$a;Lh0/g3$b;)V

    const/16 p1, 0xd

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final k(Lh0/r1;Lk0/i;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/s;

    invoke-direct {v1, v0, p1, p2}, Li0/s;-><init>(Li0/c$a;Lh0/r1;Lk0/i;)V

    const/16 p1, 0x3f9

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final k0(II)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/h;

    invoke-direct {v1, v0, p1, p2}, Li0/h;-><init>(Li0/c$a;II)V

    const/16 p1, 0x18

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final l(Lf2/z;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/q;

    invoke-direct {v1, v0, p1}, Li0/q;-><init>(Li0/c$a;Lf2/z;)V

    const/16 p1, 0x19

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public l0(Lh0/e2;)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/v;

    invoke-direct {v1, v0, p1}, Li0/v;-><init>(Li0/c$a;Lh0/e2;)V

    const/16 p1, 0xe

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final m(J)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/n;

    invoke-direct {v1, v0, p1, p2}, Li0/n;-><init>(Li0/c$a;J)V

    const/16 p1, 0x3f2

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final m0(ILj1/x$b;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Li0/p1;->H1(ILj1/x$b;)Li0/c$a;

    move-result-object p1

    new-instance p2, Li0/n0;

    invoke-direct {p2, p1, p3}, Li0/n0;-><init>(Li0/c$a;Ljava/lang/Exception;)V

    const/16 p3, 0x400

    invoke-virtual {p0, p1, p3, p2}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final n(Lk0/e;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/y0;

    invoke-direct {v1, v0, p1}, Li0/y0;-><init>(Li0/c$a;Lk0/e;)V

    const/16 p1, 0x3f7

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public n0(Lh0/h4;)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/b0;

    invoke-direct {v1, v0, p1}, Li0/b0;-><init>(Li0/c$a;Lh0/h4;)V

    const/4 p1, 0x2

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final o(Lk0/e;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->I1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/u0;

    invoke-direct {v1, v0, p1}, Li0/u0;-><init>(Li0/c$a;Lk0/e;)V

    const/16 p1, 0x3fc

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public o0(Lh0/g3;Lh0/g3$c;)V
    .locals 0

    return-void
.end method

.method public final p(Ljava/lang/Exception;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/j0;

    invoke-direct {v1, v0, p1}, Li0/j0;-><init>(Li0/c$a;Ljava/lang/Exception;)V

    const/16 p1, 0x405

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public p0(IZ)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/m;

    invoke-direct {v1, v0, p1, p2}, Li0/m;-><init>(Li0/c$a;IZ)V

    const/16 p1, 0x1e

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final q(Ljava/lang/Exception;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/l0;

    invoke-direct {v1, v0, p1}, Li0/l0;-><init>(Li0/c$a;Ljava/lang/Exception;)V

    const/16 p1, 0x406

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public q0(Z)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/b1;

    invoke-direct {v1, v0, p1}, Li0/b1;-><init>(Li0/c$a;Z)V

    const/4 p1, 0x7

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final r(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/q0;

    invoke-direct {v1, v0, p1}, Li0/q0;-><init>(Li0/c$a;Ljava/lang/String;)V

    const/16 p1, 0x3f4

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final s(Ljava/lang/String;JJ)V
    .locals 9

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v7

    new-instance v8, Li0/r0;

    move-object v0, v8

    move-object v1, v7

    move-object v2, p1

    move-wide v3, p4

    move-wide v5, p2

    invoke-direct/range {v0 .. v6}, Li0/r0;-><init>(Li0/c$a;Ljava/lang/String;JJ)V

    const/16 p1, 0x3f0

    invoke-virtual {p0, v7, p1, v8}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final t(Lh0/r1;Lk0/i;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/t;

    invoke-direct {v1, v0, p1, p2}, Li0/t;-><init>(Li0/c$a;Lh0/r1;Lk0/i;)V

    const/16 p1, 0x3f1

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final u(IJJ)V
    .locals 9

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v7

    new-instance v8, Li0/k;

    move-object v0, v8

    move-object v1, v7

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-direct/range {v0 .. v6}, Li0/k;-><init>(Li0/c$a;IJJ)V

    const/16 p1, 0x3f3

    invoke-virtual {p0, v7, p1, v8}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final v(IJ)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->I1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/i;

    invoke-direct {v1, v0, p1, p2, p3}, Li0/i;-><init>(Li0/c$a;IJ)V

    const/16 p1, 0x3fa

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final w(Lz0/a;)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/a1;

    invoke-direct {v1, v0, p1}, Li0/a1;-><init>(Li0/c$a;Lz0/a;)V

    const/16 p1, 0x1c

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public x(Ls1/e;)V
    .locals 2

    invoke-virtual {p0}, Li0/p1;->D1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/z0;

    invoke-direct {v1, v0, p1}, Li0/z0;-><init>(Li0/c$a;Ls1/e;)V

    const/16 p1, 0x1b

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final y(JI)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->I1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/p;

    invoke-direct {v1, v0, p1, p2, p3}, Li0/p;-><init>(Li0/c$a;JI)V

    const/16 p1, 0x3fd

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method

.method public final z(Lk0/e;)V
    .locals 2

    invoke-direct {p0}, Li0/p1;->J1()Li0/c$a;

    move-result-object v0

    new-instance v1, Li0/x0;

    invoke-direct {v1, v0, p1}, Li0/x0;-><init>(Li0/c$a;Lk0/e;)V

    const/16 p1, 0x3ef

    invoke-virtual {p0, v0, p1, v1}, Li0/p1;->X2(Li0/c$a;ILe2/q$a;)V

    return-void
.end method
