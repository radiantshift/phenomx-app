.class public interface abstract Li0/c;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Li0/c$a;,
        Li0/c$b;
    }
.end annotation


# virtual methods
.method public abstract A(Li0/c$a;Lf2/z;)V
.end method

.method public abstract B(Li0/c$a;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V
.end method

.method public abstract C(Li0/c$a;)V
.end method

.method public abstract D(Li0/c$a;Ljava/lang/String;JJ)V
.end method

.method public abstract E(Li0/c$a;Lk0/e;)V
.end method

.method public abstract F(Li0/c$a;F)V
.end method

.method public abstract G(Li0/c$a;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract H(Li0/c$a;Lz0/a;)V
.end method

.method public abstract I(Li0/c$a;Lh0/g3$e;Lh0/g3$e;I)V
.end method

.method public abstract J(Li0/c$a;Z)V
.end method

.method public abstract K(Li0/c$a;Ljava/lang/Exception;)V
.end method

.method public abstract L(Li0/c$a;Lh0/e2;)V
.end method

.method public abstract M(Li0/c$a;Ljava/lang/String;)V
.end method

.method public abstract N(Li0/c$a;Z)V
.end method

.method public abstract O(Li0/c$a;I)V
.end method

.method public abstract P(Li0/c$a;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li0/c$a;",
            "Ljava/util/List<",
            "Ls1/b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract Q(Li0/c$a;II)V
.end method

.method public abstract R(Li0/c$a;Lh0/h4;)V
.end method

.method public abstract S(Li0/c$a;Lh0/r1;Lk0/i;)V
.end method

.method public abstract T(Li0/c$a;ZI)V
.end method

.method public abstract U(Li0/c$a;I)V
.end method

.method public abstract V(Li0/c$a;Ljava/lang/Exception;)V
.end method

.method public abstract W(Li0/c$a;Z)V
.end method

.method public abstract X(Li0/c$a;Ljava/lang/Exception;)V
.end method

.method public abstract Y(Li0/c$a;Lj1/q;Lj1/t;)V
.end method

.method public abstract Z(Li0/c$a;IJJ)V
.end method

.method public abstract a(Li0/c$a;Ls1/e;)V
.end method

.method public abstract a0(Li0/c$a;IJ)V
.end method

.method public abstract b(Li0/c$a;Ljava/lang/String;J)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract b0(Li0/c$a;IIIF)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract c(Li0/c$a;Lh0/z1;I)V
.end method

.method public abstract c0(Li0/c$a;)V
.end method

.method public abstract d(Li0/c$a;Z)V
.end method

.method public abstract d0(Li0/c$a;Lk0/e;)V
.end method

.method public abstract e(Li0/c$a;I)V
.end method

.method public abstract e0(Li0/c$a;Lj1/t;)V
.end method

.method public abstract f(Li0/c$a;J)V
.end method

.method public abstract f0(Li0/c$a;Lj1/t;)V
.end method

.method public abstract g(Li0/c$a;ZI)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract g0(Li0/c$a;)V
.end method

.method public abstract h(Li0/c$a;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract h0(Li0/c$a;Lj0/e;)V
.end method

.method public abstract i(Li0/c$a;ILh0/r1;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract i0(Li0/c$a;Lk0/e;)V
.end method

.method public abstract j(Li0/c$a;ILk0/e;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract j0(Li0/c$a;ILjava/lang/String;J)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract k(Li0/c$a;I)V
.end method

.method public abstract k0(Li0/c$a;Lh0/r1;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract l(Li0/c$a;ILk0/e;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract m(Li0/c$a;Lk0/e;)V
.end method

.method public abstract n(Li0/c$a;Lh0/c3;)V
.end method

.method public abstract n0(Li0/c$a;JI)V
.end method

.method public abstract o0(Li0/c$a;Lj1/q;Lj1/t;)V
.end method

.method public abstract p(Li0/c$a;Ljava/lang/Object;J)V
.end method

.method public abstract p0(Li0/c$a;I)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract q(Li0/c$a;Lh0/r1;Lk0/i;)V
.end method

.method public abstract q0(Li0/c$a;Ljava/lang/String;JJ)V
.end method

.method public abstract r(Li0/c$a;IJJ)V
.end method

.method public abstract r0(Li0/c$a;IZ)V
.end method

.method public abstract s(Li0/c$a;)V
.end method

.method public abstract s0(Li0/c$a;Ljava/lang/Exception;)V
.end method

.method public abstract t(Li0/c$a;I)V
.end method

.method public abstract t0(Li0/c$a;Lh0/c3;)V
.end method

.method public abstract u(Li0/c$a;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract u0(Li0/c$a;Lh0/r1;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract v(Li0/c$a;Ljava/lang/String;)V
.end method

.method public abstract v0(Li0/c$a;Z)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract w(Li0/c$a;Lh0/o;)V
.end method

.method public abstract w0(Li0/c$a;Lj1/q;Lj1/t;)V
.end method

.method public abstract x(Li0/c$a;)V
.end method

.method public abstract y(Li0/c$a;Ljava/lang/String;J)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract y0(Lh0/g3;Li0/c$b;)V
.end method

.method public abstract z(Li0/c$a;Lh0/g3$b;)V
.end method

.method public abstract z0(Li0/c$a;Lh0/f3;)V
.end method
