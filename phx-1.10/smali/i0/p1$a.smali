.class final Li0/p1$a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Li0/p1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Lh0/c4$b;

.field private b:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "Lj1/x$b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lp2/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/r<",
            "Lj1/x$b;",
            "Lh0/c4;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lj1/x$b;

.field private e:Lj1/x$b;

.field private f:Lj1/x$b;


# direct methods
.method public constructor <init>(Lh0/c4$b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Li0/p1$a;->a:Lh0/c4$b;

    invoke-static {}, Lp2/q;->q()Lp2/q;

    move-result-object p1

    iput-object p1, p0, Li0/p1$a;->b:Lp2/q;

    invoke-static {}, Lp2/r;->j()Lp2/r;

    move-result-object p1

    iput-object p1, p0, Li0/p1$a;->c:Lp2/r;

    return-void
.end method

.method static synthetic a(Li0/p1$a;)Lp2/q;
    .locals 0

    iget-object p0, p0, Li0/p1$a;->b:Lp2/q;

    return-object p0
.end method

.method private b(Lp2/r$a;Lj1/x$b;Lh0/c4;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lp2/r$a<",
            "Lj1/x$b;",
            "Lh0/c4;",
            ">;",
            "Lj1/x$b;",
            "Lh0/c4;",
            ")V"
        }
    .end annotation

    if-nez p2, :cond_0

    return-void

    :cond_0
    iget-object v0, p2, Lj1/v;->a:Ljava/lang/Object;

    invoke-virtual {p3, v0}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    :goto_0
    invoke-virtual {p1, p2, p3}, Lp2/r$a;->d(Ljava/lang/Object;Ljava/lang/Object;)Lp2/r$a;

    goto :goto_1

    :cond_1
    iget-object p3, p0, Li0/p1$a;->c:Lp2/r;

    invoke-virtual {p3, p2}, Lp2/r;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lh0/c4;

    if-eqz p3, :cond_2

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method private static c(Lh0/g3;Lp2/q;Lj1/x$b;Lh0/c4$b;)Lj1/x$b;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lh0/g3;",
            "Lp2/q<",
            "Lj1/x$b;",
            ">;",
            "Lj1/x$b;",
            "Lh0/c4$b;",
            ")",
            "Lj1/x$b;"
        }
    .end annotation

    invoke-interface {p0}, Lh0/g3;->N()Lh0/c4;

    move-result-object v0

    invoke-interface {p0}, Lh0/g3;->C()I

    move-result v1

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    move-object v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v1}, Lh0/c4;->q(I)Ljava/lang/Object;

    move-result-object v2

    :goto_0
    invoke-interface {p0}, Lh0/g3;->m()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v1, p3}, Lh0/c4;->j(ILh0/c4$b;)Lh0/c4$b;

    move-result-object v0

    invoke-interface {p0}, Lh0/g3;->S()J

    move-result-wide v4

    invoke-static {v4, v5}, Le2/n0;->C0(J)J

    move-result-wide v4

    invoke-virtual {p3}, Lh0/c4$b;->q()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Lh0/c4$b;->g(J)I

    move-result p3

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p3, -0x1

    :goto_2
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p1}, Ljava/util/AbstractCollection;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/x$b;

    invoke-interface {p0}, Lh0/g3;->m()Z

    move-result v6

    invoke-interface {p0}, Lh0/g3;->D()I

    move-result v7

    invoke-interface {p0}, Lh0/g3;->I()I

    move-result v8

    move-object v4, v1

    move-object v5, v2

    move v9, p3

    invoke-static/range {v4 .. v9}, Li0/p1$a;->i(Lj1/x$b;Ljava/lang/Object;ZIII)Z

    move-result v4

    if-eqz v4, :cond_3

    return-object v1

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_5

    if-eqz p2, :cond_5

    invoke-interface {p0}, Lh0/g3;->m()Z

    move-result v6

    invoke-interface {p0}, Lh0/g3;->D()I

    move-result v7

    invoke-interface {p0}, Lh0/g3;->I()I

    move-result v8

    move-object v4, p2

    move-object v5, v2

    move v9, p3

    invoke-static/range {v4 .. v9}, Li0/p1$a;->i(Lj1/x$b;Ljava/lang/Object;ZIII)Z

    move-result p0

    if-eqz p0, :cond_5

    return-object p2

    :cond_5
    return-object v3
.end method

.method private static i(Lj1/x$b;Ljava/lang/Object;ZIII)Z
    .locals 1

    iget-object v0, p0, Lj1/v;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    if-eqz p2, :cond_1

    iget p1, p0, Lj1/v;->b:I

    if-ne p1, p3, :cond_1

    iget p1, p0, Lj1/v;->c:I

    if-eq p1, p4, :cond_2

    :cond_1
    if-nez p2, :cond_3

    iget p1, p0, Lj1/v;->b:I

    const/4 p2, -0x1

    if-ne p1, p2, :cond_3

    iget p0, p0, Lj1/v;->e:I

    if-ne p0, p5, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method private m(Lh0/c4;)V
    .locals 3

    invoke-static {}, Lp2/r;->a()Lp2/r$a;

    move-result-object v0

    iget-object v1, p0, Li0/p1$a;->b:Lp2/q;

    invoke-virtual {v1}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Li0/p1$a;->e:Lj1/x$b;

    invoke-direct {p0, v0, v1, p1}, Li0/p1$a;->b(Lp2/r$a;Lj1/x$b;Lh0/c4;)V

    iget-object v1, p0, Li0/p1$a;->f:Lj1/x$b;

    iget-object v2, p0, Li0/p1$a;->e:Lj1/x$b;

    invoke-static {v1, v2}, Lo2/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Li0/p1$a;->f:Lj1/x$b;

    invoke-direct {p0, v0, v1, p1}, Li0/p1$a;->b(Lp2/r$a;Lj1/x$b;Lh0/c4;)V

    :cond_0
    iget-object v1, p0, Li0/p1$a;->d:Lj1/x$b;

    iget-object v2, p0, Li0/p1$a;->e:Lj1/x$b;

    invoke-static {v1, v2}, Lo2/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Li0/p1$a;->d:Lj1/x$b;

    iget-object v2, p0, Li0/p1$a;->f:Lj1/x$b;

    invoke-static {v1, v2}, Lo2/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Li0/p1$a;->b:Lp2/q;

    invoke-virtual {v2}, Ljava/util/AbstractCollection;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Li0/p1$a;->b:Lp2/q;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lj1/x$b;

    invoke-direct {p0, v0, v2, p1}, Li0/p1$a;->b(Lp2/r$a;Lj1/x$b;Lh0/c4;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Li0/p1$a;->b:Lp2/q;

    iget-object v2, p0, Li0/p1$a;->d:Lj1/x$b;

    invoke-virtual {v1, v2}, Lp2/q;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :goto_1
    iget-object v1, p0, Li0/p1$a;->d:Lj1/x$b;

    invoke-direct {p0, v0, v1, p1}, Li0/p1$a;->b(Lp2/r$a;Lj1/x$b;Lh0/c4;)V

    :cond_3
    invoke-virtual {v0}, Lp2/r$a;->b()Lp2/r;

    move-result-object p1

    iput-object p1, p0, Li0/p1$a;->c:Lp2/r;

    return-void
.end method


# virtual methods
.method public d()Lj1/x$b;
    .locals 1

    iget-object v0, p0, Li0/p1$a;->d:Lj1/x$b;

    return-object v0
.end method

.method public e()Lj1/x$b;
    .locals 1

    iget-object v0, p0, Li0/p1$a;->b:Lp2/q;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Li0/p1$a;->b:Lp2/q;

    invoke-static {v0}, Lp2/t;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/x$b;

    :goto_0
    return-object v0
.end method

.method public f(Lj1/x$b;)Lh0/c4;
    .locals 1

    iget-object v0, p0, Li0/p1$a;->c:Lp2/r;

    invoke-virtual {v0, p1}, Lp2/r;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh0/c4;

    return-object p1
.end method

.method public g()Lj1/x$b;
    .locals 1

    iget-object v0, p0, Li0/p1$a;->e:Lj1/x$b;

    return-object v0
.end method

.method public h()Lj1/x$b;
    .locals 1

    iget-object v0, p0, Li0/p1$a;->f:Lj1/x$b;

    return-object v0
.end method

.method public j(Lh0/g3;)V
    .locals 3

    iget-object v0, p0, Li0/p1$a;->b:Lp2/q;

    iget-object v1, p0, Li0/p1$a;->e:Lj1/x$b;

    iget-object v2, p0, Li0/p1$a;->a:Lh0/c4$b;

    invoke-static {p1, v0, v1, v2}, Li0/p1$a;->c(Lh0/g3;Lp2/q;Lj1/x$b;Lh0/c4$b;)Lj1/x$b;

    move-result-object p1

    iput-object p1, p0, Li0/p1$a;->d:Lj1/x$b;

    return-void
.end method

.method public k(Ljava/util/List;Lj1/x$b;Lh0/g3;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lj1/x$b;",
            ">;",
            "Lj1/x$b;",
            "Lh0/g3;",
            ")V"
        }
    .end annotation

    invoke-static {p1}, Lp2/q;->m(Ljava/util/Collection;)Lp2/q;

    move-result-object v0

    iput-object v0, p0, Li0/p1$a;->b:Lp2/q;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/x$b;

    iput-object p1, p0, Li0/p1$a;->e:Lj1/x$b;

    invoke-static {p2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/x$b;

    iput-object p1, p0, Li0/p1$a;->f:Lj1/x$b;

    :cond_0
    iget-object p1, p0, Li0/p1$a;->d:Lj1/x$b;

    if-nez p1, :cond_1

    iget-object p1, p0, Li0/p1$a;->b:Lp2/q;

    iget-object p2, p0, Li0/p1$a;->e:Lj1/x$b;

    iget-object v0, p0, Li0/p1$a;->a:Lh0/c4$b;

    invoke-static {p3, p1, p2, v0}, Li0/p1$a;->c(Lh0/g3;Lp2/q;Lj1/x$b;Lh0/c4$b;)Lj1/x$b;

    move-result-object p1

    iput-object p1, p0, Li0/p1$a;->d:Lj1/x$b;

    :cond_1
    invoke-interface {p3}, Lh0/g3;->N()Lh0/c4;

    move-result-object p1

    invoke-direct {p0, p1}, Li0/p1$a;->m(Lh0/c4;)V

    return-void
.end method

.method public l(Lh0/g3;)V
    .locals 3

    iget-object v0, p0, Li0/p1$a;->b:Lp2/q;

    iget-object v1, p0, Li0/p1$a;->e:Lj1/x$b;

    iget-object v2, p0, Li0/p1$a;->a:Lh0/c4$b;

    invoke-static {p1, v0, v1, v2}, Li0/p1$a;->c(Lh0/g3;Lp2/q;Lj1/x$b;Lh0/c4$b;)Lj1/x$b;

    move-result-object v0

    iput-object v0, p0, Li0/p1$a;->d:Lj1/x$b;

    invoke-interface {p1}, Lh0/g3;->N()Lh0/c4;

    move-result-object p1

    invoke-direct {p0, p1}, Li0/p1$a;->m(Lh0/c4;)V

    return-void
.end method
