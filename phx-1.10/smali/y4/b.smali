.class public Ly4/b;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lz4/k;

.field private b:Ln4/a;

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lz4/k$d;",
            ">;>;"
        }
    .end annotation
.end field

.field final d:Lz4/k$c;


# direct methods
.method public constructor <init>(Lm4/a;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ly4/b$a;

    invoke-direct {v0, p0}, Ly4/b$a;-><init>(Ly4/b;)V

    iput-object v0, p0, Ly4/b;->d:Lz4/k$c;

    new-instance v1, Lz4/k;

    sget-object v2, Lz4/s;->b:Lz4/s;

    const-string v3, "flutter/deferredcomponent"

    invoke-direct {v1, p1, v3, v2}, Lz4/k;-><init>(Lz4/c;Ljava/lang/String;Lz4/l;)V

    iput-object v1, p0, Ly4/b;->a:Lz4/k;

    invoke-virtual {v1, v0}, Lz4/k;->e(Lz4/k$c;)V

    invoke-static {}, Ll4/a;->e()Ll4/a;

    move-result-object p1

    invoke-virtual {p1}, Ll4/a;->a()Ln4/a;

    move-result-object p1

    iput-object p1, p0, Ly4/b;->b:Ln4/a;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Ly4/b;->c:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Ly4/b;)Ln4/a;
    .locals 0

    iget-object p0, p0, Ly4/b;->b:Ln4/a;

    return-object p0
.end method

.method static synthetic b(Ly4/b;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Ly4/b;->c:Ljava/util/Map;

    return-object p0
.end method


# virtual methods
.method public c(Ln4/a;)V
    .locals 0

    iput-object p1, p0, Ly4/b;->b:Ln4/a;

    return-void
.end method
