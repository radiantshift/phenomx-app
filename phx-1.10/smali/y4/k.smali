.class public Ly4/k;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ly4/k$f;,
        Ly4/k$b;,
        Ly4/k$c;,
        Ly4/k$e;,
        Ly4/k$d;,
        Ly4/k$g;
    }
.end annotation


# instance fields
.field private final a:Lz4/k;

.field private b:Ly4/k$g;

.field private final c:Lz4/k$c;


# direct methods
.method public constructor <init>(Lm4/a;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ly4/k$a;

    invoke-direct {v0, p0}, Ly4/k$a;-><init>(Ly4/k;)V

    iput-object v0, p0, Ly4/k;->c:Lz4/k$c;

    new-instance v1, Lz4/k;

    sget-object v2, Lz4/s;->b:Lz4/s;

    const-string v3, "flutter/platform_views"

    invoke-direct {v1, p1, v3, v2}, Lz4/k;-><init>(Lz4/c;Ljava/lang/String;Lz4/l;)V

    iput-object v1, p0, Ly4/k;->a:Lz4/k;

    invoke-virtual {v1, v0}, Lz4/k;->e(Lz4/k$c;)V

    return-void
.end method

.method static synthetic a(Ly4/k;)Ly4/k$g;
    .locals 0

    iget-object p0, p0, Ly4/k;->b:Ly4/k$g;

    return-object p0
.end method

.method static synthetic b(Ljava/lang/Exception;)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Ly4/k;->c(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static c(Ljava/lang/Exception;)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Ll4/b;->d(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public d(I)V
    .locals 2

    iget-object v0, p0, Ly4/k;->a:Lz4/k;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "viewFocused"

    invoke-virtual {v0, v1, p1}, Lz4/k;->c(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public e(Ly4/k$g;)V
    .locals 0

    iput-object p1, p0, Ly4/k;->b:Ly4/k$g;

    return-void
.end method
