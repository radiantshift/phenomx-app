.class Ly4/l$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz4/k$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ly4/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic f:Ly4/l;


# direct methods
.method constructor <init>(Ly4/l;)V
    .locals 0

    iput-object p1, p0, Ly4/l$b;->f:Ly4/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public C(Lz4/j;Lz4/k$d;)V
    .locals 2

    iget-object v0, p1, Lz4/j;->a:Ljava/lang/String;

    iget-object p1, p1, Lz4/j;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    const-string v1, "get"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "put"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2}, Lz4/k$d;->c()V

    goto :goto_2

    :cond_0
    iget-object v0, p0, Ly4/l$b;->f:Ly4/l;

    check-cast p1, [B

    invoke-static {v0, p1}, Ly4/l;->b(Ly4/l;[B)[B

    const/4 p1, 0x0

    :goto_0
    invoke-interface {p2, p1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    goto :goto_2

    :cond_1
    iget-object p1, p0, Ly4/l$b;->f:Ly4/l;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Ly4/l;->c(Ly4/l;Z)Z

    iget-object p1, p0, Ly4/l$b;->f:Ly4/l;

    invoke-static {p1}, Ly4/l;->d(Ly4/l;)Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Ly4/l$b;->f:Ly4/l;

    iget-boolean v0, p1, Ly4/l;->a:Z

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {p1, p2}, Ly4/l;->f(Ly4/l;Lz4/k$d;)Lz4/k$d;

    goto :goto_2

    :cond_3
    :goto_1
    iget-object p1, p0, Ly4/l$b;->f:Ly4/l;

    invoke-static {p1}, Ly4/l;->a(Ly4/l;)[B

    move-result-object v0

    invoke-static {p1, v0}, Ly4/l;->e(Ly4/l;[B)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    :goto_2
    return-void
.end method
