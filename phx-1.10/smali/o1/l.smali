.class final Lo1/l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lj1/q0;


# instance fields
.field private final f:I

.field private final g:Lo1/p;

.field private h:I


# direct methods
.method public constructor <init>(Lo1/p;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo1/l;->g:Lo1/p;

    iput p2, p0, Lo1/l;->f:I

    const/4 p1, -0x1

    iput p1, p0, Lo1/l;->h:I

    return-void
.end method

.method private c()Z
    .locals 2

    iget v0, p0, Lo1/l;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, -0x3

    if-eq v0, v1, :cond_0

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget v0, p0, Lo1/l;->h:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->a(Z)V

    iget-object v0, p0, Lo1/l;->g:Lo1/p;

    iget v1, p0, Lo1/l;->f:I

    invoke-virtual {v0, v1}, Lo1/p;->y(I)I

    move-result v0

    iput v0, p0, Lo1/l;->h:I

    return-void
.end method

.method public b()V
    .locals 3

    iget v0, p0, Lo1/l;->h:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_2

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lo1/l;->g:Lo1/p;

    invoke-virtual {v0}, Lo1/p;->U()V

    goto :goto_0

    :cond_0
    const/4 v1, -0x3

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lo1/l;->g:Lo1/p;

    invoke-virtual {v1, v0}, Lo1/p;->V(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v0, Lo1/r;

    iget-object v1, p0, Lo1/l;->g:Lo1/p;

    invoke-virtual {v1}, Lo1/p;->o()Lj1/z0;

    move-result-object v1

    iget v2, p0, Lo1/l;->f:I

    invoke-virtual {v1, v2}, Lj1/z0;->b(I)Lj1/x0;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lj1/x0;->b(I)Lh0/r1;

    move-result-object v1

    iget-object v1, v1, Lh0/r1;->q:Ljava/lang/String;

    invoke-direct {v0, v1}, Lo1/r;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d()V
    .locals 3

    iget v0, p0, Lo1/l;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lo1/l;->g:Lo1/p;

    iget v2, p0, Lo1/l;->f:I

    invoke-virtual {v0, v2}, Lo1/p;->p0(I)V

    iput v1, p0, Lo1/l;->h:I

    :cond_0
    return-void
.end method

.method public e(Lh0/s1;Lk0/g;I)I
    .locals 2

    iget v0, p0, Lo1/l;->h:I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_0

    const/4 p1, 0x4

    invoke-virtual {p2, p1}, Lk0/a;->e(I)V

    const/4 p1, -0x4

    return p1

    :cond_0
    invoke-direct {p0}, Lo1/l;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lo1/l;->g:Lo1/p;

    iget v1, p0, Lo1/l;->h:I

    invoke-virtual {v0, v1, p1, p2, p3}, Lo1/p;->e0(ILh0/s1;Lk0/g;I)I

    move-result v1

    :cond_1
    return v1
.end method

.method public i(J)I
    .locals 2

    invoke-direct {p0}, Lo1/l;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo1/l;->g:Lo1/p;

    iget v1, p0, Lo1/l;->h:I

    invoke-virtual {v0, v1, p1, p2}, Lo1/p;->o0(IJ)I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public k()Z
    .locals 2

    iget v0, p0, Lo1/l;->h:I

    const/4 v1, -0x3

    if-eq v0, v1, :cond_1

    invoke-direct {p0}, Lo1/l;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo1/l;->g:Lo1/p;

    iget v1, p0, Lo1/l;->h:I

    invoke-virtual {v0, v1}, Lo1/p;->Q(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
