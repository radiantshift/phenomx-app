.class public final Lo1/k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lj1/u;
.implements Lp1/l$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lo1/k$b;
    }
.end annotation


# instance fields
.field private A:[Lo1/p;

.field private B:[[I

.field private C:I

.field private D:Lj1/r0;

.field private final f:Lo1/h;

.field private final g:Lp1/l;

.field private final h:Lo1/g;

.field private final i:Ld2/p0;

.field private final j:Ll0/y;

.field private final k:Ll0/w$a;

.field private final l:Ld2/g0;

.field private final m:Lj1/e0$a;

.field private final n:Ld2/b;

.field private final o:Ljava/util/IdentityHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/IdentityHashMap<",
            "Lj1/q0;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lo1/s;

.field private final q:Lj1/i;

.field private final r:Z

.field private final s:I

.field private final t:Z

.field private final u:Li0/u1;

.field private final v:Lo1/p$b;

.field private w:Lj1/u$a;

.field private x:I

.field private y:Lj1/z0;

.field private z:[Lo1/p;


# direct methods
.method public constructor <init>(Lo1/h;Lp1/l;Lo1/g;Ld2/p0;Ll0/y;Ll0/w$a;Ld2/g0;Lj1/e0$a;Ld2/b;Lj1/i;ZIZLi0/u1;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo1/k;->f:Lo1/h;

    iput-object p2, p0, Lo1/k;->g:Lp1/l;

    iput-object p3, p0, Lo1/k;->h:Lo1/g;

    iput-object p4, p0, Lo1/k;->i:Ld2/p0;

    iput-object p5, p0, Lo1/k;->j:Ll0/y;

    iput-object p6, p0, Lo1/k;->k:Ll0/w$a;

    iput-object p7, p0, Lo1/k;->l:Ld2/g0;

    iput-object p8, p0, Lo1/k;->m:Lj1/e0$a;

    iput-object p9, p0, Lo1/k;->n:Ld2/b;

    iput-object p10, p0, Lo1/k;->q:Lj1/i;

    iput-boolean p11, p0, Lo1/k;->r:Z

    iput p12, p0, Lo1/k;->s:I

    iput-boolean p13, p0, Lo1/k;->t:Z

    iput-object p14, p0, Lo1/k;->u:Li0/u1;

    new-instance p1, Lo1/k$b;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lo1/k$b;-><init>(Lo1/k;Lo1/k$a;)V

    iput-object p1, p0, Lo1/k;->v:Lo1/p$b;

    const/4 p1, 0x0

    new-array p2, p1, [Lj1/r0;

    invoke-interface {p10, p2}, Lj1/i;->a([Lj1/r0;)Lj1/r0;

    move-result-object p2

    iput-object p2, p0, Lo1/k;->D:Lj1/r0;

    new-instance p2, Ljava/util/IdentityHashMap;

    invoke-direct {p2}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object p2, p0, Lo1/k;->o:Ljava/util/IdentityHashMap;

    new-instance p2, Lo1/s;

    invoke-direct {p2}, Lo1/s;-><init>()V

    iput-object p2, p0, Lo1/k;->p:Lo1/s;

    new-array p2, p1, [Lo1/p;

    iput-object p2, p0, Lo1/k;->z:[Lo1/p;

    new-array p2, p1, [Lo1/p;

    iput-object p2, p0, Lo1/k;->A:[Lo1/p;

    new-array p1, p1, [[I

    iput-object p1, p0, Lo1/k;->B:[[I

    return-void
.end method

.method private static A(Lh0/r1;)Lh0/r1;
    .locals 4

    iget-object v0, p0, Lh0/r1;->n:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Le2/n0;->L(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Le2/v;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lh0/r1$b;

    invoke-direct {v2}, Lh0/r1$b;-><init>()V

    iget-object v3, p0, Lh0/r1;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lh0/r1$b;->U(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v2

    iget-object v3, p0, Lh0/r1;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lh0/r1$b;->W(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v2

    iget-object v3, p0, Lh0/r1;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lh0/r1$b;->M(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v2

    invoke-virtual {v2, v1}, Lh0/r1$b;->g0(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lh0/r1$b;->K(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v0

    iget-object v1, p0, Lh0/r1;->o:Lz0/a;

    invoke-virtual {v0, v1}, Lh0/r1$b;->Z(Lz0/a;)Lh0/r1$b;

    move-result-object v0

    iget v1, p0, Lh0/r1;->k:I

    invoke-virtual {v0, v1}, Lh0/r1$b;->I(I)Lh0/r1$b;

    move-result-object v0

    iget v1, p0, Lh0/r1;->l:I

    invoke-virtual {v0, v1}, Lh0/r1$b;->b0(I)Lh0/r1$b;

    move-result-object v0

    iget v1, p0, Lh0/r1;->v:I

    invoke-virtual {v0, v1}, Lh0/r1$b;->n0(I)Lh0/r1$b;

    move-result-object v0

    iget v1, p0, Lh0/r1;->w:I

    invoke-virtual {v0, v1}, Lh0/r1$b;->S(I)Lh0/r1$b;

    move-result-object v0

    iget v1, p0, Lh0/r1;->x:F

    invoke-virtual {v0, v1}, Lh0/r1$b;->R(F)Lh0/r1$b;

    move-result-object v0

    iget v1, p0, Lh0/r1;->i:I

    invoke-virtual {v0, v1}, Lh0/r1$b;->i0(I)Lh0/r1$b;

    move-result-object v0

    iget p0, p0, Lh0/r1;->j:I

    invoke-virtual {v0, p0}, Lh0/r1$b;->e0(I)Lh0/r1$b;

    move-result-object p0

    invoke-virtual {p0}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object p0

    return-object p0
.end method

.method static synthetic i(Lo1/k;)I
    .locals 1

    iget v0, p0, Lo1/k;->x:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lo1/k;->x:I

    return v0
.end method

.method static synthetic j(Lo1/k;)[Lo1/p;
    .locals 0

    iget-object p0, p0, Lo1/k;->z:[Lo1/p;

    return-object p0
.end method

.method static synthetic k(Lo1/k;Lj1/z0;)Lj1/z0;
    .locals 0

    iput-object p1, p0, Lo1/k;->y:Lj1/z0;

    return-object p1
.end method

.method static synthetic l(Lo1/k;)Lj1/u$a;
    .locals 0

    iget-object p0, p0, Lo1/k;->w:Lj1/u$a;

    return-object p0
.end method

.method static synthetic m(Lo1/k;)Lp1/l;
    .locals 0

    iget-object p0, p0, Lo1/k;->g:Lp1/l;

    return-object p0
.end method

.method private t(JLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lp1/h$a;",
            ">;",
            "Ljava/util/List<",
            "Lo1/p;",
            ">;",
            "Ljava/util/List<",
            "[I>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ll0/m;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p3

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_5

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lp1/h$a;

    iget-object v7, v7, Lp1/h$a;->d:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    move-object/from16 v13, p0

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    goto/16 :goto_3

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    :goto_1
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v11

    if-ge v9, v11, :cond_3

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lp1/h$a;

    iget-object v11, v11, Lp1/h$a;->d:Ljava/lang/String;

    invoke-static {v7, v11}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lp1/h$a;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v3, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v12, v11, Lp1/h$a;->a:Landroid/net/Uri;

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v12, v11, Lp1/h$a;->b:Lh0/r1;

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v11, v11, Lp1/h$a;->b:Lh0/r1;

    iget-object v11, v11, Lh0/r1;->n:Ljava/lang/String;

    invoke-static {v11, v8}, Le2/n0;->K(Ljava/lang/String;I)I

    move-result v11

    if-ne v11, v8, :cond_1

    const/4 v11, 0x1

    goto :goto_2

    :cond_1
    const/4 v11, 0x0

    :goto_2
    and-int/2addr v10, v11

    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "audio:"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v14, 0x1

    new-array v9, v5, [Landroid/net/Uri;

    invoke-static {v9}, Le2/n0;->k([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Landroid/net/Uri;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    move-object v15, v9

    check-cast v15, [Landroid/net/Uri;

    new-array v9, v5, [Lh0/r1;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    move-object/from16 v16, v9

    check-cast v16, [Lh0/r1;

    const/16 v17, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v18

    move-object/from16 v12, p0

    move-object v13, v7

    move-object/from16 v19, p6

    move-wide/from16 v20, p1

    invoke-direct/range {v12 .. v21}, Lo1/k;->x(Ljava/lang/String;I[Landroid/net/Uri;[Lh0/r1;Lh0/r1;Ljava/util/List;Ljava/util/Map;J)Lo1/p;

    move-result-object v9

    invoke-static {v3}, Lr2/e;->k(Ljava/util/Collection;)[I

    move-result-object v11

    move-object/from16 v12, p5

    invoke-interface {v12, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v11, p4

    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v13, p0

    iget-boolean v14, v13, Lo1/k;->r:Z

    if-eqz v14, :cond_4

    if-eqz v10, :cond_4

    new-array v10, v5, [Lh0/r1;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Lh0/r1;

    new-array v8, v8, [Lj1/x0;

    new-instance v14, Lj1/x0;

    invoke-direct {v14, v7, v10}, Lj1/x0;-><init>(Ljava/lang/String;[Lh0/r1;)V

    aput-object v14, v8, v5

    new-array v7, v5, [I

    invoke-virtual {v9, v8, v5, v7}, Lo1/p;->d0([Lj1/x0;I[I)V

    :cond_4
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    :cond_5
    move-object/from16 v13, p0

    return-void
.end method

.method private v(Lp1/h;JLjava/util/List;Ljava/util/List;Ljava/util/Map;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lp1/h;",
            "J",
            "Ljava/util/List<",
            "Lo1/p;",
            ">;",
            "Ljava/util/List<",
            "[I>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ll0/m;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    iget-object v1, v0, Lp1/h;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v2, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    iget-object v7, v0, Lp1/h;->e:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x2

    const/4 v9, 0x1

    if-ge v4, v7, :cond_3

    iget-object v7, v0, Lp1/h;->e:Ljava/util/List;

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lp1/h$b;

    iget-object v7, v7, Lp1/h$b;->b:Lh0/r1;

    iget v10, v7, Lh0/r1;->w:I

    if-gtz v10, :cond_2

    iget-object v10, v7, Lh0/r1;->n:Ljava/lang/String;

    invoke-static {v10, v8}, Le2/n0;->L(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    goto :goto_1

    :cond_0
    iget-object v7, v7, Lh0/r1;->n:Ljava/lang/String;

    invoke-static {v7, v9}, Le2/n0;->L(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    aput v9, v2, v4

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_1
    const/4 v7, -0x1

    aput v7, v2, v4

    goto :goto_2

    :cond_2
    :goto_1
    aput v8, v2, v4

    add-int/lit8 v5, v5, 0x1

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    if-lez v5, :cond_4

    move v1, v5

    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    if-ge v6, v1, :cond_5

    sub-int/2addr v1, v6

    const/4 v4, 0x0

    const/4 v5, 0x1

    goto :goto_4

    :cond_5
    const/4 v4, 0x0

    :goto_3
    const/4 v5, 0x0

    :goto_4
    new-array v13, v1, [Landroid/net/Uri;

    new-array v6, v1, [Lh0/r1;

    new-array v7, v1, [I

    const/4 v10, 0x0

    const/4 v11, 0x0

    :goto_5
    iget-object v12, v0, Lp1/h;->e:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-ge v10, v12, :cond_9

    if-eqz v4, :cond_6

    aget v12, v2, v10

    if-ne v12, v8, :cond_8

    :cond_6
    if-eqz v5, :cond_7

    aget v12, v2, v10

    if-eq v12, v9, :cond_8

    :cond_7
    iget-object v12, v0, Lp1/h;->e:Ljava/util/List;

    invoke-interface {v12, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lp1/h$b;

    iget-object v14, v12, Lp1/h$b;->a:Landroid/net/Uri;

    aput-object v14, v13, v11

    iget-object v12, v12, Lp1/h$b;->b:Lh0/r1;

    aput-object v12, v6, v11

    add-int/lit8 v12, v11, 0x1

    aput v10, v7, v11

    move v11, v12

    :cond_8
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    :cond_9
    aget-object v2, v6, v3

    iget-object v2, v2, Lh0/r1;->n:Ljava/lang/String;

    invoke-static {v2, v8}, Le2/n0;->K(Ljava/lang/String;I)I

    move-result v5

    invoke-static {v2, v9}, Le2/n0;->K(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v9, :cond_a

    if-nez v2, :cond_b

    iget-object v8, v0, Lp1/h;->g:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_b

    :cond_a
    if-gt v5, v9, :cond_b

    add-int v8, v2, v5

    if-lez v8, :cond_b

    const/4 v8, 0x1

    goto :goto_6

    :cond_b
    const/4 v8, 0x0

    :goto_6
    if-nez v4, :cond_c

    if-lez v2, :cond_c

    const/4 v12, 0x1

    goto :goto_7

    :cond_c
    const/4 v12, 0x0

    :goto_7
    const-string v4, "main"

    iget-object v15, v0, Lp1/h;->j:Lh0/r1;

    iget-object v14, v0, Lp1/h;->k:Ljava/util/List;

    move-object/from16 v10, p0

    move-object v11, v4

    move-object/from16 v16, v14

    move-object v14, v6

    move-object/from16 v17, p6

    move-wide/from16 v18, p2

    invoke-direct/range {v10 .. v19}, Lo1/k;->x(Ljava/lang/String;I[Landroid/net/Uri;[Lh0/r1;Lh0/r1;Ljava/util/List;Ljava/util/Map;J)Lo1/p;

    move-result-object v10

    move-object/from16 v11, p4

    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v11, p5

    invoke-interface {v11, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v7, p0

    iget-boolean v11, v7, Lo1/k;->r:Z

    if-eqz v11, :cond_13

    if-eqz v8, :cond_13

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    if-lez v5, :cond_10

    new-array v5, v1, [Lh0/r1;

    const/4 v11, 0x0

    :goto_8
    if-ge v11, v1, :cond_d

    aget-object v12, v6, v11

    invoke-static {v12}, Lo1/k;->A(Lh0/r1;)Lh0/r1;

    move-result-object v12

    aput-object v12, v5, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_8

    :cond_d
    new-instance v1, Lj1/x0;

    invoke-direct {v1, v4, v5}, Lj1/x0;-><init>(Ljava/lang/String;[Lh0/r1;)V

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-lez v2, :cond_f

    iget-object v1, v0, Lp1/h;->j:Lh0/r1;

    if-nez v1, :cond_e

    iget-object v1, v0, Lp1/h;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_f

    :cond_e
    new-instance v1, Lj1/x0;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ":audio"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v5, v9, [Lh0/r1;

    aget-object v6, v6, v3

    iget-object v11, v0, Lp1/h;->j:Lh0/r1;

    invoke-static {v6, v11, v3}, Lo1/k;->y(Lh0/r1;Lh0/r1;Z)Lh0/r1;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-direct {v1, v2, v5}, Lj1/x0;-><init>(Ljava/lang/String;[Lh0/r1;)V

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_f
    iget-object v0, v0, Lp1/h;->k:Ljava/util/List;

    if-eqz v0, :cond_12

    const/4 v1, 0x0

    :goto_9
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_12

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ":cc:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lj1/x0;

    new-array v6, v9, [Lh0/r1;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lh0/r1;

    aput-object v11, v6, v3

    invoke-direct {v5, v2, v6}, Lj1/x0;-><init>(Ljava/lang/String;[Lh0/r1;)V

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_10
    new-array v2, v1, [Lh0/r1;

    const/4 v5, 0x0

    :goto_a
    if-ge v5, v1, :cond_11

    aget-object v11, v6, v5

    iget-object v12, v0, Lp1/h;->j:Lh0/r1;

    invoke-static {v11, v12, v9}, Lo1/k;->y(Lh0/r1;Lh0/r1;Z)Lh0/r1;

    move-result-object v11

    aput-object v11, v2, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    :cond_11
    new-instance v0, Lj1/x0;

    invoke-direct {v0, v4, v2}, Lj1/x0;-><init>(Ljava/lang/String;[Lh0/r1;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_12
    new-instance v0, Lj1/x0;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ":id3"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v9, [Lh0/r1;

    new-instance v4, Lh0/r1$b;

    invoke-direct {v4}, Lh0/r1$b;-><init>()V

    const-string v5, "ID3"

    invoke-virtual {v4, v5}, Lh0/r1$b;->U(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v4

    const-string v5, "application/id3"

    invoke-virtual {v4, v5}, Lh0/r1$b;->g0(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v4

    invoke-virtual {v4}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lj1/x0;-><init>(Ljava/lang/String;[Lh0/r1;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v1, v3, [Lj1/x0;

    invoke-interface {v8, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lj1/x0;

    new-array v2, v9, [I

    invoke-interface {v8, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    aput v0, v2, v3

    invoke-virtual {v10, v1, v3, v2}, Lo1/p;->d0([Lj1/x0;I[I)V

    :cond_13
    return-void
.end method

.method private w(J)V
    .locals 20

    move-object/from16 v10, p0

    iget-object v0, v10, Lo1/k;->g:Lp1/l;

    invoke-interface {v0}, Lp1/l;->d()Lp1/h;

    move-result-object v0

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lp1/h;

    iget-boolean v0, v10, Lo1/k;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, v1, Lp1/h;->m:Ljava/util/List;

    invoke-static {v0}, Lo1/k;->z(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :goto_0
    move-object v11, v0

    iget-object v0, v1, Lp1/h;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v12, 0x1

    xor-int/2addr v0, v12

    iget-object v7, v1, Lp1/h;->g:Ljava/util/List;

    iget-object v13, v1, Lp1/h;->h:Ljava/util/List;

    const/4 v14, 0x0

    iput v14, v10, Lo1/k;->x:I

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_1

    move-object/from16 v0, p0

    move-wide/from16 v2, p1

    move-object v4, v15

    move-object v5, v8

    move-object v6, v11

    invoke-direct/range {v0 .. v6}, Lo1/k;->v(Lp1/h;JLjava/util/List;Ljava/util/List;Ljava/util/Map;)V

    :cond_1
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-object v3, v7

    move-object v4, v15

    move-object v5, v8

    move-object v6, v11

    invoke-direct/range {v0 .. v6}, Lo1/k;->t(JLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;)V

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, v10, Lo1/k;->C:I

    const/4 v9, 0x0

    :goto_1
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v0

    if-ge v9, v0, :cond_2

    invoke-interface {v13, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lp1/h$a;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "subtitle:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v7, Lp1/h$a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v2, 0x3

    new-array v3, v12, [Landroid/net/Uri;

    iget-object v0, v7, Lp1/h$a;->a:Landroid/net/Uri;

    aput-object v0, v3, v14

    new-array v4, v12, [Lh0/r1;

    iget-object v0, v7, Lp1/h$a;->b:Lh0/r1;

    aput-object v0, v4, v14

    const/4 v5, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v16

    move-object/from16 v0, p0

    move-object v1, v6

    move-object/from16 v17, v6

    move-object/from16 v6, v16

    move-object/from16 v18, v7

    move-object v7, v11

    move-object/from16 v19, v8

    move/from16 v16, v9

    move-wide/from16 v8, p1

    invoke-direct/range {v0 .. v9}, Lo1/k;->x(Ljava/lang/String;I[Landroid/net/Uri;[Lh0/r1;Lh0/r1;Ljava/util/List;Ljava/util/Map;J)Lo1/p;

    move-result-object v0

    new-array v1, v12, [I

    aput v16, v1, v14

    move-object/from16 v2, v19

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-array v1, v12, [Lj1/x0;

    new-instance v3, Lj1/x0;

    new-array v4, v12, [Lh0/r1;

    move-object/from16 v5, v18

    iget-object v5, v5, Lp1/h$a;->b:Lh0/r1;

    aput-object v5, v4, v14

    move-object/from16 v5, v17

    invoke-direct {v3, v5, v4}, Lj1/x0;-><init>(Ljava/lang/String;[Lh0/r1;)V

    aput-object v3, v1, v14

    new-array v3, v14, [I

    invoke-virtual {v0, v1, v14, v3}, Lo1/p;->d0([Lj1/x0;I[I)V

    add-int/lit8 v9, v16, 0x1

    move-object v8, v2

    goto :goto_1

    :cond_2
    move-object v2, v8

    new-array v0, v14, [Lo1/p;

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lo1/p;

    iput-object v0, v10, Lo1/k;->z:[Lo1/p;

    new-array v0, v14, [[I

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, v10, Lo1/k;->B:[[I

    iget-object v0, v10, Lo1/k;->z:[Lo1/p;

    array-length v0, v0

    iput v0, v10, Lo1/k;->x:I

    const/4 v0, 0x0

    :goto_2
    iget v1, v10, Lo1/k;->C:I

    if-ge v0, v1, :cond_3

    iget-object v1, v10, Lo1/k;->z:[Lo1/p;

    aget-object v1, v1, v0

    invoke-virtual {v1, v12}, Lo1/p;->m0(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, v10, Lo1/k;->z:[Lo1/p;

    array-length v1, v0

    :goto_3
    if-ge v14, v1, :cond_4

    aget-object v2, v0, v14

    invoke-virtual {v2}, Lo1/p;->B()V

    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    :cond_4
    iget-object v0, v10, Lo1/k;->z:[Lo1/p;

    iput-object v0, v10, Lo1/k;->A:[Lo1/p;

    return-void
.end method

.method private x(Ljava/lang/String;I[Landroid/net/Uri;[Lh0/r1;Lh0/r1;Ljava/util/List;Ljava/util/Map;J)Lo1/p;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I[",
            "Landroid/net/Uri;",
            "[",
            "Lh0/r1;",
            "Lh0/r1;",
            "Ljava/util/List<",
            "Lh0/r1;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ll0/m;",
            ">;J)",
            "Lo1/p;"
        }
    .end annotation

    move-object/from16 v0, p0

    new-instance v11, Lo1/f;

    iget-object v2, v0, Lo1/k;->f:Lo1/h;

    iget-object v3, v0, Lo1/k;->g:Lp1/l;

    iget-object v6, v0, Lo1/k;->h:Lo1/g;

    iget-object v7, v0, Lo1/k;->i:Ld2/p0;

    iget-object v8, v0, Lo1/k;->p:Lo1/s;

    iget-object v10, v0, Lo1/k;->u:Li0/u1;

    move-object v1, v11

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v10}, Lo1/f;-><init>(Lo1/h;Lp1/l;[Landroid/net/Uri;[Lh0/r1;Lo1/g;Ld2/p0;Lo1/s;Ljava/util/List;Li0/u1;)V

    new-instance v16, Lo1/p;

    iget-object v4, v0, Lo1/k;->v:Lo1/p$b;

    iget-object v7, v0, Lo1/k;->n:Ld2/b;

    iget-object v12, v0, Lo1/k;->j:Ll0/y;

    iget-object v13, v0, Lo1/k;->k:Ll0/w$a;

    iget-object v14, v0, Lo1/k;->l:Ld2/g0;

    iget-object v15, v0, Lo1/k;->m:Lj1/e0$a;

    iget v10, v0, Lo1/k;->s:I

    move-object/from16 v1, v16

    move-object/from16 v2, p1

    move/from16 v3, p2

    move-object v5, v11

    move-object/from16 v6, p7

    move-wide/from16 v8, p8

    move/from16 v17, v10

    move-object/from16 v10, p5

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move/from16 v15, v17

    invoke-direct/range {v1 .. v15}, Lo1/p;-><init>(Ljava/lang/String;ILo1/p$b;Lo1/f;Ljava/util/Map;Ld2/b;JLh0/r1;Ll0/y;Ll0/w$a;Ld2/g0;Lj1/e0$a;I)V

    return-object v16
.end method

.method private static y(Lh0/r1;Lh0/r1;Z)Lh0/r1;
    .locals 12

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-eqz p1, :cond_0

    iget-object v0, p1, Lh0/r1;->n:Ljava/lang/String;

    iget-object v1, p1, Lh0/r1;->o:Lz0/a;

    iget v3, p1, Lh0/r1;->D:I

    iget v4, p1, Lh0/r1;->i:I

    iget v5, p1, Lh0/r1;->j:I

    iget-object v6, p1, Lh0/r1;->h:Ljava/lang/String;

    iget-object p1, p1, Lh0/r1;->g:Ljava/lang/String;

    move-object v10, v6

    move v6, v3

    move v3, v5

    move-object v5, v10

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lh0/r1;->n:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {p1, v3}, Le2/n0;->L(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    iget-object v3, p0, Lh0/r1;->o:Lz0/a;

    if-eqz p2, :cond_1

    iget v0, p0, Lh0/r1;->D:I

    iget v1, p0, Lh0/r1;->i:I

    iget v4, p0, Lh0/r1;->j:I

    iget-object v5, p0, Lh0/r1;->h:Ljava/lang/String;

    iget-object v6, p0, Lh0/r1;->g:Ljava/lang/String;

    move v10, v0

    move-object v0, p1

    move-object p1, v6

    move v6, v10

    move v11, v4

    move v4, v1

    move-object v1, v3

    move v3, v11

    goto :goto_0

    :cond_1
    move-object v5, v0

    move-object v1, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, -0x1

    move-object v0, p1

    move-object p1, v5

    :goto_0
    invoke-static {v0}, Le2/v;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz p2, :cond_2

    iget v8, p0, Lh0/r1;->k:I

    goto :goto_1

    :cond_2
    const/4 v8, -0x1

    :goto_1
    if-eqz p2, :cond_3

    iget v2, p0, Lh0/r1;->l:I

    :cond_3
    new-instance p2, Lh0/r1$b;

    invoke-direct {p2}, Lh0/r1$b;-><init>()V

    iget-object v9, p0, Lh0/r1;->f:Ljava/lang/String;

    invoke-virtual {p2, v9}, Lh0/r1$b;->U(Ljava/lang/String;)Lh0/r1$b;

    move-result-object p2

    invoke-virtual {p2, p1}, Lh0/r1$b;->W(Ljava/lang/String;)Lh0/r1$b;

    move-result-object p1

    iget-object p0, p0, Lh0/r1;->p:Ljava/lang/String;

    invoke-virtual {p1, p0}, Lh0/r1$b;->M(Ljava/lang/String;)Lh0/r1$b;

    move-result-object p0

    invoke-virtual {p0, v7}, Lh0/r1$b;->g0(Ljava/lang/String;)Lh0/r1$b;

    move-result-object p0

    invoke-virtual {p0, v0}, Lh0/r1$b;->K(Ljava/lang/String;)Lh0/r1$b;

    move-result-object p0

    invoke-virtual {p0, v1}, Lh0/r1$b;->Z(Lz0/a;)Lh0/r1$b;

    move-result-object p0

    invoke-virtual {p0, v8}, Lh0/r1$b;->I(I)Lh0/r1$b;

    move-result-object p0

    invoke-virtual {p0, v2}, Lh0/r1$b;->b0(I)Lh0/r1$b;

    move-result-object p0

    invoke-virtual {p0, v6}, Lh0/r1$b;->J(I)Lh0/r1$b;

    move-result-object p0

    invoke-virtual {p0, v4}, Lh0/r1$b;->i0(I)Lh0/r1$b;

    move-result-object p0

    invoke-virtual {p0, v3}, Lh0/r1$b;->e0(I)Lh0/r1$b;

    move-result-object p0

    invoke-virtual {p0, v5}, Lh0/r1$b;->X(Ljava/lang/String;)Lh0/r1$b;

    move-result-object p0

    invoke-virtual {p0}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object p0

    return-object p0
.end method

.method private static z(Ljava/util/List;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ll0/m;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ll0/m;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ll0/m;

    iget-object v4, v3, Ll0/m;->h:Ljava/lang/String;

    add-int/lit8 v2, v2, 0x1

    move v5, v2

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v5, v6, :cond_1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ll0/m;

    iget-object v7, v6, Ll0/m;->h:Ljava/lang/String;

    invoke-static {v7, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v3, v6}, Ll0/m;->i(Ll0/m;)Ll0/m;

    move-result-object v3

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v1, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-object v1
.end method


# virtual methods
.method public B()V
    .locals 4

    iget-object v0, p0, Lo1/k;->g:Lp1/l;

    invoke-interface {v0, p0}, Lp1/l;->o(Lp1/l$b;)V

    iget-object v0, p0, Lo1/k;->z:[Lo1/p;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3}, Lo1/p;->f0()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lo1/k;->w:Lj1/u$a;

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lo1/k;->D:Lj1/r0;

    invoke-interface {v0}, Lj1/r0;->a()Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 4

    iget-object v0, p0, Lo1/k;->z:[Lo1/p;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3}, Lo1/p;->b0()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lo1/k;->w:Lj1/u$a;

    invoke-interface {v0, p0}, Lj1/r0$a;->i(Lj1/r0;)V

    return-void
.end method

.method public c(JLh0/u3;)J
    .locals 5

    iget-object v0, p0, Lo1/k;->A:[Lo1/p;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    invoke-virtual {v3}, Lo1/p;->R()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, p1, p2, p3}, Lo1/p;->c(JLh0/u3;)J

    move-result-wide p1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-wide p1
.end method

.method public d()J
    .locals 2

    iget-object v0, p0, Lo1/k;->D:Lj1/r0;

    invoke-interface {v0}, Lj1/r0;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public e(Landroid/net/Uri;Ld2/g0$c;Z)Z
    .locals 5

    iget-object v0, p0, Lo1/k;->z:[Lo1/p;

    array-length v1, v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, v0, v3

    invoke-virtual {v4, p1, p2, p3}, Lo1/p;->a0(Landroid/net/Uri;Ld2/g0$c;Z)Z

    move-result v4

    and-int/2addr v2, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lo1/k;->w:Lj1/u$a;

    invoke-interface {p1, p0}, Lj1/r0$a;->i(Lj1/r0;)V

    return v2
.end method

.method public f()J
    .locals 2

    iget-object v0, p0, Lo1/k;->D:Lj1/r0;

    invoke-interface {v0}, Lj1/r0;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public g(J)Z
    .locals 3

    iget-object v0, p0, Lo1/k;->y:Lj1/z0;

    if-nez v0, :cond_1

    iget-object p1, p0, Lo1/k;->z:[Lo1/p;

    array-length p2, p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_0

    aget-object v2, p1, v1

    invoke-virtual {v2}, Lo1/p;->B()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return v0

    :cond_1
    iget-object v0, p0, Lo1/k;->D:Lj1/r0;

    invoke-interface {v0, p1, p2}, Lj1/r0;->g(J)Z

    move-result p1

    return p1
.end method

.method public h(J)V
    .locals 1

    iget-object v0, p0, Lo1/k;->D:Lj1/r0;

    invoke-interface {v0, p1, p2}, Lj1/r0;->h(J)V

    return-void
.end method

.method public n()J
    .locals 2

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    return-wide v0
.end method

.method public o()Lj1/z0;
    .locals 1

    iget-object v0, p0, Lo1/k;->y:Lj1/z0;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/z0;

    return-object v0
.end method

.method public p(Lj1/u$a;J)V
    .locals 0

    iput-object p1, p0, Lo1/k;->w:Lj1/u$a;

    iget-object p1, p0, Lo1/k;->g:Lp1/l;

    invoke-interface {p1, p0}, Lp1/l;->a(Lp1/l$b;)V

    invoke-direct {p0, p2, p3}, Lo1/k;->w(J)V

    return-void
.end method

.method public q()V
    .locals 4

    iget-object v0, p0, Lo1/k;->z:[Lo1/p;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3}, Lo1/p;->q()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public r(JZ)V
    .locals 4

    iget-object v0, p0, Lo1/k;->A:[Lo1/p;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3, p1, p2, p3}, Lo1/p;->r(JZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public s(J)J
    .locals 4

    iget-object v0, p0, Lo1/k;->A:[Lo1/p;

    array-length v1, v0

    if-lez v1, :cond_1

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, v1}, Lo1/p;->i0(JZ)Z

    move-result v0

    const/4 v1, 0x1

    :goto_0
    iget-object v2, p0, Lo1/k;->A:[Lo1/p;

    array-length v3, v2

    if-ge v1, v3, :cond_0

    aget-object v2, v2, v1

    invoke-virtual {v2, p1, p2, v0}, Lo1/p;->i0(JZ)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lo1/k;->p:Lo1/s;

    invoke-virtual {v0}, Lo1/s;->b()V

    :cond_1
    return-wide p1
.end method

.method public u([Lc2/t;[Z[Lj1/q0;[ZJ)J
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    array-length v3, v1

    new-array v3, v3, [I

    array-length v4, v1

    new-array v4, v4, [I

    const/4 v6, 0x0

    :goto_0
    array-length v7, v1

    if-ge v6, v7, :cond_3

    aget-object v7, v2, v6

    const/4 v8, -0x1

    if-nez v7, :cond_0

    const/4 v7, -0x1

    goto :goto_1

    :cond_0
    iget-object v7, v0, Lo1/k;->o:Ljava/util/IdentityHashMap;

    aget-object v9, v2, v6

    invoke-virtual {v7, v9}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    :goto_1
    aput v7, v3, v6

    aput v8, v4, v6

    aget-object v7, v1, v6

    if-eqz v7, :cond_2

    aget-object v7, v1, v6

    invoke-interface {v7}, Lc2/w;->c()Lj1/x0;

    move-result-object v7

    const/4 v9, 0x0

    :goto_2
    iget-object v10, v0, Lo1/k;->z:[Lo1/p;

    array-length v11, v10

    if-ge v9, v11, :cond_2

    aget-object v10, v10, v9

    invoke-virtual {v10}, Lo1/p;->o()Lj1/z0;

    move-result-object v10

    invoke-virtual {v10, v7}, Lj1/z0;->c(Lj1/x0;)I

    move-result v10

    if-eq v10, v8, :cond_1

    aput v9, v4, v6

    goto :goto_3

    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_2
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_3
    iget-object v6, v0, Lo1/k;->o:Ljava/util/IdentityHashMap;

    invoke-virtual {v6}, Ljava/util/IdentityHashMap;->clear()V

    array-length v6, v1

    new-array v7, v6, [Lj1/q0;

    array-length v8, v1

    new-array v8, v8, [Lj1/q0;

    array-length v9, v1

    new-array v14, v9, [Lc2/t;

    iget-object v9, v0, Lo1/k;->z:[Lo1/p;

    array-length v9, v9

    new-array v15, v9, [Lo1/p;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v17, 0x0

    :goto_4
    iget-object v9, v0, Lo1/k;->z:[Lo1/p;

    array-length v9, v9

    if-ge v13, v9, :cond_10

    const/4 v9, 0x0

    :goto_5
    array-length v10, v1

    if-ge v9, v10, :cond_6

    aget v10, v3, v9

    const/4 v11, 0x0

    if-ne v10, v13, :cond_4

    aget-object v10, v2, v9

    goto :goto_6

    :cond_4
    move-object v10, v11

    :goto_6
    aput-object v10, v8, v9

    aget v10, v4, v9

    if-ne v10, v13, :cond_5

    aget-object v11, v1, v9

    :cond_5
    aput-object v11, v14, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    :cond_6
    iget-object v9, v0, Lo1/k;->z:[Lo1/p;

    aget-object v11, v9, v13

    move-object v9, v11

    move-object v10, v14

    move-object v5, v11

    move-object/from16 v11, p2

    move v2, v12

    move-object v12, v8

    move/from16 v18, v6

    move v6, v13

    move-object/from16 v13, p4

    move/from16 v20, v2

    move-object/from16 v19, v14

    move-object v2, v15

    move-wide/from16 v14, p5

    move/from16 v16, v17

    invoke-virtual/range {v9 .. v16}, Lo1/p;->j0([Lc2/t;[Z[Lj1/q0;[ZJZ)Z

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    :goto_7
    array-length v12, v1

    const/4 v13, 0x1

    if-ge v10, v12, :cond_a

    aget-object v12, v8, v10

    aget v14, v4, v10

    if-ne v14, v6, :cond_7

    invoke-static {v12}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v12, v7, v10

    iget-object v11, v0, Lo1/k;->o:Ljava/util/IdentityHashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v12, v14}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v11, 0x1

    goto :goto_9

    :cond_7
    aget v14, v3, v10

    if-ne v14, v6, :cond_9

    if-nez v12, :cond_8

    goto :goto_8

    :cond_8
    const/4 v13, 0x0

    :goto_8
    invoke-static {v13}, Le2/a;->f(Z)V

    :cond_9
    :goto_9
    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    :cond_a
    if-eqz v11, :cond_e

    aput-object v5, v2, v20

    add-int/lit8 v12, v20, 0x1

    if-nez v20, :cond_c

    invoke-virtual {v5, v13}, Lo1/p;->m0(Z)V

    if-nez v9, :cond_b

    iget-object v9, v0, Lo1/k;->A:[Lo1/p;

    array-length v10, v9

    if-eqz v10, :cond_b

    const/4 v10, 0x0

    aget-object v9, v9, v10

    if-eq v5, v9, :cond_f

    :cond_b
    iget-object v5, v0, Lo1/k;->p:Lo1/s;

    invoke-virtual {v5}, Lo1/s;->b()V

    const/16 v17, 0x1

    goto :goto_b

    :cond_c
    iget v9, v0, Lo1/k;->C:I

    if-ge v6, v9, :cond_d

    goto :goto_a

    :cond_d
    const/4 v13, 0x0

    :goto_a
    invoke-virtual {v5, v13}, Lo1/p;->m0(Z)V

    goto :goto_b

    :cond_e
    move/from16 v12, v20

    :cond_f
    :goto_b
    add-int/lit8 v13, v6, 0x1

    move-object v15, v2

    move/from16 v6, v18

    move-object/from16 v14, v19

    move-object/from16 v2, p3

    goto/16 :goto_4

    :cond_10
    move-object v5, v2

    move-object v2, v15

    const/4 v9, 0x0

    invoke-static {v7, v9, v5, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v2, v12}, Le2/n0;->H0([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lo1/p;

    iput-object v1, v0, Lo1/k;->A:[Lo1/p;

    iget-object v2, v0, Lo1/k;->q:Lj1/i;

    invoke-interface {v2, v1}, Lj1/i;->a([Lj1/r0;)Lj1/r0;

    move-result-object v1

    iput-object v1, v0, Lo1/k;->D:Lj1/r0;

    return-wide p5
.end method
