.class Lo1/k$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo1/p$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lo1/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic f:Lo1/k;


# direct methods
.method private constructor <init>(Lo1/k;)V
    .locals 0

    iput-object p1, p0, Lo1/k$b;->f:Lo1/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lo1/k;Lo1/k$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lo1/k$b;-><init>(Lo1/k;)V

    return-void
.end method


# virtual methods
.method public a(Lo1/p;)V
    .locals 1

    iget-object p1, p0, Lo1/k$b;->f:Lo1/k;

    invoke-static {p1}, Lo1/k;->l(Lo1/k;)Lj1/u$a;

    move-result-object p1

    iget-object v0, p0, Lo1/k$b;->f:Lo1/k;

    invoke-interface {p1, v0}, Lj1/r0$a;->i(Lj1/r0;)V

    return-void
.end method

.method public b()V
    .locals 11

    iget-object v0, p0, Lo1/k$b;->f:Lo1/k;

    invoke-static {v0}, Lo1/k;->i(Lo1/k;)I

    move-result v0

    if-lez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lo1/k$b;->f:Lo1/k;

    invoke-static {v0}, Lo1/k;->j(Lo1/k;)[Lo1/p;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v5, v0, v3

    invoke-virtual {v5}, Lo1/p;->o()Lj1/z0;

    move-result-object v5

    iget v5, v5, Lj1/z0;->f:I

    add-int/2addr v4, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    new-array v0, v4, [Lj1/x0;

    iget-object v1, p0, Lo1/k$b;->f:Lo1/k;

    invoke-static {v1}, Lo1/k;->j(Lo1/k;)[Lo1/p;

    move-result-object v1

    array-length v3, v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_1
    if-ge v4, v3, :cond_3

    aget-object v6, v1, v4

    invoke-virtual {v6}, Lo1/p;->o()Lj1/z0;

    move-result-object v7

    iget v7, v7, Lj1/z0;->f:I

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v7, :cond_2

    add-int/lit8 v9, v5, 0x1

    invoke-virtual {v6}, Lo1/p;->o()Lj1/z0;

    move-result-object v10

    invoke-virtual {v10, v8}, Lj1/z0;->b(I)Lj1/x0;

    move-result-object v10

    aput-object v10, v0, v5

    add-int/lit8 v8, v8, 0x1

    move v5, v9

    goto :goto_2

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lo1/k$b;->f:Lo1/k;

    new-instance v2, Lj1/z0;

    invoke-direct {v2, v0}, Lj1/z0;-><init>([Lj1/x0;)V

    invoke-static {v1, v2}, Lo1/k;->k(Lo1/k;Lj1/z0;)Lj1/z0;

    iget-object v0, p0, Lo1/k$b;->f:Lo1/k;

    invoke-static {v0}, Lo1/k;->l(Lo1/k;)Lj1/u$a;

    move-result-object v0

    iget-object v1, p0, Lo1/k$b;->f:Lo1/k;

    invoke-interface {v0, v1}, Lj1/u$a;->e(Lj1/u;)V

    return-void
.end method

.method public bridge synthetic i(Lj1/r0;)V
    .locals 0

    check-cast p1, Lo1/p;

    invoke-virtual {p0, p1}, Lo1/k$b;->a(Lo1/p;)V

    return-void
.end method

.method public j(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lo1/k$b;->f:Lo1/k;

    invoke-static {v0}, Lo1/k;->m(Lo1/k;)Lp1/l;

    move-result-object v0

    invoke-interface {v0, p1}, Lp1/l;->i(Landroid/net/Uri;)V

    return-void
.end method
