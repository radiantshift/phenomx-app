.class public final Lb0/d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lx/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lx/b<",
        "Lb0/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lw/e;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lc0/y;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Ld0/d;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Le0/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lj5/a<",
            "Lw/e;",
            ">;",
            "Lj5/a<",
            "Lc0/y;",
            ">;",
            "Lj5/a<",
            "Ld0/d;",
            ">;",
            "Lj5/a<",
            "Le0/b;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb0/d;->a:Lj5/a;

    iput-object p2, p0, Lb0/d;->b:Lj5/a;

    iput-object p3, p0, Lb0/d;->c:Lj5/a;

    iput-object p4, p0, Lb0/d;->d:Lj5/a;

    iput-object p5, p0, Lb0/d;->e:Lj5/a;

    return-void
.end method

.method public static a(Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;)Lb0/d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lj5/a<",
            "Lw/e;",
            ">;",
            "Lj5/a<",
            "Lc0/y;",
            ">;",
            "Lj5/a<",
            "Ld0/d;",
            ">;",
            "Lj5/a<",
            "Le0/b;",
            ">;)",
            "Lb0/d;"
        }
    .end annotation

    new-instance v6, Lb0/d;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb0/d;-><init>(Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;)V

    return-object v6
.end method

.method public static c(Ljava/util/concurrent/Executor;Lw/e;Lc0/y;Ld0/d;Le0/b;)Lb0/c;
    .locals 7

    new-instance v6, Lb0/c;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lb0/c;-><init>(Ljava/util/concurrent/Executor;Lw/e;Lc0/y;Ld0/d;Le0/b;)V

    return-object v6
.end method


# virtual methods
.method public b()Lb0/c;
    .locals 5

    iget-object v0, p0, Lb0/d;->a:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lb0/d;->b:Lj5/a;

    invoke-interface {v1}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lw/e;

    iget-object v2, p0, Lb0/d;->c:Lj5/a;

    invoke-interface {v2}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lc0/y;

    iget-object v3, p0, Lb0/d;->d:Lj5/a;

    invoke-interface {v3}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ld0/d;

    iget-object v4, p0, Lb0/d;->e:Lj5/a;

    invoke-interface {v4}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Le0/b;

    invoke-static {v0, v1, v2, v3, v4}, Lb0/d;->c(Ljava/util/concurrent/Executor;Lw/e;Lc0/y;Ld0/d;Le0/b;)Lb0/c;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lb0/d;->b()Lb0/c;

    move-result-object v0

    return-object v0
.end method
