.class public final Lb0/g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lx/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lx/b<",
        "Lc0/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lf0/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lj5/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Lf0/a;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb0/g;->a:Lj5/a;

    return-void
.end method

.method public static a(Lf0/a;)Lc0/g;
    .locals 1

    invoke-static {p0}, Lb0/f;->a(Lf0/a;)Lc0/g;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Lx/d;->c(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc0/g;

    return-object p0
.end method

.method public static b(Lj5/a;)Lb0/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Lf0/a;",
            ">;)",
            "Lb0/g;"
        }
    .end annotation

    new-instance v0, Lb0/g;

    invoke-direct {v0, p0}, Lb0/g;-><init>(Lj5/a;)V

    return-object v0
.end method


# virtual methods
.method public c()Lc0/g;
    .locals 1

    iget-object v0, p0, Lb0/g;->a:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf0/a;

    invoke-static {v0}, Lb0/g;->a(Lf0/a;)Lc0/g;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lb0/g;->c()Lc0/g;

    move-result-object v0

    return-object v0
.end method
