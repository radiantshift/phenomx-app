.class public final Lb0/i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lx/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lx/b<",
        "Lc0/y;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Ld0/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lc0/g;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lf0/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lj5/a;Lj5/a;Lj5/a;Lj5/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Landroid/content/Context;",
            ">;",
            "Lj5/a<",
            "Ld0/d;",
            ">;",
            "Lj5/a<",
            "Lc0/g;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb0/i;->a:Lj5/a;

    iput-object p2, p0, Lb0/i;->b:Lj5/a;

    iput-object p3, p0, Lb0/i;->c:Lj5/a;

    iput-object p4, p0, Lb0/i;->d:Lj5/a;

    return-void
.end method

.method public static a(Lj5/a;Lj5/a;Lj5/a;Lj5/a;)Lb0/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Landroid/content/Context;",
            ">;",
            "Lj5/a<",
            "Ld0/d;",
            ">;",
            "Lj5/a<",
            "Lc0/g;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;)",
            "Lb0/i;"
        }
    .end annotation

    new-instance v0, Lb0/i;

    invoke-direct {v0, p0, p1, p2, p3}, Lb0/i;-><init>(Lj5/a;Lj5/a;Lj5/a;Lj5/a;)V

    return-object v0
.end method

.method public static c(Landroid/content/Context;Ld0/d;Lc0/g;Lf0/a;)Lc0/y;
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lb0/h;->a(Landroid/content/Context;Ld0/d;Lc0/g;Lf0/a;)Lc0/y;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Lx/d;->c(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc0/y;

    return-object p0
.end method


# virtual methods
.method public b()Lc0/y;
    .locals 4

    iget-object v0, p0, Lb0/i;->a:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lb0/i;->b:Lj5/a;

    invoke-interface {v1}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ld0/d;

    iget-object v2, p0, Lb0/i;->c:Lj5/a;

    invoke-interface {v2}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lc0/g;

    iget-object v3, p0, Lb0/i;->d:Lj5/a;

    invoke-interface {v3}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf0/a;

    invoke-static {v0, v1, v2, v3}, Lb0/i;->c(Landroid/content/Context;Ld0/d;Lc0/g;Lf0/a;)Lc0/y;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lb0/i;->b()Lc0/y;

    move-result-object v0

    return-object v0
.end method
