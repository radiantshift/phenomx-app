.class final Lv/a$f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lv/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Ly/e;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lv/a$f;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lv/a$f;

    invoke-direct {v0}, Lv/a$f;-><init>()V

    sput-object v0, Lv/a$f;->a:Lv/a$f;

    const-string v0, "currentCacheSizeBytes"

    invoke-static {v0}, Lo3/c;->a(Ljava/lang/String;)Lo3/c$b;

    move-result-object v0

    invoke-static {}, Lr3/a;->b()Lr3/a;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lr3/a;->c(I)Lr3/a;

    move-result-object v1

    invoke-virtual {v1}, Lr3/a;->a()Lr3/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo3/c$b;->b(Ljava/lang/annotation/Annotation;)Lo3/c$b;

    move-result-object v0

    invoke-virtual {v0}, Lo3/c$b;->a()Lo3/c;

    move-result-object v0

    sput-object v0, Lv/a$f;->b:Lo3/c;

    const-string v0, "maxCacheSizeBytes"

    invoke-static {v0}, Lo3/c;->a(Ljava/lang/String;)Lo3/c$b;

    move-result-object v0

    invoke-static {}, Lr3/a;->b()Lr3/a;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lr3/a;->c(I)Lr3/a;

    move-result-object v1

    invoke-virtual {v1}, Lr3/a;->a()Lr3/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo3/c$b;->b(Ljava/lang/annotation/Annotation;)Lo3/c$b;

    move-result-object v0

    invoke-virtual {v0}, Lo3/c$b;->a()Lo3/c;

    move-result-object v0

    sput-object v0, Lv/a$f;->c:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ly/e;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lv/a$f;->b(Ly/e;Lo3/e;)V

    return-void
.end method

.method public b(Ly/e;Lo3/e;)V
    .locals 3

    sget-object v0, Lv/a$f;->b:Lo3/c;

    invoke-virtual {p1}, Ly/e;->a()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lv/a$f;->c:Lo3/c;

    invoke-virtual {p1}, Ly/e;->b()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    return-void
.end method
