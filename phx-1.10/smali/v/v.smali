.class public final Lv/v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lx/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lx/b<",
        "Lv/t;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lf0/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lf0/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lb0/e;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lc0/s;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lc0/w;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Lb0/e;",
            ">;",
            "Lj5/a<",
            "Lc0/s;",
            ">;",
            "Lj5/a<",
            "Lc0/w;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lv/v;->a:Lj5/a;

    iput-object p2, p0, Lv/v;->b:Lj5/a;

    iput-object p3, p0, Lv/v;->c:Lj5/a;

    iput-object p4, p0, Lv/v;->d:Lj5/a;

    iput-object p5, p0, Lv/v;->e:Lj5/a;

    return-void
.end method

.method public static a(Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;)Lv/v;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Lb0/e;",
            ">;",
            "Lj5/a<",
            "Lc0/s;",
            ">;",
            "Lj5/a<",
            "Lc0/w;",
            ">;)",
            "Lv/v;"
        }
    .end annotation

    new-instance v6, Lv/v;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lv/v;-><init>(Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;)V

    return-object v6
.end method

.method public static c(Lf0/a;Lf0/a;Lb0/e;Lc0/s;Lc0/w;)Lv/t;
    .locals 7

    new-instance v6, Lv/t;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lv/t;-><init>(Lf0/a;Lf0/a;Lb0/e;Lc0/s;Lc0/w;)V

    return-object v6
.end method


# virtual methods
.method public b()Lv/t;
    .locals 5

    iget-object v0, p0, Lv/v;->a:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf0/a;

    iget-object v1, p0, Lv/v;->b:Lj5/a;

    invoke-interface {v1}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf0/a;

    iget-object v2, p0, Lv/v;->c:Lj5/a;

    invoke-interface {v2}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lb0/e;

    iget-object v3, p0, Lv/v;->d:Lj5/a;

    invoke-interface {v3}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lc0/s;

    iget-object v4, p0, Lv/v;->e:Lj5/a;

    invoke-interface {v4}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lc0/w;

    invoke-static {v0, v1, v2, v3, v4}, Lv/v;->c(Lf0/a;Lf0/a;Lb0/e;Lc0/s;Lc0/w;)Lv/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lv/v;->b()Lv/t;

    move-result-object v0

    return-object v0
.end method
