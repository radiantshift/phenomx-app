.class abstract Lv/o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lv/o$a;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lv/o$a;
    .locals 1

    new-instance v0, Lv/c$b;

    invoke-direct {v0}, Lv/c$b;-><init>()V

    return-object v0
.end method


# virtual methods
.method public abstract b()Lt/b;
.end method

.method abstract c()Lt/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lt/c<",
            "*>;"
        }
    .end annotation
.end method

.method public d()[B
    .locals 2

    invoke-virtual {p0}, Lv/o;->e()Lt/e;

    move-result-object v0

    invoke-virtual {p0}, Lv/o;->c()Lt/c;

    move-result-object v1

    invoke-virtual {v1}, Lt/c;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lt/e;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method abstract e()Lt/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lt/e<",
            "*[B>;"
        }
    .end annotation
.end method

.method public abstract f()Lv/p;
.end method

.method public abstract g()Ljava/lang/String;
.end method
