.class public Lv/t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lv/s;


# static fields
.field private static volatile e:Lv/u;


# instance fields
.field private final a:Lf0/a;

.field private final b:Lf0/a;

.field private final c:Lb0/e;

.field private final d:Lc0/s;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lf0/a;Lf0/a;Lb0/e;Lc0/s;Lc0/w;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lv/t;->a:Lf0/a;

    iput-object p2, p0, Lv/t;->b:Lf0/a;

    iput-object p3, p0, Lv/t;->c:Lb0/e;

    iput-object p4, p0, Lv/t;->d:Lc0/s;

    invoke-virtual {p5}, Lc0/w;->c()V

    return-void
.end method

.method private b(Lv/o;)Lv/i;
    .locals 4

    invoke-static {}, Lv/i;->a()Lv/i$a;

    move-result-object v0

    iget-object v1, p0, Lv/t;->a:Lf0/a;

    invoke-interface {v1}, Lf0/a;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lv/i$a;->i(J)Lv/i$a;

    move-result-object v0

    iget-object v1, p0, Lv/t;->b:Lf0/a;

    invoke-interface {v1}, Lf0/a;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lv/i$a;->k(J)Lv/i$a;

    move-result-object v0

    invoke-virtual {p1}, Lv/o;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lv/i$a;->j(Ljava/lang/String;)Lv/i$a;

    move-result-object v0

    new-instance v1, Lv/h;

    invoke-virtual {p1}, Lv/o;->b()Lt/b;

    move-result-object v2

    invoke-virtual {p1}, Lv/o;->d()[B

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lv/h;-><init>(Lt/b;[B)V

    invoke-virtual {v0, v1}, Lv/i$a;->h(Lv/h;)Lv/i$a;

    move-result-object v0

    invoke-virtual {p1}, Lv/o;->c()Lt/c;

    move-result-object p1

    invoke-virtual {p1}, Lt/c;->a()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lv/i$a;->g(Ljava/lang/Integer;)Lv/i$a;

    move-result-object p1

    invoke-virtual {p1}, Lv/i$a;->d()Lv/i;

    move-result-object p1

    return-object p1
.end method

.method public static c()Lv/t;
    .locals 2

    sget-object v0, Lv/t;->e:Lv/u;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lv/u;->b()Lv/t;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not initialized!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static d(Lv/f;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv/f;",
            ")",
            "Ljava/util/Set<",
            "Lt/b;",
            ">;"
        }
    .end annotation

    instance-of v0, p0, Lv/g;

    if-eqz v0, :cond_0

    check-cast p0, Lv/g;

    invoke-interface {p0}, Lv/g;->a()Ljava/util/Set;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p0

    return-object p0

    :cond_0
    const-string p0, "proto"

    invoke-static {p0}, Lt/b;->b(Ljava/lang/String;)Lt/b;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method public static f(Landroid/content/Context;)V
    .locals 2

    sget-object v0, Lv/t;->e:Lv/u;

    if-nez v0, :cond_1

    const-class v0, Lv/t;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lv/t;->e:Lv/u;

    if-nez v1, :cond_0

    invoke-static {}, Lv/e;->c()Lv/u$a;

    move-result-object v1

    invoke-interface {v1, p0}, Lv/u$a;->b(Landroid/content/Context;)Lv/u$a;

    move-result-object p0

    invoke-interface {p0}, Lv/u$a;->a()Lv/u;

    move-result-object p0

    sput-object p0, Lv/t;->e:Lv/u;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public a(Lv/o;Lt/h;)V
    .locals 3

    iget-object v0, p0, Lv/t;->c:Lb0/e;

    invoke-virtual {p1}, Lv/o;->f()Lv/p;

    move-result-object v1

    invoke-virtual {p1}, Lv/o;->c()Lt/c;

    move-result-object v2

    invoke-virtual {v2}, Lt/c;->c()Lt/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lv/p;->f(Lt/d;)Lv/p;

    move-result-object v1

    invoke-direct {p0, p1}, Lv/t;->b(Lv/o;)Lv/i;

    move-result-object p1

    invoke-interface {v0, v1, p1, p2}, Lb0/e;->a(Lv/p;Lv/i;Lt/h;)V

    return-void
.end method

.method public e()Lc0/s;
    .locals 1

    iget-object v0, p0, Lv/t;->d:Lc0/s;

    return-object v0
.end method

.method public g(Lv/f;)Lt/g;
    .locals 4

    new-instance v0, Lv/q;

    invoke-static {p1}, Lv/t;->d(Lv/f;)Ljava/util/Set;

    move-result-object v1

    invoke-static {}, Lv/p;->a()Lv/p$a;

    move-result-object v2

    invoke-interface {p1}, Lv/f;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lv/p$a;->b(Ljava/lang/String;)Lv/p$a;

    move-result-object v2

    invoke-interface {p1}, Lv/f;->b()[B

    move-result-object p1

    invoke-virtual {v2, p1}, Lv/p$a;->c([B)Lv/p$a;

    move-result-object p1

    invoke-virtual {p1}, Lv/p$a;->a()Lv/p;

    move-result-object p1

    invoke-direct {v0, v1, p1, p0}, Lv/q;-><init>(Ljava/util/Set;Lv/p;Lv/s;)V

    return-object v0
.end method
