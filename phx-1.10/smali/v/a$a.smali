.class final Lv/a$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lv/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Ly/a;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lv/a$a;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;

.field private static final d:Lo3/c;

.field private static final e:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lv/a$a;

    invoke-direct {v0}, Lv/a$a;-><init>()V

    sput-object v0, Lv/a$a;->a:Lv/a$a;

    const-string v0, "window"

    invoke-static {v0}, Lo3/c;->a(Ljava/lang/String;)Lo3/c$b;

    move-result-object v0

    invoke-static {}, Lr3/a;->b()Lr3/a;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lr3/a;->c(I)Lr3/a;

    move-result-object v1

    invoke-virtual {v1}, Lr3/a;->a()Lr3/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo3/c$b;->b(Ljava/lang/annotation/Annotation;)Lo3/c$b;

    move-result-object v0

    invoke-virtual {v0}, Lo3/c$b;->a()Lo3/c;

    move-result-object v0

    sput-object v0, Lv/a$a;->b:Lo3/c;

    const-string v0, "logSourceMetrics"

    invoke-static {v0}, Lo3/c;->a(Ljava/lang/String;)Lo3/c$b;

    move-result-object v0

    invoke-static {}, Lr3/a;->b()Lr3/a;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lr3/a;->c(I)Lr3/a;

    move-result-object v1

    invoke-virtual {v1}, Lr3/a;->a()Lr3/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo3/c$b;->b(Ljava/lang/annotation/Annotation;)Lo3/c$b;

    move-result-object v0

    invoke-virtual {v0}, Lo3/c$b;->a()Lo3/c;

    move-result-object v0

    sput-object v0, Lv/a$a;->c:Lo3/c;

    const-string v0, "globalMetrics"

    invoke-static {v0}, Lo3/c;->a(Ljava/lang/String;)Lo3/c$b;

    move-result-object v0

    invoke-static {}, Lr3/a;->b()Lr3/a;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lr3/a;->c(I)Lr3/a;

    move-result-object v1

    invoke-virtual {v1}, Lr3/a;->a()Lr3/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo3/c$b;->b(Ljava/lang/annotation/Annotation;)Lo3/c$b;

    move-result-object v0

    invoke-virtual {v0}, Lo3/c$b;->a()Lo3/c;

    move-result-object v0

    sput-object v0, Lv/a$a;->d:Lo3/c;

    const-string v0, "appNamespace"

    invoke-static {v0}, Lo3/c;->a(Ljava/lang/String;)Lo3/c$b;

    move-result-object v0

    invoke-static {}, Lr3/a;->b()Lr3/a;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lr3/a;->c(I)Lr3/a;

    move-result-object v1

    invoke-virtual {v1}, Lr3/a;->a()Lr3/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo3/c$b;->b(Ljava/lang/annotation/Annotation;)Lo3/c$b;

    move-result-object v0

    invoke-virtual {v0}, Lo3/c$b;->a()Lo3/c;

    move-result-object v0

    sput-object v0, Lv/a$a;->e:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ly/a;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lv/a$a;->b(Ly/a;Lo3/e;)V

    return-void
.end method

.method public b(Ly/a;Lo3/e;)V
    .locals 2

    sget-object v0, Lv/a$a;->b:Lo3/c;

    invoke-virtual {p1}, Ly/a;->d()Ly/f;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lv/a$a;->c:Lo3/c;

    invoke-virtual {p1}, Ly/a;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lv/a$a;->d:Lo3/c;

    invoke-virtual {p1}, Ly/a;->b()Ly/b;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lv/a$a;->e:Lo3/c;

    invoke-virtual {p1}, Ly/a;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    return-void
.end method
