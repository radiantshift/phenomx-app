.class final Lv/r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lt/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lt/f<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lv/p;

.field private final b:Ljava/lang/String;

.field private final c:Lt/b;

.field private final d:Lt/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lt/e<",
            "TT;[B>;"
        }
    .end annotation
.end field

.field private final e:Lv/s;


# direct methods
.method constructor <init>(Lv/p;Ljava/lang/String;Lt/b;Lt/e;Lv/s;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv/p;",
            "Ljava/lang/String;",
            "Lt/b;",
            "Lt/e<",
            "TT;[B>;",
            "Lv/s;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lv/r;->a:Lv/p;

    iput-object p2, p0, Lv/r;->b:Ljava/lang/String;

    iput-object p3, p0, Lv/r;->c:Lt/b;

    iput-object p4, p0, Lv/r;->d:Lt/e;

    iput-object p5, p0, Lv/r;->e:Lv/s;

    return-void
.end method


# virtual methods
.method public a(Lt/c;Lt/h;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lt/c<",
            "TT;>;",
            "Lt/h;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lv/r;->e:Lv/s;

    invoke-static {}, Lv/o;->a()Lv/o$a;

    move-result-object v1

    iget-object v2, p0, Lv/r;->a:Lv/p;

    invoke-virtual {v1, v2}, Lv/o$a;->e(Lv/p;)Lv/o$a;

    move-result-object v1

    invoke-virtual {v1, p1}, Lv/o$a;->c(Lt/c;)Lv/o$a;

    move-result-object p1

    iget-object v1, p0, Lv/r;->b:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lv/o$a;->f(Ljava/lang/String;)Lv/o$a;

    move-result-object p1

    iget-object v1, p0, Lv/r;->d:Lt/e;

    invoke-virtual {p1, v1}, Lv/o$a;->d(Lt/e;)Lv/o$a;

    move-result-object p1

    iget-object v1, p0, Lv/r;->c:Lt/b;

    invoke-virtual {p1, v1}, Lv/o$a;->b(Lt/b;)Lv/o$a;

    move-result-object p1

    invoke-virtual {p1}, Lv/o$a;->a()Lv/o;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lv/s;->a(Lv/o;Lt/h;)V

    return-void
.end method

.method b()Lv/p;
    .locals 1

    iget-object v0, p0, Lv/r;->a:Lv/p;

    return-object v0
.end method
