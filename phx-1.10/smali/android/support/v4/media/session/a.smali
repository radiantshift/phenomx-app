.class public interface abstract Landroid/support/v4/media/session/a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/media/session/a$a;
    }
.end annotation


# virtual methods
.method public abstract i(I)V
.end method

.method public abstract j(Z)V
.end method

.method public abstract k(Ljava/lang/CharSequence;)V
.end method

.method public abstract l()V
.end method

.method public abstract m(Landroid/support/v4/media/MediaMetadataCompat;)V
.end method

.method public abstract n(I)V
.end method

.method public abstract o()V
.end method

.method public abstract p(Landroid/os/Bundle;)V
.end method

.method public abstract q(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract r(Z)V
.end method

.method public abstract s(Landroid/support/v4/media/session/PlaybackStateCompat;)V
.end method

.method public abstract t(Ljava/lang/String;Landroid/os/Bundle;)V
.end method

.method public abstract u(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
.end method
