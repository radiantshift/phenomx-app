.class public final Ll1/i$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lj1/q0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ll1/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public final f:Ll1/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll1/i<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final g:Lj1/p0;

.field private final h:I

.field private i:Z

.field final synthetic j:Ll1/i;


# direct methods
.method public constructor <init>(Ll1/i;Ll1/i;Lj1/p0;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll1/i<",
            "TT;>;",
            "Lj1/p0;",
            "I)V"
        }
    .end annotation

    iput-object p1, p0, Ll1/i$a;->j:Ll1/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Ll1/i$a;->f:Ll1/i;

    iput-object p3, p0, Ll1/i$a;->g:Lj1/p0;

    iput p4, p0, Ll1/i$a;->h:I

    return-void
.end method

.method private a()V
    .locals 8

    iget-boolean v0, p0, Ll1/i$a;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ll1/i$a;->j:Ll1/i;

    invoke-static {v0}, Ll1/i;->A(Ll1/i;)Lj1/e0$a;

    move-result-object v1

    iget-object v0, p0, Ll1/i$a;->j:Ll1/i;

    invoke-static {v0}, Ll1/i;->x(Ll1/i;)[I

    move-result-object v0

    iget v2, p0, Ll1/i$a;->h:I

    aget v2, v0, v2

    iget-object v0, p0, Ll1/i$a;->j:Ll1/i;

    invoke-static {v0}, Ll1/i;->y(Ll1/i;)[Lh0/r1;

    move-result-object v0

    iget v3, p0, Ll1/i$a;->h:I

    aget-object v3, v0, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Ll1/i$a;->j:Ll1/i;

    invoke-static {v0}, Ll1/i;->z(Ll1/i;)J

    move-result-wide v6

    invoke-virtual/range {v1 .. v7}, Lj1/e0$a;->i(ILh0/r1;ILjava/lang/Object;J)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ll1/i$a;->i:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public b()V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 3

    iget-object v0, p0, Ll1/i$a;->j:Ll1/i;

    invoke-static {v0}, Ll1/i;->w(Ll1/i;)[Z

    move-result-object v0

    iget v1, p0, Ll1/i$a;->h:I

    aget-boolean v0, v0, v1

    invoke-static {v0}, Le2/a;->f(Z)V

    iget-object v0, p0, Ll1/i$a;->j:Ll1/i;

    invoke-static {v0}, Ll1/i;->w(Ll1/i;)[Z

    move-result-object v0

    iget v1, p0, Ll1/i$a;->h:I

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    return-void
.end method

.method public e(Lh0/s1;Lk0/g;I)I
    .locals 3

    iget-object v0, p0, Ll1/i$a;->j:Ll1/i;

    invoke-virtual {v0}, Ll1/i;->I()Z

    move-result v0

    const/4 v1, -0x3

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Ll1/i$a;->j:Ll1/i;

    invoke-static {v0}, Ll1/i;->v(Ll1/i;)Ll1/a;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ll1/i$a;->j:Ll1/i;

    invoke-static {v0}, Ll1/i;->v(Ll1/i;)Ll1/a;

    move-result-object v0

    iget v2, p0, Ll1/i$a;->h:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ll1/a;->i(I)I

    move-result v0

    iget-object v2, p0, Ll1/i$a;->g:Lj1/p0;

    invoke-virtual {v2}, Lj1/p0;->C()I

    move-result v2

    if-gt v0, v2, :cond_1

    return v1

    :cond_1
    invoke-direct {p0}, Ll1/i$a;->a()V

    iget-object v0, p0, Ll1/i$a;->g:Lj1/p0;

    iget-object v1, p0, Ll1/i$a;->j:Ll1/i;

    iget-boolean v1, v1, Ll1/i;->B:Z

    invoke-virtual {v0, p1, p2, p3, v1}, Lj1/p0;->S(Lh0/s1;Lk0/g;IZ)I

    move-result p1

    return p1
.end method

.method public i(J)I
    .locals 2

    iget-object v0, p0, Ll1/i$a;->j:Ll1/i;

    invoke-virtual {v0}, Ll1/i;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-object v0, p0, Ll1/i$a;->g:Lj1/p0;

    iget-object v1, p0, Ll1/i$a;->j:Ll1/i;

    iget-boolean v1, v1, Ll1/i;->B:Z

    invoke-virtual {v0, p1, p2, v1}, Lj1/p0;->E(JZ)I

    move-result p1

    iget-object p2, p0, Ll1/i$a;->j:Ll1/i;

    invoke-static {p2}, Ll1/i;->v(Ll1/i;)Ll1/a;

    move-result-object p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Ll1/i$a;->j:Ll1/i;

    invoke-static {p2}, Ll1/i;->v(Ll1/i;)Ll1/a;

    move-result-object p2

    iget v0, p0, Ll1/i$a;->h:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Ll1/a;->i(I)I

    move-result p2

    iget-object v0, p0, Ll1/i$a;->g:Lj1/p0;

    invoke-virtual {v0}, Lj1/p0;->C()I

    move-result v0

    sub-int/2addr p2, v0

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    :cond_1
    iget-object p2, p0, Ll1/i$a;->g:Lj1/p0;

    invoke-virtual {p2, p1}, Lj1/p0;->e0(I)V

    if-lez p1, :cond_2

    invoke-direct {p0}, Ll1/i$a;->a()V

    :cond_2
    return p1
.end method

.method public k()Z
    .locals 2

    iget-object v0, p0, Ll1/i$a;->j:Ll1/i;

    invoke-virtual {v0}, Ll1/i;->I()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ll1/i$a;->g:Lj1/p0;

    iget-object v1, p0, Ll1/i$a;->j:Ll1/i;

    iget-boolean v1, v1, Ll1/i;->B:Z

    invoke-virtual {v0, v1}, Lj1/p0;->K(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
