.class final Ll1/e$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lm0/e0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ll1/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Lh0/r1;

.field private final d:Lm0/k;

.field public e:Lh0/r1;

.field private f:Lm0/e0;

.field private g:J


# direct methods
.method public constructor <init>(IILh0/r1;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Ll1/e$a;->a:I

    iput p2, p0, Ll1/e$a;->b:I

    iput-object p3, p0, Ll1/e$a;->c:Lh0/r1;

    new-instance p1, Lm0/k;

    invoke-direct {p1}, Lm0/k;-><init>()V

    iput-object p1, p0, Ll1/e$a;->d:Lm0/k;

    return-void
.end method


# virtual methods
.method public synthetic a(Le2/a0;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lm0/d0;->b(Lm0/e0;Le2/a0;I)V

    return-void
.end method

.method public b(Ld2/i;IZI)I
    .locals 0

    iget-object p4, p0, Ll1/e$a;->f:Lm0/e0;

    invoke-static {p4}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lm0/e0;

    invoke-interface {p4, p1, p2, p3}, Lm0/e0;->d(Ld2/i;IZ)I

    move-result p1

    return p1
.end method

.method public c(Le2/a0;II)V
    .locals 0

    iget-object p3, p0, Ll1/e$a;->f:Lm0/e0;

    invoke-static {p3}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lm0/e0;

    invoke-interface {p3, p1, p2}, Lm0/e0;->a(Le2/a0;I)V

    return-void
.end method

.method public synthetic d(Ld2/i;IZ)I
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lm0/d0;->a(Lm0/e0;Ld2/i;IZ)I

    move-result p1

    return p1
.end method

.method public e(Lh0/r1;)V
    .locals 1

    iget-object v0, p0, Ll1/e$a;->c:Lh0/r1;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lh0/r1;->j(Lh0/r1;)Lh0/r1;

    move-result-object p1

    :cond_0
    iput-object p1, p0, Ll1/e$a;->e:Lh0/r1;

    iget-object p1, p0, Ll1/e$a;->f:Lm0/e0;

    invoke-static {p1}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lm0/e0;

    iget-object v0, p0, Ll1/e$a;->e:Lh0/r1;

    invoke-interface {p1, v0}, Lm0/e0;->e(Lh0/r1;)V

    return-void
.end method

.method public f(JIIILm0/e0$a;)V
    .locals 8

    iget-wide v0, p0, Ll1/e$a;->g:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    iget-object v0, p0, Ll1/e$a;->d:Lm0/k;

    iput-object v0, p0, Ll1/e$a;->f:Lm0/e0;

    :cond_0
    iget-object v0, p0, Ll1/e$a;->f:Lm0/e0;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lm0/e0;

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-interface/range {v1 .. v7}, Lm0/e0;->f(JIIILm0/e0$a;)V

    return-void
.end method

.method public g(Ll1/g$b;J)V
    .locals 0

    if-nez p1, :cond_0

    iget-object p1, p0, Ll1/e$a;->d:Lm0/k;

    iput-object p1, p0, Ll1/e$a;->f:Lm0/e0;

    return-void

    :cond_0
    iput-wide p2, p0, Ll1/e$a;->g:J

    iget p2, p0, Ll1/e$a;->a:I

    iget p3, p0, Ll1/e$a;->b:I

    invoke-interface {p1, p2, p3}, Ll1/g$b;->e(II)Lm0/e0;

    move-result-object p1

    iput-object p1, p0, Ll1/e$a;->f:Lm0/e0;

    iget-object p2, p0, Ll1/e$a;->e:Lh0/r1;

    if-eqz p2, :cond_1

    invoke-interface {p1, p2}, Lm0/e0;->e(Lh0/r1;)V

    :cond_1
    return-void
.end method
