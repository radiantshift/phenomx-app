.class public final synthetic Lh5/m;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static synthetic a(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lh5/m;->r(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic b(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lh5/m;->v(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic c(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lh5/m;->u(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic d(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lh5/m;->n(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic e(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lh5/m;->w(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic f(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lh5/m;->s(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic g(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lh5/m;->t(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic h(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lh5/m;->m(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic i(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lh5/m;->o(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic j(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lh5/m;->q(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic k(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lh5/m;->p(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static l()Lz4/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lz4/i<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lh5/n$b;->d:Lh5/n$b;

    return-object v0
.end method

.method public static synthetic m(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 1

    .line 1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-interface {p0}, Lh5/n$a;->a()V

    const/4 p0, 0x0

    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Lh5/n;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object p1

    :goto_0
    invoke-interface {p2, p1}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic n(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh5/n$c;

    :try_start_0
    invoke-interface {p0, p1}, Lh5/n$a;->g(Lh5/n$c;)Lh5/n$i;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Lh5/n;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    invoke-interface {p2, v0}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic o(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh5/n$f;

    :try_start_0
    invoke-interface {p0, p1}, Lh5/n$a;->h(Lh5/n$f;)V

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Lh5/n;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    invoke-interface {p2, v0}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic p(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh5/n$i;

    :try_start_0
    invoke-interface {p0, p1}, Lh5/n$a;->e(Lh5/n$i;)V

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Lh5/n;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    invoke-interface {p2, v0}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic q(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh5/n$e;

    :try_start_0
    invoke-interface {p0, p1}, Lh5/n$a;->b(Lh5/n$e;)V

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Lh5/n;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    invoke-interface {p2, v0}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic r(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh5/n$j;

    :try_start_0
    invoke-interface {p0, p1}, Lh5/n$a;->m(Lh5/n$j;)V

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Lh5/n;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    invoke-interface {p2, v0}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic s(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh5/n$g;

    :try_start_0
    invoke-interface {p0, p1}, Lh5/n$a;->j(Lh5/n$g;)V

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Lh5/n;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    invoke-interface {p2, v0}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic t(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh5/n$i;

    :try_start_0
    invoke-interface {p0, p1}, Lh5/n$a;->c(Lh5/n$i;)V

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Lh5/n;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    invoke-interface {p2, v0}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic u(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh5/n$i;

    :try_start_0
    invoke-interface {p0, p1}, Lh5/n$a;->f(Lh5/n$i;)Lh5/n$h;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Lh5/n;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    invoke-interface {p2, v0}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic v(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh5/n$h;

    :try_start_0
    invoke-interface {p0, p1}, Lh5/n$a;->i(Lh5/n$h;)V

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Lh5/n;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    invoke-interface {p2, v0}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic w(Lh5/n$a;Ljava/lang/Object;Lz4/a$e;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh5/n$i;

    :try_start_0
    invoke-interface {p0, p1}, Lh5/n$a;->l(Lh5/n$i;)V

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Lh5/n;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    invoke-interface {p2, v0}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static x(Lz4/c;Lh5/n$a;)V
    .locals 4

    .line 1
    new-instance v0, Lz4/a;

    invoke-static {}, Lh5/m;->l()Lz4/i;

    move-result-object v1

    const-string v2, "dev.flutter.pigeon.AndroidVideoPlayerApi.initialize"

    invoke-direct {v0, p0, v2, v1}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;)V

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    new-instance v2, Lh5/j;

    invoke-direct {v2, p1}, Lh5/j;-><init>(Lh5/n$a;)V

    invoke-virtual {v0, v2}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v1}, Lz4/a;->e(Lz4/a$d;)V

    :goto_0
    new-instance v0, Lz4/a;

    invoke-static {}, Lh5/m;->l()Lz4/i;

    move-result-object v2

    const-string v3, "dev.flutter.pigeon.AndroidVideoPlayerApi.create"

    invoke-direct {v0, p0, v3, v2}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;)V

    if-eqz p1, :cond_1

    new-instance v2, Lh5/f;

    invoke-direct {v2, p1}, Lh5/f;-><init>(Lh5/n$a;)V

    invoke-virtual {v0, v2}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v1}, Lz4/a;->e(Lz4/a$d;)V

    :goto_1
    new-instance v0, Lz4/a;

    invoke-static {}, Lh5/m;->l()Lz4/i;

    move-result-object v2

    const-string v3, "dev.flutter.pigeon.AndroidVideoPlayerApi.dispose"

    invoke-direct {v0, p0, v3, v2}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;)V

    if-eqz p1, :cond_2

    new-instance v2, Lh5/c;

    invoke-direct {v2, p1}, Lh5/c;-><init>(Lh5/n$a;)V

    invoke-virtual {v0, v2}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v0, v1}, Lz4/a;->e(Lz4/a$d;)V

    :goto_2
    new-instance v0, Lz4/a;

    invoke-static {}, Lh5/m;->l()Lz4/i;

    move-result-object v2

    const-string v3, "dev.flutter.pigeon.AndroidVideoPlayerApi.setLooping"

    invoke-direct {v0, p0, v3, v2}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;)V

    if-eqz p1, :cond_3

    new-instance v2, Lh5/l;

    invoke-direct {v2, p1}, Lh5/l;-><init>(Lh5/n$a;)V

    invoke-virtual {v0, v2}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_3

    :cond_3
    invoke-virtual {v0, v1}, Lz4/a;->e(Lz4/a$d;)V

    :goto_3
    new-instance v0, Lz4/a;

    invoke-static {}, Lh5/m;->l()Lz4/i;

    move-result-object v2

    const-string v3, "dev.flutter.pigeon.AndroidVideoPlayerApi.setVolume"

    invoke-direct {v0, p0, v3, v2}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;)V

    if-eqz p1, :cond_4

    new-instance v2, Lh5/b;

    invoke-direct {v2, p1}, Lh5/b;-><init>(Lh5/n$a;)V

    invoke-virtual {v0, v2}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_4

    :cond_4
    invoke-virtual {v0, v1}, Lz4/a;->e(Lz4/a$d;)V

    :goto_4
    new-instance v0, Lz4/a;

    invoke-static {}, Lh5/m;->l()Lz4/i;

    move-result-object v2

    const-string v3, "dev.flutter.pigeon.AndroidVideoPlayerApi.setPlaybackSpeed"

    invoke-direct {v0, p0, v3, v2}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;)V

    if-eqz p1, :cond_5

    new-instance v2, Lh5/h;

    invoke-direct {v2, p1}, Lh5/h;-><init>(Lh5/n$a;)V

    invoke-virtual {v0, v2}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_5

    :cond_5
    invoke-virtual {v0, v1}, Lz4/a;->e(Lz4/a$d;)V

    :goto_5
    new-instance v0, Lz4/a;

    invoke-static {}, Lh5/m;->l()Lz4/i;

    move-result-object v2

    const-string v3, "dev.flutter.pigeon.AndroidVideoPlayerApi.play"

    invoke-direct {v0, p0, v3, v2}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;)V

    if-eqz p1, :cond_6

    new-instance v2, Lh5/i;

    invoke-direct {v2, p1}, Lh5/i;-><init>(Lh5/n$a;)V

    invoke-virtual {v0, v2}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_6

    :cond_6
    invoke-virtual {v0, v1}, Lz4/a;->e(Lz4/a$d;)V

    :goto_6
    new-instance v0, Lz4/a;

    invoke-static {}, Lh5/m;->l()Lz4/i;

    move-result-object v2

    const-string v3, "dev.flutter.pigeon.AndroidVideoPlayerApi.position"

    invoke-direct {v0, p0, v3, v2}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;)V

    if-eqz p1, :cond_7

    new-instance v2, Lh5/e;

    invoke-direct {v2, p1}, Lh5/e;-><init>(Lh5/n$a;)V

    invoke-virtual {v0, v2}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_7

    :cond_7
    invoke-virtual {v0, v1}, Lz4/a;->e(Lz4/a$d;)V

    :goto_7
    new-instance v0, Lz4/a;

    invoke-static {}, Lh5/m;->l()Lz4/i;

    move-result-object v2

    const-string v3, "dev.flutter.pigeon.AndroidVideoPlayerApi.seekTo"

    invoke-direct {v0, p0, v3, v2}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;)V

    if-eqz p1, :cond_8

    new-instance v2, Lh5/d;

    invoke-direct {v2, p1}, Lh5/d;-><init>(Lh5/n$a;)V

    invoke-virtual {v0, v2}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_8

    :cond_8
    invoke-virtual {v0, v1}, Lz4/a;->e(Lz4/a$d;)V

    :goto_8
    new-instance v0, Lz4/a;

    invoke-static {}, Lh5/m;->l()Lz4/i;

    move-result-object v2

    const-string v3, "dev.flutter.pigeon.AndroidVideoPlayerApi.pause"

    invoke-direct {v0, p0, v3, v2}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;)V

    if-eqz p1, :cond_9

    new-instance v2, Lh5/g;

    invoke-direct {v2, p1}, Lh5/g;-><init>(Lh5/n$a;)V

    invoke-virtual {v0, v2}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_9

    :cond_9
    invoke-virtual {v0, v1}, Lz4/a;->e(Lz4/a$d;)V

    :goto_9
    new-instance v0, Lz4/a;

    invoke-static {}, Lh5/m;->l()Lz4/i;

    move-result-object v2

    const-string v3, "dev.flutter.pigeon.AndroidVideoPlayerApi.setMixWithOthers"

    invoke-direct {v0, p0, v3, v2}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;)V

    if-eqz p1, :cond_a

    new-instance p0, Lh5/k;

    invoke-direct {p0, p1}, Lh5/k;-><init>(Lh5/n$a;)V

    invoke-virtual {v0, p0}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_a

    :cond_a
    invoke-virtual {v0, v1}, Lz4/a;->e(Lz4/a$d;)V

    :goto_a
    return-void
.end method
