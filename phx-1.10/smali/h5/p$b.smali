.class Lh5/p$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/g3$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lh5/p;->m(Lh0/s;Lh5/o;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private f:Z

.field final synthetic g:Lh5/o;

.field final synthetic h:Lh5/p;


# direct methods
.method constructor <init>(Lh5/p;Lh5/o;)V
    .locals 0

    iput-object p1, p0, Lh5/p$b;->h:Lh5/p;

    iput-object p2, p0, Lh5/p$b;->g:Lh5/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lh5/p$b;->f:Z

    return-void
.end method


# virtual methods
.method public synthetic A(I)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->p(Lh0/g3$d;I)V

    return-void
.end method

.method public synthetic B(ZI)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->r(Lh0/g3$d;ZI)V

    return-void
.end method

.method public C(Z)V
    .locals 2

    iget-boolean v0, p0, Lh5/p$b;->f:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lh5/p$b;->f:Z

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iget-boolean v0, p0, Lh5/p$b;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "bufferingStart"

    goto :goto_0

    :cond_0
    const-string v0, "bufferingEnd"

    :goto_0
    const-string v1, "event"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lh5/p$b;->g:Lh5/o;

    invoke-virtual {v0, p1}, Lh5/o;->a(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public synthetic D(Z)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->j(Lh0/g3$d;Z)V

    return-void
.end method

.method public synthetic E(I)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->s(Lh0/g3$d;I)V

    return-void
.end method

.method public synthetic J(Lh0/c3;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->q(Lh0/g3$d;Lh0/c3;)V

    return-void
.end method

.method public synthetic M(Z)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->h(Lh0/g3$d;Z)V

    return-void
.end method

.method public synthetic N(Lh0/g3$e;Lh0/g3$e;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lh0/i3;->t(Lh0/g3$d;Lh0/g3$e;Lh0/g3$e;I)V

    return-void
.end method

.method public synthetic O(Lh0/c4;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->A(Lh0/g3$d;Lh0/c4;I)V

    return-void
.end method

.method public synthetic P()V
    .locals 0

    invoke-static {p0}, Lh0/i3;->u(Lh0/g3$d;)V

    return-void
.end method

.method public synthetic R()V
    .locals 0

    invoke-static {p0}, Lh0/i3;->w(Lh0/g3$d;)V

    return-void
.end method

.method public synthetic T(Lh0/o;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->e(Lh0/g3$d;Lh0/o;)V

    return-void
.end method

.method public synthetic V(Lh0/z1;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->k(Lh0/g3$d;Lh0/z1;I)V

    return-void
.end method

.method public synthetic W(F)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->D(Lh0/g3$d;F)V

    return-void
.end method

.method public X(Lh0/c3;)V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lh5/p$b;->C(Z)V

    iget-object v0, p0, Lh5/p$b;->g:Lh5/o;

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Video player had error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    const-string v2, "VideoError"

    invoke-virtual {v0, v2, p1, v1}, Lh5/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public Y(I)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    invoke-virtual {p0, v0}, Lh5/p$b;->C(Z)V

    iget-object v0, p0, Lh5/p$b;->h:Lh5/p;

    invoke-virtual {v0}, Lh5/p;->h()V

    goto :goto_0

    :cond_0
    const/4 v2, 0x3

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lh5/p$b;->h:Lh5/p;

    iget-boolean v3, v2, Lh5/p;->f:Z

    if-nez v3, :cond_2

    iput-boolean v0, v2, Lh5/p;->f:Z

    invoke-virtual {v2}, Lh5/p;->i()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v2, "event"

    const-string v3, "completed"

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lh5/p$b;->g:Lh5/o;

    invoke-virtual {v2, v0}, Lh5/o;->a(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    if-eq p1, v1, :cond_3

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lh5/p$b;->C(Z)V

    :cond_3
    return-void
.end method

.method public synthetic Z(ZI)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->n(Lh0/g3$d;ZI)V

    return-void
.end method

.method public synthetic a0(Lj0/e;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->a(Lh0/g3$d;Lj0/e;)V

    return-void
.end method

.method public synthetic b(Z)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->y(Lh0/g3$d;Z)V

    return-void
.end method

.method public synthetic h(Lh0/f3;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->o(Lh0/g3$d;Lh0/f3;)V

    return-void
.end method

.method public synthetic i(I)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->v(Lh0/g3$d;I)V

    return-void
.end method

.method public synthetic i0(Z)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->x(Lh0/g3$d;Z)V

    return-void
.end method

.method public synthetic j(Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->c(Lh0/g3$d;Ljava/util/List;)V

    return-void
.end method

.method public synthetic j0(Lh0/g3$b;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->b(Lh0/g3$d;Lh0/g3$b;)V

    return-void
.end method

.method public synthetic k0(II)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->z(Lh0/g3$d;II)V

    return-void
.end method

.method public synthetic l(Lf2/z;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->C(Lh0/g3$d;Lf2/z;)V

    return-void
.end method

.method public synthetic l0(Lh0/e2;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->l(Lh0/g3$d;Lh0/e2;)V

    return-void
.end method

.method public synthetic n0(Lh0/h4;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->B(Lh0/g3$d;Lh0/h4;)V

    return-void
.end method

.method public synthetic o0(Lh0/g3;Lh0/g3$c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->g(Lh0/g3$d;Lh0/g3;Lh0/g3$c;)V

    return-void
.end method

.method public synthetic p0(IZ)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->f(Lh0/g3$d;IZ)V

    return-void
.end method

.method public q0(Z)V
    .locals 3

    iget-object v0, p0, Lh5/p$b;->g:Lh5/o;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "event"

    const-string v2, "isPlayingStateUpdate"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const-string v1, "isPlaying"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lh5/p$b;->g:Lh5/o;

    invoke-virtual {p1, v0}, Lh5/o;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public synthetic w(Lz0/a;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->m(Lh0/g3$d;Lz0/a;)V

    return-void
.end method

.method public synthetic x(Ls1/e;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->d(Lh0/g3$d;Ls1/e;)V

    return-void
.end method
