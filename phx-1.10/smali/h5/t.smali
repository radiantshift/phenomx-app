.class public Lh5/t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lq4/a;
.implements Lh5/n$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh5/t$a;,
        Lh5/t$b;,
        Lh5/t$c;
    }
.end annotation


# instance fields
.field private final f:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Lh5/p;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lh5/t$a;

.field private final h:Lh5/q;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    new-instance v0, Lh5/q;

    invoke-direct {v0}, Lh5/q;-><init>()V

    iput-object v0, p0, Lh5/t;->h:Lh5/q;

    return-void
.end method

.method private n()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-virtual {v1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lh5/p;

    invoke-virtual {v1}, Lh5/p;->c()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lh5/t;->n()V

    return-void
.end method

.method public b(Lh5/n$e;)V
    .locals 3

    iget-object v0, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lh5/n$e;->c()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh5/p;

    invoke-virtual {p1}, Lh5/n$e;->b()Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, p1}, Lh5/p;->k(Z)V

    return-void
.end method

.method public c(Lh5/n$i;)V
    .locals 3

    iget-object v0, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lh5/n$i;->b()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh5/p;

    invoke-virtual {p1}, Lh5/p;->f()V

    return-void
.end method

.method public d(Lq4/a$b;)V
    .locals 2

    iget-object v0, p0, Lh5/t;->g:Lh5/t$a;

    if-nez v0, :cond_0

    const-string v0, "VideoPlayerPlugin"

    const-string v1, "Detached from the engine before registering to it."

    invoke-static {v0, v1}, Ll4/b;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lh5/t;->g:Lh5/t$a;

    invoke-virtual {p1}, Lq4/a$b;->b()Lz4/c;

    move-result-object p1

    invoke-virtual {v0, p1}, Lh5/t$a;->b(Lz4/c;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lh5/t;->g:Lh5/t$a;

    invoke-virtual {p0}, Lh5/t;->a()V

    return-void
.end method

.method public e(Lh5/n$i;)V
    .locals 3

    iget-object v0, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lh5/n$i;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh5/p;

    invoke-virtual {v0}, Lh5/p;->c()V

    iget-object v0, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lh5/n$i;->b()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->remove(J)V

    return-void
.end method

.method public f(Lh5/n$i;)Lh5/n$h;
    .locals 4

    iget-object v0, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lh5/n$i;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh5/p;

    new-instance v1, Lh5/n$h$a;

    invoke-direct {v1}, Lh5/n$h$a;-><init>()V

    invoke-virtual {v0}, Lh5/p;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh5/n$h$a;->b(Ljava/lang/Long;)Lh5/n$h$a;

    move-result-object v1

    invoke-virtual {p1}, Lh5/n$i;->b()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v1, p1}, Lh5/n$h$a;->c(Ljava/lang/Long;)Lh5/n$h$a;

    move-result-object p1

    invoke-virtual {p1}, Lh5/n$h$a;->a()Lh5/n$h;

    move-result-object p1

    invoke-virtual {v0}, Lh5/p;->h()V

    return-object p1
.end method

.method public g(Lh5/n$c;)Lh5/n$i;
    .locals 10

    iget-object v0, p0, Lh5/t;->g:Lh5/t$a;

    iget-object v0, v0, Lh5/t$a;->e:Lio/flutter/view/d;

    invoke-interface {v0}, Lio/flutter/view/d;->a()Lio/flutter/view/d$c;

    move-result-object v0

    new-instance v3, Lz4/d;

    iget-object v1, p0, Lh5/t;->g:Lh5/t$a;

    iget-object v1, v1, Lh5/t$a;->b:Lz4/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "flutter.io/videoPlayer/videoEvents"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lio/flutter/view/d$c;->e()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v1, v2}, Lz4/d;-><init>(Lz4/c;Ljava/lang/String;)V

    invoke-virtual {p1}, Lh5/n$c;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lh5/n$c;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lh5/t;->g:Lh5/t$a;

    iget-object v1, v1, Lh5/t$a;->d:Lh5/t$b;

    invoke-virtual {p1}, Lh5/n$c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lh5/n$c;->e()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, v2, p1}, Lh5/t$b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lh5/t;->g:Lh5/t$a;

    iget-object v1, v1, Lh5/t$a;->c:Lh5/t$c;

    invoke-virtual {p1}, Lh5/n$c;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Lh5/t$c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    new-instance v9, Lh5/p;

    iget-object v1, p0, Lh5/t;->g:Lh5/t$a;

    iget-object v2, v1, Lh5/t$a;->a:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "asset:///"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iget-object v8, p0, Lh5/t;->h:Lh5/q;

    move-object v1, v9

    move-object v4, v0

    invoke-direct/range {v1 .. v8}, Lh5/p;-><init>(Landroid/content/Context;Lz4/d;Lio/flutter/view/d$c;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lh5/q;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lh5/n$c;->d()Ljava/util/Map;

    move-result-object v7

    new-instance v9, Lh5/p;

    iget-object v1, p0, Lh5/t;->g:Lh5/t$a;

    iget-object v2, v1, Lh5/t$a;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lh5/n$c;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lh5/n$c;->c()Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lh5/t;->h:Lh5/q;

    move-object v1, v9

    move-object v4, v0

    invoke-direct/range {v1 .. v8}, Lh5/p;-><init>(Landroid/content/Context;Lz4/d;Lio/flutter/view/d$c;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lh5/q;)V

    :goto_1
    iget-object p1, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-interface {v0}, Lio/flutter/view/d$c;->e()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2, v9}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    new-instance p1, Lh5/n$i$a;

    invoke-direct {p1}, Lh5/n$i$a;-><init>()V

    invoke-interface {v0}, Lio/flutter/view/d$c;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Lh5/n$i$a;->b(Ljava/lang/Long;)Lh5/n$i$a;

    move-result-object p1

    invoke-virtual {p1}, Lh5/n$i$a;->a()Lh5/n$i;

    move-result-object p1

    return-object p1
.end method

.method public h(Lh5/n$f;)V
    .locals 1

    iget-object v0, p0, Lh5/t;->h:Lh5/q;

    invoke-virtual {p1}, Lh5/n$f;->b()Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, v0, Lh5/q;->a:Z

    return-void
.end method

.method public i(Lh5/n$h;)V
    .locals 3

    iget-object v0, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lh5/n$h;->c()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh5/p;

    invoke-virtual {p1}, Lh5/n$h;->b()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lh5/p;->g(I)V

    return-void
.end method

.method public j(Lh5/n$g;)V
    .locals 3

    iget-object v0, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lh5/n$g;->c()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh5/p;

    invoke-virtual {p1}, Lh5/n$g;->b()Ljava/lang/Double;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lh5/p;->l(D)V

    return-void
.end method

.method public k(Lq4/a$b;)V
    .locals 8

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    :try_start_0
    new-instance v0, Lh5/a;

    invoke-direct {v0}, Lh5/a;-><init>()V

    invoke-static {v0}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V
    :try_end_0
    .catch Ljava/security/KeyManagementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_0
    const-string v1, "VideoPlayerPlugin"

    const-string v2, "Failed to enable TLSv1.1 and TLSv1.2 Protocols for API level 19 and below.\nFor more information about Socket Security, please consult the following link:\nhttps://developer.android.com/reference/javax/net/ssl/SSLSocket"

    invoke-static {v1, v2, v0}, Ll4/b;->h(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_1
    invoke-static {}, Ll4/a;->e()Ll4/a;

    move-result-object v0

    new-instance v7, Lh5/t$a;

    invoke-virtual {p1}, Lq4/a$b;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lq4/a$b;->b()Lz4/c;

    move-result-object v3

    invoke-virtual {v0}, Ll4/a;->c()Lo4/d;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lh5/s;

    invoke-direct {v4, v1}, Lh5/s;-><init>(Lo4/d;)V

    invoke-virtual {v0}, Ll4/a;->c()Lo4/d;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Lh5/r;

    invoke-direct {v5, v0}, Lh5/r;-><init>(Lo4/d;)V

    invoke-virtual {p1}, Lq4/a$b;->f()Lio/flutter/view/d;

    move-result-object v6

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lh5/t$a;-><init>(Landroid/content/Context;Lz4/c;Lh5/t$c;Lh5/t$b;Lio/flutter/view/d;)V

    iput-object v7, p0, Lh5/t;->g:Lh5/t$a;

    invoke-virtual {p1}, Lq4/a$b;->b()Lz4/c;

    move-result-object p1

    invoke-virtual {v7, p0, p1}, Lh5/t$a;->a(Lh5/t;Lz4/c;)V

    return-void
.end method

.method public l(Lh5/n$i;)V
    .locals 3

    iget-object v0, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lh5/n$i;->b()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh5/p;

    invoke-virtual {p1}, Lh5/p;->e()V

    return-void
.end method

.method public m(Lh5/n$j;)V
    .locals 3

    iget-object v0, p0, Lh5/t;->f:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lh5/n$j;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh5/p;

    invoke-virtual {p1}, Lh5/n$j;->c()Ljava/lang/Double;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lh5/p;->n(D)V

    return-void
.end method
