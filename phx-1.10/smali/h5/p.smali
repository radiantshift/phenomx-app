.class final Lh5/p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lh0/s;

.field private b:Landroid/view/Surface;

.field private final c:Lio/flutter/view/d$c;

.field private d:Lh5/o;

.field private final e:Lz4/d;

.field f:Z

.field private final g:Lh5/q;

.field private h:Ld2/u$b;


# direct methods
.method constructor <init>(Landroid/content/Context;Lz4/d;Lio/flutter/view/d$c;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lh5/q;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lz4/d;",
            "Lio/flutter/view/d$c;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lh5/q;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lh5/p;->f:Z

    new-instance v0, Ld2/u$b;

    invoke-direct {v0}, Ld2/u$b;-><init>()V

    iput-object v0, p0, Lh5/p;->h:Ld2/u$b;

    iput-object p2, p0, Lh5/p;->e:Lz4/d;

    iput-object p3, p0, Lh5/p;->c:Lio/flutter/view/d$c;

    iput-object p7, p0, Lh5/p;->g:Lh5/q;

    new-instance p2, Lh0/s$b;

    invoke-direct {p2, p1}, Lh0/s$b;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2}, Lh0/s$b;->g()Lh0/s;

    move-result-object p2

    invoke-static {p4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p3

    invoke-virtual {p0, p6}, Lh5/p;->a(Ljava/util/Map;)V

    new-instance p4, Ld2/t$a;

    iget-object p6, p0, Lh5/p;->h:Ld2/u$b;

    invoke-direct {p4, p1, p6}, Ld2/t$a;-><init>(Landroid/content/Context;Ld2/l$a;)V

    invoke-direct {p0, p3, p4, p5}, Lh5/p;->b(Landroid/net/Uri;Ld2/l$a;Ljava/lang/String;)Lj1/x;

    move-result-object p1

    invoke-interface {p2, p1}, Lh0/s;->w(Lj1/x;)V

    invoke-interface {p2}, Lh0/g3;->c()V

    new-instance p1, Lh5/o;

    invoke-direct {p1}, Lh5/o;-><init>()V

    invoke-direct {p0, p2, p1}, Lh5/p;->m(Lh0/s;Lh5/o;)V

    return-void
.end method

.method private b(Landroid/net/Uri;Ld2/l$a;Ljava/lang/String;)Lj1/x;
    .locals 6

    const/4 v0, 0x4

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-nez p3, :cond_0

    invoke-static {p1}, Le2/n0;->n0(Landroid/net/Uri;)I

    move-result v1

    goto :goto_2

    :cond_0
    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :goto_0
    const/4 p3, -0x1

    goto :goto_1

    :sswitch_0
    const-string v5, "other"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_1

    goto :goto_0

    :cond_1
    const/4 p3, 0x3

    goto :goto_1

    :sswitch_1
    const-string v5, "dash"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_2

    goto :goto_0

    :cond_2
    const/4 p3, 0x2

    goto :goto_1

    :sswitch_2
    const-string v5, "hls"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_3

    goto :goto_0

    :cond_3
    const/4 p3, 0x1

    goto :goto_1

    :sswitch_3
    const-string v5, "ss"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_4

    goto :goto_0

    :cond_4
    const/4 p3, 0x0

    :goto_1
    packed-switch p3, :pswitch_data_0

    const/4 v1, -0x1

    goto :goto_2

    :pswitch_0
    const/4 v1, 0x4

    goto :goto_2

    :pswitch_1
    const/4 v1, 0x2

    goto :goto_2

    :pswitch_2
    const/4 v1, 0x1

    :goto_2
    :pswitch_3
    if-eqz v1, :cond_8

    if-eq v1, v4, :cond_7

    if-eq v1, v3, :cond_6

    if-ne v1, v0, :cond_5

    new-instance p3, Lj1/l0$b;

    invoke-direct {p3, p2}, Lj1/l0$b;-><init>(Ld2/l$a;)V

    invoke-static {p1}, Lh0/z1;->d(Landroid/net/Uri;)Lh0/z1;

    move-result-object p1

    invoke-virtual {p3, p1}, Lj1/l0$b;->b(Lh0/z1;)Lj1/l0;

    move-result-object p1

    return-object p1

    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Unsupported type: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    new-instance p3, Lcom/google/android/exoplayer2/source/hls/HlsMediaSource$Factory;

    invoke-direct {p3, p2}, Lcom/google/android/exoplayer2/source/hls/HlsMediaSource$Factory;-><init>(Ld2/l$a;)V

    invoke-static {p1}, Lh0/z1;->d(Landroid/net/Uri;)Lh0/z1;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/google/android/exoplayer2/source/hls/HlsMediaSource$Factory;->a(Lh0/z1;)Lcom/google/android/exoplayer2/source/hls/HlsMediaSource;

    move-result-object p1

    return-object p1

    :cond_7
    new-instance p3, Lcom/google/android/exoplayer2/source/smoothstreaming/SsMediaSource$Factory;

    new-instance v0, Lcom/google/android/exoplayer2/source/smoothstreaming/a$a;

    invoke-direct {v0, p2}, Lcom/google/android/exoplayer2/source/smoothstreaming/a$a;-><init>(Ld2/l$a;)V

    invoke-direct {p3, v0, p2}, Lcom/google/android/exoplayer2/source/smoothstreaming/SsMediaSource$Factory;-><init>(Lcom/google/android/exoplayer2/source/smoothstreaming/b$a;Ld2/l$a;)V

    invoke-static {p1}, Lh0/z1;->d(Landroid/net/Uri;)Lh0/z1;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/google/android/exoplayer2/source/smoothstreaming/SsMediaSource$Factory;->a(Lh0/z1;)Lcom/google/android/exoplayer2/source/smoothstreaming/SsMediaSource;

    move-result-object p1

    return-object p1

    :cond_8
    new-instance p3, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Factory;

    new-instance v0, Lcom/google/android/exoplayer2/source/dash/c$a;

    invoke-direct {v0, p2}, Lcom/google/android/exoplayer2/source/dash/c$a;-><init>(Ld2/l$a;)V

    invoke-direct {p3, v0, p2}, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Factory;-><init>(Lcom/google/android/exoplayer2/source/dash/a$a;Ld2/l$a;)V

    invoke-static {p1}, Lh0/z1;->d(Landroid/net/Uri;)Lh0/z1;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Factory;->a(Lh0/z1;)Lcom/google/android/exoplayer2/source/dash/DashMediaSource;

    move-result-object p1

    return-object p1

    nop

    :sswitch_data_0
    .sparse-switch
        0xe60 -> :sswitch_3
        0x193ef -> :sswitch_2
        0x2eef92 -> :sswitch_1
        0x6527f10 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private static j(Lh0/s;Z)V
    .locals 2

    new-instance v0, Lj0/e$e;

    invoke-direct {v0}, Lj0/e$e;-><init>()V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lj0/e$e;->c(I)Lj0/e$e;

    move-result-object v0

    invoke-virtual {v0}, Lj0/e$e;->a()Lj0/e;

    move-result-object v0

    xor-int/lit8 p1, p1, 0x1

    invoke-interface {p0, v0, p1}, Lh0/s;->o(Lj0/e;Z)V

    return-void
.end method

.method private m(Lh0/s;Lh5/o;)V
    .locals 2

    iput-object p1, p0, Lh5/p;->a:Lh0/s;

    iput-object p2, p0, Lh5/p;->d:Lh5/o;

    iget-object v0, p0, Lh5/p;->e:Lz4/d;

    new-instance v1, Lh5/p$a;

    invoke-direct {v1, p0, p2}, Lh5/p$a;-><init>(Lh5/p;Lh5/o;)V

    invoke-virtual {v0, v1}, Lz4/d;->d(Lz4/d$d;)V

    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Lh5/p;->c:Lio/flutter/view/d$c;

    invoke-interface {v1}, Lio/flutter/view/d$c;->d()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lh5/p;->b:Landroid/view/Surface;

    invoke-interface {p1, v0}, Lh0/g3;->l(Landroid/view/Surface;)V

    iget-object v0, p0, Lh5/p;->g:Lh5/q;

    iget-boolean v0, v0, Lh5/q;->a:Z

    invoke-static {p1, v0}, Lh5/p;->j(Lh0/s;Z)V

    new-instance v0, Lh5/p$b;

    invoke-direct {v0, p0, p2}, Lh5/p$b;-><init>(Lh5/p;Lh5/o;)V

    invoke-interface {p1, v0}, Lh0/g3;->G(Lh0/g3$d;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    const-string v2, "User-Agent"

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v2, "ExoPlayer"

    :goto_0
    iget-object v3, p0, Lh5/p;->h:Ld2/u$b;

    invoke-virtual {v3, v2}, Ld2/u$b;->e(Ljava/lang/String;)Ld2/u$b;

    move-result-object v2

    invoke-virtual {v2, v1}, Ld2/u$b;->c(Z)Ld2/u$b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lh5/p;->h:Ld2/u$b;

    invoke-virtual {v0, p1}, Ld2/u$b;->d(Ljava/util/Map;)Ld2/u$b;

    :cond_1
    return-void
.end method

.method c()V
    .locals 2

    iget-boolean v0, p0, Lh5/p;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh5/p;->a:Lh0/s;

    invoke-interface {v0}, Lh0/g3;->b()V

    :cond_0
    iget-object v0, p0, Lh5/p;->c:Lio/flutter/view/d$c;

    invoke-interface {v0}, Lio/flutter/view/d$c;->a()V

    iget-object v0, p0, Lh5/p;->e:Lz4/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lz4/d;->d(Lz4/d$d;)V

    iget-object v0, p0, Lh5/p;->b:Landroid/view/Surface;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :cond_1
    iget-object v0, p0, Lh5/p;->a:Lh0/s;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lh0/g3;->a()V

    :cond_2
    return-void
.end method

.method d()J
    .locals 2

    iget-object v0, p0, Lh5/p;->a:Lh0/s;

    invoke-interface {v0}, Lh0/g3;->S()J

    move-result-wide v0

    return-wide v0
.end method

.method e()V
    .locals 2

    iget-object v0, p0, Lh5/p;->a:Lh0/s;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lh0/g3;->k(Z)V

    return-void
.end method

.method f()V
    .locals 2

    iget-object v0, p0, Lh5/p;->a:Lh0/s;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lh0/g3;->k(Z)V

    return-void
.end method

.method g(I)V
    .locals 3

    iget-object v0, p0, Lh5/p;->a:Lh0/s;

    int-to-long v1, p1

    invoke-interface {v0, v1, v2}, Lh0/g3;->R(J)V

    return-void
.end method

.method h()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "event"

    const-string v2, "bufferingUpdate"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Number;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v2, p0, Lh5/p;->a:Lh0/s;

    invoke-interface {v2}, Lh0/g3;->r()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "values"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lh5/p;->d:Lh5/o;

    invoke-virtual {v1, v0}, Lh5/o;->a(Ljava/lang/Object;)V

    return-void
.end method

.method i()V
    .locals 5

    iget-boolean v0, p0, Lh5/p;->f:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "event"

    const-string v2, "initialized"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lh5/p;->a:Lh0/s;

    invoke-interface {v1}, Lh0/g3;->M()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "duration"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lh5/p;->a:Lh0/s;

    invoke-interface {v1}, Lh0/s;->y()Lh0/r1;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lh5/p;->a:Lh0/s;

    invoke-interface {v1}, Lh0/s;->y()Lh0/r1;

    move-result-object v1

    iget v2, v1, Lh0/r1;->v:I

    iget v3, v1, Lh0/r1;->w:I

    iget v1, v1, Lh0/r1;->y:I

    const/16 v4, 0x5a

    if-eq v1, v4, :cond_0

    const/16 v4, 0x10e

    if-ne v1, v4, :cond_1

    :cond_0
    iget-object v2, p0, Lh5/p;->a:Lh0/s;

    invoke-interface {v2}, Lh0/s;->y()Lh0/r1;

    move-result-object v2

    iget v2, v2, Lh0/r1;->w:I

    iget-object v3, p0, Lh5/p;->a:Lh0/s;

    invoke-interface {v3}, Lh0/s;->y()Lh0/r1;

    move-result-object v3

    iget v3, v3, Lh0/r1;->v:I

    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v4, "width"

    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "height"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0xb4

    if-ne v1, v2, :cond_2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "rotationCorrection"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v1, p0, Lh5/p;->d:Lh5/o;

    invoke-virtual {v1, v0}, Lh5/o;->a(Ljava/lang/Object;)V

    :cond_3
    return-void
.end method

.method k(Z)V
    .locals 1

    iget-object v0, p0, Lh5/p;->a:Lh0/s;

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-interface {v0, p1}, Lh0/g3;->F(I)V

    return-void
.end method

.method l(D)V
    .locals 1

    new-instance v0, Lh0/f3;

    double-to-float p1, p1

    invoke-direct {v0, p1}, Lh0/f3;-><init>(F)V

    iget-object p1, p0, Lh5/p;->a:Lh0/s;

    invoke-interface {p1, v0}, Lh0/g3;->h(Lh0/f3;)V

    return-void
.end method

.method n(D)V
    .locals 2

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(DD)D

    move-result-wide p1

    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide p1

    double-to-float p1, p1

    iget-object p2, p0, Lh5/p;->a:Lh0/s;

    invoke-interface {p2, p1}, Lh0/g3;->i(F)V

    return-void
.end method
