.class public final Ls1/e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# static fields
.field public static final h:Ls1/e;

.field private static final i:Ljava/lang/String;

.field private static final j:Ljava/lang/String;

.field public static final k:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Ls1/e;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final f:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "Ls1/b;",
            ">;"
        }
    .end annotation
.end field

.field public final g:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ls1/e;

    invoke-static {}, Lp2/q;->q()Lp2/q;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Ls1/e;-><init>(Ljava/util/List;J)V

    sput-object v0, Ls1/e;->h:Ls1/e;

    const/4 v0, 0x0

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/e;->i:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/e;->j:Ljava/lang/String;

    sget-object v0, Ls1/d;->a:Ls1/d;

    sput-object v0, Ls1/e;->k:Lh0/h$a;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ls1/b;",
            ">;J)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lp2/q;->m(Ljava/util/Collection;)Lp2/q;

    move-result-object p1

    iput-object p1, p0, Ls1/e;->f:Lp2/q;

    iput-wide p2, p0, Ls1/e;->g:J

    return-void
.end method

.method public static synthetic a(Landroid/os/Bundle;)Ls1/e;
    .locals 0

    invoke-static {p0}, Ls1/e;->b(Landroid/os/Bundle;)Ls1/e;

    move-result-object p0

    return-object p0
.end method

.method private static final b(Landroid/os/Bundle;)Ls1/e;
    .locals 3

    sget-object v0, Ls1/e;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lp2/q;->q()Lp2/q;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget-object v1, Ls1/b;->O:Lh0/h$a;

    invoke-static {v1, v0}, Le2/c;->b(Lh0/h$a;Ljava/util/List;)Lp2/q;

    move-result-object v0

    :goto_0
    sget-object v1, Ls1/e;->j:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    new-instance p0, Ls1/e;

    invoke-direct {p0, v0, v1, v2}, Ls1/e;-><init>(Ljava/util/List;J)V

    return-object p0
.end method
