.class public abstract Ls1/g;
.super Lk0/j;
.source ""

# interfaces
.implements Ls1/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lk0/j<",
        "Ls1/l;",
        "Ls1/m;",
        "Ls1/j;",
        ">;",
        "Ls1/i;"
    }
.end annotation


# instance fields
.field private final n:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    new-array v1, v0, [Ls1/l;

    new-array v0, v0, [Ls1/m;

    invoke-direct {p0, v1, v0}, Lk0/j;-><init>([Lk0/g;[Lk0/h;)V

    iput-object p1, p0, Ls1/g;->n:Ljava/lang/String;

    const/16 p1, 0x400

    invoke-virtual {p0, p1}, Lk0/j;->v(I)V

    return-void
.end method

.method static synthetic w(Ls1/g;Lk0/h;)V
    .locals 0

    invoke-virtual {p0, p1}, Lk0/j;->s(Lk0/h;)V

    return-void
.end method


# virtual methods
.method protected abstract A([BIZ)Ls1/h;
.end method

.method protected final B(Ls1/l;Ls1/m;Z)Ls1/j;
    .locals 8

    :try_start_0
    iget-object v0, p1, Lk0/g;->h:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-virtual {p0, v1, v0, p3}, Ls1/g;->A([BIZ)Ls1/h;

    move-result-object v5

    iget-wide v3, p1, Lk0/g;->j:J

    iget-wide v6, p1, Ls1/l;->n:J

    move-object v2, p2

    invoke-virtual/range {v2 .. v7}, Ls1/m;->p(JLs1/h;J)V

    const/high16 p1, -0x80000000

    invoke-virtual {p2, p1}, Lk0/a;->g(I)V
    :try_end_0
    .catch Ls1/j; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x0

    return-object p1

    :catch_0
    move-exception p1

    return-object p1
.end method

.method public b(J)V
    .locals 0

    return-void
.end method

.method protected bridge synthetic h()Lk0/g;
    .locals 1

    invoke-virtual {p0}, Ls1/g;->x()Ls1/l;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic i()Lk0/h;
    .locals 1

    invoke-virtual {p0}, Ls1/g;->y()Ls1/m;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic j(Ljava/lang/Throwable;)Lk0/f;
    .locals 0

    invoke-virtual {p0, p1}, Ls1/g;->z(Ljava/lang/Throwable;)Ls1/j;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic k(Lk0/g;Lk0/h;Z)Lk0/f;
    .locals 0

    check-cast p1, Ls1/l;

    check-cast p2, Ls1/m;

    invoke-virtual {p0, p1, p2, p3}, Ls1/g;->B(Ls1/l;Ls1/m;Z)Ls1/j;

    move-result-object p1

    return-object p1
.end method

.method protected final x()Ls1/l;
    .locals 1

    new-instance v0, Ls1/l;

    invoke-direct {v0}, Ls1/l;-><init>()V

    return-object v0
.end method

.method protected final y()Ls1/m;
    .locals 1

    new-instance v0, Ls1/g$a;

    invoke-direct {v0, p0}, Ls1/g$a;-><init>(Ls1/g;)V

    return-object v0
.end method

.method protected final z(Ljava/lang/Throwable;)Ls1/j;
    .locals 2

    new-instance v0, Ls1/j;

    const-string v1, "Unexpected decode error"

    invoke-direct {v0, v1, p1}, Ls1/j;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method
