.class public final Ls1/o;
.super Lh0/f;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private A:Lh0/r1;

.field private B:Ls1/i;

.field private C:Ls1/l;

.field private D:Ls1/m;

.field private E:Ls1/m;

.field private F:I

.field private G:J

.field private H:J

.field private I:J

.field private final s:Landroid/os/Handler;

.field private final t:Ls1/n;

.field private final u:Ls1/k;

.field private final v:Lh0/s1;

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:I


# direct methods
.method public constructor <init>(Ls1/n;Landroid/os/Looper;)V
    .locals 1

    sget-object v0, Ls1/k;->a:Ls1/k;

    invoke-direct {p0, p1, p2, v0}, Ls1/o;-><init>(Ls1/n;Landroid/os/Looper;Ls1/k;)V

    return-void
.end method

.method public constructor <init>(Ls1/n;Landroid/os/Looper;Ls1/k;)V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lh0/f;-><init>(I)V

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ls1/n;

    iput-object p1, p0, Ls1/o;->t:Ls1/n;

    if-nez p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p2, p0}, Le2/n0;->v(Landroid/os/Looper;Landroid/os/Handler$Callback;)Landroid/os/Handler;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Ls1/o;->s:Landroid/os/Handler;

    iput-object p3, p0, Ls1/o;->u:Ls1/k;

    new-instance p1, Lh0/s1;

    invoke-direct {p1}, Lh0/s1;-><init>()V

    iput-object p1, p0, Ls1/o;->v:Lh0/s1;

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Ls1/o;->G:J

    iput-wide p1, p0, Ls1/o;->H:J

    iput-wide p1, p0, Ls1/o;->I:J

    return-void
.end method

.method private U()V
    .locals 4

    new-instance v0, Ls1/e;

    invoke-static {}, Lp2/q;->q()Lp2/q;

    move-result-object v1

    iget-wide v2, p0, Ls1/o;->I:J

    invoke-direct {p0, v2, v3}, Ls1/o;->X(J)J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Ls1/e;-><init>(Ljava/util/List;J)V

    invoke-direct {p0, v0}, Ls1/o;->f0(Ls1/e;)V

    return-void
.end method

.method private V(J)J
    .locals 1
    .annotation runtime Lorg/checkerframework/checker/nullness/qual/RequiresNonNull;
        value = {
            "subtitle"
        }
    .end annotation

    .annotation runtime Lorg/checkerframework/dataflow/qual/SideEffectFree;
    .end annotation

    iget-object v0, p0, Ls1/o;->D:Ls1/m;

    invoke-virtual {v0, p1, p2}, Ls1/m;->a(J)I

    move-result p1

    if-eqz p1, :cond_2

    iget-object p2, p0, Ls1/o;->D:Ls1/m;

    invoke-virtual {p2}, Ls1/m;->d()I

    move-result p2

    if-nez p2, :cond_0

    goto :goto_1

    :cond_0
    const/4 p2, -0x1

    if-ne p1, p2, :cond_1

    iget-object p1, p0, Ls1/o;->D:Ls1/m;

    invoke-virtual {p1}, Ls1/m;->d()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    invoke-virtual {p1, p2}, Ls1/m;->b(I)J

    move-result-wide p1

    goto :goto_0

    :cond_1
    iget-object p2, p0, Ls1/o;->D:Ls1/m;

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {p2, p1}, Ls1/m;->b(I)J

    move-result-wide p1

    :goto_0
    return-wide p1

    :cond_2
    :goto_1
    iget-object p1, p0, Ls1/o;->D:Ls1/m;

    iget-wide p1, p1, Lk0/h;->g:J

    return-wide p1
.end method

.method private W()J
    .locals 4

    iget v0, p0, Ls1/o;->F:I

    const-wide v1, 0x7fffffffffffffffL

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    return-wide v1

    :cond_0
    iget-object v0, p0, Ls1/o;->D:Ls1/m;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Ls1/o;->F:I

    iget-object v3, p0, Ls1/o;->D:Ls1/m;

    invoke-virtual {v3}, Ls1/m;->d()I

    move-result v3

    if-lt v0, v3, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ls1/o;->D:Ls1/m;

    iget v1, p0, Ls1/o;->F:I

    invoke-virtual {v0, v1}, Ls1/m;->b(I)J

    move-result-wide v1

    :goto_0
    return-wide v1
.end method

.method private X(J)J
    .locals 7
    .annotation runtime Lorg/checkerframework/dataflow/qual/SideEffectFree;
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, p1, v2

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-static {v4}, Le2/a;->f(Z)V

    iget-wide v4, p0, Ls1/o;->H:J

    cmp-long v6, v4, v2

    if-eqz v6, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Le2/a;->f(Z)V

    iget-wide v0, p0, Ls1/o;->H:J

    sub-long/2addr p1, v0

    return-wide p1
.end method

.method private Y(Ls1/j;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Subtitle decoding failed. streamFormat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ls1/o;->A:Lh0/r1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TextRenderer"

    invoke-static {v1, v0, p1}, Le2/r;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Ls1/o;->U()V

    invoke-direct {p0}, Ls1/o;->d0()V

    return-void
.end method

.method private Z()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Ls1/o;->y:Z

    iget-object v0, p0, Ls1/o;->u:Ls1/k;

    iget-object v1, p0, Ls1/o;->A:Lh0/r1;

    invoke-static {v1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lh0/r1;

    invoke-interface {v0, v1}, Ls1/k;->b(Lh0/r1;)Ls1/i;

    move-result-object v0

    iput-object v0, p0, Ls1/o;->B:Ls1/i;

    return-void
.end method

.method private a0(Ls1/e;)V
    .locals 2

    iget-object v0, p0, Ls1/o;->t:Ls1/n;

    iget-object v1, p1, Ls1/e;->f:Lp2/q;

    invoke-interface {v0, v1}, Ls1/n;->j(Ljava/util/List;)V

    iget-object v0, p0, Ls1/o;->t:Ls1/n;

    invoke-interface {v0, p1}, Ls1/n;->x(Ls1/e;)V

    return-void
.end method

.method private b0()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Ls1/o;->C:Ls1/l;

    const/4 v1, -0x1

    iput v1, p0, Ls1/o;->F:I

    iget-object v1, p0, Ls1/o;->D:Ls1/m;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lk0/h;->o()V

    iput-object v0, p0, Ls1/o;->D:Ls1/m;

    :cond_0
    iget-object v1, p0, Ls1/o;->E:Ls1/m;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lk0/h;->o()V

    iput-object v0, p0, Ls1/o;->E:Ls1/m;

    :cond_1
    return-void
.end method

.method private c0()V
    .locals 1

    invoke-direct {p0}, Ls1/o;->b0()V

    iget-object v0, p0, Ls1/o;->B:Ls1/i;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls1/i;

    invoke-interface {v0}, Lk0/d;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Ls1/o;->B:Ls1/i;

    const/4 v0, 0x0

    iput v0, p0, Ls1/o;->z:I

    return-void
.end method

.method private d0()V
    .locals 0

    invoke-direct {p0}, Ls1/o;->c0()V

    invoke-direct {p0}, Ls1/o;->Z()V

    return-void
.end method

.method private f0(Ls1/e;)V
    .locals 2

    iget-object v0, p0, Ls1/o;->s:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Ls1/o;->a0(Ls1/e;)V

    :goto_0
    return-void
.end method


# virtual methods
.method protected K()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Ls1/o;->A:Lh0/r1;

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Ls1/o;->G:J

    invoke-direct {p0}, Ls1/o;->U()V

    iput-wide v0, p0, Ls1/o;->H:J

    iput-wide v0, p0, Ls1/o;->I:J

    invoke-direct {p0}, Ls1/o;->c0()V

    return-void
.end method

.method protected M(JZ)V
    .locals 0

    iput-wide p1, p0, Ls1/o;->I:J

    invoke-direct {p0}, Ls1/o;->U()V

    const/4 p1, 0x0

    iput-boolean p1, p0, Ls1/o;->w:Z

    iput-boolean p1, p0, Ls1/o;->x:Z

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Ls1/o;->G:J

    iget p1, p0, Ls1/o;->z:I

    if-eqz p1, :cond_0

    invoke-direct {p0}, Ls1/o;->d0()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Ls1/o;->b0()V

    iget-object p1, p0, Ls1/o;->B:Ls1/i;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ls1/i;

    invoke-interface {p1}, Lk0/d;->flush()V

    :goto_0
    return-void
.end method

.method protected Q([Lh0/r1;JJ)V
    .locals 0

    iput-wide p4, p0, Ls1/o;->H:J

    const/4 p2, 0x0

    aget-object p1, p1, p2

    iput-object p1, p0, Ls1/o;->A:Lh0/r1;

    iget-object p1, p0, Ls1/o;->B:Ls1/i;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    iput p1, p0, Ls1/o;->z:I

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Ls1/o;->Z()V

    :goto_0
    return-void
.end method

.method public a(Lh0/r1;)I
    .locals 1

    iget-object v0, p0, Ls1/o;->u:Ls1/k;

    invoke-interface {v0, p1}, Ls1/k;->a(Lh0/r1;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget p1, p1, Lh0/r1;->L:I

    if-nez p1, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :goto_0
    invoke-static {p1}, Lh0/q3;->a(I)I

    move-result p1

    return p1

    :cond_1
    iget-object p1, p1, Lh0/r1;->q:Ljava/lang/String;

    invoke-static {p1}, Le2/v;->r(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    :goto_1
    invoke-static {p1}, Lh0/q3;->a(I)I

    move-result p1

    return p1

    :cond_2
    const/4 p1, 0x0

    goto :goto_1
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Ls1/o;->x:Z

    return v0
.end method

.method public e0(J)V
    .locals 1

    invoke-virtual {p0}, Lh0/f;->v()Z

    move-result v0

    invoke-static {v0}, Le2/a;->f(Z)V

    iput-wide p1, p0, Ls1/o;->G:J

    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    const-string v0, "TextRenderer"

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 1

    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ls1/e;

    invoke-direct {p0, p1}, Ls1/o;->a0(Ls1/e;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public k()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public p(JJ)V
    .locals 8

    iput-wide p1, p0, Ls1/o;->I:J

    invoke-virtual {p0}, Lh0/f;->v()Z

    move-result p3

    const/4 p4, 0x1

    if-eqz p3, :cond_0

    iget-wide v0, p0, Ls1/o;->G:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p3, v0, v2

    if-eqz p3, :cond_0

    cmp-long p3, p1, v0

    if-ltz p3, :cond_0

    invoke-direct {p0}, Ls1/o;->b0()V

    iput-boolean p4, p0, Ls1/o;->x:Z

    :cond_0
    iget-boolean p3, p0, Ls1/o;->x:Z

    if-eqz p3, :cond_1

    return-void

    :cond_1
    iget-object p3, p0, Ls1/o;->E:Ls1/m;

    if-nez p3, :cond_2

    iget-object p3, p0, Ls1/o;->B:Ls1/i;

    invoke-static {p3}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ls1/i;

    invoke-interface {p3, p1, p2}, Ls1/i;->b(J)V

    :try_start_0
    iget-object p3, p0, Ls1/o;->B:Ls1/i;

    invoke-static {p3}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ls1/i;

    invoke-interface {p3}, Lk0/d;->d()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ls1/m;

    iput-object p3, p0, Ls1/o;->E:Ls1/m;
    :try_end_0
    .catch Ls1/j; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Ls1/o;->Y(Ls1/j;)V

    return-void

    :cond_2
    :goto_0
    invoke-virtual {p0}, Lh0/f;->getState()I

    move-result p3

    const/4 v0, 0x2

    if-eq p3, v0, :cond_3

    return-void

    :cond_3
    iget-object p3, p0, Ls1/o;->D:Ls1/m;

    const/4 v1, 0x0

    if-eqz p3, :cond_4

    invoke-direct {p0}, Ls1/o;->W()J

    move-result-wide v2

    const/4 p3, 0x0

    :goto_1
    cmp-long v4, v2, p1

    if-gtz v4, :cond_5

    iget p3, p0, Ls1/o;->F:I

    add-int/2addr p3, p4

    iput p3, p0, Ls1/o;->F:I

    invoke-direct {p0}, Ls1/o;->W()J

    move-result-wide v2

    const/4 p3, 0x1

    goto :goto_1

    :cond_4
    const/4 p3, 0x0

    :cond_5
    iget-object v2, p0, Ls1/o;->E:Ls1/m;

    const/4 v3, 0x0

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lk0/a;->k()Z

    move-result v4

    if-eqz v4, :cond_7

    if-nez p3, :cond_9

    invoke-direct {p0}, Ls1/o;->W()J

    move-result-wide v4

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v2, v4, v6

    if-nez v2, :cond_9

    iget v2, p0, Ls1/o;->z:I

    if-ne v2, v0, :cond_6

    invoke-direct {p0}, Ls1/o;->d0()V

    goto :goto_2

    :cond_6
    invoke-direct {p0}, Ls1/o;->b0()V

    iput-boolean p4, p0, Ls1/o;->x:Z

    goto :goto_2

    :cond_7
    iget-wide v4, v2, Lk0/h;->g:J

    cmp-long v6, v4, p1

    if-gtz v6, :cond_9

    iget-object p3, p0, Ls1/o;->D:Ls1/m;

    if-eqz p3, :cond_8

    invoke-virtual {p3}, Lk0/h;->o()V

    :cond_8
    invoke-virtual {v2, p1, p2}, Ls1/m;->a(J)I

    move-result p3

    iput p3, p0, Ls1/o;->F:I

    iput-object v2, p0, Ls1/o;->D:Ls1/m;

    iput-object v3, p0, Ls1/o;->E:Ls1/m;

    const/4 p3, 0x1

    :cond_9
    :goto_2
    if-eqz p3, :cond_a

    iget-object p3, p0, Ls1/o;->D:Ls1/m;

    invoke-static {p3}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Ls1/o;->V(J)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Ls1/o;->X(J)J

    move-result-wide v4

    new-instance p3, Ls1/e;

    iget-object v2, p0, Ls1/o;->D:Ls1/m;

    invoke-virtual {v2, p1, p2}, Ls1/m;->c(J)Ljava/util/List;

    move-result-object p1

    invoke-direct {p3, p1, v4, v5}, Ls1/e;-><init>(Ljava/util/List;J)V

    invoke-direct {p0, p3}, Ls1/o;->f0(Ls1/e;)V

    :cond_a
    iget p1, p0, Ls1/o;->z:I

    if-ne p1, v0, :cond_b

    return-void

    :cond_b
    :goto_3
    :try_start_1
    iget-boolean p1, p0, Ls1/o;->w:Z

    if-nez p1, :cond_13

    iget-object p1, p0, Ls1/o;->C:Ls1/l;

    if-nez p1, :cond_d

    iget-object p1, p0, Ls1/o;->B:Ls1/i;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ls1/i;

    invoke-interface {p1}, Lk0/d;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ls1/l;

    if-nez p1, :cond_c

    return-void

    :cond_c
    iput-object p1, p0, Ls1/o;->C:Ls1/l;

    :cond_d
    iget p2, p0, Ls1/o;->z:I

    if-ne p2, p4, :cond_e

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Lk0/a;->n(I)V

    iget-object p2, p0, Ls1/o;->B:Ls1/i;

    invoke-static {p2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ls1/i;

    invoke-interface {p2, p1}, Lk0/d;->c(Ljava/lang/Object;)V

    iput-object v3, p0, Ls1/o;->C:Ls1/l;

    iput v0, p0, Ls1/o;->z:I

    return-void

    :cond_e
    iget-object p2, p0, Ls1/o;->v:Lh0/s1;

    invoke-virtual {p0, p2, p1, v1}, Lh0/f;->R(Lh0/s1;Lk0/g;I)I

    move-result p2

    const/4 p3, -0x4

    if-ne p2, p3, :cond_12

    invoke-virtual {p1}, Lk0/a;->k()Z

    move-result p2

    if-eqz p2, :cond_f

    iput-boolean p4, p0, Ls1/o;->w:Z

    iput-boolean v1, p0, Ls1/o;->y:Z

    goto :goto_5

    :cond_f
    iget-object p2, p0, Ls1/o;->v:Lh0/s1;

    iget-object p2, p2, Lh0/s1;->b:Lh0/r1;

    if-nez p2, :cond_10

    return-void

    :cond_10
    iget-wide p2, p2, Lh0/r1;->u:J

    iput-wide p2, p1, Ls1/l;->n:J

    invoke-virtual {p1}, Lk0/g;->q()V

    iget-boolean p2, p0, Ls1/o;->y:Z

    invoke-virtual {p1}, Lk0/a;->m()Z

    move-result p3

    if-nez p3, :cond_11

    const/4 p3, 0x1

    goto :goto_4

    :cond_11
    const/4 p3, 0x0

    :goto_4
    and-int/2addr p2, p3

    iput-boolean p2, p0, Ls1/o;->y:Z

    :goto_5
    iget-boolean p2, p0, Ls1/o;->y:Z

    if-nez p2, :cond_b

    iget-object p2, p0, Ls1/o;->B:Ls1/i;

    invoke-static {p2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ls1/i;

    invoke-interface {p2, p1}, Lk0/d;->c(Ljava/lang/Object;)V

    iput-object v3, p0, Ls1/o;->C:Ls1/l;
    :try_end_1
    .catch Ls1/j; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :cond_12
    const/4 p1, -0x3

    if-ne p2, p1, :cond_b

    return-void

    :catch_1
    move-exception p1

    invoke-direct {p0, p1}, Ls1/o;->Y(Ls1/j;)V

    :cond_13
    return-void
.end method
