.class public final Ls1/b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls1/b$b;
    }
.end annotation


# static fields
.field private static final A:Ljava/lang/String;

.field private static final B:Ljava/lang/String;

.field private static final C:Ljava/lang/String;

.field private static final D:Ljava/lang/String;

.field private static final E:Ljava/lang/String;

.field private static final F:Ljava/lang/String;

.field private static final G:Ljava/lang/String;

.field private static final H:Ljava/lang/String;

.field private static final I:Ljava/lang/String;

.field private static final J:Ljava/lang/String;

.field private static final K:Ljava/lang/String;

.field private static final L:Ljava/lang/String;

.field private static final M:Ljava/lang/String;

.field private static final N:Ljava/lang/String;

.field public static final O:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Ls1/b;",
            ">;"
        }
    .end annotation
.end field

.field public static final w:Ls1/b;

.field private static final x:Ljava/lang/String;

.field private static final y:Ljava/lang/String;

.field private static final z:Ljava/lang/String;


# instance fields
.field public final f:Ljava/lang/CharSequence;

.field public final g:Landroid/text/Layout$Alignment;

.field public final h:Landroid/text/Layout$Alignment;

.field public final i:Landroid/graphics/Bitmap;

.field public final j:F

.field public final k:I

.field public final l:I

.field public final m:F

.field public final n:I

.field public final o:F

.field public final p:F

.field public final q:Z

.field public final r:I

.field public final s:I

.field public final t:F

.field public final u:I

.field public final v:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ls1/b$b;

    invoke-direct {v0}, Ls1/b$b;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ls1/b$b;->o(Ljava/lang/CharSequence;)Ls1/b$b;

    move-result-object v0

    invoke-virtual {v0}, Ls1/b$b;->a()Ls1/b;

    move-result-object v0

    sput-object v0, Ls1/b;->w:Ls1/b;

    const/4 v0, 0x0

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->x:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->y:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->z:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->A:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->B:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->C:Ljava/lang/String;

    const/4 v0, 0x6

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->D:Ljava/lang/String;

    const/4 v0, 0x7

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->E:Ljava/lang/String;

    const/16 v0, 0x8

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->F:Ljava/lang/String;

    const/16 v0, 0x9

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->G:Ljava/lang/String;

    const/16 v0, 0xa

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->H:Ljava/lang/String;

    const/16 v0, 0xb

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->I:Ljava/lang/String;

    const/16 v0, 0xc

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->J:Ljava/lang/String;

    const/16 v0, 0xd

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->K:Ljava/lang/String;

    const/16 v0, 0xe

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->L:Ljava/lang/String;

    const/16 v0, 0xf

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->M:Ljava/lang/String;

    const/16 v0, 0x10

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ls1/b;->N:Ljava/lang/String;

    sget-object v0, Ls1/a;->a:Ls1/a;

    sput-object v0, Ls1/b;->O:Lh0/h$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;Landroid/text/Layout$Alignment;Landroid/graphics/Bitmap;FIIFIIFFFZIIF)V
    .locals 4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez v1, :cond_0

    invoke-static {p4}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    if-nez v2, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    invoke-static {v3}, Le2/a;->a(Z)V

    :goto_1
    instance-of v3, v1, Landroid/text/Spanned;

    if-eqz v3, :cond_2

    invoke-static {p1}, Landroid/text/SpannedString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannedString;

    move-result-object v1

    :goto_2
    iput-object v1, v0, Ls1/b;->f:Ljava/lang/CharSequence;

    move-object v1, p2

    goto :goto_3

    :cond_2
    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :goto_3
    iput-object v1, v0, Ls1/b;->g:Landroid/text/Layout$Alignment;

    move-object v1, p3

    iput-object v1, v0, Ls1/b;->h:Landroid/text/Layout$Alignment;

    iput-object v2, v0, Ls1/b;->i:Landroid/graphics/Bitmap;

    move v1, p5

    iput v1, v0, Ls1/b;->j:F

    move v1, p6

    iput v1, v0, Ls1/b;->k:I

    move v1, p7

    iput v1, v0, Ls1/b;->l:I

    move v1, p8

    iput v1, v0, Ls1/b;->m:F

    move v1, p9

    iput v1, v0, Ls1/b;->n:I

    move/from16 v1, p12

    iput v1, v0, Ls1/b;->o:F

    move/from16 v1, p13

    iput v1, v0, Ls1/b;->p:F

    move/from16 v1, p14

    iput-boolean v1, v0, Ls1/b;->q:Z

    move/from16 v1, p15

    iput v1, v0, Ls1/b;->r:I

    move v1, p10

    iput v1, v0, Ls1/b;->s:I

    move v1, p11

    iput v1, v0, Ls1/b;->t:F

    move/from16 v1, p16

    iput v1, v0, Ls1/b;->u:I

    move/from16 v1, p17

    iput v1, v0, Ls1/b;->v:F

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;Landroid/text/Layout$Alignment;Landroid/graphics/Bitmap;FIIFIIFFFZIIFLs1/b$a;)V
    .locals 0

    invoke-direct/range {p0 .. p17}, Ls1/b;-><init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;Landroid/text/Layout$Alignment;Landroid/graphics/Bitmap;FIIFIIFFFZIIF)V

    return-void
.end method

.method public static synthetic a(Landroid/os/Bundle;)Ls1/b;
    .locals 0

    invoke-static {p0}, Ls1/b;->c(Landroid/os/Bundle;)Ls1/b;

    move-result-object p0

    return-object p0
.end method

.method private static final c(Landroid/os/Bundle;)Ls1/b;
    .locals 4

    new-instance v0, Ls1/b$b;

    invoke-direct {v0}, Ls1/b$b;-><init>()V

    sget-object v1, Ls1/b;->x:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Ls1/b$b;->o(Ljava/lang/CharSequence;)Ls1/b$b;

    :cond_0
    sget-object v1, Ls1/b;->y:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Landroid/text/Layout$Alignment;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Ls1/b$b;->p(Landroid/text/Layout$Alignment;)Ls1/b$b;

    :cond_1
    sget-object v1, Ls1/b;->z:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Landroid/text/Layout$Alignment;

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Ls1/b$b;->j(Landroid/text/Layout$Alignment;)Ls1/b$b;

    :cond_2
    sget-object v1, Ls1/b;->A:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, Ls1/b$b;->f(Landroid/graphics/Bitmap;)Ls1/b$b;

    :cond_3
    sget-object v1, Ls1/b;->B:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Ls1/b;->C:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ls1/b$b;->h(FI)Ls1/b$b;

    :cond_4
    sget-object v1, Ls1/b;->D:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ls1/b$b;->i(I)Ls1/b$b;

    :cond_5
    sget-object v1, Ls1/b;->E:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Ls1/b$b;->k(F)Ls1/b$b;

    :cond_6
    sget-object v1, Ls1/b;->F:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ls1/b$b;->l(I)Ls1/b$b;

    :cond_7
    sget-object v1, Ls1/b;->H:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    sget-object v2, Ls1/b;->G:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ls1/b$b;->q(FI)Ls1/b$b;

    :cond_8
    sget-object v1, Ls1/b;->I:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Ls1/b$b;->n(F)Ls1/b$b;

    :cond_9
    sget-object v1, Ls1/b;->J:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Ls1/b$b;->g(F)Ls1/b$b;

    :cond_a
    sget-object v1, Ls1/b;->K:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ls1/b$b;->s(I)Ls1/b$b;

    :cond_b
    sget-object v1, Ls1/b;->L:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_c

    invoke-virtual {v0}, Ls1/b$b;->b()Ls1/b$b;

    :cond_c
    sget-object v1, Ls1/b;->M:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ls1/b$b;->r(I)Ls1/b$b;

    :cond_d
    sget-object v1, Ls1/b;->N:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result p0

    invoke-virtual {v0, p0}, Ls1/b$b;->m(F)Ls1/b$b;

    :cond_e
    invoke-virtual {v0}, Ls1/b$b;->a()Ls1/b;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public b()Ls1/b$b;
    .locals 2

    new-instance v0, Ls1/b$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ls1/b$b;-><init>(Ls1/b;Ls1/b$a;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_4

    const-class v2, Ls1/b;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_2

    :cond_1
    check-cast p1, Ls1/b;

    iget-object v2, p0, Ls1/b;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, Ls1/b;->f:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Ls1/b;->g:Landroid/text/Layout$Alignment;

    iget-object v3, p1, Ls1/b;->g:Landroid/text/Layout$Alignment;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Ls1/b;->h:Landroid/text/Layout$Alignment;

    iget-object v3, p1, Ls1/b;->h:Landroid/text/Layout$Alignment;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Ls1/b;->i:Landroid/graphics/Bitmap;

    if-nez v2, :cond_2

    iget-object v2, p1, Ls1/b;->i:Landroid/graphics/Bitmap;

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v3, p1, Ls1/b;->i:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_3

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->sameAs(Landroid/graphics/Bitmap;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_0
    iget v2, p0, Ls1/b;->j:F

    iget v3, p1, Ls1/b;->j:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Ls1/b;->k:I

    iget v3, p1, Ls1/b;->k:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Ls1/b;->l:I

    iget v3, p1, Ls1/b;->l:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Ls1/b;->m:F

    iget v3, p1, Ls1/b;->m:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Ls1/b;->n:I

    iget v3, p1, Ls1/b;->n:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Ls1/b;->o:F

    iget v3, p1, Ls1/b;->o:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Ls1/b;->p:F

    iget v3, p1, Ls1/b;->p:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-boolean v2, p0, Ls1/b;->q:Z

    iget-boolean v3, p1, Ls1/b;->q:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Ls1/b;->r:I

    iget v3, p1, Ls1/b;->r:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Ls1/b;->s:I

    iget v3, p1, Ls1/b;->s:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Ls1/b;->t:F

    iget v3, p1, Ls1/b;->t:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Ls1/b;->u:I

    iget v3, p1, Ls1/b;->u:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Ls1/b;->v:F

    iget p1, p1, Ls1/b;->v:F

    cmpl-float p1, v2, p1

    if-nez p1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_4
    :goto_2
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Ls1/b;->f:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Ls1/b;->g:Landroid/text/Layout$Alignment;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Ls1/b;->h:Landroid/text/Layout$Alignment;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Ls1/b;->i:Landroid/graphics/Bitmap;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget v1, p0, Ls1/b;->j:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget v1, p0, Ls1/b;->k:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget v1, p0, Ls1/b;->l:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget v1, p0, Ls1/b;->m:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget v1, p0, Ls1/b;->n:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget v1, p0, Ls1/b;->o:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iget v1, p0, Ls1/b;->p:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/16 v2, 0xa

    aput-object v1, v0, v2

    iget-boolean v1, p0, Ls1/b;->q:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/16 v2, 0xb

    aput-object v1, v0, v2

    iget v1, p0, Ls1/b;->r:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xc

    aput-object v1, v0, v2

    iget v1, p0, Ls1/b;->s:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xd

    aput-object v1, v0, v2

    iget v1, p0, Ls1/b;->t:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/16 v2, 0xe

    aput-object v1, v0, v2

    iget v1, p0, Ls1/b;->u:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xf

    aput-object v1, v0, v2

    iget v1, p0, Ls1/b;->v:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/16 v2, 0x10

    aput-object v1, v0, v2

    invoke-static {v0}, Lo2/j;->b([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
