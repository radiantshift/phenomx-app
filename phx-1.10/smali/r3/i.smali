.class Lr3/i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/g;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Lo3/c;

.field private final d:Lr3/f;


# direct methods
.method constructor <init>(Lr3/f;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lr3/i;->a:Z

    iput-boolean v0, p0, Lr3/i;->b:Z

    iput-object p1, p0, Lr3/i;->d:Lr3/f;

    return-void
.end method

.method private a()V
    .locals 2

    iget-boolean v0, p0, Lr3/i;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lr3/i;->a:Z

    return-void

    :cond_0
    new-instance v0, Lo3/b;

    const-string v1, "Cannot encode a second value in the ValueEncoderContext"

    invoke-direct {v0, v1}, Lo3/b;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method b(Lo3/c;Z)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lr3/i;->a:Z

    iput-object p1, p0, Lr3/i;->c:Lo3/c;

    iput-boolean p2, p0, Lr3/i;->b:Z

    return-void
.end method

.method public e(Ljava/lang/String;)Lo3/g;
    .locals 3

    invoke-direct {p0}, Lr3/i;->a()V

    iget-object v0, p0, Lr3/i;->d:Lr3/f;

    iget-object v1, p0, Lr3/i;->c:Lo3/c;

    iget-boolean v2, p0, Lr3/i;->b:Z

    invoke-virtual {v0, v1, p1, v2}, Lr3/f;->h(Lo3/c;Ljava/lang/Object;Z)Lo3/e;

    return-object p0
.end method

.method public f(Z)Lo3/g;
    .locals 3

    invoke-direct {p0}, Lr3/i;->a()V

    iget-object v0, p0, Lr3/i;->d:Lr3/f;

    iget-object v1, p0, Lr3/i;->c:Lo3/c;

    iget-boolean v2, p0, Lr3/i;->b:Z

    invoke-virtual {v0, v1, p1, v2}, Lr3/f;->n(Lo3/c;ZZ)Lr3/f;

    return-object p0
.end method
