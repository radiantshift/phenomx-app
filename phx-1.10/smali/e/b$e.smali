.class abstract Le/b$e;
.super Le/b$f;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Le/b$f<",
        "TK;TV;>;",
        "Ljava/util/Iterator<",
        "Ljava/util/Map$Entry<",
        "TK;TV;>;>;"
    }
.end annotation


# instance fields
.field f:Le/b$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Le/b$c<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field g:Le/b$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Le/b$c<",
            "TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Le/b$c;Le/b$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le/b$c<",
            "TK;TV;>;",
            "Le/b$c<",
            "TK;TV;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Le/b$f;-><init>()V

    iput-object p2, p0, Le/b$e;->f:Le/b$c;

    iput-object p1, p0, Le/b$e;->g:Le/b$c;

    return-void
.end method

.method private e()Le/b$c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Le/b$c<",
            "TK;TV;>;"
        }
    .end annotation

    iget-object v0, p0, Le/b$e;->g:Le/b$c;

    iget-object v1, p0, Le/b$e;->f:Le/b$c;

    if-eq v0, v1, :cond_1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Le/b$e;->c(Le/b$c;)Le/b$c;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public a(Le/b$c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le/b$c<",
            "TK;TV;>;)V"
        }
    .end annotation

    iget-object v0, p0, Le/b$e;->f:Le/b$c;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Le/b$e;->g:Le/b$c;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Le/b$e;->g:Le/b$c;

    iput-object v0, p0, Le/b$e;->f:Le/b$c;

    :cond_0
    iget-object v0, p0, Le/b$e;->f:Le/b$c;

    if-ne v0, p1, :cond_1

    invoke-virtual {p0, v0}, Le/b$e;->b(Le/b$c;)Le/b$c;

    move-result-object v0

    iput-object v0, p0, Le/b$e;->f:Le/b$c;

    :cond_1
    iget-object v0, p0, Le/b$e;->g:Le/b$c;

    if-ne v0, p1, :cond_2

    invoke-direct {p0}, Le/b$e;->e()Le/b$c;

    move-result-object p1

    iput-object p1, p0, Le/b$e;->g:Le/b$c;

    :cond_2
    return-void
.end method

.method abstract b(Le/b$c;)Le/b$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le/b$c<",
            "TK;TV;>;)",
            "Le/b$c<",
            "TK;TV;>;"
        }
    .end annotation
.end method

.method abstract c(Le/b$c;)Le/b$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le/b$c<",
            "TK;TV;>;)",
            "Le/b$c<",
            "TK;TV;>;"
        }
    .end annotation
.end method

.method public d()Ljava/util/Map$Entry;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry<",
            "TK;TV;>;"
        }
    .end annotation

    iget-object v0, p0, Le/b$e;->g:Le/b$c;

    invoke-direct {p0}, Le/b$e;->e()Le/b$c;

    move-result-object v1

    iput-object v1, p0, Le/b$e;->g:Le/b$c;

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Le/b$e;->g:Le/b$c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Le/b$e;->d()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method
