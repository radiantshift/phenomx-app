.class public final Lp0/d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lm0/l;


# static fields
.field public static final o:Lm0/r;


# instance fields
.field private final a:[B

.field private final b:Le2/a0;

.field private final c:Z

.field private final d:Lm0/s$a;

.field private e:Lm0/n;

.field private f:Lm0/e0;

.field private g:I

.field private h:Lz0/a;

.field private i:Lm0/v;

.field private j:I

.field private k:I

.field private l:Lp0/b;

.field private m:I

.field private n:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lp0/c;->b:Lp0/c;

    sput-object v0, Lp0/d;->o:Lm0/r;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lp0/d;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x2a

    new-array v0, v0, [B

    iput-object v0, p0, Lp0/d;->a:[B

    new-instance v0, Le2/a0;

    const v1, 0x8000

    new-array v1, v1, [B

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Le2/a0;-><init>([BI)V

    iput-object v0, p0, Lp0/d;->b:Le2/a0;

    const/4 v0, 0x1

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lp0/d;->c:Z

    new-instance p1, Lm0/s$a;

    invoke-direct {p1}, Lm0/s$a;-><init>()V

    iput-object p1, p0, Lp0/d;->d:Lm0/s$a;

    iput v2, p0, Lp0/d;->g:I

    return-void
.end method

.method public static synthetic d()[Lm0/l;
    .locals 1

    invoke-static {}, Lp0/d;->k()[Lm0/l;

    move-result-object v0

    return-object v0
.end method

.method private f(Le2/a0;Z)J
    .locals 4

    iget-object v0, p0, Lp0/d;->i:Lm0/v;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Le2/a0;->f()I

    move-result v0

    :goto_0
    invoke-virtual {p1}, Le2/a0;->g()I

    move-result v1

    add-int/lit8 v1, v1, -0x10

    if-gt v0, v1, :cond_1

    invoke-virtual {p1, v0}, Le2/a0;->R(I)V

    iget-object v1, p0, Lp0/d;->i:Lm0/v;

    iget v2, p0, Lp0/d;->k:I

    iget-object v3, p0, Lp0/d;->d:Lm0/s$a;

    invoke-static {p1, v1, v2, v3}, Lm0/s;->d(Le2/a0;Lm0/v;ILm0/s$a;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    invoke-virtual {p1, v0}, Le2/a0;->R(I)V

    iget-object p1, p0, Lp0/d;->d:Lm0/s$a;

    iget-wide p1, p1, Lm0/s$a;->a:J

    return-wide p1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_5

    :goto_2
    invoke-virtual {p1}, Le2/a0;->g()I

    move-result p2

    iget v1, p0, Lp0/d;->j:I

    sub-int/2addr p2, v1

    if-gt v0, p2, :cond_4

    invoke-virtual {p1, v0}, Le2/a0;->R(I)V

    const/4 p2, 0x0

    :try_start_0
    iget-object v1, p0, Lp0/d;->i:Lm0/v;

    iget v2, p0, Lp0/d;->k:I

    iget-object v3, p0, Lp0/d;->d:Lm0/s$a;

    invoke-static {p1, v1, v2, v3}, Lm0/s;->d(Le2/a0;Lm0/v;ILm0/s$a;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    const/4 v1, 0x0

    :goto_3
    invoke-virtual {p1}, Le2/a0;->f()I

    move-result v2

    invoke-virtual {p1}, Le2/a0;->g()I

    move-result v3

    if-le v2, v3, :cond_2

    goto :goto_4

    :cond_2
    move p2, v1

    :goto_4
    if-eqz p2, :cond_3

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Le2/a0;->g()I

    move-result p2

    invoke-virtual {p1, p2}, Le2/a0;->R(I)V

    goto :goto_5

    :cond_5
    invoke-virtual {p1, v0}, Le2/a0;->R(I)V

    :goto_5
    const-wide/16 p1, -0x1

    return-wide p1
.end method

.method private h(Lm0/m;)V
    .locals 5

    invoke-static {p1}, Lm0/t;->b(Lm0/m;)I

    move-result v0

    iput v0, p0, Lp0/d;->k:I

    iget-object v0, p0, Lp0/d;->e:Lm0/n;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lm0/n;

    invoke-interface {p1}, Lm0/m;->p()J

    move-result-wide v1

    invoke-interface {p1}, Lm0/m;->a()J

    move-result-wide v3

    invoke-direct {p0, v1, v2, v3, v4}, Lp0/d;->i(JJ)Lm0/b0;

    move-result-object p1

    invoke-interface {v0, p1}, Lm0/n;->k(Lm0/b0;)V

    const/4 p1, 0x5

    iput p1, p0, Lp0/d;->g:I

    return-void
.end method

.method private i(JJ)Lm0/b0;
    .locals 8

    iget-object v0, p0, Lp0/d;->i:Lm0/v;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lp0/d;->i:Lm0/v;

    iget-object v0, v2, Lm0/v;->k:Lm0/v$a;

    if-eqz v0, :cond_0

    new-instance p3, Lm0/u;

    invoke-direct {p3, v2, p1, p2}, Lm0/u;-><init>(Lm0/v;J)V

    return-object p3

    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v3, p3, v0

    if-eqz v3, :cond_1

    iget-wide v0, v2, Lm0/v;->j:J

    const-wide/16 v3, 0x0

    cmp-long v5, v0, v3

    if-lez v5, :cond_1

    new-instance v0, Lp0/b;

    iget v3, p0, Lp0/d;->k:I

    move-object v1, v0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lp0/b;-><init>(Lm0/v;IJJ)V

    iput-object v0, p0, Lp0/d;->l:Lp0/b;

    invoke-virtual {v0}, Lm0/a;->b()Lm0/b0;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Lm0/b0$b;

    invoke-virtual {v2}, Lm0/v;->f()J

    move-result-wide p2

    invoke-direct {p1, p2, p3}, Lm0/b0$b;-><init>(J)V

    return-object p1
.end method

.method private j(Lm0/m;)V
    .locals 3

    iget-object v0, p0, Lp0/d;->a:[B

    array-length v1, v0

    const/4 v2, 0x0

    invoke-interface {p1, v0, v2, v1}, Lm0/m;->n([BII)V

    invoke-interface {p1}, Lm0/m;->g()V

    const/4 p1, 0x2

    iput p1, p0, Lp0/d;->g:I

    return-void
.end method

.method private static synthetic k()[Lm0/l;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lm0/l;

    new-instance v1, Lp0/d;

    invoke-direct {v1}, Lp0/d;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method private l()V
    .locals 11

    iget-wide v0, p0, Lp0/d;->n:J

    const-wide/32 v2, 0xf4240

    mul-long v0, v0, v2

    iget-object v2, p0, Lp0/d;->i:Lm0/v;

    invoke-static {v2}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lm0/v;

    iget v2, v2, Lm0/v;->e:I

    int-to-long v2, v2

    div-long v5, v0, v2

    iget-object v0, p0, Lp0/d;->f:Lm0/e0;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lm0/e0;

    iget v8, p0, Lp0/d;->m:I

    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-interface/range {v4 .. v10}, Lm0/e0;->f(JIIILm0/e0$a;)V

    return-void
.end method

.method private m(Lm0/m;Lm0/a0;)I
    .locals 6

    iget-object v0, p0, Lp0/d;->f:Lm0/e0;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lp0/d;->i:Lm0/v;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lp0/d;->l:Lp0/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lm0/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lp0/d;->l:Lp0/b;

    invoke-virtual {v0, p1, p2}, Lm0/a;->c(Lm0/m;Lm0/a0;)I

    move-result p1

    return p1

    :cond_0
    iget-wide v0, p0, Lp0/d;->n:J

    const-wide/16 v2, -0x1

    const/4 p2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    iget-object v0, p0, Lp0/d;->i:Lm0/v;

    invoke-static {p1, v0}, Lm0/s;->i(Lm0/m;Lm0/v;)J

    move-result-wide v0

    iput-wide v0, p0, Lp0/d;->n:J

    return p2

    :cond_1
    iget-object v0, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {v0}, Le2/a0;->g()I

    move-result v0

    const v1, 0x8000

    if-ge v0, v1, :cond_4

    iget-object v4, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {v4}, Le2/a0;->e()[B

    move-result-object v4

    sub-int/2addr v1, v0

    invoke-interface {p1, v4, v0, v1}, Lm0/m;->read([BII)I

    move-result p1

    const/4 v1, -0x1

    if-ne p1, v1, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_0
    if-nez v4, :cond_3

    iget-object v1, p0, Lp0/d;->b:Le2/a0;

    add-int/2addr v0, p1

    invoke-virtual {v1, v0}, Le2/a0;->Q(I)V

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {p1}, Le2/a0;->a()I

    move-result p1

    if-nez p1, :cond_5

    invoke-direct {p0}, Lp0/d;->l()V

    return v1

    :cond_4
    const/4 v4, 0x0

    :cond_5
    :goto_1
    iget-object p1, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {p1}, Le2/a0;->f()I

    move-result p1

    iget v0, p0, Lp0/d;->m:I

    iget v1, p0, Lp0/d;->j:I

    if-ge v0, v1, :cond_6

    iget-object v5, p0, Lp0/d;->b:Le2/a0;

    sub-int/2addr v1, v0

    invoke-virtual {v5}, Le2/a0;->a()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v5, v0}, Le2/a0;->S(I)V

    :cond_6
    iget-object v0, p0, Lp0/d;->b:Le2/a0;

    invoke-direct {p0, v0, v4}, Lp0/d;->f(Le2/a0;Z)J

    move-result-wide v0

    iget-object v4, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {v4}, Le2/a0;->f()I

    move-result v4

    sub-int/2addr v4, p1

    iget-object v5, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {v5, p1}, Le2/a0;->R(I)V

    iget-object p1, p0, Lp0/d;->f:Lm0/e0;

    iget-object v5, p0, Lp0/d;->b:Le2/a0;

    invoke-interface {p1, v5, v4}, Lm0/e0;->a(Le2/a0;I)V

    iget p1, p0, Lp0/d;->m:I

    add-int/2addr p1, v4

    iput p1, p0, Lp0/d;->m:I

    cmp-long p1, v0, v2

    if-eqz p1, :cond_7

    invoke-direct {p0}, Lp0/d;->l()V

    iput p2, p0, Lp0/d;->m:I

    iput-wide v0, p0, Lp0/d;->n:J

    :cond_7
    iget-object p1, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {p1}, Le2/a0;->a()I

    move-result p1

    const/16 v0, 0x10

    if-ge p1, v0, :cond_8

    iget-object p1, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {p1}, Le2/a0;->a()I

    move-result p1

    iget-object v0, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {v0}, Le2/a0;->e()[B

    move-result-object v0

    iget-object v1, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {v1}, Le2/a0;->f()I

    move-result v1

    iget-object v2, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {v2}, Le2/a0;->e()[B

    move-result-object v2

    invoke-static {v0, v1, v2, p2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {v0, p2}, Le2/a0;->R(I)V

    iget-object v0, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {v0, p1}, Le2/a0;->Q(I)V

    :cond_8
    return p2
.end method

.method private n(Lm0/m;)V
    .locals 2

    iget-boolean v0, p0, Lp0/d;->c:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-static {p1, v0}, Lm0/t;->d(Lm0/m;Z)Lz0/a;

    move-result-object p1

    iput-object p1, p0, Lp0/d;->h:Lz0/a;

    iput v1, p0, Lp0/d;->g:I

    return-void
.end method

.method private o(Lm0/m;)V
    .locals 3

    new-instance v0, Lm0/t$a;

    iget-object v1, p0, Lp0/d;->i:Lm0/v;

    invoke-direct {v0, v1}, Lm0/t$a;-><init>(Lm0/v;)V

    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_0

    invoke-static {p1, v0}, Lm0/t;->e(Lm0/m;Lm0/t$a;)Z

    move-result v1

    iget-object v2, v0, Lm0/t$a;->a:Lm0/v;

    invoke-static {v2}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lm0/v;

    iput-object v2, p0, Lp0/d;->i:Lm0/v;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lp0/d;->i:Lm0/v;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lp0/d;->i:Lm0/v;

    iget p1, p1, Lm0/v;->c:I

    const/4 v0, 0x6

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lp0/d;->j:I

    iget-object p1, p0, Lp0/d;->f:Lm0/e0;

    invoke-static {p1}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lm0/e0;

    iget-object v0, p0, Lp0/d;->i:Lm0/v;

    iget-object v1, p0, Lp0/d;->a:[B

    iget-object v2, p0, Lp0/d;->h:Lz0/a;

    invoke-virtual {v0, v1, v2}, Lm0/v;->g([BLz0/a;)Lh0/r1;

    move-result-object v0

    invoke-interface {p1, v0}, Lm0/e0;->e(Lh0/r1;)V

    const/4 p1, 0x4

    iput p1, p0, Lp0/d;->g:I

    return-void
.end method

.method private p(Lm0/m;)V
    .locals 0

    invoke-static {p1}, Lm0/t;->i(Lm0/m;)V

    const/4 p1, 0x3

    iput p1, p0, Lp0/d;->g:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public b(JJ)V
    .locals 4

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    cmp-long v3, p1, v1

    if-nez v3, :cond_0

    iput v0, p0, Lp0/d;->g:I

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lp0/d;->l:Lp0/b;

    if-eqz p1, :cond_1

    invoke-virtual {p1, p3, p4}, Lm0/a;->h(J)V

    :cond_1
    :goto_0
    cmp-long p1, p3, v1

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    const-wide/16 v1, -0x1

    :goto_1
    iput-wide v1, p0, Lp0/d;->n:J

    iput v0, p0, Lp0/d;->m:I

    iget-object p1, p0, Lp0/d;->b:Le2/a0;

    invoke-virtual {p1, v0}, Le2/a0;->N(I)V

    return-void
.end method

.method public c(Lm0/n;)V
    .locals 2

    iput-object p1, p0, Lp0/d;->e:Lm0/n;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lm0/n;->e(II)Lm0/e0;

    move-result-object v0

    iput-object v0, p0, Lp0/d;->f:Lm0/e0;

    invoke-interface {p1}, Lm0/n;->i()V

    return-void
.end method

.method public e(Lm0/m;)Z
    .locals 1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lm0/t;->c(Lm0/m;Z)Lz0/a;

    invoke-static {p1}, Lm0/t;->a(Lm0/m;)Z

    move-result p1

    return p1
.end method

.method public g(Lm0/m;Lm0/a0;)I
    .locals 3

    iget v0, p0, Lp0/d;->g:I

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1, p2}, Lp0/d;->m(Lm0/m;Lm0/a0;)I

    move-result p1

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_1
    invoke-direct {p0, p1}, Lp0/d;->h(Lm0/m;)V

    return v1

    :cond_2
    invoke-direct {p0, p1}, Lp0/d;->o(Lm0/m;)V

    return v1

    :cond_3
    invoke-direct {p0, p1}, Lp0/d;->p(Lm0/m;)V

    return v1

    :cond_4
    invoke-direct {p0, p1}, Lp0/d;->j(Lm0/m;)V

    return v1

    :cond_5
    invoke-direct {p0, p1}, Lp0/d;->n(Lm0/m;)V

    return v1
.end method
