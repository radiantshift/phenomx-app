.class final Lp0/b$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lm0/a$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lp0/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field private final a:Lm0/v;

.field private final b:I

.field private final c:Lm0/s$a;


# direct methods
.method private constructor <init>(Lm0/v;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lp0/b$b;->a:Lm0/v;

    iput p2, p0, Lp0/b$b;->b:I

    new-instance p1, Lm0/s$a;

    invoke-direct {p1}, Lm0/s$a;-><init>()V

    iput-object p1, p0, Lp0/b$b;->c:Lm0/s$a;

    return-void
.end method

.method synthetic constructor <init>(Lm0/v;ILp0/b$a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lp0/b$b;-><init>(Lm0/v;I)V

    return-void
.end method

.method private c(Lm0/m;)J
    .locals 7

    :goto_0
    invoke-interface {p1}, Lm0/m;->m()J

    move-result-wide v0

    invoke-interface {p1}, Lm0/m;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x6

    sub-long/2addr v2, v4

    cmp-long v6, v0, v2

    if-gez v6, :cond_0

    iget-object v0, p0, Lp0/b$b;->a:Lm0/v;

    iget v1, p0, Lp0/b$b;->b:I

    iget-object v2, p0, Lp0/b$b;->c:Lm0/s$a;

    invoke-static {p1, v0, v1, v2}, Lm0/s;->h(Lm0/m;Lm0/v;ILm0/s$a;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lm0/m;->o(I)V

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Lm0/m;->m()J

    move-result-wide v0

    invoke-interface {p1}, Lm0/m;->a()J

    move-result-wide v2

    sub-long/2addr v2, v4

    cmp-long v4, v0, v2

    if-ltz v4, :cond_1

    invoke-interface {p1}, Lm0/m;->a()J

    move-result-wide v0

    invoke-interface {p1}, Lm0/m;->m()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v1, v0

    invoke-interface {p1, v1}, Lm0/m;->o(I)V

    iget-object p1, p0, Lp0/b$b;->a:Lm0/v;

    iget-wide v0, p1, Lm0/v;->j:J

    return-wide v0

    :cond_1
    iget-object p1, p0, Lp0/b$b;->c:Lm0/s$a;

    iget-wide v0, p1, Lm0/s$a;->a:J

    return-wide v0
.end method


# virtual methods
.method public a(Lm0/m;J)Lm0/a$e;
    .locals 10

    invoke-interface {p1}, Lm0/m;->p()J

    move-result-wide v0

    invoke-direct {p0, p1}, Lp0/b$b;->c(Lm0/m;)J

    move-result-wide v2

    invoke-interface {p1}, Lm0/m;->m()J

    move-result-wide v4

    iget-object v6, p0, Lp0/b$b;->a:Lm0/v;

    iget v6, v6, Lm0/v;->c:I

    const/4 v7, 0x6

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-interface {p1, v6}, Lm0/m;->o(I)V

    invoke-direct {p0, p1}, Lp0/b$b;->c(Lm0/m;)J

    move-result-wide v6

    invoke-interface {p1}, Lm0/m;->m()J

    move-result-wide v8

    cmp-long p1, v2, p2

    if-gtz p1, :cond_0

    cmp-long p1, v6, p2

    if-lez p1, :cond_0

    invoke-static {v4, v5}, Lm0/a$e;->e(J)Lm0/a$e;

    move-result-object p1

    return-object p1

    :cond_0
    cmp-long p1, v6, p2

    if-gtz p1, :cond_1

    invoke-static {v6, v7, v8, v9}, Lm0/a$e;->f(JJ)Lm0/a$e;

    move-result-object p1

    return-object p1

    :cond_1
    invoke-static {v2, v3, v0, v1}, Lm0/a$e;->d(JJ)Lm0/a$e;

    move-result-object p1

    return-object p1
.end method

.method public synthetic b()V
    .locals 0

    invoke-static {p0}, Lm0/b;->a(Lm0/a$f;)V

    return-void
.end method
