.class public Lc0/s;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lw/e;

.field private final c:Ld0/d;

.field private final d:Lc0/y;

.field private final e:Ljava/util/concurrent/Executor;

.field private final f:Le0/b;

.field private final g:Lf0/a;

.field private final h:Lf0/a;

.field private final i:Ld0/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lw/e;Ld0/d;Lc0/y;Ljava/util/concurrent/Executor;Le0/b;Lf0/a;Lf0/a;Ld0/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc0/s;->a:Landroid/content/Context;

    iput-object p2, p0, Lc0/s;->b:Lw/e;

    iput-object p3, p0, Lc0/s;->c:Ld0/d;

    iput-object p4, p0, Lc0/s;->d:Lc0/y;

    iput-object p5, p0, Lc0/s;->e:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lc0/s;->f:Le0/b;

    iput-object p7, p0, Lc0/s;->g:Lf0/a;

    iput-object p8, p0, Lc0/s;->h:Lf0/a;

    iput-object p9, p0, Lc0/s;->i:Ld0/c;

    return-void
.end method

.method public static synthetic a(Lc0/s;Lv/p;ILjava/lang/Runnable;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lc0/s;->t(Lv/p;ILjava/lang/Runnable;)V

    return-void
.end method

.method public static synthetic b(Lc0/s;Lv/p;)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0, p1}, Lc0/s;->l(Lv/p;)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic c(Lc0/s;Ljava/util/Map;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lc0/s;->q(Ljava/util/Map;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic d(Lc0/s;Lv/p;)Ljava/lang/Iterable;
    .locals 0

    invoke-direct {p0, p1}, Lc0/s;->m(Lv/p;)Ljava/lang/Iterable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic e(Lc0/s;Ljava/lang/Iterable;Lv/p;J)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lc0/s;->n(Ljava/lang/Iterable;Lv/p;J)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic f(Lc0/s;Lv/p;J)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lc0/s;->r(Lv/p;J)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic g(Lc0/s;Ljava/lang/Iterable;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lc0/s;->o(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic h(Lc0/s;Lv/p;I)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1, p2}, Lc0/s;->s(Lv/p;I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic i(Lc0/s;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lc0/s;->p()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private synthetic l(Lv/p;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lc0/s;->c:Ld0/d;

    invoke-interface {v0, p1}, Ld0/d;->z(Lv/p;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method private synthetic m(Lv/p;)Ljava/lang/Iterable;
    .locals 1

    iget-object v0, p0, Lc0/s;->c:Ld0/d;

    invoke-interface {v0, p1}, Ld0/d;->m(Lv/p;)Ljava/lang/Iterable;

    move-result-object p1

    return-object p1
.end method

.method private synthetic n(Ljava/lang/Iterable;Lv/p;J)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lc0/s;->c:Ld0/d;

    invoke-interface {v0, p1}, Ld0/d;->A(Ljava/lang/Iterable;)V

    iget-object p1, p0, Lc0/s;->c:Ld0/d;

    iget-object v0, p0, Lc0/s;->g:Lf0/a;

    invoke-interface {v0}, Lf0/a;->a()J

    move-result-wide v0

    add-long/2addr v0, p3

    invoke-interface {p1, p2, v0, v1}, Ld0/d;->C(Lv/p;J)V

    const/4 p1, 0x0

    return-object p1
.end method

.method private synthetic o(Ljava/lang/Iterable;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lc0/s;->c:Ld0/d;

    invoke-interface {v0, p1}, Ld0/d;->f(Ljava/lang/Iterable;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method private synthetic p()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lc0/s;->i:Ld0/c;

    invoke-interface {v0}, Ld0/c;->c()V

    const/4 v0, 0x0

    return-object v0
.end method

.method private synthetic q(Ljava/util/Map;)Ljava/lang/Object;
    .locals 5

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v1, p0, Lc0/s;->i:Ld0/c;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    sget-object v4, Ly/c$b;->l:Ly/c$b;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v0}, Ld0/c;->b(JLy/c$b;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private synthetic r(Lv/p;J)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lc0/s;->c:Ld0/d;

    iget-object v1, p0, Lc0/s;->g:Lf0/a;

    invoke-interface {v1}, Lf0/a;->a()J

    move-result-wide v1

    add-long/2addr v1, p2

    invoke-interface {v0, p1, v1, v2}, Ld0/d;->C(Lv/p;J)V

    const/4 p1, 0x0

    return-object p1
.end method

.method private synthetic s(Lv/p;I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lc0/s;->d:Lc0/y;

    add-int/lit8 p2, p2, 0x1

    invoke-interface {v0, p1, p2}, Lc0/y;->b(Lv/p;I)V

    const/4 p1, 0x0

    return-object p1
.end method

.method private synthetic t(Lv/p;ILjava/lang/Runnable;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lc0/s;->f:Le0/b;

    iget-object v1, p0, Lc0/s;->c:Ld0/d;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lc0/r;

    invoke-direct {v2, v1}, Lc0/r;-><init>(Ld0/d;)V

    invoke-interface {v0, v2}, Le0/b;->d(Le0/b$a;)Ljava/lang/Object;

    invoke-virtual {p0}, Lc0/s;->k()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lc0/s;->f:Le0/b;

    new-instance v1, Lc0/o;

    invoke-direct {v1, p0, p1, p2}, Lc0/o;-><init>(Lc0/s;Lv/p;I)V

    invoke-interface {v0, v1}, Le0/b;->d(Le0/b$a;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lc0/s;->u(Lv/p;I)Lw/g;
    :try_end_0
    .catch Le0/a; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    :try_start_1
    iget-object v0, p0, Lc0/s;->d:Lc0/y;

    add-int/lit8 p2, p2, 0x1

    invoke-interface {v0, p1, p2}, Lc0/y;->b(Lv/p;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-interface {p3}, Ljava/lang/Runnable;->run()V

    return-void

    :goto_1
    invoke-interface {p3}, Ljava/lang/Runnable;->run()V

    throw p1
.end method


# virtual methods
.method public j(Lw/m;)Lv/i;
    .locals 4

    iget-object v0, p0, Lc0/s;->f:Le0/b;

    iget-object v1, p0, Lc0/s;->i:Ld0/c;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lc0/q;

    invoke-direct {v2, v1}, Lc0/q;-><init>(Ld0/c;)V

    invoke-interface {v0, v2}, Le0/b;->d(Le0/b$a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/a;

    invoke-static {}, Lv/i;->a()Lv/i$a;

    move-result-object v1

    iget-object v2, p0, Lc0/s;->g:Lf0/a;

    invoke-interface {v2}, Lf0/a;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lv/i$a;->i(J)Lv/i$a;

    move-result-object v1

    iget-object v2, p0, Lc0/s;->h:Lf0/a;

    invoke-interface {v2}, Lf0/a;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lv/i$a;->k(J)Lv/i$a;

    move-result-object v1

    const-string v2, "GDT_CLIENT_METRICS"

    invoke-virtual {v1, v2}, Lv/i$a;->j(Ljava/lang/String;)Lv/i$a;

    move-result-object v1

    new-instance v2, Lv/h;

    const-string v3, "proto"

    invoke-static {v3}, Lt/b;->b(Ljava/lang/String;)Lt/b;

    move-result-object v3

    invoke-virtual {v0}, Ly/a;->f()[B

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lv/h;-><init>(Lt/b;[B)V

    invoke-virtual {v1, v2}, Lv/i$a;->h(Lv/h;)Lv/i$a;

    move-result-object v0

    invoke-virtual {v0}, Lv/i$a;->d()Lv/i;

    move-result-object v0

    invoke-interface {p1, v0}, Lw/m;->a(Lv/i;)Lv/i;

    move-result-object p1

    return-object p1
.end method

.method k()Z
    .locals 2

    iget-object v0, p0, Lc0/s;->a:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public u(Lv/p;I)Lw/g;
    .locals 11

    iget-object v0, p0, Lc0/s;->b:Lw/e;

    invoke-virtual {p1}, Lv/p;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lw/e;->a(Ljava/lang/String;)Lw/m;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Lw/g;->e(J)Lw/g;

    move-result-object v3

    :cond_0
    :goto_0
    move-wide v8, v1

    :cond_1
    :goto_1
    iget-object v1, p0, Lc0/s;->f:Le0/b;

    new-instance v2, Lc0/m;

    invoke-direct {v2, p0, p1}, Lc0/m;-><init>(Lc0/s;Lv/p;)V

    invoke-interface {v1, v2}, Le0/b;->d(Le0/b$a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lc0/s;->f:Le0/b;

    new-instance v2, Lc0/n;

    invoke-direct {v2, p0, p1}, Lc0/n;-><init>(Lc0/s;Lv/p;)V

    invoke-interface {v1, v2}, Le0/b;->d(Le0/b$a;)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ljava/lang/Iterable;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    return-object v3

    :cond_2
    if-nez v0, :cond_3

    const-string v1, "Uploader"

    const-string v2, "Unknown backend for %s, deleting event batch for it..."

    invoke-static {v1, v2, p1}, Lz/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-static {}, Lw/g;->a()Lw/g;

    move-result-object v1

    :goto_2
    move-object v3, v1

    goto :goto_4

    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ld0/k;

    invoke-virtual {v3}, Ld0/k;->b()Lv/i;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    invoke-virtual {p1}, Lv/p;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, v0}, Lc0/s;->j(Lw/m;)Lv/i;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-static {}, Lw/f;->a()Lw/f$a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lw/f$a;->b(Ljava/lang/Iterable;)Lw/f$a;

    move-result-object v1

    invoke-virtual {p1}, Lv/p;->c()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lw/f$a;->c([B)Lw/f$a;

    move-result-object v1

    invoke-virtual {v1}, Lw/f$a;->a()Lw/f;

    move-result-object v1

    invoke-interface {v0, v1}, Lw/m;->b(Lw/f;)Lw/g;

    move-result-object v1

    goto :goto_2

    :goto_4
    invoke-virtual {v3}, Lw/g;->c()Lw/g$a;

    move-result-object v1

    sget-object v2, Lw/g$a;->g:Lw/g$a;

    const/4 v10, 0x1

    if-ne v1, v2, :cond_6

    iget-object v0, p0, Lc0/s;->f:Le0/b;

    new-instance v1, Lc0/k;

    move-object v4, v1

    move-object v5, p0

    move-object v7, p1

    invoke-direct/range {v4 .. v9}, Lc0/k;-><init>(Lc0/s;Ljava/lang/Iterable;Lv/p;J)V

    invoke-interface {v0, v1}, Le0/b;->d(Le0/b$a;)Ljava/lang/Object;

    iget-object v0, p0, Lc0/s;->d:Lc0/y;

    add-int/2addr p2, v10

    invoke-interface {v0, p1, p2, v10}, Lc0/y;->a(Lv/p;IZ)V

    return-object v3

    :cond_6
    iget-object v1, p0, Lc0/s;->f:Le0/b;

    new-instance v2, Lc0/j;

    invoke-direct {v2, p0, v6}, Lc0/j;-><init>(Lc0/s;Ljava/lang/Iterable;)V

    invoke-interface {v1, v2}, Le0/b;->d(Le0/b$a;)Ljava/lang/Object;

    invoke-virtual {v3}, Lw/g;->c()Lw/g$a;

    move-result-object v1

    sget-object v2, Lw/g$a;->f:Lw/g$a;

    if-ne v1, v2, :cond_7

    invoke-virtual {v3}, Lw/g;->b()J

    move-result-wide v1

    invoke-static {v8, v9, v1, v2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    invoke-virtual {p1}, Lv/p;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lc0/s;->f:Le0/b;

    new-instance v5, Lc0/h;

    invoke-direct {v5, p0}, Lc0/h;-><init>(Lc0/s;)V

    invoke-interface {v4, v5}, Le0/b;->d(Le0/b$a;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v3}, Lw/g;->c()Lw/g$a;

    move-result-object v1

    sget-object v2, Lw/g$a;->i:Lw/g$a;

    if-ne v1, v2, :cond_1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ld0/k;

    invoke-virtual {v4}, Ld0/k;->b()Lv/i;

    move-result-object v4

    invoke-virtual {v4}, Lv/i;->j()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_6

    :cond_8
    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v5, v10

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    :goto_6
    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lc0/s;->f:Le0/b;

    new-instance v4, Lc0/l;

    invoke-direct {v4, p0, v1}, Lc0/l;-><init>(Lc0/s;Ljava/util/Map;)V

    invoke-interface {v2, v4}, Le0/b;->d(Le0/b$a;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_a
    iget-object p2, p0, Lc0/s;->f:Le0/b;

    new-instance v0, Lc0/p;

    invoke-direct {v0, p0, p1, v8, v9}, Lc0/p;-><init>(Lc0/s;Lv/p;J)V

    invoke-interface {p2, v0}, Le0/b;->d(Le0/b$a;)Ljava/lang/Object;

    return-object v3
.end method

.method public v(Lv/p;ILjava/lang/Runnable;)V
    .locals 2

    iget-object v0, p0, Lc0/s;->e:Ljava/util/concurrent/Executor;

    new-instance v1, Lc0/i;

    invoke-direct {v1, p0, p1, p2, p3}, Lc0/i;-><init>(Lc0/s;Lv/p;ILjava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
