.class public final Lc0/t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lx/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lx/b<",
        "Lc0/s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lw/e;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Ld0/d;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lc0/y;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Le0/b;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lf0/a;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lf0/a;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Ld0/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Landroid/content/Context;",
            ">;",
            "Lj5/a<",
            "Lw/e;",
            ">;",
            "Lj5/a<",
            "Ld0/d;",
            ">;",
            "Lj5/a<",
            "Lc0/y;",
            ">;",
            "Lj5/a<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lj5/a<",
            "Le0/b;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Ld0/c;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc0/t;->a:Lj5/a;

    iput-object p2, p0, Lc0/t;->b:Lj5/a;

    iput-object p3, p0, Lc0/t;->c:Lj5/a;

    iput-object p4, p0, Lc0/t;->d:Lj5/a;

    iput-object p5, p0, Lc0/t;->e:Lj5/a;

    iput-object p6, p0, Lc0/t;->f:Lj5/a;

    iput-object p7, p0, Lc0/t;->g:Lj5/a;

    iput-object p8, p0, Lc0/t;->h:Lj5/a;

    iput-object p9, p0, Lc0/t;->i:Lj5/a;

    return-void
.end method

.method public static a(Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;)Lc0/t;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Landroid/content/Context;",
            ">;",
            "Lj5/a<",
            "Lw/e;",
            ">;",
            "Lj5/a<",
            "Ld0/d;",
            ">;",
            "Lj5/a<",
            "Lc0/y;",
            ">;",
            "Lj5/a<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lj5/a<",
            "Le0/b;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Ld0/c;",
            ">;)",
            "Lc0/t;"
        }
    .end annotation

    new-instance v10, Lc0/t;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lc0/t;-><init>(Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;)V

    return-object v10
.end method

.method public static c(Landroid/content/Context;Lw/e;Ld0/d;Lc0/y;Ljava/util/concurrent/Executor;Le0/b;Lf0/a;Lf0/a;Ld0/c;)Lc0/s;
    .locals 11

    new-instance v10, Lc0/s;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lc0/s;-><init>(Landroid/content/Context;Lw/e;Ld0/d;Lc0/y;Ljava/util/concurrent/Executor;Le0/b;Lf0/a;Lf0/a;Ld0/c;)V

    return-object v10
.end method


# virtual methods
.method public b()Lc0/s;
    .locals 10

    iget-object v0, p0, Lc0/t;->a:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/content/Context;

    iget-object v0, p0, Lc0/t;->b:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lw/e;

    iget-object v0, p0, Lc0/t;->c:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ld0/d;

    iget-object v0, p0, Lc0/t;->d:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lc0/y;

    iget-object v0, p0, Lc0/t;->e:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lc0/t;->f:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Le0/b;

    iget-object v0, p0, Lc0/t;->g:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lf0/a;

    iget-object v0, p0, Lc0/t;->h:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lf0/a;

    iget-object v0, p0, Lc0/t;->i:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ld0/c;

    invoke-static/range {v1 .. v9}, Lc0/t;->c(Landroid/content/Context;Lw/e;Ld0/d;Lc0/y;Ljava/util/concurrent/Executor;Le0/b;Lf0/a;Lf0/a;Ld0/c;)Lc0/s;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lc0/t;->b()Lc0/s;

    move-result-object v0

    return-object v0
.end method
