.class public final Lc0/x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lx/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lx/b<",
        "Lc0/w;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Ld0/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lc0/y;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Le0/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lj5/a;Lj5/a;Lj5/a;Lj5/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lj5/a<",
            "Ld0/d;",
            ">;",
            "Lj5/a<",
            "Lc0/y;",
            ">;",
            "Lj5/a<",
            "Le0/b;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc0/x;->a:Lj5/a;

    iput-object p2, p0, Lc0/x;->b:Lj5/a;

    iput-object p3, p0, Lc0/x;->c:Lj5/a;

    iput-object p4, p0, Lc0/x;->d:Lj5/a;

    return-void
.end method

.method public static a(Lj5/a;Lj5/a;Lj5/a;Lj5/a;)Lc0/x;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lj5/a<",
            "Ld0/d;",
            ">;",
            "Lj5/a<",
            "Lc0/y;",
            ">;",
            "Lj5/a<",
            "Le0/b;",
            ">;)",
            "Lc0/x;"
        }
    .end annotation

    new-instance v0, Lc0/x;

    invoke-direct {v0, p0, p1, p2, p3}, Lc0/x;-><init>(Lj5/a;Lj5/a;Lj5/a;Lj5/a;)V

    return-object v0
.end method

.method public static c(Ljava/util/concurrent/Executor;Ld0/d;Lc0/y;Le0/b;)Lc0/w;
    .locals 1

    new-instance v0, Lc0/w;

    invoke-direct {v0, p0, p1, p2, p3}, Lc0/w;-><init>(Ljava/util/concurrent/Executor;Ld0/d;Lc0/y;Le0/b;)V

    return-object v0
.end method


# virtual methods
.method public b()Lc0/w;
    .locals 4

    iget-object v0, p0, Lc0/x;->a:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lc0/x;->b:Lj5/a;

    invoke-interface {v1}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ld0/d;

    iget-object v2, p0, Lc0/x;->c:Lj5/a;

    invoke-interface {v2}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lc0/y;

    iget-object v3, p0, Lc0/x;->d:Lj5/a;

    invoke-interface {v3}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Le0/b;

    invoke-static {v0, v1, v2, v3}, Lc0/x;->c(Ljava/util/concurrent/Executor;Ld0/d;Lc0/y;Le0/b;)Lc0/w;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lc0/x;->b()Lc0/w;

    move-result-object v0

    return-object v0
.end method
