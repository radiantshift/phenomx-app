.class public final Lg1/c;
.super Lz0/h;
.source ""


# instance fields
.field private final a:Le2/a0;

.field private final b:Le2/z;

.field private c:Le2/j0;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lz0/h;-><init>()V

    new-instance v0, Le2/a0;

    invoke-direct {v0}, Le2/a0;-><init>()V

    iput-object v0, p0, Lg1/c;->a:Le2/a0;

    new-instance v0, Le2/z;

    invoke-direct {v0}, Le2/z;-><init>()V

    iput-object v0, p0, Lg1/c;->b:Le2/z;

    return-void
.end method


# virtual methods
.method protected b(Lz0/e;Ljava/nio/ByteBuffer;)Lz0/a;
    .locals 6

    iget-object v0, p0, Lg1/c;->c:Le2/j0;

    if-eqz v0, :cond_0

    iget-wide v1, p1, Lz0/e;->n:J

    invoke-virtual {v0}, Le2/j0;->e()J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Le2/j0;

    iget-wide v1, p1, Lk0/g;->j:J

    invoke-direct {v0, v1, v2}, Le2/j0;-><init>(J)V

    iput-object v0, p0, Lg1/c;->c:Le2/j0;

    iget-wide v1, p1, Lk0/g;->j:J

    iget-wide v3, p1, Lz0/e;->n:J

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Le2/j0;->a(J)J

    :cond_1
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object p1

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result p2

    iget-object v0, p0, Lg1/c;->a:Le2/a0;

    invoke-virtual {v0, p1, p2}, Le2/a0;->P([BI)V

    iget-object v0, p0, Lg1/c;->b:Le2/z;

    invoke-virtual {v0, p1, p2}, Le2/z;->o([BI)V

    iget-object p1, p0, Lg1/c;->b:Le2/z;

    const/16 p2, 0x27

    invoke-virtual {p1, p2}, Le2/z;->r(I)V

    iget-object p1, p0, Lg1/c;->b:Le2/z;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Le2/z;->h(I)I

    move-result p1

    int-to-long v0, p1

    const/16 p1, 0x20

    shl-long/2addr v0, p1

    iget-object v2, p0, Lg1/c;->b:Le2/z;

    invoke-virtual {v2, p1}, Le2/z;->h(I)I

    move-result p1

    int-to-long v2, p1

    or-long/2addr v0, v2

    iget-object p1, p0, Lg1/c;->b:Le2/z;

    const/16 v2, 0x14

    invoke-virtual {p1, v2}, Le2/z;->r(I)V

    iget-object p1, p0, Lg1/c;->b:Le2/z;

    const/16 v2, 0xc

    invoke-virtual {p1, v2}, Le2/z;->h(I)I

    move-result p1

    iget-object v2, p0, Lg1/c;->b:Le2/z;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Le2/z;->h(I)I

    move-result v2

    const/4 v3, 0x0

    iget-object v4, p0, Lg1/c;->a:Le2/a0;

    const/16 v5, 0xe

    invoke-virtual {v4, v5}, Le2/a0;->S(I)V

    if-eqz v2, :cond_6

    const/16 v4, 0xff

    if-eq v2, v4, :cond_5

    const/4 p1, 0x4

    if-eq v2, p1, :cond_4

    const/4 p1, 0x5

    if-eq v2, p1, :cond_3

    const/4 p1, 0x6

    if-eq v2, p1, :cond_2

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lg1/c;->a:Le2/a0;

    iget-object v2, p0, Lg1/c;->c:Le2/j0;

    invoke-static {p1, v0, v1, v2}, Lg1/g;->d(Le2/a0;JLe2/j0;)Lg1/g;

    move-result-object v3

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lg1/c;->a:Le2/a0;

    iget-object v2, p0, Lg1/c;->c:Le2/j0;

    invoke-static {p1, v0, v1, v2}, Lg1/d;->d(Le2/a0;JLe2/j0;)Lg1/d;

    move-result-object v3

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lg1/c;->a:Le2/a0;

    invoke-static {p1}, Lg1/f;->d(Le2/a0;)Lg1/f;

    move-result-object v3

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lg1/c;->a:Le2/a0;

    invoke-static {v2, p1, v0, v1}, Lg1/a;->d(Le2/a0;IJ)Lg1/a;

    move-result-object v3

    goto :goto_0

    :cond_6
    new-instance v3, Lg1/e;

    invoke-direct {v3}, Lg1/e;-><init>()V

    :goto_0
    const/4 p1, 0x0

    if-nez v3, :cond_7

    new-instance p2, Lz0/a;

    new-array p1, p1, [Lz0/a$b;

    invoke-direct {p2, p1}, Lz0/a;-><init>([Lz0/a$b;)V

    goto :goto_1

    :cond_7
    new-instance v0, Lz0/a;

    new-array p2, p2, [Lz0/a$b;

    aput-object v3, p2, p1

    invoke-direct {v0, p2}, Lz0/a;-><init>([Lz0/a$b;)V

    move-object p2, v0

    :goto_1
    return-object p2
.end method
