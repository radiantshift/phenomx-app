.class final enum Lg4/d$c;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg4/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lg4/d$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum f:Lg4/d$c;

.field public static final enum g:Lg4/d$c;

.field public static final enum h:Lg4/d$c;

.field public static final enum i:Lg4/d$c;

.field public static final enum j:Lg4/d$c;

.field private static final synthetic k:[Lg4/d$c;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    new-instance v0, Lg4/d$c;

    const-string v1, "none"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lg4/d$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lg4/d$c;->f:Lg4/d$c;

    new-instance v1, Lg4/d$c;

    const-string v3, "loading"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lg4/d$c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lg4/d$c;->g:Lg4/d$c;

    new-instance v3, Lg4/d$c;

    const-string v5, "buffering"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lg4/d$c;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lg4/d$c;->h:Lg4/d$c;

    new-instance v5, Lg4/d$c;

    const-string v7, "ready"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lg4/d$c;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lg4/d$c;->i:Lg4/d$c;

    new-instance v7, Lg4/d$c;

    const-string v9, "completed"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lg4/d$c;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lg4/d$c;->j:Lg4/d$c;

    const/4 v9, 0x5

    new-array v9, v9, [Lg4/d$c;

    aput-object v0, v9, v2

    aput-object v1, v9, v4

    aput-object v3, v9, v6

    aput-object v5, v9, v8

    aput-object v7, v9, v10

    sput-object v9, Lg4/d$c;->k:[Lg4/d$c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lg4/d$c;
    .locals 1

    const-class v0, Lg4/d$c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lg4/d$c;

    return-object p0
.end method

.method public static values()[Lg4/d$c;
    .locals 1

    sget-object v0, Lg4/d$c;->k:[Lg4/d$c;

    invoke-virtual {v0}, [Lg4/d$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lg4/d$c;

    return-object v0
.end method
