.class public Lg4/d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz4/k$c;
.implements Lh0/g3$d;
.implements Lz0/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg4/d$c;
    }
.end annotation


# static fields
.field private static N:Ljava/util/Random;


# instance fields
.field private A:Lh0/w1;

.field private B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/media/audiofx/AudioEffect;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/media/audiofx/AudioEffect;",
            ">;"
        }
    .end annotation
.end field

.field private E:I

.field private F:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private G:Lh0/s;

.field private H:Lm0/i;

.field private I:Ljava/lang/Integer;

.field private J:Lj1/x;

.field private K:Ljava/lang/Integer;

.field private final L:Landroid/os/Handler;

.field private final M:Ljava/lang/Runnable;

.field private final f:Landroid/content/Context;

.field private final g:Lz4/k;

.field private final h:Lg4/e;

.field private final i:Lg4/e;

.field private j:Lg4/d$c;

.field private k:J

.field private l:J

.field private m:J

.field private n:Ljava/lang/Long;

.field private o:J

.field private p:Ljava/lang/Integer;

.field private q:Lz4/k$d;

.field private r:Lz4/k$d;

.field private s:Lz4/k$d;

.field private t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lj1/x;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ld1/c;

.field private v:Ld1/b;

.field private w:I

.field private x:Lj0/e;

.field private y:Lh0/x1;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lg4/d;->N:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lz4/c;Ljava/lang/String;Ljava/util/Map;Ljava/util/List;Ljava/lang/Boolean;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lz4/c;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "**>;",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lg4/d;->t:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lg4/d;->C:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lg4/d;->D:Ljava/util/Map;

    const/4 v0, 0x0

    iput v0, p0, Lg4/d;->E:I

    new-instance v1, Lm0/i;

    invoke-direct {v1}, Lm0/i;-><init>()V

    iput-object v1, p0, Lg4/d;->H:Lm0/i;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lg4/d;->L:Landroid/os/Handler;

    new-instance v1, Lg4/d$a;

    invoke-direct {v1, p0}, Lg4/d$a;-><init>(Lg4/d;)V

    iput-object v1, p0, Lg4/d;->M:Ljava/lang/Runnable;

    iput-object p1, p0, Lg4/d;->f:Landroid/content/Context;

    iput-object p5, p0, Lg4/d;->B:Ljava/util/List;

    if-eqz p6, :cond_0

    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lg4/d;->z:Z

    new-instance p1, Lz4/k;

    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string p6, "com.ryanheise.just_audio.methods."

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    invoke-direct {p1, p2, p5}, Lz4/k;-><init>(Lz4/c;Ljava/lang/String;)V

    iput-object p1, p0, Lg4/d;->g:Lz4/k;

    invoke-virtual {p1, p0}, Lz4/k;->e(Lz4/k$c;)V

    new-instance p1, Lg4/e;

    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string p6, "com.ryanheise.just_audio.events."

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    invoke-direct {p1, p2, p5}, Lg4/e;-><init>(Lz4/c;Ljava/lang/String;)V

    iput-object p1, p0, Lg4/d;->h:Lg4/e;

    new-instance p1, Lg4/e;

    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string p6, "com.ryanheise.just_audio.data."

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lg4/e;-><init>(Lz4/c;Ljava/lang/String;)V

    iput-object p1, p0, Lg4/d;->i:Lg4/e;

    sget-object p1, Lg4/d$c;->f:Lg4/d$c;

    iput-object p1, p0, Lg4/d;->j:Lg4/d$c;

    iget-object p1, p0, Lg4/d;->H:Lm0/i;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lm0/i;->h(Z)Lm0/i;

    if-eqz p4, :cond_3

    const-string p1, "androidLoadControl"

    invoke-interface {p4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    const-wide/16 p2, 0x3e8

    if-eqz p1, :cond_2

    new-instance p5, Lh0/k$a;

    invoke-direct {p5}, Lh0/k$a;-><init>()V

    const-string p6, "minBufferDuration"

    invoke-interface {p1, p6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p6

    invoke-static {p6}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object p6

    invoke-virtual {p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    div-long/2addr v1, p2

    long-to-int p6, v1

    const-string v1, "maxBufferDuration"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    div-long/2addr v1, p2

    long-to-int v2, v1

    const-string v1, "bufferForPlaybackDuration"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    div-long/2addr v3, p2

    long-to-int v1, v3

    const-string v3, "bufferForPlaybackAfterRebufferDuration"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    div-long/2addr v3, p2

    long-to-int v4, v3

    invoke-virtual {p5, p6, v2, v1, v4}, Lh0/k$a;->c(IIII)Lh0/k$a;

    move-result-object p5

    const-string p6, "prioritizeTimeOverSizeThresholds"

    invoke-interface {p1, p6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p6

    check-cast p6, Ljava/lang/Boolean;

    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p6

    invoke-virtual {p5, p6}, Lh0/k$a;->d(Z)Lh0/k$a;

    move-result-object p5

    const-string p6, "backBufferDuration"

    invoke-interface {p1, p6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p6

    invoke-static {p6}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object p6

    invoke-virtual {p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    div-long/2addr v1, p2

    long-to-int p6, v1

    invoke-virtual {p5, p6, v0}, Lh0/k$a;->b(IZ)Lh0/k$a;

    move-result-object p5

    const-string p6, "targetBufferBytes"

    invoke-interface {p1, p6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1, p6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p5, p1}, Lh0/k$a;->e(I)Lh0/k$a;

    :cond_1
    invoke-virtual {p5}, Lh0/k$a;->a()Lh0/k;

    move-result-object p1

    iput-object p1, p0, Lg4/d;->y:Lh0/x1;

    :cond_2
    const-string p1, "androidLivePlaybackSpeedControl"

    invoke-interface {p4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-eqz p1, :cond_3

    new-instance p4, Lh0/j$b;

    invoke-direct {p4}, Lh0/j$b;-><init>()V

    const-string p5, "fallbackMinPlaybackSpeed"

    invoke-interface {p1, p5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Double;

    invoke-virtual {p5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p5

    double-to-float p5, p5

    invoke-virtual {p4, p5}, Lh0/j$b;->c(F)Lh0/j$b;

    move-result-object p4

    const-string p5, "fallbackMaxPlaybackSpeed"

    invoke-interface {p1, p5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Double;

    invoke-virtual {p5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p5

    double-to-float p5, p5

    invoke-virtual {p4, p5}, Lh0/j$b;->b(F)Lh0/j$b;

    move-result-object p4

    const-string p5, "minUpdateInterval"

    invoke-interface {p1, p5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    invoke-static {p5}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object p5

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p5

    div-long/2addr p5, p2

    invoke-virtual {p4, p5, p6}, Lh0/j$b;->f(J)Lh0/j$b;

    move-result-object p4

    const-string p5, "proportionalControlFactor"

    invoke-interface {p1, p5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Double;

    invoke-virtual {p5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p5

    double-to-float p5, p5

    invoke-virtual {p4, p5}, Lh0/j$b;->g(F)Lh0/j$b;

    move-result-object p4

    const-string p5, "maxLiveOffsetErrorForUnitSpeed"

    invoke-interface {p1, p5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    invoke-static {p5}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object p5

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p5

    div-long/2addr p5, p2

    invoke-virtual {p4, p5, p6}, Lh0/j$b;->d(J)Lh0/j$b;

    move-result-object p4

    const-string p5, "targetLiveOffsetIncrementOnRebuffer"

    invoke-interface {p1, p5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    invoke-static {p5}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object p5

    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    move-result-wide p5

    div-long/2addr p5, p2

    invoke-virtual {p4, p5, p6}, Lh0/j$b;->h(J)Lh0/j$b;

    move-result-object p2

    const-string p3, "minPossibleLiveOffsetSmoothingFactor"

    invoke-interface {p1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p3

    double-to-float p1, p3

    invoke-virtual {p2, p1}, Lh0/j$b;->e(F)Lh0/j$b;

    move-result-object p1

    invoke-virtual {p1}, Lh0/j$b;->a()Lh0/j;

    move-result-object p1

    iput-object p1, p0, Lg4/d;->A:Lh0/w1;

    :cond_3
    return-void
.end method

.method private A0()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0}, Lg4/d;->v0()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lg4/d;->F:Ljava/util/Map;

    return-void
.end method

.method private B0()V
    .locals 3

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    if-nez v0, :cond_3

    new-instance v0, Lh0/s$b;

    iget-object v1, p0, Lg4/d;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lh0/s$b;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lg4/d;->y:Lh0/x1;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lh0/s$b;->o(Lh0/x1;)Lh0/s$b;

    :cond_0
    iget-object v1, p0, Lg4/d;->A:Lh0/w1;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Lh0/s$b;->n(Lh0/w1;)Lh0/s$b;

    :cond_1
    iget-boolean v1, p0, Lg4/d;->z:Z

    if-eqz v1, :cond_2

    new-instance v1, Lh0/m;

    iget-object v2, p0, Lg4/d;->f:Landroid/content/Context;

    invoke-direct {v1, v2}, Lh0/m;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lh0/m;->j(Z)Lh0/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/s$b;->p(Lh0/t3;)Lh0/s$b;

    :cond_2
    invoke-virtual {v0}, Lh0/s$b;->g()Lh0/s;

    move-result-object v0

    iput-object v0, p0, Lg4/d;->G:Lh0/s;

    iget-boolean v1, p0, Lg4/d;->z:Z

    invoke-interface {v0, v1}, Lh0/s;->A(Z)V

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0}, Lh0/s;->O()I

    move-result v0

    invoke-direct {p0, v0}, Lg4/d;->W0(I)V

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0, p0}, Lh0/g3;->G(Lh0/g3$d;)V

    :cond_3
    return-void
.end method

.method private C0()Ljava/util/Map;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lg4/d;->D:Ljava/util/Map;

    const-string v1, "AndroidEqualizer"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/audiofx/Equalizer;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v0}, Landroid/media/audiofx/Equalizer;->getNumberOfBands()S

    move-result v4

    const/4 v5, 0x5

    const/4 v6, 0x4

    const/4 v7, 0x3

    const/4 v8, 0x6

    const/4 v9, 0x2

    const-wide v10, 0x408f400000000000L    # 1000.0

    const/4 v12, 0x1

    if-ge v3, v4, :cond_0

    const/16 v4, 0xa

    new-array v4, v4, [Ljava/lang/Object;

    const-string v13, "index"

    aput-object v13, v4, v2

    invoke-static {v3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    aput-object v13, v4, v12

    const-string v13, "lowerFrequency"

    aput-object v13, v4, v9

    invoke-virtual {v0, v3}, Landroid/media/audiofx/Equalizer;->getBandFreqRange(S)[I

    move-result-object v9

    aget v9, v9, v2

    int-to-double v13, v9

    div-double/2addr v13, v10

    invoke-static {v13, v14}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v4, v7

    const-string v7, "upperFrequency"

    aput-object v7, v4, v6

    invoke-virtual {v0, v3}, Landroid/media/audiofx/Equalizer;->getBandFreqRange(S)[I

    move-result-object v6

    aget v6, v6, v12

    int-to-double v6, v6

    div-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const-string v5, "centerFrequency"

    aput-object v5, v4, v8

    const/4 v5, 0x7

    invoke-virtual {v0, v3}, Landroid/media/audiofx/Equalizer;->getCenterFreq(S)I

    move-result v6

    int-to-double v6, v6

    div-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "gain"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    invoke-virtual {v0, v3}, Landroid/media/audiofx/Equalizer;->getBandLevel(S)S

    move-result v6

    int-to-double v6, v6

    div-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, Lg4/d;->Q0([Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    int-to-short v3, v3

    goto :goto_0

    :cond_0
    new-array v3, v9, [Ljava/lang/Object;

    const-string v4, "parameters"

    aput-object v4, v3, v2

    new-array v4, v8, [Ljava/lang/Object;

    const-string v8, "minDecibels"

    aput-object v8, v4, v2

    invoke-virtual {v0}, Landroid/media/audiofx/Equalizer;->getBandLevelRange()[S

    move-result-object v8

    aget-short v2, v8, v2

    int-to-double v13, v2

    div-double/2addr v13, v10

    invoke-static {v13, v14}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v4, v12

    const-string v2, "maxDecibels"

    aput-object v2, v4, v9

    invoke-virtual {v0}, Landroid/media/audiofx/Equalizer;->getBandLevelRange()[S

    move-result-object v0

    aget-short v0, v0, v12

    int-to-double v8, v0

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v7

    const-string v0, "bands"

    aput-object v0, v4, v6

    aput-object v1, v4, v5

    invoke-static {v4}, Lg4/d;->Q0([Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    aput-object v0, v3, v12

    invoke-static {v3}, Lg4/d;->Q0([Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private D0(ID)V
    .locals 3

    iget-object v0, p0, Lg4/d;->D:Ljava/util/Map;

    const-string v1, "AndroidEqualizer"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/audiofx/Equalizer;

    int-to-short p1, p1

    const-wide v1, 0x408f400000000000L    # 1000.0

    mul-double p2, p2, v1

    invoke-static {p2, p3}, Ljava/lang/Math;->round(D)J

    move-result-wide p2

    long-to-int p3, p2

    int-to-short p2, p3

    invoke-virtual {v0, p1, p2}, Landroid/media/audiofx/Equalizer;->setBandLevel(SS)V

    return-void
.end method

.method private E0(Ljava/lang/Object;)Lj1/x;
    .locals 2

    check-cast p1, Ljava/util/Map;

    const-string v0, "id"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lg4/d;->t:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/x;

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lg4/d;->x0(Ljava/lang/Object;)Lj1/x;

    move-result-object v1

    iget-object p1, p0, Lg4/d;->t:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v1
.end method

.method public static synthetic F(Lz4/k$d;)V
    .locals 0

    invoke-static {p0}, Lg4/d;->M0(Lz4/k$d;)V

    return-void
.end method

.method private F0(Ljava/lang/Object;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List<",
            "Lj1/x;",
            ">;"
        }
    .end annotation

    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_1

    check-cast p1, Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v2}, Lg4/d;->E0(Ljava/lang/Object;)Lj1/x;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "List expected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private G0(Ljava/lang/Object;)[Lj1/x;
    .locals 1

    invoke-direct {p0, p1}, Lg4/d;->F0(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lj1/x;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    return-object v0
.end method

.method public static synthetic H(Lz4/k$d;)V
    .locals 0

    invoke-static {p0}, Lg4/d;->K0(Lz4/k$d;)V

    return-void
.end method

.method private H0()J
    .locals 5

    iget-wide v0, p0, Lg4/d;->o:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-wide v0

    :cond_0
    iget-object v0, p0, Lg4/d;->j:Lg4/d$c;

    sget-object v1, Lg4/d$c;->f:Lg4/d$c;

    if-eq v0, v1, :cond_3

    sget-object v1, Lg4/d$c;->g:Lg4/d$c;

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lg4/d;->n:Ljava/lang/Long;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    iget-object v0, p0, Lg4/d;->n:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_2
    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0}, Lh0/g3;->S()J

    move-result-wide v0

    return-wide v0

    :cond_3
    :goto_0
    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0}, Lh0/g3;->S()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gez v4, :cond_4

    move-wide v0, v2

    :cond_4
    return-wide v0
.end method

.method public static synthetic I(Lz4/k$d;)V
    .locals 0

    invoke-static {p0}, Lg4/d;->L0(Lz4/k$d;)V

    return-void
.end method

.method private I0()J
    .locals 2

    iget-object v0, p0, Lg4/d;->j:Lg4/d$c;

    sget-object v1, Lg4/d$c;->f:Lg4/d$c;

    if-eq v0, v1, :cond_1

    sget-object v1, Lg4/d$c;->g:Lg4/d$c;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0}, Lh0/g3;->M()J

    move-result-wide v0

    return-wide v0

    :cond_1
    :goto_0
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    return-wide v0
.end method

.method public static J0(Ljava/lang/Object;)Ljava/lang/Long;
    .locals 2

    if-eqz p0, :cond_1

    instance-of v0, p0, Ljava/lang/Long;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    int-to-long v0, p0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    goto :goto_1

    :cond_1
    :goto_0
    check-cast p0, Ljava/lang/Long;

    :goto_1
    return-object p0
.end method

.method private K()V
    .locals 2

    const-string v0, "abort"

    const-string v1, "Connection aborted"

    invoke-direct {p0, v0, v1}, Lg4/d;->U0(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static synthetic K0(Lz4/k$d;)V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p0, v0}, Lz4/k$d;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private L()V
    .locals 2

    iget-object v0, p0, Lg4/d;->s:Lz4/k$d;

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, v1}, Lz4/k$d;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lg4/d;->s:Lz4/k$d;

    iput-object v0, p0, Lg4/d;->n:Ljava/lang/Long;

    :cond_0
    return-void
.end method

.method private static synthetic L0(Lz4/k$d;)V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p0, v0}, Lz4/k$d;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private static synthetic M0(Lz4/k$d;)V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p0, v0}, Lz4/k$d;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private N0(Lj1/x;JLjava/lang/Integer;Lz4/k$d;)V
    .locals 0

    iput-wide p2, p0, Lg4/d;->o:J

    iput-object p4, p0, Lg4/d;->p:Ljava/lang/Integer;

    const/4 p2, 0x0

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result p3

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    iput-object p3, p0, Lg4/d;->K:Ljava/lang/Integer;

    sget-object p3, Lg4/d$b;->a:[I

    iget-object p4, p0, Lg4/d;->j:Lg4/d$c;

    invoke-virtual {p4}, Ljava/lang/Enum;->ordinal()I

    move-result p4

    aget p3, p3, p4

    const/4 p4, 0x1

    if-eq p3, p4, :cond_2

    const/4 p4, 0x2

    if-eq p3, p4, :cond_1

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lg4/d;->K()V

    :goto_1
    iget-object p3, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p3}, Lh0/g3;->b()V

    :cond_2
    iput p2, p0, Lg4/d;->w:I

    iput-object p5, p0, Lg4/d;->q:Lz4/k$d;

    invoke-direct {p0}, Lg4/d;->g1()V

    sget-object p2, Lg4/d$c;->g:Lg4/d$c;

    iput-object p2, p0, Lg4/d;->j:Lg4/d$c;

    invoke-direct {p0}, Lg4/d;->A0()V

    iput-object p1, p0, Lg4/d;->J:Lj1/x;

    iget-object p2, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p2, p1}, Lh0/s;->w(Lj1/x;)V

    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1}, Lh0/g3;->c()V

    return-void
.end method

.method private O0(D)V
    .locals 2

    const-wide v0, 0x408f400000000000L    # 1000.0

    mul-double p1, p1, v0

    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide p1

    long-to-int p2, p1

    iget-object p1, p0, Lg4/d;->D:Ljava/util/Map;

    const-string v0, "AndroidLoudnessEnhancer"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/media/audiofx/LoudnessEnhancer;

    invoke-virtual {p1, p2}, Landroid/media/audiofx/LoudnessEnhancer;->setTargetGain(I)V

    return-void
.end method

.method static P0(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    instance-of v0, p0, Ljava/util/Map;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/util/Map;

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static synthetic Q(Lg4/d;)Lh0/s;
    .locals 0

    iget-object p0, p0, Lg4/d;->G:Lh0/s;

    return-object p0
.end method

.method static varargs Q0([Ljava/lang/Object;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    aget-object v2, p0, v1

    check-cast v2, Ljava/lang/String;

    add-int/lit8 v3, v1, 0x1

    aget-object v3, p0, v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static synthetic U(Lg4/d;)J
    .locals 2

    iget-wide v0, p0, Lg4/d;->m:J

    return-wide v0
.end method

.method private U0(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lg4/d;->q:Lz4/k$d;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, v1}, Lz4/k$d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v1, p0, Lg4/d;->q:Lz4/k$d;

    :cond_0
    iget-object v0, p0, Lg4/d;->h:Lg4/e;

    invoke-virtual {v0, p1, p2, v1}, Lg4/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private V0(III)V
    .locals 1

    new-instance v0, Lj0/e$e;

    invoke-direct {v0}, Lj0/e$e;-><init>()V

    invoke-virtual {v0, p1}, Lj0/e$e;->c(I)Lj0/e$e;

    invoke-virtual {v0, p2}, Lj0/e$e;->d(I)Lj0/e$e;

    invoke-virtual {v0, p3}, Lj0/e$e;->f(I)Lj0/e$e;

    invoke-virtual {v0}, Lj0/e$e;->a()Lj0/e;

    move-result-object p1

    iget-object p2, p0, Lg4/d;->j:Lg4/d$c;

    sget-object p3, Lg4/d$c;->g:Lg4/d$c;

    if-ne p2, p3, :cond_0

    iput-object p1, p0, Lg4/d;->x:Lj0/e;

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lg4/d;->G:Lh0/s;

    const/4 p3, 0x0

    invoke-interface {p2, p1, p3}, Lh0/s;->o(Lj0/e;Z)V

    :goto_0
    return-void
.end method

.method private W0(I)V
    .locals 4

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lg4/d;->I:Ljava/lang/Integer;

    invoke-direct {p0}, Lg4/d;->r0()V

    iget-object p1, p0, Lg4/d;->I:Ljava/lang/Integer;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lg4/d;->B:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map;

    iget-object v2, p0, Lg4/d;->I:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v2}, Lg4/d;->w0(Ljava/lang/Object;I)Landroid/media/audiofx/AudioEffect;

    move-result-object v0

    const-string v2, "enabled"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/media/audiofx/AudioEffect;->setEnabled(Z)I

    :cond_1
    iget-object v2, p0, Lg4/d;->C:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lg4/d;->D:Ljava/util/Map;

    const-string v3, "type"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lg4/d;->A0()V

    return-void
.end method

.method private a1(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Ljava/util/Map;

    const-string v0, "id"

    invoke-static {p1, v0}, Lg4/d;->P0(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lg4/d;->t:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/x;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v1, "type"

    invoke-static {p1, v1}, Lg4/d;->P0(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    const-string v2, "concatenating"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v0, "looping"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    const-string v0, "child"

    invoke-static {p1, v0}, Lg4/d;->P0(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-direct {p0, p1}, Lg4/d;->a1(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    check-cast v0, Lj1/k;

    const-string v1, "shuffleOrder"

    invoke-static {p1, v1}, Lg4/d;->P0(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {p0, v1}, Lg4/d;->y0(Ljava/util/List;)Lj1/s0;

    move-result-object v1

    invoke-virtual {v0, v1}, Lj1/k;->u0(Lj1/s0;)V

    const-string v0, "children"

    invoke-static {p1, v0}, Lg4/d;->P0(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lg4/d;->a1(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method

.method static synthetic b0(Lg4/d;)V
    .locals 0

    invoke-direct {p0}, Lg4/d;->g0()V

    return-void
.end method

.method static synthetic d0(Lg4/d;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lg4/d;->L:Landroid/os/Handler;

    return-object p0
.end method

.method private e0(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lg4/d;->D:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/media/audiofx/AudioEffect;

    invoke-virtual {p1, p2}, Landroid/media/audiofx/AudioEffect;->setEnabled(Z)I

    return-void
.end method

.method private e1()V
    .locals 2

    iget-object v0, p0, Lg4/d;->L:Landroid/os/Handler;

    iget-object v1, p0, Lg4/d;->M:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lg4/d;->L:Landroid/os/Handler;

    iget-object v1, p0, Lg4/d;->M:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private f1()Z
    .locals 2

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0}, Lh0/g3;->E()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lg4/d;->K:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lg4/d;->K:Ljava/lang/Integer;

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private g0()V
    .locals 0

    invoke-direct {p0}, Lg4/d;->A0()V

    invoke-direct {p0}, Lg4/d;->h0()V

    return-void
.end method

.method private g1()V
    .locals 2

    invoke-direct {p0}, Lg4/d;->H0()J

    move-result-wide v0

    iput-wide v0, p0, Lg4/d;->k:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lg4/d;->l:J

    return-void
.end method

.method private h0()V
    .locals 2

    iget-object v0, p0, Lg4/d;->F:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lg4/d;->h:Lg4/e;

    invoke-virtual {v1, v0}, Lg4/e;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lg4/d;->F:Ljava/util/Map;

    :cond_0
    return-void
.end method

.method private h1()Z
    .locals 5

    invoke-direct {p0}, Lg4/d;->H0()J

    move-result-wide v0

    iget-wide v2, p0, Lg4/d;->k:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-direct {p0}, Lg4/d;->H0()J

    move-result-wide v0

    iput-wide v0, p0, Lg4/d;->k:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lg4/d;->l:J

    const/4 v0, 0x1

    return v0
.end method

.method private m0()Ld2/l$a;
    .locals 3

    iget-object v0, p0, Lg4/d;->f:Landroid/content/Context;

    const-string v1, "just_audio"

    invoke-static {v0, v1}, Le2/n0;->l0(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ld2/u$b;

    invoke-direct {v1}, Ld2/u$b;-><init>()V

    invoke-virtual {v1, v0}, Ld2/u$b;->e(Ljava/lang/String;)Ld2/u$b;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ld2/u$b;->c(Z)Ld2/u$b;

    move-result-object v0

    new-instance v1, Ld2/t$a;

    iget-object v2, p0, Lg4/d;->f:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Ld2/t$a;-><init>(Landroid/content/Context;Ld2/l$a;)V

    return-object v1
.end method

.method private r0()V
    .locals 2

    iget-object v0, p0, Lg4/d;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/audiofx/AudioEffect;

    invoke-virtual {v1}, Landroid/media/audiofx/AudioEffect;->release()V

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lg4/d;->D:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method private s0()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lg4/d;->u:Ld1/c;

    const-string v2, "url"

    if-eqz v1, :cond_0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v3, p0, Lg4/d;->u:Ld1/c;

    iget-object v3, v3, Ld1/c;->g:Ljava/lang/String;

    const-string v4, "title"

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lg4/d;->u:Ld1/c;

    iget-object v3, v3, Ld1/c;->h:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "info"

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lg4/d;->v:Ld1/b;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v3, p0, Lg4/d;->v:Ld1/b;

    iget v3, v3, Ld1/b;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "bitrate"

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lg4/d;->v:Ld1/b;

    iget-object v3, v3, Ld1/b;->g:Ljava/lang/String;

    const-string v4, "genre"

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lg4/d;->v:Ld1/b;

    iget-object v3, v3, Ld1/b;->h:Ljava/lang/String;

    const-string v4, "name"

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lg4/d;->v:Ld1/b;

    iget v3, v3, Ld1/b;->k:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "metadataInterval"

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lg4/d;->v:Ld1/b;

    iget-object v3, v3, Ld1/b;->i:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lg4/d;->v:Ld1/b;

    iget-boolean v2, v2, Ld1/b;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "isPublic"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "headers"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method private t0()V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Lg4/d;->n:Ljava/lang/Long;

    iget-object v1, p0, Lg4/d;->s:Lz4/k$d;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v1, v2}, Lz4/k$d;->a(Ljava/lang/Object;)V

    iput-object v0, p0, Lg4/d;->s:Lz4/k$d;

    return-void
.end method

.method private u0(Ljava/lang/Object;)Lj1/k;
    .locals 1

    iget-object v0, p0, Lg4/d;->t:Ljava/util/Map;

    check-cast p1, Ljava/lang/String;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/k;

    return-object p1
.end method

.method private v0()Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0}, Lg4/d;->I0()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v7, v1, v5

    if-nez v7, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lg4/d;->I0()J

    move-result-wide v1

    mul-long v1, v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_0
    iget-object v2, p0, Lg4/d;->G:Lh0/s;

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lh0/g3;->r()J

    move-result-wide v5

    goto :goto_1

    :cond_1
    const-wide/16 v5, 0x0

    :goto_1
    iput-wide v5, p0, Lg4/d;->m:J

    iget-object v2, p0, Lg4/d;->j:Lg4/d$c;

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v5, "processingState"

    invoke-interface {v0, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v5, p0, Lg4/d;->k:J

    mul-long v5, v5, v3

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v5, "updatePosition"

    invoke-interface {v0, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v5, p0, Lg4/d;->l:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v5, "updateTime"

    invoke-interface {v0, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v5, p0, Lg4/d;->k:J

    iget-wide v7, p0, Lg4/d;->m:J

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    mul-long v5, v5, v3

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "bufferedPosition"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lg4/d;->s0()Ljava/util/Map;

    move-result-object v2

    const-string v3, "icyMetadata"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "duration"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lg4/d;->K:Ljava/lang/Integer;

    const-string v2, "currentIndex"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lg4/d;->I:Ljava/lang/Integer;

    const-string v2, "androidAudioSessionId"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method private w0(Ljava/lang/Object;I)Landroid/media/audiofx/AudioEffect;
    .locals 4

    check-cast p1, Ljava/util/Map;

    const-string v0, "type"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    const-string v2, "AndroidEqualizer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "AndroidLoudnessEnhancer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const-string v0, "targetGain"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double v0, v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int p1, v0

    new-instance v0, Landroid/media/audiofx/LoudnessEnhancer;

    invoke-direct {v0, p2}, Landroid/media/audiofx/LoudnessEnhancer;-><init>(I)V

    invoke-virtual {v0, p1}, Landroid/media/audiofx/LoudnessEnhancer;->setTargetGain(I)V

    return-object v0

    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "AndroidLoudnessEnhancer requires minSdkVersion >= 19"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown AudioEffect type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_2
    new-instance p1, Landroid/media/audiofx/Equalizer;

    const/4 v0, 0x0

    invoke-direct {p1, v0, p2}, Landroid/media/audiofx/Equalizer;-><init>(II)V

    return-object p1
.end method

.method private x0(Ljava/lang/Object;)Lj1/x;
    .locals 10

    check-cast p1, Ljava/util/Map;

    const-string v0, "id"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "type"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, -0x1

    sparse-switch v3, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v3, "silence"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v5, 0x6

    goto :goto_0

    :sswitch_1
    const-string v3, "progressive"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v5, 0x5

    goto :goto_0

    :sswitch_2
    const-string v3, "clipping"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v5, 0x4

    goto :goto_0

    :sswitch_3
    const-string v3, "looping"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_3
    const/4 v5, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "dash"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    goto :goto_0

    :cond_4
    const/4 v5, 0x2

    goto :goto_0

    :sswitch_5
    const-string v3, "hls"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_0

    :cond_5
    const/4 v5, 0x1

    goto :goto_0

    :sswitch_6
    const-string v3, "concatenating"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    goto :goto_0

    :cond_6
    const/4 v5, 0x0

    :goto_0
    const-string v2, "child"

    const-string v3, "uri"

    packed-switch v5, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown AudioSource type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v1, Lj1/t0$b;

    invoke-direct {v1}, Lj1/t0$b;-><init>()V

    const-string v2, "duration"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lj1/t0$b;->b(J)Lj1/t0$b;

    move-result-object p1

    invoke-virtual {p1, v0}, Lj1/t0$b;->c(Ljava/lang/Object;)Lj1/t0$b;

    move-result-object p1

    invoke-virtual {p1}, Lj1/t0$b;->a()Lj1/t0;

    move-result-object p1

    return-object p1

    :pswitch_1
    new-instance v1, Lj1/l0$b;

    invoke-direct {p0}, Lg4/d;->m0()Ld2/l$a;

    move-result-object v2

    iget-object v4, p0, Lg4/d;->H:Lm0/i;

    invoke-direct {v1, v2, v4}, Lj1/l0$b;-><init>(Ld2/l$a;Lm0/r;)V

    new-instance v2, Lh0/z1$c;

    invoke-direct {v2}, Lh0/z1$c;-><init>()V

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v2, p1}, Lh0/z1$c;->f(Landroid/net/Uri;)Lh0/z1$c;

    move-result-object p1

    invoke-virtual {p1, v0}, Lh0/z1$c;->e(Ljava/lang/Object;)Lh0/z1$c;

    move-result-object p1

    invoke-virtual {p1}, Lh0/z1$c;->a()Lh0/z1;

    move-result-object p1

    invoke-virtual {v1, p1}, Lj1/l0$b;->b(Lh0/z1;)Lj1/l0;

    move-result-object p1

    return-object p1

    :pswitch_2
    const-string v0, "start"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "end"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object v1

    new-instance v9, Lj1/e;

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-direct {p0, p1}, Lg4/d;->E0(Ljava/lang/Object;)Lj1/x;

    move-result-object v4

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_1

    :cond_7
    const-wide/16 v2, 0x0

    :goto_1
    move-wide v5, v2

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_2

    :cond_8
    const-wide/high16 v0, -0x8000000000000000L

    :goto_2
    move-wide v7, v0

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Lj1/e;-><init>(Lj1/x;JJ)V

    return-object v9

    :pswitch_3
    const-string v0, "count"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-direct {p0, p1}, Lg4/d;->E0(Ljava/lang/Object;)Lj1/x;

    move-result-object p1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-array v1, v0, [Lj1/x;

    :goto_3
    if-ge v4, v0, :cond_9

    aput-object p1, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_9
    new-instance p1, Lj1/k;

    invoke-direct {p1, v1}, Lj1/k;-><init>([Lj1/x;)V

    return-object p1

    :pswitch_4
    new-instance v1, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Factory;

    invoke-direct {p0}, Lg4/d;->m0()Ld2/l$a;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Factory;-><init>(Ld2/l$a;)V

    new-instance v2, Lh0/z1$c;

    invoke-direct {v2}, Lh0/z1$c;-><init>()V

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v2, p1}, Lh0/z1$c;->f(Landroid/net/Uri;)Lh0/z1$c;

    move-result-object p1

    const-string v2, "application/dash+xml"

    invoke-virtual {p1, v2}, Lh0/z1$c;->d(Ljava/lang/String;)Lh0/z1$c;

    move-result-object p1

    invoke-virtual {p1, v0}, Lh0/z1$c;->e(Ljava/lang/Object;)Lh0/z1$c;

    move-result-object p1

    invoke-virtual {p1}, Lh0/z1$c;->a()Lh0/z1;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Factory;->a(Lh0/z1;)Lcom/google/android/exoplayer2/source/dash/DashMediaSource;

    move-result-object p1

    return-object p1

    :pswitch_5
    new-instance v0, Lcom/google/android/exoplayer2/source/hls/HlsMediaSource$Factory;

    invoke-direct {p0}, Lg4/d;->m0()Ld2/l$a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/source/hls/HlsMediaSource$Factory;-><init>(Ld2/l$a;)V

    new-instance v1, Lh0/z1$c;

    invoke-direct {v1}, Lh0/z1$c;-><init>()V

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v1, p1}, Lh0/z1$c;->f(Landroid/net/Uri;)Lh0/z1$c;

    move-result-object p1

    const-string v1, "application/x-mpegURL"

    invoke-virtual {p1, v1}, Lh0/z1$c;->d(Ljava/lang/String;)Lh0/z1$c;

    move-result-object p1

    invoke-virtual {p1}, Lh0/z1$c;->a()Lh0/z1;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/source/hls/HlsMediaSource$Factory;->a(Lh0/z1;)Lcom/google/android/exoplayer2/source/hls/HlsMediaSource;

    move-result-object p1

    return-object p1

    :pswitch_6
    const-string v0, "children"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lg4/d;->G0(Ljava/lang/Object;)[Lj1/x;

    move-result-object v0

    new-instance v1, Lj1/k;

    const-string v2, "useLazyPreparation"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const-string v3, "shuffleOrder"

    invoke-static {p1, v3}, Lg4/d;->P0(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, Lg4/d;->y0(Ljava/util/List;)Lj1/s0;

    move-result-object p1

    invoke-direct {v1, v4, v2, p1, v0}, Lj1/k;-><init>(ZZLj1/s0;[Lj1/x;)V

    return-object v1

    :sswitch_data_0
    .sparse-switch
        -0x1a9425ce -> :sswitch_6
        0x193ef -> :sswitch_5
        0x2eef92 -> :sswitch_4
        0x14db9ebe -> :sswitch_3
        0x36c0fcc2 -> :sswitch_2
        0x43720b8b -> :sswitch_1
        0x7cbaf4a1 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private y0(Ljava/util/List;)Lj1/s0;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lj1/s0;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v1, v0, [I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Lj1/s0$a;

    sget-object v0, Lg4/d;->N:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-direct {p1, v1, v2, v3}, Lj1/s0$a;-><init>([IJ)V

    return-object p1
.end method


# virtual methods
.method public synthetic A(I)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->p(Lh0/g3$d;I)V

    return-void
.end method

.method public synthetic B(ZI)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->r(Lh0/g3$d;ZI)V

    return-void
.end method

.method public C(Lz4/j;Lz4/k$d;)V
    .locals 16

    move-object/from16 v7, p0

    move-object/from16 v1, p1

    move-object/from16 v8, p2

    invoke-direct/range {p0 .. p0}, Lg4/d;->B0()V

    const/4 v9, 0x0

    :try_start_0
    iget-object v2, v1, Lz4/j;->a:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v4

    const/4 v6, 0x1

    sparse-switch v4, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v4, "audioEffectSetEnabled"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0x12

    goto/16 :goto_0

    :sswitch_1
    const-string v4, "setAutomaticallyWaitsToMinimizeStalling"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0xa

    goto/16 :goto_0

    :sswitch_2
    const-string v4, "androidEqualizerGetParameters"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0x14

    goto/16 :goto_0

    :sswitch_3
    const-string v4, "setPreferredPeakBitRate"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0xc

    goto/16 :goto_0

    :sswitch_4
    const-string v4, "setSpeed"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v3, 0x4

    goto/16 :goto_0

    :sswitch_5
    const-string v4, "setPitch"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v3, 0x5

    goto/16 :goto_0

    :sswitch_6
    const-string v4, "concatenatingMove"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0x10

    goto/16 :goto_0

    :sswitch_7
    const-string v4, "concatenatingRemoveRange"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0xf

    goto/16 :goto_0

    :sswitch_8
    const-string v4, "setVolume"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v3, 0x3

    goto/16 :goto_0

    :sswitch_9
    const-string v4, "pause"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v3, 0x2

    goto/16 :goto_0

    :sswitch_a
    const-string v4, "seek"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0xd

    goto/16 :goto_0

    :sswitch_b
    const-string v4, "play"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    goto/16 :goto_0

    :sswitch_c
    const-string v4, "load"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :sswitch_d
    const-string v4, "setLoopMode"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v3, 0x7

    goto :goto_0

    :sswitch_e
    const-string v4, "setAndroidAudioAttributes"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0x11

    goto :goto_0

    :sswitch_f
    const-string v4, "androidLoudnessEnhancerSetTargetGain"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0x13

    goto :goto_0

    :sswitch_10
    const-string v4, "setCanUseNetworkResourcesForLiveStreamingWhilePaused"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0xb

    goto :goto_0

    :sswitch_11
    const-string v4, "setShuffleOrder"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0x9

    goto :goto_0

    :sswitch_12
    const-string v4, "concatenatingInsertAll"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0xe

    goto :goto_0

    :sswitch_13
    const-string v4, "setSkipSilence"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v3, 0x6

    goto :goto_0

    :sswitch_14
    const-string v4, "setShuffleMode"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v3, 0x8

    goto :goto_0

    :sswitch_15
    const-string v4, "androidEqualizerBandSetGain"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    const/16 v3, 0x15

    :cond_0
    :goto_0
    const-wide/16 v10, 0x3e8

    const-wide v12, -0x7fffffffffffffffL    # -4.9E-324

    const-string v2, "index"

    const-string v4, "audioSource"

    const-string v14, "enabled"

    const-string v15, "shuffleOrder"

    const-string v5, "id"

    packed-switch v3, :pswitch_data_0

    :try_start_1
    invoke-interface/range {p2 .. p2}, Lz4/k$d;->c()V

    goto/16 :goto_6

    :pswitch_0
    const-string v2, "bandIndex"

    invoke-virtual {v1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const-string v3, "gain"

    invoke-virtual {v1, v3}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-direct {v7, v2, v3, v4}, Lg4/d;->D0(ID)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    :goto_1
    invoke-interface {v8, v1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    goto/16 :goto_6

    :pswitch_1
    invoke-direct/range {p0 .. p0}, Lg4/d;->C0()Ljava/util/Map;

    move-result-object v1

    goto :goto_1

    :pswitch_2
    const-string v2, "targetGain"

    invoke-virtual {v1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-direct {v7, v1, v2}, Lg4/d;->O0(D)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto :goto_1

    :pswitch_3
    const-string v2, "type"

    invoke-virtual {v1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v14}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-direct {v7, v2, v1}, Lg4/d;->e0(Ljava/lang/String;Z)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto :goto_1

    :pswitch_4
    const-string v2, "contentType"

    invoke-virtual {v1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const-string v3, "flags"

    invoke-virtual {v1, v3}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const-string v4, "usage"

    invoke-virtual {v1, v4}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v7, v2, v3, v1}, Lg4/d;->V0(III)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto :goto_1

    :pswitch_5
    invoke-virtual {v1, v5}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v7, v2}, Lg4/d;->u0(Ljava/lang/Object;)Lj1/k;

    move-result-object v2

    const-string v3, "currentIndex"

    invoke-virtual {v1, v3}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const-string v4, "newIndex"

    invoke-virtual {v1, v4}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v6, v7, Lg4/d;->L:Landroid/os/Handler;

    new-instance v10, Lg4/a;

    invoke-direct {v10, v8}, Lg4/a;-><init>(Lz4/k$d;)V

    invoke-virtual {v2, v3, v4, v6, v10}, Lj1/k;->k0(IILandroid/os/Handler;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v5}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v7, v2}, Lg4/d;->u0(Ljava/lang/Object;)Lj1/k;

    move-result-object v2

    invoke-virtual {v1, v15}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {v7, v1}, Lg4/d;->y0(Ljava/util/List;)Lj1/s0;

    move-result-object v1

    :goto_2
    invoke-virtual {v2, v1}, Lj1/k;->u0(Lj1/s0;)V

    goto/16 :goto_6

    :pswitch_6
    invoke-virtual {v1, v5}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v7, v2}, Lg4/d;->u0(Ljava/lang/Object;)Lj1/k;

    move-result-object v2

    const-string v3, "startIndex"

    invoke-virtual {v1, v3}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const-string v4, "endIndex"

    invoke-virtual {v1, v4}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v6, v7, Lg4/d;->L:Landroid/os/Handler;

    new-instance v10, Lg4/c;

    invoke-direct {v10, v8}, Lg4/c;-><init>(Lz4/k$d;)V

    invoke-virtual {v2, v3, v4, v6, v10}, Lj1/k;->p0(IILandroid/os/Handler;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v5}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v7, v2}, Lg4/d;->u0(Ljava/lang/Object;)Lj1/k;

    move-result-object v2

    invoke-virtual {v1, v15}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {v7, v1}, Lg4/d;->y0(Ljava/util/List;)Lj1/s0;

    move-result-object v1

    goto :goto_2

    :pswitch_7
    invoke-virtual {v1, v5}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v7, v3}, Lg4/d;->u0(Ljava/lang/Object;)Lj1/k;

    move-result-object v3

    invoke-virtual {v1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const-string v4, "children"

    invoke-virtual {v1, v4}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v7, v4}, Lg4/d;->F0(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    iget-object v6, v7, Lg4/d;->L:Landroid/os/Handler;

    new-instance v10, Lg4/b;

    invoke-direct {v10, v8}, Lg4/b;-><init>(Lz4/k$d;)V

    invoke-virtual {v3, v2, v4, v6, v10}, Lj1/k;->S(ILjava/util/Collection;Landroid/os/Handler;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v5}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v7, v2}, Lg4/d;->u0(Ljava/lang/Object;)Lj1/k;

    move-result-object v2

    invoke-virtual {v1, v15}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {v7, v1}, Lg4/d;->y0(Ljava/util/List;)Lj1/s0;

    move-result-object v1

    goto :goto_2

    :pswitch_8
    const-string v3, "position"

    invoke-virtual {v1, v3}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v3, :cond_1

    goto :goto_3

    :cond_1
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    div-long v12, v2, v10

    :goto_3
    invoke-virtual {v7, v12, v13, v1, v8}, Lg4/d;->T0(JLjava/lang/Integer;Lz4/k$d;)V

    goto/16 :goto_6

    :pswitch_9
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_1

    :pswitch_a
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_1

    :pswitch_b
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_1

    :pswitch_c
    invoke-virtual {v1, v4}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v7, v1}, Lg4/d;->a1(Ljava/lang/Object;)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_1

    :pswitch_d
    const-string v2, "shuffleMode"

    invoke-virtual {v1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v6, :cond_2

    const/4 v5, 0x1

    goto :goto_4

    :cond_2
    const/4 v5, 0x0

    :goto_4
    invoke-virtual {v7, v5}, Lg4/d;->Z0(Z)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_1

    :pswitch_e
    const-string v2, "loopMode"

    invoke-virtual {v1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v7, v1}, Lg4/d;->X0(I)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_1

    :pswitch_f
    invoke-virtual {v1, v14}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v7, v1}, Lg4/d;->b1(Z)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_1

    :pswitch_10
    const-string v2, "pitch"

    invoke-virtual {v1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v7, v1}, Lg4/d;->Y0(F)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_1

    :pswitch_11
    const-string v2, "speed"

    invoke-virtual {v1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v7, v1}, Lg4/d;->c1(F)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_1

    :pswitch_12
    const-string v2, "volume"

    invoke-virtual {v1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v7, v1}, Lg4/d;->d1(F)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_1

    :pswitch_13
    invoke-virtual/range {p0 .. p0}, Lg4/d;->R0()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_1

    :pswitch_14
    invoke-virtual {v7, v8}, Lg4/d;->S0(Lz4/k$d;)V

    goto :goto_6

    :pswitch_15
    const-string v2, "initialPosition"

    invoke-virtual {v1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lg4/d;->J0(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "initialIndex"

    invoke-virtual {v1, v3}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v1, v4}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v7, v1}, Lg4/d;->E0(Ljava/lang/Object;)Lj1/x;

    move-result-object v3

    if-nez v2, :cond_3

    goto :goto_5

    :cond_3
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    div-long/2addr v1, v10

    move-wide v12, v1

    :goto_5
    move-object/from16 v1, p0

    move-object v2, v3

    move-wide v3, v12

    move-object/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lg4/d;->N0(Lj1/x;JLjava/lang/Integer;Lz4/k$d;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_6
    invoke-direct/range {p0 .. p0}, Lg4/d;->h0()V

    goto :goto_8

    :catchall_0
    move-exception v0

    move-object v1, v0

    goto :goto_9

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_7
    invoke-interface {v8, v1, v9, v9}, Lz4/k$d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_6

    :catch_1
    move-exception v0

    move-object v1, v0

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Illegal state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_7

    :goto_8
    return-void

    :goto_9
    invoke-direct/range {p0 .. p0}, Lg4/d;->h0()V

    throw v1

    :sswitch_data_0
    .sparse-switch
        -0x7aad3a17 -> :sswitch_15
        -0x76787586 -> :sswitch_14
        -0x6fccfba0 -> :sswitch_13
        -0x5bd749ea -> :sswitch_12
        -0x5878aea9 -> :sswitch_11
        -0x29f8037e -> :sswitch_10
        -0x1494f7ca -> :sswitch_f
        -0x64229a0 -> :sswitch_e
        -0x2e1df17 -> :sswitch_d
        0x32c4e6 -> :sswitch_c
        0x348b34 -> :sswitch_b
        0x35ce78 -> :sswitch_a
        0x65825f6 -> :sswitch_9
        0x27f73e1c -> :sswitch_8
        0x3264dd87 -> :sswitch_7
        0x3ad42123 -> :sswitch_6
        0x538783fe -> :sswitch_5
        0x53b4c105 -> :sswitch_4
        0x56b389ef -> :sswitch_3
        0x60da657d -> :sswitch_2
        0x613a0038 -> :sswitch_1
        0x7e381ce6 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic D(Z)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->j(Lh0/g3$d;Z)V

    return-void
.end method

.method public synthetic E(I)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->s(Lh0/g3$d;I)V

    return-void
.end method

.method public synthetic J(Lh0/c3;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->q(Lh0/g3$d;Lh0/c3;)V

    return-void
.end method

.method public synthetic M(Z)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->h(Lh0/g3$d;Z)V

    return-void
.end method

.method public N(Lh0/g3$e;Lh0/g3$e;I)V
    .locals 0

    invoke-direct {p0}, Lg4/d;->g1()V

    if-eqz p3, :cond_0

    const/4 p1, 0x1

    if-eq p3, p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lg4/d;->f1()Z

    :goto_0
    invoke-direct {p0}, Lg4/d;->g0()V

    return-void
.end method

.method public O(Lh0/c4;I)V
    .locals 5

    iget-wide p1, p0, Lg4/d;->o:J

    const/4 v0, 0x0

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v3, p1, v1

    if-nez v3, :cond_0

    iget-object p1, p0, Lg4/d;->p:Ljava/lang/Integer;

    if-eqz p1, :cond_2

    :cond_0
    iget-object p1, p0, Lg4/d;->p:Ljava/lang/Integer;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    iget-object p2, p0, Lg4/d;->G:Lh0/s;

    iget-wide v3, p0, Lg4/d;->o:J

    invoke-interface {p2, p1, v3, v4}, Lh0/g3;->q(IJ)V

    const/4 p1, 0x0

    iput-object p1, p0, Lg4/d;->p:Ljava/lang/Integer;

    iput-wide v1, p0, Lg4/d;->o:J

    :cond_2
    invoke-direct {p0}, Lg4/d;->f1()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-direct {p0}, Lg4/d;->g0()V

    :cond_3
    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1}, Lh0/g3;->x()I

    move-result p1

    const/4 p2, 0x4

    if-ne p1, p2, :cond_6

    :try_start_0
    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1}, Lh0/g3;->s()Z

    move-result p1

    const-wide/16 v1, 0x0

    if-eqz p1, :cond_5

    iget p1, p0, Lg4/d;->E:I

    if-nez p1, :cond_4

    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1}, Lh0/g3;->P()I

    move-result p1

    if-lez p1, :cond_4

    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1, v0, v1, v2}, Lh0/g3;->q(IJ)V

    goto :goto_1

    :cond_4
    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1}, Lh0/g3;->B()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1}, Lh0/g3;->v()V

    goto :goto_1

    :cond_5
    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1}, Lh0/g3;->E()I

    move-result p1

    iget-object p2, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p2}, Lh0/g3;->P()I

    move-result p2

    if-ge p1, p2, :cond_6

    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1}, Lh0/g3;->E()I

    move-result p2

    invoke-interface {p1, p2, v1, v2}, Lh0/g3;->q(IJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_6
    :goto_1
    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1}, Lh0/g3;->P()I

    move-result p1

    iput p1, p0, Lg4/d;->E:I

    return-void
.end method

.method public synthetic P()V
    .locals 0

    invoke-static {p0}, Lh0/i3;->u(Lh0/g3$d;)V

    return-void
.end method

.method public synthetic R()V
    .locals 0

    invoke-static {p0}, Lh0/i3;->w(Lh0/g3$d;)V

    return-void
.end method

.method public R0()V
    .locals 2

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0}, Lh0/g3;->s()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lh0/g3;->k(Z)V

    invoke-direct {p0}, Lg4/d;->g1()V

    iget-object v0, p0, Lg4/d;->r:Lz4/k$d;

    if-eqz v0, :cond_1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, v1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lg4/d;->r:Lz4/k$d;

    :cond_1
    return-void
.end method

.method public S0(Lz4/k$d;)V
    .locals 2

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0}, Lh0/g3;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1, v0}, Lz4/k$d;->a(Ljava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, p0, Lg4/d;->r:Lz4/k$d;

    if-eqz v0, :cond_1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, v1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    :cond_1
    iput-object p1, p0, Lg4/d;->r:Lz4/k$d;

    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lh0/g3;->k(Z)V

    invoke-direct {p0}, Lg4/d;->g1()V

    iget-object p1, p0, Lg4/d;->j:Lg4/d$c;

    sget-object v0, Lg4/d$c;->j:Lg4/d$c;

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lg4/d;->r:Lz4/k$d;

    if-eqz p1, :cond_2

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1, v0}, Lz4/k$d;->a(Ljava/lang/Object;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lg4/d;->r:Lz4/k$d;

    :cond_2
    return-void
.end method

.method public synthetic T(Lh0/o;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->e(Lh0/g3$d;Lh0/o;)V

    return-void
.end method

.method public T0(JLjava/lang/Integer;Lz4/k$d;)V
    .locals 2

    iget-object v0, p0, Lg4/d;->j:Lg4/d$c;

    sget-object v1, Lg4/d$c;->f:Lg4/d$c;

    if-eq v0, v1, :cond_2

    sget-object v1, Lg4/d$c;->g:Lg4/d$c;

    if-ne v0, v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-direct {p0}, Lg4/d;->L()V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lg4/d;->n:Ljava/lang/Long;

    iput-object p4, p0, Lg4/d;->s:Lz4/k$d;

    if-eqz p3, :cond_1

    :try_start_0
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    goto :goto_0

    :cond_1
    iget-object p3, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p3}, Lh0/g3;->E()I

    move-result p3

    :goto_0
    iget-object p4, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p4, p3, p1, p2}, Lh0/g3;->q(IJ)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    const/4 p2, 0x0

    iput-object p2, p0, Lg4/d;->s:Lz4/k$d;

    iput-object p2, p0, Lg4/d;->n:Ljava/lang/Long;

    throw p1

    :cond_2
    :goto_1
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p4, p1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic V(Lh0/z1;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->k(Lh0/g3$d;Lh0/z1;I)V

    return-void
.end method

.method public synthetic W(F)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->D(Lh0/g3$d;F)V

    return-void
.end method

.method public X(Lh0/c3;)V
    .locals 4

    instance-of v0, p1, Lh0/q;

    const/4 v1, 0x1

    const-string v2, "AudioPlayer"

    if-eqz v0, :cond_3

    check-cast p1, Lh0/q;

    iget v0, p1, Lh0/q;->n:I

    if-eqz v0, :cond_2

    if-eq v0, v1, :cond_1

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default ExoPlaybackException: "

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TYPE_UNEXPECTED: "

    :goto_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lh0/q;->m()Ljava/lang/RuntimeException;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TYPE_RENDERER: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lh0/q;->k()Ljava/lang/Exception;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TYPE_SOURCE: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lh0/q;->l()Ljava/io/IOException;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ll4/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p1, Lh0/q;->n:I

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "default PlaybackException: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ll4/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p1, Lh0/c3;->f:I

    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lg4/d;->U0(Ljava/lang/String;Ljava/lang/String;)V

    iget p1, p0, Lg4/d;->w:I

    add-int/2addr p1, v1

    iput p1, p0, Lg4/d;->w:I

    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1}, Lh0/g3;->B()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lg4/d;->K:Ljava/lang/Integer;

    if-eqz p1, :cond_4

    iget v0, p0, Lg4/d;->w:I

    const/4 v2, 0x5

    if-gt v0, v2, :cond_4

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    add-int/2addr p1, v1

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0}, Lh0/g3;->N()Lh0/c4;

    move-result-object v0

    invoke-virtual {v0}, Lh0/c4;->t()I

    move-result v0

    if-ge p1, v0, :cond_4

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    iget-object v1, p0, Lg4/d;->J:Lj1/x;

    invoke-interface {v0, v1}, Lh0/s;->w(Lj1/x;)V

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0}, Lh0/g3;->c()V

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    const-wide/16 v1, 0x0

    invoke-interface {v0, p1, v1, v2}, Lh0/g3;->q(IJ)V

    :cond_4
    return-void
.end method

.method public X0(I)V
    .locals 1

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0, p1}, Lh0/g3;->F(I)V

    return-void
.end method

.method public Y(I)V
    .locals 7

    const/4 v0, 0x2

    if-eq p1, v0, :cond_7

    const/4 v0, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eq p1, v0, :cond_3

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto/16 :goto_1

    :cond_0
    iget-object p1, p0, Lg4/d;->j:Lg4/d$c;

    sget-object v0, Lg4/d$c;->j:Lg4/d$c;

    if-eq p1, v0, :cond_1

    invoke-direct {p0}, Lg4/d;->g1()V

    iput-object v0, p0, Lg4/d;->j:Lg4/d$c;

    invoke-direct {p0}, Lg4/d;->g0()V

    :cond_1
    iget-object p1, p0, Lg4/d;->q:Lz4/k$d;

    if-eqz p1, :cond_2

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lg4/d;->q:Lz4/k$d;

    invoke-interface {v0, p1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    iput-object v2, p0, Lg4/d;->q:Lz4/k$d;

    iget-object p1, p0, Lg4/d;->x:Lj0/e;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0, p1, v1}, Lh0/s;->o(Lj0/e;Z)V

    iput-object v2, p0, Lg4/d;->x:Lj0/e;

    :cond_2
    iget-object p1, p0, Lg4/d;->r:Lz4/k$d;

    if-eqz p1, :cond_9

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1, v0}, Lz4/k$d;->a(Ljava/lang/Object;)V

    iput-object v2, p0, Lg4/d;->r:Lz4/k$d;

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1}, Lh0/g3;->s()Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-direct {p0}, Lg4/d;->g1()V

    :cond_4
    sget-object p1, Lg4/d$c;->i:Lg4/d$c;

    iput-object p1, p0, Lg4/d;->j:Lg4/d$c;

    invoke-direct {p0}, Lg4/d;->g0()V

    iget-object p1, p0, Lg4/d;->q:Lz4/k$d;

    if-eqz p1, :cond_6

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0}, Lg4/d;->I0()J

    move-result-wide v3

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v3, v5

    if-nez v0, :cond_5

    move-object v0, v2

    goto :goto_0

    :cond_5
    const-wide/16 v3, 0x3e8

    invoke-direct {p0}, Lg4/d;->I0()J

    move-result-wide v5

    mul-long v5, v5, v3

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    const-string v3, "duration"

    invoke-interface {p1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lg4/d;->q:Lz4/k$d;

    invoke-interface {v0, p1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    iput-object v2, p0, Lg4/d;->q:Lz4/k$d;

    iget-object p1, p0, Lg4/d;->x:Lj0/e;

    if-eqz p1, :cond_6

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0, p1, v1}, Lh0/s;->o(Lj0/e;Z)V

    iput-object v2, p0, Lg4/d;->x:Lj0/e;

    :cond_6
    iget-object p1, p0, Lg4/d;->s:Lz4/k$d;

    if-eqz p1, :cond_9

    invoke-direct {p0}, Lg4/d;->t0()V

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lg4/d;->h1()Z

    iget-object p1, p0, Lg4/d;->j:Lg4/d$c;

    sget-object v0, Lg4/d$c;->h:Lg4/d$c;

    if-eq p1, v0, :cond_8

    sget-object v1, Lg4/d$c;->g:Lg4/d$c;

    if-eq p1, v1, :cond_8

    iput-object v0, p0, Lg4/d;->j:Lg4/d$c;

    invoke-direct {p0}, Lg4/d;->g0()V

    :cond_8
    invoke-direct {p0}, Lg4/d;->e1()V

    :cond_9
    :goto_1
    return-void
.end method

.method public Y0(F)V
    .locals 3

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0}, Lh0/g3;->g()Lh0/f3;

    move-result-object v0

    iget v1, v0, Lh0/f3;->g:F

    cmpl-float v1, v1, p1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lg4/d;->G:Lh0/s;

    new-instance v2, Lh0/f3;

    iget v0, v0, Lh0/f3;->f:F

    invoke-direct {v2, v0, p1}, Lh0/f3;-><init>(FF)V

    invoke-interface {v1, v2}, Lh0/g3;->h(Lh0/f3;)V

    invoke-direct {p0}, Lg4/d;->A0()V

    return-void
.end method

.method public synthetic Z(ZI)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->n(Lh0/g3$d;ZI)V

    return-void
.end method

.method public Z0(Z)V
    .locals 1

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0, p1}, Lh0/g3;->u(Z)V

    return-void
.end method

.method public synthetic a0(Lj0/e;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->a(Lh0/g3$d;Lj0/e;)V

    return-void
.end method

.method public synthetic b(Z)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->y(Lh0/g3$d;Z)V

    return-void
.end method

.method public b1(Z)V
    .locals 1

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0, p1}, Lh0/s;->f(Z)V

    return-void
.end method

.method public c1(F)V
    .locals 3

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0}, Lh0/g3;->g()Lh0/f3;

    move-result-object v0

    iget v1, v0, Lh0/f3;->f:F

    cmpl-float v1, v1, p1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lg4/d;->G:Lh0/s;

    new-instance v2, Lh0/f3;

    iget v0, v0, Lh0/f3;->g:F

    invoke-direct {v2, p1, v0}, Lh0/f3;-><init>(FF)V

    invoke-interface {v1, v2}, Lh0/g3;->h(Lh0/f3;)V

    iget-object p1, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {p1}, Lh0/g3;->s()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lg4/d;->g1()V

    :cond_1
    invoke-direct {p0}, Lg4/d;->A0()V

    return-void
.end method

.method public d1(F)V
    .locals 1

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    invoke-interface {v0, p1}, Lh0/g3;->i(F)V

    return-void
.end method

.method public synthetic h(Lh0/f3;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->o(Lh0/g3$d;Lh0/f3;)V

    return-void
.end method

.method public synthetic i(I)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->v(Lh0/g3$d;I)V

    return-void
.end method

.method public synthetic i0(Z)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->x(Lh0/g3$d;Z)V

    return-void
.end method

.method public synthetic j(Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->c(Lh0/g3$d;Ljava/util/List;)V

    return-void
.end method

.method public synthetic j0(Lh0/g3$b;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->b(Lh0/g3$d;Lh0/g3$b;)V

    return-void
.end method

.method public synthetic k0(II)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->z(Lh0/g3$d;II)V

    return-void
.end method

.method public synthetic l(Lf2/z;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->C(Lh0/g3$d;Lf2/z;)V

    return-void
.end method

.method public synthetic l0(Lh0/e2;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->l(Lh0/g3$d;Lh0/e2;)V

    return-void
.end method

.method public n0(Lh0/h4;)V
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Lh0/h4;->b()Lp2/q;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/AbstractCollection;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    invoke-virtual {p1}, Lh0/h4;->b()Lp2/q;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lh0/h4$a;

    invoke-virtual {v2}, Lh0/h4$a;->b()Lj1/x0;

    move-result-object v2

    const/4 v3, 0x0

    :goto_1
    iget v4, v2, Lj1/x0;->f:I

    if-ge v3, v4, :cond_2

    invoke-virtual {v2, v3}, Lj1/x0;->b(I)Lh0/r1;

    move-result-object v4

    iget-object v4, v4, Lh0/r1;->o:Lz0/a;

    if-eqz v4, :cond_1

    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v4}, Lz0/a;->h()I

    move-result v6

    if-ge v5, v6, :cond_1

    invoke-virtual {v4, v5}, Lz0/a;->g(I)Lz0/a$b;

    move-result-object v6

    instance-of v7, v6, Ld1/b;

    if-eqz v7, :cond_0

    check-cast v6, Ld1/b;

    iput-object v6, p0, Lg4/d;->v:Ld1/b;

    invoke-direct {p0}, Lg4/d;->g0()V

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public synthetic o0(Lh0/g3;Lh0/g3$c;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->g(Lh0/g3$d;Lh0/g3;Lh0/g3$c;)V

    return-void
.end method

.method public synthetic p0(IZ)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/i3;->f(Lh0/g3$d;IZ)V

    return-void
.end method

.method public synthetic q0(Z)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->i(Lh0/g3$d;Z)V

    return-void
.end method

.method public w(Lz0/a;)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lz0/a;->h()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v0}, Lz0/a;->g(I)Lz0/a$b;

    move-result-object v1

    instance-of v2, v1, Ld1/c;

    if-eqz v2, :cond_0

    check-cast v1, Ld1/c;

    iput-object v1, p0, Lg4/d;->u:Ld1/c;

    invoke-direct {p0}, Lg4/d;->g0()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public synthetic x(Ls1/e;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/i3;->d(Lh0/g3$d;Ls1/e;)V

    return-void
.end method

.method public z0()V
    .locals 3

    iget-object v0, p0, Lg4/d;->j:Lg4/d$c;

    sget-object v1, Lg4/d$c;->g:Lg4/d$c;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lg4/d;->K()V

    :cond_0
    iget-object v0, p0, Lg4/d;->r:Lz4/k$d;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, v2}, Lz4/k$d;->a(Ljava/lang/Object;)V

    iput-object v1, p0, Lg4/d;->r:Lz4/k$d;

    :cond_1
    iget-object v0, p0, Lg4/d;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iput-object v1, p0, Lg4/d;->J:Lj1/x;

    invoke-direct {p0}, Lg4/d;->r0()V

    iget-object v0, p0, Lg4/d;->G:Lh0/s;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lh0/g3;->a()V

    iput-object v1, p0, Lg4/d;->G:Lh0/s;

    sget-object v0, Lg4/d$c;->f:Lg4/d$c;

    iput-object v0, p0, Lg4/d;->j:Lg4/d$c;

    invoke-direct {p0}, Lg4/d;->g0()V

    :cond_2
    iget-object v0, p0, Lg4/d;->h:Lg4/e;

    invoke-virtual {v0}, Lg4/e;->c()V

    iget-object v0, p0, Lg4/d;->i:Lg4/e;

    invoke-virtual {v0}, Lg4/e;->c()V

    return-void
.end method
