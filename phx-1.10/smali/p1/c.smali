.class public final Lp1/c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lp1/l;
.implements Ld2/h0$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lp1/c$b;,
        Lp1/c$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lp1/l;",
        "Ld2/h0$b<",
        "Ld2/j0<",
        "Lp1/i;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final u:Lp1/l$a;


# instance fields
.field private final f:Lo1/g;

.field private final g:Lp1/k;

.field private final h:Ld2/g0;

.field private final i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/net/Uri;",
            "Lp1/c$c;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lp1/l$b;",
            ">;"
        }
    .end annotation
.end field

.field private final k:D

.field private l:Lj1/e0$a;

.field private m:Ld2/h0;

.field private n:Landroid/os/Handler;

.field private o:Lp1/l$e;

.field private p:Lp1/h;

.field private q:Landroid/net/Uri;

.field private r:Lp1/g;

.field private s:Z

.field private t:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lp1/b;->a:Lp1/b;

    sput-object v0, Lp1/c;->u:Lp1/l$a;

    return-void
.end method

.method public constructor <init>(Lo1/g;Ld2/g0;Lp1/k;)V
    .locals 6

    const-wide/high16 v4, 0x400c000000000000L    # 3.5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lp1/c;-><init>(Lo1/g;Ld2/g0;Lp1/k;D)V

    return-void
.end method

.method public constructor <init>(Lo1/g;Ld2/g0;Lp1/k;D)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lp1/c;->f:Lo1/g;

    iput-object p3, p0, Lp1/c;->g:Lp1/k;

    iput-object p2, p0, Lp1/c;->h:Ld2/g0;

    iput-wide p4, p0, Lp1/c;->k:D

    new-instance p1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object p1, p0, Lp1/c;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lp1/c;->i:Ljava/util/HashMap;

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lp1/c;->t:J

    return-void
.end method

.method static synthetic A(Lp1/c;)Lp1/g;
    .locals 0

    iget-object p0, p0, Lp1/c;->r:Lp1/g;

    return-object p0
.end method

.method static synthetic B(Lp1/c;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lp1/c;->i:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic C(Lp1/c;)Lo1/g;
    .locals 0

    iget-object p0, p0, Lp1/c;->f:Lo1/g;

    return-object p0
.end method

.method static synthetic D(Lp1/c;)Lj1/e0$a;
    .locals 0

    iget-object p0, p0, Lp1/c;->l:Lj1/e0$a;

    return-object p0
.end method

.method static synthetic E(Lp1/c;)Ld2/g0;
    .locals 0

    iget-object p0, p0, Lp1/c;->h:Ld2/g0;

    return-object p0
.end method

.method private F(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    new-instance v3, Lp1/c$c;

    invoke-direct {v3, p0, v2}, Lp1/c$c;-><init>(Lp1/c;Landroid/net/Uri;)V

    iget-object v4, p0, Lp1/c;->i:Ljava/util/HashMap;

    invoke-virtual {v4, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static G(Lp1/g;Lp1/g;)Lp1/g$d;
    .locals 4

    iget-wide v0, p1, Lp1/g;->k:J

    iget-wide v2, p0, Lp1/g;->k:J

    sub-long/2addr v0, v2

    long-to-int p1, v0

    iget-object p0, p0, Lp1/g;->r:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lp1/g$d;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method private H(Lp1/g;Lp1/g;)Lp1/g;
    .locals 2

    invoke-virtual {p2, p1}, Lp1/g;->f(Lp1/g;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean p2, p2, Lp1/g;->o:Z

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lp1/g;->d()Lp1/g;

    move-result-object p1

    :cond_0
    return-object p1

    :cond_1
    invoke-direct {p0, p1, p2}, Lp1/c;->J(Lp1/g;Lp1/g;)J

    move-result-wide v0

    invoke-direct {p0, p1, p2}, Lp1/c;->I(Lp1/g;Lp1/g;)I

    move-result p1

    invoke-virtual {p2, v0, v1, p1}, Lp1/g;->c(JI)Lp1/g;

    move-result-object p1

    return-object p1
.end method

.method private I(Lp1/g;Lp1/g;)I
    .locals 3

    iget-boolean v0, p2, Lp1/g;->i:Z

    if-eqz v0, :cond_0

    iget p1, p2, Lp1/g;->j:I

    return p1

    :cond_0
    iget-object v0, p0, Lp1/c;->r:Lp1/g;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget v0, v0, Lp1/g;->j:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez p1, :cond_2

    return v0

    :cond_2
    invoke-static {p1, p2}, Lp1/c;->G(Lp1/g;Lp1/g;)Lp1/g$d;

    move-result-object v2

    if-eqz v2, :cond_3

    iget p1, p1, Lp1/g;->j:I

    iget v0, v2, Lp1/g$e;->i:I

    add-int/2addr p1, v0

    iget-object p2, p2, Lp1/g;->r:Ljava/util/List;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lp1/g$d;

    iget p2, p2, Lp1/g$e;->i:I

    sub-int/2addr p1, p2

    return p1

    :cond_3
    return v0
.end method

.method private J(Lp1/g;Lp1/g;)J
    .locals 8

    iget-boolean v0, p2, Lp1/g;->p:Z

    if-eqz v0, :cond_0

    iget-wide p1, p2, Lp1/g;->h:J

    return-wide p1

    :cond_0
    iget-object v0, p0, Lp1/c;->r:Lp1/g;

    if-eqz v0, :cond_1

    iget-wide v0, v0, Lp1/g;->h:J

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x0

    :goto_0
    if-nez p1, :cond_2

    return-wide v0

    :cond_2
    iget-object v2, p1, Lp1/g;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {p1, p2}, Lp1/c;->G(Lp1/g;Lp1/g;)Lp1/g$d;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-wide p1, p1, Lp1/g;->h:J

    iget-wide v0, v3, Lp1/g$e;->j:J

    add-long/2addr p1, v0

    return-wide p1

    :cond_3
    int-to-long v2, v2

    iget-wide v4, p2, Lp1/g;->k:J

    iget-wide v6, p1, Lp1/g;->k:J

    sub-long/2addr v4, v6

    cmp-long p2, v2, v4

    if-nez p2, :cond_4

    invoke-virtual {p1}, Lp1/g;->e()J

    move-result-wide p1

    return-wide p1

    :cond_4
    return-wide v0
.end method

.method private K(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3

    iget-object v0, p0, Lp1/c;->r:Lp1/g;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lp1/g;->v:Lp1/g$f;

    iget-boolean v1, v1, Lp1/g$f;->e:Z

    if-eqz v1, :cond_1

    iget-object v0, v0, Lp1/g;->t:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp1/g$c;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p1

    iget-wide v1, v0, Lp1/g$c;->b:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "_HLS_msn"

    invoke-virtual {p1, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget v0, v0, Lp1/g$c;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_HLS_part"

    invoke-virtual {p1, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method private L(Landroid/net/Uri;)Z
    .locals 4

    iget-object v0, p0, Lp1/c;->p:Lp1/h;

    iget-object v0, v0, Lp1/h;->e:Ljava/util/List;

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lp1/h$b;

    iget-object v3, v3, Lp1/h$b;->a:Landroid/net/Uri;

    invoke-virtual {p1, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private M()Z
    .locals 10

    iget-object v0, p0, Lp1/c;->p:Lp1/h;

    iget-object v0, v0, Lp1/h;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v1, :cond_1

    iget-object v6, p0, Lp1/c;->i:Ljava/util/HashMap;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lp1/h$b;

    iget-object v7, v7, Lp1/h$b;->a:Landroid/net/Uri;

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lp1/c$c;

    invoke-static {v6}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lp1/c$c;

    invoke-static {v6}, Lp1/c$c;->d(Lp1/c$c;)J

    move-result-wide v7

    cmp-long v9, v2, v7

    if-lez v9, :cond_0

    invoke-static {v6}, Lp1/c$c;->e(Lp1/c$c;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lp1/c;->q:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lp1/c;->K(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v6, v0}, Lp1/c$c;->f(Lp1/c$c;Landroid/net/Uri;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    return v4
.end method

.method private N(Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lp1/c;->q:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lp1/c;->L(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lp1/c;->r:Lp1/g;

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lp1/g;->o:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lp1/c;->q:Landroid/net/Uri;

    iget-object v0, p0, Lp1/c;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp1/c$c;

    invoke-static {v0}, Lp1/c$c;->g(Lp1/c$c;)Lp1/g;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-boolean v2, v1, Lp1/g;->o:Z

    if-eqz v2, :cond_1

    iput-object v1, p0, Lp1/c;->r:Lp1/g;

    iget-object p1, p0, Lp1/c;->o:Lp1/l$e;

    invoke-interface {p1, v1}, Lp1/l$e;->h(Lp1/g;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lp1/c;->K(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p1

    invoke-static {v0, p1}, Lp1/c$c;->f(Lp1/c$c;Landroid/net/Uri;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private O(Landroid/net/Uri;Ld2/g0$c;Z)Z
    .locals 3

    iget-object v0, p0, Lp1/c;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lp1/l$b;

    invoke-interface {v2, p1, p2, p3}, Lp1/l$b;->e(Landroid/net/Uri;Ld2/g0$c;Z)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    or-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return v1
.end method

.method private S(Landroid/net/Uri;Lp1/g;)V
    .locals 2

    iget-object v0, p0, Lp1/c;->q:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lp1/c;->r:Lp1/g;

    if-nez p1, :cond_0

    iget-boolean p1, p2, Lp1/g;->o:Z

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lp1/c;->s:Z

    iget-wide v0, p2, Lp1/g;->h:J

    iput-wide v0, p0, Lp1/c;->t:J

    :cond_0
    iput-object p2, p0, Lp1/c;->r:Lp1/g;

    iget-object p1, p0, Lp1/c;->o:Lp1/l$e;

    invoke-interface {p1, p2}, Lp1/l$e;->h(Lp1/g;)V

    :cond_1
    iget-object p1, p0, Lp1/c;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lp1/l$b;

    invoke-interface {p2}, Lp1/l$b;->b()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method static synthetic p(Lp1/c;Landroid/net/Uri;Ld2/g0$c;Z)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lp1/c;->O(Landroid/net/Uri;Ld2/g0$c;Z)Z

    move-result p0

    return p0
.end method

.method static synthetic q(Lp1/c;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lp1/c;->n:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic r(Lp1/c;)Lp1/h;
    .locals 0

    iget-object p0, p0, Lp1/c;->p:Lp1/h;

    return-object p0
.end method

.method static synthetic s(Lp1/c;)Lp1/k;
    .locals 0

    iget-object p0, p0, Lp1/c;->g:Lp1/k;

    return-object p0
.end method

.method static synthetic u(Lp1/c;Lp1/g;Lp1/g;)Lp1/g;
    .locals 0

    invoke-direct {p0, p1, p2}, Lp1/c;->H(Lp1/g;Lp1/g;)Lp1/g;

    move-result-object p0

    return-object p0
.end method

.method static synthetic v(Lp1/c;Landroid/net/Uri;Lp1/g;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lp1/c;->S(Landroid/net/Uri;Lp1/g;)V

    return-void
.end method

.method static synthetic w(Lp1/c;)D
    .locals 2

    iget-wide v0, p0, Lp1/c;->k:D

    return-wide v0
.end method

.method static synthetic x(Lp1/c;)Landroid/net/Uri;
    .locals 0

    iget-object p0, p0, Lp1/c;->q:Landroid/net/Uri;

    return-object p0
.end method

.method static synthetic y(Lp1/c;)Z
    .locals 0

    invoke-direct {p0}, Lp1/c;->M()Z

    move-result p0

    return p0
.end method

.method static synthetic z(Lp1/c;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 0

    iget-object p0, p0, Lp1/c;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object p0
.end method


# virtual methods
.method public P(Ld2/j0;JJZ)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld2/j0<",
            "Lp1/i;",
            ">;JJZ)V"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p1

    new-instance v14, Lj1/q;

    iget-wide v3, v1, Ld2/j0;->a:J

    iget-object v5, v1, Ld2/j0;->b:Ld2/p;

    invoke-virtual/range {p1 .. p1}, Ld2/j0;->f()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Ld2/j0;->d()Ljava/util/Map;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Ld2/j0;->b()J

    move-result-wide v12

    move-object v2, v14

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    invoke-direct/range {v2 .. v13}, Lj1/q;-><init>(JLd2/p;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    iget-object v2, v0, Lp1/c;->h:Ld2/g0;

    iget-wide v3, v1, Ld2/j0;->a:J

    invoke-interface {v2, v3, v4}, Ld2/g0;->a(J)V

    iget-object v1, v0, Lp1/c;->l:Lj1/e0$a;

    const/4 v2, 0x4

    invoke-virtual {v1, v14, v2}, Lj1/e0$a;->q(Lj1/q;I)V

    return-void
.end method

.method public Q(Ld2/j0;JJ)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld2/j0<",
            "Lp1/i;",
            ">;JJ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p1 .. p1}, Ld2/j0;->e()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lp1/i;

    instance-of v3, v2, Lp1/g;

    if-eqz v3, :cond_0

    iget-object v4, v2, Lp1/i;->a:Ljava/lang/String;

    invoke-static {v4}, Lp1/h;->e(Ljava/lang/String;)Lp1/h;

    move-result-object v4

    goto :goto_0

    :cond_0
    move-object v4, v2

    check-cast v4, Lp1/h;

    :goto_0
    iput-object v4, v0, Lp1/c;->p:Lp1/h;

    iget-object v5, v4, Lp1/h;->e:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lp1/h$b;

    iget-object v5, v5, Lp1/h$b;->a:Landroid/net/Uri;

    iput-object v5, v0, Lp1/c;->q:Landroid/net/Uri;

    iget-object v5, v0, Lp1/c;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v6, Lp1/c$b;

    const/4 v7, 0x0

    invoke-direct {v6, v0, v7}, Lp1/c$b;-><init>(Lp1/c;Lp1/c$a;)V

    invoke-virtual {v5, v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, v4, Lp1/h;->d:Ljava/util/List;

    invoke-direct {v0, v4}, Lp1/c;->F(Ljava/util/List;)V

    new-instance v4, Lj1/q;

    iget-wide v6, v1, Ld2/j0;->a:J

    iget-object v8, v1, Ld2/j0;->b:Ld2/p;

    invoke-virtual/range {p1 .. p1}, Ld2/j0;->f()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Ld2/j0;->d()Ljava/util/Map;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Ld2/j0;->b()J

    move-result-wide v15

    move-object v5, v4

    move-wide/from16 v11, p2

    move-wide/from16 v13, p4

    invoke-direct/range {v5 .. v16}, Lj1/q;-><init>(JLd2/p;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    iget-object v5, v0, Lp1/c;->i:Ljava/util/HashMap;

    iget-object v6, v0, Lp1/c;->q:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lp1/c$c;

    if-eqz v3, :cond_1

    check-cast v2, Lp1/g;

    invoke-static {v5, v2, v4}, Lp1/c$c;->c(Lp1/c$c;Lp1/g;Lj1/q;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v5}, Lp1/c$c;->o()V

    :goto_1
    iget-object v2, v0, Lp1/c;->h:Ld2/g0;

    iget-wide v5, v1, Ld2/j0;->a:J

    invoke-interface {v2, v5, v6}, Ld2/g0;->a(J)V

    iget-object v1, v0, Lp1/c;->l:Lj1/e0$a;

    const/4 v2, 0x4

    invoke-virtual {v1, v4, v2}, Lj1/e0$a;->t(Lj1/q;I)V

    return-void
.end method

.method public R(Ld2/j0;JJLjava/io/IOException;I)Ld2/h0$c;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld2/j0<",
            "Lp1/i;",
            ">;JJ",
            "Ljava/io/IOException;",
            "I)",
            "Ld2/h0$c;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    new-instance v15, Lj1/q;

    iget-wide v4, v1, Ld2/j0;->a:J

    iget-object v6, v1, Ld2/j0;->b:Ld2/p;

    invoke-virtual/range {p1 .. p1}, Ld2/j0;->f()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Ld2/j0;->d()Ljava/util/Map;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Ld2/j0;->b()J

    move-result-wide v13

    move-object v3, v15

    move-wide/from16 v9, p2

    move-wide/from16 v11, p4

    invoke-direct/range {v3 .. v14}, Lj1/q;-><init>(JLd2/p;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    new-instance v3, Lj1/t;

    iget v4, v1, Ld2/j0;->c:I

    invoke-direct {v3, v4}, Lj1/t;-><init>(I)V

    iget-object v4, v0, Lp1/c;->h:Ld2/g0;

    new-instance v5, Ld2/g0$c;

    move/from16 v6, p7

    invoke-direct {v5, v15, v3, v2, v6}, Ld2/g0$c;-><init>(Lj1/q;Lj1/t;Ljava/io/IOException;I)V

    invoke-interface {v4, v5}, Ld2/g0;->c(Ld2/g0$c;)J

    move-result-wide v3

    const/4 v5, 0x0

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v8, v3, v6

    if-nez v8, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    iget-object v7, v0, Lp1/c;->l:Lj1/e0$a;

    iget v8, v1, Ld2/j0;->c:I

    invoke-virtual {v7, v15, v8, v2, v6}, Lj1/e0$a;->x(Lj1/q;ILjava/io/IOException;Z)V

    if-eqz v6, :cond_1

    iget-object v2, v0, Lp1/c;->h:Ld2/g0;

    iget-wide v7, v1, Ld2/j0;->a:J

    invoke-interface {v2, v7, v8}, Ld2/g0;->a(J)V

    :cond_1
    if-eqz v6, :cond_2

    sget-object v1, Ld2/h0;->g:Ld2/h0$c;

    goto :goto_1

    :cond_2
    invoke-static {v5, v3, v4}, Ld2/h0;->h(ZJ)Ld2/h0$c;

    move-result-object v1

    :goto_1
    return-object v1
.end method

.method public a(Lp1/l$b;)V
    .locals 1

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lp1/c;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Lp1/c;->q:Landroid/net/Uri;

    iput-object v0, p0, Lp1/c;->r:Lp1/g;

    iput-object v0, p0, Lp1/c;->p:Lp1/h;

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v1, p0, Lp1/c;->t:J

    iget-object v1, p0, Lp1/c;->m:Ld2/h0;

    invoke-virtual {v1}, Ld2/h0;->l()V

    iput-object v0, p0, Lp1/c;->m:Ld2/h0;

    iget-object v1, p0, Lp1/c;->i:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lp1/c$c;

    invoke-virtual {v2}, Lp1/c$c;->x()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lp1/c;->n:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iput-object v0, p0, Lp1/c;->n:Landroid/os/Handler;

    iget-object v0, p0, Lp1/c;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lp1/c;->s:Z

    return v0
.end method

.method public d()Lp1/h;
    .locals 1

    iget-object v0, p0, Lp1/c;->p:Lp1/h;

    return-object v0
.end method

.method public e(Landroid/net/Uri;J)Z
    .locals 1

    iget-object v0, p0, Lp1/c;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lp1/c$c;

    if-eqz p1, :cond_0

    invoke-static {p1, p2, p3}, Lp1/c$c;->b(Lp1/c$c;J)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public f(Landroid/net/Uri;)Z
    .locals 1

    iget-object v0, p0, Lp1/c;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lp1/c$c;

    invoke-virtual {p1}, Lp1/c$c;->k()Z

    move-result p1

    return p1
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lp1/c;->m:Ld2/h0;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ld2/h0;->b()V

    :cond_0
    iget-object v0, p0, Lp1/c;->q:Landroid/net/Uri;

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lp1/c;->h(Landroid/net/Uri;)V

    :cond_1
    return-void
.end method

.method public h(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lp1/c;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lp1/c$c;

    invoke-virtual {p1}, Lp1/c$c;->r()V

    return-void
.end method

.method public i(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lp1/c;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lp1/c$c;

    invoke-virtual {p1}, Lp1/c$c;->o()V

    return-void
.end method

.method public j(Landroid/net/Uri;Z)Lp1/g;
    .locals 1

    iget-object v0, p0, Lp1/c;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp1/c$c;

    invoke-virtual {v0}, Lp1/c$c;->j()Lp1/g;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lp1/c;->N(Landroid/net/Uri;)V

    :cond_0
    return-object v0
.end method

.method public k(Landroid/net/Uri;Lj1/e0$a;Lp1/l$e;)V
    .locals 7

    invoke-static {}, Le2/n0;->w()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lp1/c;->n:Landroid/os/Handler;

    iput-object p2, p0, Lp1/c;->l:Lj1/e0$a;

    iput-object p3, p0, Lp1/c;->o:Lp1/l$e;

    new-instance p3, Ld2/j0;

    iget-object v0, p0, Lp1/c;->f:Lo1/g;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lo1/g;->a(I)Ld2/l;

    move-result-object v0

    iget-object v2, p0, Lp1/c;->g:Lp1/k;

    invoke-interface {v2}, Lp1/k;->b()Ld2/j0$a;

    move-result-object v2

    invoke-direct {p3, v0, p1, v1, v2}, Ld2/j0;-><init>(Ld2/l;Landroid/net/Uri;ILd2/j0$a;)V

    iget-object p1, p0, Lp1/c;->m:Ld2/h0;

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Le2/a;->f(Z)V

    new-instance p1, Ld2/h0;

    const-string v0, "DefaultHlsPlaylistTracker:MultivariantPlaylist"

    invoke-direct {p1, v0}, Ld2/h0;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lp1/c;->m:Ld2/h0;

    iget-object v0, p0, Lp1/c;->h:Ld2/g0;

    iget v1, p3, Ld2/j0;->c:I

    invoke-interface {v0, v1}, Ld2/g0;->d(I)I

    move-result v0

    invoke-virtual {p1, p3, p0, v0}, Ld2/h0;->n(Ld2/h0$e;Ld2/h0$b;I)J

    move-result-wide v5

    new-instance p1, Lj1/q;

    iget-wide v2, p3, Ld2/j0;->a:J

    iget-object v4, p3, Ld2/j0;->b:Ld2/p;

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lj1/q;-><init>(JLd2/p;J)V

    iget p3, p3, Ld2/j0;->c:I

    invoke-virtual {p2, p1, p3}, Lj1/e0$a;->z(Lj1/q;I)V

    return-void
.end method

.method public bridge synthetic l(Ld2/h0$e;JJLjava/io/IOException;I)Ld2/h0$c;
    .locals 0

    check-cast p1, Ld2/j0;

    invoke-virtual/range {p0 .. p7}, Lp1/c;->R(Ld2/j0;JJLjava/io/IOException;I)Ld2/h0$c;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic m(Ld2/h0$e;JJZ)V
    .locals 0

    check-cast p1, Ld2/j0;

    invoke-virtual/range {p0 .. p6}, Lp1/c;->P(Ld2/j0;JJZ)V

    return-void
.end method

.method public n()J
    .locals 2

    iget-wide v0, p0, Lp1/c;->t:J

    return-wide v0
.end method

.method public o(Lp1/l$b;)V
    .locals 1

    iget-object v0, p0, Lp1/c;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public bridge synthetic t(Ld2/h0$e;JJ)V
    .locals 0

    check-cast p1, Ld2/j0;

    invoke-virtual/range {p0 .. p5}, Lp1/c;->Q(Ld2/j0;JJ)V

    return-void
.end method
