.class public Lp1/g$e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lp1/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final f:Ljava/lang/String;

.field public final g:Lp1/g$d;

.field public final h:J

.field public final i:I

.field public final j:J

.field public final k:Ll0/m;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:J

.field public final o:J

.field public final p:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;Lp1/g$d;JIJLl0/m;Ljava/lang/String;Ljava/lang/String;JJZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lp1/g$e;->f:Ljava/lang/String;

    iput-object p2, p0, Lp1/g$e;->g:Lp1/g$d;

    iput-wide p3, p0, Lp1/g$e;->h:J

    iput p5, p0, Lp1/g$e;->i:I

    iput-wide p6, p0, Lp1/g$e;->j:J

    iput-object p8, p0, Lp1/g$e;->k:Ll0/m;

    iput-object p9, p0, Lp1/g$e;->l:Ljava/lang/String;

    iput-object p10, p0, Lp1/g$e;->m:Ljava/lang/String;

    iput-wide p11, p0, Lp1/g$e;->n:J

    iput-wide p13, p0, Lp1/g$e;->o:J

    iput-boolean p15, p0, Lp1/g$e;->p:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lp1/g$d;JIJLl0/m;Ljava/lang/String;Ljava/lang/String;JJZLp1/g$a;)V
    .locals 0

    invoke-direct/range {p0 .. p15}, Lp1/g$e;-><init>(Ljava/lang/String;Lp1/g$d;JIJLl0/m;Ljava/lang/String;Ljava/lang/String;JJZ)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;)I
    .locals 5

    iget-wide v0, p0, Lp1/g$e;->j:J

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    iget-wide v0, p0, Lp1/g$e;->j:J

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long p1, v0, v2

    if-gez p1, :cond_1

    const/4 p1, -0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lp1/g$e;->a(Ljava/lang/Long;)I

    move-result p1

    return p1
.end method
