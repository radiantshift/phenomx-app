.class final Lu/b$d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lu/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Lu/l;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lu/b$d;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;

.field private static final d:Lo3/c;

.field private static final e:Lo3/c;

.field private static final f:Lo3/c;

.field private static final g:Lo3/c;

.field private static final h:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lu/b$d;

    invoke-direct {v0}, Lu/b$d;-><init>()V

    sput-object v0, Lu/b$d;->a:Lu/b$d;

    const-string v0, "eventTimeMs"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$d;->b:Lo3/c;

    const-string v0, "eventCode"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$d;->c:Lo3/c;

    const-string v0, "eventUptimeMs"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$d;->d:Lo3/c;

    const-string v0, "sourceExtension"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$d;->e:Lo3/c;

    const-string v0, "sourceExtensionJsonProto3"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$d;->f:Lo3/c;

    const-string v0, "timezoneOffsetSeconds"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$d;->g:Lo3/c;

    const-string v0, "networkConnectionInfo"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$d;->h:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lu/l;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lu/b$d;->b(Lu/l;Lo3/e;)V

    return-void
.end method

.method public b(Lu/l;Lo3/e;)V
    .locals 3

    sget-object v0, Lu/b$d;->b:Lo3/c;

    invoke-virtual {p1}, Lu/l;->c()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lu/b$d;->c:Lo3/c;

    invoke-virtual {p1}, Lu/l;->b()Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$d;->d:Lo3/c;

    invoke-virtual {p1}, Lu/l;->d()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lu/b$d;->e:Lo3/c;

    invoke-virtual {p1}, Lu/l;->f()[B

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$d;->f:Lo3/c;

    invoke-virtual {p1}, Lu/l;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$d;->g:Lo3/c;

    invoke-virtual {p1}, Lu/l;->h()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lu/b$d;->h:Lo3/c;

    invoke-virtual {p1}, Lu/l;->e()Lu/o;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    return-void
.end method
