.class final Lu/b$e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lu/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Lu/m;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lu/b$e;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;

.field private static final d:Lo3/c;

.field private static final e:Lo3/c;

.field private static final f:Lo3/c;

.field private static final g:Lo3/c;

.field private static final h:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lu/b$e;

    invoke-direct {v0}, Lu/b$e;-><init>()V

    sput-object v0, Lu/b$e;->a:Lu/b$e;

    const-string v0, "requestTimeMs"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$e;->b:Lo3/c;

    const-string v0, "requestUptimeMs"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$e;->c:Lo3/c;

    const-string v0, "clientInfo"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$e;->d:Lo3/c;

    const-string v0, "logSource"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$e;->e:Lo3/c;

    const-string v0, "logSourceName"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$e;->f:Lo3/c;

    const-string v0, "logEvent"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$e;->g:Lo3/c;

    const-string v0, "qosTier"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$e;->h:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lu/m;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lu/b$e;->b(Lu/m;Lo3/e;)V

    return-void
.end method

.method public b(Lu/m;Lo3/e;)V
    .locals 3

    sget-object v0, Lu/b$e;->b:Lo3/c;

    invoke-virtual {p1}, Lu/m;->g()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lu/b$e;->c:Lo3/c;

    invoke-virtual {p1}, Lu/m;->h()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lu/b$e;->d:Lo3/c;

    invoke-virtual {p1}, Lu/m;->b()Lu/k;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$e;->e:Lo3/c;

    invoke-virtual {p1}, Lu/m;->d()Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$e;->f:Lo3/c;

    invoke-virtual {p1}, Lu/m;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$e;->g:Lo3/c;

    invoke-virtual {p1}, Lu/m;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$e;->h:Lo3/c;

    invoke-virtual {p1}, Lu/m;->f()Lu/p;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    return-void
.end method
