.class final Lu/b$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lu/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Lu/a;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lu/b$a;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;

.field private static final d:Lo3/c;

.field private static final e:Lo3/c;

.field private static final f:Lo3/c;

.field private static final g:Lo3/c;

.field private static final h:Lo3/c;

.field private static final i:Lo3/c;

.field private static final j:Lo3/c;

.field private static final k:Lo3/c;

.field private static final l:Lo3/c;

.field private static final m:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lu/b$a;

    invoke-direct {v0}, Lu/b$a;-><init>()V

    sput-object v0, Lu/b$a;->a:Lu/b$a;

    const-string v0, "sdkVersion"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$a;->b:Lo3/c;

    const-string v0, "model"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$a;->c:Lo3/c;

    const-string v0, "hardware"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$a;->d:Lo3/c;

    const-string v0, "device"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$a;->e:Lo3/c;

    const-string v0, "product"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$a;->f:Lo3/c;

    const-string v0, "osBuild"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$a;->g:Lo3/c;

    const-string v0, "manufacturer"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$a;->h:Lo3/c;

    const-string v0, "fingerprint"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$a;->i:Lo3/c;

    const-string v0, "locale"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$a;->j:Lo3/c;

    const-string v0, "country"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$a;->k:Lo3/c;

    const-string v0, "mccMnc"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$a;->l:Lo3/c;

    const-string v0, "applicationBuild"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$a;->m:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lu/a;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lu/b$a;->b(Lu/a;Lo3/e;)V

    return-void
.end method

.method public b(Lu/a;Lo3/e;)V
    .locals 2

    sget-object v0, Lu/b$a;->b:Lo3/c;

    invoke-virtual {p1}, Lu/a;->m()Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$a;->c:Lo3/c;

    invoke-virtual {p1}, Lu/a;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$a;->d:Lo3/c;

    invoke-virtual {p1}, Lu/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$a;->e:Lo3/c;

    invoke-virtual {p1}, Lu/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$a;->f:Lo3/c;

    invoke-virtual {p1}, Lu/a;->l()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$a;->g:Lo3/c;

    invoke-virtual {p1}, Lu/a;->k()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$a;->h:Lo3/c;

    invoke-virtual {p1}, Lu/a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$a;->i:Lo3/c;

    invoke-virtual {p1}, Lu/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$a;->j:Lo3/c;

    invoke-virtual {p1}, Lu/a;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$a;->k:Lo3/c;

    invoke-virtual {p1}, Lu/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$a;->l:Lo3/c;

    invoke-virtual {p1}, Lu/a;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$a;->m:Lo3/c;

    invoke-virtual {p1}, Lu/a;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    return-void
.end method
