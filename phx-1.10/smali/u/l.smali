.class public abstract Lu/l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lu/l$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()Lu/l$a;
    .locals 1

    new-instance v0, Lu/f$b;

    invoke-direct {v0}, Lu/f$b;-><init>()V

    return-object v0
.end method

.method public static i(Ljava/lang/String;)Lu/l$a;
    .locals 1

    invoke-static {}, Lu/l;->a()Lu/l$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lu/l$a;->g(Ljava/lang/String;)Lu/l$a;

    move-result-object p0

    return-object p0
.end method

.method public static j([B)Lu/l$a;
    .locals 1

    invoke-static {}, Lu/l;->a()Lu/l$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lu/l$a;->f([B)Lu/l$a;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract b()Ljava/lang/Integer;
.end method

.method public abstract c()J
.end method

.method public abstract d()J
.end method

.method public abstract e()Lu/o;
.end method

.method public abstract f()[B
.end method

.method public abstract g()Ljava/lang/String;
.end method

.method public abstract h()J
.end method
