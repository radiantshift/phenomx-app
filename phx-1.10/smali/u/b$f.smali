.class final Lu/b$f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lu/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Lu/o;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lu/b$f;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lu/b$f;

    invoke-direct {v0}, Lu/b$f;-><init>()V

    sput-object v0, Lu/b$f;->a:Lu/b$f;

    const-string v0, "networkType"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$f;->b:Lo3/c;

    const-string v0, "mobileSubtype"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lu/b$f;->c:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lu/o;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lu/b$f;->b(Lu/o;Lo3/e;)V

    return-void
.end method

.method public b(Lu/o;Lo3/e;)V
    .locals 2

    sget-object v0, Lu/b$f;->b:Lo3/c;

    invoke-virtual {p1}, Lu/o;->c()Lu/o$c;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lu/b$f;->c:Lo3/c;

    invoke-virtual {p1}, Lu/o;->b()Lu/o$b;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    return-void
.end method
