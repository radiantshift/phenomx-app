.class final Lg3/a$s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg3/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "s"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Lg3/b0$e$d;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lg3/a$s;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;

.field private static final d:Lo3/c;

.field private static final e:Lo3/c;

.field private static final f:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lg3/a$s;

    invoke-direct {v0}, Lg3/a$s;-><init>()V

    sput-object v0, Lg3/a$s;->a:Lg3/a$s;

    const-string v0, "timestamp"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$s;->b:Lo3/c;

    const-string v0, "type"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$s;->c:Lo3/c;

    const-string v0, "app"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$s;->d:Lo3/c;

    const-string v0, "device"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$s;->e:Lo3/c;

    const-string v0, "log"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$s;->f:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lg3/b0$e$d;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lg3/a$s;->b(Lg3/b0$e$d;Lo3/e;)V

    return-void
.end method

.method public b(Lg3/b0$e$d;Lo3/e;)V
    .locals 3

    sget-object v0, Lg3/a$s;->b:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d;->e()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lg3/a$s;->c:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$s;->d:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d;->b()Lg3/b0$e$d$a;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$s;->e:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d;->c()Lg3/b0$e$d$c;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$s;->f:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d;->d()Lg3/b0$e$d$d;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    return-void
.end method
