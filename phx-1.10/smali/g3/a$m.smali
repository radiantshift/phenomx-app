.class final Lg3/a$m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg3/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "m"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Lg3/b0$e$d$a$b;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lg3/a$m;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;

.field private static final d:Lo3/c;

.field private static final e:Lo3/c;

.field private static final f:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lg3/a$m;

    invoke-direct {v0}, Lg3/a$m;-><init>()V

    sput-object v0, Lg3/a$m;->a:Lg3/a$m;

    const-string v0, "threads"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$m;->b:Lo3/c;

    const-string v0, "exception"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$m;->c:Lo3/c;

    const-string v0, "appExitInfo"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$m;->d:Lo3/c;

    const-string v0, "signal"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$m;->e:Lo3/c;

    const-string v0, "binaries"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$m;->f:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lg3/b0$e$d$a$b;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lg3/a$m;->b(Lg3/b0$e$d$a$b;Lo3/e;)V

    return-void
.end method

.method public b(Lg3/b0$e$d$a$b;Lo3/e;)V
    .locals 2

    sget-object v0, Lg3/a$m;->b:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d$a$b;->f()Lg3/c0;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$m;->c:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d$a$b;->d()Lg3/b0$e$d$a$b$c;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$m;->d:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d$a$b;->b()Lg3/b0$a;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$m;->e:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d$a$b;->e()Lg3/b0$e$d$a$b$d;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$m;->f:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d$a$b;->c()Lg3/c0;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    return-void
.end method
