.class final Lg3/a$j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg3/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "j"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Lg3/b0$e;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lg3/a$j;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;

.field private static final d:Lo3/c;

.field private static final e:Lo3/c;

.field private static final f:Lo3/c;

.field private static final g:Lo3/c;

.field private static final h:Lo3/c;

.field private static final i:Lo3/c;

.field private static final j:Lo3/c;

.field private static final k:Lo3/c;

.field private static final l:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lg3/a$j;

    invoke-direct {v0}, Lg3/a$j;-><init>()V

    sput-object v0, Lg3/a$j;->a:Lg3/a$j;

    const-string v0, "generator"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$j;->b:Lo3/c;

    const-string v0, "identifier"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$j;->c:Lo3/c;

    const-string v0, "startedAt"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$j;->d:Lo3/c;

    const-string v0, "endedAt"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$j;->e:Lo3/c;

    const-string v0, "crashed"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$j;->f:Lo3/c;

    const-string v0, "app"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$j;->g:Lo3/c;

    const-string v0, "user"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$j;->h:Lo3/c;

    const-string v0, "os"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$j;->i:Lo3/c;

    const-string v0, "device"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$j;->j:Lo3/c;

    const-string v0, "events"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$j;->k:Lo3/c;

    const-string v0, "generatorType"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$j;->l:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lg3/b0$e;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lg3/a$j;->b(Lg3/b0$e;Lo3/e;)V

    return-void
.end method

.method public b(Lg3/b0$e;Lo3/e;)V
    .locals 3

    sget-object v0, Lg3/a$j;->b:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$j;->c:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e;->i()[B

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$j;->d:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e;->k()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lg3/a$j;->e:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e;->d()Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$j;->f:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e;->m()Z

    move-result v1

    invoke-interface {p2, v0, v1}, Lo3/e;->d(Lo3/c;Z)Lo3/e;

    sget-object v0, Lg3/a$j;->g:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e;->b()Lg3/b0$e$a;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$j;->h:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e;->l()Lg3/b0$e$f;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$j;->i:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e;->j()Lg3/b0$e$e;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$j;->j:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e;->c()Lg3/b0$e$c;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$j;->k:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e;->e()Lg3/c0;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$j;->l:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e;->g()I

    move-result p1

    invoke-interface {p2, v0, p1}, Lo3/e;->c(Lo3/c;I)Lo3/e;

    return-void
.end method
