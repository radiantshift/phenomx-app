.class public abstract Lg3/b0$e$b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg3/b0$e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Lg3/b0$e;
.end method

.method public abstract b(Lg3/b0$e$a;)Lg3/b0$e$b;
.end method

.method public abstract c(Z)Lg3/b0$e$b;
.end method

.method public abstract d(Lg3/b0$e$c;)Lg3/b0$e$b;
.end method

.method public abstract e(Ljava/lang/Long;)Lg3/b0$e$b;
.end method

.method public abstract f(Lg3/c0;)Lg3/b0$e$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg3/c0<",
            "Lg3/b0$e$d;",
            ">;)",
            "Lg3/b0$e$b;"
        }
    .end annotation
.end method

.method public abstract g(Ljava/lang/String;)Lg3/b0$e$b;
.end method

.method public abstract h(I)Lg3/b0$e$b;
.end method

.method public abstract i(Ljava/lang/String;)Lg3/b0$e$b;
.end method

.method public j([B)Lg3/b0$e$b;
    .locals 2

    new-instance v0, Ljava/lang/String;

    invoke-static {}, Lg3/b0;->a()Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {p0, v0}, Lg3/b0$e$b;->i(Ljava/lang/String;)Lg3/b0$e$b;

    move-result-object p1

    return-object p1
.end method

.method public abstract k(Lg3/b0$e$e;)Lg3/b0$e$b;
.end method

.method public abstract l(J)Lg3/b0$e$b;
.end method

.method public abstract m(Lg3/b0$e$f;)Lg3/b0$e$b;
.end method
