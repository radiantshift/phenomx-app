.class final Lg3/a$d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg3/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Lg3/b0;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lg3/a$d;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;

.field private static final d:Lo3/c;

.field private static final e:Lo3/c;

.field private static final f:Lo3/c;

.field private static final g:Lo3/c;

.field private static final h:Lo3/c;

.field private static final i:Lo3/c;

.field private static final j:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lg3/a$d;

    invoke-direct {v0}, Lg3/a$d;-><init>()V

    sput-object v0, Lg3/a$d;->a:Lg3/a$d;

    const-string v0, "sdkVersion"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$d;->b:Lo3/c;

    const-string v0, "gmpAppId"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$d;->c:Lo3/c;

    const-string v0, "platform"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$d;->d:Lo3/c;

    const-string v0, "installationUuid"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$d;->e:Lo3/c;

    const-string v0, "buildVersion"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$d;->f:Lo3/c;

    const-string v0, "displayVersion"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$d;->g:Lo3/c;

    const-string v0, "session"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$d;->h:Lo3/c;

    const-string v0, "ndkPayload"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$d;->i:Lo3/c;

    const-string v0, "appExitInfo"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$d;->j:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lg3/b0;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lg3/a$d;->b(Lg3/b0;Lo3/e;)V

    return-void
.end method

.method public b(Lg3/b0;Lo3/e;)V
    .locals 2

    sget-object v0, Lg3/a$d;->b:Lo3/c;

    invoke-virtual {p1}, Lg3/b0;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$d;->c:Lo3/c;

    invoke-virtual {p1}, Lg3/b0;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$d;->d:Lo3/c;

    invoke-virtual {p1}, Lg3/b0;->i()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lo3/e;->c(Lo3/c;I)Lo3/e;

    sget-object v0, Lg3/a$d;->e:Lo3/c;

    invoke-virtual {p1}, Lg3/b0;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$d;->f:Lo3/c;

    invoke-virtual {p1}, Lg3/b0;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$d;->g:Lo3/c;

    invoke-virtual {p1}, Lg3/b0;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$d;->h:Lo3/c;

    invoke-virtual {p1}, Lg3/b0;->k()Lg3/b0$e;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$d;->i:Lo3/c;

    invoke-virtual {p1}, Lg3/b0;->h()Lg3/b0$d;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$d;->j:Lo3/c;

    invoke-virtual {p1}, Lg3/b0;->c()Lg3/b0$a;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    return-void
.end method
