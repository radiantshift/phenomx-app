.class final Lg3/a$r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg3/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "r"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Lg3/b0$e$d$c;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lg3/a$r;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;

.field private static final d:Lo3/c;

.field private static final e:Lo3/c;

.field private static final f:Lo3/c;

.field private static final g:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lg3/a$r;

    invoke-direct {v0}, Lg3/a$r;-><init>()V

    sput-object v0, Lg3/a$r;->a:Lg3/a$r;

    const-string v0, "batteryLevel"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$r;->b:Lo3/c;

    const-string v0, "batteryVelocity"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$r;->c:Lo3/c;

    const-string v0, "proximityOn"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$r;->d:Lo3/c;

    const-string v0, "orientation"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$r;->e:Lo3/c;

    const-string v0, "ramUsed"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$r;->f:Lo3/c;

    const-string v0, "diskUsed"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$r;->g:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lg3/b0$e$d$c;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lg3/a$r;->b(Lg3/b0$e$d$c;Lo3/e;)V

    return-void
.end method

.method public b(Lg3/b0$e$d$c;Lo3/e;)V
    .locals 3

    sget-object v0, Lg3/a$r;->b:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d$c;->b()Ljava/lang/Double;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$r;->c:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d$c;->c()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lo3/e;->c(Lo3/c;I)Lo3/e;

    sget-object v0, Lg3/a$r;->d:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d$c;->g()Z

    move-result v1

    invoke-interface {p2, v0, v1}, Lo3/e;->d(Lo3/c;Z)Lo3/e;

    sget-object v0, Lg3/a$r;->e:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d$c;->e()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lo3/e;->c(Lo3/c;I)Lo3/e;

    sget-object v0, Lg3/a$r;->f:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d$c;->f()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lg3/a$r;->g:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$d$c;->d()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    return-void
.end method
