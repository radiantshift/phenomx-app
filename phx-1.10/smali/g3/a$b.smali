.class final Lg3/a$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg3/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Lg3/b0$a;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lg3/a$b;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;

.field private static final d:Lo3/c;

.field private static final e:Lo3/c;

.field private static final f:Lo3/c;

.field private static final g:Lo3/c;

.field private static final h:Lo3/c;

.field private static final i:Lo3/c;

.field private static final j:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lg3/a$b;

    invoke-direct {v0}, Lg3/a$b;-><init>()V

    sput-object v0, Lg3/a$b;->a:Lg3/a$b;

    const-string v0, "pid"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$b;->b:Lo3/c;

    const-string v0, "processName"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$b;->c:Lo3/c;

    const-string v0, "reasonCode"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$b;->d:Lo3/c;

    const-string v0, "importance"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$b;->e:Lo3/c;

    const-string v0, "pss"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$b;->f:Lo3/c;

    const-string v0, "rss"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$b;->g:Lo3/c;

    const-string v0, "timestamp"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$b;->h:Lo3/c;

    const-string v0, "traceFile"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$b;->i:Lo3/c;

    const-string v0, "buildIdMappingForArch"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$b;->j:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lg3/b0$a;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lg3/a$b;->b(Lg3/b0$a;Lo3/e;)V

    return-void
.end method

.method public b(Lg3/b0$a;Lo3/e;)V
    .locals 3

    sget-object v0, Lg3/a$b;->b:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$a;->d()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lo3/e;->c(Lo3/c;I)Lo3/e;

    sget-object v0, Lg3/a$b;->c:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$b;->d:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$a;->g()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lo3/e;->c(Lo3/c;I)Lo3/e;

    sget-object v0, Lg3/a$b;->e:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$a;->c()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lo3/e;->c(Lo3/c;I)Lo3/e;

    sget-object v0, Lg3/a$b;->f:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$a;->f()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lg3/a$b;->g:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$a;->h()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lg3/a$b;->h:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$a;->i()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lg3/a$b;->i:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$a;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$b;->j:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$a;->b()Lg3/c0;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    return-void
.end method
