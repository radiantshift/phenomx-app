.class final Lg3/a$g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg3/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Lg3/b0$e$a;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lg3/a$g;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;

.field private static final d:Lo3/c;

.field private static final e:Lo3/c;

.field private static final f:Lo3/c;

.field private static final g:Lo3/c;

.field private static final h:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lg3/a$g;

    invoke-direct {v0}, Lg3/a$g;-><init>()V

    sput-object v0, Lg3/a$g;->a:Lg3/a$g;

    const-string v0, "identifier"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$g;->b:Lo3/c;

    const-string v0, "version"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$g;->c:Lo3/c;

    const-string v0, "displayVersion"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$g;->d:Lo3/c;

    const-string v0, "organization"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$g;->e:Lo3/c;

    const-string v0, "installationUuid"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$g;->f:Lo3/c;

    const-string v0, "developmentPlatform"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$g;->g:Lo3/c;

    const-string v0, "developmentPlatformVersion"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$g;->h:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lg3/b0$e$a;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lg3/a$g;->b(Lg3/b0$e$a;Lo3/e;)V

    return-void
.end method

.method public b(Lg3/b0$e$a;Lo3/e;)V
    .locals 2

    sget-object v0, Lg3/a$g;->b:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$g;->c:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$g;->d:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$g;->e:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$a;->g()Lg3/b0$e$a$b;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$g;->f:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$g;->g:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$g;->h:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$a;->c()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    return-void
.end method
