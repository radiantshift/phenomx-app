.class public abstract Lg3/b0$e;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg3/b0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg3/b0$e$d;,
        Lg3/b0$e$c;,
        Lg3/b0$e$e;,
        Lg3/b0$e$a;,
        Lg3/b0$e$f;,
        Lg3/b0$e$b;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lg3/b0$e$b;
    .locals 2

    new-instance v0, Lg3/h$b;

    invoke-direct {v0}, Lg3/h$b;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lg3/h$b;->c(Z)Lg3/b0$e$b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract b()Lg3/b0$e$a;
.end method

.method public abstract c()Lg3/b0$e$c;
.end method

.method public abstract d()Ljava/lang/Long;
.end method

.method public abstract e()Lg3/c0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lg3/c0<",
            "Lg3/b0$e$d;",
            ">;"
        }
    .end annotation
.end method

.method public abstract f()Ljava/lang/String;
.end method

.method public abstract g()I
.end method

.method public abstract h()Ljava/lang/String;
.end method

.method public i()[B
    .locals 2

    invoke-virtual {p0}, Lg3/b0$e;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lg3/b0;->a()Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    return-object v0
.end method

.method public abstract j()Lg3/b0$e$e;
.end method

.method public abstract k()J
.end method

.method public abstract l()Lg3/b0$e$f;
.end method

.method public abstract m()Z
.end method

.method public abstract n()Lg3/b0$e$b;
.end method

.method o(Lg3/c0;)Lg3/b0$e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg3/c0<",
            "Lg3/b0$e$d;",
            ">;)",
            "Lg3/b0$e;"
        }
    .end annotation

    invoke-virtual {p0}, Lg3/b0$e;->n()Lg3/b0$e$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lg3/b0$e$b;->f(Lg3/c0;)Lg3/b0$e$b;

    move-result-object p1

    invoke-virtual {p1}, Lg3/b0$e$b;->a()Lg3/b0$e;

    move-result-object p1

    return-object p1
.end method

.method p(JZLjava/lang/String;)Lg3/b0$e;
    .locals 1

    invoke-virtual {p0}, Lg3/b0$e;->n()Lg3/b0$e$b;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lg3/b0$e$b;->e(Ljava/lang/Long;)Lg3/b0$e$b;

    invoke-virtual {v0, p3}, Lg3/b0$e$b;->c(Z)Lg3/b0$e$b;

    if-eqz p4, :cond_0

    invoke-static {}, Lg3/b0$e$f;->a()Lg3/b0$e$f$a;

    move-result-object p1

    invoke-virtual {p1, p4}, Lg3/b0$e$f$a;->b(Ljava/lang/String;)Lg3/b0$e$f$a;

    move-result-object p1

    invoke-virtual {p1}, Lg3/b0$e$f$a;->a()Lg3/b0$e$f;

    move-result-object p1

    invoke-virtual {v0, p1}, Lg3/b0$e$b;->m(Lg3/b0$e$f;)Lg3/b0$e$b;

    :cond_0
    invoke-virtual {v0}, Lg3/b0$e$b;->a()Lg3/b0$e;

    move-result-object p1

    return-object p1
.end method
