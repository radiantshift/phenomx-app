.class final Lg3/a$i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lo3/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg3/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "i"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lo3/d<",
        "Lg3/b0$e$c;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Lg3/a$i;

.field private static final b:Lo3/c;

.field private static final c:Lo3/c;

.field private static final d:Lo3/c;

.field private static final e:Lo3/c;

.field private static final f:Lo3/c;

.field private static final g:Lo3/c;

.field private static final h:Lo3/c;

.field private static final i:Lo3/c;

.field private static final j:Lo3/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lg3/a$i;

    invoke-direct {v0}, Lg3/a$i;-><init>()V

    sput-object v0, Lg3/a$i;->a:Lg3/a$i;

    const-string v0, "arch"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$i;->b:Lo3/c;

    const-string v0, "model"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$i;->c:Lo3/c;

    const-string v0, "cores"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$i;->d:Lo3/c;

    const-string v0, "ram"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$i;->e:Lo3/c;

    const-string v0, "diskSpace"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$i;->f:Lo3/c;

    const-string v0, "simulator"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$i;->g:Lo3/c;

    const-string v0, "state"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$i;->h:Lo3/c;

    const-string v0, "manufacturer"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$i;->i:Lo3/c;

    const-string v0, "modelClass"

    invoke-static {v0}, Lo3/c;->d(Ljava/lang/String;)Lo3/c;

    move-result-object v0

    sput-object v0, Lg3/a$i;->j:Lo3/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lg3/b0$e$c;

    check-cast p2, Lo3/e;

    invoke-virtual {p0, p1, p2}, Lg3/a$i;->b(Lg3/b0$e$c;Lo3/e;)V

    return-void
.end method

.method public b(Lg3/b0$e$c;Lo3/e;)V
    .locals 3

    sget-object v0, Lg3/a$i;->b:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$c;->b()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lo3/e;->c(Lo3/c;I)Lo3/e;

    sget-object v0, Lg3/a$i;->c:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$c;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$i;->d:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$c;->c()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lo3/e;->c(Lo3/c;I)Lo3/e;

    sget-object v0, Lg3/a$i;->e:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$c;->h()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lg3/a$i;->f:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$c;->d()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lo3/e;->b(Lo3/c;J)Lo3/e;

    sget-object v0, Lg3/a$i;->g:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$c;->j()Z

    move-result v1

    invoke-interface {p2, v0, v1}, Lo3/e;->d(Lo3/c;Z)Lo3/e;

    sget-object v0, Lg3/a$i;->h:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$c;->i()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lo3/e;->c(Lo3/c;I)Lo3/e;

    sget-object v0, Lg3/a$i;->i:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$c;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    sget-object v0, Lg3/a$i;->j:Lo3/c;

    invoke-virtual {p1}, Lg3/b0$e$c;->g()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lo3/e;->a(Lo3/c;Ljava/lang/Object;)Lo3/e;

    return-void
.end method
