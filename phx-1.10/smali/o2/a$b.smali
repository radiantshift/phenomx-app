.class final enum Lo2/a$b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lo2/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lo2/a$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum f:Lo2/a$b;

.field public static final enum g:Lo2/a$b;

.field public static final enum h:Lo2/a$b;

.field public static final enum i:Lo2/a$b;

.field private static final synthetic j:[Lo2/a$b;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lo2/a$b;

    const-string v1, "READY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lo2/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lo2/a$b;->f:Lo2/a$b;

    new-instance v0, Lo2/a$b;

    const-string v1, "NOT_READY"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lo2/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lo2/a$b;->g:Lo2/a$b;

    new-instance v0, Lo2/a$b;

    const-string v1, "DONE"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lo2/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lo2/a$b;->h:Lo2/a$b;

    new-instance v0, Lo2/a$b;

    const-string v1, "FAILED"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lo2/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lo2/a$b;->i:Lo2/a$b;

    invoke-static {}, Lo2/a$b;->a()[Lo2/a$b;

    move-result-object v0

    sput-object v0, Lo2/a$b;->j:[Lo2/a$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static synthetic a()[Lo2/a$b;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Lo2/a$b;

    sget-object v1, Lo2/a$b;->f:Lo2/a$b;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lo2/a$b;->g:Lo2/a$b;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lo2/a$b;->h:Lo2/a$b;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lo2/a$b;->i:Lo2/a$b;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lo2/a$b;
    .locals 1

    const-class v0, Lo2/a$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lo2/a$b;

    return-object p0
.end method

.method public static values()[Lo2/a$b;
    .locals 1

    sget-object v0, Lo2/a$b;->j:[Lo2/a$b;

    invoke-virtual {v0}, [Lo2/a$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lo2/a$b;

    return-object v0
.end method
