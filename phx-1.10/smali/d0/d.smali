.class public interface abstract Ld0/d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# virtual methods
.method public abstract A(Ljava/lang/Iterable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ld0/k;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract C(Lv/p;J)V
.end method

.method public abstract e()I
.end method

.method public abstract f(Ljava/lang/Iterable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ld0/k;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract g(Lv/p;Lv/i;)Ld0/k;
.end method

.method public abstract m(Lv/p;)Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv/p;",
            ")",
            "Ljava/lang/Iterable<",
            "Ld0/k;",
            ">;"
        }
    .end annotation
.end method

.method public abstract p()Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Lv/p;",
            ">;"
        }
    .end annotation
.end method

.method public abstract q(Lv/p;)J
.end method

.method public abstract z(Lv/p;)Z
.end method
