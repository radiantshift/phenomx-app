.class public final Ld0/n0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lx/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lx/b<",
        "Ld0/m0;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lf0/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lf0/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Ld0/e;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Ld0/t0;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Ld0/e;",
            ">;",
            "Lj5/a<",
            "Ld0/t0;",
            ">;",
            "Lj5/a<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld0/n0;->a:Lj5/a;

    iput-object p2, p0, Ld0/n0;->b:Lj5/a;

    iput-object p3, p0, Ld0/n0;->c:Lj5/a;

    iput-object p4, p0, Ld0/n0;->d:Lj5/a;

    iput-object p5, p0, Ld0/n0;->e:Lj5/a;

    return-void
.end method

.method public static a(Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;)Ld0/n0;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Ld0/e;",
            ">;",
            "Lj5/a<",
            "Ld0/t0;",
            ">;",
            "Lj5/a<",
            "Ljava/lang/String;",
            ">;)",
            "Ld0/n0;"
        }
    .end annotation

    new-instance v6, Ld0/n0;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Ld0/n0;-><init>(Lj5/a;Lj5/a;Lj5/a;Lj5/a;Lj5/a;)V

    return-object v6
.end method

.method public static c(Lf0/a;Lf0/a;Ljava/lang/Object;Ljava/lang/Object;Lj5/a;)Ld0/m0;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/a;",
            "Lf0/a;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Lj5/a<",
            "Ljava/lang/String;",
            ">;)",
            "Ld0/m0;"
        }
    .end annotation

    new-instance v6, Ld0/m0;

    move-object v3, p2

    check-cast v3, Ld0/e;

    move-object v4, p3

    check-cast v4, Ld0/t0;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Ld0/m0;-><init>(Lf0/a;Lf0/a;Ld0/e;Ld0/t0;Lj5/a;)V

    return-object v6
.end method


# virtual methods
.method public b()Ld0/m0;
    .locals 5

    iget-object v0, p0, Ld0/n0;->a:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf0/a;

    iget-object v1, p0, Ld0/n0;->b:Lj5/a;

    invoke-interface {v1}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf0/a;

    iget-object v2, p0, Ld0/n0;->c:Lj5/a;

    invoke-interface {v2}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Ld0/n0;->d:Lj5/a;

    invoke-interface {v3}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Ld0/n0;->e:Lj5/a;

    invoke-static {v0, v1, v2, v3, v4}, Ld0/n0;->c(Lf0/a;Lf0/a;Ljava/lang/Object;Ljava/lang/Object;Lj5/a;)Ld0/m0;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Ld0/n0;->b()Ld0/m0;

    move-result-object v0

    return-object v0
.end method
