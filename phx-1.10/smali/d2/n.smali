.class public final Ld2/n;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field private final f:Ld2/l;

.field private final g:Ld2/p;

.field private final h:[B

.field private i:Z

.field private j:Z

.field private k:J


# direct methods
.method public constructor <init>(Ld2/l;Ld2/p;)V
    .locals 1

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Ld2/n;->i:Z

    iput-boolean v0, p0, Ld2/n;->j:Z

    iput-object p1, p0, Ld2/n;->f:Ld2/l;

    iput-object p2, p0, Ld2/n;->g:Ld2/p;

    const/4 p1, 0x1

    new-array p1, p1, [B

    iput-object p1, p0, Ld2/n;->h:[B

    return-void
.end method

.method private a()V
    .locals 2

    iget-boolean v0, p0, Ld2/n;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ld2/n;->f:Ld2/l;

    iget-object v1, p0, Ld2/n;->g:Ld2/p;

    invoke-interface {v0, v1}, Ld2/l;->b(Ld2/p;)J

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld2/n;->i:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public b()V
    .locals 0

    invoke-direct {p0}, Ld2/n;->a()V

    return-void
.end method

.method public close()V
    .locals 1

    iget-boolean v0, p0, Ld2/n;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ld2/n;->f:Ld2/l;

    invoke-interface {v0}, Ld2/l;->close()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld2/n;->j:Z

    :cond_0
    return-void
.end method

.method public read()I
    .locals 2

    iget-object v0, p0, Ld2/n;->h:[B

    invoke-virtual {p0, v0}, Ld2/n;->read([B)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ld2/n;->h:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    and-int/lit16 v1, v0, 0xff

    :goto_0
    return v1
.end method

.method public read([B)I
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Ld2/n;->read([BII)I

    move-result p1

    return p1
.end method

.method public read([BII)I
    .locals 2

    iget-boolean v0, p0, Ld2/n;->j:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Le2/a;->f(Z)V

    invoke-direct {p0}, Ld2/n;->a()V

    iget-object v0, p0, Ld2/n;->f:Ld2/l;

    invoke-interface {v0, p1, p2, p3}, Ld2/i;->read([BII)I

    move-result p1

    const/4 p2, -0x1

    if-ne p1, p2, :cond_0

    return p2

    :cond_0
    iget-wide p2, p0, Ld2/n;->k:J

    int-to-long v0, p1

    add-long/2addr p2, v0

    iput-wide p2, p0, Ld2/n;->k:J

    return p1
.end method
