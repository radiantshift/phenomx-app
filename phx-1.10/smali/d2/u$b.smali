.class public final Ld2/u$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ld2/l$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld2/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field private final a:Ld2/d0;

.field private b:Ld2/p0;

.field private c:Lo2/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lo2/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private e:I

.field private f:I

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ld2/d0;

    invoke-direct {v0}, Ld2/d0;-><init>()V

    iput-object v0, p0, Ld2/u$b;->a:Ld2/d0;

    const/16 v0, 0x1f40

    iput v0, p0, Ld2/u$b;->e:I

    iput v0, p0, Ld2/u$b;->f:I

    return-void
.end method


# virtual methods
.method public bridge synthetic a()Ld2/l;
    .locals 1

    invoke-virtual {p0}, Ld2/u$b;->b()Ld2/u;

    move-result-object v0

    return-object v0
.end method

.method public b()Ld2/u;
    .locals 10

    new-instance v9, Ld2/u;

    iget-object v1, p0, Ld2/u$b;->d:Ljava/lang/String;

    iget v2, p0, Ld2/u$b;->e:I

    iget v3, p0, Ld2/u$b;->f:I

    iget-boolean v4, p0, Ld2/u$b;->g:Z

    iget-object v5, p0, Ld2/u$b;->a:Ld2/d0;

    iget-object v6, p0, Ld2/u$b;->c:Lo2/l;

    iget-boolean v7, p0, Ld2/u$b;->h:Z

    const/4 v8, 0x0

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Ld2/u;-><init>(Ljava/lang/String;IIZLd2/d0;Lo2/l;ZLd2/u$a;)V

    iget-object v0, p0, Ld2/u$b;->b:Ld2/p0;

    if-eqz v0, :cond_0

    invoke-virtual {v9, v0}, Ld2/g;->i(Ld2/p0;)V

    :cond_0
    return-object v9
.end method

.method public c(Z)Ld2/u$b;
    .locals 0

    iput-boolean p1, p0, Ld2/u$b;->g:Z

    return-object p0
.end method

.method public final d(Ljava/util/Map;)Ld2/u$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ld2/u$b;"
        }
    .end annotation

    iget-object v0, p0, Ld2/u$b;->a:Ld2/d0;

    invoke-virtual {v0, p1}, Ld2/d0;->a(Ljava/util/Map;)V

    return-object p0
.end method

.method public e(Ljava/lang/String;)Ld2/u$b;
    .locals 0

    iput-object p1, p0, Ld2/u$b;->d:Ljava/lang/String;

    return-object p0
.end method
