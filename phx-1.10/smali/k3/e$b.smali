.class final Lk3/e$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lk3/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field private final f:Le3/p;

.field private final g:Ln2/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln2/j<",
            "Le3/p;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic h:Lk3/e;


# direct methods
.method private constructor <init>(Lk3/e;Le3/p;Ln2/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le3/p;",
            "Ln2/j<",
            "Le3/p;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lk3/e$b;->h:Lk3/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lk3/e$b;->f:Le3/p;

    iput-object p3, p0, Lk3/e$b;->g:Ln2/j;

    return-void
.end method

.method synthetic constructor <init>(Lk3/e;Le3/p;Ln2/j;Lk3/e$a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lk3/e$b;-><init>(Lk3/e;Le3/p;Ln2/j;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v0, p0, Lk3/e$b;->h:Lk3/e;

    iget-object v1, p0, Lk3/e$b;->f:Le3/p;

    iget-object v2, p0, Lk3/e$b;->g:Ln2/j;

    invoke-static {v0, v1, v2}, Lk3/e;->c(Lk3/e;Le3/p;Ln2/j;)V

    iget-object v0, p0, Lk3/e$b;->h:Lk3/e;

    invoke-static {v0}, Lk3/e;->d(Lk3/e;)Le3/b0;

    move-result-object v0

    invoke-virtual {v0}, Le3/b0;->e()V

    iget-object v0, p0, Lk3/e$b;->h:Lk3/e;

    invoke-static {v0}, Lk3/e;->e(Lk3/e;)D

    move-result-wide v0

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Delay for: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double v6, v0, v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    const-string v6, "%.2f"

    invoke-static {v4, v6, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " s for report: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lk3/e$b;->f:Le3/p;

    invoke-virtual {v4}, Le3/p;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lb3/f;->b(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lk3/e;->f(D)V

    return-void
.end method
