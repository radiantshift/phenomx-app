.class public Lcom/google/firebase/remoteconfig/RemoteConfigRegistrar;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/google/firebase/components/ComponentRegistrar;


# static fields
.field private static final LIBRARY_NAME:Ljava/lang/String; = "fire-rc"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Ly2/f0;Ly2/e;)Lcom/google/firebase/remoteconfig/c;
    .locals 0

    invoke-static {p0, p1}, Lcom/google/firebase/remoteconfig/RemoteConfigRegistrar;->lambda$getComponents$0(Ly2/f0;Ly2/e;)Lcom/google/firebase/remoteconfig/c;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic lambda$getComponents$0(Ly2/f0;Ly2/e;)Lcom/google/firebase/remoteconfig/c;
    .locals 8

    new-instance v7, Lcom/google/firebase/remoteconfig/c;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, Ly2/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/content/Context;

    invoke-interface {p1, p0}, Ly2/e;->g(Ly2/f0;)Ljava/lang/Object;

    move-result-object p0

    move-object v2, p0

    check-cast v2, Ljava/util/concurrent/ScheduledExecutorService;

    const-class p0, Lt2/e;

    invoke-interface {p1, p0}, Ly2/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    move-object v3, p0

    check-cast v3, Lt2/e;

    const-class p0, Lv3/e;

    invoke-interface {p1, p0}, Ly2/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    move-object v4, p0

    check-cast v4, Lv3/e;

    const-class p0, Lcom/google/firebase/abt/component/a;

    invoke-interface {p1, p0}, Ly2/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/firebase/abt/component/a;

    const-string v0, "frc"

    invoke-virtual {p0, v0}, Lcom/google/firebase/abt/component/a;->b(Ljava/lang/String;)Lu2/c;

    move-result-object v5

    const-class p0, Lw2/a;

    invoke-interface {p1, p0}, Ly2/e;->f(Ljava/lang/Class;)Lu3/b;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/firebase/remoteconfig/c;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lt2/e;Lv3/e;Lu2/c;Lu3/b;)V

    return-object v7
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ly2/c<",
            "*>;>;"
        }
    .end annotation

    const-class v0, Lx2/b;

    const-class v1, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1}, Ly2/f0;->a(Ljava/lang/Class;Ljava/lang/Class;)Ly2/f0;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ly2/c;

    const-class v2, Lcom/google/firebase/remoteconfig/c;

    invoke-static {v2}, Ly2/c;->c(Ljava/lang/Class;)Ly2/c$b;

    move-result-object v2

    const-string v3, "fire-rc"

    invoke-virtual {v2, v3}, Ly2/c$b;->g(Ljava/lang/String;)Ly2/c$b;

    move-result-object v2

    const-class v4, Landroid/content/Context;

    invoke-static {v4}, Ly2/r;->i(Ljava/lang/Class;)Ly2/r;

    move-result-object v4

    invoke-virtual {v2, v4}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v2

    invoke-static {v0}, Ly2/r;->j(Ly2/f0;)Ly2/r;

    move-result-object v4

    invoke-virtual {v2, v4}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v2

    const-class v4, Lt2/e;

    invoke-static {v4}, Ly2/r;->i(Ljava/lang/Class;)Ly2/r;

    move-result-object v4

    invoke-virtual {v2, v4}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v2

    const-class v4, Lv3/e;

    invoke-static {v4}, Ly2/r;->i(Ljava/lang/Class;)Ly2/r;

    move-result-object v4

    invoke-virtual {v2, v4}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v2

    const-class v4, Lcom/google/firebase/abt/component/a;

    invoke-static {v4}, Ly2/r;->i(Ljava/lang/Class;)Ly2/r;

    move-result-object v4

    invoke-virtual {v2, v4}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v2

    const-class v4, Lw2/a;

    invoke-static {v4}, Ly2/r;->h(Ljava/lang/Class;)Ly2/r;

    move-result-object v4

    invoke-virtual {v2, v4}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v2

    new-instance v4, Lc4/v;

    invoke-direct {v4, v0}, Lc4/v;-><init>(Ly2/f0;)V

    invoke-virtual {v2, v4}, Ly2/c$b;->e(Ly2/h;)Ly2/c$b;

    move-result-object v0

    invoke-virtual {v0}, Ly2/c$b;->d()Ly2/c$b;

    move-result-object v0

    invoke-virtual {v0}, Ly2/c$b;->c()Ly2/c;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v0, "21.4.0"

    invoke-static {v3, v0}, Lb4/h;->b(Ljava/lang/String;Ljava/lang/String;)Ly2/c;

    move-result-object v0

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
