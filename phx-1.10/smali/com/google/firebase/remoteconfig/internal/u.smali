.class public Lcom/google/firebase/remoteconfig/internal/u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lc4/p;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/remoteconfig/internal/u$b;
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:I

.field private final c:Lc4/r;


# direct methods
.method private constructor <init>(JILc4/r;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/firebase/remoteconfig/internal/u;->a:J

    iput p3, p0, Lcom/google/firebase/remoteconfig/internal/u;->b:I

    iput-object p4, p0, Lcom/google/firebase/remoteconfig/internal/u;->c:Lc4/r;

    return-void
.end method

.method synthetic constructor <init>(JILc4/r;Lcom/google/firebase/remoteconfig/internal/u$a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/firebase/remoteconfig/internal/u;-><init>(JILc4/r;)V

    return-void
.end method

.method static d()Lcom/google/firebase/remoteconfig/internal/u$b;
    .locals 2

    new-instance v0, Lcom/google/firebase/remoteconfig/internal/u$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/firebase/remoteconfig/internal/u$b;-><init>(Lcom/google/firebase/remoteconfig/internal/u$a;)V

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/google/firebase/remoteconfig/internal/u;->a:J

    return-wide v0
.end method

.method public b()Lc4/r;
    .locals 1

    iget-object v0, p0, Lcom/google/firebase/remoteconfig/internal/u;->c:Lc4/r;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/firebase/remoteconfig/internal/u;->b:I

    return v0
.end method
