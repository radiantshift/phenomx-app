.class public Lcom/google/firebase/crashlytics/CrashlyticsRegistrar;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/google/firebase/components/ComponentRegistrar;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/firebase/crashlytics/CrashlyticsRegistrar;Ly2/e;)Lcom/google/firebase/crashlytics/a;
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/firebase/crashlytics/CrashlyticsRegistrar;->b(Ly2/e;)Lcom/google/firebase/crashlytics/a;

    move-result-object p0

    return-object p0
.end method

.method private b(Ly2/e;)Lcom/google/firebase/crashlytics/a;
    .locals 4

    const-class v0, Lt2/e;

    invoke-interface {p1, v0}, Ly2/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt2/e;

    const-class v1, Lb3/a;

    invoke-interface {p1, v1}, Ly2/e;->i(Ljava/lang/Class;)Lu3/a;

    move-result-object v1

    const-class v2, Lw2/a;

    invoke-interface {p1, v2}, Ly2/e;->i(Ljava/lang/Class;)Lu3/a;

    move-result-object v2

    const-class v3, Lv3/e;

    invoke-interface {p1, v3}, Ly2/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lv3/e;

    invoke-static {v0, p1, v1, v2}, Lcom/google/firebase/crashlytics/a;->e(Lt2/e;Lv3/e;Lu3/a;Lu3/a;)Lcom/google/firebase/crashlytics/a;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ly2/c<",
            "*>;>;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Ly2/c;

    const-class v1, Lcom/google/firebase/crashlytics/a;

    invoke-static {v1}, Ly2/c;->c(Ljava/lang/Class;)Ly2/c$b;

    move-result-object v1

    const-string v2, "fire-cls"

    invoke-virtual {v1, v2}, Ly2/c$b;->g(Ljava/lang/String;)Ly2/c$b;

    move-result-object v1

    const-class v3, Lt2/e;

    invoke-static {v3}, Ly2/r;->i(Ljava/lang/Class;)Ly2/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v1

    const-class v3, Lv3/e;

    invoke-static {v3}, Ly2/r;->i(Ljava/lang/Class;)Ly2/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v1

    const-class v3, Lb3/a;

    invoke-static {v3}, Ly2/r;->a(Ljava/lang/Class;)Ly2/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v1

    const-class v3, Lw2/a;

    invoke-static {v3}, Ly2/r;->a(Ljava/lang/Class;)Ly2/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v1

    new-instance v3, La3/f;

    invoke-direct {v3, p0}, La3/f;-><init>(Lcom/google/firebase/crashlytics/CrashlyticsRegistrar;)V

    invoke-virtual {v1, v3}, Ly2/c$b;->e(Ly2/h;)Ly2/c$b;

    move-result-object v1

    invoke-virtual {v1}, Ly2/c$b;->d()Ly2/c$b;

    move-result-object v1

    invoke-virtual {v1}, Ly2/c$b;->c()Ly2/c;

    move-result-object v1

    const/4 v3, 0x0

    aput-object v1, v0, v3

    const-string v1, "18.3.7"

    invoke-static {v2, v1}, Lb4/h;->b(Ljava/lang/String;Ljava/lang/String;)Ly2/c;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
