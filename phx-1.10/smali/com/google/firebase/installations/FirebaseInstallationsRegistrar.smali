.class public Lcom/google/firebase/installations/FirebaseInstallationsRegistrar;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/google/firebase/components/ComponentRegistrar;


# static fields
.field private static final LIBRARY_NAME:Ljava/lang/String; = "fire-installations"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Ly2/e;)Lv3/e;
    .locals 0

    invoke-static {p0}, Lcom/google/firebase/installations/FirebaseInstallationsRegistrar;->lambda$getComponents$0(Ly2/e;)Lv3/e;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic lambda$getComponents$0(Ly2/e;)Lv3/e;
    .locals 6

    new-instance v0, Lcom/google/firebase/installations/c;

    const-class v1, Lt2/e;

    invoke-interface {p0, v1}, Ly2/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lt2/e;

    const-class v2, Lt3/i;

    invoke-interface {p0, v2}, Ly2/e;->f(Ljava/lang/Class;)Lu3/b;

    move-result-object v2

    const-class v3, Lx2/a;

    const-class v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4}, Ly2/f0;->a(Ljava/lang/Class;Ljava/lang/Class;)Ly2/f0;

    move-result-object v3

    invoke-interface {p0, v3}, Ly2/e;->g(Ly2/f0;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    const-class v4, Lx2/b;

    const-class v5, Ljava/util/concurrent/Executor;

    invoke-static {v4, v5}, Ly2/f0;->a(Ljava/lang/Class;Ljava/lang/Class;)Ly2/f0;

    move-result-object v4

    invoke-interface {p0, v4}, Ly2/e;->g(Ly2/f0;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    invoke-static {p0}, Lz2/i;->b(Ljava/util/concurrent/Executor;)Ljava/util/concurrent/Executor;

    move-result-object p0

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/firebase/installations/c;-><init>(Lt2/e;Lu3/b;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ly2/c<",
            "*>;>;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Ly2/c;

    const-class v1, Lv3/e;

    invoke-static {v1}, Ly2/c;->c(Ljava/lang/Class;)Ly2/c$b;

    move-result-object v1

    const-string v2, "fire-installations"

    invoke-virtual {v1, v2}, Ly2/c$b;->g(Ljava/lang/String;)Ly2/c$b;

    move-result-object v1

    const-class v3, Lt2/e;

    invoke-static {v3}, Ly2/r;->i(Ljava/lang/Class;)Ly2/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v1

    const-class v3, Lt3/i;

    invoke-static {v3}, Ly2/r;->h(Ljava/lang/Class;)Ly2/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v1

    const-class v3, Lx2/a;

    const-class v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4}, Ly2/f0;->a(Ljava/lang/Class;Ljava/lang/Class;)Ly2/f0;

    move-result-object v3

    invoke-static {v3}, Ly2/r;->j(Ly2/f0;)Ly2/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v1

    const-class v3, Lx2/b;

    const-class v4, Ljava/util/concurrent/Executor;

    invoke-static {v3, v4}, Ly2/f0;->a(Ljava/lang/Class;Ljava/lang/Class;)Ly2/f0;

    move-result-object v3

    invoke-static {v3}, Ly2/r;->j(Ly2/f0;)Ly2/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Ly2/c$b;->b(Ly2/r;)Ly2/c$b;

    move-result-object v1

    sget-object v3, Lv3/f;->a:Lv3/f;

    invoke-virtual {v1, v3}, Ly2/c$b;->e(Ly2/h;)Ly2/c$b;

    move-result-object v1

    invoke-virtual {v1}, Ly2/c$b;->c()Ly2/c;

    move-result-object v1

    const/4 v3, 0x0

    aput-object v1, v0, v3

    invoke-static {}, Lt3/h;->a()Ly2/c;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    const-string v1, "17.1.3"

    invoke-static {v2, v1}, Lb4/h;->b(Ljava/lang/String;Ljava/lang/String;)Ly2/c;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
