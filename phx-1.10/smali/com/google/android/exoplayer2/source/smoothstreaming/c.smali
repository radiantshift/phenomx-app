.class final Lcom/google/android/exoplayer2/source/smoothstreaming/c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lj1/u;
.implements Lj1/r0$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lj1/u;",
        "Lj1/r0$a<",
        "Ll1/i<",
        "Lcom/google/android/exoplayer2/source/smoothstreaming/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final f:Lcom/google/android/exoplayer2/source/smoothstreaming/b$a;

.field private final g:Ld2/p0;

.field private final h:Ld2/i0;

.field private final i:Ll0/y;

.field private final j:Ll0/w$a;

.field private final k:Ld2/g0;

.field private final l:Lj1/e0$a;

.field private final m:Ld2/b;

.field private final n:Lj1/z0;

.field private final o:Lj1/i;

.field private p:Lj1/u$a;

.field private q:Lr1/a;

.field private r:[Ll1/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ll1/i<",
            "Lcom/google/android/exoplayer2/source/smoothstreaming/b;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lj1/r0;


# direct methods
.method public constructor <init>(Lr1/a;Lcom/google/android/exoplayer2/source/smoothstreaming/b$a;Ld2/p0;Lj1/i;Ll0/y;Ll0/w$a;Ld2/g0;Lj1/e0$a;Ld2/i0;Ld2/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->q:Lr1/a;

    iput-object p2, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->f:Lcom/google/android/exoplayer2/source/smoothstreaming/b$a;

    iput-object p3, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->g:Ld2/p0;

    iput-object p9, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->h:Ld2/i0;

    iput-object p5, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->i:Ll0/y;

    iput-object p6, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->j:Ll0/w$a;

    iput-object p7, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->k:Ld2/g0;

    iput-object p8, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->l:Lj1/e0$a;

    iput-object p10, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->m:Ld2/b;

    iput-object p4, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->o:Lj1/i;

    invoke-static {p1, p5}, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->l(Lr1/a;Ll0/y;)Lj1/z0;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->n:Lj1/z0;

    const/4 p1, 0x0

    invoke-static {p1}, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->m(I)[Ll1/i;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->r:[Ll1/i;

    invoke-interface {p4, p1}, Lj1/i;->a([Lj1/r0;)Lj1/r0;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->s:Lj1/r0;

    return-void
.end method

.method private k(Lc2/t;J)Ll1/i;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc2/t;",
            "J)",
            "Ll1/i<",
            "Lcom/google/android/exoplayer2/source/smoothstreaming/b;",
            ">;"
        }
    .end annotation

    move-object v13, p0

    iget-object v0, v13, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->n:Lj1/z0;

    invoke-interface/range {p1 .. p1}, Lc2/w;->c()Lj1/x0;

    move-result-object v1

    invoke-virtual {v0, v1}, Lj1/z0;->c(Lj1/x0;)I

    move-result v0

    iget-object v2, v13, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->f:Lcom/google/android/exoplayer2/source/smoothstreaming/b$a;

    iget-object v3, v13, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->h:Ld2/i0;

    iget-object v4, v13, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->q:Lr1/a;

    iget-object v7, v13, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->g:Ld2/p0;

    move v5, v0

    move-object/from16 v6, p1

    invoke-interface/range {v2 .. v7}, Lcom/google/android/exoplayer2/source/smoothstreaming/b$a;->a(Ld2/i0;Lr1/a;ILc2/t;Ld2/p0;)Lcom/google/android/exoplayer2/source/smoothstreaming/b;

    move-result-object v4

    new-instance v14, Ll1/i;

    iget-object v1, v13, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->q:Lr1/a;

    iget-object v1, v1, Lr1/a;->f:[Lr1/a$b;

    aget-object v0, v1, v0

    iget v1, v0, Lr1/a$b;->a:I

    iget-object v6, v13, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->m:Ld2/b;

    iget-object v9, v13, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->i:Ll0/y;

    iget-object v10, v13, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->j:Ll0/w$a;

    iget-object v11, v13, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->k:Ld2/g0;

    iget-object v12, v13, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->l:Lj1/e0$a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, v14

    move-object v5, p0

    move-wide/from16 v7, p2

    invoke-direct/range {v0 .. v12}, Ll1/i;-><init>(I[I[Lh0/r1;Ll1/j;Lj1/r0$a;Ld2/b;JLl0/y;Ll0/w$a;Ld2/g0;Lj1/e0$a;)V

    return-object v14
.end method

.method private static l(Lr1/a;Ll0/y;)Lj1/z0;
    .locals 8

    iget-object v0, p0, Lr1/a;->f:[Lr1/a$b;

    array-length v0, v0

    new-array v0, v0, [Lj1/x0;

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lr1/a;->f:[Lr1/a$b;

    array-length v4, v3

    if-ge v2, v4, :cond_1

    aget-object v3, v3, v2

    iget-object v3, v3, Lr1/a$b;->j:[Lh0/r1;

    array-length v4, v3

    new-array v4, v4, [Lh0/r1;

    const/4 v5, 0x0

    :goto_1
    array-length v6, v3

    if-ge v5, v6, :cond_0

    aget-object v6, v3, v5

    invoke-interface {p1, v6}, Ll0/y;->d(Lh0/r1;)I

    move-result v7

    invoke-virtual {v6, v7}, Lh0/r1;->c(I)Lh0/r1;

    move-result-object v6

    aput-object v6, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_0
    new-instance v3, Lj1/x0;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5, v4}, Lj1/x0;-><init>(Ljava/lang/String;[Lh0/r1;)V

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance p0, Lj1/z0;

    invoke-direct {p0, v0}, Lj1/z0;-><init>([Lj1/x0;)V

    return-object p0
.end method

.method private static m(I)[Ll1/i;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[",
            "Ll1/i<",
            "Lcom/google/android/exoplayer2/source/smoothstreaming/b;",
            ">;"
        }
    .end annotation

    new-array p0, p0, [Ll1/i;

    return-object p0
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->s:Lj1/r0;

    invoke-interface {v0}, Lj1/r0;->a()Z

    move-result v0

    return v0
.end method

.method public c(JLh0/u3;)J
    .locals 6

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->r:[Ll1/i;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    iget v4, v3, Ll1/i;->f:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    invoke-virtual {v3, p1, p2, p3}, Ll1/i;->c(JLh0/u3;)J

    move-result-wide p1

    return-wide p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-wide p1
.end method

.method public d()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->s:Lj1/r0;

    invoke-interface {v0}, Lj1/r0;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public f()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->s:Lj1/r0;

    invoke-interface {v0}, Lj1/r0;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public g(J)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->s:Lj1/r0;

    invoke-interface {v0, p1, p2}, Lj1/r0;->g(J)Z

    move-result p1

    return p1
.end method

.method public h(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->s:Lj1/r0;

    invoke-interface {v0, p1, p2}, Lj1/r0;->h(J)V

    return-void
.end method

.method public bridge synthetic i(Lj1/r0;)V
    .locals 0

    check-cast p1, Ll1/i;

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->t(Ll1/i;)V

    return-void
.end method

.method public n()J
    .locals 2

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    return-wide v0
.end method

.method public o()Lj1/z0;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->n:Lj1/z0;

    return-object v0
.end method

.method public p(Lj1/u$a;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->p:Lj1/u$a;

    invoke-interface {p1, p0}, Lj1/u$a;->e(Lj1/u;)V

    return-void
.end method

.method public q()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->h:Ld2/i0;

    invoke-interface {v0}, Ld2/i0;->b()V

    return-void
.end method

.method public r(JZ)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->r:[Ll1/i;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3, p1, p2, p3}, Ll1/i;->r(JZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public s(J)J
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->r:[Ll1/i;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3, p1, p2}, Ll1/i;->S(J)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-wide p1
.end method

.method public t(Ll1/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll1/i<",
            "Lcom/google/android/exoplayer2/source/smoothstreaming/b;",
            ">;)V"
        }
    .end annotation

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->p:Lj1/u$a;

    invoke-interface {p1, p0}, Lj1/r0$a;->i(Lj1/r0;)V

    return-void
.end method

.method public u([Lc2/t;[Z[Lj1/q0;[ZJ)J
    .locals 5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_4

    aget-object v2, p3, v1

    if-eqz v2, :cond_2

    aget-object v2, p3, v1

    check-cast v2, Ll1/i;

    aget-object v3, p1, v1

    if-eqz v3, :cond_1

    aget-boolean v3, p2, v1

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Ll1/i;->E()Ll1/j;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer2/source/smoothstreaming/b;

    aget-object v4, p1, v1

    invoke-interface {v3, v4}, Lcom/google/android/exoplayer2/source/smoothstreaming/b;->d(Lc2/t;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    :goto_1
    invoke-virtual {v2}, Ll1/i;->P()V

    const/4 v2, 0x0

    aput-object v2, p3, v1

    :cond_2
    :goto_2
    aget-object v2, p3, v1

    if-nez v2, :cond_3

    aget-object v2, p1, v1

    if-eqz v2, :cond_3

    aget-object v2, p1, v1

    invoke-direct {p0, v2, p5, p6}, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->k(Lc2/t;J)Ll1/i;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    aput-object v2, p3, v1

    const/4 v2, 0x1

    aput-boolean v2, p4, v1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    invoke-static {p1}, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->m(I)[Ll1/i;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->r:[Ll1/i;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    iget-object p1, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->o:Lj1/i;

    iget-object p2, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->r:[Ll1/i;

    invoke-interface {p1, p2}, Lj1/i;->a([Lj1/r0;)Lj1/r0;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->s:Lj1/r0;

    return-wide p5
.end method

.method public v()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->r:[Ll1/i;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3}, Ll1/i;->P()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->p:Lj1/u$a;

    return-void
.end method

.method public w(Lr1/a;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->q:Lr1/a;

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->r:[Ll1/i;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3}, Ll1/i;->E()Ll1/j;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer2/source/smoothstreaming/b;

    invoke-interface {v3, p1}, Lcom/google/android/exoplayer2/source/smoothstreaming/b;->h(Lr1/a;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/source/smoothstreaming/c;->p:Lj1/u$a;

    invoke-interface {p1, p0}, Lj1/r0$a;->i(Lj1/r0;)V

    return-void
.end method
