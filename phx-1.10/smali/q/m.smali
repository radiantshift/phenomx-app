.class public final Lq/m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lq4/a;
.implements Lr4/a;


# instance fields
.field private final f:Lq/n;

.field private g:Lz4/k;

.field private h:Lz4/o;

.field private i:Lr4/c;

.field private j:Lq/l;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lq/n;

    invoke-direct {v0}, Lq/n;-><init>()V

    iput-object v0, p0, Lq/m;->f:Lq/n;

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lq/m;->i:Lr4/c;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lq/m;->f:Lq/n;

    invoke-interface {v0, v1}, Lr4/c;->e(Lz4/m;)V

    iget-object v0, p0, Lq/m;->i:Lr4/c;

    iget-object v1, p0, Lq/m;->f:Lq/n;

    invoke-interface {v0, v1}, Lr4/c;->f(Lz4/p;)V

    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lq/m;->h:Lz4/o;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lq/m;->f:Lq/n;

    invoke-interface {v0, v1}, Lz4/o;->c(Lz4/m;)Lz4/o;

    iget-object v0, p0, Lq/m;->h:Lz4/o;

    iget-object v1, p0, Lq/m;->f:Lq/n;

    invoke-interface {v0, v1}, Lz4/o;->b(Lz4/p;)Lz4/o;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lq/m;->i:Lr4/c;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lq/m;->f:Lq/n;

    invoke-interface {v0, v1}, Lr4/c;->c(Lz4/m;)V

    iget-object v0, p0, Lq/m;->i:Lr4/c;

    iget-object v1, p0, Lq/m;->f:Lq/n;

    invoke-interface {v0, v1}, Lr4/c;->b(Lz4/p;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private f(Landroid/content/Context;Lz4/c;)V
    .locals 3

    new-instance v0, Lz4/k;

    const-string v1, "flutter.baseflow.com/permissions/methods"

    invoke-direct {v0, p2, v1}, Lz4/k;-><init>(Lz4/c;Ljava/lang/String;)V

    iput-object v0, p0, Lq/m;->g:Lz4/k;

    new-instance p2, Lq/l;

    new-instance v0, Lq/a;

    invoke-direct {v0}, Lq/a;-><init>()V

    iget-object v1, p0, Lq/m;->f:Lq/n;

    new-instance v2, Lq/p;

    invoke-direct {v2}, Lq/p;-><init>()V

    invoke-direct {p2, p1, v0, v1, v2}, Lq/l;-><init>(Landroid/content/Context;Lq/a;Lq/n;Lq/p;)V

    iput-object p2, p0, Lq/m;->j:Lq/l;

    iget-object p1, p0, Lq/m;->g:Lz4/k;

    invoke-virtual {p1, p2}, Lz4/k;->e(Lz4/k$c;)V

    return-void
.end method

.method private i(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lq/m;->j:Lq/l;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lq/l;->i(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method private j()V
    .locals 2

    iget-object v0, p0, Lq/m;->g:Lz4/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lz4/k;->e(Lz4/k$c;)V

    iput-object v1, p0, Lq/m;->g:Lz4/k;

    iput-object v1, p0, Lq/m;->j:Lq/l;

    return-void
.end method

.method private l()V
    .locals 2

    iget-object v0, p0, Lq/m;->j:Lq/l;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lq/l;->i(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public b()V
    .locals 0

    invoke-direct {p0}, Lq/m;->l()V

    invoke-direct {p0}, Lq/m;->a()V

    return-void
.end method

.method public d(Lq4/a$b;)V
    .locals 0

    invoke-direct {p0}, Lq/m;->j()V

    return-void
.end method

.method public e(Lr4/c;)V
    .locals 1

    invoke-interface {p1}, Lr4/c;->d()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lq/m;->i(Landroid/app/Activity;)V

    iput-object p1, p0, Lq/m;->i:Lr4/c;

    invoke-direct {p0}, Lq/m;->c()V

    return-void
.end method

.method public g(Lr4/c;)V
    .locals 0

    invoke-virtual {p0, p1}, Lq/m;->e(Lr4/c;)V

    return-void
.end method

.method public h()V
    .locals 0

    invoke-virtual {p0}, Lq/m;->b()V

    return-void
.end method

.method public k(Lq4/a$b;)V
    .locals 1

    invoke-virtual {p1}, Lq4/a$b;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lq4/a$b;->b()Lz4/c;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lq/m;->f(Landroid/content/Context;Lz4/c;)V

    return-void
.end method
