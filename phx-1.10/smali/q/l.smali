.class final Lq/l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz4/k$c;


# instance fields
.field private final f:Landroid/content/Context;

.field private final g:Lq/a;

.field private final h:Lq/n;

.field private final i:Lq/p;

.field private j:Landroid/app/Activity;


# direct methods
.method constructor <init>(Landroid/content/Context;Lq/a;Lq/n;Lq/p;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lq/l;->f:Landroid/content/Context;

    iput-object p2, p0, Lq/l;->g:Lq/a;

    iput-object p3, p0, Lq/l;->h:Lq/n;

    iput-object p4, p0, Lq/l;->i:Lq/p;

    return-void
.end method

.method public static synthetic a(Lz4/k$d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lq/l;->f(Lz4/k$d;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic b(Lz4/k$d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lq/l;->e(Lz4/k$d;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic c(Lz4/k$d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lq/l;->h(Lz4/k$d;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic d(Lz4/k$d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lq/l;->g(Lz4/k$d;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static synthetic e(Lz4/k$d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p0, p1, p2, v0}, Lz4/k$d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private static synthetic f(Lz4/k$d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p0, p1, p2, v0}, Lz4/k$d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private static synthetic g(Lz4/k$d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p0, p1, p2, v0}, Lz4/k$d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private static synthetic h(Lz4/k$d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p0, p1, p2, v0}, Lz4/k$d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public C(Lz4/j;Lz4/k$d;)V
    .locals 4

    iget-object v0, p1, Lz4/j;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, -0x1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v1, "requestPermissions"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x4

    goto :goto_0

    :sswitch_1
    const-string v1, "openAppSettings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x3

    goto :goto_0

    :sswitch_2
    const-string v1, "checkPermissionStatus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v2, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "shouldShowRequestPermissionRationale"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    :cond_3
    const/4 v2, 0x1

    goto :goto_0

    :sswitch_4
    const-string v1, "checkServiceStatus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    :goto_0
    packed-switch v2, :pswitch_data_0

    invoke-interface {p2}, Lz4/k$d;->c()V

    goto/16 :goto_1

    :pswitch_0
    invoke-virtual {p1}, Lz4/j;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    iget-object v0, p0, Lq/l;->h:Lq/n;

    iget-object v1, p0, Lq/l;->j:Landroid/app/Activity;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lq/i;

    invoke-direct {v2, p2}, Lq/i;-><init>(Lz4/k$d;)V

    new-instance v3, Lq/d;

    invoke-direct {v3, p2}, Lq/d;-><init>(Lz4/k$d;)V

    invoke-virtual {v0, p1, v1, v2, v3}, Lq/n;->i(Ljava/util/List;Landroid/app/Activity;Lq/n$b;Lq/b;)V

    goto :goto_1

    :pswitch_1
    iget-object p1, p0, Lq/l;->g:Lq/a;

    iget-object v0, p0, Lq/l;->f:Landroid/content/Context;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lq/c;

    invoke-direct {v1, p2}, Lq/c;-><init>(Lz4/k$d;)V

    new-instance v2, Lq/f;

    invoke-direct {v2, p2}, Lq/f;-><init>(Lz4/k$d;)V

    invoke-virtual {p1, v0, v1, v2}, Lq/a;->a(Landroid/content/Context;Lq/a$a;Lq/b;)V

    goto :goto_1

    :pswitch_2
    iget-object p1, p1, Lz4/j;->b:Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    iget-object v0, p0, Lq/l;->h:Lq/n;

    iget-object v1, p0, Lq/l;->f:Landroid/content/Context;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lq/h;

    invoke-direct {v2, p2}, Lq/h;-><init>(Lz4/k$d;)V

    invoke-virtual {v0, p1, v1, v2}, Lq/n;->e(ILandroid/content/Context;Lq/n$a;)V

    goto :goto_1

    :pswitch_3
    iget-object p1, p1, Lz4/j;->b:Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    iget-object v0, p0, Lq/l;->h:Lq/n;

    iget-object v1, p0, Lq/l;->j:Landroid/app/Activity;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lq/j;

    invoke-direct {v2, p2}, Lq/j;-><init>(Lz4/k$d;)V

    new-instance v3, Lq/g;

    invoke-direct {v3, p2}, Lq/g;-><init>(Lz4/k$d;)V

    invoke-virtual {v0, p1, v1, v2, v3}, Lq/n;->j(ILandroid/app/Activity;Lq/n$c;Lq/b;)V

    goto :goto_1

    :pswitch_4
    iget-object p1, p1, Lz4/j;->b:Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    iget-object v0, p0, Lq/l;->i:Lq/p;

    iget-object v1, p0, Lq/l;->f:Landroid/content/Context;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lq/k;

    invoke-direct {v2, p2}, Lq/k;-><init>(Lz4/k$d;)V

    new-instance v3, Lq/e;

    invoke-direct {v3, p2}, Lq/e;-><init>(Lz4/k$d;)V

    invoke-virtual {v0, p1, v1, v2, v3}, Lq/p;->a(ILandroid/content/Context;Lq/p$a;Lq/b;)V

    :goto_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x5c086121 -> :sswitch_4
        -0x3ca2ffb7 -> :sswitch_3
        -0x22583c37 -> :sswitch_2
        0x14b278ba -> :sswitch_1
        0x637dca75 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public i(Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lq/l;->j:Landroid/app/Activity;

    return-void
.end method
