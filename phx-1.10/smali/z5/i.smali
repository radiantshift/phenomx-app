.class public final Lz5/i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz5/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lz5/b<",
        "TR;>;"
    }
.end annotation


# instance fields
.field private final a:Lz5/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lz5/b<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Lu5/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lu5/l<",
            "TT;TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lz5/b;Lu5/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lz5/b<",
            "+TT;>;",
            "Lu5/l<",
            "-TT;+TR;>;)V"
        }
    .end annotation

    const-string v0, "sequence"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transformer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lz5/i;->a:Lz5/b;

    iput-object p2, p0, Lz5/i;->b:Lu5/l;

    return-void
.end method

.method public static final synthetic a(Lz5/i;)Lz5/b;
    .locals 0

    iget-object p0, p0, Lz5/i;->a:Lz5/b;

    return-object p0
.end method

.method public static final synthetic b(Lz5/i;)Lu5/l;
    .locals 0

    iget-object p0, p0, Lz5/i;->b:Lu5/l;

    return-object p0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TR;>;"
        }
    .end annotation

    new-instance v0, Lz5/i$a;

    invoke-direct {v0, p0}, Lz5/i$a;-><init>(Lz5/i;)V

    return-object v0
.end method
