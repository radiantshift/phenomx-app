.class public abstract Lk0/a;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(I)V
    .locals 1

    iget v0, p0, Lk0/a;->f:I

    or-int/2addr p1, v0

    iput p1, p0, Lk0/a;->f:I

    return-void
.end method

.method public f()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lk0/a;->f:I

    return-void
.end method

.method public final g(I)V
    .locals 1

    iget v0, p0, Lk0/a;->f:I

    not-int p1, p1

    and-int/2addr p1, v0

    iput p1, p0, Lk0/a;->f:I

    return-void
.end method

.method protected final h(I)Z
    .locals 1

    iget v0, p0, Lk0/a;->f:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final i()Z
    .locals 1

    const/high16 v0, 0x10000000

    invoke-virtual {p0, v0}, Lk0/a;->h(I)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    const/high16 v0, -0x80000000

    invoke-virtual {p0, v0}, Lk0/a;->h(I)Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lk0/a;->h(I)Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    const/high16 v0, 0x8000000

    invoke-virtual {p0, v0}, Lk0/a;->h(I)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lk0/a;->h(I)Z

    move-result v0

    return v0
.end method

.method public final n(I)V
    .locals 0

    iput p1, p0, Lk0/a;->f:I

    return-void
.end method
