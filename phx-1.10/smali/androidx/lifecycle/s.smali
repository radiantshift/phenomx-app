.class public final Landroidx/lifecycle/s;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lj/a$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj/a$b<",
            "Lk/d;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lj/a$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj/a$b<",
            "Landroidx/lifecycle/a0;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lj/a$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj/a$b<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroidx/lifecycle/s$b;

    invoke-direct {v0}, Landroidx/lifecycle/s$b;-><init>()V

    sput-object v0, Landroidx/lifecycle/s;->a:Lj/a$b;

    new-instance v0, Landroidx/lifecycle/s$c;

    invoke-direct {v0}, Landroidx/lifecycle/s$c;-><init>()V

    sput-object v0, Landroidx/lifecycle/s;->b:Lj/a$b;

    new-instance v0, Landroidx/lifecycle/s$a;

    invoke-direct {v0}, Landroidx/lifecycle/s$a;-><init>()V

    sput-object v0, Landroidx/lifecycle/s;->c:Lj/a$b;

    return-void
.end method

.method public static final a(Lk/d;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lk/d;",
            ":",
            "Landroidx/lifecycle/a0;",
            ">(TT;)V"
        }
    .end annotation

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0}, Landroidx/lifecycle/h;->a()Landroidx/lifecycle/d;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/lifecycle/d;->b()Landroidx/lifecycle/d$b;

    move-result-object v0

    sget-object v1, Landroidx/lifecycle/d$b;->g:Landroidx/lifecycle/d$b;

    if-eq v0, v1, :cond_1

    sget-object v1, Landroidx/lifecycle/d$b;->h:Landroidx/lifecycle/d$b;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    invoke-interface {p0}, Lk/d;->j()Landroidx/savedstate/a;

    move-result-object v0

    const-string v1, "androidx.lifecycle.internal.SavedStateHandlesProvider"

    invoke-virtual {v0, v1}, Landroidx/savedstate/a;->c(Ljava/lang/String;)Landroidx/savedstate/a$c;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Landroidx/lifecycle/t;

    invoke-interface {p0}, Lk/d;->j()Landroidx/savedstate/a;

    move-result-object v2

    move-object v3, p0

    check-cast v3, Landroidx/lifecycle/a0;

    invoke-direct {v0, v2, v3}, Landroidx/lifecycle/t;-><init>(Landroidx/savedstate/a;Landroidx/lifecycle/a0;)V

    invoke-interface {p0}, Lk/d;->j()Landroidx/savedstate/a;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroidx/savedstate/a;->h(Ljava/lang/String;Landroidx/savedstate/a$c;)V

    invoke-interface {p0}, Landroidx/lifecycle/h;->a()Landroidx/lifecycle/d;

    move-result-object p0

    new-instance v1, Landroidx/lifecycle/SavedStateHandleAttacher;

    invoke-direct {v1, v0}, Landroidx/lifecycle/SavedStateHandleAttacher;-><init>(Landroidx/lifecycle/t;)V

    invoke-virtual {p0, v1}, Landroidx/lifecycle/d;->a(Landroidx/lifecycle/g;)V

    :cond_2
    return-void

    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final b(Landroidx/lifecycle/a0;)Landroidx/lifecycle/u;
    .locals 4

    const-class v0, Landroidx/lifecycle/u;

    const-string v1, "<this>"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lj/c;

    invoke-direct {v1}, Lj/c;-><init>()V

    sget-object v2, Landroidx/lifecycle/s$d;->f:Landroidx/lifecycle/s$d;

    invoke-static {v0}, Lkotlin/jvm/internal/p;->b(Ljava/lang/Class;)Ly5/c;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Lj/c;->a(Ly5/c;Lu5/l;)V

    invoke-virtual {v1}, Lj/c;->b()Landroidx/lifecycle/w$b;

    move-result-object v1

    new-instance v2, Landroidx/lifecycle/w;

    invoke-direct {v2, p0, v1}, Landroidx/lifecycle/w;-><init>(Landroidx/lifecycle/a0;Landroidx/lifecycle/w$b;)V

    const-string p0, "androidx.lifecycle.internal.SavedStateHandlesVM"

    invoke-virtual {v2, p0, v0}, Landroidx/lifecycle/w;->a(Ljava/lang/String;Ljava/lang/Class;)Landroidx/lifecycle/v;

    move-result-object p0

    check-cast p0, Landroidx/lifecycle/u;

    return-object p0
.end method
