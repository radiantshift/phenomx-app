.class public Landroidx/lifecycle/w;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/lifecycle/w$a;,
        Landroidx/lifecycle/w$b;,
        Landroidx/lifecycle/w$c;,
        Landroidx/lifecycle/w$d;
    }
.end annotation


# instance fields
.field private final a:Landroidx/lifecycle/z;

.field private final b:Landroidx/lifecycle/w$b;

.field private final c:Lj/a;


# direct methods
.method public constructor <init>(Landroidx/lifecycle/a0;Landroidx/lifecycle/w$b;)V
    .locals 1

    const-string v0, "owner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "factory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Landroidx/lifecycle/a0;->d()Landroidx/lifecycle/z;

    move-result-object v0

    invoke-static {p1}, Landroidx/lifecycle/y;->a(Landroidx/lifecycle/a0;)Lj/a;

    move-result-object p1

    invoke-direct {p0, v0, p2, p1}, Landroidx/lifecycle/w;-><init>(Landroidx/lifecycle/z;Landroidx/lifecycle/w$b;Lj/a;)V

    return-void
.end method

.method public constructor <init>(Landroidx/lifecycle/z;Landroidx/lifecycle/w$b;Lj/a;)V
    .locals 1

    const-string v0, "store"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "factory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultCreationExtras"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroidx/lifecycle/w;->a:Landroidx/lifecycle/z;

    iput-object p2, p0, Landroidx/lifecycle/w;->b:Landroidx/lifecycle/w$b;

    iput-object p3, p0, Landroidx/lifecycle/w;->c:Lj/a;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/Class;)Landroidx/lifecycle/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/v;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modelClass"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Landroidx/lifecycle/w;->a:Landroidx/lifecycle/z;

    invoke-virtual {v0, p1}, Landroidx/lifecycle/z;->b(Ljava/lang/String;)Landroidx/lifecycle/v;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object p1, p0, Landroidx/lifecycle/w;->b:Landroidx/lifecycle/w$b;

    instance-of p2, p1, Landroidx/lifecycle/w$d;

    if-eqz p2, :cond_0

    check-cast p1, Landroidx/lifecycle/w$d;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/i;->b(Ljava/lang/Object;)V

    invoke-virtual {p1, v0}, Landroidx/lifecycle/w$d;->a(Landroidx/lifecycle/v;)V

    :cond_1
    const-string p1, "null cannot be cast to non-null type T of androidx.lifecycle.ViewModelProvider.get"

    invoke-static {v0, p1}, Lkotlin/jvm/internal/i;->c(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_2
    new-instance v0, Lj/d;

    iget-object v1, p0, Landroidx/lifecycle/w;->c:Lj/a;

    invoke-direct {v0, v1}, Lj/d;-><init>(Lj/a;)V

    sget-object v1, Landroidx/lifecycle/w$c;->c:Lj/a$b;

    invoke-virtual {v0, v1, p1}, Lj/d;->b(Lj/a$b;Ljava/lang/Object;)V

    :try_start_0
    iget-object v1, p0, Landroidx/lifecycle/w;->b:Landroidx/lifecycle/w$b;

    invoke-interface {v1, p2, v0}, Landroidx/lifecycle/w$b;->b(Ljava/lang/Class;Lj/a;)Landroidx/lifecycle/v;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    iget-object v0, p0, Landroidx/lifecycle/w;->b:Landroidx/lifecycle/w$b;

    invoke-interface {v0, p2}, Landroidx/lifecycle/w$b;->a(Ljava/lang/Class;)Landroidx/lifecycle/v;

    move-result-object p2

    :goto_1
    iget-object v0, p0, Landroidx/lifecycle/w;->a:Landroidx/lifecycle/z;

    invoke-virtual {v0, p1, p2}, Landroidx/lifecycle/z;->d(Ljava/lang/String;Landroidx/lifecycle/v;)V

    return-object p2
.end method
