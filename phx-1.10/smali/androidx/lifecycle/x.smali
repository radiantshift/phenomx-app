.class public final synthetic Landroidx/lifecycle/x;
.super Ljava/lang/Object;
.source ""


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Landroidx/lifecycle/w$b;->a:Landroidx/lifecycle/w$b$a;

    return-void
.end method

.method public static a(Landroidx/lifecycle/w$b;Ljava/lang/Class;)Landroidx/lifecycle/v;
    .locals 0

    const-string p0, "modelClass"

    .line 1
    invoke-static {p1, p0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Factory.create(String) is unsupported.  This Factory requires `CreationExtras` to be passed into `create` method."

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
