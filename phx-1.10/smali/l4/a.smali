.class public final Ll4/a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ll4/a$b;
    }
.end annotation


# static fields
.field private static e:Ll4/a;

.field private static f:Z


# instance fields
.field private a:Lo4/d;

.field private b:Ln4/a;

.field private c:Lio/flutter/embedding/engine/FlutterJNI$c;

.field private d:Ljava/util/concurrent/ExecutorService;


# direct methods
.method private constructor <init>(Lo4/d;Ln4/a;Lio/flutter/embedding/engine/FlutterJNI$c;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ll4/a;->a:Lo4/d;

    iput-object p2, p0, Ll4/a;->b:Ln4/a;

    iput-object p3, p0, Ll4/a;->c:Lio/flutter/embedding/engine/FlutterJNI$c;

    iput-object p4, p0, Ll4/a;->d:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method synthetic constructor <init>(Lo4/d;Ln4/a;Lio/flutter/embedding/engine/FlutterJNI$c;Ljava/util/concurrent/ExecutorService;Ll4/a$a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Ll4/a;-><init>(Lo4/d;Ln4/a;Lio/flutter/embedding/engine/FlutterJNI$c;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method

.method public static e()Ll4/a;
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Ll4/a;->f:Z

    sget-object v0, Ll4/a;->e:Ll4/a;

    if-nez v0, :cond_0

    new-instance v0, Ll4/a$b;

    invoke-direct {v0}, Ll4/a$b;-><init>()V

    invoke-virtual {v0}, Ll4/a$b;->a()Ll4/a;

    move-result-object v0

    sput-object v0, Ll4/a;->e:Ll4/a;

    :cond_0
    sget-object v0, Ll4/a;->e:Ll4/a;

    return-object v0
.end method


# virtual methods
.method public a()Ln4/a;
    .locals 1

    iget-object v0, p0, Ll4/a;->b:Ln4/a;

    return-object v0
.end method

.method public b()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Ll4/a;->d:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public c()Lo4/d;
    .locals 1

    iget-object v0, p0, Ll4/a;->a:Lo4/d;

    return-object v0
.end method

.method public d()Lio/flutter/embedding/engine/FlutterJNI$c;
    .locals 1

    iget-object v0, p0, Ll4/a;->c:Lio/flutter/embedding/engine/FlutterJNI$c;

    return-object v0
.end method
