.class public Lc5/e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lio/flutter/plugins/firebase/core/FlutterFirebasePlugin;
.implements Lz4/k$c;
.implements Lq4/a;
.implements Lz4/d$d;


# instance fields
.field private f:Lz4/k;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lc4/d;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lz4/d;

.field private final i:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lc5/e;->g:Ljava/util/Map;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lc5/e;->i:Landroid/os/Handler;

    return-void
.end method

.method public static synthetic a(Ln2/j;)V
    .locals 0

    invoke-static {p0}, Lc5/e;->l(Ln2/j;)V

    return-void
.end method

.method public static synthetic b(Lc5/e;Lt2/e;Ln2/j;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lc5/e;->m(Lt2/e;Ln2/j;)V

    return-void
.end method

.method public static synthetic c(Lz4/k$d;Ln2/i;)V
    .locals 0

    invoke-static {p0, p1}, Lc5/e;->n(Lz4/k$d;Ln2/i;)V

    return-void
.end method

.method static synthetic e(Lc5/e;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lc5/e;->i:Landroid/os/Handler;

    return-object p0
.end method

.method private g(Lc4/s;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc4/s;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Lc4/s;->a()[B

    move-result-object v1

    const-string v2, "value"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lc4/s;->b()I

    move-result p1

    invoke-direct {p0, p1}, Lc5/e;->p(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "source"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method private h(Lcom/google/firebase/remoteconfig/a;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/firebase/remoteconfig/a;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1}, Lcom/google/firebase/remoteconfig/a;->n()Lc4/p;

    move-result-object v1

    invoke-interface {v1}, Lc4/p;->b()Lc4/r;

    move-result-object v1

    invoke-virtual {v1}, Lc4/r;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "fetchTimeout"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/firebase/remoteconfig/a;->n()Lc4/p;

    move-result-object v1

    invoke-interface {v1}, Lc4/p;->b()Lc4/r;

    move-result-object v1

    invoke-virtual {v1}, Lc4/r;->b()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v3, "minimumFetchInterval"

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/firebase/remoteconfig/a;->n()Lc4/p;

    move-result-object v1

    invoke-interface {v1}, Lc4/p;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v3, "lastFetchTime"

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/firebase/remoteconfig/a;->n()Lc4/p;

    move-result-object p1

    invoke-interface {p1}, Lc4/p;->c()I

    move-result p1

    invoke-direct {p0, p1}, Lc5/e;->o(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "lastFetchStatus"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sending fetchTimeout: "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "FRCPlugin"

    invoke-static {v1, p1}, Ll4/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private j(Ljava/util/Map;)Lcom/google/firebase/remoteconfig/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/firebase/remoteconfig/a;"
        }
    .end annotation

    const-string v0, "appName"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lt2/e;->o(Ljava/lang/String;)Lt2/e;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/remoteconfig/a;->o(Lt2/e;)Lcom/google/firebase/remoteconfig/a;

    move-result-object p1

    return-object p1
.end method

.method private static synthetic l(Ln2/j;)V
    .locals 1

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Ln2/j;->c(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Ln2/j;->b(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method

.method private synthetic m(Lt2/e;Ln2/j;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, Lcom/google/firebase/remoteconfig/a;->o(Lt2/e;)Lcom/google/firebase/remoteconfig/a;

    move-result-object p1

    invoke-direct {p0, p1}, Lc5/e;->h(Lcom/google/firebase/remoteconfig/a;)Ljava/util/Map;

    move-result-object v0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    const-string v0, "parameters"

    invoke-virtual {p1}, Lcom/google/firebase/remoteconfig/a;->m()Ljava/util/Map;

    move-result-object p1

    invoke-direct {p0, p1}, Lc5/e;->q(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2, v1}, Ln2/j;->c(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p2, p1}, Ln2/j;->b(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method

.method private static synthetic n(Lz4/k$d;Ln2/i;)V
    .locals 4

    invoke-virtual {p1}, Ln2/i;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ln2/i;->i()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p0, p1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    goto :goto_3

    :cond_0
    invoke-virtual {p1}, Ln2/i;->h()Ljava/lang/Exception;

    move-result-object p1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    instance-of v1, p1, Lc4/o;

    const-string v2, "message"

    const-string v3, "code"

    if-eqz v1, :cond_1

    const-string v1, "throttled"

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "frequency of requests exceeds throttled limits"

    :goto_0
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    instance-of v1, p1, Lc4/m;

    if-eqz v1, :cond_2

    const-string v1, "internal"

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "internal remote config fetch error"

    goto :goto_0

    :cond_2
    instance-of v1, p1, Lc4/q;

    if-eqz v1, :cond_3

    const-string v1, "remote-config-server-error"

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    const-string v2, "Forbidden"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "forbidden"

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    const-string v1, "unknown"

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "unknown remote config error"

    goto :goto_0

    :cond_4
    :goto_1
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_5
    const/4 p1, 0x0

    :goto_2
    const-string v1, "firebase_remote_config"

    invoke-interface {p0, v1, p1, v0}, Lz4/k$d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    :goto_3
    return-void
.end method

.method private o(I)Ljava/lang/String;
    .locals 1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_2

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const-string p1, "failure"

    return-object p1

    :cond_0
    const-string p1, "throttled"

    return-object p1

    :cond_1
    const-string p1, "noFetchYet"

    return-object p1

    :cond_2
    const-string p1, "success"

    return-object p1
.end method

.method private p(I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const-string p1, "static"

    return-object p1

    :cond_0
    const-string p1, "remote"

    return-object p1

    :cond_1
    const-string p1, "default"

    return-object p1
.end method

.method private q(Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lc4/s;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lc4/s;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v3, Lc4/s;

    invoke-direct {p0, v3}, Lc5/e;->g(Lc4/s;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private r(Lz4/c;)V
    .locals 2

    const-string v0, "plugins.flutter.io/firebase_remote_config"

    invoke-static {v0, p0}, Lio/flutter/plugins/firebase/core/FlutterFirebasePluginRegistry;->registerPlugin(Ljava/lang/String;Lio/flutter/plugins/firebase/core/FlutterFirebasePlugin;)V

    new-instance v1, Lz4/k;

    invoke-direct {v1, p1, v0}, Lz4/k;-><init>(Lz4/c;Ljava/lang/String;)V

    iput-object v1, p0, Lc5/e;->f:Lz4/k;

    invoke-virtual {v1, p0}, Lz4/k;->e(Lz4/k$c;)V

    new-instance v0, Lz4/d;

    const-string v1, "plugins.flutter.io/firebase_remote_config_updated"

    invoke-direct {v0, p1, v1}, Lz4/d;-><init>(Lz4/c;Ljava/lang/String;)V

    iput-object v0, p0, Lc5/e;->h:Lz4/d;

    invoke-virtual {v0, p0}, Lz4/d;->d(Lz4/d$d;)V

    return-void
.end method

.method private s()V
    .locals 2

    iget-object v0, p0, Lc5/e;->f:Lz4/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lz4/k;->e(Lz4/k$c;)V

    iput-object v1, p0, Lc5/e;->f:Lz4/k;

    iget-object v0, p0, Lc5/e;->h:Lz4/d;

    invoke-virtual {v0, v1}, Lz4/d;->d(Lz4/d$d;)V

    iput-object v1, p0, Lc5/e;->h:Lz4/d;

    return-void
.end method


# virtual methods
.method public C(Lz4/j;Lz4/k$d;)V
    .locals 6

    invoke-virtual {p1}, Lz4/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {p0, v0}, Lc5/e;->j(Ljava/util/Map;)Lcom/google/firebase/remoteconfig/a;

    move-result-object v0

    iget-object v1, p1, Lz4/j;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, -0x1

    sparse-switch v2, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v2, "RemoteConfig#setDefaults"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v5, 0x7

    goto :goto_0

    :sswitch_1
    const-string v2, "RemoteConfig#fetchAndActivate"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v5, 0x6

    goto :goto_0

    :sswitch_2
    const-string v2, "RemoteConfig#getAll"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v5, 0x5

    goto :goto_0

    :sswitch_3
    const-string v2, "RemoteConfig#activate"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v5, 0x4

    goto :goto_0

    :sswitch_4
    const-string v2, "RemoteConfig#fetch"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    :cond_4
    const/4 v5, 0x3

    goto :goto_0

    :sswitch_5
    const-string v2, "RemoteConfig#getProperties"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    :cond_5
    const/4 v5, 0x2

    goto :goto_0

    :sswitch_6
    const-string v2, "RemoteConfig#setConfigSettings"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    :cond_6
    const/4 v5, 0x1

    goto :goto_0

    :sswitch_7
    const-string v2, "RemoteConfig#ensureInitialized"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_0

    :cond_7
    const/4 v5, 0x0

    :goto_0
    packed-switch v5, :pswitch_data_0

    invoke-interface {p2}, Lz4/k$d;->c()V

    return-void

    :pswitch_0
    const-string v1, "defaults"

    invoke-virtual {p1, v1}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Ljava/util/Map;

    invoke-virtual {v0, p1}, Lcom/google/firebase/remoteconfig/a;->z(Ljava/util/Map;)Ln2/i;

    move-result-object p1

    goto :goto_2

    :pswitch_1
    invoke-virtual {v0}, Lcom/google/firebase/remoteconfig/a;->l()Ln2/i;

    move-result-object p1

    goto :goto_2

    :pswitch_2
    invoke-virtual {v0}, Lcom/google/firebase/remoteconfig/a;->m()Ljava/util/Map;

    move-result-object p1

    invoke-direct {p0, p1}, Lc5/e;->q(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    :goto_1
    invoke-static {p1}, Ln2/l;->d(Ljava/lang/Object;)Ln2/i;

    move-result-object p1

    goto :goto_2

    :pswitch_3
    invoke-virtual {v0}, Lcom/google/firebase/remoteconfig/a;->h()Ln2/i;

    move-result-object p1

    goto :goto_2

    :pswitch_4
    invoke-virtual {v0}, Lcom/google/firebase/remoteconfig/a;->k()Ln2/i;

    move-result-object p1

    goto :goto_2

    :pswitch_5
    invoke-direct {p0, v0}, Lc5/e;->h(Lcom/google/firebase/remoteconfig/a;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    :pswitch_6
    const-string v1, "fetchTimeout"

    invoke-virtual {p1, v1}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v2, "minimumFetchInterval"

    invoke-virtual {p1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    new-instance v2, Lc4/r$b;

    invoke-direct {v2}, Lc4/r$b;-><init>()V

    int-to-long v3, v1

    invoke-virtual {v2, v3, v4}, Lc4/r$b;->d(J)Lc4/r$b;

    move-result-object v1

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Lc4/r$b;->e(J)Lc4/r$b;

    move-result-object p1

    invoke-virtual {p1}, Lc4/r$b;->c()Lc4/r;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/remoteconfig/a;->x(Lc4/r;)Ln2/i;

    move-result-object p1

    goto :goto_2

    :pswitch_7
    new-array p1, v3, [Ln2/i;

    invoke-virtual {v0}, Lcom/google/firebase/remoteconfig/a;->j()Ln2/i;

    move-result-object v0

    aput-object v0, p1, v4

    invoke-static {p1}, Ln2/l;->f([Ln2/i;)Ln2/i;

    move-result-object p1

    :goto_2
    new-instance v0, Lc5/c;

    invoke-direct {v0, p2}, Lc5/c;-><init>(Lz4/k$d;)V

    invoke-virtual {p1, v0}, Ln2/i;->b(Ln2/d;)Ln2/i;

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x444528c5 -> :sswitch_7
        -0x39aef1f4 -> :sswitch_6
        -0x312298d2 -> :sswitch_5
        0x2cfdbf -> :sswitch_4
        0x2d6c3ce -> :sswitch_3
        0x7271406 -> :sswitch_2
        0xbced8ab -> :sswitch_1
        0x3fc37019 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public d(Lq4/a$b;)V
    .locals 0

    invoke-direct {p0}, Lc5/e;->s()V

    return-void
.end method

.method public didReinitializeFirebaseCore()Ln2/i;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ln2/i<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Ln2/j;

    invoke-direct {v0}, Ln2/j;-><init>()V

    sget-object v1, Lio/flutter/plugins/firebase/core/FlutterFirebasePlugin;->cachedThreadPool:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lc5/b;

    invoke-direct {v2, v0}, Lc5/b;-><init>(Ln2/j;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ln2/j;->a()Ln2/i;

    move-result-object v0

    return-object v0
.end method

.method public f(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Ljava/util/Map;

    const-string v0, "appName"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lc5/e;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc4/d;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lc4/d;->remove()V

    iget-object v0, p0, Lc5/e;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public getPluginConstantsForFirebaseApp(Lt2/e;)Ln2/i;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lt2/e;",
            ")",
            "Ln2/i<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Ln2/j;

    invoke-direct {v0}, Ln2/j;-><init>()V

    sget-object v1, Lio/flutter/plugins/firebase/core/FlutterFirebasePlugin;->cachedThreadPool:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lc5/a;

    invoke-direct {v2, p0, p1, v0}, Lc5/a;-><init>(Lc5/e;Lt2/e;Ln2/j;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ln2/j;->a()Ln2/i;

    move-result-object p1

    return-object p1
.end method

.method public i(Ljava/lang/Object;Lz4/d$b;)V
    .locals 3

    check-cast p1, Ljava/util/Map;

    invoke-direct {p0, p1}, Lc5/e;->j(Ljava/util/Map;)Lcom/google/firebase/remoteconfig/a;

    move-result-object v0

    const-string v1, "appName"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    iget-object v1, p0, Lc5/e;->g:Ljava/util/Map;

    new-instance v2, Lc5/e$a;

    invoke-direct {v2, p0, p2}, Lc5/e$a;-><init>(Lc5/e;Lz4/d$b;)V

    invoke-virtual {v0, v2}, Lcom/google/firebase/remoteconfig/a;->i(Lc4/c;)Lc4/d;

    move-result-object p2

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public k(Lq4/a$b;)V
    .locals 0

    invoke-virtual {p1}, Lq4/a$b;->b()Lz4/c;

    move-result-object p1

    invoke-direct {p0, p1}, Lc5/e;->r(Lz4/c;)V

    return-void
.end method
