.class public Lh6/a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lq4/a;
.implements Lz4/k$c;
.implements Lz4/d$d;
.implements Lr4/a;
.implements Lz4/n;


# instance fields
.field private f:Landroid/content/BroadcastReceiver;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Landroid/content/Context;

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lh6/a;->j:Z

    return-void
.end method

.method private a(Lz4/d$b;)Landroid/content/BroadcastReceiver;
    .locals 1

    new-instance v0, Lh6/a$a;

    invoke-direct {v0, p0, p1}, Lh6/a$a;-><init>(Lh6/a;Lz4/d$b;)V

    return-object v0
.end method

.method private j(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lh6/a;->j:Z

    if-eqz v0, :cond_0

    iput-object v1, p0, Lh6/a;->g:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lh6/a;->j:Z

    :cond_0
    iput-object v1, p0, Lh6/a;->h:Ljava/lang/String;

    iget-object v0, p0, Lh6/a;->f:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_1
    return-void
.end method

.method private static l(Lz4/c;Lh6/a;)V
    .locals 2

    new-instance v0, Lz4/k;

    const-string v1, "uni_links/messages"

    invoke-direct {v0, p0, v1}, Lz4/k;-><init>(Lz4/c;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lz4/k;->e(Lz4/k$c;)V

    new-instance v0, Lz4/d;

    const-string v1, "uni_links/events"

    invoke-direct {v0, p0, v1}, Lz4/d;-><init>(Lz4/c;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lz4/d;->d(Lz4/d$d;)V

    return-void
.end method


# virtual methods
.method public C(Lz4/j;Lz4/k$d;)V
    .locals 2

    iget-object v0, p1, Lz4/j;->a:Ljava/lang/String;

    const-string v1, "getInitialLink"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lh6/a;->g:Ljava/lang/String;

    :goto_0
    invoke-interface {p2, p1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    iget-object p1, p1, Lz4/j;->a:Ljava/lang/String;

    const-string v0, "getLatestLink"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lh6/a;->h:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-interface {p2}, Lz4/k$d;->c()V

    :goto_1
    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public c(Landroid/content/Intent;)Z
    .locals 1

    iget-object v0, p0, Lh6/a;->i:Landroid/content/Context;

    invoke-direct {p0, v0, p1}, Lh6/a;->j(Landroid/content/Context;Landroid/content/Intent;)V

    const/4 p1, 0x0

    return p1
.end method

.method public d(Lq4/a$b;)V
    .locals 0

    return-void
.end method

.method public e(Lr4/c;)V
    .locals 1

    invoke-interface {p1, p0}, Lr4/c;->g(Lz4/n;)V

    iget-object v0, p0, Lh6/a;->i:Landroid/content/Context;

    invoke-interface {p1}, Lr4/c;->d()Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lh6/a;->j(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method public f(Ljava/lang/Object;)V
    .locals 0

    const/4 p1, 0x0

    iput-object p1, p0, Lh6/a;->f:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public g(Lr4/c;)V
    .locals 1

    invoke-interface {p1, p0}, Lr4/c;->g(Lz4/n;)V

    iget-object v0, p0, Lh6/a;->i:Landroid/content/Context;

    invoke-interface {p1}, Lr4/c;->d()Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lh6/a;->j(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method public h()V
    .locals 0

    return-void
.end method

.method public i(Ljava/lang/Object;Lz4/d$b;)V
    .locals 0

    invoke-direct {p0, p2}, Lh6/a;->a(Lz4/d$b;)Landroid/content/BroadcastReceiver;

    move-result-object p1

    iput-object p1, p0, Lh6/a;->f:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public k(Lq4/a$b;)V
    .locals 1

    invoke-virtual {p1}, Lq4/a$b;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lh6/a;->i:Landroid/content/Context;

    invoke-virtual {p1}, Lq4/a$b;->b()Lz4/c;

    move-result-object p1

    invoke-static {p1, p0}, Lh6/a;->l(Lz4/c;Lh6/a;)V

    return-void
.end method
