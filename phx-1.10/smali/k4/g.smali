.class public final Lk4/g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lq4/a;
.implements Lk4/a$c;
.implements Lr4/a;


# instance fields
.field private f:Lk4/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lk4/a$a;
    .locals 1

    iget-object v0, p0, Lk4/g;->f:Lk4/f;

    invoke-static {v0}, Lkotlin/jvm/internal/i;->b(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lk4/f;->b()Lk4/a$a;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lk4/g;->f:Lk4/f;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lk4/f;->c(Landroid/app/Activity;)V

    :goto_0
    return-void
.end method

.method public c(Lk4/a$b;)V
    .locals 1

    iget-object v0, p0, Lk4/g;->f:Lk4/f;

    invoke-static {v0}, Lkotlin/jvm/internal/i;->b(Ljava/lang/Object;)V

    invoke-static {p1}, Lkotlin/jvm/internal/i;->b(Ljava/lang/Object;)V

    invoke-virtual {v0, p1}, Lk4/f;->d(Lk4/a$b;)V

    return-void
.end method

.method public d(Lq4/a$b;)V
    .locals 1

    const-string v0, "binding"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lq4/a$b;->b()Lz4/c;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lk4/d;->f(Lz4/c;Lk4/a$c;)V

    iput-object v0, p0, Lk4/g;->f:Lk4/f;

    return-void
.end method

.method public e(Lr4/c;)V
    .locals 1

    const-string v0, "binding"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lk4/g;->f:Lk4/f;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Lr4/c;->d()Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {v0, p1}, Lk4/f;->c(Landroid/app/Activity;)V

    :goto_0
    return-void
.end method

.method public g(Lr4/c;)V
    .locals 1

    const-string v0, "binding"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lk4/g;->e(Lr4/c;)V

    return-void
.end method

.method public h()V
    .locals 0

    invoke-virtual {p0}, Lk4/g;->b()V

    return-void
.end method

.method public k(Lq4/a$b;)V
    .locals 1

    const-string v0, "flutterPluginBinding"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lq4/a$b;->b()Lz4/c;

    move-result-object p1

    invoke-static {p1, p0}, Lk4/d;->f(Lz4/c;Lk4/a$c;)V

    new-instance p1, Lk4/f;

    invoke-direct {p1}, Lk4/f;-><init>()V

    iput-object p1, p0, Lk4/g;->f:Lk4/f;

    return-void
.end method
