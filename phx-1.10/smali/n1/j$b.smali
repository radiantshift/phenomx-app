.class public Ln1/j$b;
.super Ln1/j;
.source ""

# interfaces
.implements Lm1/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ln1/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field final i:Ln1/k$a;


# direct methods
.method public constructor <init>(JLh0/r1;Ljava/util/List;Ln1/k$a;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lh0/r1;",
            "Ljava/util/List<",
            "Ln1/b;",
            ">;",
            "Ln1/k$a;",
            "Ljava/util/List<",
            "Ln1/e;",
            ">;",
            "Ljava/util/List<",
            "Ln1/e;",
            ">;",
            "Ljava/util/List<",
            "Ln1/e;",
            ">;)V"
        }
    .end annotation

    const/4 v9, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Ln1/j;-><init>(JLh0/r1;Ljava/util/List;Ln1/k;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ln1/j$a;)V

    move-object v1, p5

    iput-object v1, v0, Ln1/j$b;->i:Ln1/k$a;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Lm1/f;
    .locals 0

    return-object p0
.end method

.method public c()Ln1/i;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public d(J)J
    .locals 1

    iget-object v0, p0, Ln1/j$b;->i:Ln1/k$a;

    invoke-virtual {v0, p1, p2}, Ln1/k$a;->j(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public e(JJ)J
    .locals 1

    iget-object v0, p0, Ln1/j$b;->i:Ln1/k$a;

    invoke-virtual {v0, p1, p2, p3, p4}, Ln1/k$a;->i(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public f(JJ)J
    .locals 1

    iget-object v0, p0, Ln1/j$b;->i:Ln1/k$a;

    invoke-virtual {v0, p1, p2, p3, p4}, Ln1/k$a;->h(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public g(JJ)J
    .locals 1

    iget-object v0, p0, Ln1/j$b;->i:Ln1/k$a;

    invoke-virtual {v0, p1, p2, p3, p4}, Ln1/k$a;->d(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public h(JJ)J
    .locals 1

    iget-object v0, p0, Ln1/j$b;->i:Ln1/k$a;

    invoke-virtual {v0, p1, p2, p3, p4}, Ln1/k$a;->f(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public i(J)Ln1/i;
    .locals 1

    iget-object v0, p0, Ln1/j$b;->i:Ln1/k$a;

    invoke-virtual {v0, p0, p1, p2}, Ln1/k$a;->k(Ln1/j;J)Ln1/i;

    move-result-object p1

    return-object p1
.end method

.method public j()Z
    .locals 1

    iget-object v0, p0, Ln1/j$b;->i:Ln1/k$a;

    invoke-virtual {v0}, Ln1/k$a;->l()Z

    move-result v0

    return v0
.end method

.method public k()J
    .locals 2

    iget-object v0, p0, Ln1/j$b;->i:Ln1/k$a;

    invoke-virtual {v0}, Ln1/k$a;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public l(J)J
    .locals 1

    iget-object v0, p0, Ln1/j$b;->i:Ln1/k$a;

    invoke-virtual {v0, p1, p2}, Ln1/k$a;->g(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public m(JJ)J
    .locals 1

    iget-object v0, p0, Ln1/j$b;->i:Ln1/k$a;

    invoke-virtual {v0, p1, p2, p3, p4}, Ln1/k$a;->c(JJ)J

    move-result-wide p1

    return-wide p1
.end method
