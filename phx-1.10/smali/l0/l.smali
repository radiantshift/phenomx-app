.class public final Ll0/l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ll0/b0;


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:Lh0/z1$f;

.field private c:Ll0/y;

.field private d:Ld2/l$a;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ll0/l;->a:Ljava/lang/Object;

    return-void
.end method

.method private b(Lh0/z1$f;)Ll0/y;
    .locals 4

    iget-object v0, p0, Ll0/l;->d:Ld2/l$a;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ld2/u$b;

    invoke-direct {v0}, Ld2/u$b;-><init>()V

    iget-object v1, p0, Ll0/l;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ld2/u$b;->e(Ljava/lang/String;)Ld2/u$b;

    move-result-object v0

    :goto_0
    new-instance v1, Ll0/l0;

    iget-object v2, p1, Lh0/z1$f;->c:Landroid/net/Uri;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    iget-boolean v3, p1, Lh0/z1$f;->h:Z

    invoke-direct {v1, v2, v3, v0}, Ll0/l0;-><init>(Ljava/lang/String;ZLd2/l$a;)V

    iget-object v0, p1, Lh0/z1$f;->e:Lp2/r;

    invoke-virtual {v0}, Lp2/r;->g()Lp2/s;

    move-result-object v0

    invoke-virtual {v0}, Lp2/s;->h()Lp2/s0;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Ll0/l0;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    new-instance v0, Ll0/h$b;

    invoke-direct {v0}, Ll0/h$b;-><init>()V

    iget-object v2, p1, Lh0/z1$f;->a:Ljava/util/UUID;

    sget-object v3, Ll0/k0;->d:Ll0/g0$c;

    invoke-virtual {v0, v2, v3}, Ll0/h$b;->e(Ljava/util/UUID;Ll0/g0$c;)Ll0/h$b;

    move-result-object v0

    iget-boolean v2, p1, Lh0/z1$f;->f:Z

    invoke-virtual {v0, v2}, Ll0/h$b;->b(Z)Ll0/h$b;

    move-result-object v0

    iget-boolean v2, p1, Lh0/z1$f;->g:Z

    invoke-virtual {v0, v2}, Ll0/h$b;->c(Z)Ll0/h$b;

    move-result-object v0

    iget-object v2, p1, Lh0/z1$f;->j:Lp2/q;

    invoke-static {v2}, Lr2/e;->k(Ljava/util/Collection;)[I

    move-result-object v2

    invoke-virtual {v0, v2}, Ll0/h$b;->d([I)Ll0/h$b;

    move-result-object v0

    invoke-virtual {v0, v1}, Ll0/h$b;->a(Ll0/n0;)Ll0/h;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lh0/z1$f;->c()[B

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ll0/h;->G(I[B)V

    return-object v0
.end method


# virtual methods
.method public a(Lh0/z1;)Ll0/y;
    .locals 2

    iget-object v0, p1, Lh0/z1;->g:Lh0/z1$h;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p1, Lh0/z1;->g:Lh0/z1$h;

    iget-object p1, p1, Lh0/z1$h;->c:Lh0/z1$f;

    if-eqz p1, :cond_2

    sget v0, Le2/n0;->a:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ll0/l;->a:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Ll0/l;->b:Lh0/z1$f;

    invoke-static {p1, v1}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-object p1, p0, Ll0/l;->b:Lh0/z1$f;

    invoke-direct {p0, p1}, Ll0/l;->b(Lh0/z1$f;)Ll0/y;

    move-result-object p1

    iput-object p1, p0, Ll0/l;->c:Ll0/y;

    :cond_1
    iget-object p1, p0, Ll0/l;->c:Ll0/y;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ll0/y;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_2
    :goto_0
    sget-object p1, Ll0/y;->a:Ll0/y;

    return-object p1
.end method
