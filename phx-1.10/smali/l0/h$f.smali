.class Ll0/h$f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ll0/y$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ll0/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation


# instance fields
.field private final b:Ll0/w$a;

.field private c:Ll0/o;

.field private d:Z

.field final synthetic e:Ll0/h;


# direct methods
.method public constructor <init>(Ll0/h;Ll0/w$a;)V
    .locals 0

    iput-object p1, p0, Ll0/h$f;->e:Ll0/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Ll0/h$f;->b:Ll0/w$a;

    return-void
.end method

.method public static synthetic b(Ll0/h$f;)V
    .locals 0

    invoke-direct {p0}, Ll0/h$f;->f()V

    return-void
.end method

.method public static synthetic c(Ll0/h$f;Lh0/r1;)V
    .locals 0

    invoke-direct {p0, p1}, Ll0/h$f;->e(Lh0/r1;)V

    return-void
.end method

.method private synthetic e(Lh0/r1;)V
    .locals 4

    iget-object v0, p0, Ll0/h$f;->e:Ll0/h;

    invoke-static {v0}, Ll0/h;->r(Ll0/h;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ll0/h$f;->d:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ll0/h$f;->e:Ll0/h;

    invoke-static {v0}, Ll0/h;->l(Ll0/h;)Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Looper;

    iget-object v2, p0, Ll0/h$f;->b:Ll0/w$a;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, p1, v3}, Ll0/h;->m(Ll0/h;Landroid/os/Looper;Ll0/w$a;Lh0/r1;Z)Ll0/o;

    move-result-object p1

    iput-object p1, p0, Ll0/h$f;->c:Ll0/o;

    iget-object p1, p0, Ll0/h$f;->e:Ll0/h;

    invoke-static {p1}, Ll0/h;->k(Ll0/h;)Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method private synthetic f()V
    .locals 2

    iget-boolean v0, p0, Ll0/h$f;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Ll0/h$f;->c:Ll0/o;

    if-eqz v0, :cond_1

    iget-object v1, p0, Ll0/h$f;->b:Ll0/w$a;

    invoke-interface {v0, v1}, Ll0/o;->b(Ll0/w$a;)V

    :cond_1
    iget-object v0, p0, Ll0/h$f;->e:Ll0/h;

    invoke-static {v0}, Ll0/h;->k(Ll0/h;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Ll0/h$f;->d:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Ll0/h$f;->e:Ll0/h;

    invoke-static {v0}, Ll0/h;->q(Ll0/h;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    new-instance v1, Ll0/i;

    invoke-direct {v1, p0}, Ll0/i;-><init>(Ll0/h$f;)V

    invoke-static {v0, v1}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    return-void
.end method

.method public d(Lh0/r1;)V
    .locals 2

    iget-object v0, p0, Ll0/h$f;->e:Ll0/h;

    invoke-static {v0}, Ll0/h;->q(Ll0/h;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    new-instance v1, Ll0/j;

    invoke-direct {v1, p0, p1}, Ll0/j;-><init>(Ll0/h$f;Lh0/r1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
