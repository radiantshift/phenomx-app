.class public Ll0/w$a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ll0/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ll0/w$a$a;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:Lj1/x$b;

.field private final c:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Ll0/w$a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Ll0/w$a;-><init>(Ljava/util/concurrent/CopyOnWriteArrayList;ILj1/x$b;)V

    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/CopyOnWriteArrayList;ILj1/x$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Ll0/w$a$a;",
            ">;I",
            "Lj1/x$b;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ll0/w$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput p2, p0, Ll0/w$a;->a:I

    iput-object p3, p0, Ll0/w$a;->b:Lj1/x$b;

    return-void
.end method

.method public static synthetic a(Ll0/w$a;Ll0/w;)V
    .locals 0

    invoke-direct {p0, p1}, Ll0/w$a;->s(Ll0/w;)V

    return-void
.end method

.method public static synthetic b(Ll0/w$a;Ll0/w;)V
    .locals 0

    invoke-direct {p0, p1}, Ll0/w$a;->o(Ll0/w;)V

    return-void
.end method

.method public static synthetic c(Ll0/w$a;Ll0/w;)V
    .locals 0

    invoke-direct {p0, p1}, Ll0/w$a;->n(Ll0/w;)V

    return-void
.end method

.method public static synthetic d(Ll0/w$a;Ll0/w;)V
    .locals 0

    invoke-direct {p0, p1}, Ll0/w$a;->p(Ll0/w;)V

    return-void
.end method

.method public static synthetic e(Ll0/w$a;Ll0/w;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ll0/w$a;->r(Ll0/w;Ljava/lang/Exception;)V

    return-void
.end method

.method public static synthetic f(Ll0/w$a;Ll0/w;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ll0/w$a;->q(Ll0/w;I)V

    return-void
.end method

.method private synthetic n(Ll0/w;)V
    .locals 2

    iget v0, p0, Ll0/w$a;->a:I

    iget-object v1, p0, Ll0/w$a;->b:Lj1/x$b;

    invoke-interface {p1, v0, v1}, Ll0/w;->h0(ILj1/x$b;)V

    return-void
.end method

.method private synthetic o(Ll0/w;)V
    .locals 2

    iget v0, p0, Ll0/w$a;->a:I

    iget-object v1, p0, Ll0/w$a;->b:Lj1/x$b;

    invoke-interface {p1, v0, v1}, Ll0/w;->e0(ILj1/x$b;)V

    return-void
.end method

.method private synthetic p(Ll0/w;)V
    .locals 2

    iget v0, p0, Ll0/w$a;->a:I

    iget-object v1, p0, Ll0/w$a;->b:Lj1/x$b;

    invoke-interface {p1, v0, v1}, Ll0/w;->F(ILj1/x$b;)V

    return-void
.end method

.method private synthetic q(Ll0/w;I)V
    .locals 2

    iget v0, p0, Ll0/w$a;->a:I

    iget-object v1, p0, Ll0/w$a;->b:Lj1/x$b;

    invoke-interface {p1, v0, v1}, Ll0/w;->g0(ILj1/x$b;)V

    iget v0, p0, Ll0/w$a;->a:I

    iget-object v1, p0, Ll0/w$a;->b:Lj1/x$b;

    invoke-interface {p1, v0, v1, p2}, Ll0/w;->C(ILj1/x$b;I)V

    return-void
.end method

.method private synthetic r(Ll0/w;Ljava/lang/Exception;)V
    .locals 2

    iget v0, p0, Ll0/w$a;->a:I

    iget-object v1, p0, Ll0/w$a;->b:Lj1/x$b;

    invoke-interface {p1, v0, v1, p2}, Ll0/w;->m0(ILj1/x$b;Ljava/lang/Exception;)V

    return-void
.end method

.method private synthetic s(Ll0/w;)V
    .locals 2

    iget v0, p0, Ll0/w$a;->a:I

    iget-object v1, p0, Ll0/w$a;->b:Lj1/x$b;

    invoke-interface {p1, v0, v1}, Ll0/w;->L(ILj1/x$b;)V

    return-void
.end method


# virtual methods
.method public g(Landroid/os/Handler;Ll0/w;)V
    .locals 2

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ll0/w$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Ll0/w$a$a;

    invoke-direct {v1, p1, p2}, Ll0/w$a$a;-><init>(Landroid/os/Handler;Ll0/w;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public h()V
    .locals 4

    iget-object v0, p0, Ll0/w$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ll0/w$a$a;

    iget-object v2, v1, Ll0/w$a$a;->b:Ll0/w;

    iget-object v1, v1, Ll0/w$a$a;->a:Landroid/os/Handler;

    new-instance v3, Ll0/s;

    invoke-direct {v3, p0, v2}, Ll0/s;-><init>(Ll0/w$a;Ll0/w;)V

    invoke-static {v1, v3}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public i()V
    .locals 4

    iget-object v0, p0, Ll0/w$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ll0/w$a$a;

    iget-object v2, v1, Ll0/w$a$a;->b:Ll0/w;

    iget-object v1, v1, Ll0/w$a$a;->a:Landroid/os/Handler;

    new-instance v3, Ll0/r;

    invoke-direct {v3, p0, v2}, Ll0/r;-><init>(Ll0/w$a;Ll0/w;)V

    invoke-static {v1, v3}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public j()V
    .locals 4

    iget-object v0, p0, Ll0/w$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ll0/w$a$a;

    iget-object v2, v1, Ll0/w$a$a;->b:Ll0/w;

    iget-object v1, v1, Ll0/w$a$a;->a:Landroid/os/Handler;

    new-instance v3, Ll0/t;

    invoke-direct {v3, p0, v2}, Ll0/t;-><init>(Ll0/w$a;Ll0/w;)V

    invoke-static {v1, v3}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public k(I)V
    .locals 4

    iget-object v0, p0, Ll0/w$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ll0/w$a$a;

    iget-object v2, v1, Ll0/w$a$a;->b:Ll0/w;

    iget-object v1, v1, Ll0/w$a$a;->a:Landroid/os/Handler;

    new-instance v3, Ll0/u;

    invoke-direct {v3, p0, v2, p1}, Ll0/u;-><init>(Ll0/w$a;Ll0/w;I)V

    invoke-static {v1, v3}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public l(Ljava/lang/Exception;)V
    .locals 4

    iget-object v0, p0, Ll0/w$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ll0/w$a$a;

    iget-object v2, v1, Ll0/w$a$a;->b:Ll0/w;

    iget-object v1, v1, Ll0/w$a$a;->a:Landroid/os/Handler;

    new-instance v3, Ll0/v;

    invoke-direct {v3, p0, v2, p1}, Ll0/v;-><init>(Ll0/w$a;Ll0/w;Ljava/lang/Exception;)V

    invoke-static {v1, v3}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public m()V
    .locals 4

    iget-object v0, p0, Ll0/w$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ll0/w$a$a;

    iget-object v2, v1, Ll0/w$a$a;->b:Ll0/w;

    iget-object v1, v1, Ll0/w$a$a;->a:Landroid/os/Handler;

    new-instance v3, Ll0/q;

    invoke-direct {v3, p0, v2}, Ll0/q;-><init>(Ll0/w$a;Ll0/w;)V

    invoke-static {v1, v3}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public t(Ll0/w;)V
    .locals 3

    iget-object v0, p0, Ll0/w$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ll0/w$a$a;

    iget-object v2, v1, Ll0/w$a$a;->b:Ll0/w;

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Ll0/w$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public u(ILj1/x$b;)Ll0/w$a;
    .locals 2

    new-instance v0, Ll0/w$a;

    iget-object v1, p0, Ll0/w$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0, v1, p1, p2}, Ll0/w$a;-><init>(Ljava/util/concurrent/CopyOnWriteArrayList;ILj1/x$b;)V

    return-object v0
.end method
