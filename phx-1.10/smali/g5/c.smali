.class public final Lg5/c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lq4/a;
.implements Lr4/a;


# instance fields
.field private f:Lg5/a;

.field private g:Lg5/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public b()V
    .locals 2

    iget-object v0, p0, Lg5/c;->f:Lg5/a;

    if-nez v0, :cond_0

    const-string v0, "UrlLauncherPlugin"

    const-string v1, "urlLauncher was never set."

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lg5/c;->g:Lg5/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lg5/b;->d(Landroid/app/Activity;)V

    return-void
.end method

.method public d(Lq4/a$b;)V
    .locals 1

    iget-object p1, p0, Lg5/c;->f:Lg5/a;

    if-nez p1, :cond_0

    const-string p1, "UrlLauncherPlugin"

    const-string v0, "Already detached from the engine."

    invoke-static {p1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-virtual {p1}, Lg5/a;->f()V

    const/4 p1, 0x0

    iput-object p1, p0, Lg5/c;->f:Lg5/a;

    iput-object p1, p0, Lg5/c;->g:Lg5/b;

    return-void
.end method

.method public e(Lr4/c;)V
    .locals 1

    iget-object v0, p0, Lg5/c;->f:Lg5/a;

    if-nez v0, :cond_0

    const-string p1, "UrlLauncherPlugin"

    const-string v0, "urlLauncher was never set."

    invoke-static {p1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lg5/c;->g:Lg5/b;

    invoke-interface {p1}, Lr4/c;->d()Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {v0, p1}, Lg5/b;->d(Landroid/app/Activity;)V

    return-void
.end method

.method public g(Lr4/c;)V
    .locals 0

    invoke-virtual {p0, p1}, Lg5/c;->e(Lr4/c;)V

    return-void
.end method

.method public h()V
    .locals 0

    invoke-virtual {p0}, Lg5/c;->b()V

    return-void
.end method

.method public k(Lq4/a$b;)V
    .locals 3

    new-instance v0, Lg5/b;

    invoke-virtual {p1}, Lq4/a$b;->a()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lg5/b;-><init>(Landroid/content/Context;Landroid/app/Activity;)V

    iput-object v0, p0, Lg5/c;->g:Lg5/b;

    new-instance v1, Lg5/a;

    invoke-direct {v1, v0}, Lg5/a;-><init>(Lg5/b;)V

    iput-object v1, p0, Lg5/c;->f:Lg5/a;

    invoke-virtual {p1}, Lq4/a$b;->b()Lz4/c;

    move-result-object p1

    invoke-virtual {v1, p1}, Lg5/a;->e(Lz4/c;)V

    return-void
.end method
