.class public Lc2/a0$a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc2/a0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Z

.field private l:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:I

.field private n:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o:I

.field private p:I

.field private q:I

.field private r:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private t:I

.field private u:I

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lj1/x0;",
            "Lc2/y;",
            ">;"
        }
    .end annotation
.end field

.field private z:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    iput v0, p0, Lc2/a0$a;->a:I

    iput v0, p0, Lc2/a0$a;->b:I

    iput v0, p0, Lc2/a0$a;->c:I

    iput v0, p0, Lc2/a0$a;->d:I

    iput v0, p0, Lc2/a0$a;->i:I

    iput v0, p0, Lc2/a0$a;->j:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lc2/a0$a;->k:Z

    invoke-static {}, Lp2/q;->q()Lp2/q;

    move-result-object v1

    iput-object v1, p0, Lc2/a0$a;->l:Lp2/q;

    const/4 v1, 0x0

    iput v1, p0, Lc2/a0$a;->m:I

    invoke-static {}, Lp2/q;->q()Lp2/q;

    move-result-object v2

    iput-object v2, p0, Lc2/a0$a;->n:Lp2/q;

    iput v1, p0, Lc2/a0$a;->o:I

    iput v0, p0, Lc2/a0$a;->p:I

    iput v0, p0, Lc2/a0$a;->q:I

    invoke-static {}, Lp2/q;->q()Lp2/q;

    move-result-object v0

    iput-object v0, p0, Lc2/a0$a;->r:Lp2/q;

    invoke-static {}, Lp2/q;->q()Lp2/q;

    move-result-object v0

    iput-object v0, p0, Lc2/a0$a;->s:Lp2/q;

    iput v1, p0, Lc2/a0$a;->t:I

    iput v1, p0, Lc2/a0$a;->u:I

    iput-boolean v1, p0, Lc2/a0$a;->v:Z

    iput-boolean v1, p0, Lc2/a0$a;->w:Z

    iput-boolean v1, p0, Lc2/a0$a;->x:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lc2/a0$a;->y:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lc2/a0$a;->z:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lc2/a0$a;-><init>()V

    invoke-virtual {p0, p1}, Lc2/a0$a;->E(Landroid/content/Context;)Lc2/a0$a;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lc2/a0$a;->H(Landroid/content/Context;Z)Lc2/a0$a;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lc2/a0;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lc2/a0;->F:Lc2/a0;

    iget v2, v1, Lc2/a0;->f:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->a:I

    invoke-static {}, Lc2/a0;->b()Ljava/lang/String;

    move-result-object v0

    iget v2, v1, Lc2/a0;->g:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->b:I

    invoke-static {}, Lc2/a0;->m()Ljava/lang/String;

    move-result-object v0

    iget v2, v1, Lc2/a0;->h:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->c:I

    invoke-static {}, Lc2/a0;->t()Ljava/lang/String;

    move-result-object v0

    iget v2, v1, Lc2/a0;->i:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->d:I

    invoke-static {}, Lc2/a0;->u()Ljava/lang/String;

    move-result-object v0

    iget v2, v1, Lc2/a0;->j:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->e:I

    invoke-static {}, Lc2/a0;->v()Ljava/lang/String;

    move-result-object v0

    iget v2, v1, Lc2/a0;->k:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->f:I

    invoke-static {}, Lc2/a0;->w()Ljava/lang/String;

    move-result-object v0

    iget v2, v1, Lc2/a0;->l:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->g:I

    invoke-static {}, Lc2/a0;->x()Ljava/lang/String;

    move-result-object v0

    iget v2, v1, Lc2/a0;->m:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->h:I

    invoke-static {}, Lc2/a0;->y()Ljava/lang/String;

    move-result-object v0

    iget v2, v1, Lc2/a0;->n:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->i:I

    invoke-static {}, Lc2/a0;->z()Ljava/lang/String;

    move-result-object v0

    iget v2, v1, Lc2/a0;->o:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->j:I

    invoke-static {}, Lc2/a0;->c()Ljava/lang/String;

    move-result-object v0

    iget-boolean v2, v1, Lc2/a0;->p:Z

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lc2/a0$a;->k:Z

    invoke-static {}, Lc2/a0;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/String;

    invoke-static {v0, v3}, Lo2/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v0}, Lp2/q;->n([Ljava/lang/Object;)Lp2/q;

    move-result-object v0

    iput-object v0, p0, Lc2/a0$a;->l:Lp2/q;

    invoke-static {}, Lc2/a0;->e()Ljava/lang/String;

    move-result-object v0

    iget v3, v1, Lc2/a0;->r:I

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->m:I

    invoke-static {}, Lc2/a0;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    new-array v3, v2, [Ljava/lang/String;

    invoke-static {v0, v3}, Lo2/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v0}, Lc2/a0$a;->C([Ljava/lang/String;)Lp2/q;

    move-result-object v0

    iput-object v0, p0, Lc2/a0$a;->n:Lp2/q;

    invoke-static {}, Lc2/a0;->g()Ljava/lang/String;

    move-result-object v0

    iget v3, v1, Lc2/a0;->t:I

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->o:I

    invoke-static {}, Lc2/a0;->h()Ljava/lang/String;

    move-result-object v0

    iget v3, v1, Lc2/a0;->u:I

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->p:I

    invoke-static {}, Lc2/a0;->i()Ljava/lang/String;

    move-result-object v0

    iget v3, v1, Lc2/a0;->v:I

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->q:I

    invoke-static {}, Lc2/a0;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    new-array v3, v2, [Ljava/lang/String;

    invoke-static {v0, v3}, Lo2/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v0}, Lp2/q;->n([Ljava/lang/Object;)Lp2/q;

    move-result-object v0

    iput-object v0, p0, Lc2/a0$a;->r:Lp2/q;

    invoke-static {}, Lc2/a0;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    new-array v3, v2, [Ljava/lang/String;

    invoke-static {v0, v3}, Lo2/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v0}, Lc2/a0$a;->C([Ljava/lang/String;)Lp2/q;

    move-result-object v0

    iput-object v0, p0, Lc2/a0$a;->s:Lp2/q;

    invoke-static {}, Lc2/a0;->l()Ljava/lang/String;

    move-result-object v0

    iget v3, v1, Lc2/a0;->y:I

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->t:I

    invoke-static {}, Lc2/a0;->n()Ljava/lang/String;

    move-result-object v0

    iget v3, v1, Lc2/a0;->z:I

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc2/a0$a;->u:I

    invoke-static {}, Lc2/a0;->o()Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, v1, Lc2/a0;->A:Z

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lc2/a0$a;->v:Z

    invoke-static {}, Lc2/a0;->p()Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, v1, Lc2/a0;->B:Z

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lc2/a0$a;->w:Z

    invoke-static {}, Lc2/a0;->q()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, v1, Lc2/a0;->C:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lc2/a0$a;->x:Z

    invoke-static {}, Lc2/a0;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lp2/q;->q()Lp2/q;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget-object v1, Lc2/y;->j:Lh0/h$a;

    invoke-static {v1, v0}, Le2/c;->b(Lh0/h$a;Ljava/util/List;)Lp2/q;

    move-result-object v0

    :goto_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lc2/a0$a;->y:Ljava/util/HashMap;

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lc2/y;

    iget-object v4, p0, Lc2/a0$a;->y:Ljava/util/HashMap;

    iget-object v5, v3, Lc2/y;->f:Lj1/x0;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-static {}, Lc2/a0;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object p1

    new-array v0, v2, [I

    invoke-static {p1, v0}, Lo2/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lc2/a0$a;->z:Ljava/util/HashSet;

    array-length v0, p1

    :goto_2
    if-ge v2, v0, :cond_2

    aget v1, p1, v2

    iget-object v3, p0, Lc2/a0$a;->z:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method

.method protected constructor <init>(Lc2/a0;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1}, Lc2/a0$a;->B(Lc2/a0;)V

    return-void
.end method

.method private B(Lc2/a0;)V
    .locals 2
    .annotation runtime Lorg/checkerframework/checker/nullness/qual/EnsuresNonNull;
        value = {
            "preferredVideoMimeTypes",
            "preferredAudioLanguages",
            "preferredAudioMimeTypes",
            "preferredTextLanguages",
            "overrides",
            "disabledTrackTypes"
        }
    .end annotation

    iget v0, p1, Lc2/a0;->f:I

    iput v0, p0, Lc2/a0$a;->a:I

    iget v0, p1, Lc2/a0;->g:I

    iput v0, p0, Lc2/a0$a;->b:I

    iget v0, p1, Lc2/a0;->h:I

    iput v0, p0, Lc2/a0$a;->c:I

    iget v0, p1, Lc2/a0;->i:I

    iput v0, p0, Lc2/a0$a;->d:I

    iget v0, p1, Lc2/a0;->j:I

    iput v0, p0, Lc2/a0$a;->e:I

    iget v0, p1, Lc2/a0;->k:I

    iput v0, p0, Lc2/a0$a;->f:I

    iget v0, p1, Lc2/a0;->l:I

    iput v0, p0, Lc2/a0$a;->g:I

    iget v0, p1, Lc2/a0;->m:I

    iput v0, p0, Lc2/a0$a;->h:I

    iget v0, p1, Lc2/a0;->n:I

    iput v0, p0, Lc2/a0$a;->i:I

    iget v0, p1, Lc2/a0;->o:I

    iput v0, p0, Lc2/a0$a;->j:I

    iget-boolean v0, p1, Lc2/a0;->p:Z

    iput-boolean v0, p0, Lc2/a0$a;->k:Z

    iget-object v0, p1, Lc2/a0;->q:Lp2/q;

    iput-object v0, p0, Lc2/a0$a;->l:Lp2/q;

    iget v0, p1, Lc2/a0;->r:I

    iput v0, p0, Lc2/a0$a;->m:I

    iget-object v0, p1, Lc2/a0;->s:Lp2/q;

    iput-object v0, p0, Lc2/a0$a;->n:Lp2/q;

    iget v0, p1, Lc2/a0;->t:I

    iput v0, p0, Lc2/a0$a;->o:I

    iget v0, p1, Lc2/a0;->u:I

    iput v0, p0, Lc2/a0$a;->p:I

    iget v0, p1, Lc2/a0;->v:I

    iput v0, p0, Lc2/a0$a;->q:I

    iget-object v0, p1, Lc2/a0;->w:Lp2/q;

    iput-object v0, p0, Lc2/a0$a;->r:Lp2/q;

    iget-object v0, p1, Lc2/a0;->x:Lp2/q;

    iput-object v0, p0, Lc2/a0$a;->s:Lp2/q;

    iget v0, p1, Lc2/a0;->y:I

    iput v0, p0, Lc2/a0$a;->t:I

    iget v0, p1, Lc2/a0;->z:I

    iput v0, p0, Lc2/a0$a;->u:I

    iget-boolean v0, p1, Lc2/a0;->A:Z

    iput-boolean v0, p0, Lc2/a0$a;->v:Z

    iget-boolean v0, p1, Lc2/a0;->B:Z

    iput-boolean v0, p0, Lc2/a0$a;->w:Z

    iget-boolean v0, p1, Lc2/a0;->C:Z

    iput-boolean v0, p0, Lc2/a0$a;->x:Z

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, Lc2/a0;->E:Lp2/s;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lc2/a0$a;->z:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    iget-object p1, p1, Lc2/a0;->D:Lp2/r;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lc2/a0$a;->y:Ljava/util/HashMap;

    return-void
.end method

.method private static C([Ljava/lang/String;)Lp2/q;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Lp2/q<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lp2/q;->k()Lp2/q$a;

    move-result-object v0

    invoke-static {p0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/String;

    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p0, v2

    invoke-static {v3}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Le2/n0;->E0(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lp2/q$a;->f(Ljava/lang/Object;)Lp2/q$a;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lp2/q$a;->h()Lp2/q;

    move-result-object p0

    return-object p0
.end method

.method private F(Landroid/content/Context;)V
    .locals 2

    sget v0, Le2/n0;->a:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "captioning"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/accessibility/CaptioningManager;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/accessibility/CaptioningManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/16 v0, 0x440

    iput v0, p0, Lc2/a0$a;->t:I

    invoke-virtual {p1}, Landroid/view/accessibility/CaptioningManager;->getLocale()Ljava/util/Locale;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-static {p1}, Le2/n0;->X(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lp2/q;->r(Ljava/lang/Object;)Lp2/q;

    move-result-object p1

    iput-object p1, p0, Lc2/a0$a;->s:Lp2/q;

    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic a(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->a:I

    return p0
.end method

.method static synthetic b(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->b:I

    return p0
.end method

.method static synthetic c(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->c:I

    return p0
.end method

.method static synthetic d(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->d:I

    return p0
.end method

.method static synthetic e(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->e:I

    return p0
.end method

.method static synthetic f(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->f:I

    return p0
.end method

.method static synthetic g(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->g:I

    return p0
.end method

.method static synthetic h(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->h:I

    return p0
.end method

.method static synthetic i(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->i:I

    return p0
.end method

.method static synthetic j(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->j:I

    return p0
.end method

.method static synthetic k(Lc2/a0$a;)Z
    .locals 0

    iget-boolean p0, p0, Lc2/a0$a;->k:Z

    return p0
.end method

.method static synthetic l(Lc2/a0$a;)Lp2/q;
    .locals 0

    iget-object p0, p0, Lc2/a0$a;->l:Lp2/q;

    return-object p0
.end method

.method static synthetic m(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->m:I

    return p0
.end method

.method static synthetic n(Lc2/a0$a;)Lp2/q;
    .locals 0

    iget-object p0, p0, Lc2/a0$a;->n:Lp2/q;

    return-object p0
.end method

.method static synthetic o(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->o:I

    return p0
.end method

.method static synthetic p(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->p:I

    return p0
.end method

.method static synthetic q(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->q:I

    return p0
.end method

.method static synthetic r(Lc2/a0$a;)Lp2/q;
    .locals 0

    iget-object p0, p0, Lc2/a0$a;->r:Lp2/q;

    return-object p0
.end method

.method static synthetic s(Lc2/a0$a;)Lp2/q;
    .locals 0

    iget-object p0, p0, Lc2/a0$a;->s:Lp2/q;

    return-object p0
.end method

.method static synthetic t(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->t:I

    return p0
.end method

.method static synthetic u(Lc2/a0$a;)I
    .locals 0

    iget p0, p0, Lc2/a0$a;->u:I

    return p0
.end method

.method static synthetic v(Lc2/a0$a;)Z
    .locals 0

    iget-boolean p0, p0, Lc2/a0$a;->v:Z

    return p0
.end method

.method static synthetic w(Lc2/a0$a;)Z
    .locals 0

    iget-boolean p0, p0, Lc2/a0$a;->w:Z

    return p0
.end method

.method static synthetic x(Lc2/a0$a;)Z
    .locals 0

    iget-boolean p0, p0, Lc2/a0$a;->x:Z

    return p0
.end method

.method static synthetic y(Lc2/a0$a;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lc2/a0$a;->y:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic z(Lc2/a0$a;)Ljava/util/HashSet;
    .locals 0

    iget-object p0, p0, Lc2/a0$a;->z:Ljava/util/HashSet;

    return-object p0
.end method


# virtual methods
.method public A()Lc2/a0;
    .locals 1

    new-instance v0, Lc2/a0;

    invoke-direct {v0, p0}, Lc2/a0;-><init>(Lc2/a0$a;)V

    return-object v0
.end method

.method protected D(Lc2/a0;)Lc2/a0$a;
    .locals 0

    invoke-direct {p0, p1}, Lc2/a0$a;->B(Lc2/a0;)V

    return-object p0
.end method

.method public E(Landroid/content/Context;)Lc2/a0$a;
    .locals 2

    sget v0, Le2/n0;->a:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    invoke-direct {p0, p1}, Lc2/a0$a;->F(Landroid/content/Context;)V

    :cond_0
    return-object p0
.end method

.method public G(IIZ)Lc2/a0$a;
    .locals 0

    iput p1, p0, Lc2/a0$a;->i:I

    iput p2, p0, Lc2/a0$a;->j:I

    iput-boolean p3, p0, Lc2/a0$a;->k:Z

    return-object p0
.end method

.method public H(Landroid/content/Context;Z)Lc2/a0$a;
    .locals 1

    invoke-static {p1}, Le2/n0;->O(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object p1

    iget v0, p1, Landroid/graphics/Point;->x:I

    iget p1, p1, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v0, p1, p2}, Lc2/a0$a;->G(IIZ)Lc2/a0$a;

    move-result-object p1

    return-object p1
.end method
