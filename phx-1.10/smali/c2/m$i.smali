.class final Lc2/m$i;
.super Lc2/m$h;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc2/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "i"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lc2/m$h<",
        "Lc2/m$i;",
        ">;"
    }
.end annotation


# instance fields
.field private final j:Z

.field private final k:Lc2/m$d;

.field private final l:Z

.field private final m:Z

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:Z

.field private final s:Z

.field private final t:I

.field private final u:Z

.field private final v:Z

.field private final w:I


# direct methods
.method public constructor <init>(ILj1/x0;ILc2/m$d;IIZ)V
    .locals 4

    invoke-direct {p0, p1, p2, p3}, Lc2/m$h;-><init>(ILj1/x0;I)V

    iput-object p4, p0, Lc2/m$i;->k:Lc2/m$d;

    iget-boolean p1, p4, Lc2/m$d;->k0:Z

    if-eqz p1, :cond_0

    const/16 p1, 0x18

    goto :goto_0

    :cond_0
    const/16 p1, 0x10

    :goto_0
    iget-boolean p2, p4, Lc2/m$d;->j0:Z

    const/4 p3, 0x1

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    and-int p2, p6, p1

    if-eqz p2, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    iput-boolean p2, p0, Lc2/m$i;->s:Z

    const/high16 p2, -0x40800000    # -1.0f

    const/4 p6, -0x1

    if-eqz p7, :cond_6

    iget-object v1, p0, Lc2/m$h;->i:Lh0/r1;

    iget v2, v1, Lh0/r1;->v:I

    if-eq v2, p6, :cond_2

    iget v3, p4, Lc2/a0;->f:I

    if-gt v2, v3, :cond_6

    :cond_2
    iget v2, v1, Lh0/r1;->w:I

    if-eq v2, p6, :cond_3

    iget v3, p4, Lc2/a0;->g:I

    if-gt v2, v3, :cond_6

    :cond_3
    iget v2, v1, Lh0/r1;->x:F

    cmpl-float v3, v2, p2

    if-eqz v3, :cond_4

    iget v3, p4, Lc2/a0;->h:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_6

    :cond_4
    iget v1, v1, Lh0/r1;->m:I

    if-eq v1, p6, :cond_5

    iget v2, p4, Lc2/a0;->i:I

    if-gt v1, v2, :cond_6

    :cond_5
    const/4 v1, 0x1

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    :goto_2
    iput-boolean v1, p0, Lc2/m$i;->j:Z

    if-eqz p7, :cond_b

    iget-object p7, p0, Lc2/m$h;->i:Lh0/r1;

    iget v1, p7, Lh0/r1;->v:I

    if-eq v1, p6, :cond_7

    iget v2, p4, Lc2/a0;->j:I

    if-lt v1, v2, :cond_b

    :cond_7
    iget v1, p7, Lh0/r1;->w:I

    if-eq v1, p6, :cond_8

    iget v2, p4, Lc2/a0;->k:I

    if-lt v1, v2, :cond_b

    :cond_8
    iget v1, p7, Lh0/r1;->x:F

    cmpl-float p2, v1, p2

    if-eqz p2, :cond_9

    iget p2, p4, Lc2/a0;->l:I

    int-to-float p2, p2

    cmpl-float p2, v1, p2

    if-ltz p2, :cond_b

    :cond_9
    iget p2, p7, Lh0/r1;->m:I

    if-eq p2, p6, :cond_a

    iget p6, p4, Lc2/a0;->m:I

    if-lt p2, p6, :cond_b

    :cond_a
    const/4 p2, 0x1

    goto :goto_3

    :cond_b
    const/4 p2, 0x0

    :goto_3
    iput-boolean p2, p0, Lc2/m$i;->l:Z

    invoke-static {p5, v0}, Lc2/m;->I(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lc2/m$i;->m:Z

    iget-object p2, p0, Lc2/m$h;->i:Lh0/r1;

    iget p6, p2, Lh0/r1;->m:I

    iput p6, p0, Lc2/m$i;->n:I

    invoke-virtual {p2}, Lh0/r1;->f()I

    move-result p2

    iput p2, p0, Lc2/m$i;->o:I

    iget-object p2, p0, Lc2/m$h;->i:Lh0/r1;

    iget p2, p2, Lh0/r1;->j:I

    iget p6, p4, Lc2/a0;->r:I

    invoke-static {p2, p6}, Lc2/m;->t(II)I

    move-result p2

    iput p2, p0, Lc2/m$i;->q:I

    iget-object p2, p0, Lc2/m$h;->i:Lh0/r1;

    iget p2, p2, Lh0/r1;->j:I

    if-eqz p2, :cond_d

    and-int/2addr p2, p3

    if-eqz p2, :cond_c

    goto :goto_4

    :cond_c
    const/4 p2, 0x0

    goto :goto_5

    :cond_d
    :goto_4
    const/4 p2, 0x1

    :goto_5
    iput-boolean p2, p0, Lc2/m$i;->r:Z

    const p2, 0x7fffffff

    const/4 p6, 0x0

    :goto_6
    iget-object p7, p4, Lc2/a0;->q:Lp2/q;

    invoke-virtual {p7}, Ljava/util/AbstractCollection;->size()I

    move-result p7

    if-ge p6, p7, :cond_f

    iget-object p7, p0, Lc2/m$h;->i:Lh0/r1;

    iget-object p7, p7, Lh0/r1;->q:Ljava/lang/String;

    if-eqz p7, :cond_e

    iget-object v1, p4, Lc2/a0;->q:Lp2/q;

    invoke-interface {v1, p6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p7

    if-eqz p7, :cond_e

    move p2, p6

    goto :goto_7

    :cond_e
    add-int/lit8 p6, p6, 0x1

    goto :goto_6

    :cond_f
    :goto_7
    iput p2, p0, Lc2/m$i;->p:I

    invoke-static {p5}, Lh0/q3;->e(I)I

    move-result p2

    const/16 p4, 0x80

    if-ne p2, p4, :cond_10

    const/4 p2, 0x1

    goto :goto_8

    :cond_10
    const/4 p2, 0x0

    :goto_8
    iput-boolean p2, p0, Lc2/m$i;->u:Z

    invoke-static {p5}, Lh0/q3;->g(I)I

    move-result p2

    const/16 p4, 0x40

    if-ne p2, p4, :cond_11

    goto :goto_9

    :cond_11
    const/4 p3, 0x0

    :goto_9
    iput-boolean p3, p0, Lc2/m$i;->v:Z

    iget-object p2, p0, Lc2/m$h;->i:Lh0/r1;

    iget-object p2, p2, Lh0/r1;->q:Ljava/lang/String;

    invoke-static {p2}, Lc2/m;->u(Ljava/lang/String;)I

    move-result p2

    iput p2, p0, Lc2/m$i;->w:I

    invoke-direct {p0, p5, p1}, Lc2/m$i;->i(II)I

    move-result p1

    iput p1, p0, Lc2/m$i;->t:I

    return-void
.end method

.method public static synthetic c(Lc2/m$i;Lc2/m$i;)I
    .locals 0

    invoke-static {p0, p1}, Lc2/m$i;->e(Lc2/m$i;Lc2/m$i;)I

    move-result p0

    return p0
.end method

.method public static synthetic d(Lc2/m$i;Lc2/m$i;)I
    .locals 0

    invoke-static {p0, p1}, Lc2/m$i;->f(Lc2/m$i;Lc2/m$i;)I

    move-result p0

    return p0
.end method

.method private static e(Lc2/m$i;Lc2/m$i;)I
    .locals 4

    invoke-static {}, Lp2/k;->j()Lp2/k;

    move-result-object v0

    iget-boolean v1, p0, Lc2/m$i;->m:Z

    iget-boolean v2, p1, Lc2/m$i;->m:Z

    invoke-virtual {v0, v1, v2}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v0

    iget v1, p0, Lc2/m$i;->q:I

    iget v2, p1, Lc2/m$i;->q:I

    invoke-virtual {v0, v1, v2}, Lp2/k;->d(II)Lp2/k;

    move-result-object v0

    iget-boolean v1, p0, Lc2/m$i;->r:Z

    iget-boolean v2, p1, Lc2/m$i;->r:Z

    invoke-virtual {v0, v1, v2}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v0

    iget-boolean v1, p0, Lc2/m$i;->j:Z

    iget-boolean v2, p1, Lc2/m$i;->j:Z

    invoke-virtual {v0, v1, v2}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v0

    iget-boolean v1, p0, Lc2/m$i;->l:Z

    iget-boolean v2, p1, Lc2/m$i;->l:Z

    invoke-virtual {v0, v1, v2}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v0

    iget v1, p0, Lc2/m$i;->p:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lc2/m$i;->p:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {}, Lp2/h0;->b()Lp2/h0;

    move-result-object v3

    invoke-virtual {v3}, Lp2/h0;->d()Lp2/h0;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object v0

    iget-boolean v1, p0, Lc2/m$i;->u:Z

    iget-boolean v2, p1, Lc2/m$i;->u:Z

    invoke-virtual {v0, v1, v2}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v0

    iget-boolean v1, p0, Lc2/m$i;->v:Z

    iget-boolean v2, p1, Lc2/m$i;->v:Z

    invoke-virtual {v0, v1, v2}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v0

    iget-boolean v1, p0, Lc2/m$i;->u:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lc2/m$i;->v:Z

    if-eqz v1, :cond_0

    iget p0, p0, Lc2/m$i;->w:I

    iget p1, p1, Lc2/m$i;->w:I

    invoke-virtual {v0, p0, p1}, Lp2/k;->d(II)Lp2/k;

    move-result-object v0

    :cond_0
    invoke-virtual {v0}, Lp2/k;->i()I

    move-result p0

    return p0
.end method

.method private static f(Lc2/m$i;Lc2/m$i;)I
    .locals 5

    iget-boolean v0, p0, Lc2/m$i;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lc2/m$i;->m:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lc2/m;->v()Lp2/h0;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Lc2/m;->v()Lp2/h0;

    move-result-object v0

    invoke-virtual {v0}, Lp2/h0;->d()Lp2/h0;

    move-result-object v0

    :goto_0
    invoke-static {}, Lp2/k;->j()Lp2/k;

    move-result-object v1

    iget v2, p0, Lc2/m$i;->n:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lc2/m$i;->n:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lc2/m$i;->k:Lc2/m$d;

    iget-boolean v4, v4, Lc2/a0;->B:Z

    if-eqz v4, :cond_1

    invoke-static {}, Lc2/m;->v()Lp2/h0;

    move-result-object v4

    invoke-virtual {v4}, Lp2/h0;->d()Lp2/h0;

    move-result-object v4

    goto :goto_1

    :cond_1
    invoke-static {}, Lc2/m;->w()Lp2/h0;

    move-result-object v4

    :goto_1
    invoke-virtual {v1, v2, v3, v4}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object v1

    iget v2, p0, Lc2/m$i;->o:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lc2/m$i;->o:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object v1

    iget p0, p0, Lc2/m$i;->n:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    iget p1, p1, Lc2/m$i;->n:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p0, p1, v0}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object p0

    invoke-virtual {p0}, Lp2/k;->i()I

    move-result p0

    return p0
.end method

.method public static g(Ljava/util/List;Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lc2/m$i;",
            ">;",
            "Ljava/util/List<",
            "Lc2/m$i;",
            ">;)I"
        }
    .end annotation

    invoke-static {}, Lp2/k;->j()Lp2/k;

    move-result-object v0

    sget-object v1, Lc2/q;->f:Lc2/q;

    invoke-static {p0, v1}, Ljava/util/Collections;->max(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lc2/m$i;

    sget-object v2, Lc2/q;->f:Lc2/q;

    invoke-static {p1, v2}, Ljava/util/Collections;->max(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lc2/m$i;

    sget-object v3, Lc2/q;->f:Lc2/q;

    invoke-virtual {v0, v1, v2, v3}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object v0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lp2/k;->d(II)Lp2/k;

    move-result-object v0

    sget-object v1, Lc2/r;->f:Lc2/r;

    invoke-static {p0, v1}, Ljava/util/Collections;->max(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc2/m$i;

    sget-object v1, Lc2/r;->f:Lc2/r;

    invoke-static {p1, v1}, Ljava/util/Collections;->max(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lc2/m$i;

    sget-object v1, Lc2/r;->f:Lc2/r;

    invoke-virtual {v0, p0, p1, v1}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object p0

    invoke-virtual {p0}, Lp2/k;->i()I

    move-result p0

    return p0
.end method

.method public static h(ILj1/x0;Lc2/m$d;[II)Lp2/q;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lj1/x0;",
            "Lc2/m$d;",
            "[II)",
            "Lp2/q<",
            "Lc2/m$i;",
            ">;"
        }
    .end annotation

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    iget v0, v9, Lc2/a0;->n:I

    iget v1, v9, Lc2/a0;->o:I

    iget-boolean v2, v9, Lc2/a0;->p:Z

    invoke-static {v8, v0, v1, v2}, Lc2/m;->s(Lj1/x0;IIZ)I

    move-result v10

    invoke-static {}, Lp2/q;->k()Lp2/q$a;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_0
    iget v0, v8, Lj1/x0;->f:I

    if-ge v13, v0, :cond_2

    invoke-virtual {v8, v13}, Lj1/x0;->b(I)Lh0/r1;

    move-result-object v0

    invoke-virtual {v0}, Lh0/r1;->f()I

    move-result v0

    const v1, 0x7fffffff

    if-eq v10, v1, :cond_1

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    if-gt v0, v10, :cond_0

    goto :goto_1

    :cond_0
    const/4 v7, 0x0

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v0, 0x1

    const/4 v7, 0x1

    :goto_2
    new-instance v14, Lc2/m$i;

    aget v5, p3, v13

    move-object v0, v14

    move v1, p0

    move-object/from16 v2, p1

    move v3, v13

    move-object/from16 v4, p2

    move/from16 v6, p4

    invoke-direct/range {v0 .. v7}, Lc2/m$i;-><init>(ILj1/x0;ILc2/m$d;IIZ)V

    invoke-virtual {v11, v14}, Lp2/q$a;->f(Ljava/lang/Object;)Lp2/q$a;

    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v11}, Lp2/q$a;->h()Lp2/q;

    move-result-object v0

    return-object v0
.end method

.method private i(II)I
    .locals 2

    iget-object v0, p0, Lc2/m$h;->i:Lh0/r1;

    iget v0, v0, Lh0/r1;->j:I

    and-int/lit16 v0, v0, 0x4000

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lc2/m$i;->k:Lc2/m$d;

    iget-boolean v0, v0, Lc2/m$d;->s0:Z

    invoke-static {p1, v0}, Lc2/m;->I(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    iget-boolean v0, p0, Lc2/m$i;->j:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lc2/m$i;->k:Lc2/m$d;

    iget-boolean v0, v0, Lc2/m$d;->i0:Z

    if-nez v0, :cond_2

    return v1

    :cond_2
    invoke-static {p1, v1}, Lc2/m;->I(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lc2/m$i;->l:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lc2/m$i;->j:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lc2/m$h;->i:Lh0/r1;

    iget v0, v0, Lh0/r1;->m:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lc2/m$i;->k:Lc2/m$d;

    iget-boolean v1, v0, Lc2/a0;->C:Z

    if-nez v1, :cond_3

    iget-boolean v0, v0, Lc2/a0;->B:Z

    if-nez v0, :cond_3

    and-int/2addr p1, p2

    if-eqz p1, :cond_3

    const/4 p1, 0x2

    goto :goto_0

    :cond_3
    const/4 p1, 0x1

    :goto_0
    return p1
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lc2/m$i;->t:I

    return v0
.end method

.method public bridge synthetic b(Lc2/m$h;)Z
    .locals 0

    check-cast p1, Lc2/m$i;

    invoke-virtual {p0, p1}, Lc2/m$i;->j(Lc2/m$i;)Z

    move-result p1

    return p1
.end method

.method public j(Lc2/m$i;)Z
    .locals 2

    iget-boolean v0, p0, Lc2/m$i;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lc2/m$h;->i:Lh0/r1;

    iget-object v0, v0, Lh0/r1;->q:Ljava/lang/String;

    iget-object v1, p1, Lc2/m$h;->i:Lh0/r1;

    iget-object v1, v1, Lh0/r1;->q:Ljava/lang/String;

    invoke-static {v0, v1}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lc2/m$i;->k:Lc2/m$d;

    iget-boolean v0, v0, Lc2/m$d;->l0:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lc2/m$i;->u:Z

    iget-boolean v1, p1, Lc2/m$i;->u:Z

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lc2/m$i;->v:Z

    iget-boolean p1, p1, Lc2/m$i;->v:Z

    if-ne v0, p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
