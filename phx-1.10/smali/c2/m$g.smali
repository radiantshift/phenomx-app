.class final Lc2/m$g;
.super Lc2/m$h;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc2/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lc2/m$h<",
        "Lc2/m$g;",
        ">;",
        "Ljava/lang/Comparable<",
        "Lc2/m$g;",
        ">;"
    }
.end annotation


# instance fields
.field private final j:I

.field private final k:Z

.field private final l:Z

.field private final m:Z

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:Z


# direct methods
.method public constructor <init>(ILj1/x0;ILc2/m$d;ILjava/lang/String;)V
    .locals 5

    invoke-direct {p0, p1, p2, p3}, Lc2/m$h;-><init>(ILj1/x0;I)V

    const/4 p1, 0x0

    invoke-static {p5, p1}, Lc2/m;->I(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lc2/m$g;->k:Z

    iget-object p2, p0, Lc2/m$h;->i:Lh0/r1;

    iget p2, p2, Lh0/r1;->i:I

    iget p3, p4, Lc2/a0;->z:I

    not-int p3, p3

    and-int/2addr p2, p3

    and-int/lit8 p3, p2, 0x1

    const/4 v0, 0x1

    if-eqz p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    iput-boolean p3, p0, Lc2/m$g;->l:Z

    and-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    iput-boolean p2, p0, Lc2/m$g;->m:Z

    const p2, 0x7fffffff

    iget-object p3, p4, Lc2/a0;->x:Lp2/q;

    invoke-virtual {p3}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_2

    const-string p3, ""

    invoke-static {p3}, Lp2/q;->r(Ljava/lang/Object;)Lp2/q;

    move-result-object p3

    goto :goto_2

    :cond_2
    iget-object p3, p4, Lc2/a0;->x:Lp2/q;

    :goto_2
    const/4 v1, 0x0

    :goto_3
    invoke-virtual {p3}, Ljava/util/AbstractCollection;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    iget-object v2, p0, Lc2/m$h;->i:Lh0/r1;

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-boolean v4, p4, Lc2/a0;->A:Z

    invoke-static {v2, v3, v4}, Lc2/m;->B(Lh0/r1;Ljava/lang/String;Z)I

    move-result v2

    if-lez v2, :cond_3

    move p2, v1

    goto :goto_4

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_4
    iput p2, p0, Lc2/m$g;->n:I

    iput v2, p0, Lc2/m$g;->o:I

    iget-object p2, p0, Lc2/m$h;->i:Lh0/r1;

    iget p2, p2, Lh0/r1;->j:I

    iget p3, p4, Lc2/a0;->y:I

    invoke-static {p2, p3}, Lc2/m;->t(II)I

    move-result p2

    iput p2, p0, Lc2/m$g;->p:I

    iget-object p3, p0, Lc2/m$h;->i:Lh0/r1;

    iget p3, p3, Lh0/r1;->j:I

    and-int/lit16 p3, p3, 0x440

    if-eqz p3, :cond_5

    const/4 p3, 0x1

    goto :goto_5

    :cond_5
    const/4 p3, 0x0

    :goto_5
    iput-boolean p3, p0, Lc2/m$g;->r:Z

    invoke-static {p6}, Lc2/m;->Q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    if-nez p3, :cond_6

    const/4 p3, 0x1

    goto :goto_6

    :cond_6
    const/4 p3, 0x0

    :goto_6
    iget-object v1, p0, Lc2/m$h;->i:Lh0/r1;

    invoke-static {v1, p6, p3}, Lc2/m;->B(Lh0/r1;Ljava/lang/String;Z)I

    move-result p3

    iput p3, p0, Lc2/m$g;->q:I

    if-gtz v2, :cond_9

    iget-object p6, p4, Lc2/a0;->x:Lp2/q;

    invoke-virtual {p6}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result p6

    if-eqz p6, :cond_7

    if-gtz p2, :cond_9

    :cond_7
    iget-boolean p2, p0, Lc2/m$g;->l:Z

    if-nez p2, :cond_9

    iget-boolean p2, p0, Lc2/m$g;->m:Z

    if-eqz p2, :cond_8

    if-lez p3, :cond_8

    goto :goto_7

    :cond_8
    const/4 p2, 0x0

    goto :goto_8

    :cond_9
    :goto_7
    const/4 p2, 0x1

    :goto_8
    iget-boolean p3, p4, Lc2/m$d;->s0:Z

    invoke-static {p5, p3}, Lc2/m;->I(IZ)Z

    move-result p3

    if-eqz p3, :cond_a

    if-eqz p2, :cond_a

    const/4 p1, 0x1

    :cond_a
    iput p1, p0, Lc2/m$g;->j:I

    return-void
.end method

.method public static c(Ljava/util/List;Ljava/util/List;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lc2/m$g;",
            ">;",
            "Ljava/util/List<",
            "Lc2/m$g;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc2/m$g;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lc2/m$g;

    invoke-virtual {p0, p1}, Lc2/m$g;->d(Lc2/m$g;)I

    move-result p0

    return p0
.end method

.method public static e(ILj1/x0;Lc2/m$d;[ILjava/lang/String;)Lp2/q;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lj1/x0;",
            "Lc2/m$d;",
            "[I",
            "Ljava/lang/String;",
            ")",
            "Lp2/q<",
            "Lc2/m$g;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lp2/q;->k()Lp2/q$a;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    iget v2, p1, Lj1/x0;->f:I

    if-ge v1, v2, :cond_0

    new-instance v9, Lc2/m$g;

    aget v7, p3, v1

    move-object v2, v9

    move v3, p0

    move-object v4, p1

    move v5, v1

    move-object v6, p2

    move-object v8, p4

    invoke-direct/range {v2 .. v8}, Lc2/m$g;-><init>(ILj1/x0;ILc2/m$d;ILjava/lang/String;)V

    invoke-virtual {v0, v9}, Lp2/q$a;->f(Ljava/lang/Object;)Lp2/q$a;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lp2/q$a;->h()Lp2/q;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lc2/m$g;->j:I

    return v0
.end method

.method public bridge synthetic b(Lc2/m$h;)Z
    .locals 0

    check-cast p1, Lc2/m$g;

    invoke-virtual {p0, p1}, Lc2/m$g;->f(Lc2/m$g;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lc2/m$g;

    invoke-virtual {p0, p1}, Lc2/m$g;->d(Lc2/m$g;)I

    move-result p1

    return p1
.end method

.method public d(Lc2/m$g;)I
    .locals 4

    invoke-static {}, Lp2/k;->j()Lp2/k;

    move-result-object v0

    iget-boolean v1, p0, Lc2/m$g;->k:Z

    iget-boolean v2, p1, Lc2/m$g;->k:Z

    invoke-virtual {v0, v1, v2}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v0

    iget v1, p0, Lc2/m$g;->n:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lc2/m$g;->n:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {}, Lp2/h0;->b()Lp2/h0;

    move-result-object v3

    invoke-virtual {v3}, Lp2/h0;->d()Lp2/h0;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object v0

    iget v1, p0, Lc2/m$g;->o:I

    iget v2, p1, Lc2/m$g;->o:I

    invoke-virtual {v0, v1, v2}, Lp2/k;->d(II)Lp2/k;

    move-result-object v0

    iget v1, p0, Lc2/m$g;->p:I

    iget v2, p1, Lc2/m$g;->p:I

    invoke-virtual {v0, v1, v2}, Lp2/k;->d(II)Lp2/k;

    move-result-object v0

    iget-boolean v1, p0, Lc2/m$g;->l:Z

    iget-boolean v2, p1, Lc2/m$g;->l:Z

    invoke-virtual {v0, v1, v2}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v0

    iget-boolean v1, p0, Lc2/m$g;->m:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, Lc2/m$g;->m:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget v3, p0, Lc2/m$g;->o:I

    if-nez v3, :cond_0

    invoke-static {}, Lp2/h0;->b()Lp2/h0;

    move-result-object v3

    goto :goto_0

    :cond_0
    invoke-static {}, Lp2/h0;->b()Lp2/h0;

    move-result-object v3

    invoke-virtual {v3}, Lp2/h0;->d()Lp2/h0;

    move-result-object v3

    :goto_0
    invoke-virtual {v0, v1, v2, v3}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object v0

    iget v1, p0, Lc2/m$g;->q:I

    iget v2, p1, Lc2/m$g;->q:I

    invoke-virtual {v0, v1, v2}, Lp2/k;->d(II)Lp2/k;

    move-result-object v0

    iget v1, p0, Lc2/m$g;->p:I

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lc2/m$g;->r:Z

    iget-boolean p1, p1, Lc2/m$g;->r:Z

    invoke-virtual {v0, v1, p1}, Lp2/k;->h(ZZ)Lp2/k;

    move-result-object v0

    :cond_1
    invoke-virtual {v0}, Lp2/k;->i()I

    move-result p1

    return p1
.end method

.method public f(Lc2/m$g;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method
