.class public final Lc2/y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# static fields
.field private static final h:Ljava/lang/String;

.field private static final i:Ljava/lang/String;

.field public static final j:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Lc2/y;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final f:Lj1/x0;

.field public final g:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/y;->h:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/y;->i:Ljava/lang/String;

    sget-object v0, Lc2/x;->a:Lc2/x;

    sput-object v0, Lc2/y;->j:Lh0/h$a;

    return-void
.end method

.method public constructor <init>(Lj1/x0;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj1/x0;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p2}, Ljava/util/Collections;->min(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-static {p2}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p1, Lj1/x0;->f:I

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw p1

    :cond_1
    :goto_0
    iput-object p1, p0, Lc2/y;->f:Lj1/x0;

    invoke-static {p2}, Lp2/q;->m(Ljava/util/Collection;)Lp2/q;

    move-result-object p1

    iput-object p1, p0, Lc2/y;->g:Lp2/q;

    return-void
.end method

.method public static synthetic a(Landroid/os/Bundle;)Lc2/y;
    .locals 0

    invoke-static {p0}, Lc2/y;->c(Landroid/os/Bundle;)Lc2/y;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic c(Landroid/os/Bundle;)Lc2/y;
    .locals 2

    sget-object v0, Lc2/y;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    sget-object v1, Lj1/x0;->m:Lh0/h$a;

    invoke-interface {v1, v0}, Lh0/h$a;->a(Landroid/os/Bundle;)Lh0/h;

    move-result-object v0

    check-cast v0, Lj1/x0;

    sget-object v1, Lc2/y;->i:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object p0

    invoke-static {p0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [I

    new-instance v1, Lc2/y;

    invoke-static {p0}, Lr2/e;->c([I)Ljava/util/List;

    move-result-object p0

    invoke-direct {v1, v0, p0}, Lc2/y;-><init>(Lj1/x0;Ljava/util/List;)V

    return-object v1
.end method


# virtual methods
.method public b()I
    .locals 1

    iget-object v0, p0, Lc2/y;->f:Lj1/x0;

    iget v0, v0, Lj1/x0;->h:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const-class v2, Lc2/y;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    check-cast p1, Lc2/y;

    iget-object v2, p0, Lc2/y;->f:Lj1/x0;

    iget-object v3, p1, Lc2/y;->f:Lj1/x0;

    invoke-virtual {v2, v3}, Lj1/x0;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lc2/y;->g:Lp2/q;

    iget-object p1, p1, Lc2/y;->g:Lp2/q;

    invoke-virtual {v2, p1}, Lp2/q;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lc2/y;->f:Lj1/x0;

    invoke-virtual {v0}, Lj1/x0;->hashCode()I

    move-result v0

    iget-object v1, p0, Lc2/y;->g:Lp2/q;

    invoke-virtual {v1}, Lp2/q;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    return v0
.end method
