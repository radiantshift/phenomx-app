.class public Lc2/a0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc2/a0$a;
    }
.end annotation


# static fields
.field public static final F:Lc2/a0;

.field public static final G:Lc2/a0;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final H:Ljava/lang/String;

.field private static final I:Ljava/lang/String;

.field private static final J:Ljava/lang/String;

.field private static final K:Ljava/lang/String;

.field private static final L:Ljava/lang/String;

.field private static final M:Ljava/lang/String;

.field private static final N:Ljava/lang/String;

.field private static final O:Ljava/lang/String;

.field private static final P:Ljava/lang/String;

.field private static final Q:Ljava/lang/String;

.field private static final R:Ljava/lang/String;

.field private static final S:Ljava/lang/String;

.field private static final T:Ljava/lang/String;

.field private static final U:Ljava/lang/String;

.field private static final V:Ljava/lang/String;

.field private static final W:Ljava/lang/String;

.field private static final X:Ljava/lang/String;

.field private static final Y:Ljava/lang/String;

.field private static final Z:Ljava/lang/String;

.field private static final a0:Ljava/lang/String;

.field private static final b0:Ljava/lang/String;

.field private static final c0:Ljava/lang/String;

.field private static final d0:Ljava/lang/String;

.field private static final e0:Ljava/lang/String;

.field private static final f0:Ljava/lang/String;

.field private static final g0:Ljava/lang/String;

.field public static final h0:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Lc2/a0;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field public final A:Z

.field public final B:Z

.field public final C:Z

.field public final D:Lp2/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/r<",
            "Lj1/x0;",
            "Lc2/y;",
            ">;"
        }
    .end annotation
.end field

.field public final E:Lp2/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/s<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:Z

.field public final q:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final r:I

.field public final s:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final t:I

.field public final u:I

.field public final v:I

.field public final w:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final x:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final y:I

.field public final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lc2/a0$a;

    invoke-direct {v0}, Lc2/a0$a;-><init>()V

    invoke-virtual {v0}, Lc2/a0$a;->A()Lc2/a0;

    move-result-object v0

    sput-object v0, Lc2/a0;->F:Lc2/a0;

    sput-object v0, Lc2/a0;->G:Lc2/a0;

    const/4 v0, 0x1

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->H:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->I:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->J:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->K:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->L:Ljava/lang/String;

    const/4 v0, 0x6

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->M:Ljava/lang/String;

    const/4 v0, 0x7

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->N:Ljava/lang/String;

    const/16 v0, 0x8

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->O:Ljava/lang/String;

    const/16 v0, 0x9

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->P:Ljava/lang/String;

    const/16 v0, 0xa

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->Q:Ljava/lang/String;

    const/16 v0, 0xb

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->R:Ljava/lang/String;

    const/16 v0, 0xc

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->S:Ljava/lang/String;

    const/16 v0, 0xd

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->T:Ljava/lang/String;

    const/16 v0, 0xe

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->U:Ljava/lang/String;

    const/16 v0, 0xf

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->V:Ljava/lang/String;

    const/16 v0, 0x10

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->W:Ljava/lang/String;

    const/16 v0, 0x11

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->X:Ljava/lang/String;

    const/16 v0, 0x12

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->Y:Ljava/lang/String;

    const/16 v0, 0x13

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->Z:Ljava/lang/String;

    const/16 v0, 0x14

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->a0:Ljava/lang/String;

    const/16 v0, 0x15

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->b0:Ljava/lang/String;

    const/16 v0, 0x16

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->c0:Ljava/lang/String;

    const/16 v0, 0x17

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->d0:Ljava/lang/String;

    const/16 v0, 0x18

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->e0:Ljava/lang/String;

    const/16 v0, 0x19

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->f0:Ljava/lang/String;

    const/16 v0, 0x1a

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc2/a0;->g0:Ljava/lang/String;

    sget-object v0, Lc2/z;->a:Lc2/z;

    sput-object v0, Lc2/a0;->h0:Lh0/h$a;

    return-void
.end method

.method protected constructor <init>(Lc2/a0$a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lc2/a0$a;->a(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->f:I

    invoke-static {p1}, Lc2/a0$a;->b(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->g:I

    invoke-static {p1}, Lc2/a0$a;->c(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->h:I

    invoke-static {p1}, Lc2/a0$a;->d(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->i:I

    invoke-static {p1}, Lc2/a0$a;->e(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->j:I

    invoke-static {p1}, Lc2/a0$a;->f(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->k:I

    invoke-static {p1}, Lc2/a0$a;->g(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->l:I

    invoke-static {p1}, Lc2/a0$a;->h(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->m:I

    invoke-static {p1}, Lc2/a0$a;->i(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->n:I

    invoke-static {p1}, Lc2/a0$a;->j(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->o:I

    invoke-static {p1}, Lc2/a0$a;->k(Lc2/a0$a;)Z

    move-result v0

    iput-boolean v0, p0, Lc2/a0;->p:Z

    invoke-static {p1}, Lc2/a0$a;->l(Lc2/a0$a;)Lp2/q;

    move-result-object v0

    iput-object v0, p0, Lc2/a0;->q:Lp2/q;

    invoke-static {p1}, Lc2/a0$a;->m(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->r:I

    invoke-static {p1}, Lc2/a0$a;->n(Lc2/a0$a;)Lp2/q;

    move-result-object v0

    iput-object v0, p0, Lc2/a0;->s:Lp2/q;

    invoke-static {p1}, Lc2/a0$a;->o(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->t:I

    invoke-static {p1}, Lc2/a0$a;->p(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->u:I

    invoke-static {p1}, Lc2/a0$a;->q(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->v:I

    invoke-static {p1}, Lc2/a0$a;->r(Lc2/a0$a;)Lp2/q;

    move-result-object v0

    iput-object v0, p0, Lc2/a0;->w:Lp2/q;

    invoke-static {p1}, Lc2/a0$a;->s(Lc2/a0$a;)Lp2/q;

    move-result-object v0

    iput-object v0, p0, Lc2/a0;->x:Lp2/q;

    invoke-static {p1}, Lc2/a0$a;->t(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->y:I

    invoke-static {p1}, Lc2/a0$a;->u(Lc2/a0$a;)I

    move-result v0

    iput v0, p0, Lc2/a0;->z:I

    invoke-static {p1}, Lc2/a0$a;->v(Lc2/a0$a;)Z

    move-result v0

    iput-boolean v0, p0, Lc2/a0;->A:Z

    invoke-static {p1}, Lc2/a0$a;->w(Lc2/a0$a;)Z

    move-result v0

    iput-boolean v0, p0, Lc2/a0;->B:Z

    invoke-static {p1}, Lc2/a0$a;->x(Lc2/a0$a;)Z

    move-result v0

    iput-boolean v0, p0, Lc2/a0;->C:Z

    invoke-static {p1}, Lc2/a0$a;->y(Lc2/a0$a;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Lp2/r;->c(Ljava/util/Map;)Lp2/r;

    move-result-object v0

    iput-object v0, p0, Lc2/a0;->D:Lp2/r;

    invoke-static {p1}, Lc2/a0$a;->z(Lc2/a0$a;)Ljava/util/HashSet;

    move-result-object p1

    invoke-static {p1}, Lp2/s;->k(Ljava/util/Collection;)Lp2/s;

    move-result-object p1

    iput-object p1, p0, Lc2/a0;->E:Lp2/s;

    return-void
.end method

.method public static A(Landroid/os/Bundle;)Lc2/a0;
    .locals 1

    new-instance v0, Lc2/a0$a;

    invoke-direct {v0, p0}, Lc2/a0$a;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Lc2/a0$a;->A()Lc2/a0;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->M:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->N:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->W:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->X:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->f0:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->H:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->I:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->Y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->Z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->a0:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->J:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->K:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->O:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->g0:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->L:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->b0:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic q()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->c0:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->d0:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->e0:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->P:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic u()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->Q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic v()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->R:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic w()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->S:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic x()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->T:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic y()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->U:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic z()Ljava/lang/String;
    .locals 1

    sget-object v0, Lc2/a0;->V:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_1

    :cond_1
    check-cast p1, Lc2/a0;

    iget v2, p0, Lc2/a0;->f:I

    iget v3, p1, Lc2/a0;->f:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lc2/a0;->g:I

    iget v3, p1, Lc2/a0;->g:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lc2/a0;->h:I

    iget v3, p1, Lc2/a0;->h:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lc2/a0;->i:I

    iget v3, p1, Lc2/a0;->i:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lc2/a0;->j:I

    iget v3, p1, Lc2/a0;->j:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lc2/a0;->k:I

    iget v3, p1, Lc2/a0;->k:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lc2/a0;->l:I

    iget v3, p1, Lc2/a0;->l:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lc2/a0;->m:I

    iget v3, p1, Lc2/a0;->m:I

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lc2/a0;->p:Z

    iget-boolean v3, p1, Lc2/a0;->p:Z

    if-ne v2, v3, :cond_2

    iget v2, p0, Lc2/a0;->n:I

    iget v3, p1, Lc2/a0;->n:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lc2/a0;->o:I

    iget v3, p1, Lc2/a0;->o:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lc2/a0;->q:Lp2/q;

    iget-object v3, p1, Lc2/a0;->q:Lp2/q;

    invoke-virtual {v2, v3}, Lp2/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lc2/a0;->r:I

    iget v3, p1, Lc2/a0;->r:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lc2/a0;->s:Lp2/q;

    iget-object v3, p1, Lc2/a0;->s:Lp2/q;

    invoke-virtual {v2, v3}, Lp2/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lc2/a0;->t:I

    iget v3, p1, Lc2/a0;->t:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lc2/a0;->u:I

    iget v3, p1, Lc2/a0;->u:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lc2/a0;->v:I

    iget v3, p1, Lc2/a0;->v:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lc2/a0;->w:Lp2/q;

    iget-object v3, p1, Lc2/a0;->w:Lp2/q;

    invoke-virtual {v2, v3}, Lp2/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lc2/a0;->x:Lp2/q;

    iget-object v3, p1, Lc2/a0;->x:Lp2/q;

    invoke-virtual {v2, v3}, Lp2/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lc2/a0;->y:I

    iget v3, p1, Lc2/a0;->y:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lc2/a0;->z:I

    iget v3, p1, Lc2/a0;->z:I

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lc2/a0;->A:Z

    iget-boolean v3, p1, Lc2/a0;->A:Z

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lc2/a0;->B:Z

    iget-boolean v3, p1, Lc2/a0;->B:Z

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lc2/a0;->C:Z

    iget-boolean v3, p1, Lc2/a0;->C:Z

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lc2/a0;->D:Lp2/r;

    iget-object v3, p1, Lc2/a0;->D:Lp2/r;

    invoke-virtual {v2, v3}, Lp2/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lc2/a0;->E:Lp2/s;

    iget-object p1, p1, Lc2/a0;->E:Lp2/s;

    invoke-virtual {v2, p1}, Lp2/s;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lc2/a0;->f:I

    const/16 v1, 0x1f

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->g:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->h:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->i:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->j:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->k:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->l:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->m:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lc2/a0;->p:Z

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->n:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->o:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lc2/a0;->q:Lp2/q;

    invoke-virtual {v2}, Lp2/q;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->r:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lc2/a0;->s:Lp2/q;

    invoke-virtual {v2}, Lp2/q;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->t:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->u:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->v:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lc2/a0;->w:Lp2/q;

    invoke-virtual {v2}, Lp2/q;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lc2/a0;->x:Lp2/q;

    invoke-virtual {v2}, Lp2/q;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->y:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lc2/a0;->z:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lc2/a0;->A:Z

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lc2/a0;->B:Z

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lc2/a0;->C:Z

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lc2/a0;->D:Lp2/r;

    invoke-virtual {v2}, Lp2/r;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lc2/a0;->E:Lp2/s;

    invoke-virtual {v1}, Lp2/s;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
