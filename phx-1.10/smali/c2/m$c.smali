.class final Lc2/m$c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc2/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lc2/m$c;",
        ">;"
    }
.end annotation


# instance fields
.field private final f:Z

.field private final g:Z


# direct methods
.method public constructor <init>(Lh0/r1;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget p1, p1, Lh0/r1;->i:I

    const/4 v0, 0x1

    and-int/2addr p1, v0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lc2/m$c;->f:Z

    invoke-static {p2, v1}, Lc2/m;->I(IZ)Z

    move-result p1

    iput-boolean p1, p0, Lc2/m$c;->g:Z

    return-void
.end method


# virtual methods
.method public a(Lc2/m$c;)I
    .locals 3

    invoke-static {}, Lp2/k;->j()Lp2/k;

    move-result-object v0

    iget-boolean v1, p0, Lc2/m$c;->g:Z

    iget-boolean v2, p1, Lc2/m$c;->g:Z

    invoke-virtual {v0, v1, v2}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v0

    iget-boolean v1, p0, Lc2/m$c;->f:Z

    iget-boolean p1, p1, Lc2/m$c;->f:Z

    invoke-virtual {v0, v1, p1}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object p1

    invoke-virtual {p1}, Lp2/k;->i()I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lc2/m$c;

    invoke-virtual {p0, p1}, Lc2/m$c;->a(Lc2/m$c;)I

    move-result p1

    return p1
.end method
