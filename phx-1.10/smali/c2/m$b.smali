.class final Lc2/m$b;
.super Lc2/m$h;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc2/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lc2/m$h<",
        "Lc2/m$b;",
        ">;",
        "Ljava/lang/Comparable<",
        "Lc2/m$b;",
        ">;"
    }
.end annotation


# instance fields
.field private final A:Z

.field private final j:I

.field private final k:Z

.field private final l:Ljava/lang/String;

.field private final m:Lc2/m$d;

.field private final n:Z

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:Z

.field private final s:I

.field private final t:I

.field private final u:Z

.field private final v:I

.field private final w:I

.field private final x:I

.field private final y:I

.field private final z:Z


# direct methods
.method public constructor <init>(ILj1/x0;ILc2/m$d;IZLo2/l;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lj1/x0;",
            "I",
            "Lc2/m$d;",
            "IZ",
            "Lo2/l<",
            "Lh0/r1;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lc2/m$h;-><init>(ILj1/x0;I)V

    iput-object p4, p0, Lc2/m$b;->m:Lc2/m$d;

    iget-object p1, p0, Lc2/m$h;->i:Lh0/r1;

    iget-object p1, p1, Lh0/r1;->h:Ljava/lang/String;

    invoke-static {p1}, Lc2/m;->Q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lc2/m$b;->l:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-static {p5, p1}, Lc2/m;->I(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lc2/m$b;->n:Z

    const/4 p2, 0x0

    :goto_0
    iget-object p3, p4, Lc2/a0;->s:Lp2/q;

    invoke-virtual {p3}, Ljava/util/AbstractCollection;->size()I

    move-result p3

    const v0, 0x7fffffff

    if-ge p2, p3, :cond_1

    iget-object p3, p0, Lc2/m$h;->i:Lh0/r1;

    iget-object v1, p4, Lc2/a0;->s:Lp2/q;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p3, v1, p1}, Lc2/m;->B(Lh0/r1;Ljava/lang/String;Z)I

    move-result p3

    if-lez p3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    const p2, 0x7fffffff

    const/4 p3, 0x0

    :goto_1
    iput p2, p0, Lc2/m$b;->p:I

    iput p3, p0, Lc2/m$b;->o:I

    iget-object p2, p0, Lc2/m$h;->i:Lh0/r1;

    iget p2, p2, Lh0/r1;->j:I

    iget p3, p4, Lc2/a0;->t:I

    invoke-static {p2, p3}, Lc2/m;->t(II)I

    move-result p2

    iput p2, p0, Lc2/m$b;->q:I

    iget-object p2, p0, Lc2/m$h;->i:Lh0/r1;

    iget p3, p2, Lh0/r1;->j:I

    const/4 v1, 0x1

    if-eqz p3, :cond_3

    and-int/2addr p3, v1

    if-eqz p3, :cond_2

    goto :goto_2

    :cond_2
    const/4 p3, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 p3, 0x1

    :goto_3
    iput-boolean p3, p0, Lc2/m$b;->r:Z

    iget p3, p2, Lh0/r1;->i:I

    and-int/2addr p3, v1

    if-eqz p3, :cond_4

    const/4 p3, 0x1

    goto :goto_4

    :cond_4
    const/4 p3, 0x0

    :goto_4
    iput-boolean p3, p0, Lc2/m$b;->u:Z

    iget p3, p2, Lh0/r1;->D:I

    iput p3, p0, Lc2/m$b;->v:I

    iget v2, p2, Lh0/r1;->E:I

    iput v2, p0, Lc2/m$b;->w:I

    iget v2, p2, Lh0/r1;->m:I

    iput v2, p0, Lc2/m$b;->x:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_5

    iget v4, p4, Lc2/a0;->v:I

    if-gt v2, v4, :cond_7

    :cond_5
    if-eq p3, v3, :cond_6

    iget v2, p4, Lc2/a0;->u:I

    if-gt p3, v2, :cond_7

    :cond_6
    invoke-interface {p7, p2}, Lo2/l;->apply(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_7

    const/4 p2, 0x1

    goto :goto_5

    :cond_7
    const/4 p2, 0x0

    :goto_5
    iput-boolean p2, p0, Lc2/m$b;->k:Z

    invoke-static {}, Le2/n0;->g0()[Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    :goto_6
    array-length p7, p2

    if-ge p3, p7, :cond_9

    iget-object p7, p0, Lc2/m$h;->i:Lh0/r1;

    aget-object v2, p2, p3

    invoke-static {p7, v2, p1}, Lc2/m;->B(Lh0/r1;Ljava/lang/String;Z)I

    move-result p7

    if-lez p7, :cond_8

    goto :goto_7

    :cond_8
    add-int/lit8 p3, p3, 0x1

    goto :goto_6

    :cond_9
    const p3, 0x7fffffff

    const/4 p7, 0x0

    :goto_7
    iput p3, p0, Lc2/m$b;->s:I

    iput p7, p0, Lc2/m$b;->t:I

    const/4 p2, 0x0

    :goto_8
    iget-object p3, p4, Lc2/a0;->w:Lp2/q;

    invoke-virtual {p3}, Ljava/util/AbstractCollection;->size()I

    move-result p3

    if-ge p2, p3, :cond_b

    iget-object p3, p0, Lc2/m$h;->i:Lh0/r1;

    iget-object p3, p3, Lh0/r1;->q:Ljava/lang/String;

    if-eqz p3, :cond_a

    iget-object p7, p4, Lc2/a0;->w:Lp2/q;

    invoke-interface {p7, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p7

    invoke-virtual {p3, p7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_a

    move v0, p2

    goto :goto_9

    :cond_a
    add-int/lit8 p2, p2, 0x1

    goto :goto_8

    :cond_b
    :goto_9
    iput v0, p0, Lc2/m$b;->y:I

    invoke-static {p5}, Lh0/q3;->e(I)I

    move-result p2

    const/16 p3, 0x80

    if-ne p2, p3, :cond_c

    const/4 p2, 0x1

    goto :goto_a

    :cond_c
    const/4 p2, 0x0

    :goto_a
    iput-boolean p2, p0, Lc2/m$b;->z:Z

    invoke-static {p5}, Lh0/q3;->g(I)I

    move-result p2

    const/16 p3, 0x40

    if-ne p2, p3, :cond_d

    const/4 p1, 0x1

    :cond_d
    iput-boolean p1, p0, Lc2/m$b;->A:Z

    invoke-direct {p0, p5, p6}, Lc2/m$b;->f(IZ)I

    move-result p1

    iput p1, p0, Lc2/m$b;->j:I

    return-void
.end method

.method public static c(Ljava/util/List;Ljava/util/List;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lc2/m$b;",
            ">;",
            "Ljava/util/List<",
            "Lc2/m$b;",
            ">;)I"
        }
    .end annotation

    invoke-static {p0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lc2/m$b;

    invoke-static {p1}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lc2/m$b;

    invoke-virtual {p0, p1}, Lc2/m$b;->d(Lc2/m$b;)I

    move-result p0

    return p0
.end method

.method public static e(ILj1/x0;Lc2/m$d;[IZLo2/l;)Lp2/q;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lj1/x0;",
            "Lc2/m$d;",
            "[IZ",
            "Lo2/l<",
            "Lh0/r1;",
            ">;)",
            "Lp2/q<",
            "Lc2/m$b;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lp2/q;->k()Lp2/q$a;

    move-result-object v0

    const/4 v1, 0x0

    move-object v10, p1

    :goto_0
    iget v2, v10, Lj1/x0;->f:I

    if-ge v1, v2, :cond_0

    new-instance v11, Lc2/m$b;

    aget v7, p3, v1

    move-object v2, v11

    move v3, p0

    move-object v4, p1

    move v5, v1

    move-object v6, p2

    move/from16 v8, p4

    move-object/from16 v9, p5

    invoke-direct/range {v2 .. v9}, Lc2/m$b;-><init>(ILj1/x0;ILc2/m$d;IZLo2/l;)V

    invoke-virtual {v0, v11}, Lp2/q$a;->f(Ljava/lang/Object;)Lp2/q$a;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lp2/q$a;->h()Lp2/q;

    move-result-object v0

    return-object v0
.end method

.method private f(IZ)I
    .locals 2

    iget-object v0, p0, Lc2/m$b;->m:Lc2/m$d;

    iget-boolean v0, v0, Lc2/m$d;->s0:Z

    invoke-static {p1, v0}, Lc2/m;->I(IZ)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-boolean v0, p0, Lc2/m$b;->k:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lc2/m$b;->m:Lc2/m$d;

    iget-boolean v0, v0, Lc2/m$d;->m0:Z

    if-nez v0, :cond_1

    return v1

    :cond_1
    invoke-static {p1, v1}, Lc2/m;->I(IZ)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-boolean p1, p0, Lc2/m$b;->k:Z

    if-eqz p1, :cond_3

    iget-object p1, p0, Lc2/m$h;->i:Lh0/r1;

    iget p1, p1, Lh0/r1;->m:I

    const/4 v0, -0x1

    if-eq p1, v0, :cond_3

    iget-object p1, p0, Lc2/m$b;->m:Lc2/m$d;

    iget-boolean v0, p1, Lc2/a0;->C:Z

    if-nez v0, :cond_3

    iget-boolean v0, p1, Lc2/a0;->B:Z

    if-nez v0, :cond_3

    iget-boolean p1, p1, Lc2/m$d;->u0:Z

    if-nez p1, :cond_2

    if-nez p2, :cond_3

    :cond_2
    const/4 p1, 0x2

    goto :goto_0

    :cond_3
    const/4 p1, 0x1

    :goto_0
    return p1
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lc2/m$b;->j:I

    return v0
.end method

.method public bridge synthetic b(Lc2/m$h;)Z
    .locals 0

    check-cast p1, Lc2/m$b;

    invoke-virtual {p0, p1}, Lc2/m$b;->g(Lc2/m$b;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lc2/m$b;

    invoke-virtual {p0, p1}, Lc2/m$b;->d(Lc2/m$b;)I

    move-result p1

    return p1
.end method

.method public d(Lc2/m$b;)I
    .locals 5

    iget-boolean v0, p0, Lc2/m$b;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lc2/m$b;->n:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lc2/m;->v()Lp2/h0;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Lc2/m;->v()Lp2/h0;

    move-result-object v0

    invoke-virtual {v0}, Lp2/h0;->d()Lp2/h0;

    move-result-object v0

    :goto_0
    invoke-static {}, Lp2/k;->j()Lp2/k;

    move-result-object v1

    iget-boolean v2, p0, Lc2/m$b;->n:Z

    iget-boolean v3, p1, Lc2/m$b;->n:Z

    invoke-virtual {v1, v2, v3}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v1

    iget v2, p0, Lc2/m$b;->p:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lc2/m$b;->p:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {}, Lp2/h0;->b()Lp2/h0;

    move-result-object v4

    invoke-virtual {v4}, Lp2/h0;->d()Lp2/h0;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object v1

    iget v2, p0, Lc2/m$b;->o:I

    iget v3, p1, Lc2/m$b;->o:I

    invoke-virtual {v1, v2, v3}, Lp2/k;->d(II)Lp2/k;

    move-result-object v1

    iget v2, p0, Lc2/m$b;->q:I

    iget v3, p1, Lc2/m$b;->q:I

    invoke-virtual {v1, v2, v3}, Lp2/k;->d(II)Lp2/k;

    move-result-object v1

    iget-boolean v2, p0, Lc2/m$b;->u:Z

    iget-boolean v3, p1, Lc2/m$b;->u:Z

    invoke-virtual {v1, v2, v3}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v1

    iget-boolean v2, p0, Lc2/m$b;->r:Z

    iget-boolean v3, p1, Lc2/m$b;->r:Z

    invoke-virtual {v1, v2, v3}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v1

    iget v2, p0, Lc2/m$b;->s:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lc2/m$b;->s:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {}, Lp2/h0;->b()Lp2/h0;

    move-result-object v4

    invoke-virtual {v4}, Lp2/h0;->d()Lp2/h0;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object v1

    iget v2, p0, Lc2/m$b;->t:I

    iget v3, p1, Lc2/m$b;->t:I

    invoke-virtual {v1, v2, v3}, Lp2/k;->d(II)Lp2/k;

    move-result-object v1

    iget-boolean v2, p0, Lc2/m$b;->k:Z

    iget-boolean v3, p1, Lc2/m$b;->k:Z

    invoke-virtual {v1, v2, v3}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v1

    iget v2, p0, Lc2/m$b;->y:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lc2/m$b;->y:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {}, Lp2/h0;->b()Lp2/h0;

    move-result-object v4

    invoke-virtual {v4}, Lp2/h0;->d()Lp2/h0;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object v1

    iget v2, p0, Lc2/m$b;->x:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lc2/m$b;->x:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lc2/m$b;->m:Lc2/m$d;

    iget-boolean v4, v4, Lc2/a0;->B:Z

    if-eqz v4, :cond_1

    invoke-static {}, Lc2/m;->v()Lp2/h0;

    move-result-object v4

    invoke-virtual {v4}, Lp2/h0;->d()Lp2/h0;

    move-result-object v4

    goto :goto_1

    :cond_1
    invoke-static {}, Lc2/m;->w()Lp2/h0;

    move-result-object v4

    :goto_1
    invoke-virtual {v1, v2, v3, v4}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object v1

    iget-boolean v2, p0, Lc2/m$b;->z:Z

    iget-boolean v3, p1, Lc2/m$b;->z:Z

    invoke-virtual {v1, v2, v3}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v1

    iget-boolean v2, p0, Lc2/m$b;->A:Z

    iget-boolean v3, p1, Lc2/m$b;->A:Z

    invoke-virtual {v1, v2, v3}, Lp2/k;->g(ZZ)Lp2/k;

    move-result-object v1

    iget v2, p0, Lc2/m$b;->v:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lc2/m$b;->v:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object v1

    iget v2, p0, Lc2/m$b;->w:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lc2/m$b;->w:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object v1

    iget v2, p0, Lc2/m$b;->x:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lc2/m$b;->x:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lc2/m$b;->l:Ljava/lang/String;

    iget-object p1, p1, Lc2/m$b;->l:Ljava/lang/String;

    invoke-static {v4, p1}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    invoke-static {}, Lc2/m;->w()Lp2/h0;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v2, v3, v0}, Lp2/k;->f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;

    move-result-object p1

    invoke-virtual {p1}, Lp2/k;->i()I

    move-result p1

    return p1
.end method

.method public g(Lc2/m$b;)Z
    .locals 4

    iget-object v0, p0, Lc2/m$b;->m:Lc2/m$d;

    iget-boolean v1, v0, Lc2/m$d;->p0:Z

    const/4 v2, -0x1

    if-nez v1, :cond_0

    iget-object v1, p0, Lc2/m$h;->i:Lh0/r1;

    iget v1, v1, Lh0/r1;->D:I

    if-eq v1, v2, :cond_3

    iget-object v3, p1, Lc2/m$h;->i:Lh0/r1;

    iget v3, v3, Lh0/r1;->D:I

    if-ne v1, v3, :cond_3

    :cond_0
    iget-boolean v0, v0, Lc2/m$d;->n0:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lc2/m$h;->i:Lh0/r1;

    iget-object v0, v0, Lh0/r1;->q:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v1, p1, Lc2/m$h;->i:Lh0/r1;

    iget-object v1, v1, Lh0/r1;->q:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lc2/m$b;->m:Lc2/m$d;

    iget-boolean v1, v0, Lc2/m$d;->o0:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lc2/m$h;->i:Lh0/r1;

    iget v1, v1, Lh0/r1;->E:I

    if-eq v1, v2, :cond_3

    iget-object v2, p1, Lc2/m$h;->i:Lh0/r1;

    iget v2, v2, Lh0/r1;->E:I

    if-ne v1, v2, :cond_3

    :cond_2
    iget-boolean v0, v0, Lc2/m$d;->q0:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lc2/m$b;->z:Z

    iget-boolean v1, p1, Lc2/m$b;->z:Z

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, Lc2/m$b;->A:Z

    iget-boolean p1, p1, Lc2/m$b;->A:Z

    if-ne v0, p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
