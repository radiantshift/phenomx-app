.class Lf4/b$a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf4/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf4/b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroidx/media/a;

.field private d:Landroid/content/BroadcastReceiver;

.field private e:Landroid/content/BroadcastReceiver;

.field private f:Landroid/content/Context;

.field private g:Landroid/media/AudioManager;

.field private h:Ljava/lang/Object;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/media/AudioDeviceInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lf4/b$a;->a:Landroid/os/Handler;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf4/b$a;->b:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf4/b$a;->i:Ljava/util/List;

    iput-object p1, p0, Lf4/b$a;->f:Landroid/content/Context;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/media/AudioManager;

    iput-object p1, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x17

    if-lt p1, v0, :cond_0

    invoke-direct {p0}, Lf4/b$a;->u0()V

    :cond_0
    return-void
.end method

.method static synthetic A(Lf4/b$a;I)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->Q0(I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private A0()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic B(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->m0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private B0()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic C(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->B0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private C0()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic D(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->g0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private D0(I)Ljava/lang/Object;
    .locals 1

    const/16 v0, 0x17

    invoke-static {v0}, Lf4/b;->g(I)V

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method static synthetic E(Lf4/b$a;Ljava/lang/String;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->R0(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private E0()Ljava/lang/Object;
    .locals 1

    const/16 v0, 0x15

    invoke-static {v0}, Lf4/b;->g(I)V

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isVolumeFixed()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic F(Lf4/b$a;Ljava/lang/String;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->n0(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private synthetic F0(I)V
    .locals 2

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lf4/b$a;->b()Z

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "onAudioFocusChanged"

    invoke-direct {p0, p1, v0}, Lf4/b$a;->v0(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic G(Lf4/b$a;ILjava/lang/Double;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1, p2}, Lf4/b$a;->H0(ILjava/lang/Double;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private G0()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->loadSoundEffects()V

    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic H(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->G0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private H0(ILjava/lang/Double;)Ljava/lang/Object;
    .locals 3

    if-eqz p2, :cond_0

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    double-to-float p2, v1

    invoke-virtual {v0, p1, p2}, Landroid/media/AudioManager;->playSoundEffect(IF)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {p2, p1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic I(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->X0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private I0()V
    .locals 4

    iget-object v0, p0, Lf4/b$a;->d:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lf4/b$a$b;

    invoke-direct {v0, p0}, Lf4/b$a$b;-><init>(Lf4/b$a;)V

    iput-object v0, p0, Lf4/b$a;->d:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lf4/b$a;->f:Landroid/content/Context;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.media.AUDIO_BECOMING_NOISY"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method static synthetic J(Lf4/b$a;Ljava/lang/String;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->o0(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private J0()V
    .locals 4

    iget-object v0, p0, Lf4/b$a;->e:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lf4/b$a$c;

    invoke-direct {v0, p0}, Lf4/b$a$c;-><init>(Lf4/b$a;)V

    iput-object v0, p0, Lf4/b$a;->e:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lf4/b$a;->f:Landroid/content/Context;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method static synthetic K(Lf4/b$a;III)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lf4/b$a;->W(III)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic L(Lf4/b$a;I)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->k0(I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private L0(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "*>;)Z"
        }
    .end annotation

    iget-object v0, p0, Lf4/b$a;->c:Landroidx/media/a;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    new-instance v2, Landroidx/media/a$b;

    const-string v3, "gainType"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v2, v3}, Landroidx/media/a$b;-><init>(I)V

    new-instance v3, Lf4/a;

    invoke-direct {v3, p0}, Lf4/a;-><init>(Lf4/b$a;)V

    invoke-virtual {v2, v3}, Landroidx/media/a$b;->e(Landroid/media/AudioManager$OnAudioFocusChangeListener;)Landroidx/media/a$b;

    const-string v3, "audioAttributes"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    invoke-direct {p0, v3}, Lf4/b$a;->a0(Ljava/util/Map;)Landroidx/media/AudioAttributesCompat;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroidx/media/a$b;->c(Landroidx/media/AudioAttributesCompat;)Landroidx/media/a$b;

    :cond_1
    const-string v3, "willPauseWhenDucked"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v2, p1}, Landroidx/media/a$b;->g(Z)Landroidx/media/a$b;

    :cond_2
    invoke-virtual {v2}, Landroidx/media/a$b;->a()Landroidx/media/a;

    move-result-object p1

    iput-object p1, p0, Lf4/b$a;->c:Landroidx/media/a;

    iget-object v2, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-static {v2, p1}, Landroidx/media/b;->b(Landroid/media/AudioManager;Landroidx/media/a;)I

    move-result p1

    if-ne p1, v1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_4

    invoke-direct {p0}, Lf4/b$a;->I0()V

    invoke-direct {p0}, Lf4/b$a;->J0()V

    :cond_4
    return v1
.end method

.method static synthetic M(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->l0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private M0(I)Ljava/lang/Object;
    .locals 1

    const/16 v0, 0x1d

    invoke-static {v0}, Lf4/b;->g(I)V

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setAllowedCapturePolicy(I)V

    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic N(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->z0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private N0(Z)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic O([Landroid/media/AudioDeviceInfo;)Ljava/util/List;
    .locals 0

    invoke-static {p0}, Lf4/b$a;->f0([Landroid/media/AudioDeviceInfo;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private O0(Ljava/lang/Integer;)Z
    .locals 4

    const/16 v0, 0x1f

    invoke-static {v0}, Lf4/b;->g(I)V

    iget-object v0, p0, Lf4/b$a;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioDeviceInfo;

    invoke-virtual {v1}, Landroid/media/AudioDeviceInfo;->getId()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v2, v3, :cond_0

    iget-object p1, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {p1, v1}, Landroid/media/AudioManager;->setCommunicationDevice(Landroid/media/AudioDeviceInfo;)Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method static synthetic P(Lf4/b$a;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lf4/b$a;->v0(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private P0(Z)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic Q(Lf4/b$a;II)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1, p2}, Lf4/b$a;->Y(II)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private Q0(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setMode(I)V

    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic R(Lf4/b$a;III)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lf4/b$a;->X(III)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private R0(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic S(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->p0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private S0(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setRingerMode(I)V

    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic T(Lf4/b$a;I)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->q0(I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private T0(Z)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic U(Lf4/b$a;I)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->r0(I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private U0(III)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    const/4 p1, 0x0

    return-object p1
.end method

.method private V0()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->startBluetoothSco()V

    const/4 v0, 0x0

    return-object v0
.end method

.method private W(III)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    const/4 p1, 0x0

    return-object p1
.end method

.method private W0()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    const/4 v0, 0x0

    return-object v0
.end method

.method private X(III)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/AudioManager;->adjustSuggestedStreamVolume(III)V

    const/4 p1, 0x0

    return-object p1
.end method

.method private X0()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->unloadSoundEffects()V

    const/4 v0, 0x0

    return-object v0
.end method

.method private Y(II)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1, p2}, Landroid/media/AudioManager;->adjustVolume(II)V

    const/4 p1, 0x0

    return-object p1
.end method

.method private Y0()V
    .locals 2

    iget-object v0, p0, Lf4/b$a;->d:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lf4/b$a;->f:Landroid/content/Context;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lf4/b$a;->d:Landroid/content/BroadcastReceiver;

    :cond_1
    :goto_0
    return-void
.end method

.method private Z()Ljava/lang/Object;
    .locals 1

    const/16 v0, 0x1f

    invoke-static {v0}, Lf4/b;->g(I)V

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->clearCommunicationDevice()V

    const/4 v0, 0x0

    return-object v0
.end method

.method private Z0()V
    .locals 2

    iget-object v0, p0, Lf4/b$a;->e:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lf4/b$a;->f:Landroid/content/Context;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lf4/b$a;->e:Landroid/content/BroadcastReceiver;

    :cond_1
    :goto_0
    return-void
.end method

.method public static synthetic a(Lf4/b$a;I)V
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->F0(I)V

    return-void
.end method

.method private a0(Ljava/util/Map;)Landroidx/media/AudioAttributesCompat;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "**>;)",
            "Landroidx/media/AudioAttributesCompat;"
        }
    .end annotation

    new-instance v0, Landroidx/media/AudioAttributesCompat$a;

    invoke-direct {v0}, Landroidx/media/AudioAttributesCompat$a;-><init>()V

    const-string v1, "contentType"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/media/AudioAttributesCompat$a;->b(I)Landroidx/media/AudioAttributesCompat$a;

    :cond_0
    const-string v1, "flags"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/media/AudioAttributesCompat$a;->c(I)Landroidx/media/AudioAttributesCompat$a;

    :cond_1
    const-string v1, "usage"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Landroidx/media/AudioAttributesCompat$a;->e(I)Landroidx/media/AudioAttributesCompat$a;

    :cond_2
    invoke-virtual {v0}, Landroidx/media/AudioAttributesCompat$a;->a()Landroidx/media/AudioAttributesCompat;

    move-result-object p1

    return-object p1
.end method

.method private b()Z
    .locals 4

    iget-object v0, p0, Lf4/b$a;->f:Landroid/content/Context;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-direct {p0}, Lf4/b$a;->Y0()V

    invoke-direct {p0}, Lf4/b$a;->Z0()V

    iget-object v0, p0, Lf4/b$a;->c:Landroidx/media/a;

    const/4 v2, 0x1

    if-nez v0, :cond_1

    return v2

    :cond_1
    iget-object v3, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-static {v3, v0}, Landroidx/media/b;->a(Landroid/media/AudioManager;Landroidx/media/a;)I

    move-result v0

    const/4 v3, 0x0

    iput-object v3, p0, Lf4/b$a;->c:Landroidx/media/a;

    if-ne v0, v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method private b0(Ljava/util/Map;)Ljava/lang/Object;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "**>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    const/16 v0, 0x13

    invoke-static {v0}, Lf4/b;->g(I)V

    new-instance v0, Landroid/view/KeyEvent;

    const-string v1, "downTime"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lf4/b;->d(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-string v1, "eventTime"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lf4/b;->d(Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v1, "action"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const-string v1, "code"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const-string v1, "repeat"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const-string v1, "metaState"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    const-string v1, "deviceId"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const-string v1, "scancode"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v11

    const-string v1, "flags"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v12

    const-string v1, "source"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object v1, v0

    invoke-direct/range {v1 .. v13}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    iget-object p1, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {p1, v0}, Landroid/media/AudioManager;->dispatchMediaKeyEvent(Landroid/view/KeyEvent;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic c(Lf4/b$a;Ljava/util/List;)Z
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->L0(Ljava/util/List;)Z

    move-result p0

    return p0
.end method

.method static synthetic d(Lf4/b$a;)Z
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->b()Z

    move-result p0

    return p0
.end method

.method private d0()V
    .locals 2

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    iget-object v1, p0, Lf4/b$a;->h:Ljava/lang/Object;

    check-cast v1, Landroid/media/AudioDeviceCallback;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterAudioDeviceCallback(Landroid/media/AudioDeviceCallback;)V

    return-void
.end method

.method static synthetic e(Lf4/b$a;I)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->s0(I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private static e0(Landroid/media/AudioDeviceInfo;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/AudioDeviceInfo;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "id"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Landroid/media/AudioDeviceInfo;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const/4 v1, 0x2

    const-string v2, "productName"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Landroid/media/AudioDeviceInfo;->getProductName()Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const/4 v1, 0x4

    const-string v2, "address"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Landroid/media/AudioDeviceInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const/4 v1, 0x6

    const-string v2, "isSource"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Landroid/media/AudioDeviceInfo;->isSource()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const/16 v1, 0x8

    const-string v2, "isSink"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Landroid/media/AudioDeviceInfo;->isSink()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const/16 v1, 0xa

    const-string v2, "sampleRates"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Landroid/media/AudioDeviceInfo;->getSampleRates()[I

    move-result-object v1

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const/16 v1, 0xc

    const-string v2, "channelMasks"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Landroid/media/AudioDeviceInfo;->getChannelMasks()[I

    move-result-object v1

    const/16 v2, 0xd

    aput-object v1, v0, v2

    const/16 v1, 0xe

    const-string v2, "channelIndexMasks"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Landroid/media/AudioDeviceInfo;->getChannelIndexMasks()[I

    move-result-object v1

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const/16 v1, 0x10

    const-string v2, "channelCounts"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Landroid/media/AudioDeviceInfo;->getChannelCounts()[I

    move-result-object v1

    const/16 v2, 0x11

    aput-object v1, v0, v2

    const/16 v1, 0x12

    const-string v2, "encodings"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Landroid/media/AudioDeviceInfo;->getEncodings()[I

    move-result-object v1

    const/16 v2, 0x13

    aput-object v1, v0, v2

    const/16 v1, 0x14

    const-string v2, "type"

    aput-object v2, v0, v1

    invoke-virtual {p0}, Landroid/media/AudioDeviceInfo;->getType()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const/16 v1, 0x15

    aput-object p0, v0, v1

    invoke-static {v0}, Lf4/b;->f([Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lf4/b$a;III)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lf4/b$a;->t0(III)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private static f0([Landroid/media/AudioDeviceInfo;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/media/AudioDeviceInfo;",
            ")",
            "Ljava/util/List<",
            "*>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p0, v2

    invoke-static {v3}, Lf4/b$a;->e0(Landroid/media/AudioDeviceInfo;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static synthetic g(Lf4/b$a;I)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->S0(I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private g0()Ljava/lang/Object;
    .locals 1

    const/16 v0, 0x15

    invoke-static {v0}, Lf4/b;->g(I)V

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->generateAudioSessionId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lf4/b$a;III)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lf4/b$a;->U0(III)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private h0()Ljava/lang/Object;
    .locals 1

    const/16 v0, 0x1d

    invoke-static {v0}, Lf4/b;->g(I)V

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getAllowedCapturePolicy()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lf4/b$a;I)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->D0(I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private i0()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    const/16 v0, 0x1f

    invoke-static {v0}, Lf4/b;->g(I)V

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getAvailableCommunicationDevices()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lf4/b$a;->i:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lf4/b$a;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioDeviceInfo;

    invoke-static {v2}, Lf4/b$a;->e0(Landroid/media/AudioDeviceInfo;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static synthetic j(Lf4/b$a;)Ljava/util/List;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->i0()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private j0()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const/16 v0, 0x1f

    invoke-static {v0}, Lf4/b;->g(I)V

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getCommunicationDevice()Landroid/media/AudioDeviceInfo;

    move-result-object v0

    invoke-static {v0}, Lf4/b$a;->e0(Landroid/media/AudioDeviceInfo;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lf4/b$a;Ljava/lang/Integer;)Z
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->O0(Ljava/lang/Integer;)Z

    move-result p0

    return p0
.end method

.method private k0(I)Ljava/lang/Object;
    .locals 8

    const/16 v0, 0x17

    invoke-static {v0}, Lf4/b;->g(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v1, p1}, Landroid/media/AudioManager;->getDevices(I)[Landroid/media/AudioDeviceInfo;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_1

    aget-object v3, p1, v2

    const/4 v4, 0x0

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x1c

    if-lt v5, v6, :cond_0

    invoke-virtual {v3}, Landroid/media/AudioDeviceInfo;->getAddress()Ljava/lang/String;

    move-result-object v4

    :cond_0
    const/16 v5, 0x16

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "id"

    aput-object v6, v5, v1

    invoke-virtual {v3}, Landroid/media/AudioDeviceInfo;->getId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x1

    aput-object v6, v5, v7

    const/4 v6, 0x2

    const-string v7, "productName"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    invoke-virtual {v3}, Landroid/media/AudioDeviceInfo;->getProductName()Ljava/lang/CharSequence;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "address"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    aput-object v4, v5, v6

    const/4 v4, 0x6

    const-string v6, "isSource"

    aput-object v6, v5, v4

    const/4 v4, 0x7

    invoke-virtual {v3}, Landroid/media/AudioDeviceInfo;->isSource()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v4

    const/16 v4, 0x8

    const-string v6, "isSink"

    aput-object v6, v5, v4

    const/16 v4, 0x9

    invoke-virtual {v3}, Landroid/media/AudioDeviceInfo;->isSink()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v4

    const/16 v4, 0xa

    const-string v6, "sampleRates"

    aput-object v6, v5, v4

    const/16 v4, 0xb

    invoke-virtual {v3}, Landroid/media/AudioDeviceInfo;->getSampleRates()[I

    move-result-object v6

    invoke-static {v6}, Lf4/b;->e([I)Ljava/util/ArrayList;

    move-result-object v6

    aput-object v6, v5, v4

    const/16 v4, 0xc

    const-string v6, "channelMasks"

    aput-object v6, v5, v4

    const/16 v4, 0xd

    invoke-virtual {v3}, Landroid/media/AudioDeviceInfo;->getChannelMasks()[I

    move-result-object v6

    invoke-static {v6}, Lf4/b;->e([I)Ljava/util/ArrayList;

    move-result-object v6

    aput-object v6, v5, v4

    const/16 v4, 0xe

    const-string v6, "channelIndexMasks"

    aput-object v6, v5, v4

    const/16 v4, 0xf

    invoke-virtual {v3}, Landroid/media/AudioDeviceInfo;->getChannelIndexMasks()[I

    move-result-object v6

    invoke-static {v6}, Lf4/b;->e([I)Ljava/util/ArrayList;

    move-result-object v6

    aput-object v6, v5, v4

    const/16 v4, 0x10

    const-string v6, "channelCounts"

    aput-object v6, v5, v4

    const/16 v4, 0x11

    invoke-virtual {v3}, Landroid/media/AudioDeviceInfo;->getChannelCounts()[I

    move-result-object v6

    invoke-static {v6}, Lf4/b;->e([I)Ljava/util/ArrayList;

    move-result-object v6

    aput-object v6, v5, v4

    const/16 v4, 0x12

    const-string v6, "encodings"

    aput-object v6, v5, v4

    const/16 v4, 0x13

    invoke-virtual {v3}, Landroid/media/AudioDeviceInfo;->getEncodings()[I

    move-result-object v6

    invoke-static {v6}, Lf4/b;->e([I)Ljava/util/ArrayList;

    move-result-object v6

    aput-object v6, v5, v4

    const/16 v4, 0x14

    const-string v6, "type"

    aput-object v6, v5, v4

    const/16 v4, 0x15

    invoke-virtual {v3}, Landroid/media/AudioDeviceInfo;->getType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v4

    invoke-static {v5}, Lf4/b;->f([Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    return-object v0
.end method

.method static synthetic l(Lf4/b$a;)Ljava/util/Map;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->j0()Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private l0()Ljava/lang/Object;
    .locals 14

    const/16 v0, 0x1c

    invoke-static {v0}, Lf4/b;->g(I)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->getMicrophones()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/MicrophoneInfo;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getFrequencyResponse()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x2

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/util/Pair;

    new-instance v10, Ljava/util/ArrayList;

    new-array v9, v9, [Ljava/lang/Double;

    iget-object v11, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Float;

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    float-to-double v11, v11

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v9, v8

    iget-object v6, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    float-to-double v11, v6

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v9, v7

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v10, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v4, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getChannelMapping()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/util/Pair;

    new-instance v11, Ljava/util/ArrayList;

    new-array v12, v9, [Ljava/lang/Integer;

    iget-object v13, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v13, Ljava/lang/Integer;

    aput-object v13, v12, v8

    iget-object v10, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v10, Ljava/lang/Integer;

    aput-object v10, v12, v7

    invoke-static {v12}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    invoke-direct {v11, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    const/16 v6, 0x1e

    new-array v6, v6, [Ljava/lang/Object;

    const-string v10, "description"

    aput-object v10, v6, v8

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getDescription()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const-string v7, "id"

    aput-object v7, v6, v9

    const/4 v7, 0x3

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getId()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "type"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getType()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "address"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getAddress()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-string v8, "location"

    aput-object v8, v6, v7

    const/16 v7, 0x9

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getLocation()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xa

    const-string v8, "group"

    aput-object v8, v6, v7

    const/16 v7, 0xb

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getGroup()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xc

    const-string v8, "indexInTheGroup"

    aput-object v8, v6, v7

    const/16 v7, 0xd

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getIndexInTheGroup()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xe

    const-string v8, "position"

    aput-object v8, v6, v7

    const/16 v7, 0xf

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getPosition()Landroid/media/MicrophoneInfo$Coordinate3F;

    move-result-object v8

    invoke-static {v8}, Lf4/b;->b(Landroid/media/MicrophoneInfo$Coordinate3F;)Ljava/util/ArrayList;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x10

    const-string v8, "orientation"

    aput-object v8, v6, v7

    const/16 v7, 0x11

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getOrientation()Landroid/media/MicrophoneInfo$Coordinate3F;

    move-result-object v8

    invoke-static {v8}, Lf4/b;->b(Landroid/media/MicrophoneInfo$Coordinate3F;)Ljava/util/ArrayList;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x12

    const-string v8, "frequencyResponse"

    aput-object v8, v6, v7

    const/16 v7, 0x13

    aput-object v4, v6, v7

    const/16 v4, 0x14

    const-string v7, "channelMapping"

    aput-object v7, v6, v4

    const/16 v4, 0x15

    aput-object v5, v6, v4

    const/16 v4, 0x16

    const-string v5, "sensitivity"

    aput-object v5, v6, v4

    const/16 v4, 0x17

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getSensitivity()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v6, v4

    const/16 v4, 0x18

    const-string v5, "maxSpl"

    aput-object v5, v6, v4

    const/16 v4, 0x19

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getMaxSpl()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v6, v4

    const/16 v4, 0x1a

    const-string v5, "minSpl"

    aput-object v5, v6, v4

    const/16 v4, 0x1b

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getMinSpl()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v6, v4

    const-string v4, "directionality"

    aput-object v4, v6, v0

    const/16 v4, 0x1d

    invoke-virtual {v3}, Landroid/media/MicrophoneInfo;->getDirectionality()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v4

    invoke-static {v6}, Lf4/b;->f([Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_2
    return-object v1
.end method

.method static synthetic m(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->Z()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private m0()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lf4/b$a;Z)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->T0(Z)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private n0(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic o(Lf4/b$a;Ljava/util/Map;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->b0(Ljava/util/Map;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private o0(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    const/16 v0, 0x11

    invoke-static {v0}, Lf4/b;->g(I)V

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic p(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->C0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private p0()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic q(Lf4/b$a;I)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->M0(I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private q0(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method static synthetic r(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->h0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private r0(I)Ljava/lang/Object;
    .locals 1

    const/16 v0, 0x1c

    invoke-static {v0}, Lf4/b;->g(I)V

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamMinVolume(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method static synthetic s(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->w0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private s0(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method static synthetic t(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->V0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private t0(III)Ljava/lang/Object;
    .locals 1

    const/16 v0, 0x1c

    invoke-static {v0}, Lf4/b;->g(I)V

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/AudioManager;->getStreamVolumeDb(III)F

    move-result p1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    return-object p1
.end method

.method static synthetic u(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->W0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private u0()V
    .locals 3

    new-instance v0, Lf4/b$a$a;

    invoke-direct {v0, p0}, Lf4/b$a$a;-><init>(Lf4/b$a;)V

    iput-object v0, p0, Lf4/b$a;->h:Ljava/lang/Object;

    iget-object v1, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    check-cast v0, Landroid/media/AudioDeviceCallback;

    iget-object v2, p0, Lf4/b$a;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0, v2}, Landroid/media/AudioManager;->registerAudioDeviceCallback(Landroid/media/AudioDeviceCallback;Landroid/os/Handler;)V

    return-void
.end method

.method static synthetic v(Lf4/b$a;Z)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->N0(Z)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private varargs v0(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    iget-object v0, p0, Lf4/b$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf4/b;

    new-instance v2, Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Lf4/b;->a(Lf4/b;)Lz4/k;

    move-result-object v1

    invoke-virtual {v1, p1, v2}, Lz4/k;->c(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic w(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->x0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private w0()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoAvailableOffCall()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic x(Lf4/b$a;Z)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lf4/b$a;->P0(Z)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private x0()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic y(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->A0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic z(Lf4/b$a;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lf4/b$a;->E0()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private z0()Ljava/lang/Object;
    .locals 1

    const/16 v0, 0x1d

    invoke-static {v0}, Lf4/b;->g(I)V

    invoke-static {}, Landroid/media/AudioManager;->isHapticPlaybackSupported()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public K0(Lf4/b;)V
    .locals 1

    iget-object v0, p0, Lf4/b$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public V(Lf4/b;)V
    .locals 1

    iget-object v0, p0, Lf4/b$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public c0()V
    .locals 2

    invoke-direct {p0}, Lf4/b$a;->b()Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lf4/b$a;->d0()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lf4/b$a;->f:Landroid/content/Context;

    iput-object v0, p0, Lf4/b$a;->g:Landroid/media/AudioManager;

    return-void
.end method

.method public y0()Z
    .locals 1

    iget-object v0, p0, Lf4/b$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
