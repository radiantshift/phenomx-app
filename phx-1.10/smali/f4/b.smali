.class public Lf4/b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz4/k$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf4/b$a;
    }
.end annotation


# static fields
.field private static h:Lf4/b$a;


# instance fields
.field private f:Lz4/c;

.field private g:Lz4/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lz4/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lf4/b;->h:Lf4/b$a;

    if-nez v0, :cond_0

    new-instance v0, Lf4/b$a;

    invoke-direct {v0, p1}, Lf4/b$a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lf4/b;->h:Lf4/b$a;

    :cond_0
    iput-object p2, p0, Lf4/b;->f:Lz4/c;

    new-instance p1, Lz4/k;

    const-string v0, "com.ryanheise.android_audio_manager"

    invoke-direct {p1, p2, v0}, Lz4/k;-><init>(Lz4/c;Ljava/lang/String;)V

    iput-object p1, p0, Lf4/b;->g:Lz4/k;

    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-virtual {p1, p0}, Lf4/b$a;->V(Lf4/b;)V

    iget-object p1, p0, Lf4/b;->g:Lz4/k;

    invoke-virtual {p1, p0}, Lz4/k;->e(Lz4/k$c;)V

    return-void
.end method

.method static synthetic a(Lf4/b;)Lz4/k;
    .locals 0

    iget-object p0, p0, Lf4/b;->g:Lz4/k;

    return-object p0
.end method

.method static b(Landroid/media/MicrophoneInfo$Coordinate3F;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/MicrophoneInfo$Coordinate3F;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget v1, p0, Landroid/media/MicrophoneInfo$Coordinate3F;->x:F

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, p0, Landroid/media/MicrophoneInfo$Coordinate3F;->y:F

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget p0, p0, Landroid/media/MicrophoneInfo$Coordinate3F;->z:F

    float-to-double v1, p0

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method static d(Ljava/lang/Object;)Ljava/lang/Long;
    .locals 2

    if-eqz p0, :cond_1

    instance-of v0, p0, Ljava/lang/Long;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    int-to-long v0, p0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    goto :goto_1

    :cond_1
    :goto_0
    check-cast p0, Ljava/lang/Long;

    :goto_1
    return-object p0
.end method

.method static e([I)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    aget v2, p0, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static varargs f([Ljava/lang/Object;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    aget-object v2, p0, v1

    check-cast v2, Ljava/lang/String;

    add-int/lit8 v3, v1, 0x1

    aget-object v3, p0, v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static g(I)V
    .locals 3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, p0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requires API level "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public C(Lz4/j;Lz4/k$d;)V
    .locals 6

    :try_start_0
    iget-object v0, p1, Lz4/j;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iget-object p1, p1, Lz4/j;->a:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    sparse-switch v2, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v2, "generateAudioSessionId"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x21

    goto/16 :goto_0

    :sswitch_1
    const-string v2, "isVolumeFixed"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x3

    goto/16 :goto_0

    :sswitch_2
    const-string v2, "setMode"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x1e

    goto/16 :goto_0

    :sswitch_3
    const-string v2, "getAvailableCommunicationDevices"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0xf

    goto/16 :goto_0

    :sswitch_4
    const-string v2, "playSoundEffect"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x24

    goto/16 :goto_0

    :sswitch_5
    const-string v2, "setRingerMode"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0xc

    goto/16 :goto_0

    :sswitch_6
    const-string v2, "unloadSoundEffects"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x26

    goto/16 :goto_0

    :sswitch_7
    const-string v2, "abandonAudioFocus"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_0

    :sswitch_8
    const-string v2, "adjustSuggestedStreamVolume"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x6

    goto/16 :goto_0

    :sswitch_9
    const-string v2, "clearCommunicationDevice"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x12

    goto/16 :goto_0

    :sswitch_a
    const-string v2, "setStreamVolume"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_b
    const-string v2, "getAllowedCapturePolicy"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x16

    goto/16 :goto_0

    :sswitch_c
    const-string v2, "getProperty"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x27

    goto/16 :goto_0

    :sswitch_d
    const-string v2, "isStreamMute"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0xe

    goto/16 :goto_0

    :sswitch_e
    const-string v2, "adjustVolume"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x5

    goto/16 :goto_0

    :sswitch_f
    const-string v2, "setParameters"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x22

    goto/16 :goto_0

    :sswitch_10
    const-string v2, "getRingerMode"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x7

    goto/16 :goto_0

    :sswitch_11
    const-string v2, "isBluetoothScoAvailableOffCall"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x17

    goto/16 :goto_0

    :sswitch_12
    const-string v2, "getStreamVolume"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0xa

    goto/16 :goto_0

    :sswitch_13
    const-string v2, "stopBluetoothSco"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x19

    goto/16 :goto_0

    :sswitch_14
    const-string v2, "getParameters"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x23

    goto/16 :goto_0

    :sswitch_15
    const-string v2, "dispatchMediaKeyEvent"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x2

    goto/16 :goto_0

    :sswitch_16
    const-string v2, "getMode"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x1f

    goto/16 :goto_0

    :sswitch_17
    const-string v2, "getStreamVolumeDb"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0xb

    goto/16 :goto_0

    :sswitch_18
    const-string v2, "setCommunicationDevice"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x10

    goto/16 :goto_0

    :sswitch_19
    const-string v2, "startBluetoothSco"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x18

    goto/16 :goto_0

    :sswitch_1a
    const-string v2, "isMusicActive"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x20

    goto/16 :goto_0

    :sswitch_1b
    const-string v2, "loadSoundEffects"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x25

    goto/16 :goto_0

    :sswitch_1c
    const-string v2, "getStreamMinVolume"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x9

    goto/16 :goto_0

    :sswitch_1d
    const-string v2, "setBluetoothScoOn"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x1a

    goto/16 :goto_0

    :sswitch_1e
    const-string v2, "setAllowedCapturePolicy"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x15

    goto/16 :goto_0

    :sswitch_1f
    const-string v2, "getMicrophones"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x29

    goto/16 :goto_0

    :sswitch_20
    const-string v2, "adjustStreamVolume"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x4

    goto/16 :goto_0

    :sswitch_21
    const-string v2, "isBluetoothScoOn"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x1b

    goto :goto_0

    :sswitch_22
    const-string v2, "setSpeakerphoneOn"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x13

    goto :goto_0

    :sswitch_23
    const-string v2, "setMicrophoneMute"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x1c

    goto :goto_0

    :sswitch_24
    const-string v2, "requestAudioFocus"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_25
    const-string v2, "isHapticPlaybackSupported"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x2a

    goto :goto_0

    :sswitch_26
    const-string v2, "isSpeakerphoneOn"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x14

    goto :goto_0

    :sswitch_27
    const-string v2, "getStreamMaxVolume"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :sswitch_28
    const-string v2, "isMicrophoneMute"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x1d

    goto :goto_0

    :sswitch_29
    const-string v2, "getDevices"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x28

    goto :goto_0

    :sswitch_2a
    const-string v2, "getCommunicationDevice"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 v1, 0x11

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    invoke-interface {p2}, Lz4/k$d;->c()V

    goto/16 :goto_2

    :pswitch_0
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->N(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    :goto_1
    invoke-interface {p2, p1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    goto/16 :goto_2

    :pswitch_1
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->M(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    :pswitch_2
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Lf4/b$a;->L(Lf4/b$a;I)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    :pswitch_3
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v0}, Lf4/b$a;->J(Lf4/b$a;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    :pswitch_4
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->I(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    :pswitch_5
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->H(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    :pswitch_6
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-static {p1, v1, v0}, Lf4/b$a;->G(Lf4/b$a;ILjava/lang/Double;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    :pswitch_7
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v0}, Lf4/b$a;->F(Lf4/b$a;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    :pswitch_8
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v0}, Lf4/b$a;->E(Lf4/b$a;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    :pswitch_9
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->D(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    :pswitch_a
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->C(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    :pswitch_b
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->B(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    :pswitch_c
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Lf4/b$a;->A(Lf4/b$a;I)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_d
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->y(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_e
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p1, v0}, Lf4/b$a;->x(Lf4/b$a;Z)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_f
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->w(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_10
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p1, v0}, Lf4/b$a;->v(Lf4/b$a;Z)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_11
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->u(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_12
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->t(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_13
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->s(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_14
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->r(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_15
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Lf4/b$a;->q(Lf4/b$a;I)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_16
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->p(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_17
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p1, v0}, Lf4/b$a;->n(Lf4/b$a;Z)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_18
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->m(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_19
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->l(Lf4/b$a;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_1a
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {p1, v0}, Lf4/b$a;->k(Lf4/b$a;Ljava/lang/Integer;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_1b
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->j(Lf4/b$a;)Ljava/util/List;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_1c
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Lf4/b$a;->i(Lf4/b$a;I)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_1d
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v1, v2, v0}, Lf4/b$a;->h(Lf4/b$a;III)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_1e
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Lf4/b$a;->g(Lf4/b$a;I)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_1f
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v1, v2, v0}, Lf4/b$a;->f(Lf4/b$a;III)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_20
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Lf4/b$a;->e(Lf4/b$a;I)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_21
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Lf4/b$a;->U(Lf4/b$a;I)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_22
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Lf4/b$a;->T(Lf4/b$a;I)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_23
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->S(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_24
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v1, v2, v0}, Lf4/b$a;->R(Lf4/b$a;III)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_25
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v1, v0}, Lf4/b$a;->Q(Lf4/b$a;II)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_26
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v1, v2, v0}, Lf4/b$a;->K(Lf4/b$a;III)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_27
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->z(Lf4/b$a;)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_28
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-static {p1, v0}, Lf4/b$a;->o(Lf4/b$a;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_29
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1}, Lf4/b$a;->d(Lf4/b$a;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto/16 :goto_1

    :pswitch_2a
    sget-object p1, Lf4/b;->h:Lf4/b$a;

    invoke-static {p1, v0}, Lf4/b$a;->c(Lf4/b$a;Ljava/util/List;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0, v0}, Lz4/k$d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x68d7016a -> :sswitch_2a
        -0x653a1759 -> :sswitch_29
        -0x641dbdd3 -> :sswitch_28
        -0x5e4f1038 -> :sswitch_27
        -0x5d286128 -> :sswitch_26
        -0x5adb498e -> :sswitch_25
        -0x59af196f -> :sswitch_24
        -0x543b109b -> :sswitch_23
        -0x4d45b3f0 -> :sswitch_22
        -0x4c9a73e6 -> :sswitch_21
        -0x4759d017 -> :sswitch_20
        -0x410d2cad -> :sswitch_1f
        -0x4054a92e -> :sswitch_1e
        -0x3cb7c6ae -> :sswitch_1d
        -0x3043f9ca -> :sswitch_1c
        -0x2d824707 -> :sswitch_1b
        -0x2963f9ff -> :sswitch_1a
        -0x22a10fed -> :sswitch_19
        -0x1a9241f6 -> :sswitch_18
        -0x16b26e32 -> :sswitch_17
        -0x47d5de7 -> :sswitch_16
        0x9153925 -> :sswitch_15
        0x99879e0 -> :sswitch_14
        0xb21c3b3 -> :sswitch_13
        0x107e1530 -> :sswitch_12
        0x1bf5d05f -> :sswitch_11
        0x252e5a16 -> :sswitch_10
        0x37bcc7ec -> :sswitch_f
        0x38dee389 -> :sswitch_e
        0x3a315283 -> :sswitch_d
        0x40a81b4b -> :sswitch_c
        0x455827c6 -> :sswitch_b
        0x46c7103c -> :sswitch_a
        0x49fcee3f -> :sswitch_9
        0x4afd9d2e -> :sswitch_8
        0x50e69af7 -> :sswitch_7
        0x52277592 -> :sswitch_6
        0x5352a822 -> :sswitch_5
        0x59acfbac -> :sswitch_4
        0x5da380da -> :sswitch_3
        0x764d6925 -> :sswitch_2
        0x766c0cf0 -> :sswitch_1
        0x7ccf63f0 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lf4/b;->g:Lz4/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lz4/k;->e(Lz4/k$c;)V

    sget-object v0, Lf4/b;->h:Lf4/b$a;

    invoke-virtual {v0, p0}, Lf4/b$a;->K0(Lf4/b;)V

    sget-object v0, Lf4/b;->h:Lf4/b$a;

    invoke-virtual {v0}, Lf4/b$a;->y0()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lf4/b;->h:Lf4/b$a;

    invoke-virtual {v0}, Lf4/b$a;->c0()V

    sput-object v1, Lf4/b;->h:Lf4/b$a;

    :cond_0
    iput-object v1, p0, Lf4/b;->g:Lz4/k;

    iput-object v1, p0, Lf4/b;->f:Lz4/c;

    return-void
.end method
