.class public Le4/d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lq4/a;
.implements Lr4/a;


# instance fields
.field private f:Lz4/k;

.field private g:Lz4/d;

.field private h:Le4/c;

.field private i:Lq4/a$b;

.field private j:Lr4/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lz4/c;Lr4/c;)V
    .locals 2

    new-instance v0, Le4/c;

    invoke-interface {p2}, Lr4/c;->d()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Le4/c;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Le4/d;->h:Le4/c;

    new-instance v0, Lz4/k;

    const-string v1, "com.llfbandit.record/messages"

    invoke-direct {v0, p1, v1}, Lz4/k;-><init>(Lz4/c;Ljava/lang/String;)V

    iput-object v0, p0, Le4/d;->f:Lz4/k;

    iget-object v1, p0, Le4/d;->h:Le4/c;

    invoke-virtual {v0, v1}, Lz4/k;->e(Lz4/k$c;)V

    iget-object v0, p0, Le4/d;->h:Le4/c;

    invoke-interface {p2, v0}, Lr4/c;->b(Lz4/p;)V

    new-instance p2, Lz4/d;

    const-string v0, "com.llfbandit.record/events"

    invoke-direct {p2, p1, v0}, Lz4/d;-><init>(Lz4/c;Ljava/lang/String;)V

    iput-object p2, p0, Le4/d;->g:Lz4/d;

    iget-object p1, p0, Le4/d;->h:Le4/c;

    invoke-virtual {p2, p1}, Lz4/d;->d(Lz4/d$d;)V

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Le4/d;->j:Lr4/c;

    iget-object v1, p0, Le4/d;->h:Le4/c;

    invoke-interface {v0, v1}, Lr4/c;->f(Lz4/p;)V

    const/4 v0, 0x0

    iput-object v0, p0, Le4/d;->j:Lr4/c;

    iget-object v1, p0, Le4/d;->f:Lz4/k;

    invoke-virtual {v1, v0}, Lz4/k;->e(Lz4/k$c;)V

    iget-object v1, p0, Le4/d;->g:Lz4/d;

    invoke-virtual {v1, v0}, Lz4/d;->d(Lz4/d$d;)V

    iget-object v1, p0, Le4/d;->h:Le4/c;

    invoke-virtual {v1}, Le4/c;->c()V

    iput-object v0, p0, Le4/d;->h:Le4/c;

    iput-object v0, p0, Le4/d;->f:Lz4/k;

    iput-object v0, p0, Le4/d;->g:Lz4/d;

    return-void
.end method


# virtual methods
.method public b()V
    .locals 0

    invoke-direct {p0}, Le4/d;->c()V

    return-void
.end method

.method public d(Lq4/a$b;)V
    .locals 0

    const/4 p1, 0x0

    iput-object p1, p0, Le4/d;->i:Lq4/a$b;

    return-void
.end method

.method public e(Lr4/c;)V
    .locals 1

    iput-object p1, p0, Le4/d;->j:Lr4/c;

    iget-object v0, p0, Le4/d;->i:Lq4/a$b;

    invoke-virtual {v0}, Lq4/a$b;->b()Lz4/c;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Le4/d;->a(Lz4/c;Lr4/c;)V

    return-void
.end method

.method public g(Lr4/c;)V
    .locals 0

    invoke-virtual {p0, p1}, Le4/d;->e(Lr4/c;)V

    return-void
.end method

.method public h()V
    .locals 0

    invoke-virtual {p0}, Le4/d;->b()V

    return-void
.end method

.method public k(Lq4/a$b;)V
    .locals 0

    iput-object p1, p0, Le4/d;->i:Lq4/a$b;

    return-void
.end method
