.class Le4/a$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le4/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final f:Ljava/lang/String;

.field final g:Ljava/lang/String;

.field final h:I

.field final i:I

.field final j:S

.field final k:S

.field private l:I

.field m:Ljava/util/concurrent/CountDownLatch;

.field final synthetic n:Le4/a;


# direct methods
.method constructor <init>(Le4/a;Ljava/lang/String;Ljava/lang/String;IISS)V
    .locals 1

    iput-object p1, p0, Le4/a$a;->n:Le4/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput p1, p0, Le4/a$a;->l:I

    new-instance p1, Ljava/util/concurrent/CountDownLatch;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object p1, p0, Le4/a$a;->m:Ljava/util/concurrent/CountDownLatch;

    iput-object p2, p0, Le4/a$a;->f:Ljava/lang/String;

    iput-object p3, p0, Le4/a$a;->g:Ljava/lang/String;

    iput p4, p0, Le4/a$a;->h:I

    iput p5, p0, Le4/a$a;->i:I

    iput-short p6, p0, Le4/a$a;->j:S

    iput-short p7, p0, Le4/a$a;->k:S

    return-void
.end method

.method private b(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Reading of audio buffer failed: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, -0x6

    if-eq p1, v1, :cond_3

    const/4 v1, -0x3

    if-eq p1, v1, :cond_2

    const/4 v1, -0x2

    if-eq p1, v1, :cond_1

    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    const-string v1, "Unknown ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ")"

    goto :goto_0

    :cond_0
    const-string p1, "AudioRecord.ERROR"

    goto :goto_0

    :cond_1
    const-string p1, "AudioRecord.ERROR_BAD_VALUE"

    goto :goto_0

    :cond_2
    const-string p1, "AudioRecord.ERROR_INVALID_OPERATION"

    goto :goto_0

    :cond_3
    const-string p1, "AudioRecord.ERROR_DEAD_OBJECT"

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private c([BI)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    div-int/lit8 v2, p2, 0x2

    if-ge v0, v2, :cond_1

    mul-int/lit8 v2, v0, 0x2

    aget-byte v3, p1, v2

    add-int/lit8 v2, v2, 0x1

    aget-byte v2, p1, v2

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-le v2, v1, :cond_0

    move v1, v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Le4/a$a;->n:Le4/a;

    invoke-static {p1}, Le4/a;->l(Le4/a;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object p1

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    int-to-double v0, v1

    const-wide/high16 v4, 0x40e0000000000000L    # 32768.0

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->log10(D)D

    move-result-wide v0

    mul-double v0, v0, v2

    double-to-int p2, v0

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    return-void
.end method

.method private d(Ljava/io/RandomAccessFile;)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    const-string v0, "RIFF"

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    iget v0, p0, Le4/a$a;->l:I

    add-int/lit8 v0, v0, 0x24

    invoke-static {v0}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeInt(I)V

    const-string v0, "WAVE"

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    const-string v0, "fmt "

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    const/16 v0, 0x10

    invoke-static {v0}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeInt(I)V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Short;->reverseBytes(S)S

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeShort(I)V

    iget-short v0, p0, Le4/a$a;->j:S

    invoke-static {v0}, Ljava/lang/Short;->reverseBytes(S)S

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeShort(I)V

    iget v0, p0, Le4/a$a;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeInt(I)V

    iget v0, p0, Le4/a$a;->h:I

    iget-short v1, p0, Le4/a$a;->j:S

    mul-int v0, v0, v1

    iget-short v1, p0, Le4/a$a;->k:S

    mul-int v0, v0, v1

    div-int/lit8 v0, v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeInt(I)V

    iget-short v0, p0, Le4/a$a;->j:S

    iget-short v1, p0, Le4/a$a;->k:S

    mul-int v0, v0, v1

    div-int/lit8 v0, v0, 0x8

    int-to-short v0, v0

    invoke-static {v0}, Ljava/lang/Short;->reverseBytes(S)S

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeShort(I)V

    iget-short v0, p0, Le4/a$a;->k:S

    invoke-static {v0}, Ljava/lang/Short;->reverseBytes(S)S

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeShort(I)V

    const-string v0, "data"

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    iget v0, p0, Le4/a$a;->l:I

    invoke-static {v0}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/RandomAccessFile;->writeInt(I)V

    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    iget-object v0, p0, Le4/a$a;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    return-void
.end method

.method public run()V
    .locals 6

    const-string v0, "wav"

    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    iget-object v2, p0, Le4/a$a;->f:Ljava/lang/String;

    const-string v3, "rw"

    invoke-direct {v1, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    iget v2, p0, Le4/a$a;->i:I

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v3, v4}, Ljava/io/RandomAccessFile;->setLength(J)V

    iget-object v3, p0, Le4/a$a;->g:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, v1}, Le4/a$a;->d(Ljava/io/RandomAccessFile;)V

    :cond_0
    :goto_0
    iget-object v3, p0, Le4/a$a;->n:Le4/a;

    invoke-static {v3}, Le4/a;->a(Le4/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_1
    iget-object v3, p0, Le4/a$a;->n:Le4/a;

    invoke-static {v3}, Le4/a;->c(Le4/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-eqz v3, :cond_1

    const-wide/16 v3, 0x64

    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_1

    :cond_1
    iget-object v3, p0, Le4/a$a;->n:Le4/a;

    invoke-static {v3}, Le4/a;->a(Le4/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v3, p0, Le4/a$a;->n:Le4/a;

    invoke-static {v3}, Le4/a;->k(Le4/a;)Landroid/media/AudioRecord;

    move-result-object v3

    iget v4, p0, Le4/a$a;->i:I

    invoke-virtual {v3, v2, v4}, Landroid/media/AudioRecord;->read(Ljava/nio/ByteBuffer;I)I

    move-result v3

    if-ltz v3, :cond_2

    if-lez v3, :cond_0

    iget v4, p0, Le4/a$a;->l:I

    add-int/2addr v4, v3

    iput v4, p0, Le4/a$a;->l:I

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-direct {p0, v4, v3}, Le4/a$a;->c([BI)V

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5, v3}, Ljava/io/RandomAccessFile;->write([BII)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0, v3}, Le4/a$a;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v2, p0, Le4/a$a;->g:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Le4/a$a;->d(Ljava/io/RandomAccessFile;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    :try_start_2
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    iget-object v0, p0, Le4/a$a;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v1

    :try_start_4
    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_2
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception v0

    goto :goto_3

    :catch_0
    move-exception v0

    :try_start_5
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Writing of recorded audio failed"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :goto_3
    iget-object v1, p0, Le4/a$a;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method
