.class public Le4/c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz4/k$c;
.implements Lz4/d$d;
.implements Lz4/p;


# instance fields
.field private final f:Landroid/app/Activity;

.field private g:Le4/e;

.field private h:Lz4/k$d;

.field private i:Lz4/d$b;


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le4/c;->f:Landroid/app/Activity;

    return-void
.end method

.method private b()V
    .locals 3

    iget-object v0, p0, Le4/c;->f:Landroid/app/Activity;

    const-string v1, "android.permission.RECORD_AUDIO"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3e9

    invoke-static {v0, v1, v2}, Landroidx/core/app/a;->f(Landroid/app/Activity;[Ljava/lang/String;I)V

    return-void
.end method

.method private d(Lz4/k$d;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Le4/c;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCacheDir()Ljava/io/File;

    move-result-object v0

    :try_start_0
    const-string v1, "audio"

    const-string v2, ".m4a"

    invoke-static {v1, v2, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "record"

    const-string v3, "Cannot create temp file."

    invoke-interface {p1, v2, v3, v1}, Lz4/k$d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    const/4 p1, 0x0

    return-object p1
.end method

.method private e(Lz4/k$d;)V
    .locals 1

    invoke-direct {p0}, Le4/c;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Le4/c;->h:Lz4/k$d;

    invoke-direct {p0}, Le4/c;->b()V

    goto :goto_0

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {p1, v0}, Lz4/k$d;->a(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private g()Z
    .locals 2

    iget-object v0, p0, Le4/c;->f:Landroid/app/Activity;

    const-string v1, "android.permission.RECORD_AUDIO"

    invoke-static {v0, v1}, Landroidx/core/content/a;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private h(Ljava/lang/String;)Le4/e;
    .locals 2

    new-instance v0, Le4/a;

    invoke-direct {v0}, Le4/a;-><init>()V

    invoke-interface {v0, p1}, Le4/e;->g(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Le4/b;

    iget-object v1, p0, Le4/c;->f:Landroid/app/Activity;

    invoke-direct {v0, v1}, Le4/b;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, p1}, Le4/e;->g(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private j(I)V
    .locals 1

    iget-object v0, p0, Le4/c;->i:Lz4/d$b;

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Lz4/d$b;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public C(Lz4/j;Lz4/k$d;)V
    .locals 12

    iget-object v0, p1, Lz4/j;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, -0x1

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v1, "dispose"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    :cond_0
    const/16 v5, 0xa

    goto/16 :goto_0

    :sswitch_1
    const-string v1, "getAmplitude"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_0

    :cond_1
    const/16 v5, 0x9

    goto/16 :goto_0

    :sswitch_2
    const-string v1, "listInputDevices"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_0

    :cond_2
    const/16 v5, 0x8

    goto/16 :goto_0

    :sswitch_3
    const-string v1, "hasPermission"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    :cond_3
    const/4 v5, 0x7

    goto :goto_0

    :sswitch_4
    const-string v1, "isEncoderSupported"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_0

    :cond_4
    const/4 v5, 0x6

    goto :goto_0

    :sswitch_5
    const-string v1, "start"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    goto :goto_0

    :cond_5
    const/4 v5, 0x5

    goto :goto_0

    :sswitch_6
    const-string v1, "pause"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    goto :goto_0

    :cond_6
    const/4 v5, 0x4

    goto :goto_0

    :sswitch_7
    const-string v1, "stop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    goto :goto_0

    :cond_7
    const/4 v5, 0x3

    goto :goto_0

    :sswitch_8
    const-string v1, "isPaused"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    goto :goto_0

    :cond_8
    const/4 v5, 0x2

    goto :goto_0

    :sswitch_9
    const-string v1, "resume"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    goto :goto_0

    :cond_9
    const/4 v5, 0x1

    goto :goto_0

    :sswitch_a
    const-string v1, "isRecording"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    goto :goto_0

    :cond_a
    const/4 v5, 0x0

    :goto_0
    const-string v0, "encoder"

    const/4 v1, 0x0

    packed-switch v5, :pswitch_data_0

    invoke-interface {p2}, Lz4/k$d;->c()V

    goto/16 :goto_5

    :pswitch_0
    invoke-virtual {p0}, Le4/c;->c()V

    goto :goto_1

    :pswitch_1
    iget-object p1, p0, Le4/c;->g:Le4/e;

    if-eqz p1, :cond_b

    invoke-interface {p1}, Le4/e;->e()Ljava/util/Map;

    move-result-object p1

    goto :goto_3

    :cond_b
    :goto_1
    :pswitch_2
    invoke-interface {p2, v1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    goto/16 :goto_5

    :pswitch_3
    invoke-direct {p0, p2}, Le4/c;->e(Lz4/k$d;)V

    goto/16 :goto_5

    :pswitch_4
    invoke-virtual {p1, v0}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, Le4/c;->h(Ljava/lang/String;)Le4/e;

    move-result-object v0

    invoke-interface {v0, p1}, Le4/e;->g(Ljava/lang/String;)Z

    move-result p1

    :goto_2
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    :goto_3
    invoke-interface {p2, p1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    goto/16 :goto_5

    :pswitch_5
    const-string v2, "path"

    invoke-virtual {p1, v2}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v2, :cond_c

    invoke-direct {p0, p2}, Le4/c;->d(Lz4/k$d;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_c

    return-void

    :cond_c
    move-object v6, v2

    invoke-virtual {p1, v0}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    const-string v0, "bitRate"

    invoke-virtual {p1, v0}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const-string v0, "samplingRate"

    invoke-virtual {p1, v0}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    const-string v0, "numChannels"

    invoke-virtual {p1, v0}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const-string v0, "device"

    invoke-virtual {p1, v0}, Lz4/j;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    move-object v11, p1

    check-cast v11, Ljava/util/Map;

    invoke-direct {p0, v7}, Le4/c;->h(Ljava/lang/String;)Le4/e;

    move-result-object v5

    iput-object v5, p0, Le4/c;->g:Le4/e;

    :try_start_0
    invoke-interface/range {v5 .. v11}, Le4/e;->i(Ljava/lang/String;Ljava/lang/String;IIILjava/util/Map;)V

    invoke-interface {p2, v1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    invoke-direct {p0, v4}, Le4/c;->j(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_5

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    const-string v1, "-1"

    goto :goto_4

    :pswitch_6
    iget-object p1, p0, Le4/c;->g:Le4/e;

    if-eqz p1, :cond_b

    :try_start_1
    invoke-interface {p1}, Le4/e;->d()V

    invoke-interface {p2, v1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    invoke-direct {p0, v3}, Le4/c;->j(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    const-string v1, "-3"

    goto :goto_4

    :pswitch_7
    iget-object p1, p0, Le4/c;->g:Le4/e;

    if-eqz p1, :cond_b

    :try_start_2
    invoke-interface {p1}, Le4/e;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    invoke-direct {p0, v2}, Le4/c;->j(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_5

    :catch_2
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    const-string v1, "-2"

    :goto_4
    invoke-interface {p2, v1, v0, p1}, Lz4/k$d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_5

    :pswitch_8
    iget-object p1, p0, Le4/c;->g:Le4/e;

    if-eqz p1, :cond_d

    invoke-interface {p1}, Le4/e;->h()Z

    move-result p1

    goto/16 :goto_2

    :cond_d
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto/16 :goto_3

    :pswitch_9
    iget-object p1, p0, Le4/c;->g:Le4/e;

    if-eqz p1, :cond_b

    :try_start_3
    invoke-interface {p1}, Le4/e;->f()V

    invoke-interface {p2, v1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    invoke-direct {p0, v4}, Le4/c;->j(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_5

    :catch_3
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    const-string v1, "-4"

    goto :goto_4

    :pswitch_a
    iget-object p1, p0, Le4/c;->g:Le4/e;

    if-eqz p1, :cond_d

    invoke-interface {p1}, Le4/e;->j()Z

    move-result p1

    goto/16 :goto_2

    :goto_5
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7e8a4019 -> :sswitch_a
        -0x37b237d3 -> :sswitch_9
        -0x13267508 -> :sswitch_8
        0x360802 -> :sswitch_7
        0x65825f6 -> :sswitch_6
        0x68ac462 -> :sswitch_5
        0x6e92c3c -> :sswitch_4
        0xa3e3c09 -> :sswitch_3
        0x30177d71 -> :sswitch_2
        0x4b3f0dcd -> :sswitch_1
        0x63a5261f -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(I[Ljava/lang/String;[I)Z
    .locals 1

    const/4 p2, 0x0

    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Le4/c;->h:Lz4/k$d;

    if-eqz p1, :cond_1

    array-length v0, p3

    if-lez v0, :cond_0

    aget p2, p3, p2

    if-nez p2, :cond_0

    sget-object p2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_0
    sget-object p2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_0
    invoke-interface {p1, p2}, Lz4/k$d;->a(Ljava/lang/Object;)V

    const/4 p1, 0x0

    iput-object p1, p0, Le4/c;->h:Lz4/k$d;

    const/4 p1, 0x1

    return p1

    :cond_1
    return p2
.end method

.method c()V
    .locals 1

    iget-object v0, p0, Le4/c;->g:Le4/e;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Le4/e;->close()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Le4/c;->h:Lz4/k$d;

    return-void
.end method

.method public f(Ljava/lang/Object;)V
    .locals 0

    const/4 p1, 0x0

    iput-object p1, p0, Le4/c;->i:Lz4/d$b;

    return-void
.end method

.method public i(Ljava/lang/Object;Lz4/d$b;)V
    .locals 0

    iput-object p2, p0, Le4/c;->i:Lz4/d$b;

    return-void
.end method
