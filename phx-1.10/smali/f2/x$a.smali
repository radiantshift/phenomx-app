.class public final Lf2/x$a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf2/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lf2/x;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lf2/x;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p2, :cond_0

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/Handler;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lf2/x$a;->a:Landroid/os/Handler;

    iput-object p2, p0, Lf2/x$a;->b:Lf2/x;

    return-void
.end method

.method public static synthetic a(Lf2/x$a;Lk0/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lf2/x$a;->s(Lk0/e;)V

    return-void
.end method

.method public static synthetic b(Lf2/x$a;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lf2/x$a;->r(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic c(Lf2/x$a;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1}, Lf2/x$a;->y(Ljava/lang/Exception;)V

    return-void
.end method

.method public static synthetic d(Lf2/x$a;Lk0/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lf2/x$a;->u(Lk0/e;)V

    return-void
.end method

.method public static synthetic e(Lf2/x$a;Ljava/lang/Object;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lf2/x$a;->w(Ljava/lang/Object;J)V

    return-void
.end method

.method public static synthetic f(Lf2/x$a;IJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lf2/x$a;->t(IJ)V

    return-void
.end method

.method public static synthetic g(Lf2/x$a;Ljava/lang/String;JJ)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lf2/x$a;->q(Ljava/lang/String;JJ)V

    return-void
.end method

.method public static synthetic h(Lf2/x$a;Lf2/z;)V
    .locals 0

    invoke-direct {p0, p1}, Lf2/x$a;->z(Lf2/z;)V

    return-void
.end method

.method public static synthetic i(Lf2/x$a;Lh0/r1;Lk0/i;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lf2/x$a;->v(Lh0/r1;Lk0/i;)V

    return-void
.end method

.method public static synthetic j(Lf2/x$a;JI)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lf2/x$a;->x(JI)V

    return-void
.end method

.method private synthetic q(Ljava/lang/String;JJ)V
    .locals 7

    iget-object v0, p0, Lf2/x$a;->b:Lf2/x;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lf2/x;

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-interface/range {v1 .. v6}, Lf2/x;->g(Ljava/lang/String;JJ)V

    return-void
.end method

.method private synthetic r(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lf2/x$a;->b:Lf2/x;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf2/x;

    invoke-interface {v0, p1}, Lf2/x;->e(Ljava/lang/String;)V

    return-void
.end method

.method private synthetic s(Lk0/e;)V
    .locals 1

    invoke-virtual {p1}, Lk0/e;->c()V

    iget-object v0, p0, Lf2/x$a;->b:Lf2/x;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf2/x;

    invoke-interface {v0, p1}, Lf2/x;->o(Lk0/e;)V

    return-void
.end method

.method private synthetic t(IJ)V
    .locals 1

    iget-object v0, p0, Lf2/x$a;->b:Lf2/x;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf2/x;

    invoke-interface {v0, p1, p2, p3}, Lf2/x;->v(IJ)V

    return-void
.end method

.method private synthetic u(Lk0/e;)V
    .locals 1

    iget-object v0, p0, Lf2/x$a;->b:Lf2/x;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf2/x;

    invoke-interface {v0, p1}, Lf2/x;->n(Lk0/e;)V

    return-void
.end method

.method private synthetic v(Lh0/r1;Lk0/i;)V
    .locals 1

    iget-object v0, p0, Lf2/x$a;->b:Lf2/x;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf2/x;

    invoke-interface {v0, p1}, Lf2/x;->A(Lh0/r1;)V

    iget-object v0, p0, Lf2/x$a;->b:Lf2/x;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf2/x;

    invoke-interface {v0, p1, p2}, Lf2/x;->k(Lh0/r1;Lk0/i;)V

    return-void
.end method

.method private synthetic w(Ljava/lang/Object;J)V
    .locals 1

    iget-object v0, p0, Lf2/x$a;->b:Lf2/x;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf2/x;

    invoke-interface {v0, p1, p2, p3}, Lf2/x;->f(Ljava/lang/Object;J)V

    return-void
.end method

.method private synthetic x(JI)V
    .locals 1

    iget-object v0, p0, Lf2/x$a;->b:Lf2/x;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf2/x;

    invoke-interface {v0, p1, p2, p3}, Lf2/x;->y(JI)V

    return-void
.end method

.method private synthetic y(Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lf2/x$a;->b:Lf2/x;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf2/x;

    invoke-interface {v0, p1}, Lf2/x;->q(Ljava/lang/Exception;)V

    return-void
.end method

.method private synthetic z(Lf2/z;)V
    .locals 1

    iget-object v0, p0, Lf2/x$a;->b:Lf2/x;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf2/x;

    invoke-interface {v0, p1}, Lf2/x;->l(Lf2/z;)V

    return-void
.end method


# virtual methods
.method public A(Ljava/lang/Object;)V
    .locals 4

    iget-object v0, p0, Lf2/x$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lf2/x$a;->a:Landroid/os/Handler;

    new-instance v3, Lf2/s;

    invoke-direct {v3, p0, p1, v0, v1}, Lf2/s;-><init>(Lf2/x$a;Ljava/lang/Object;J)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public B(JI)V
    .locals 2

    iget-object v0, p0, Lf2/x$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lf2/o;

    invoke-direct {v1, p0, p1, p2, p3}, Lf2/o;-><init>(Lf2/x$a;JI)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public C(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lf2/x$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lf2/r;

    invoke-direct {v1, p0, p1}, Lf2/r;-><init>(Lf2/x$a;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public D(Lf2/z;)V
    .locals 2

    iget-object v0, p0, Lf2/x$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lf2/p;

    invoke-direct {v1, p0, p1}, Lf2/p;-><init>(Lf2/x$a;Lf2/z;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public k(Ljava/lang/String;JJ)V
    .locals 9

    iget-object v0, p0, Lf2/x$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v8, Lf2/u;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lf2/u;-><init>(Lf2/x$a;Ljava/lang/String;JJ)V

    invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public l(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lf2/x$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lf2/t;

    invoke-direct {v1, p0, p1}, Lf2/t;-><init>(Lf2/x$a;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public m(Lk0/e;)V
    .locals 2

    invoke-virtual {p1}, Lk0/e;->c()V

    iget-object v0, p0, Lf2/x$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lf2/v;

    invoke-direct {v1, p0, p1}, Lf2/v;-><init>(Lf2/x$a;Lk0/e;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public n(IJ)V
    .locals 2

    iget-object v0, p0, Lf2/x$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lf2/n;

    invoke-direct {v1, p0, p1, p2, p3}, Lf2/n;-><init>(Lf2/x$a;IJ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public o(Lk0/e;)V
    .locals 2

    iget-object v0, p0, Lf2/x$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lf2/w;

    invoke-direct {v1, p0, p1}, Lf2/w;-><init>(Lf2/x$a;Lk0/e;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public p(Lh0/r1;Lk0/i;)V
    .locals 2

    iget-object v0, p0, Lf2/x$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lf2/q;

    invoke-direct {v1, p0, p1, p2}, Lf2/q;-><init>(Lf2/x$a;Lh0/r1;Lk0/i;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
