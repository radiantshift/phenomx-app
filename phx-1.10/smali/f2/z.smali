.class public final Lf2/z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# static fields
.field public static final j:Lf2/z;

.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/lang/String;

.field private static final m:Ljava/lang/String;

.field private static final n:Ljava/lang/String;

.field public static final o:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Lf2/z;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf2/z;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lf2/z;-><init>(II)V

    sput-object v0, Lf2/z;->j:Lf2/z;

    invoke-static {v1}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lf2/z;->k:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lf2/z;->l:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lf2/z;->m:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lf2/z;->n:Ljava/lang/String;

    sget-object v0, Lf2/y;->a:Lf2/y;

    sput-object v0, Lf2/z;->o:Lh0/h$a;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 2

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, p2, v0, v1}, Lf2/z;-><init>(IIIF)V

    return-void
.end method

.method public constructor <init>(IIIF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lf2/z;->f:I

    iput p2, p0, Lf2/z;->g:I

    iput p3, p0, Lf2/z;->h:I

    iput p4, p0, Lf2/z;->i:F

    return-void
.end method

.method public static synthetic a(Landroid/os/Bundle;)Lf2/z;
    .locals 0

    invoke-static {p0}, Lf2/z;->b(Landroid/os/Bundle;)Lf2/z;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic b(Landroid/os/Bundle;)Lf2/z;
    .locals 5

    sget-object v0, Lf2/z;->k:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    sget-object v2, Lf2/z;->l:Ljava/lang/String;

    invoke-virtual {p0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    sget-object v3, Lf2/z;->m:Ljava/lang/String;

    invoke-virtual {p0, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    sget-object v3, Lf2/z;->n:Ljava/lang/String;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {p0, v3, v4}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result p0

    new-instance v3, Lf2/z;

    invoke-direct {v3, v0, v2, v1, p0}, Lf2/z;-><init>(IIIF)V

    return-object v3
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lf2/z;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast p1, Lf2/z;

    iget v1, p0, Lf2/z;->f:I

    iget v3, p1, Lf2/z;->f:I

    if-ne v1, v3, :cond_1

    iget v1, p0, Lf2/z;->g:I

    iget v3, p1, Lf2/z;->g:I

    if-ne v1, v3, :cond_1

    iget v1, p0, Lf2/z;->h:I

    iget v3, p1, Lf2/z;->h:I

    if-ne v1, v3, :cond_1

    iget v1, p0, Lf2/z;->i:F

    iget p1, p1, Lf2/z;->i:F

    cmpl-float p1, v1, p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    return v2
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lf2/z;->f:I

    const/16 v1, 0xd9

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lf2/z;->g:I

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lf2/z;->h:I

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lf2/z;->i:F

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    add-int/2addr v1, v0

    return v1
.end method
