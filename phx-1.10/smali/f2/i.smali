.class public final Lf2/i;
.super Landroid/view/Surface;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf2/i$b;
    }
.end annotation


# static fields
.field private static i:I

.field private static j:Z


# instance fields
.field public final f:Z

.field private final g:Lf2/i$b;

.field private h:Z


# direct methods
.method private constructor <init>(Lf2/i$b;Landroid/graphics/SurfaceTexture;Z)V
    .locals 0

    invoke-direct {p0, p2}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object p1, p0, Lf2/i;->g:Lf2/i$b;

    iput-boolean p3, p0, Lf2/i;->f:Z

    return-void
.end method

.method synthetic constructor <init>(Lf2/i$b;Landroid/graphics/SurfaceTexture;ZLf2/i$a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lf2/i;-><init>(Lf2/i$b;Landroid/graphics/SurfaceTexture;Z)V

    return-void
.end method

.method private static d(Landroid/content/Context;)I
    .locals 0

    invoke-static {p0}, Le2/m;->c(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-static {}, Le2/m;->d()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x2

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method public static declared-synchronized e(Landroid/content/Context;)Z
    .locals 3

    const-class v0, Lf2/i;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Lf2/i;->j:Z

    const/4 v2, 0x1

    if-nez v1, :cond_0

    invoke-static {p0}, Lf2/i;->d(Landroid/content/Context;)I

    move-result p0

    sput p0, Lf2/i;->i:I

    sput-boolean v2, Lf2/i;->j:Z

    :cond_0
    sget p0, Lf2/i;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    monitor-exit v0

    return v2

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static f(Landroid/content/Context;Z)Lf2/i;
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-static {p0}, Lf2/i;->e(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    invoke-static {p0}, Le2/a;->f(Z)V

    new-instance p0, Lf2/i$b;

    invoke-direct {p0}, Lf2/i$b;-><init>()V

    if-eqz p1, :cond_2

    sget v0, Lf2/i;->i:I

    :cond_2
    invoke-virtual {p0, v0}, Lf2/i$b;->a(I)Lf2/i;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public release()V
    .locals 2

    invoke-super {p0}, Landroid/view/Surface;->release()V

    iget-object v0, p0, Lf2/i;->g:Lf2/i$b;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lf2/i;->h:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lf2/i;->g:Lf2/i$b;

    invoke-virtual {v1}, Lf2/i$b;->c()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lf2/i;->h:Z

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
