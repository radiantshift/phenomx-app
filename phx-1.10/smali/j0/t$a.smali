.class public final Lj0/t$a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj0/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lj0/t;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lj0/t;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p2, :cond_0

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/Handler;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lj0/t$a;->a:Landroid/os/Handler;

    iput-object p2, p0, Lj0/t$a;->b:Lj0/t;

    return-void
.end method

.method private synthetic A(IJJ)V
    .locals 7

    iget-object v0, p0, Lj0/t$a;->b:Lj0/t;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lj0/t;

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-interface/range {v1 .. v6}, Lj0/t;->u(IJJ)V

    return-void
.end method

.method public static synthetic a(Lj0/t$a;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lj0/t$a;->z(Z)V

    return-void
.end method

.method public static synthetic b(Lj0/t$a;Lk0/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lj0/t$a;->v(Lk0/e;)V

    return-void
.end method

.method public static synthetic c(Lj0/t$a;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1}, Lj0/t$a;->r(Ljava/lang/Exception;)V

    return-void
.end method

.method public static synthetic d(Lj0/t$a;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1}, Lj0/t$a;->s(Ljava/lang/Exception;)V

    return-void
.end method

.method public static synthetic e(Lj0/t$a;Lh0/r1;Lk0/i;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj0/t$a;->x(Lh0/r1;Lk0/i;)V

    return-void
.end method

.method public static synthetic f(Lj0/t$a;Ljava/lang/String;JJ)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lj0/t$a;->t(Ljava/lang/String;JJ)V

    return-void
.end method

.method public static synthetic g(Lj0/t$a;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lj0/t$a;->u(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic h(Lj0/t$a;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj0/t$a;->y(J)V

    return-void
.end method

.method public static synthetic i(Lj0/t$a;IJJ)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lj0/t$a;->A(IJJ)V

    return-void
.end method

.method public static synthetic j(Lj0/t$a;Lk0/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lj0/t$a;->w(Lk0/e;)V

    return-void
.end method

.method private synthetic r(Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lj0/t$a;->b:Lj0/t;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj0/t;

    invoke-interface {v0, p1}, Lj0/t;->p(Ljava/lang/Exception;)V

    return-void
.end method

.method private synthetic s(Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lj0/t$a;->b:Lj0/t;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj0/t;

    invoke-interface {v0, p1}, Lj0/t;->c(Ljava/lang/Exception;)V

    return-void
.end method

.method private synthetic t(Ljava/lang/String;JJ)V
    .locals 7

    iget-object v0, p0, Lj0/t$a;->b:Lj0/t;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lj0/t;

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-interface/range {v1 .. v6}, Lj0/t;->s(Ljava/lang/String;JJ)V

    return-void
.end method

.method private synthetic u(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lj0/t$a;->b:Lj0/t;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj0/t;

    invoke-interface {v0, p1}, Lj0/t;->r(Ljava/lang/String;)V

    return-void
.end method

.method private synthetic v(Lk0/e;)V
    .locals 1

    invoke-virtual {p1}, Lk0/e;->c()V

    iget-object v0, p0, Lj0/t$a;->b:Lj0/t;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj0/t;

    invoke-interface {v0, p1}, Lj0/t;->d(Lk0/e;)V

    return-void
.end method

.method private synthetic w(Lk0/e;)V
    .locals 1

    iget-object v0, p0, Lj0/t$a;->b:Lj0/t;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj0/t;

    invoke-interface {v0, p1}, Lj0/t;->z(Lk0/e;)V

    return-void
.end method

.method private synthetic x(Lh0/r1;Lk0/i;)V
    .locals 1

    iget-object v0, p0, Lj0/t$a;->b:Lj0/t;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj0/t;

    invoke-interface {v0, p1}, Lj0/t;->B(Lh0/r1;)V

    iget-object v0, p0, Lj0/t$a;->b:Lj0/t;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj0/t;

    invoke-interface {v0, p1, p2}, Lj0/t;->t(Lh0/r1;Lk0/i;)V

    return-void
.end method

.method private synthetic y(J)V
    .locals 1

    iget-object v0, p0, Lj0/t$a;->b:Lj0/t;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj0/t;

    invoke-interface {v0, p1, p2}, Lj0/t;->m(J)V

    return-void
.end method

.method private synthetic z(Z)V
    .locals 1

    iget-object v0, p0, Lj0/t$a;->b:Lj0/t;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj0/t;

    invoke-interface {v0, p1}, Lj0/t;->b(Z)V

    return-void
.end method


# virtual methods
.method public B(J)V
    .locals 2

    iget-object v0, p0, Lj0/t$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lj0/k;

    invoke-direct {v1, p0, p1, p2}, Lj0/k;-><init>(Lj0/t$a;J)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public C(Z)V
    .locals 2

    iget-object v0, p0, Lj0/t$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lj0/s;

    invoke-direct {v1, p0, p1}, Lj0/s;-><init>(Lj0/t$a;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public D(IJJ)V
    .locals 9

    iget-object v0, p0, Lj0/t$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v8, Lj0/j;

    move-object v1, v8

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lj0/j;-><init>(Lj0/t$a;IJJ)V

    invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public k(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lj0/t$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lj0/m;

    invoke-direct {v1, p0, p1}, Lj0/m;-><init>(Lj0/t$a;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public l(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lj0/t$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lj0/n;

    invoke-direct {v1, p0, p1}, Lj0/n;-><init>(Lj0/t$a;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public m(Ljava/lang/String;JJ)V
    .locals 9

    iget-object v0, p0, Lj0/t$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v8, Lj0/p;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lj0/p;-><init>(Lj0/t$a;Ljava/lang/String;JJ)V

    invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public n(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lj0/t$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lj0/o;

    invoke-direct {v1, p0, p1}, Lj0/o;-><init>(Lj0/t$a;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public o(Lk0/e;)V
    .locals 2

    invoke-virtual {p1}, Lk0/e;->c()V

    iget-object v0, p0, Lj0/t$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lj0/q;

    invoke-direct {v1, p0, p1}, Lj0/q;-><init>(Lj0/t$a;Lk0/e;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public p(Lk0/e;)V
    .locals 2

    iget-object v0, p0, Lj0/t$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lj0/r;

    invoke-direct {v1, p0, p1}, Lj0/r;-><init>(Lj0/t$a;Lk0/e;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public q(Lh0/r1;Lk0/i;)V
    .locals 2

    iget-object v0, p0, Lj0/t$a;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lj0/l;

    invoke-direct {v1, p0, p1, p2}, Lj0/l;-><init>(Lj0/t$a;Lh0/r1;Lk0/i;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
