.class public final Lj0/c0$f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj0/c0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation


# instance fields
.field private a:Lj0/f;

.field private b:Lj0/h;

.field private c:Z

.field private d:Z

.field private e:I

.field f:Lj0/c0$e;

.field g:Lh0/s$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lj0/f;->c:Lj0/f;

    iput-object v0, p0, Lj0/c0$f;->a:Lj0/f;

    const/4 v0, 0x0

    iput v0, p0, Lj0/c0$f;->e:I

    sget-object v0, Lj0/c0$e;->a:Lj0/c0$e;

    iput-object v0, p0, Lj0/c0$f;->f:Lj0/c0$e;

    return-void
.end method

.method static synthetic a(Lj0/c0$f;)Lj0/f;
    .locals 0

    iget-object p0, p0, Lj0/c0$f;->a:Lj0/f;

    return-object p0
.end method

.method static synthetic b(Lj0/c0$f;)Lj0/h;
    .locals 0

    iget-object p0, p0, Lj0/c0$f;->b:Lj0/h;

    return-object p0
.end method

.method static synthetic c(Lj0/c0$f;)Z
    .locals 0

    iget-boolean p0, p0, Lj0/c0$f;->c:Z

    return p0
.end method

.method static synthetic d(Lj0/c0$f;)Z
    .locals 0

    iget-boolean p0, p0, Lj0/c0$f;->d:Z

    return p0
.end method

.method static synthetic e(Lj0/c0$f;)I
    .locals 0

    iget p0, p0, Lj0/c0$f;->e:I

    return p0
.end method


# virtual methods
.method public f()Lj0/c0;
    .locals 2

    iget-object v0, p0, Lj0/c0$f;->b:Lj0/h;

    if-nez v0, :cond_0

    new-instance v0, Lj0/c0$h;

    const/4 v1, 0x0

    new-array v1, v1, [Lj0/g;

    invoke-direct {v0, v1}, Lj0/c0$h;-><init>([Lj0/g;)V

    iput-object v0, p0, Lj0/c0$f;->b:Lj0/h;

    :cond_0
    new-instance v0, Lj0/c0;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lj0/c0;-><init>(Lj0/c0$f;Lj0/c0$a;)V

    return-object v0
.end method

.method public g(Lj0/f;)Lj0/c0$f;
    .locals 0

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lj0/c0$f;->a:Lj0/f;

    return-object p0
.end method

.method public h(Z)Lj0/c0$f;
    .locals 0

    iput-boolean p1, p0, Lj0/c0$f;->d:Z

    return-object p0
.end method

.method public i(Z)Lj0/c0$f;
    .locals 0

    iput-boolean p1, p0, Lj0/c0$f;->c:Z

    return-object p0
.end method

.method public j(I)Lj0/c0$f;
    .locals 0

    iput p1, p0, Lj0/c0$f;->e:I

    return-object p0
.end method
