.class public final Lj0/e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj0/e$c;,
        Lj0/e$b;,
        Lj0/e$e;,
        Lj0/e$d;
    }
.end annotation


# static fields
.field public static final l:Lj0/e;

.field private static final m:Ljava/lang/String;

.field private static final n:Ljava/lang/String;

.field private static final o:Ljava/lang/String;

.field private static final p:Ljava/lang/String;

.field private static final q:Ljava/lang/String;

.field public static final r:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Lj0/e;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field private k:Lj0/e$d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lj0/e$e;

    invoke-direct {v0}, Lj0/e$e;-><init>()V

    invoke-virtual {v0}, Lj0/e$e;->a()Lj0/e;

    move-result-object v0

    sput-object v0, Lj0/e;->l:Lj0/e;

    const/4 v0, 0x0

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lj0/e;->m:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lj0/e;->n:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lj0/e;->o:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lj0/e;->p:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lj0/e;->q:Ljava/lang/String;

    sget-object v0, Lj0/d;->a:Lj0/d;

    sput-object v0, Lj0/e;->r:Lh0/h$a;

    return-void
.end method

.method private constructor <init>(IIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lj0/e;->f:I

    iput p2, p0, Lj0/e;->g:I

    iput p3, p0, Lj0/e;->h:I

    iput p4, p0, Lj0/e;->i:I

    iput p5, p0, Lj0/e;->j:I

    return-void
.end method

.method synthetic constructor <init>(IIIIILj0/e$a;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lj0/e;-><init>(IIIII)V

    return-void
.end method

.method public static synthetic a(Landroid/os/Bundle;)Lj0/e;
    .locals 0

    invoke-static {p0}, Lj0/e;->c(Landroid/os/Bundle;)Lj0/e;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic c(Landroid/os/Bundle;)Lj0/e;
    .locals 3

    new-instance v0, Lj0/e$e;

    invoke-direct {v0}, Lj0/e$e;-><init>()V

    sget-object v1, Lj0/e;->m:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lj0/e$e;->c(I)Lj0/e$e;

    :cond_0
    sget-object v1, Lj0/e;->n:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lj0/e$e;->d(I)Lj0/e$e;

    :cond_1
    sget-object v1, Lj0/e;->o:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lj0/e$e;->f(I)Lj0/e$e;

    :cond_2
    sget-object v1, Lj0/e;->p:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lj0/e$e;->b(I)Lj0/e$e;

    :cond_3
    sget-object v1, Lj0/e;->q:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {v0, p0}, Lj0/e$e;->e(I)Lj0/e$e;

    :cond_4
    invoke-virtual {v0}, Lj0/e$e;->a()Lj0/e;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public b()Lj0/e$d;
    .locals 2

    iget-object v0, p0, Lj0/e;->k:Lj0/e$d;

    if-nez v0, :cond_0

    new-instance v0, Lj0/e$d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lj0/e$d;-><init>(Lj0/e;Lj0/e$a;)V

    iput-object v0, p0, Lj0/e;->k:Lj0/e$d;

    :cond_0
    iget-object v0, p0, Lj0/e;->k:Lj0/e$d;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const-class v2, Lj0/e;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    check-cast p1, Lj0/e;

    iget v2, p0, Lj0/e;->f:I

    iget v3, p1, Lj0/e;->f:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lj0/e;->g:I

    iget v3, p1, Lj0/e;->g:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lj0/e;->h:I

    iget v3, p1, Lj0/e;->h:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lj0/e;->i:I

    iget v3, p1, Lj0/e;->i:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lj0/e;->j:I

    iget p1, p1, Lj0/e;->j:I

    if-ne v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lj0/e;->f:I

    const/16 v1, 0x20f

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lj0/e;->g:I

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lj0/e;->h:I

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lj0/e;->i:I

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lj0/e;->j:I

    add-int/2addr v1, v0

    return v1
.end method
