.class public final Lw/l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lx/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lx/b<",
        "Lw/k;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lw/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lj5/a;Lj5/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Landroid/content/Context;",
            ">;",
            "Lj5/a<",
            "Lw/i;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lw/l;->a:Lj5/a;

    iput-object p2, p0, Lw/l;->b:Lj5/a;

    return-void
.end method

.method public static a(Lj5/a;Lj5/a;)Lw/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Landroid/content/Context;",
            ">;",
            "Lj5/a<",
            "Lw/i;",
            ">;)",
            "Lw/l;"
        }
    .end annotation

    new-instance v0, Lw/l;

    invoke-direct {v0, p0, p1}, Lw/l;-><init>(Lj5/a;Lj5/a;)V

    return-object v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/Object;)Lw/k;
    .locals 1

    new-instance v0, Lw/k;

    check-cast p1, Lw/i;

    invoke-direct {v0, p0, p1}, Lw/k;-><init>(Landroid/content/Context;Lw/i;)V

    return-object v0
.end method


# virtual methods
.method public b()Lw/k;
    .locals 2

    iget-object v0, p0, Lw/l;->a:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lw/l;->b:Lj5/a;

    invoke-interface {v1}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lw/l;->c(Landroid/content/Context;Ljava/lang/Object;)Lw/k;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lw/l;->b()Lw/k;

    move-result-object v0

    return-object v0
.end method
