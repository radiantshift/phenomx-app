.class public final Lw/j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lx/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lx/b<",
        "Lw/i;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lf0/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lj5/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj5/a<",
            "Lf0/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lj5/a;Lj5/a;Lj5/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Landroid/content/Context;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lw/j;->a:Lj5/a;

    iput-object p2, p0, Lw/j;->b:Lj5/a;

    iput-object p3, p0, Lw/j;->c:Lj5/a;

    return-void
.end method

.method public static a(Lj5/a;Lj5/a;Lj5/a;)Lw/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lj5/a<",
            "Landroid/content/Context;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;",
            "Lj5/a<",
            "Lf0/a;",
            ">;)",
            "Lw/j;"
        }
    .end annotation

    new-instance v0, Lw/j;

    invoke-direct {v0, p0, p1, p2}, Lw/j;-><init>(Lj5/a;Lj5/a;Lj5/a;)V

    return-object v0
.end method

.method public static c(Landroid/content/Context;Lf0/a;Lf0/a;)Lw/i;
    .locals 1

    new-instance v0, Lw/i;

    invoke-direct {v0, p0, p1, p2}, Lw/i;-><init>(Landroid/content/Context;Lf0/a;Lf0/a;)V

    return-object v0
.end method


# virtual methods
.method public b()Lw/i;
    .locals 3

    iget-object v0, p0, Lw/j;->a:Lj5/a;

    invoke-interface {v0}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lw/j;->b:Lj5/a;

    invoke-interface {v1}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf0/a;

    iget-object v2, p0, Lw/j;->c:Lj5/a;

    invoke-interface {v2}, Lj5/a;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf0/a;

    invoke-static {v0, v1, v2}, Lw/j;->c(Landroid/content/Context;Lf0/a;Lf0/a;)Lw/i;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lw/j;->b()Lw/i;

    move-result-object v0

    return-object v0
.end method
