.class public final Lz0/g;
.super Lh0/f;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private A:J

.field private B:Lz0/a;

.field private C:J

.field private final s:Lz0/d;

.field private final t:Lz0/f;

.field private final u:Landroid/os/Handler;

.field private final v:Lz0/e;

.field private final w:Z

.field private x:Lz0/c;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Lz0/f;Landroid/os/Looper;)V
    .locals 1

    sget-object v0, Lz0/d;->a:Lz0/d;

    invoke-direct {p0, p1, p2, v0}, Lz0/g;-><init>(Lz0/f;Landroid/os/Looper;Lz0/d;)V

    return-void
.end method

.method public constructor <init>(Lz0/f;Landroid/os/Looper;Lz0/d;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lz0/g;-><init>(Lz0/f;Landroid/os/Looper;Lz0/d;Z)V

    return-void
.end method

.method public constructor <init>(Lz0/f;Landroid/os/Looper;Lz0/d;Z)V
    .locals 1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lh0/f;-><init>(I)V

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lz0/f;

    iput-object p1, p0, Lz0/g;->t:Lz0/f;

    if-nez p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p2, p0}, Le2/n0;->v(Landroid/os/Looper;Landroid/os/Handler$Callback;)Landroid/os/Handler;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lz0/g;->u:Landroid/os/Handler;

    invoke-static {p3}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lz0/d;

    iput-object p1, p0, Lz0/g;->s:Lz0/d;

    iput-boolean p4, p0, Lz0/g;->w:Z

    new-instance p1, Lz0/e;

    invoke-direct {p1}, Lz0/e;-><init>()V

    iput-object p1, p0, Lz0/g;->v:Lz0/e;

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lz0/g;->C:J

    return-void
.end method

.method private U(Lz0/a;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lz0/a;",
            "Ljava/util/List<",
            "Lz0/a$b;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lz0/a;->h()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {p1, v0}, Lz0/a;->g(I)Lz0/a$b;

    move-result-object v1

    invoke-interface {v1}, Lz0/a$b;->b()Lh0/r1;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lz0/g;->s:Lz0/d;

    invoke-interface {v2, v1}, Lz0/d;->a(Lh0/r1;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lz0/g;->s:Lz0/d;

    invoke-interface {v2, v1}, Lz0/d;->b(Lh0/r1;)Lz0/c;

    move-result-object v1

    invoke-virtual {p1, v0}, Lz0/a;->g(I)Lz0/a$b;

    move-result-object v2

    invoke-interface {v2}, Lz0/a$b;->c()[B

    move-result-object v2

    invoke-static {v2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    iget-object v3, p0, Lz0/g;->v:Lz0/e;

    invoke-virtual {v3}, Lk0/g;->f()V

    iget-object v3, p0, Lz0/g;->v:Lz0/e;

    array-length v4, v2

    invoke-virtual {v3, v4}, Lk0/g;->p(I)V

    iget-object v3, p0, Lz0/g;->v:Lz0/e;

    iget-object v3, v3, Lk0/g;->h:Ljava/nio/ByteBuffer;

    invoke-static {v3}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lz0/g;->v:Lz0/e;

    invoke-virtual {v2}, Lk0/g;->q()V

    iget-object v2, p0, Lz0/g;->v:Lz0/e;

    invoke-interface {v1, v2}, Lz0/c;->a(Lz0/e;)Lz0/a;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v1, p2}, Lz0/g;->U(Lz0/a;Ljava/util/List;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1, v0}, Lz0/a;->g(I)Lz0/a$b;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private V(J)J
    .locals 7
    .annotation runtime Lorg/checkerframework/dataflow/qual/SideEffectFree;
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, p1, v2

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-static {v4}, Le2/a;->f(Z)V

    iget-wide v4, p0, Lz0/g;->C:J

    cmp-long v6, v4, v2

    if-eqz v6, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Le2/a;->f(Z)V

    iget-wide v0, p0, Lz0/g;->C:J

    sub-long/2addr p1, v0

    return-wide p1
.end method

.method private W(Lz0/a;)V
    .locals 2

    iget-object v0, p0, Lz0/g;->u:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lz0/g;->X(Lz0/a;)V

    :goto_0
    return-void
.end method

.method private X(Lz0/a;)V
    .locals 1

    iget-object v0, p0, Lz0/g;->t:Lz0/f;

    invoke-interface {v0, p1}, Lz0/f;->w(Lz0/a;)V

    return-void
.end method

.method private Y(J)Z
    .locals 4

    iget-object v0, p0, Lz0/g;->B:Lz0/a;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-boolean v2, p0, Lz0/g;->w:Z

    if-nez v2, :cond_0

    iget-wide v2, v0, Lz0/a;->g:J

    invoke-direct {p0, p1, p2}, Lz0/g;->V(J)J

    move-result-wide p1

    cmp-long v0, v2, p1

    if-gtz v0, :cond_1

    :cond_0
    iget-object p1, p0, Lz0/g;->B:Lz0/a;

    invoke-direct {p0, p1}, Lz0/g;->W(Lz0/a;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lz0/g;->B:Lz0/a;

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    iget-boolean p2, p0, Lz0/g;->y:Z

    if-eqz p2, :cond_2

    iget-object p2, p0, Lz0/g;->B:Lz0/a;

    if-nez p2, :cond_2

    iput-boolean v1, p0, Lz0/g;->z:Z

    :cond_2
    return p1
.end method

.method private Z()V
    .locals 4

    iget-boolean v0, p0, Lz0/g;->y:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lz0/g;->B:Lz0/a;

    if-nez v0, :cond_2

    iget-object v0, p0, Lz0/g;->v:Lz0/e;

    invoke-virtual {v0}, Lk0/g;->f()V

    invoke-virtual {p0}, Lh0/f;->F()Lh0/s1;

    move-result-object v0

    iget-object v1, p0, Lz0/g;->v:Lz0/e;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lh0/f;->R(Lh0/s1;Lk0/g;I)I

    move-result v1

    const/4 v2, -0x4

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lz0/g;->v:Lz0/e;

    invoke-virtual {v0}, Lk0/a;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lz0/g;->y:Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lz0/g;->v:Lz0/e;

    iget-wide v1, p0, Lz0/g;->A:J

    iput-wide v1, v0, Lz0/e;->n:J

    invoke-virtual {v0}, Lk0/g;->q()V

    iget-object v0, p0, Lz0/g;->x:Lz0/c;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz0/c;

    iget-object v1, p0, Lz0/g;->v:Lz0/e;

    invoke-interface {v0, v1}, Lz0/c;->a(Lz0/e;)Lz0/a;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lz0/a;->h()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {p0, v0, v1}, Lz0/g;->U(Lz0/a;Ljava/util/List;)V

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lz0/a;

    iget-object v2, p0, Lz0/g;->v:Lz0/e;

    iget-wide v2, v2, Lk0/g;->j:J

    invoke-direct {p0, v2, v3}, Lz0/g;->V(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3, v1}, Lz0/a;-><init>(JLjava/util/List;)V

    iput-object v0, p0, Lz0/g;->B:Lz0/a;

    goto :goto_0

    :cond_1
    const/4 v2, -0x5

    if-ne v1, v2, :cond_2

    iget-object v0, v0, Lh0/s1;->b:Lh0/r1;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/r1;

    iget-wide v0, v0, Lh0/r1;->u:J

    iput-wide v0, p0, Lz0/g;->A:J

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method protected K()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lz0/g;->B:Lz0/a;

    iput-object v0, p0, Lz0/g;->x:Lz0/c;

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lz0/g;->C:J

    return-void
.end method

.method protected M(JZ)V
    .locals 0

    const/4 p1, 0x0

    iput-object p1, p0, Lz0/g;->B:Lz0/a;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lz0/g;->y:Z

    iput-boolean p1, p0, Lz0/g;->z:Z

    return-void
.end method

.method protected Q([Lh0/r1;JJ)V
    .locals 2

    iget-object p2, p0, Lz0/g;->s:Lz0/d;

    const/4 p3, 0x0

    aget-object p1, p1, p3

    invoke-interface {p2, p1}, Lz0/d;->b(Lh0/r1;)Lz0/c;

    move-result-object p1

    iput-object p1, p0, Lz0/g;->x:Lz0/c;

    iget-object p1, p0, Lz0/g;->B:Lz0/a;

    if-eqz p1, :cond_0

    iget-wide p2, p1, Lz0/a;->g:J

    iget-wide v0, p0, Lz0/g;->C:J

    add-long/2addr p2, v0

    sub-long/2addr p2, p4

    invoke-virtual {p1, p2, p3}, Lz0/a;->f(J)Lz0/a;

    move-result-object p1

    iput-object p1, p0, Lz0/g;->B:Lz0/a;

    :cond_0
    iput-wide p4, p0, Lz0/g;->C:J

    return-void
.end method

.method public a(Lh0/r1;)I
    .locals 1

    iget-object v0, p0, Lz0/g;->s:Lz0/d;

    invoke-interface {v0, p1}, Lz0/d;->a(Lh0/r1;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget p1, p1, Lh0/r1;->L:I

    if-nez p1, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :goto_0
    invoke-static {p1}, Lh0/q3;->a(I)I

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x0

    invoke-static {p1}, Lh0/q3;->a(I)I

    move-result p1

    return p1
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lz0/g;->z:Z

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    const-string v0, "MetadataRenderer"

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 1

    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lz0/a;

    invoke-direct {p0, p1}, Lz0/g;->X(Lz0/a;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public k()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public p(JJ)V
    .locals 0

    const/4 p3, 0x1

    :goto_0
    if-eqz p3, :cond_0

    invoke-direct {p0}, Lz0/g;->Z()V

    invoke-direct {p0, p1, p2}, Lz0/g;->Y(J)Z

    move-result p3

    goto :goto_0

    :cond_0
    return-void
.end method
