.class public abstract Lz0/h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz0/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lz0/e;)Lz0/a;
    .locals 2

    iget-object v0, p1, Lk0/g;->h:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Le2/a;->a(Z)V

    invoke-virtual {p1}, Lk0/a;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p1, v0}, Lz0/h;->b(Lz0/e;Ljava/nio/ByteBuffer;)Lz0/a;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method protected abstract b(Lz0/e;Ljava/nio/ByteBuffer;)Lz0/a;
.end method
