.class Lp2/k$a;
.super Lp2/k;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lp2/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lp2/k;-><init>(Lp2/k$a;)V

    return-void
.end method


# virtual methods
.method public d(II)Lp2/k;
    .locals 0

    invoke-static {p1, p2}, Lr2/e;->e(II)I

    move-result p1

    invoke-virtual {p0, p1}, Lp2/k$a;->k(I)Lp2/k;

    move-result-object p1

    return-object p1
.end method

.method public e(JJ)Lp2/k;
    .locals 0

    invoke-static {p1, p2, p3, p4}, Lr2/g;->a(JJ)I

    move-result p1

    invoke-virtual {p0, p1}, Lp2/k$a;->k(I)Lp2/k;

    move-result-object p1

    return-object p1
.end method

.method public f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;",
            "Ljava/util/Comparator<",
            "TT;>;)",
            "Lp2/k;"
        }
    .end annotation

    invoke-interface {p3, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p1

    invoke-virtual {p0, p1}, Lp2/k$a;->k(I)Lp2/k;

    move-result-object p1

    return-object p1
.end method

.method public g(ZZ)Lp2/k;
    .locals 0

    invoke-static {p1, p2}, Lr2/a;->a(ZZ)I

    move-result p1

    invoke-virtual {p0, p1}, Lp2/k$a;->k(I)Lp2/k;

    move-result-object p1

    return-object p1
.end method

.method public h(ZZ)Lp2/k;
    .locals 0

    invoke-static {p2, p1}, Lr2/a;->a(ZZ)I

    move-result p1

    invoke-virtual {p0, p1}, Lp2/k$a;->k(I)Lp2/k;

    move-result-object p1

    return-object p1
.end method

.method public i()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method k(I)Lp2/k;
    .locals 0

    if-gez p1, :cond_0

    invoke-static {}, Lp2/k;->a()Lp2/k;

    move-result-object p1

    goto :goto_0

    :cond_0
    if-lez p1, :cond_1

    invoke-static {}, Lp2/k;->b()Lp2/k;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-static {}, Lp2/k;->c()Lp2/k;

    move-result-object p1

    :goto_0
    return-object p1
.end method
