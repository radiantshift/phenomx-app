.class Lp2/q$c;
.super Lp2/q;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lp2/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lp2/q<",
        "TE;>;"
    }
.end annotation


# instance fields
.field final transient h:I

.field final transient i:I

.field final synthetic j:Lp2/q;


# direct methods
.method constructor <init>(Lp2/q;II)V
    .locals 0

    iput-object p1, p0, Lp2/q$c;->j:Lp2/q;

    invoke-direct {p0}, Lp2/q;-><init>()V

    iput p2, p0, Lp2/q$c;->h:I

    iput p3, p0, Lp2/q$c;->i:I

    return-void
.end method


# virtual methods
.method d()[Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljavax/annotation/CheckForNull;
    .end annotation

    iget-object v0, p0, Lp2/q$c;->j:Lp2/q;

    invoke-virtual {v0}, Lp2/o;->d()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method e()I
    .locals 2

    iget-object v0, p0, Lp2/q$c;->j:Lp2/q;

    invoke-virtual {v0}, Lp2/o;->f()I

    move-result v0

    iget v1, p0, Lp2/q$c;->h:I

    add-int/2addr v0, v1

    iget v1, p0, Lp2/q$c;->i:I

    add-int/2addr v0, v1

    return v0
.end method

.method f()I
    .locals 2

    iget-object v0, p0, Lp2/q$c;->j:Lp2/q;

    invoke-virtual {v0}, Lp2/o;->f()I

    move-result v0

    iget v1, p0, Lp2/q$c;->h:I

    add-int/2addr v0, v1

    return v0
.end method

.method g()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    iget v0, p0, Lp2/q$c;->i:I

    invoke-static {p1, v0}, Lo2/k;->g(II)I

    iget-object v0, p0, Lp2/q$c;->j:Lp2/q;

    iget v1, p0, Lp2/q$c;->h:I

    add-int/2addr p1, v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-super {p0}, Lp2/q;->h()Lp2/s0;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    invoke-super {p0}, Lp2/q;->o()Lp2/t0;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 0

    invoke-super {p0, p1}, Lp2/q;->p(I)Lp2/t0;

    move-result-object p1

    return-object p1
.end method

.method public size()I
    .locals 1

    iget v0, p0, Lp2/q$c;->i:I

    return v0
.end method

.method public bridge synthetic subList(II)Ljava/util/List;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lp2/q$c;->u(II)Lp2/q;

    move-result-object p1

    return-object p1
.end method

.method public u(II)Lp2/q;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lp2/q<",
            "TE;>;"
        }
    .end annotation

    iget v0, p0, Lp2/q$c;->i:I

    invoke-static {p1, p2, v0}, Lo2/k;->m(III)V

    iget-object v0, p0, Lp2/q$c;->j:Lp2/q;

    iget v1, p0, Lp2/q$c;->h:I

    add-int/2addr p1, v1

    add-int/2addr p2, v1

    invoke-virtual {v0, p1, p2}, Lp2/q;->u(II)Lp2/q;

    move-result-object p1

    return-object p1
.end method
