.class final Lp2/k0$b;
.super Lp2/s;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lp2/k0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Lp2/s<",
        "TK;>;"
    }
.end annotation


# instance fields
.field private final transient h:Lp2/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/r<",
            "TK;*>;"
        }
    .end annotation
.end field

.field private final transient i:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "TK;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lp2/r;Lp2/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lp2/r<",
            "TK;*>;",
            "Lp2/q<",
            "TK;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lp2/s;-><init>()V

    iput-object p1, p0, Lp2/k0$b;->h:Lp2/r;

    iput-object p2, p0, Lp2/k0$b;->i:Lp2/q;

    return-void
.end method


# virtual methods
.method public b()Lp2/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lp2/q<",
            "TK;>;"
        }
    .end annotation

    iget-object v0, p0, Lp2/k0$b;->i:Lp2/q;

    return-object v0
.end method

.method c([Ljava/lang/Object;I)I
    .locals 1

    invoke-virtual {p0}, Lp2/k0$b;->b()Lp2/q;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lp2/q;->c([Ljava/lang/Object;I)I

    move-result p1

    return p1
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/CheckForNull;
        .end annotation
    .end param

    iget-object v0, p0, Lp2/k0$b;->h:Lp2/r;

    invoke-virtual {v0, p1}, Lp2/r;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method g()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public h()Lp2/s0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lp2/s0<",
            "TK;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lp2/k0$b;->b()Lp2/q;

    move-result-object v0

    invoke-virtual {v0}, Lp2/q;->h()Lp2/s0;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lp2/k0$b;->h()Lp2/s0;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lp2/k0$b;->h:Lp2/r;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
