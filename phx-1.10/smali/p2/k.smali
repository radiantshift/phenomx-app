.class public abstract Lp2/k;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lp2/k$b;
    }
.end annotation


# static fields
.field private static final a:Lp2/k;

.field private static final b:Lp2/k;

.field private static final c:Lp2/k;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lp2/k$a;

    invoke-direct {v0}, Lp2/k$a;-><init>()V

    sput-object v0, Lp2/k;->a:Lp2/k;

    new-instance v0, Lp2/k$b;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lp2/k$b;-><init>(I)V

    sput-object v0, Lp2/k;->b:Lp2/k;

    new-instance v0, Lp2/k$b;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lp2/k$b;-><init>(I)V

    sput-object v0, Lp2/k;->c:Lp2/k;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lp2/k$a;)V
    .locals 0

    invoke-direct {p0}, Lp2/k;-><init>()V

    return-void
.end method

.method static synthetic a()Lp2/k;
    .locals 1

    sget-object v0, Lp2/k;->b:Lp2/k;

    return-object v0
.end method

.method static synthetic b()Lp2/k;
    .locals 1

    sget-object v0, Lp2/k;->c:Lp2/k;

    return-object v0
.end method

.method static synthetic c()Lp2/k;
    .locals 1

    sget-object v0, Lp2/k;->a:Lp2/k;

    return-object v0
.end method

.method public static j()Lp2/k;
    .locals 1

    sget-object v0, Lp2/k;->a:Lp2/k;

    return-object v0
.end method


# virtual methods
.method public abstract d(II)Lp2/k;
.end method

.method public abstract e(JJ)Lp2/k;
.end method

.method public abstract f(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lp2/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;",
            "Ljava/util/Comparator<",
            "TT;>;)",
            "Lp2/k;"
        }
    .end annotation
.end method

.method public abstract g(ZZ)Lp2/k;
.end method

.method public abstract h(ZZ)Lp2/k;
.end method

.method public abstract i()I
.end method
