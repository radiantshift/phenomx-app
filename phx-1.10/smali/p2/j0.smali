.class Lp2/j0;
.super Lp2/q;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lp2/q<",
        "TE;>;"
    }
.end annotation


# static fields
.field static final j:Lp2/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp2/q<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final transient h:[Ljava/lang/Object;

.field private final transient i:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lp2/j0;

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    invoke-direct {v0, v2, v1}, Lp2/j0;-><init>([Ljava/lang/Object;I)V

    sput-object v0, Lp2/j0;->j:Lp2/q;

    return-void
.end method

.method constructor <init>([Ljava/lang/Object;I)V
    .locals 0

    invoke-direct {p0}, Lp2/q;-><init>()V

    iput-object p1, p0, Lp2/j0;->h:[Ljava/lang/Object;

    iput p2, p0, Lp2/j0;->i:I

    return-void
.end method


# virtual methods
.method c([Ljava/lang/Object;I)I
    .locals 3

    iget-object v0, p0, Lp2/j0;->h:[Ljava/lang/Object;

    iget v1, p0, Lp2/j0;->i:I

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lp2/j0;->i:I

    add-int/2addr p2, p1

    return p2
.end method

.method d()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lp2/j0;->h:[Ljava/lang/Object;

    return-object v0
.end method

.method e()I
    .locals 1

    iget v0, p0, Lp2/j0;->i:I

    return v0
.end method

.method f()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method g()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    iget v0, p0, Lp2/j0;->i:I

    invoke-static {p1, v0}, Lo2/k;->g(II)I

    iget-object v0, p0, Lp2/j0;->h:[Ljava/lang/Object;

    aget-object p1, v0, p1

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p1
.end method

.method public size()I
    .locals 1

    iget v0, p0, Lp2/j0;->i:I

    return v0
.end method
