.class final Lt1/c$a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lt1/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# static fields
.field private static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lt1/c$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ls1/b;

.field public final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lt1/b;->f:Lt1/b;

    sput-object v0, Lt1/c$a;->c:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/Layout$Alignment;FIIFIFZII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ls1/b$b;

    invoke-direct {v0}, Ls1/b$b;-><init>()V

    invoke-virtual {v0, p1}, Ls1/b$b;->o(Ljava/lang/CharSequence;)Ls1/b$b;

    move-result-object p1

    invoke-virtual {p1, p2}, Ls1/b$b;->p(Landroid/text/Layout$Alignment;)Ls1/b$b;

    move-result-object p1

    invoke-virtual {p1, p3, p4}, Ls1/b$b;->h(FI)Ls1/b$b;

    move-result-object p1

    invoke-virtual {p1, p5}, Ls1/b$b;->i(I)Ls1/b$b;

    move-result-object p1

    invoke-virtual {p1, p6}, Ls1/b$b;->k(F)Ls1/b$b;

    move-result-object p1

    invoke-virtual {p1, p7}, Ls1/b$b;->l(I)Ls1/b$b;

    move-result-object p1

    invoke-virtual {p1, p8}, Ls1/b$b;->n(F)Ls1/b$b;

    move-result-object p1

    if-eqz p9, :cond_0

    invoke-virtual {p1, p10}, Ls1/b$b;->s(I)Ls1/b$b;

    :cond_0
    invoke-virtual {p1}, Ls1/b$b;->a()Ls1/b;

    move-result-object p1

    iput-object p1, p0, Lt1/c$a;->a:Ls1/b;

    iput p11, p0, Lt1/c$a;->b:I

    return-void
.end method

.method public static synthetic a(Lt1/c$a;Lt1/c$a;)I
    .locals 0

    invoke-static {p0, p1}, Lt1/c$a;->c(Lt1/c$a;Lt1/c$a;)I

    move-result p0

    return p0
.end method

.method static synthetic b()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lt1/c$a;->c:Ljava/util/Comparator;

    return-object v0
.end method

.method private static synthetic c(Lt1/c$a;Lt1/c$a;)I
    .locals 0

    iget p1, p1, Lt1/c$a;->b:I

    iget p0, p0, Lt1/c$a;->b:I

    invoke-static {p1, p0}, Ljava/lang/Integer;->compare(II)I

    move-result p0

    return p0
.end method
