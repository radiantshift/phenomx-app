.class public final synthetic Le5/h;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static synthetic a(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Le5/h;->k(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic b(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Le5/h;->m(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic c(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Le5/h;->h(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic d(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Le5/h;->j(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic e(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Le5/h;->l(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static synthetic f(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Le5/h;->i(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V

    return-void
.end method

.method public static g()Lz4/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lz4/i<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lz4/r;

    invoke-direct {v0}, Lz4/r;-><init>()V

    return-object v0
.end method

.method public static synthetic h(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V
    .locals 1

    .line 1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-interface {p0}, Le5/a$b;->a()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Le5/a;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object p1

    :goto_0
    invoke-interface {p2, p1}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic i(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V
    .locals 1

    .line 1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-interface {p0}, Le5/a$b;->e()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Le5/a;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object p1

    :goto_0
    invoke-interface {p2, p1}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic j(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V
    .locals 1

    .line 1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-interface {p0}, Le5/a$b;->b()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Le5/a;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object p1

    :goto_0
    invoke-interface {p2, p1}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic k(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V
    .locals 1

    .line 1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-interface {p0}, Le5/a$b;->g()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Le5/a;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object p1

    :goto_0
    invoke-interface {p2, p1}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic l(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V
    .locals 1

    .line 1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-interface {p0}, Le5/a$b;->c()Ljava/util/List;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Le5/a;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object p1

    :goto_0
    invoke-interface {p2, p1}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic m(Le5/a$b;Ljava/lang/Object;Lz4/a$e;)V
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-static {}, Le5/a$c;->values()[Le5/a$c;

    move-result-object v2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    aget-object p1, v2, p1

    :goto_0
    :try_start_0
    invoke-interface {p0, p1}, Le5/a$b;->f(Le5/a$c;)Ljava/util/List;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p0

    invoke-static {p0}, Le5/a;->a(Ljava/lang/Throwable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_1
    invoke-interface {p2, v0}, Lz4/a$e;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static n(Lz4/c;Le5/a$b;)V
    .locals 5

    .line 1
    invoke-interface {p0}, Lz4/c;->c()Lz4/c$c;

    move-result-object v0

    new-instance v1, Lz4/a;

    invoke-static {}, Le5/h;->g()Lz4/i;

    move-result-object v2

    const-string v3, "dev.flutter.pigeon.PathProviderApi.getTemporaryPath"

    invoke-direct {v1, p0, v3, v2, v0}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;Lz4/c$c;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-instance v2, Le5/d;

    invoke-direct {v2, p1}, Le5/d;-><init>(Le5/a$b;)V

    invoke-virtual {v1, v2}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v0}, Lz4/a;->e(Lz4/a$d;)V

    :goto_0
    invoke-interface {p0}, Lz4/c;->c()Lz4/c$c;

    move-result-object v1

    new-instance v2, Lz4/a;

    invoke-static {}, Le5/h;->g()Lz4/i;

    move-result-object v3

    const-string v4, "dev.flutter.pigeon.PathProviderApi.getApplicationSupportPath"

    invoke-direct {v2, p0, v4, v3, v1}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;Lz4/c$c;)V

    if-eqz p1, :cond_1

    new-instance v1, Le5/g;

    invoke-direct {v1, p1}, Le5/g;-><init>(Le5/a$b;)V

    invoke-virtual {v2, v1}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v2, v0}, Lz4/a;->e(Lz4/a$d;)V

    :goto_1
    invoke-interface {p0}, Lz4/c;->c()Lz4/c$c;

    move-result-object v1

    new-instance v2, Lz4/a;

    invoke-static {}, Le5/h;->g()Lz4/i;

    move-result-object v3

    const-string v4, "dev.flutter.pigeon.PathProviderApi.getApplicationDocumentsPath"

    invoke-direct {v2, p0, v4, v3, v1}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;Lz4/c$c;)V

    if-eqz p1, :cond_2

    new-instance v1, Le5/e;

    invoke-direct {v1, p1}, Le5/e;-><init>(Le5/a$b;)V

    invoke-virtual {v2, v1}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2, v0}, Lz4/a;->e(Lz4/a$d;)V

    :goto_2
    invoke-interface {p0}, Lz4/c;->c()Lz4/c$c;

    move-result-object v1

    new-instance v2, Lz4/a;

    invoke-static {}, Le5/h;->g()Lz4/i;

    move-result-object v3

    const-string v4, "dev.flutter.pigeon.PathProviderApi.getExternalStoragePath"

    invoke-direct {v2, p0, v4, v3, v1}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;Lz4/c$c;)V

    if-eqz p1, :cond_3

    new-instance v1, Le5/b;

    invoke-direct {v1, p1}, Le5/b;-><init>(Le5/a$b;)V

    invoke-virtual {v2, v1}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_3

    :cond_3
    invoke-virtual {v2, v0}, Lz4/a;->e(Lz4/a$d;)V

    :goto_3
    invoke-interface {p0}, Lz4/c;->c()Lz4/c$c;

    move-result-object v1

    new-instance v2, Lz4/a;

    invoke-static {}, Le5/h;->g()Lz4/i;

    move-result-object v3

    const-string v4, "dev.flutter.pigeon.PathProviderApi.getExternalCachePaths"

    invoke-direct {v2, p0, v4, v3, v1}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;Lz4/c$c;)V

    if-eqz p1, :cond_4

    new-instance v1, Le5/f;

    invoke-direct {v1, p1}, Le5/f;-><init>(Le5/a$b;)V

    invoke-virtual {v2, v1}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_4

    :cond_4
    invoke-virtual {v2, v0}, Lz4/a;->e(Lz4/a$d;)V

    :goto_4
    invoke-interface {p0}, Lz4/c;->c()Lz4/c$c;

    move-result-object v1

    new-instance v2, Lz4/a;

    invoke-static {}, Le5/h;->g()Lz4/i;

    move-result-object v3

    const-string v4, "dev.flutter.pigeon.PathProviderApi.getExternalStoragePaths"

    invoke-direct {v2, p0, v4, v3, v1}, Lz4/a;-><init>(Lz4/c;Ljava/lang/String;Lz4/i;Lz4/c$c;)V

    if-eqz p1, :cond_5

    new-instance p0, Le5/c;

    invoke-direct {p0, p1}, Le5/c;-><init>(Le5/a$b;)V

    invoke-virtual {v2, p0}, Lz4/a;->e(Lz4/a$d;)V

    goto :goto_5

    :cond_5
    invoke-virtual {v2, v0}, Lz4/a;->e(Lz4/a$d;)V

    :goto_5
    return-void
.end method
