.class public Lio/flutter/embedding/engine/a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/flutter/embedding/engine/a$b;
    }
.end annotation


# instance fields
.field private final a:Lio/flutter/embedding/engine/FlutterJNI;

.field private final b:Lx4/a;

.field private final c:Lm4/a;

.field private final d:Lio/flutter/embedding/engine/c;

.field private final e:La5/a;

.field private final f:Ly4/a;

.field private final g:Ly4/b;

.field private final h:Ly4/e;

.field private final i:Ly4/f;

.field private final j:Ly4/g;

.field private final k:Ly4/h;

.field private final l:Ly4/l;

.field private final m:Ly4/i;

.field private final n:Ly4/m;

.field private final o:Ly4/n;

.field private final p:Ly4/o;

.field private final q:Ly4/p;

.field private final r:Lio/flutter/plugin/platform/q;

.field private final s:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lio/flutter/embedding/engine/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Lio/flutter/embedding/engine/a$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lo4/d;Lio/flutter/embedding/engine/FlutterJNI;Lio/flutter/plugin/platform/q;[Ljava/lang/String;ZZ)V
    .locals 9

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lio/flutter/embedding/engine/a;-><init>(Landroid/content/Context;Lo4/d;Lio/flutter/embedding/engine/FlutterJNI;Lio/flutter/plugin/platform/q;[Ljava/lang/String;ZZLio/flutter/embedding/engine/d;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lo4/d;Lio/flutter/embedding/engine/FlutterJNI;Lio/flutter/plugin/platform/q;[Ljava/lang/String;ZZLio/flutter/embedding/engine/d;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lio/flutter/embedding/engine/a;->s:Ljava/util/Set;

    new-instance v0, Lio/flutter/embedding/engine/a$a;

    invoke-direct {v0, p0}, Lio/flutter/embedding/engine/a$a;-><init>(Lio/flutter/embedding/engine/a;)V

    iput-object v0, p0, Lio/flutter/embedding/engine/a;->t:Lio/flutter/embedding/engine/a$b;

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    :goto_0
    invoke-static {}, Ll4/a;->e()Ll4/a;

    move-result-object v1

    if-nez p3, :cond_0

    invoke-virtual {v1}, Ll4/a;->d()Lio/flutter/embedding/engine/FlutterJNI$c;

    move-result-object p3

    invoke-virtual {p3}, Lio/flutter/embedding/engine/FlutterJNI$c;->a()Lio/flutter/embedding/engine/FlutterJNI;

    move-result-object p3

    :cond_0
    iput-object p3, p0, Lio/flutter/embedding/engine/a;->a:Lio/flutter/embedding/engine/FlutterJNI;

    new-instance v2, Lm4/a;

    invoke-direct {v2, p3, v0}, Lm4/a;-><init>(Lio/flutter/embedding/engine/FlutterJNI;Landroid/content/res/AssetManager;)V

    iput-object v2, p0, Lio/flutter/embedding/engine/a;->c:Lm4/a;

    invoke-virtual {v2}, Lm4/a;->n()V

    invoke-static {}, Ll4/a;->e()Ll4/a;

    move-result-object v0

    invoke-virtual {v0}, Ll4/a;->a()Ln4/a;

    move-result-object v0

    new-instance v3, Ly4/a;

    invoke-direct {v3, v2, p3}, Ly4/a;-><init>(Lm4/a;Lio/flutter/embedding/engine/FlutterJNI;)V

    iput-object v3, p0, Lio/flutter/embedding/engine/a;->f:Ly4/a;

    new-instance v3, Ly4/b;

    invoke-direct {v3, v2}, Ly4/b;-><init>(Lm4/a;)V

    iput-object v3, p0, Lio/flutter/embedding/engine/a;->g:Ly4/b;

    new-instance v4, Ly4/e;

    invoke-direct {v4, v2}, Ly4/e;-><init>(Lm4/a;)V

    iput-object v4, p0, Lio/flutter/embedding/engine/a;->h:Ly4/e;

    new-instance v4, Ly4/f;

    invoke-direct {v4, v2}, Ly4/f;-><init>(Lm4/a;)V

    iput-object v4, p0, Lio/flutter/embedding/engine/a;->i:Ly4/f;

    new-instance v5, Ly4/g;

    invoke-direct {v5, v2}, Ly4/g;-><init>(Lm4/a;)V

    iput-object v5, p0, Lio/flutter/embedding/engine/a;->j:Ly4/g;

    new-instance v5, Ly4/h;

    invoke-direct {v5, v2}, Ly4/h;-><init>(Lm4/a;)V

    iput-object v5, p0, Lio/flutter/embedding/engine/a;->k:Ly4/h;

    new-instance v5, Ly4/i;

    invoke-direct {v5, v2}, Ly4/i;-><init>(Lm4/a;)V

    iput-object v5, p0, Lio/flutter/embedding/engine/a;->m:Ly4/i;

    new-instance v5, Ly4/l;

    invoke-direct {v5, v2, p7}, Ly4/l;-><init>(Lm4/a;Z)V

    iput-object v5, p0, Lio/flutter/embedding/engine/a;->l:Ly4/l;

    new-instance p7, Ly4/m;

    invoke-direct {p7, v2}, Ly4/m;-><init>(Lm4/a;)V

    iput-object p7, p0, Lio/flutter/embedding/engine/a;->n:Ly4/m;

    new-instance p7, Ly4/n;

    invoke-direct {p7, v2}, Ly4/n;-><init>(Lm4/a;)V

    iput-object p7, p0, Lio/flutter/embedding/engine/a;->o:Ly4/n;

    new-instance p7, Ly4/o;

    invoke-direct {p7, v2}, Ly4/o;-><init>(Lm4/a;)V

    iput-object p7, p0, Lio/flutter/embedding/engine/a;->p:Ly4/o;

    new-instance p7, Ly4/p;

    invoke-direct {p7, v2}, Ly4/p;-><init>(Lm4/a;)V

    iput-object p7, p0, Lio/flutter/embedding/engine/a;->q:Ly4/p;

    if-eqz v0, :cond_1

    invoke-interface {v0, v3}, Ln4/a;->f(Ly4/b;)V

    :cond_1
    new-instance p7, La5/a;

    invoke-direct {p7, p1, v4}, La5/a;-><init>(Landroid/content/Context;Ly4/f;)V

    iput-object p7, p0, Lio/flutter/embedding/engine/a;->e:La5/a;

    if-nez p2, :cond_2

    invoke-virtual {v1}, Ll4/a;->c()Lo4/d;

    move-result-object p2

    :cond_2
    invoke-virtual {p3}, Lio/flutter/embedding/engine/FlutterJNI;->isAttached()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p2, v0}, Lo4/d;->m(Landroid/content/Context;)V

    invoke-virtual {p2, p1, p5}, Lo4/d;->e(Landroid/content/Context;[Ljava/lang/String;)V

    :cond_3
    iget-object p5, p0, Lio/flutter/embedding/engine/a;->t:Lio/flutter/embedding/engine/a$b;

    invoke-virtual {p3, p5}, Lio/flutter/embedding/engine/FlutterJNI;->addEngineLifecycleListener(Lio/flutter/embedding/engine/a$b;)V

    invoke-virtual {p3, p4}, Lio/flutter/embedding/engine/FlutterJNI;->setPlatformViewsController(Lio/flutter/plugin/platform/q;)V

    invoke-virtual {p3, p7}, Lio/flutter/embedding/engine/FlutterJNI;->setLocalizationPlugin(La5/a;)V

    invoke-virtual {v1}, Ll4/a;->a()Ln4/a;

    move-result-object p5

    invoke-virtual {p3, p5}, Lio/flutter/embedding/engine/FlutterJNI;->setDeferredComponentManager(Ln4/a;)V

    invoke-virtual {p3}, Lio/flutter/embedding/engine/FlutterJNI;->isAttached()Z

    move-result p5

    if-nez p5, :cond_4

    invoke-direct {p0}, Lio/flutter/embedding/engine/a;->e()V

    :cond_4
    new-instance p5, Lx4/a;

    invoke-direct {p5, p3}, Lx4/a;-><init>(Lio/flutter/embedding/engine/FlutterJNI;)V

    iput-object p5, p0, Lio/flutter/embedding/engine/a;->b:Lx4/a;

    iput-object p4, p0, Lio/flutter/embedding/engine/a;->r:Lio/flutter/plugin/platform/q;

    invoke-virtual {p4}, Lio/flutter/plugin/platform/q;->V()V

    new-instance p3, Lio/flutter/embedding/engine/c;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p4

    invoke-direct {p3, p4, p0, p2, p8}, Lio/flutter/embedding/engine/c;-><init>(Landroid/content/Context;Lio/flutter/embedding/engine/a;Lo4/d;Lio/flutter/embedding/engine/d;)V

    iput-object p3, p0, Lio/flutter/embedding/engine/a;->d:Lio/flutter/embedding/engine/c;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    invoke-virtual {p7, p1}, La5/a;->d(Landroid/content/res/Configuration;)V

    if-eqz p6, :cond_5

    invoke-virtual {p2}, Lo4/d;->d()Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-static {p0}, Lw4/a;->a(Lio/flutter/embedding/engine/a;)V

    :cond_5
    return-void
.end method

.method static synthetic a(Lio/flutter/embedding/engine/a;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lio/flutter/embedding/engine/a;->s:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic b(Lio/flutter/embedding/engine/a;)Lio/flutter/plugin/platform/q;
    .locals 0

    iget-object p0, p0, Lio/flutter/embedding/engine/a;->r:Lio/flutter/plugin/platform/q;

    return-object p0
.end method

.method static synthetic c(Lio/flutter/embedding/engine/a;)Ly4/l;
    .locals 0

    iget-object p0, p0, Lio/flutter/embedding/engine/a;->l:Ly4/l;

    return-object p0
.end method

.method private e()V
    .locals 2

    const-string v0, "FlutterEngine"

    const-string v1, "Attaching to JNI."

    invoke-static {v0, v1}, Ll4/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->a:Lio/flutter/embedding/engine/FlutterJNI;

    invoke-virtual {v0}, Lio/flutter/embedding/engine/FlutterJNI;->attachToNative()V

    invoke-direct {p0}, Lio/flutter/embedding/engine/a;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "FlutterEngine failed to attach to its native Object reference."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private w()Z
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->a:Lio/flutter/embedding/engine/FlutterJNI;

    invoke-virtual {v0}, Lio/flutter/embedding/engine/FlutterJNI;->isAttached()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public d(Lio/flutter/embedding/engine/a$b;)V
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->s:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public f()V
    .locals 2

    const-string v0, "FlutterEngine"

    const-string v1, "Destroying."

    invoke-static {v0, v1}, Ll4/b;->f(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/flutter/embedding/engine/a$b;

    invoke-interface {v1}, Lio/flutter/embedding/engine/a$b;->a()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lio/flutter/embedding/engine/a;->d:Lio/flutter/embedding/engine/c;

    invoke-virtual {v0}, Lio/flutter/embedding/engine/c;->l()V

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->r:Lio/flutter/plugin/platform/q;

    invoke-virtual {v0}, Lio/flutter/plugin/platform/q;->X()V

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->c:Lm4/a;

    invoke-virtual {v0}, Lm4/a;->o()V

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->a:Lio/flutter/embedding/engine/FlutterJNI;

    iget-object v1, p0, Lio/flutter/embedding/engine/a;->t:Lio/flutter/embedding/engine/a$b;

    invoke-virtual {v0, v1}, Lio/flutter/embedding/engine/FlutterJNI;->removeEngineLifecycleListener(Lio/flutter/embedding/engine/a$b;)V

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->a:Lio/flutter/embedding/engine/FlutterJNI;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/flutter/embedding/engine/FlutterJNI;->setDeferredComponentManager(Ln4/a;)V

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->a:Lio/flutter/embedding/engine/FlutterJNI;

    invoke-virtual {v0}, Lio/flutter/embedding/engine/FlutterJNI;->detachFromNativeAndReleaseResources()V

    invoke-static {}, Ll4/a;->e()Ll4/a;

    move-result-object v0

    invoke-virtual {v0}, Ll4/a;->a()Ln4/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Ll4/a;->e()Ll4/a;

    move-result-object v0

    invoke-virtual {v0}, Ll4/a;->a()Ln4/a;

    move-result-object v0

    invoke-interface {v0}, Ln4/a;->d()V

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->g:Ly4/b;

    invoke-virtual {v0, v1}, Ly4/b;->c(Ln4/a;)V

    :cond_1
    return-void
.end method

.method public g()Ly4/a;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->f:Ly4/a;

    return-object v0
.end method

.method public h()Lr4/b;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->d:Lio/flutter/embedding/engine/c;

    return-object v0
.end method

.method public i()Lm4/a;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->c:Lm4/a;

    return-object v0
.end method

.method public j()Ly4/e;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->h:Ly4/e;

    return-object v0
.end method

.method public k()La5/a;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->e:La5/a;

    return-object v0
.end method

.method public l()Ly4/g;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->j:Ly4/g;

    return-object v0
.end method

.method public m()Ly4/h;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->k:Ly4/h;

    return-object v0
.end method

.method public n()Ly4/i;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->m:Ly4/i;

    return-object v0
.end method

.method public o()Lio/flutter/plugin/platform/q;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->r:Lio/flutter/plugin/platform/q;

    return-object v0
.end method

.method public p()Lq4/b;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->d:Lio/flutter/embedding/engine/c;

    return-object v0
.end method

.method public q()Lx4/a;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->b:Lx4/a;

    return-object v0
.end method

.method public r()Ly4/l;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->l:Ly4/l;

    return-object v0
.end method

.method public s()Ly4/m;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->n:Ly4/m;

    return-object v0
.end method

.method public t()Ly4/n;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->o:Ly4/n;

    return-object v0
.end method

.method public u()Ly4/o;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->p:Ly4/o;

    return-object v0
.end method

.method public v()Ly4/p;
    .locals 1

    iget-object v0, p0, Lio/flutter/embedding/engine/a;->q:Ly4/p;

    return-object v0
.end method

.method x(Landroid/content/Context;Lm4/a$b;Ljava/lang/String;Ljava/util/List;Lio/flutter/plugin/platform/q;ZZ)Lio/flutter/embedding/engine/a;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lm4/a$b;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lio/flutter/plugin/platform/q;",
            "ZZ)",
            "Lio/flutter/embedding/engine/a;"
        }
    .end annotation

    move-object v0, p2

    invoke-direct {p0}, Lio/flutter/embedding/engine/a;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, p0

    iget-object v2, v1, Lio/flutter/embedding/engine/a;->a:Lio/flutter/embedding/engine/FlutterJNI;

    iget-object v3, v0, Lm4/a$b;->c:Ljava/lang/String;

    iget-object v0, v0, Lm4/a$b;->b:Ljava/lang/String;

    move-object v4, p3

    move-object/from16 v5, p4

    invoke-virtual {v2, v3, v0, p3, v5}, Lio/flutter/embedding/engine/FlutterJNI;->spawn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lio/flutter/embedding/engine/FlutterJNI;

    move-result-object v7

    new-instance v0, Lio/flutter/embedding/engine/a;

    const/4 v6, 0x0

    const/4 v9, 0x0

    move-object v4, v0

    move-object v5, p1

    move-object/from16 v8, p5

    move/from16 v10, p6

    move/from16 v11, p7

    invoke-direct/range {v4 .. v11}, Lio/flutter/embedding/engine/a;-><init>(Landroid/content/Context;Lo4/d;Lio/flutter/embedding/engine/FlutterJNI;Lio/flutter/plugin/platform/q;[Ljava/lang/String;ZZ)V

    return-object v0

    :cond_0
    move-object v1, p0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Spawn can only be called on a fully constructed FlutterEngine"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
