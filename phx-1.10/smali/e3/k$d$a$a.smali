.class Le3/k$d$a$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ln2/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Le3/k$d$a;->a()Ln2/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ln2/h<",
        "Ll3/d;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/concurrent/Executor;

.field final synthetic b:Le3/k$d$a;


# direct methods
.method constructor <init>(Le3/k$d$a;Ljava/util/concurrent/Executor;)V
    .locals 0

    iput-object p1, p0, Le3/k$d$a$a;->b:Le3/k$d$a;

    iput-object p2, p0, Le3/k$d$a$a;->a:Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ln2/i;
    .locals 0

    check-cast p1, Ll3/d;

    invoke-virtual {p0, p1}, Le3/k$d$a$a;->b(Ll3/d;)Ln2/i;

    move-result-object p1

    return-object p1
.end method

.method public b(Ll3/d;)Ln2/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll3/d;",
            ")",
            "Ln2/i<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object p1

    const-string v1, "Received null app settings at app startup. Cannot send cached reports"

    invoke-virtual {p1, v1}, Lb3/f;->k(Ljava/lang/String;)V

    :goto_0
    invoke-static {v0}, Ln2/l;->d(Ljava/lang/Object;)Ln2/i;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object p1, p0, Le3/k$d$a$a;->b:Le3/k$d$a;

    iget-object p1, p1, Le3/k$d$a;->b:Le3/k$d;

    iget-object p1, p1, Le3/k$d;->b:Le3/k;

    invoke-static {p1}, Le3/k;->n(Le3/k;)Ln2/i;

    iget-object p1, p0, Le3/k$d$a$a;->b:Le3/k$d$a;

    iget-object p1, p1, Le3/k$d$a;->b:Le3/k$d;

    iget-object p1, p1, Le3/k$d;->b:Le3/k;

    invoke-static {p1}, Le3/k;->h(Le3/k;)Le3/e0;

    move-result-object p1

    iget-object v1, p0, Le3/k$d$a$a;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p1, v1}, Le3/e0;->v(Ljava/util/concurrent/Executor;)Ln2/i;

    iget-object p1, p0, Le3/k$d$a$a;->b:Le3/k$d$a;

    iget-object p1, p1, Le3/k$d$a;->b:Le3/k$d;

    iget-object p1, p1, Le3/k$d;->b:Le3/k;

    iget-object p1, p1, Le3/k;->q:Ln2/j;

    invoke-virtual {p1, v0}, Ln2/j;->e(Ljava/lang/Object;)Z

    goto :goto_0
.end method
