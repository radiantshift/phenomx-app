.class Le3/k$b$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ln2/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Le3/k$b;->a()Ln2/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ln2/h<",
        "Ll3/d;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/concurrent/Executor;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Le3/k$b;


# direct methods
.method constructor <init>(Le3/k$b;Ljava/util/concurrent/Executor;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Le3/k$b$a;->c:Le3/k$b;

    iput-object p2, p0, Le3/k$b$a;->a:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Le3/k$b$a;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ln2/i;
    .locals 0

    check-cast p1, Ll3/d;

    invoke-virtual {p0, p1}, Le3/k$b$a;->b(Ll3/d;)Ln2/i;

    move-result-object p1

    return-object p1
.end method

.method public b(Ll3/d;)Ln2/i;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll3/d;",
            ")",
            "Ln2/i<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object p1

    const-string v1, "Received null app settings, cannot send reports at crash time."

    invoke-virtual {p1, v1}, Lb3/f;->k(Ljava/lang/String;)V

    invoke-static {v0}, Ln2/l;->d(Ljava/lang/Object;)Ln2/i;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x2

    new-array p1, p1, [Ln2/i;

    const/4 v1, 0x0

    iget-object v2, p0, Le3/k$b$a;->c:Le3/k$b;

    iget-object v2, v2, Le3/k$b;->f:Le3/k;

    invoke-static {v2}, Le3/k;->n(Le3/k;)Ln2/i;

    move-result-object v2

    aput-object v2, p1, v1

    const/4 v1, 0x1

    iget-object v2, p0, Le3/k$b$a;->c:Le3/k$b;

    iget-object v2, v2, Le3/k$b;->f:Le3/k;

    invoke-static {v2}, Le3/k;->h(Le3/k;)Le3/e0;

    move-result-object v2

    iget-object v3, p0, Le3/k$b$a;->a:Ljava/util/concurrent/Executor;

    iget-object v4, p0, Le3/k$b$a;->c:Le3/k$b;

    iget-boolean v4, v4, Le3/k$b;->e:Z

    if-eqz v4, :cond_1

    iget-object v0, p0, Le3/k$b$a;->b:Ljava/lang/String;

    :cond_1
    invoke-virtual {v2, v3, v0}, Le3/e0;->w(Ljava/util/concurrent/Executor;Ljava/lang/String;)Ln2/i;

    move-result-object v0

    aput-object v0, p1, v1

    invoke-static {p1}, Ln2/l;->f([Ln2/i;)Ln2/i;

    move-result-object p1

    return-object p1
.end method
