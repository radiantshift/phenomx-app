.class public Le3/m;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lt2/e;

.field private final c:Le3/s;

.field private final d:Le3/b0;

.field private final e:J

.field private f:Le3/n;

.field private g:Le3/n;

.field private h:Z

.field private i:Le3/k;

.field private final j:Le3/w;

.field private final k:Lj3/f;

.field public final l:Ld3/b;

.field private final m:Lc3/a;

.field private final n:Ljava/util/concurrent/ExecutorService;

.field private final o:Le3/i;

.field private final p:Lb3/a;


# direct methods
.method public constructor <init>(Lt2/e;Le3/w;Lb3/a;Le3/s;Ld3/b;Lc3/a;Lj3/f;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le3/m;->b:Lt2/e;

    iput-object p4, p0, Le3/m;->c:Le3/s;

    invoke-virtual {p1}, Lt2/e;->l()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Le3/m;->a:Landroid/content/Context;

    iput-object p2, p0, Le3/m;->j:Le3/w;

    iput-object p3, p0, Le3/m;->p:Lb3/a;

    iput-object p5, p0, Le3/m;->l:Ld3/b;

    iput-object p6, p0, Le3/m;->m:Lc3/a;

    iput-object p8, p0, Le3/m;->n:Ljava/util/concurrent/ExecutorService;

    iput-object p7, p0, Le3/m;->k:Lj3/f;

    new-instance p1, Le3/i;

    invoke-direct {p1, p8}, Le3/i;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object p1, p0, Le3/m;->o:Le3/i;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Le3/m;->e:J

    new-instance p1, Le3/b0;

    invoke-direct {p1}, Le3/b0;-><init>()V

    iput-object p1, p0, Le3/m;->d:Le3/b0;

    return-void
.end method

.method static synthetic a(Le3/m;Ll3/i;)Ln2/i;
    .locals 0

    invoke-direct {p0, p1}, Le3/m;->i(Ll3/i;)Ln2/i;

    move-result-object p0

    return-object p0
.end method

.method static synthetic b(Le3/m;)Le3/n;
    .locals 0

    iget-object p0, p0, Le3/m;->f:Le3/n;

    return-object p0
.end method

.method static synthetic c(Le3/m;)Le3/k;
    .locals 0

    iget-object p0, p0, Le3/m;->i:Le3/k;

    return-object p0
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Le3/m;->o:Le3/i;

    new-instance v1, Le3/m$d;

    invoke-direct {v1, p0}, Le3/m$d;-><init>(Le3/m;)V

    invoke-virtual {v0, v1}, Le3/i;->h(Ljava/util/concurrent/Callable;)Ln2/i;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Le3/l0;->f(Ln2/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Le3/m;->h:Z

    return-void

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i(Ll3/i;)Ln2/i;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll3/i;",
            ")",
            "Ln2/i<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const-string v0, "Collection of crash reports disabled in Crashlytics settings."

    invoke-virtual {p0}, Le3/m;->r()V

    :try_start_0
    iget-object v1, p0, Le3/m;->l:Ld3/b;

    new-instance v2, Le3/l;

    invoke-direct {v2, p0}, Le3/l;-><init>(Le3/m;)V

    invoke-interface {v1, v2}, Ld3/b;->a(Ld3/a;)V

    iget-object v1, p0, Le3/m;->i:Le3/k;

    invoke-virtual {v1}, Le3/k;->V()V

    invoke-interface {p1}, Ll3/i;->b()Ll3/d;

    move-result-object v1

    iget-object v1, v1, Ll3/d;->b:Ll3/d$a;

    iget-boolean v1, v1, Ll3/d$a;->a:Z

    if-nez v1, :cond_0

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object p1

    invoke-virtual {p1, v0}, Lb3/f;->b(Ljava/lang/String;)V

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ln2/l;->c(Ljava/lang/Exception;)Ln2/i;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Le3/m;->q()V

    return-object p1

    :cond_0
    :try_start_1
    iget-object v0, p0, Le3/m;->i:Le3/k;

    invoke-virtual {v0, p1}, Le3/k;->B(Ll3/i;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v1, "Previous sessions could not be finalized."

    invoke-virtual {v0, v1}, Lb3/f;->k(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Le3/m;->i:Le3/k;

    invoke-interface {p1}, Ll3/i;->a()Ln2/i;

    move-result-object p1

    invoke-virtual {v0, p1}, Le3/k;->a0(Ln2/i;)Ln2/i;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Le3/m;->q()V

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v1, "Crashlytics encountered a problem during asynchronous initialization."

    invoke-virtual {v0, v1, p1}, Lb3/f;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {p1}, Ln2/l;->c(Ljava/lang/Exception;)Ln2/i;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Le3/m;->q()V

    return-object p1

    :goto_0
    invoke-virtual {p0}, Le3/m;->q()V

    throw p1
.end method

.method private k(Ll3/i;)V
    .locals 3

    new-instance v0, Le3/m$b;

    invoke-direct {v0, p0, p1}, Le3/m$b;-><init>(Le3/m;Ll3/i;)V

    iget-object p1, p0, Le3/m;->n:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v1, "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously."

    invoke-virtual {v0, v1}, Lb3/f;->b(Ljava/lang/String;)V

    const-wide/16 v0, 0x3

    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v0, v1, v2}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v1, "Crashlytics timed out during initialization."

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v1, "Crashlytics encountered a problem during initialization."

    goto :goto_0

    :catch_2
    move-exception p1

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v1, "Crashlytics was interrupted during initialization."

    :goto_0
    invoke-virtual {v0, v1, p1}, Lb3/f;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method public static l()Ljava/lang/String;
    .locals 1

    const-string v0, "18.3.7"

    return-object v0
.end method

.method static m(Ljava/lang/String;Z)Z
    .locals 2

    const/4 v0, 0x1

    if-nez p1, :cond_0

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object p0

    const-string p1, "Configured not to require a build ID."

    invoke-virtual {p0, p1}, Lb3/f;->i(Ljava/lang/String;)V

    return v0

    :cond_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    return v0

    :cond_1
    const-string p0, "FirebaseCrashlytics"

    const-string p1, "."

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, ".     |  | "

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, ".     |  |"

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".   \\ |  | /"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".    \\    /"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".     \\  /"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".      \\/"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "The Crashlytics build ID is missing. This occurs when the Crashlytics Gradle plugin is missing from your app\'s build configuration. Please review the Firebase Crashlytics onboarding instructions at https://firebase.google.com/docs/crashlytics/get-started?platform=android#add-plugin"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".      /\\"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".     /  \\"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".    /    \\"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".   / |  | \\"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method public e()Ln2/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ln2/i<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Le3/m;->i:Le3/k;

    invoke-virtual {v0}, Le3/k;->o()Ln2/i;

    move-result-object v0

    return-object v0
.end method

.method public f()Ln2/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ln2/i<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Le3/m;->i:Le3/k;

    invoke-virtual {v0}, Le3/k;->t()Ln2/i;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Le3/m;->h:Z

    return v0
.end method

.method h()Z
    .locals 1

    iget-object v0, p0, Le3/m;->f:Le3/n;

    invoke-virtual {v0}, Le3/n;->c()Z

    move-result v0

    return v0
.end method

.method public j(Ll3/i;)Ln2/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll3/i;",
            ")",
            "Ln2/i<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Le3/m;->n:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Le3/m$a;

    invoke-direct {v1, p0, p1}, Le3/m$a;-><init>(Le3/m;Ll3/i;)V

    invoke-static {v0, v1}, Le3/l0;->h(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Ln2/i;

    move-result-object p1

    return-object p1
.end method

.method public n(Ljava/lang/String;)V
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Le3/m;->e:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Le3/m;->i:Le3/k;

    invoke-virtual {v2, v0, v1, p1}, Le3/k;->e0(JLjava/lang/String;)V

    return-void
.end method

.method public o(Ljava/lang/Throwable;)V
    .locals 2

    iget-object v0, p0, Le3/m;->i:Le3/k;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Le3/k;->d0(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    return-void
.end method

.method public p(Ljava/lang/Throwable;)V
    .locals 3

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Recorded on-demand fatal events: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Le3/m;->d:Le3/b0;

    invoke-virtual {v2}, Le3/b0;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lb3/f;->b(Ljava/lang/String;)V

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Dropped on-demand fatal events: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Le3/m;->d:Le3/b0;

    invoke-virtual {v2}, Le3/b0;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lb3/f;->b(Ljava/lang/String;)V

    iget-object v0, p0, Le3/m;->i:Le3/k;

    iget-object v1, p0, Le3/m;->d:Le3/b0;

    invoke-virtual {v1}, Le3/b0;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.crashlytics.on-demand.recorded-exceptions"

    invoke-virtual {v0, v2, v1}, Le3/k;->Y(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Le3/m;->i:Le3/k;

    iget-object v1, p0, Le3/m;->d:Le3/b0;

    invoke-virtual {v1}, Le3/b0;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.crashlytics.on-demand.dropped-exceptions"

    invoke-virtual {v0, v2, v1}, Le3/k;->Y(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Le3/m;->i:Le3/k;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Le3/k;->Q(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    return-void
.end method

.method q()V
    .locals 2

    iget-object v0, p0, Le3/m;->o:Le3/i;

    new-instance v1, Le3/m$c;

    invoke-direct {v1, p0}, Le3/m$c;-><init>(Le3/m;)V

    invoke-virtual {v0, v1}, Le3/i;->h(Ljava/util/concurrent/Callable;)Ln2/i;

    return-void
.end method

.method r()V
    .locals 2

    iget-object v0, p0, Le3/m;->o:Le3/i;

    invoke-virtual {v0}, Le3/i;->b()V

    iget-object v0, p0, Le3/m;->f:Le3/n;

    invoke-virtual {v0}, Le3/n;->a()Z

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v1, "Initialization marker file was created."

    invoke-virtual {v0, v1}, Lb3/f;->i(Ljava/lang/String;)V

    return-void
.end method

.method public s(Le3/a;Ll3/i;)Z
    .locals 26

    move-object/from16 v1, p0

    move-object/from16 v0, p2

    iget-object v2, v1, Le3/m;->a:Landroid/content/Context;

    const-string v3, "com.crashlytics.RequireBuildId"

    const/4 v11, 0x1

    invoke-static {v2, v3, v11}, Le3/h;->k(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v15, p1

    iget-object v3, v15, Le3/a;->b:Ljava/lang/String;

    invoke-static {v3, v2}, Le3/m;->m(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Le3/g;

    iget-object v3, v1, Le3/m;->j:Le3/w;

    invoke-direct {v2, v3}, Le3/g;-><init>(Le3/w;)V

    invoke-virtual {v2}, Le3/g;->toString()Ljava/lang/String;

    move-result-object v14

    const/16 v25, 0x0

    :try_start_0
    new-instance v2, Le3/n;

    const-string v3, "crash_marker"

    iget-object v4, v1, Le3/m;->k:Lj3/f;

    invoke-direct {v2, v3, v4}, Le3/n;-><init>(Ljava/lang/String;Lj3/f;)V

    iput-object v2, v1, Le3/m;->g:Le3/n;

    new-instance v2, Le3/n;

    const-string v3, "initialization_marker"

    iget-object v4, v1, Le3/m;->k:Lj3/f;

    invoke-direct {v2, v3, v4}, Le3/n;-><init>(Ljava/lang/String;Lj3/f;)V

    iput-object v2, v1, Le3/m;->f:Le3/n;

    new-instance v13, Lf3/i;

    iget-object v2, v1, Le3/m;->k:Lj3/f;

    iget-object v3, v1, Le3/m;->o:Le3/i;

    invoke-direct {v13, v14, v2, v3}, Lf3/i;-><init>(Ljava/lang/String;Lj3/f;Le3/i;)V

    new-instance v12, Lf3/c;

    iget-object v2, v1, Le3/m;->k:Lj3/f;

    invoke-direct {v12, v2}, Lf3/c;-><init>(Lj3/f;)V

    new-instance v8, Lm3/a;

    const/16 v2, 0x400

    new-array v3, v11, [Lm3/d;

    new-instance v4, Lm3/c;

    const/16 v5, 0xa

    invoke-direct {v4, v5}, Lm3/c;-><init>(I)V

    aput-object v4, v3, v25

    invoke-direct {v8, v2, v3}, Lm3/a;-><init>(I[Lm3/d;)V

    iget-object v2, v1, Le3/m;->a:Landroid/content/Context;

    iget-object v3, v1, Le3/m;->j:Le3/w;

    iget-object v4, v1, Le3/m;->k:Lj3/f;

    iget-object v10, v1, Le3/m;->d:Le3/b0;

    move-object/from16 v5, p1

    move-object v6, v12

    move-object v7, v13

    move-object/from16 v9, p2

    invoke-static/range {v2 .. v10}, Le3/e0;->g(Landroid/content/Context;Le3/w;Lj3/f;Le3/a;Lf3/c;Lf3/i;Lm3/d;Ll3/i;Le3/b0;)Le3/e0;

    move-result-object v22

    new-instance v2, Le3/k;

    iget-object v3, v1, Le3/m;->a:Landroid/content/Context;

    iget-object v4, v1, Le3/m;->o:Le3/i;

    iget-object v5, v1, Le3/m;->j:Le3/w;

    iget-object v6, v1, Le3/m;->c:Le3/s;

    iget-object v7, v1, Le3/m;->k:Lj3/f;

    iget-object v8, v1, Le3/m;->g:Le3/n;

    iget-object v9, v1, Le3/m;->p:Lb3/a;

    iget-object v10, v1, Le3/m;->m:Lc3/a;

    move-object/from16 v21, v12

    move-object v12, v2

    move-object/from16 v20, v13

    move-object v13, v3

    move-object v3, v14

    move-object v14, v4

    move-object v15, v5

    move-object/from16 v16, v6

    move-object/from16 v17, v7

    move-object/from16 v18, v8

    move-object/from16 v19, p1

    move-object/from16 v23, v9

    move-object/from16 v24, v10

    invoke-direct/range {v12 .. v24}, Le3/k;-><init>(Landroid/content/Context;Le3/i;Le3/w;Le3/s;Lj3/f;Le3/n;Le3/a;Lf3/i;Lf3/c;Le3/e0;Lb3/a;Lc3/a;)V

    iput-object v2, v1, Le3/m;->i:Le3/k;

    invoke-virtual/range {p0 .. p0}, Le3/m;->h()Z

    move-result v2

    invoke-direct/range {p0 .. p0}, Le3/m;->d()V

    iget-object v4, v1, Le3/m;->i:Le3/k;

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v5

    invoke-virtual {v4, v3, v5, v0}, Le3/k;->z(Ljava/lang/String;Ljava/lang/Thread$UncaughtExceptionHandler;Ll3/i;)V

    if-eqz v2, :cond_0

    iget-object v2, v1, Le3/m;->a:Landroid/content/Context;

    invoke-static {v2}, Le3/h;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v2

    const-string v3, "Crashlytics did not finish previous background initialization. Initializing synchronously."

    invoke-virtual {v2, v3}, Lb3/f;->b(Ljava/lang/String;)V

    invoke-direct {v1, v0}, Le3/m;->k(Ll3/i;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v25

    :cond_0
    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v2, "Successfully configured exception handler."

    invoke-virtual {v0, v2}, Lb3/f;->b(Ljava/lang/String;)V

    return v11

    :catch_0
    move-exception v0

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v2

    const-string v3, "Crashlytics was not started due to an exception during initialization"

    invoke-virtual {v2, v3, v0}, Lb3/f;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    iput-object v0, v1, Le3/m;->i:Le3/k;

    return v25

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "The Crashlytics build ID is missing. This occurs when the Crashlytics Gradle plugin is missing from your app\'s build configuration. Please review the Firebase Crashlytics onboarding instructions at https://firebase.google.com/docs/crashlytics/get-started?platform=android#add-plugin"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public t()Ln2/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ln2/i<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Le3/m;->i:Le3/k;

    invoke-virtual {v0}, Le3/k;->W()Ln2/i;

    move-result-object v0

    return-object v0
.end method

.method public u(Ljava/lang/Boolean;)V
    .locals 1

    iget-object v0, p0, Le3/m;->c:Le3/s;

    invoke-virtual {v0, p1}, Le3/s;->g(Ljava/lang/Boolean;)V

    return-void
.end method

.method public v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Le3/m;->i:Le3/k;

    invoke-virtual {v0, p1, p2}, Le3/k;->X(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Le3/m;->i:Le3/k;

    invoke-virtual {v0, p1, p2}, Le3/k;->Y(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public x(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Le3/m;->i:Le3/k;

    invoke-virtual {v0, p1}, Le3/k;->Z(Ljava/lang/String;)V

    return-void
.end method
