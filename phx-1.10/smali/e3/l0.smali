.class public final Le3/l0;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "awaitEvenIfOnMainThread task continuation executor"

    invoke-static {v0}, Le3/u;->c(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Le3/l0;->a:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public static synthetic a(Ln2/j;Ln2/i;)Ljava/lang/Void;
    .locals 0

    invoke-static {p0, p1}, Le3/l0;->m(Ln2/j;Ln2/i;)Ljava/lang/Void;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic b(Ln2/j;Ln2/i;)Ljava/lang/Void;
    .locals 0

    invoke-static {p0, p1}, Le3/l0;->l(Ln2/j;Ln2/i;)Ljava/lang/Void;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic c(Ljava/util/concurrent/CountDownLatch;Ln2/i;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0, p1}, Le3/l0;->i(Ljava/util/concurrent/CountDownLatch;Ln2/i;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic d(Ln2/j;Ln2/i;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0, p1}, Le3/l0;->j(Ln2/j;Ln2/i;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic e(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;Ln2/j;)V
    .locals 0

    invoke-static {p0, p1, p2}, Le3/l0;->k(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;Ln2/j;)V

    return-void
.end method

.method public static f(Ln2/i;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ln2/i<",
            "TT;>;)TT;"
        }
    .end annotation

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sget-object v1, Le3/l0;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Le3/h0;

    invoke-direct {v2, v0}, Le3/h0;-><init>(Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {p0, v1, v2}, Ln2/i;->e(Ljava/util/concurrent/Executor;Ln2/a;)Ln2/i;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_0

    const-wide/16 v1, 0x3

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x4

    :goto_0
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    invoke-virtual {p0}, Ln2/i;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ln2/i;->i()Ljava/lang/Object;

    move-result-object p0

    return-object p0

    :cond_1
    invoke-virtual {p0}, Ln2/i;->j()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Ln2/i;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ln2/i;->h()Ljava/lang/Exception;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_2
    new-instance p0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {p0}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw p0

    :cond_3
    new-instance p0, Ljava/util/concurrent/CancellationException;

    const-string v0, "Task is already canceled"

    invoke-direct {p0, v0}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static g(Ljava/util/concurrent/CountDownLatch;JLjava/util/concurrent/TimeUnit;)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide p1

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-long/2addr v1, p1

    :goto_0
    :try_start_1
    sget-object p3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, p1, p2, p3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result p0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    :cond_0
    return p0

    :catch_0
    const/4 v0, 0x1

    :try_start_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sub-long p1, v1, p1

    goto :goto_0

    :catchall_0
    move-exception p0

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    :cond_1
    throw p0
.end method

.method public static h(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Ln2/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Callable<",
            "Ln2/i<",
            "TT;>;>;)",
            "Ln2/i<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Ln2/j;

    invoke-direct {v0}, Ln2/j;-><init>()V

    new-instance v1, Le3/g0;

    invoke-direct {v1, p1, p0, v0}, Le3/g0;-><init>(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;Ln2/j;)V

    invoke-interface {p0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ln2/j;->a()Ln2/i;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic i(Ljava/util/concurrent/CountDownLatch;Ln2/i;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    const/4 p0, 0x0

    return-object p0
.end method

.method private static synthetic j(Ln2/j;Ln2/i;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p1}, Ln2/i;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ln2/i;->i()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Ln2/j;->c(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ln2/i;->h()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ln2/i;->h()Ljava/lang/Exception;

    move-result-object p1

    invoke-virtual {p0, p1}, Ln2/j;->b(Ljava/lang/Exception;)V

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static synthetic k(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;Ln2/j;)V
    .locals 1

    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ln2/i;

    new-instance v0, Le3/k0;

    invoke-direct {v0, p2}, Le3/k0;-><init>(Ln2/j;)V

    invoke-virtual {p0, p1, v0}, Ln2/i;->e(Ljava/util/concurrent/Executor;Ln2/a;)Ln2/i;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p2, p0}, Ln2/j;->b(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method

.method private static synthetic l(Ln2/j;Ln2/i;)Ljava/lang/Void;
    .locals 1

    invoke-virtual {p1}, Ln2/i;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ln2/i;->i()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Ln2/j;->e(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ln2/i;->h()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ln2/i;->h()Ljava/lang/Exception;

    move-result-object p1

    invoke-virtual {p0, p1}, Ln2/j;->d(Ljava/lang/Exception;)Z

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static synthetic m(Ln2/j;Ln2/i;)Ljava/lang/Void;
    .locals 1

    invoke-virtual {p1}, Ln2/i;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ln2/i;->i()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Ln2/j;->e(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ln2/i;->h()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ln2/i;->h()Ljava/lang/Exception;

    move-result-object p1

    invoke-virtual {p0, p1}, Ln2/j;->d(Ljava/lang/Exception;)Z

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static n(Ljava/util/concurrent/Executor;Ln2/i;Ln2/i;)Ln2/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Ln2/i<",
            "TT;>;",
            "Ln2/i<",
            "TT;>;)",
            "Ln2/i<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Ln2/j;

    invoke-direct {v0}, Ln2/j;-><init>()V

    new-instance v1, Le3/i0;

    invoke-direct {v1, v0}, Le3/i0;-><init>(Ln2/j;)V

    invoke-virtual {p1, p0, v1}, Ln2/i;->e(Ljava/util/concurrent/Executor;Ln2/a;)Ln2/i;

    invoke-virtual {p2, p0, v1}, Ln2/i;->e(Ljava/util/concurrent/Executor;Ln2/a;)Ln2/i;

    invoke-virtual {v0}, Ln2/j;->a()Ln2/i;

    move-result-object p0

    return-object p0
.end method

.method public static o(Ln2/i;Ln2/i;)Ln2/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ln2/i<",
            "TT;>;",
            "Ln2/i<",
            "TT;>;)",
            "Ln2/i<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Ln2/j;

    invoke-direct {v0}, Ln2/j;-><init>()V

    new-instance v1, Le3/j0;

    invoke-direct {v1, v0}, Le3/j0;-><init>(Ln2/j;)V

    invoke-virtual {p0, v1}, Ln2/i;->f(Ln2/a;)Ln2/i;

    invoke-virtual {p1, v1}, Ln2/i;->f(Ln2/a;)Ln2/i;

    invoke-virtual {v0}, Ln2/j;->a()Ln2/i;

    move-result-object p0

    return-object p0
.end method
