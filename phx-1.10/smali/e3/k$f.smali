.class Le3/k$f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Le3/k;->d0(Ljava/lang/Thread;Ljava/lang/Throwable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic f:J

.field final synthetic g:Ljava/lang/Throwable;

.field final synthetic h:Ljava/lang/Thread;

.field final synthetic i:Le3/k;


# direct methods
.method constructor <init>(Le3/k;JLjava/lang/Throwable;Ljava/lang/Thread;)V
    .locals 0

    iput-object p1, p0, Le3/k$f;->i:Le3/k;

    iput-wide p2, p0, Le3/k$f;->f:J

    iput-object p4, p0, Le3/k$f;->g:Ljava/lang/Throwable;

    iput-object p5, p0, Le3/k$f;->h:Ljava/lang/Thread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v0, p0, Le3/k$f;->i:Le3/k;

    invoke-virtual {v0}, Le3/k;->L()Z

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Le3/k$f;->f:J

    invoke-static {v0, v1}, Le3/k;->b(J)J

    move-result-wide v6

    iget-object v0, p0, Le3/k$f;->i:Le3/k;

    invoke-static {v0}, Le3/k;->c(Le3/k;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v1, "Tried to write a non-fatal exception while no session was open."

    invoke-virtual {v0, v1}, Lb3/f;->k(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Le3/k$f;->i:Le3/k;

    invoke-static {v0}, Le3/k;->h(Le3/k;)Le3/e0;

    move-result-object v2

    iget-object v3, p0, Le3/k$f;->g:Ljava/lang/Throwable;

    iget-object v4, p0, Le3/k$f;->h:Ljava/lang/Thread;

    invoke-virtual/range {v2 .. v7}, Le3/e0;->s(Ljava/lang/Throwable;Ljava/lang/Thread;Ljava/lang/String;J)V

    :cond_1
    return-void
.end method
