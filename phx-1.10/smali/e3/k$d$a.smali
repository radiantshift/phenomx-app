.class Le3/k$d$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Le3/k$d;->b(Ljava/lang/Boolean;)Ln2/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ln2/i<",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Boolean;

.field final synthetic b:Le3/k$d;


# direct methods
.method constructor <init>(Le3/k$d;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Le3/k$d$a;->b:Le3/k$d;

    iput-object p2, p0, Le3/k$d$a;->a:Ljava/lang/Boolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ln2/i;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ln2/i<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Le3/k$d$a;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v1, "Deleting cached crash reports..."

    invoke-virtual {v0, v1}, Lb3/f;->i(Ljava/lang/String;)V

    iget-object v0, p0, Le3/k$d$a;->b:Le3/k$d;

    iget-object v0, v0, Le3/k$d;->b:Le3/k;

    invoke-virtual {v0}, Le3/k;->N()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Le3/k;->d(Ljava/util/List;)V

    iget-object v0, p0, Le3/k$d$a;->b:Le3/k$d;

    iget-object v0, v0, Le3/k$d;->b:Le3/k;

    invoke-static {v0}, Le3/k;->h(Le3/k;)Le3/e0;

    move-result-object v0

    invoke-virtual {v0}, Le3/e0;->u()V

    iget-object v0, p0, Le3/k$d$a;->b:Le3/k$d;

    iget-object v0, v0, Le3/k$d;->b:Le3/k;

    iget-object v0, v0, Le3/k;->q:Ln2/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ln2/j;->e(Ljava/lang/Object;)Z

    invoke-static {v1}, Ln2/l;->d(Ljava/lang/Object;)Ln2/i;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v1, "Sending cached crash reports..."

    invoke-virtual {v0, v1}, Lb3/f;->b(Ljava/lang/String;)V

    iget-object v0, p0, Le3/k$d$a;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Le3/k$d$a;->b:Le3/k$d;

    iget-object v1, v1, Le3/k$d;->b:Le3/k;

    invoke-static {v1}, Le3/k;->l(Le3/k;)Le3/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Le3/s;->c(Z)V

    iget-object v0, p0, Le3/k$d$a;->b:Le3/k$d;

    iget-object v0, v0, Le3/k$d;->b:Le3/k;

    invoke-static {v0}, Le3/k;->m(Le3/k;)Le3/i;

    move-result-object v0

    invoke-virtual {v0}, Le3/i;->c()Ljava/util/concurrent/Executor;

    move-result-object v0

    iget-object v1, p0, Le3/k$d$a;->b:Le3/k$d;

    iget-object v1, v1, Le3/k$d;->a:Ln2/i;

    new-instance v2, Le3/k$d$a$a;

    invoke-direct {v2, p0, v0}, Le3/k$d$a$a;-><init>(Le3/k$d$a;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v1, v0, v2}, Ln2/i;->m(Ljava/util/concurrent/Executor;Ln2/h;)Ln2/i;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Le3/k$d$a;->a()Ln2/i;

    move-result-object v0

    return-object v0
.end method
