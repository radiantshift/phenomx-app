.class public final enum Lc4/n$a;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc4/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lc4/n$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum g:Lc4/n$a;

.field public static final enum h:Lc4/n$a;

.field public static final enum i:Lc4/n$a;

.field public static final enum j:Lc4/n$a;

.field public static final enum k:Lc4/n$a;

.field private static final synthetic l:[Lc4/n$a;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    new-instance v0, Lc4/n$a;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lc4/n$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lc4/n$a;->g:Lc4/n$a;

    new-instance v1, Lc4/n$a;

    const-string v3, "CONFIG_UPDATE_STREAM_ERROR"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v4}, Lc4/n$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lc4/n$a;->h:Lc4/n$a;

    new-instance v3, Lc4/n$a;

    const-string v5, "CONFIG_UPDATE_MESSAGE_INVALID"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6, v6}, Lc4/n$a;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lc4/n$a;->i:Lc4/n$a;

    new-instance v5, Lc4/n$a;

    const-string v7, "CONFIG_UPDATE_NOT_FETCHED"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8, v8}, Lc4/n$a;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lc4/n$a;->j:Lc4/n$a;

    new-instance v7, Lc4/n$a;

    const-string v9, "CONFIG_UPDATE_UNAVAILABLE"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10, v10}, Lc4/n$a;-><init>(Ljava/lang/String;II)V

    sput-object v7, Lc4/n$a;->k:Lc4/n$a;

    const/4 v9, 0x5

    new-array v9, v9, [Lc4/n$a;

    aput-object v0, v9, v2

    aput-object v1, v9, v4

    aput-object v3, v9, v6

    aput-object v5, v9, v8

    aput-object v7, v9, v10

    sput-object v9, Lc4/n$a;->l:[Lc4/n$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lc4/n$a;->f:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lc4/n$a;
    .locals 1

    const-class v0, Lc4/n$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lc4/n$a;

    return-object p0
.end method

.method public static values()[Lc4/n$a;
    .locals 1

    sget-object v0, Lc4/n$a;->l:[Lc4/n$a;

    invoke-virtual {v0}, [Lc4/n$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lc4/n$a;

    return-object v0
.end method
