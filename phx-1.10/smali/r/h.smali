.class public final enum Lr/h;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lr/h;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum f:Lr/h;

.field public static final enum g:Lr/h;

.field public static final enum h:Lr/h;

.field public static final enum i:Lr/h;

.field public static final enum j:Lr/h;

.field public static final enum k:Lr/h;

.field public static final enum l:Lr/h;

.field private static final synthetic m:[Lr/h;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lr/h;

    const-string v1, "multipleRequests"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lr/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/h;->f:Lr/h;

    new-instance v0, Lr/h;

    const-string v1, "unimplemented"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lr/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/h;->g:Lr/h;

    new-instance v0, Lr/h;

    const-string v1, "noLanguageIntent"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lr/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/h;->h:Lr/h;

    new-instance v0, Lr/h;

    const-string v1, "recognizerNotAvailable"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lr/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/h;->i:Lr/h;

    new-instance v0, Lr/h;

    const-string v1, "missingOrInvalidArg"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lr/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/h;->j:Lr/h;

    new-instance v0, Lr/h;

    const-string v1, "missingContext"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lr/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/h;->k:Lr/h;

    new-instance v0, Lr/h;

    const-string v1, "unknown"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lr/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/h;->l:Lr/h;

    invoke-static {}, Lr/h;->a()[Lr/h;

    move-result-object v0

    sput-object v0, Lr/h;->m:[Lr/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static final synthetic a()[Lr/h;
    .locals 3

    const/4 v0, 0x7

    new-array v0, v0, [Lr/h;

    sget-object v1, Lr/h;->f:Lr/h;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lr/h;->g:Lr/h;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lr/h;->h:Lr/h;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lr/h;->i:Lr/h;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lr/h;->j:Lr/h;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lr/h;->k:Lr/h;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lr/h;->l:Lr/h;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lr/h;
    .locals 1

    const-class v0, Lr/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lr/h;

    return-object p0
.end method

.method public static values()[Lr/h;
    .locals 1

    sget-object v0, Lr/h;->m:[Lr/h;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lr/h;

    return-object v0
.end method
