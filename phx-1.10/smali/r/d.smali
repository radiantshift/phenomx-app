.class final Lr/d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz4/k$d;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lz4/k$d;


# direct methods
.method public constructor <init>(Lz4/k$d;)V
    .locals 2

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lr/d;->a:Landroid/os/Handler;

    iput-object p1, p0, Lr/d;->b:Lz4/k$d;

    return-void
.end method

.method public static synthetic d(Lr/d;Ljava/lang/Object;)V
    .locals 0

    invoke-static {p0, p1}, Lr/d;->i(Lr/d;Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic e(Lr/d;)V
    .locals 0

    invoke-static {p0}, Lr/d;->h(Lr/d;)V

    return-void
.end method

.method public static synthetic f(Lr/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lr/d;->g(Lr/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private static final g(Lr/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$errorCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lr/d;->b:Lz4/k$d;

    invoke-interface {p0, p1, p2, p3}, Lz4/k$d;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private static final h(Lr/d;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lr/d;->b:Lz4/k$d;

    invoke-interface {p0}, Lz4/k$d;->c()V

    return-void
.end method

.method private static final i(Lr/d;Ljava/lang/Object;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lr/d;->b:Lz4/k$d;

    invoke-interface {p0, p1}, Lz4/k$d;->a(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lr/d;->a:Landroid/os/Handler;

    new-instance v1, Lr/b;

    invoke-direct {v1, p0, p1}, Lr/b;-><init>(Lr/d;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    const-string v0, "errorCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lr/d;->a:Landroid/os/Handler;

    new-instance v1, Lr/c;

    invoke-direct {v1, p0, p1, p2, p3}, Lr/c;-><init>(Lr/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lr/d;->a:Landroid/os/Handler;

    new-instance v1, Lr/a;

    invoke-direct {v1, p0}, Lr/a;-><init>(Lr/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
