.class public final enum Lr/r;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lr/r;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum f:Lr/r;

.field public static final enum g:Lr/r;

.field public static final enum h:Lr/r;

.field public static final enum i:Lr/r;

.field public static final enum j:Lr/r;

.field public static final enum k:Lr/r;

.field private static final synthetic l:[Lr/r;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lr/r;

    const-string v1, "listening"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lr/r;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/r;->f:Lr/r;

    new-instance v0, Lr/r;

    const-string v1, "notListening"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lr/r;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/r;->g:Lr/r;

    new-instance v0, Lr/r;

    const-string v1, "unavailable"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lr/r;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/r;->h:Lr/r;

    new-instance v0, Lr/r;

    const-string v1, "available"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lr/r;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/r;->i:Lr/r;

    new-instance v0, Lr/r;

    const-string v1, "done"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lr/r;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/r;->j:Lr/r;

    new-instance v0, Lr/r;

    const-string v1, "doneNoResult"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lr/r;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/r;->k:Lr/r;

    invoke-static {}, Lr/r;->a()[Lr/r;

    move-result-object v0

    sput-object v0, Lr/r;->l:[Lr/r;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static final synthetic a()[Lr/r;
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Lr/r;

    sget-object v1, Lr/r;->f:Lr/r;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lr/r;->g:Lr/r;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lr/r;->h:Lr/r;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lr/r;->i:Lr/r;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lr/r;->j:Lr/r;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lr/r;->k:Lr/r;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lr/r;
    .locals 1

    const-class v0, Lr/r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lr/r;

    return-object p0
.end method

.method public static values()[Lr/r;
    .locals 1

    sget-object v0, Lr/r;->l:[Lr/r;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lr/r;

    return-object v0
.end method
