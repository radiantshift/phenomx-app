.class public final enum Lr/f;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lr/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum f:Lr/f;

.field public static final enum g:Lr/f;

.field public static final enum h:Lr/f;

.field public static final enum i:Lr/f;

.field private static final synthetic j:[Lr/f;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lr/f;

    const-string v1, "deviceDefault"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lr/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/f;->f:Lr/f;

    new-instance v0, Lr/f;

    const-string v1, "dictation"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lr/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/f;->g:Lr/f;

    new-instance v0, Lr/f;

    const-string v1, "search"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lr/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/f;->h:Lr/f;

    new-instance v0, Lr/f;

    const-string v1, "confirmation"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lr/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/f;->i:Lr/f;

    invoke-static {}, Lr/f;->a()[Lr/f;

    move-result-object v0

    sput-object v0, Lr/f;->j:[Lr/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static final synthetic a()[Lr/f;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Lr/f;

    sget-object v1, Lr/f;->f:Lr/f;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lr/f;->g:Lr/f;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lr/f;->h:Lr/f;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lr/f;->i:Lr/f;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lr/f;
    .locals 1

    const-class v0, Lr/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lr/f;

    return-object p0
.end method

.method public static values()[Lr/f;
    .locals 1

    sget-object v0, Lr/f;->j:[Lr/f;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lr/f;

    return-object v0
.end method
