.class public final enum Lr/g;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lr/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum f:Lr/g;

.field public static final enum g:Lr/g;

.field public static final enum h:Lr/g;

.field public static final enum i:Lr/g;

.field private static final synthetic j:[Lr/g;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lr/g;

    const-string v1, "textRecognition"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lr/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/g;->f:Lr/g;

    new-instance v0, Lr/g;

    const-string v1, "notifyStatus"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lr/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/g;->g:Lr/g;

    new-instance v0, Lr/g;

    const-string v1, "notifyError"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lr/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/g;->h:Lr/g;

    new-instance v0, Lr/g;

    const-string v1, "soundLevelChange"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lr/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lr/g;->i:Lr/g;

    invoke-static {}, Lr/g;->a()[Lr/g;

    move-result-object v0

    sput-object v0, Lr/g;->j:[Lr/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static final synthetic a()[Lr/g;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Lr/g;

    sget-object v1, Lr/g;->f:Lr/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lr/g;->g:Lr/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lr/g;->h:Lr/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lr/g;->i:Lr/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lr/g;
    .locals 1

    const-class v0, Lr/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lr/g;

    return-object p0
.end method

.method public static values()[Lr/g;
    .locals 1

    sget-object v0, Lr/g;->j:[Lr/g;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lr/g;

    return-object v0
.end method
