.class public La3/d;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lu3/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lu3/a<",
            "Lw2/a;",
            ">;"
        }
    .end annotation
.end field

.field private volatile b:Lc3/a;

.field private volatile c:Ld3/b;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ld3/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lu3/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu3/a<",
            "Lw2/a;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ld3/c;

    invoke-direct {v0}, Ld3/c;-><init>()V

    new-instance v1, Lc3/f;

    invoke-direct {v1}, Lc3/f;-><init>()V

    invoke-direct {p0, p1, v0, v1}, La3/d;-><init>(Lu3/a;Ld3/b;Lc3/a;)V

    return-void
.end method

.method public constructor <init>(Lu3/a;Ld3/b;Lc3/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu3/a<",
            "Lw2/a;",
            ">;",
            "Ld3/b;",
            "Lc3/a;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, La3/d;->a:Lu3/a;

    iput-object p2, p0, La3/d;->c:Ld3/b;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, La3/d;->d:Ljava/util/List;

    iput-object p3, p0, La3/d;->b:Lc3/a;

    invoke-direct {p0}, La3/d;->f()V

    return-void
.end method

.method public static synthetic a(La3/d;Lu3/b;)V
    .locals 0

    invoke-direct {p0, p1}, La3/d;->i(Lu3/b;)V

    return-void
.end method

.method public static synthetic b(La3/d;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1, p2}, La3/d;->g(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public static synthetic c(La3/d;Ld3/a;)V
    .locals 0

    invoke-direct {p0, p1}, La3/d;->h(Ld3/a;)V

    return-void
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, La3/d;->a:Lu3/a;

    new-instance v1, La3/c;

    invoke-direct {v1, p0}, La3/c;-><init>(La3/d;)V

    invoke-interface {v0, v1}, Lu3/a;->a(Lu3/a$a;)V

    return-void
.end method

.method private synthetic g(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, La3/d;->b:Lc3/a;

    invoke-interface {v0, p1, p2}, Lc3/a;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method private synthetic h(Ld3/a;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, La3/d;->c:Ld3/b;

    instance-of v0, v0, Ld3/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, La3/d;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, La3/d;->c:Ld3/b;

    invoke-interface {v0, p1}, Ld3/b;->a(Ld3/a;)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private synthetic i(Lu3/b;)V
    .locals 5

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v1, "AnalyticsConnector now available."

    invoke-virtual {v0, v1}, Lb3/f;->b(Ljava/lang/String;)V

    invoke-interface {p1}, Lu3/b;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lw2/a;

    new-instance v0, Lc3/e;

    invoke-direct {v0, p1}, Lc3/e;-><init>(Lw2/a;)V

    new-instance v1, La3/e;

    invoke-direct {v1}, La3/e;-><init>()V

    invoke-static {p1, v1}, La3/d;->j(Lw2/a;La3/e;)Lw2/a$a;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object p1

    const-string v2, "Registered Firebase Analytics listener."

    invoke-virtual {p1, v2}, Lb3/f;->b(Ljava/lang/String;)V

    new-instance p1, Lc3/d;

    invoke-direct {p1}, Lc3/d;-><init>()V

    new-instance v2, Lc3/c;

    const/16 v3, 0x1f4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v2, v0, v3, v4}, Lc3/c;-><init>(Lc3/e;ILjava/util/concurrent/TimeUnit;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, La3/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ld3/a;

    invoke-virtual {p1, v3}, Lc3/d;->a(Ld3/a;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1, p1}, La3/e;->a(Lc3/b;)V

    invoke-virtual {v1, v2}, La3/e;->b(Lc3/b;)V

    iput-object p1, p0, La3/d;->c:Ld3/b;

    iput-object v2, p0, La3/d;->b:Lc3/a;

    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object p1

    const-string v0, "Could not register Firebase Analytics listener; a listener is already registered."

    invoke-virtual {p1, v0}, Lb3/f;->k(Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method private static j(Lw2/a;La3/e;)Lw2/a$a;
    .locals 2

    const-string v0, "clx"

    invoke-interface {p0, v0, p1}, Lw2/a;->d(Ljava/lang/String;Lw2/a$b;)Lw2/a$a;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object v0

    const-string v1, "Could not register AnalyticsConnectorListener with Crashlytics origin."

    invoke-virtual {v0, v1}, Lb3/f;->b(Ljava/lang/String;)V

    const-string v0, "crash"

    invoke-interface {p0, v0, p1}, Lw2/a;->d(Ljava/lang/String;Lw2/a$b;)Lw2/a$a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lb3/f;->f()Lb3/f;

    move-result-object p0

    const-string p1, "A new version of the Google Analytics for Firebase SDK is now available. For improved performance and compatibility with Crashlytics, please update to the latest version."

    invoke-virtual {p0, p1}, Lb3/f;->k(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public d()Lc3/a;
    .locals 1

    new-instance v0, La3/a;

    invoke-direct {v0, p0}, La3/a;-><init>(La3/d;)V

    return-object v0
.end method

.method public e()Ld3/b;
    .locals 1

    new-instance v0, La3/b;

    invoke-direct {v0, p0}, La3/b;-><init>(La3/d;)V

    return-object v0
.end method
