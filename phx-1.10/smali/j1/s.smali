.class public final Lj1/s;
.super Lj1/b1;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj1/s$b;,
        Lj1/s$a;
    }
.end annotation


# instance fields
.field private final r:Z

.field private final s:Lh0/c4$d;

.field private final t:Lh0/c4$b;

.field private u:Lj1/s$a;

.field private v:Lj1/r;

.field private w:Z

.field private x:Z

.field private y:Z


# direct methods
.method public constructor <init>(Lj1/x;Z)V
    .locals 1

    invoke-direct {p0, p1}, Lj1/b1;-><init>(Lj1/x;)V

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lj1/x;->e()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    iput-boolean p2, p0, Lj1/s;->r:Z

    new-instance p2, Lh0/c4$d;

    invoke-direct {p2}, Lh0/c4$d;-><init>()V

    iput-object p2, p0, Lj1/s;->s:Lh0/c4$d;

    new-instance p2, Lh0/c4$b;

    invoke-direct {p2}, Lh0/c4$b;-><init>()V

    iput-object p2, p0, Lj1/s;->t:Lh0/c4$b;

    invoke-interface {p1}, Lj1/x;->g()Lh0/c4;

    move-result-object p2

    if-eqz p2, :cond_1

    const/4 p1, 0x0

    invoke-static {p2, p1, p1}, Lj1/s$a;->z(Lh0/c4;Ljava/lang/Object;Ljava/lang/Object;)Lj1/s$a;

    move-result-object p1

    iput-object p1, p0, Lj1/s;->u:Lj1/s$a;

    iput-boolean v0, p0, Lj1/s;->y:Z

    goto :goto_1

    :cond_1
    invoke-interface {p1}, Lj1/x;->a()Lh0/z1;

    move-result-object p1

    invoke-static {p1}, Lj1/s$a;->y(Lh0/z1;)Lj1/s$a;

    move-result-object p1

    iput-object p1, p0, Lj1/s;->u:Lj1/s$a;

    :goto_1
    return-void
.end method

.method private a0(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lj1/s;->u:Lj1/s$a;

    invoke-static {v0}, Lj1/s$a;->w(Lj1/s$a;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lj1/s;->u:Lj1/s$a;

    invoke-static {v0}, Lj1/s$a;->w(Lj1/s$a;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lj1/s$a;->n:Ljava/lang/Object;

    :cond_0
    return-object p1
.end method

.method private b0(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lj1/s;->u:Lj1/s$a;

    invoke-static {v0}, Lj1/s$a;->w(Lj1/s$a;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lj1/s$a;->n:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lj1/s;->u:Lj1/s$a;

    invoke-static {p1}, Lj1/s$a;->w(Lj1/s$a;)Ljava/lang/Object;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private d0(J)V
    .locals 6
    .annotation runtime Lorg/checkerframework/checker/nullness/qual/RequiresNonNull;
        value = {
            "unpreparedMaskingMediaPeriod"
        }
    .end annotation

    iget-object v0, p0, Lj1/s;->v:Lj1/r;

    iget-object v1, p0, Lj1/s;->u:Lj1/s$a;

    iget-object v2, v0, Lj1/r;->f:Lj1/x$b;

    iget-object v2, v2, Lj1/v;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lj1/s$a;->f(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lj1/s;->u:Lj1/s$a;

    iget-object v3, p0, Lj1/s;->t:Lh0/c4$b;

    invoke-virtual {v2, v1, v3}, Lh0/c4;->j(ILh0/c4$b;)Lh0/c4$b;

    move-result-object v1

    iget-wide v1, v1, Lh0/c4$b;->i:J

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1

    cmp-long v3, p1, v1

    if-ltz v3, :cond_1

    const-wide/16 p1, 0x0

    const-wide/16 v3, 0x1

    sub-long/2addr v1, v3

    invoke-static {p1, p2, v1, v2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    :cond_1
    invoke-virtual {v0, p1, p2}, Lj1/r;->w(J)V

    return-void
.end method


# virtual methods
.method public E()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lj1/s;->x:Z

    iput-boolean v0, p0, Lj1/s;->w:Z

    invoke-super {p0}, Lj1/g;->E()V

    return-void
.end method

.method protected P(Lj1/x$b;)Lj1/x$b;
    .locals 1

    iget-object v0, p1, Lj1/v;->a:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lj1/s;->a0(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lj1/x$b;->c(Ljava/lang/Object;)Lj1/x$b;

    move-result-object p1

    return-object p1
.end method

.method protected V(Lh0/c4;)V
    .locals 14

    iget-boolean v0, p0, Lj1/s;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lj1/s;->u:Lj1/s$a;

    invoke-virtual {v0, p1}, Lj1/s$a;->x(Lh0/c4;)Lj1/s$a;

    move-result-object p1

    iput-object p1, p0, Lj1/s;->u:Lj1/s$a;

    iget-object p1, p0, Lj1/s;->v:Lj1/r;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lj1/r;->l()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lj1/s;->d0(J)V

    goto/16 :goto_3

    :cond_0
    invoke-virtual {p1}, Lh0/c4;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lj1/s;->y:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lj1/s;->u:Lj1/s$a;

    invoke-virtual {v0, p1}, Lj1/s$a;->x(Lh0/c4;)Lj1/s$a;

    move-result-object p1

    goto :goto_0

    :cond_1
    sget-object v0, Lh0/c4$d;->w:Ljava/lang/Object;

    sget-object v1, Lj1/s$a;->n:Ljava/lang/Object;

    invoke-static {p1, v0, v1}, Lj1/s$a;->z(Lh0/c4;Ljava/lang/Object;Ljava/lang/Object;)Lj1/s$a;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lj1/s;->u:Lj1/s$a;

    goto/16 :goto_3

    :cond_2
    iget-object v0, p0, Lj1/s;->s:Lh0/c4$d;

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    iget-object v0, p0, Lj1/s;->s:Lh0/c4$d;

    invoke-virtual {v0}, Lh0/c4$d;->e()J

    move-result-wide v2

    iget-object v0, p0, Lj1/s;->s:Lh0/c4$d;

    iget-object v0, v0, Lh0/c4$d;->f:Ljava/lang/Object;

    iget-object v4, p0, Lj1/s;->v:Lj1/r;

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lj1/r;->m()J

    move-result-wide v4

    iget-object v6, p0, Lj1/s;->u:Lj1/s$a;

    iget-object v7, p0, Lj1/s;->v:Lj1/r;

    iget-object v7, v7, Lj1/r;->f:Lj1/x$b;

    iget-object v7, v7, Lj1/v;->a:Ljava/lang/Object;

    iget-object v8, p0, Lj1/s;->t:Lh0/c4$b;

    invoke-virtual {v6, v7, v8}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    iget-object v6, p0, Lj1/s;->t:Lh0/c4$b;

    invoke-virtual {v6}, Lh0/c4$b;->q()J

    move-result-wide v6

    add-long/2addr v6, v4

    iget-object v4, p0, Lj1/s;->u:Lj1/s$a;

    iget-object v5, p0, Lj1/s;->s:Lh0/c4$d;

    invoke-virtual {v4, v1, v5}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    move-result-object v1

    invoke-virtual {v1}, Lh0/c4$d;->e()J

    move-result-wide v4

    cmp-long v1, v6, v4

    if-eqz v1, :cond_3

    move-wide v12, v6

    goto :goto_1

    :cond_3
    move-wide v12, v2

    :goto_1
    iget-object v9, p0, Lj1/s;->s:Lh0/c4$d;

    iget-object v10, p0, Lj1/s;->t:Lh0/c4$b;

    const/4 v11, 0x0

    move-object v8, p1

    invoke-virtual/range {v8 .. v13}, Lh0/c4;->n(Lh0/c4$d;Lh0/c4$b;IJ)Landroid/util/Pair;

    move-result-object v1

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-boolean v1, p0, Lj1/s;->y:Z

    if-eqz v1, :cond_4

    iget-object v0, p0, Lj1/s;->u:Lj1/s$a;

    invoke-virtual {v0, p1}, Lj1/s$a;->x(Lh0/c4;)Lj1/s$a;

    move-result-object p1

    goto :goto_2

    :cond_4
    invoke-static {p1, v0, v2}, Lj1/s$a;->z(Lh0/c4;Ljava/lang/Object;Ljava/lang/Object;)Lj1/s$a;

    move-result-object p1

    :goto_2
    iput-object p1, p0, Lj1/s;->u:Lj1/s$a;

    iget-object p1, p0, Lj1/s;->v:Lj1/r;

    if-eqz p1, :cond_5

    invoke-direct {p0, v3, v4}, Lj1/s;->d0(J)V

    iget-object p1, p1, Lj1/r;->f:Lj1/x$b;

    iget-object v0, p1, Lj1/v;->a:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lj1/s;->b0(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lj1/x$b;->c(Ljava/lang/Object;)Lj1/x$b;

    move-result-object p1

    goto :goto_4

    :cond_5
    :goto_3
    const/4 p1, 0x0

    :goto_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj1/s;->y:Z

    iput-boolean v0, p0, Lj1/s;->x:Z

    iget-object v0, p0, Lj1/s;->u:Lj1/s$a;

    invoke-virtual {p0, v0}, Lj1/a;->D(Lh0/c4;)V

    if-eqz p1, :cond_6

    iget-object v0, p0, Lj1/s;->v:Lj1/r;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/r;

    invoke-virtual {v0, p1}, Lj1/r;->k(Lj1/x$b;)V

    :cond_6
    return-void
.end method

.method public Y()V
    .locals 1

    iget-boolean v0, p0, Lj1/s;->r:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lj1/s;->w:Z

    invoke-virtual {p0}, Lj1/b1;->X()V

    :cond_0
    return-void
.end method

.method public Z(Lj1/x$b;Ld2/b;J)Lj1/r;
    .locals 1

    new-instance v0, Lj1/r;

    invoke-direct {v0, p1, p2, p3, p4}, Lj1/r;-><init>(Lj1/x$b;Ld2/b;J)V

    iget-object p2, p0, Lj1/b1;->p:Lj1/x;

    invoke-virtual {v0, p2}, Lj1/r;->y(Lj1/x;)V

    iget-boolean p2, p0, Lj1/s;->x:Z

    if-eqz p2, :cond_0

    iget-object p2, p1, Lj1/v;->a:Ljava/lang/Object;

    invoke-direct {p0, p2}, Lj1/s;->b0(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p1, p2}, Lj1/x$b;->c(Ljava/lang/Object;)Lj1/x$b;

    move-result-object p1

    invoke-virtual {v0, p1}, Lj1/r;->k(Lj1/x$b;)V

    goto :goto_0

    :cond_0
    iput-object v0, p0, Lj1/s;->v:Lj1/r;

    iget-boolean p1, p0, Lj1/s;->w:Z

    if-nez p1, :cond_1

    const/4 p1, 0x1

    iput-boolean p1, p0, Lj1/s;->w:Z

    invoke-virtual {p0}, Lj1/b1;->X()V

    :cond_1
    :goto_0
    return-object v0
.end method

.method public c0()Lh0/c4;
    .locals 1

    iget-object v0, p0, Lj1/s;->u:Lj1/s$a;

    return-object v0
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public i(Lj1/u;)V
    .locals 1

    move-object v0, p1

    check-cast v0, Lj1/r;

    invoke-virtual {v0}, Lj1/r;->x()V

    iget-object v0, p0, Lj1/s;->v:Lj1/r;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lj1/s;->v:Lj1/r;

    :cond_0
    return-void
.end method

.method public bridge synthetic j(Lj1/x$b;Ld2/b;J)Lj1/u;
    .locals 0

    invoke-virtual {p0, p1, p2, p3, p4}, Lj1/s;->Z(Lj1/x$b;Ld2/b;J)Lj1/r;

    move-result-object p1

    return-object p1
.end method
