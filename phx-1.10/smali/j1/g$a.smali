.class final Lj1/g$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lj1/e0;
.implements Ll0/w;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj1/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field private final f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private g:Lj1/e0$a;

.field private h:Ll0/w$a;

.field final synthetic i:Lj1/g;


# direct methods
.method public constructor <init>(Lj1/g;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iput-object p1, p0, Lj1/g$a;->i:Lj1/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lj1/a;->w(Lj1/x$b;)Lj1/e0$a;

    move-result-object v1

    iput-object v1, p0, Lj1/g$a;->g:Lj1/e0$a;

    invoke-virtual {p1, v0}, Lj1/a;->u(Lj1/x$b;)Ll0/w$a;

    move-result-object p1

    iput-object p1, p0, Lj1/g$a;->h:Ll0/w$a;

    iput-object p2, p0, Lj1/g$a;->f:Ljava/lang/Object;

    return-void
.end method

.method private b(ILj1/x$b;)Z
    .locals 3

    if-eqz p2, :cond_0

    iget-object v0, p0, Lj1/g$a;->i:Lj1/g;

    iget-object v1, p0, Lj1/g$a;->f:Ljava/lang/Object;

    invoke-virtual {v0, v1, p2}, Lj1/g;->I(Ljava/lang/Object;Lj1/x$b;)Lj1/x$b;

    move-result-object p2

    if-nez p2, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p2, 0x0

    :cond_1
    iget-object v0, p0, Lj1/g$a;->i:Lj1/g;

    iget-object v1, p0, Lj1/g$a;->f:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lj1/g;->K(Ljava/lang/Object;I)I

    move-result p1

    iget-object v0, p0, Lj1/g$a;->g:Lj1/e0$a;

    iget v1, v0, Lj1/e0$a;->a:I

    if-ne v1, p1, :cond_2

    iget-object v0, v0, Lj1/e0$a;->b:Lj1/x$b;

    invoke-static {v0, p2}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lj1/g$a;->i:Lj1/g;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, p1, p2, v1, v2}, Lj1/a;->v(ILj1/x$b;J)Lj1/e0$a;

    move-result-object v0

    iput-object v0, p0, Lj1/g$a;->g:Lj1/e0$a;

    :cond_3
    iget-object v0, p0, Lj1/g$a;->h:Ll0/w$a;

    iget v1, v0, Ll0/w$a;->a:I

    if-ne v1, p1, :cond_4

    iget-object v0, v0, Ll0/w$a;->b:Lj1/x$b;

    invoke-static {v0, p2}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    iget-object v0, p0, Lj1/g$a;->i:Lj1/g;

    invoke-virtual {v0, p1, p2}, Lj1/a;->s(ILj1/x$b;)Ll0/w$a;

    move-result-object p1

    iput-object p1, p0, Lj1/g$a;->h:Ll0/w$a;

    :cond_5
    const/4 p1, 0x1

    return p1
.end method

.method private h(Lj1/t;)Lj1/t;
    .locals 14

    iget-object v0, p0, Lj1/g$a;->i:Lj1/g;

    iget-object v1, p0, Lj1/g$a;->f:Ljava/lang/Object;

    iget-wide v2, p1, Lj1/t;->f:J

    invoke-virtual {v0, v1, v2, v3}, Lj1/g;->J(Ljava/lang/Object;J)J

    move-result-wide v10

    iget-object v0, p0, Lj1/g$a;->i:Lj1/g;

    iget-object v1, p0, Lj1/g$a;->f:Ljava/lang/Object;

    iget-wide v2, p1, Lj1/t;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lj1/g;->J(Ljava/lang/Object;J)J

    move-result-wide v12

    iget-wide v0, p1, Lj1/t;->f:J

    cmp-long v2, v10, v0

    if-nez v2, :cond_0

    iget-wide v0, p1, Lj1/t;->g:J

    cmp-long v2, v12, v0

    if-nez v2, :cond_0

    return-object p1

    :cond_0
    new-instance v0, Lj1/t;

    iget v5, p1, Lj1/t;->a:I

    iget v6, p1, Lj1/t;->b:I

    iget-object v7, p1, Lj1/t;->c:Lh0/r1;

    iget v8, p1, Lj1/t;->d:I

    iget-object v9, p1, Lj1/t;->e:Ljava/lang/Object;

    move-object v4, v0

    invoke-direct/range {v4 .. v13}, Lj1/t;-><init>(IILh0/r1;ILjava/lang/Object;JJ)V

    return-object v0
.end method


# virtual methods
.method public C(ILj1/x$b;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/g$a;->b(ILj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lj1/g$a;->h:Ll0/w$a;

    invoke-virtual {p1, p3}, Ll0/w$a;->k(I)V

    :cond_0
    return-void
.end method

.method public F(ILj1/x$b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/g$a;->b(ILj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lj1/g$a;->h:Ll0/w$a;

    invoke-virtual {p1}, Ll0/w$a;->j()V

    :cond_0
    return-void
.end method

.method public H(ILj1/x$b;Lj1/q;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/g$a;->b(ILj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lj1/g$a;->g:Lj1/e0$a;

    invoke-direct {p0, p4}, Lj1/g$a;->h(Lj1/t;)Lj1/t;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Lj1/e0$a;->s(Lj1/q;Lj1/t;)V

    :cond_0
    return-void
.end method

.method public I(ILj1/x$b;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/g$a;->b(ILj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lj1/g$a;->g:Lj1/e0$a;

    invoke-direct {p0, p3}, Lj1/g$a;->h(Lj1/t;)Lj1/t;

    move-result-object p2

    invoke-virtual {p1, p2}, Lj1/e0$a;->j(Lj1/t;)V

    :cond_0
    return-void
.end method

.method public K(ILj1/x$b;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/g$a;->b(ILj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lj1/g$a;->g:Lj1/e0$a;

    invoke-direct {p0, p3}, Lj1/g$a;->h(Lj1/t;)Lj1/t;

    move-result-object p2

    invoke-virtual {p1, p2}, Lj1/e0$a;->E(Lj1/t;)V

    :cond_0
    return-void
.end method

.method public L(ILj1/x$b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/g$a;->b(ILj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lj1/g$a;->h:Ll0/w$a;

    invoke-virtual {p1}, Ll0/w$a;->m()V

    :cond_0
    return-void
.end method

.method public Q(ILj1/x$b;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/g$a;->b(ILj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lj1/g$a;->g:Lj1/e0$a;

    invoke-direct {p0, p4}, Lj1/g$a;->h(Lj1/t;)Lj1/t;

    move-result-object p2

    invoke-virtual {p1, p3, p2, p5, p6}, Lj1/e0$a;->y(Lj1/q;Lj1/t;Ljava/io/IOException;Z)V

    :cond_0
    return-void
.end method

.method public U(ILj1/x$b;Lj1/q;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/g$a;->b(ILj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lj1/g$a;->g:Lj1/e0$a;

    invoke-direct {p0, p4}, Lj1/g$a;->h(Lj1/t;)Lj1/t;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Lj1/e0$a;->B(Lj1/q;Lj1/t;)V

    :cond_0
    return-void
.end method

.method public d0(ILj1/x$b;Lj1/q;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/g$a;->b(ILj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lj1/g$a;->g:Lj1/e0$a;

    invoke-direct {p0, p4}, Lj1/g$a;->h(Lj1/t;)Lj1/t;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Lj1/e0$a;->v(Lj1/q;Lj1/t;)V

    :cond_0
    return-void
.end method

.method public e0(ILj1/x$b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/g$a;->b(ILj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lj1/g$a;->h:Ll0/w$a;

    invoke-virtual {p1}, Ll0/w$a;->i()V

    :cond_0
    return-void
.end method

.method public synthetic g0(ILj1/x$b;)V
    .locals 0

    invoke-static {p0, p1, p2}, Ll0/p;->a(Ll0/w;ILj1/x$b;)V

    return-void
.end method

.method public h0(ILj1/x$b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/g$a;->b(ILj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lj1/g$a;->h:Ll0/w$a;

    invoke-virtual {p1}, Ll0/w$a;->h()V

    :cond_0
    return-void
.end method

.method public m0(ILj1/x$b;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/g$a;->b(ILj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lj1/g$a;->h:Ll0/w$a;

    invoke-virtual {p1, p3}, Ll0/w$a;->l(Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method
