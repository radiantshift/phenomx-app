.class public abstract Lj1/a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lj1/x;


# instance fields
.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lj1/x$c;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lj1/x$c;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lj1/e0$a;

.field private final i:Ll0/w$a;

.field private j:Landroid/os/Looper;

.field private k:Lh0/c4;

.field private l:Li0/u1;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lj1/a;->f:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lj1/a;->g:Ljava/util/HashSet;

    new-instance v0, Lj1/e0$a;

    invoke-direct {v0}, Lj1/e0$a;-><init>()V

    iput-object v0, p0, Lj1/a;->h:Lj1/e0$a;

    new-instance v0, Ll0/w$a;

    invoke-direct {v0}, Ll0/w$a;-><init>()V

    iput-object v0, p0, Lj1/a;->i:Ll0/w$a;

    return-void
.end method


# virtual methods
.method protected final A()Li0/u1;
    .locals 1

    iget-object v0, p0, Lj1/a;->l:Li0/u1;

    invoke-static {v0}, Le2/a;->h(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Li0/u1;

    return-object v0
.end method

.method protected final B()Z
    .locals 1

    iget-object v0, p0, Lj1/a;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected abstract C(Ld2/p0;)V
.end method

.method protected final D(Lh0/c4;)V
    .locals 2

    iput-object p1, p0, Lj1/a;->k:Lh0/c4;

    iget-object v0, p0, Lj1/a;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/x$c;

    invoke-interface {v1, p0, p1}, Lj1/x$c;->a(Lj1/x;Lh0/c4;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected abstract E()V
.end method

.method public final b(Landroid/os/Handler;Ll0/w;)V
    .locals 1

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lj1/a;->i:Ll0/w$a;

    invoke-virtual {v0, p1, p2}, Ll0/w$a;->g(Landroid/os/Handler;Ll0/w;)V

    return-void
.end method

.method public final c(Lj1/x$c;)V
    .locals 2

    iget-object v0, p0, Lj1/a;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lj1/a;->g:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    iget-object p1, p0, Lj1/a;->g:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lj1/a;->y()V

    :cond_0
    return-void
.end method

.method public synthetic e()Z
    .locals 1

    invoke-static {p0}, Lj1/w;->b(Lj1/x;)Z

    move-result v0

    return v0
.end method

.method public final f(Landroid/os/Handler;Lj1/e0;)V
    .locals 1

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lj1/a;->h:Lj1/e0$a;

    invoke-virtual {v0, p1, p2}, Lj1/e0$a;->g(Landroid/os/Handler;Lj1/e0;)V

    return-void
.end method

.method public synthetic g()Lh0/c4;
    .locals 1

    invoke-static {p0}, Lj1/w;->a(Lj1/x;)Lh0/c4;

    move-result-object v0

    return-object v0
.end method

.method public final k(Lj1/x$c;)V
    .locals 1

    iget-object v0, p0, Lj1/a;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lj1/a;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lj1/a;->j:Landroid/os/Looper;

    iput-object p1, p0, Lj1/a;->k:Lh0/c4;

    iput-object p1, p0, Lj1/a;->l:Li0/u1;

    iget-object p1, p0, Lj1/a;->g:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/HashSet;->clear()V

    invoke-virtual {p0}, Lj1/a;->E()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lj1/a;->c(Lj1/x$c;)V

    :goto_0
    return-void
.end method

.method public final n(Lj1/x$c;)V
    .locals 2

    iget-object v0, p0, Lj1/a;->j:Landroid/os/Looper;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lj1/a;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    iget-object v1, p0, Lj1/a;->g:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lj1/a;->z()V

    :cond_0
    return-void
.end method

.method public final o(Ll0/w;)V
    .locals 1

    iget-object v0, p0, Lj1/a;->i:Ll0/w$a;

    invoke-virtual {v0, p1}, Ll0/w$a;->t(Ll0/w;)V

    return-void
.end method

.method public final p(Lj1/e0;)V
    .locals 1

    iget-object v0, p0, Lj1/a;->h:Lj1/e0$a;

    invoke-virtual {v0, p1}, Lj1/e0$a;->C(Lj1/e0;)V

    return-void
.end method

.method public final q(Lj1/x$c;Ld2/p0;Li0/u1;)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lj1/a;->j:Landroid/os/Looper;

    if-eqz v1, :cond_1

    if-ne v1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Le2/a;->a(Z)V

    iput-object p3, p0, Lj1/a;->l:Li0/u1;

    iget-object p3, p0, Lj1/a;->k:Lh0/c4;

    iget-object v1, p0, Lj1/a;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lj1/a;->j:Landroid/os/Looper;

    if-nez v1, :cond_2

    iput-object v0, p0, Lj1/a;->j:Landroid/os/Looper;

    iget-object p3, p0, Lj1/a;->g:Ljava/util/HashSet;

    invoke-virtual {p3, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p2}, Lj1/a;->C(Ld2/p0;)V

    goto :goto_2

    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p0, p1}, Lj1/a;->n(Lj1/x$c;)V

    invoke-interface {p1, p0, p3}, Lj1/x$c;->a(Lj1/x;Lh0/c4;)V

    :cond_3
    :goto_2
    return-void
.end method

.method protected final s(ILj1/x$b;)Ll0/w$a;
    .locals 1

    iget-object v0, p0, Lj1/a;->i:Ll0/w$a;

    invoke-virtual {v0, p1, p2}, Ll0/w$a;->u(ILj1/x$b;)Ll0/w$a;

    move-result-object p1

    return-object p1
.end method

.method protected final u(Lj1/x$b;)Ll0/w$a;
    .locals 2

    iget-object v0, p0, Lj1/a;->i:Ll0/w$a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ll0/w$a;->u(ILj1/x$b;)Ll0/w$a;

    move-result-object p1

    return-object p1
.end method

.method protected final v(ILj1/x$b;J)Lj1/e0$a;
    .locals 1

    iget-object v0, p0, Lj1/a;->h:Lj1/e0$a;

    invoke-virtual {v0, p1, p2, p3, p4}, Lj1/e0$a;->F(ILj1/x$b;J)Lj1/e0$a;

    move-result-object p1

    return-object p1
.end method

.method protected final w(Lj1/x$b;)Lj1/e0$a;
    .locals 4

    iget-object v0, p0, Lj1/a;->h:Lj1/e0$a;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, p1, v2, v3}, Lj1/e0$a;->F(ILj1/x$b;J)Lj1/e0$a;

    move-result-object p1

    return-object p1
.end method

.method protected final x(Lj1/x$b;J)Lj1/e0$a;
    .locals 2

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lj1/a;->h:Lj1/e0$a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1, p2, p3}, Lj1/e0$a;->F(ILj1/x$b;J)Lj1/e0$a;

    move-result-object p1

    return-object p1
.end method

.method protected y()V
    .locals 0

    return-void
.end method

.method protected z()V
    .locals 0

    return-void
.end method
