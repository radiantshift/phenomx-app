.class public final Lj1/l0;
.super Lj1/a;
.source ""

# interfaces
.implements Lj1/k0$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj1/l0$b;
    }
.end annotation


# instance fields
.field private final m:Lh0/z1;

.field private final n:Lh0/z1$h;

.field private final o:Ld2/l$a;

.field private final p:Lj1/f0$a;

.field private final q:Ll0/y;

.field private final r:Ld2/g0;

.field private final s:I

.field private t:Z

.field private u:J

.field private v:Z

.field private w:Z

.field private x:Ld2/p0;


# direct methods
.method private constructor <init>(Lh0/z1;Ld2/l$a;Lj1/f0$a;Ll0/y;Ld2/g0;I)V
    .locals 1

    invoke-direct {p0}, Lj1/a;-><init>()V

    iget-object v0, p1, Lh0/z1;->g:Lh0/z1$h;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/z1$h;

    iput-object v0, p0, Lj1/l0;->n:Lh0/z1$h;

    iput-object p1, p0, Lj1/l0;->m:Lh0/z1;

    iput-object p2, p0, Lj1/l0;->o:Ld2/l$a;

    iput-object p3, p0, Lj1/l0;->p:Lj1/f0$a;

    iput-object p4, p0, Lj1/l0;->q:Ll0/y;

    iput-object p5, p0, Lj1/l0;->r:Ld2/g0;

    iput p6, p0, Lj1/l0;->s:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lj1/l0;->t:Z

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lj1/l0;->u:J

    return-void
.end method

.method synthetic constructor <init>(Lh0/z1;Ld2/l$a;Lj1/f0$a;Ll0/y;Ld2/g0;ILj1/l0$a;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lj1/l0;-><init>(Lh0/z1;Ld2/l$a;Lj1/f0$a;Ll0/y;Ld2/g0;I)V

    return-void
.end method

.method private F()V
    .locals 9

    new-instance v8, Lj1/u0;

    iget-wide v1, p0, Lj1/l0;->u:J

    iget-boolean v3, p0, Lj1/l0;->v:Z

    iget-boolean v5, p0, Lj1/l0;->w:Z

    iget-object v7, p0, Lj1/l0;->m:Lh0/z1;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lj1/u0;-><init>(JZZZLjava/lang/Object;Lh0/z1;)V

    iget-boolean v0, p0, Lj1/l0;->t:Z

    if-eqz v0, :cond_0

    new-instance v0, Lj1/l0$a;

    invoke-direct {v0, p0, v8}, Lj1/l0$a;-><init>(Lj1/l0;Lh0/c4;)V

    move-object v8, v0

    :cond_0
    invoke-virtual {p0, v8}, Lj1/a;->D(Lh0/c4;)V

    return-void
.end method


# virtual methods
.method protected C(Ld2/p0;)V
    .locals 2

    iput-object p1, p0, Lj1/l0;->x:Ld2/p0;

    iget-object p1, p0, Lj1/l0;->q:Ll0/y;

    invoke-interface {p1}, Ll0/y;->c()V

    iget-object p1, p0, Lj1/l0;->q:Ll0/y;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    invoke-virtual {p0}, Lj1/a;->A()Li0/u1;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ll0/y;->b(Landroid/os/Looper;Li0/u1;)V

    invoke-direct {p0}, Lj1/l0;->F()V

    return-void
.end method

.method protected E()V
    .locals 1

    iget-object v0, p0, Lj1/l0;->q:Ll0/y;

    invoke-interface {v0}, Ll0/y;->a()V

    return-void
.end method

.method public a()Lh0/z1;
    .locals 1

    iget-object v0, p0, Lj1/l0;->m:Lh0/z1;

    return-object v0
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public i(Lj1/u;)V
    .locals 0

    check-cast p1, Lj1/k0;

    invoke-virtual {p1}, Lj1/k0;->f0()V

    return-void
.end method

.method public j(Lj1/x$b;Ld2/b;J)Lj1/u;
    .locals 14

    move-object v12, p0

    iget-object v0, v12, Lj1/l0;->o:Ld2/l$a;

    invoke-interface {v0}, Ld2/l$a;->a()Ld2/l;

    move-result-object v2

    iget-object v0, v12, Lj1/l0;->x:Ld2/p0;

    if-eqz v0, :cond_0

    invoke-interface {v2, v0}, Ld2/l;->i(Ld2/p0;)V

    :cond_0
    new-instance v13, Lj1/k0;

    iget-object v0, v12, Lj1/l0;->n:Lh0/z1$h;

    iget-object v1, v0, Lh0/z1$h;->a:Landroid/net/Uri;

    iget-object v0, v12, Lj1/l0;->p:Lj1/f0$a;

    invoke-virtual {p0}, Lj1/a;->A()Li0/u1;

    move-result-object v3

    invoke-interface {v0, v3}, Lj1/f0$a;->a(Li0/u1;)Lj1/f0;

    move-result-object v3

    iget-object v4, v12, Lj1/l0;->q:Ll0/y;

    invoke-virtual {p0, p1}, Lj1/a;->u(Lj1/x$b;)Ll0/w$a;

    move-result-object v5

    iget-object v6, v12, Lj1/l0;->r:Ld2/g0;

    invoke-virtual {p0, p1}, Lj1/a;->w(Lj1/x$b;)Lj1/e0$a;

    move-result-object v7

    iget-object v0, v12, Lj1/l0;->n:Lh0/z1$h;

    iget-object v10, v0, Lh0/z1$h;->f:Ljava/lang/String;

    iget v11, v12, Lj1/l0;->s:I

    move-object v0, v13

    move-object v8, p0

    move-object/from16 v9, p2

    invoke-direct/range {v0 .. v11}, Lj1/k0;-><init>(Landroid/net/Uri;Ld2/l;Lj1/f0;Ll0/y;Ll0/w$a;Ld2/g0;Lj1/e0$a;Lj1/k0$b;Ld2/b;Ljava/lang/String;I)V

    return-object v13
.end method

.method public r(JZZ)V
    .locals 3

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    iget-wide p1, p0, Lj1/l0;->u:J

    :cond_0
    iget-boolean v0, p0, Lj1/l0;->t:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lj1/l0;->u:J

    cmp-long v2, v0, p1

    if-nez v2, :cond_1

    iget-boolean v0, p0, Lj1/l0;->v:Z

    if-ne v0, p3, :cond_1

    iget-boolean v0, p0, Lj1/l0;->w:Z

    if-ne v0, p4, :cond_1

    return-void

    :cond_1
    iput-wide p1, p0, Lj1/l0;->u:J

    iput-boolean p3, p0, Lj1/l0;->v:Z

    iput-boolean p4, p0, Lj1/l0;->w:Z

    const/4 p1, 0x0

    iput-boolean p1, p0, Lj1/l0;->t:Z

    invoke-direct {p0}, Lj1/l0;->F()V

    return-void
.end method
