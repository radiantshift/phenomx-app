.class public final Lj1/t0;
.super Lj1/a;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj1/t0$d;,
        Lj1/t0$c;,
        Lj1/t0$b;
    }
.end annotation


# static fields
.field private static final o:Lh0/r1;

.field private static final p:Lh0/z1;

.field private static final q:[B


# instance fields
.field private final m:J

.field private final n:Lh0/z1;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lh0/r1$b;

    invoke-direct {v0}, Lh0/r1$b;-><init>()V

    const-string v1, "audio/raw"

    invoke-virtual {v0, v1}, Lh0/r1$b;->g0(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lh0/r1$b;->J(I)Lh0/r1$b;

    move-result-object v0

    const v2, 0xac44

    invoke-virtual {v0, v2}, Lh0/r1$b;->h0(I)Lh0/r1$b;

    move-result-object v0

    invoke-virtual {v0, v1}, Lh0/r1$b;->a0(I)Lh0/r1$b;

    move-result-object v0

    invoke-virtual {v0}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object v0

    sput-object v0, Lj1/t0;->o:Lh0/r1;

    new-instance v2, Lh0/z1$c;

    invoke-direct {v2}, Lh0/z1$c;-><init>()V

    const-string v3, "SilenceMediaSource"

    invoke-virtual {v2, v3}, Lh0/z1$c;->c(Ljava/lang/String;)Lh0/z1$c;

    move-result-object v2

    sget-object v3, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lh0/z1$c;->f(Landroid/net/Uri;)Lh0/z1$c;

    move-result-object v2

    iget-object v0, v0, Lh0/r1;->q:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lh0/z1$c;->d(Ljava/lang/String;)Lh0/z1$c;

    move-result-object v0

    invoke-virtual {v0}, Lh0/z1$c;->a()Lh0/z1;

    move-result-object v0

    sput-object v0, Lj1/t0;->p:Lh0/z1;

    invoke-static {v1, v1}, Le2/n0;->d0(II)I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    new-array v0, v0, [B

    sput-object v0, Lj1/t0;->q:[B

    return-void
.end method

.method private constructor <init>(JLh0/z1;)V
    .locals 3

    invoke-direct {p0}, Lj1/a;-><init>()V

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->a(Z)V

    iput-wide p1, p0, Lj1/t0;->m:J

    iput-object p3, p0, Lj1/t0;->n:Lh0/z1;

    return-void
.end method

.method synthetic constructor <init>(JLh0/z1;Lj1/t0$a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lj1/t0;-><init>(JLh0/z1;)V

    return-void
.end method

.method static synthetic F()Lh0/z1;
    .locals 1

    sget-object v0, Lj1/t0;->p:Lh0/z1;

    return-object v0
.end method

.method static synthetic G()Lh0/r1;
    .locals 1

    sget-object v0, Lj1/t0;->o:Lh0/r1;

    return-object v0
.end method

.method static synthetic H(J)J
    .locals 0

    invoke-static {p0, p1}, Lj1/t0;->K(J)J

    move-result-wide p0

    return-wide p0
.end method

.method static synthetic I(J)J
    .locals 0

    invoke-static {p0, p1}, Lj1/t0;->L(J)J

    move-result-wide p0

    return-wide p0
.end method

.method static synthetic J()[B
    .locals 1

    sget-object v0, Lj1/t0;->q:[B

    return-object v0
.end method

.method private static K(J)J
    .locals 2

    const-wide/32 v0, 0xac44

    mul-long p0, p0, v0

    const-wide/32 v0, 0xf4240

    div-long/2addr p0, v0

    const/4 v0, 0x2

    invoke-static {v0, v0}, Le2/n0;->d0(II)I

    move-result v0

    int-to-long v0, v0

    mul-long v0, v0, p0

    return-wide v0
.end method

.method private static L(J)J
    .locals 2

    const/4 v0, 0x2

    invoke-static {v0, v0}, Le2/n0;->d0(II)I

    move-result v0

    int-to-long v0, v0

    div-long/2addr p0, v0

    const-wide/32 v0, 0xf4240

    mul-long p0, p0, v0

    const-wide/32 v0, 0xac44

    div-long/2addr p0, v0

    return-wide p0
.end method


# virtual methods
.method protected C(Ld2/p0;)V
    .locals 8

    new-instance p1, Lj1/u0;

    iget-wide v1, p0, Lj1/t0;->m:J

    iget-object v7, p0, Lj1/t0;->n:Lh0/z1;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v7}, Lj1/u0;-><init>(JZZZLjava/lang/Object;Lh0/z1;)V

    invoke-virtual {p0, p1}, Lj1/a;->D(Lh0/c4;)V

    return-void
.end method

.method protected E()V
    .locals 0

    return-void
.end method

.method public a()Lh0/z1;
    .locals 1

    iget-object v0, p0, Lj1/t0;->n:Lh0/z1;

    return-object v0
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public i(Lj1/u;)V
    .locals 0

    return-void
.end method

.method public j(Lj1/x$b;Ld2/b;J)Lj1/u;
    .locals 0

    new-instance p1, Lj1/t0$c;

    iget-wide p2, p0, Lj1/t0;->m:J

    invoke-direct {p1, p2, p3}, Lj1/t0$c;-><init>(J)V

    return-object p1
.end method
