.class public final Lj1/r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lj1/u;
.implements Lj1/u$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj1/r$a;
    }
.end annotation


# instance fields
.field public final f:Lj1/x$b;

.field private final g:J

.field private final h:Ld2/b;

.field private i:Lj1/x;

.field private j:Lj1/u;

.field private k:Lj1/u$a;

.field private l:Lj1/r$a;

.field private m:Z

.field private n:J


# direct methods
.method public constructor <init>(Lj1/x$b;Ld2/b;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lj1/r;->f:Lj1/x$b;

    iput-object p2, p0, Lj1/r;->h:Ld2/b;

    iput-wide p3, p0, Lj1/r;->g:J

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lj1/r;->n:J

    return-void
.end method

.method private t(J)J
    .locals 5

    iget-wide v0, p0, Lj1/r;->n:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    move-wide p1, v0

    :cond_0
    return-wide p1
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-object v0, p0, Lj1/r;->j:Lj1/u;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lj1/u;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c(JLh0/u3;)J
    .locals 1

    iget-object v0, p0, Lj1/r;->j:Lj1/u;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/u;

    invoke-interface {v0, p1, p2, p3}, Lj1/u;->c(JLh0/u3;)J

    move-result-wide p1

    return-wide p1
.end method

.method public d()J
    .locals 2

    iget-object v0, p0, Lj1/r;->j:Lj1/u;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/u;

    invoke-interface {v0}, Lj1/u;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public e(Lj1/u;)V
    .locals 1

    iget-object p1, p0, Lj1/r;->k:Lj1/u$a;

    invoke-static {p1}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/u$a;

    invoke-interface {p1, p0}, Lj1/u$a;->e(Lj1/u;)V

    iget-object p1, p0, Lj1/r;->l:Lj1/r$a;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lj1/r;->f:Lj1/x$b;

    invoke-interface {p1, v0}, Lj1/r$a;->a(Lj1/x$b;)V

    :cond_0
    return-void
.end method

.method public f()J
    .locals 2

    iget-object v0, p0, Lj1/r;->j:Lj1/u;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/u;

    invoke-interface {v0}, Lj1/u;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public g(J)Z
    .locals 1

    iget-object v0, p0, Lj1/r;->j:Lj1/u;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lj1/u;->g(J)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public h(J)V
    .locals 1

    iget-object v0, p0, Lj1/r;->j:Lj1/u;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/u;

    invoke-interface {v0, p1, p2}, Lj1/u;->h(J)V

    return-void
.end method

.method public bridge synthetic i(Lj1/r0;)V
    .locals 0

    check-cast p1, Lj1/u;

    invoke-virtual {p0, p1}, Lj1/r;->v(Lj1/u;)V

    return-void
.end method

.method public k(Lj1/x$b;)V
    .locals 4

    iget-wide v0, p0, Lj1/r;->g:J

    invoke-direct {p0, v0, v1}, Lj1/r;->t(J)J

    move-result-wide v0

    iget-object v2, p0, Lj1/r;->i:Lj1/x;

    invoke-static {v2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lj1/x;

    iget-object v3, p0, Lj1/r;->h:Ld2/b;

    invoke-interface {v2, p1, v3, v0, v1}, Lj1/x;->j(Lj1/x$b;Ld2/b;J)Lj1/u;

    move-result-object p1

    iput-object p1, p0, Lj1/r;->j:Lj1/u;

    iget-object v2, p0, Lj1/r;->k:Lj1/u$a;

    if-eqz v2, :cond_0

    invoke-interface {p1, p0, v0, v1}, Lj1/u;->p(Lj1/u$a;J)V

    :cond_0
    return-void
.end method

.method public l()J
    .locals 2

    iget-wide v0, p0, Lj1/r;->n:J

    return-wide v0
.end method

.method public m()J
    .locals 2

    iget-wide v0, p0, Lj1/r;->g:J

    return-wide v0
.end method

.method public n()J
    .locals 2

    iget-object v0, p0, Lj1/r;->j:Lj1/u;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/u;

    invoke-interface {v0}, Lj1/u;->n()J

    move-result-wide v0

    return-wide v0
.end method

.method public o()Lj1/z0;
    .locals 1

    iget-object v0, p0, Lj1/r;->j:Lj1/u;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/u;

    invoke-interface {v0}, Lj1/u;->o()Lj1/z0;

    move-result-object v0

    return-object v0
.end method

.method public p(Lj1/u$a;J)V
    .locals 0

    iput-object p1, p0, Lj1/r;->k:Lj1/u$a;

    iget-object p1, p0, Lj1/r;->j:Lj1/u;

    if-eqz p1, :cond_0

    iget-wide p2, p0, Lj1/r;->g:J

    invoke-direct {p0, p2, p3}, Lj1/r;->t(J)J

    move-result-wide p2

    invoke-interface {p1, p0, p2, p3}, Lj1/u;->p(Lj1/u$a;J)V

    :cond_0
    return-void
.end method

.method public q()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lj1/r;->j:Lj1/u;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lj1/u;->q()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lj1/r;->i:Lj1/x;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lj1/x;->d()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lj1/r;->l:Lj1/r$a;

    if-eqz v1, :cond_2

    iget-boolean v2, p0, Lj1/r;->m:Z

    if-nez v2, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lj1/r;->m:Z

    iget-object v2, p0, Lj1/r;->f:Lj1/x$b;

    invoke-interface {v1, v2, v0}, Lj1/r$a;->b(Lj1/x$b;Ljava/io/IOException;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    throw v0
.end method

.method public r(JZ)V
    .locals 1

    iget-object v0, p0, Lj1/r;->j:Lj1/u;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/u;

    invoke-interface {v0, p1, p2, p3}, Lj1/u;->r(JZ)V

    return-void
.end method

.method public s(J)J
    .locals 1

    iget-object v0, p0, Lj1/r;->j:Lj1/u;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/u;

    invoke-interface {v0, p1, p2}, Lj1/u;->s(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public u([Lc2/t;[Z[Lj1/q0;[ZJ)J
    .locals 15

    move-object v0, p0

    iget-wide v1, v0, Lj1/r;->n:J

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v5, v1, v3

    if-eqz v5, :cond_0

    iget-wide v5, v0, Lj1/r;->g:J

    cmp-long v7, p5, v5

    if-nez v7, :cond_0

    iput-wide v3, v0, Lj1/r;->n:J

    move-wide v13, v1

    goto :goto_0

    :cond_0
    move-wide/from16 v13, p5

    :goto_0
    iget-object v1, v0, Lj1/r;->j:Lj1/u;

    invoke-static {v1}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lj1/u;

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    invoke-interface/range {v8 .. v14}, Lj1/u;->u([Lc2/t;[Z[Lj1/q0;[ZJ)J

    move-result-wide v1

    return-wide v1
.end method

.method public v(Lj1/u;)V
    .locals 0

    iget-object p1, p0, Lj1/r;->k:Lj1/u$a;

    invoke-static {p1}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/u$a;

    invoke-interface {p1, p0}, Lj1/r0$a;->i(Lj1/r0;)V

    return-void
.end method

.method public w(J)V
    .locals 0

    iput-wide p1, p0, Lj1/r;->n:J

    return-void
.end method

.method public x()V
    .locals 2

    iget-object v0, p0, Lj1/r;->j:Lj1/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lj1/r;->i:Lj1/x;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/x;

    iget-object v1, p0, Lj1/r;->j:Lj1/u;

    invoke-interface {v0, v1}, Lj1/x;->i(Lj1/u;)V

    :cond_0
    return-void
.end method

.method public y(Lj1/x;)V
    .locals 1

    iget-object v0, p0, Lj1/r;->i:Lj1/x;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->f(Z)V

    iput-object p1, p0, Lj1/r;->i:Lj1/x;

    return-void
.end method
