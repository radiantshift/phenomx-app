.class final Lj1/k0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lj1/u;
.implements Lm0/n;
.implements Ld2/h0$b;
.implements Ld2/h0$f;
.implements Lj1/p0$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj1/k0$d;,
        Lj1/k0$e;,
        Lj1/k0$a;,
        Lj1/k0$c;,
        Lj1/k0$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lj1/u;",
        "Lm0/n;",
        "Ld2/h0$b<",
        "Lj1/k0$a;",
        ">;",
        "Ld2/h0$f;",
        "Lj1/p0$d;"
    }
.end annotation


# static fields
.field private static final R:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final S:Lh0/r1;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Lj1/k0$e;

.field private D:Lm0/b0;

.field private E:J

.field private F:Z

.field private G:I

.field private H:Z

.field private I:Z

.field private J:I

.field private K:Z

.field private L:J

.field private M:J

.field private N:Z

.field private O:I

.field private P:Z

.field private Q:Z

.field private final f:Landroid/net/Uri;

.field private final g:Ld2/l;

.field private final h:Ll0/y;

.field private final i:Ld2/g0;

.field private final j:Lj1/e0$a;

.field private final k:Ll0/w$a;

.field private final l:Lj1/k0$b;

.field private final m:Ld2/b;

.field private final n:Ljava/lang/String;

.field private final o:J

.field private final p:Ld2/h0;

.field private final q:Lj1/f0;

.field private final r:Le2/g;

.field private final s:Ljava/lang/Runnable;

.field private final t:Ljava/lang/Runnable;

.field private final u:Landroid/os/Handler;

.field private v:Lj1/u$a;

.field private w:Ld1/b;

.field private x:[Lj1/p0;

.field private y:[Lj1/k0$d;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lj1/k0;->L()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lj1/k0;->R:Ljava/util/Map;

    new-instance v0, Lh0/r1$b;

    invoke-direct {v0}, Lh0/r1$b;-><init>()V

    const-string v1, "icy"

    invoke-virtual {v0, v1}, Lh0/r1$b;->U(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v0

    const-string v1, "application/x-icy"

    invoke-virtual {v0, v1}, Lh0/r1$b;->g0(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v0

    invoke-virtual {v0}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object v0

    sput-object v0, Lj1/k0;->S:Lh0/r1;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ld2/l;Lj1/f0;Ll0/y;Ll0/w$a;Ld2/g0;Lj1/e0$a;Lj1/k0$b;Ld2/b;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lj1/k0;->f:Landroid/net/Uri;

    iput-object p2, p0, Lj1/k0;->g:Ld2/l;

    iput-object p4, p0, Lj1/k0;->h:Ll0/y;

    iput-object p5, p0, Lj1/k0;->k:Ll0/w$a;

    iput-object p6, p0, Lj1/k0;->i:Ld2/g0;

    iput-object p7, p0, Lj1/k0;->j:Lj1/e0$a;

    iput-object p8, p0, Lj1/k0;->l:Lj1/k0$b;

    iput-object p9, p0, Lj1/k0;->m:Ld2/b;

    iput-object p10, p0, Lj1/k0;->n:Ljava/lang/String;

    int-to-long p1, p11

    iput-wide p1, p0, Lj1/k0;->o:J

    new-instance p1, Ld2/h0;

    const-string p2, "ProgressiveMediaPeriod"

    invoke-direct {p1, p2}, Ld2/h0;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lj1/k0;->p:Ld2/h0;

    iput-object p3, p0, Lj1/k0;->q:Lj1/f0;

    new-instance p1, Le2/g;

    invoke-direct {p1}, Le2/g;-><init>()V

    iput-object p1, p0, Lj1/k0;->r:Le2/g;

    new-instance p1, Lj1/g0;

    invoke-direct {p1, p0}, Lj1/g0;-><init>(Lj1/k0;)V

    iput-object p1, p0, Lj1/k0;->s:Ljava/lang/Runnable;

    new-instance p1, Lj1/i0;

    invoke-direct {p1, p0}, Lj1/i0;-><init>(Lj1/k0;)V

    iput-object p1, p0, Lj1/k0;->t:Ljava/lang/Runnable;

    invoke-static {}, Le2/n0;->w()Landroid/os/Handler;

    move-result-object p1

    iput-object p1, p0, Lj1/k0;->u:Landroid/os/Handler;

    const/4 p1, 0x0

    new-array p2, p1, [Lj1/k0$d;

    iput-object p2, p0, Lj1/k0;->y:[Lj1/k0$d;

    new-array p1, p1, [Lj1/p0;

    iput-object p1, p0, Lj1/k0;->x:[Lj1/p0;

    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p1, p0, Lj1/k0;->M:J

    iput-wide p1, p0, Lj1/k0;->E:J

    const/4 p1, 0x1

    iput p1, p0, Lj1/k0;->G:I

    return-void
.end method

.method static synthetic A(Lj1/k0;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lj1/k0;->u:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic B(Lj1/k0;Z)J
    .locals 0

    invoke-direct {p0, p1}, Lj1/k0;->N(Z)J

    move-result-wide p0

    return-wide p0
.end method

.method static synthetic C()Ljava/util/Map;
    .locals 1

    sget-object v0, Lj1/k0;->R:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic D(Lj1/k0;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lj1/k0;->n:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic E(Lj1/k0;)V
    .locals 0

    invoke-direct {p0}, Lj1/k0;->Z()V

    return-void
.end method

.method static synthetic F(Lj1/k0;)Ld1/b;
    .locals 0

    iget-object p0, p0, Lj1/k0;->w:Ld1/b;

    return-object p0
.end method

.method static synthetic G(Lj1/k0;Ld1/b;)Ld1/b;
    .locals 0

    iput-object p1, p0, Lj1/k0;->w:Ld1/b;

    return-object p1
.end method

.method static synthetic H()Lh0/r1;
    .locals 1

    sget-object v0, Lj1/k0;->S:Lh0/r1;

    return-object v0
.end method

.method static synthetic I(Lj1/k0;)J
    .locals 2

    iget-wide v0, p0, Lj1/k0;->o:J

    return-wide v0
.end method

.method private J()V
    .locals 1
    .annotation runtime Lorg/checkerframework/checker/nullness/qual/EnsuresNonNull;
        value = {
            "trackState",
            "seekMap"
        }
    .end annotation

    iget-boolean v0, p0, Lj1/k0;->A:Z

    invoke-static {v0}, Le2/a;->f(Z)V

    iget-object v0, p0, Lj1/k0;->C:Lj1/k0$e;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lj1/k0;->D:Lm0/b0;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private K(Lj1/k0$a;I)Z
    .locals 6

    iget-boolean v0, p0, Lj1/k0;->K:Z

    const/4 v1, 0x1

    if-nez v0, :cond_3

    iget-object v0, p0, Lj1/k0;->D:Lm0/b0;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lm0/b0;->j()J

    move-result-wide v2

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-boolean p2, p0, Lj1/k0;->A:Z

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-direct {p0}, Lj1/k0;->k0()Z

    move-result p2

    if-nez p2, :cond_1

    iput-boolean v1, p0, Lj1/k0;->N:Z

    return v0

    :cond_1
    iget-boolean p2, p0, Lj1/k0;->A:Z

    iput-boolean p2, p0, Lj1/k0;->I:Z

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lj1/k0;->L:J

    iput v0, p0, Lj1/k0;->O:I

    iget-object p2, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v4, p2

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, p2, v0

    invoke-virtual {v5}, Lj1/p0;->V()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-static {p1, v2, v3, v2, v3}, Lj1/k0$a;->h(Lj1/k0$a;JJ)V

    return v1

    :cond_3
    :goto_1
    iput p2, p0, Lj1/k0;->O:I

    return v1
.end method

.method private static L()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "Icy-MetaData"

    const-string v2, "1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private M()I
    .locals 5

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v4, v0, v2

    invoke-virtual {v4}, Lj1/p0;->G()I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return v3
.end method

.method private N(Z)J
    .locals 5

    const-wide/high16 v0, -0x8000000000000000L

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    if-nez p1, :cond_0

    iget-object v3, p0, Lj1/k0;->C:Lj1/k0$e;

    invoke-static {v3}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lj1/k0$e;

    iget-object v3, v3, Lj1/k0$e;->c:[Z

    aget-boolean v3, v3, v2

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lj1/p0;->z()J

    move-result-wide v3

    invoke-static {v0, v1, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-wide v0
.end method

.method private P()Z
    .locals 5

    iget-wide v0, p0, Lj1/k0;->M:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private synthetic R()V
    .locals 1

    iget-boolean v0, p0, Lj1/k0;->Q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lj1/k0;->v:Lj1/u$a;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/u$a;

    invoke-interface {v0, p0}, Lj1/r0$a;->i(Lj1/r0;)V

    :cond_0
    return-void
.end method

.method private synthetic S()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lj1/k0;->K:Z

    return-void
.end method

.method private synthetic T(Lm0/b0;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/k0;->h0(Lm0/b0;)V

    return-void
.end method

.method private U()V
    .locals 11

    iget-boolean v0, p0, Lj1/k0;->Q:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lj1/k0;->A:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lj1/k0;->z:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lj1/k0;->D:Lm0/b0;

    if-nez v0, :cond_0

    goto/16 :goto_5

    :cond_0
    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lj1/p0;->F()Lh0/r1;

    move-result-object v4

    if-nez v4, :cond_1

    return-void

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lj1/k0;->r:Le2/g;

    invoke-virtual {v0}, Le2/g;->c()Z

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v0, v0

    new-array v1, v0, [Lj1/x0;

    new-array v3, v0, [Z

    const/4 v4, 0x0

    :goto_1
    const/4 v5, 0x1

    if-ge v4, v0, :cond_9

    iget-object v6, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object v6, v6, v4

    invoke-virtual {v6}, Lj1/p0;->F()Lh0/r1;

    move-result-object v6

    invoke-static {v6}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lh0/r1;

    iget-object v7, v6, Lh0/r1;->q:Ljava/lang/String;

    invoke-static {v7}, Le2/v;->o(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    invoke-static {v7}, Le2/v;->s(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    goto :goto_2

    :cond_3
    const/4 v7, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v7, 0x1

    :goto_3
    aput-boolean v7, v3, v4

    iget-boolean v9, p0, Lj1/k0;->B:Z

    or-int/2addr v7, v9

    iput-boolean v7, p0, Lj1/k0;->B:Z

    iget-object v7, p0, Lj1/k0;->w:Ld1/b;

    if-eqz v7, :cond_8

    if-nez v8, :cond_5

    iget-object v9, p0, Lj1/k0;->y:[Lj1/k0$d;

    aget-object v9, v9, v4

    iget-boolean v9, v9, Lj1/k0$d;->b:Z

    if-eqz v9, :cond_7

    :cond_5
    iget-object v9, v6, Lh0/r1;->o:Lz0/a;

    if-nez v9, :cond_6

    new-instance v9, Lz0/a;

    new-array v10, v5, [Lz0/a$b;

    aput-object v7, v10, v2

    invoke-direct {v9, v10}, Lz0/a;-><init>([Lz0/a$b;)V

    goto :goto_4

    :cond_6
    new-array v10, v5, [Lz0/a$b;

    aput-object v7, v10, v2

    invoke-virtual {v9, v10}, Lz0/a;->d([Lz0/a$b;)Lz0/a;

    move-result-object v9

    :goto_4
    invoke-virtual {v6}, Lh0/r1;->b()Lh0/r1$b;

    move-result-object v6

    invoke-virtual {v6, v9}, Lh0/r1$b;->Z(Lz0/a;)Lh0/r1$b;

    move-result-object v6

    invoke-virtual {v6}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object v6

    :cond_7
    if-eqz v8, :cond_8

    iget v8, v6, Lh0/r1;->k:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_8

    iget v8, v6, Lh0/r1;->l:I

    if-ne v8, v9, :cond_8

    iget v8, v7, Ld1/b;->f:I

    if-eq v8, v9, :cond_8

    invoke-virtual {v6}, Lh0/r1;->b()Lh0/r1$b;

    move-result-object v6

    iget v7, v7, Ld1/b;->f:I

    invoke-virtual {v6, v7}, Lh0/r1$b;->I(I)Lh0/r1$b;

    move-result-object v6

    invoke-virtual {v6}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object v6

    :cond_8
    iget-object v7, p0, Lj1/k0;->h:Ll0/y;

    invoke-interface {v7, v6}, Ll0/y;->d(Lh0/r1;)I

    move-result v7

    invoke-virtual {v6, v7}, Lh0/r1;->c(I)Lh0/r1;

    move-result-object v6

    new-instance v7, Lj1/x0;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    new-array v5, v5, [Lh0/r1;

    aput-object v6, v5, v2

    invoke-direct {v7, v8, v5}, Lj1/x0;-><init>(Ljava/lang/String;[Lh0/r1;)V

    aput-object v7, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_9
    new-instance v0, Lj1/k0$e;

    new-instance v2, Lj1/z0;

    invoke-direct {v2, v1}, Lj1/z0;-><init>([Lj1/x0;)V

    invoke-direct {v0, v2, v3}, Lj1/k0$e;-><init>(Lj1/z0;[Z)V

    iput-object v0, p0, Lj1/k0;->C:Lj1/k0$e;

    iput-boolean v5, p0, Lj1/k0;->A:Z

    iget-object v0, p0, Lj1/k0;->v:Lj1/u$a;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/u$a;

    invoke-interface {v0, p0}, Lj1/u$a;->e(Lj1/u;)V

    :cond_a
    :goto_5
    return-void
.end method

.method private V(I)V
    .locals 10

    invoke-direct {p0}, Lj1/k0;->J()V

    iget-object v0, p0, Lj1/k0;->C:Lj1/k0$e;

    iget-object v1, v0, Lj1/k0$e;->d:[Z

    aget-boolean v2, v1, p1

    if-nez v2, :cond_0

    iget-object v0, v0, Lj1/k0$e;->a:Lj1/z0;

    invoke-virtual {v0, p1}, Lj1/z0;->b(I)Lj1/x0;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lj1/x0;->b(I)Lh0/r1;

    move-result-object v5

    iget-object v3, p0, Lj1/k0;->j:Lj1/e0$a;

    iget-object v0, v5, Lh0/r1;->q:Ljava/lang/String;

    invoke-static {v0}, Le2/v;->k(Ljava/lang/String;)I

    move-result v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-wide v8, p0, Lj1/k0;->L:J

    invoke-virtual/range {v3 .. v9}, Lj1/e0$a;->i(ILh0/r1;ILjava/lang/Object;J)V

    const/4 v0, 0x1

    aput-boolean v0, v1, p1

    :cond_0
    return-void
.end method

.method private W(I)V
    .locals 3

    invoke-direct {p0}, Lj1/k0;->J()V

    iget-object v0, p0, Lj1/k0;->C:Lj1/k0$e;

    iget-object v0, v0, Lj1/k0$e;->b:[Z

    iget-boolean v1, p0, Lj1/k0;->N:Z

    if-eqz v1, :cond_2

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object p1, v0, p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lj1/p0;->K(Z)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_1

    :cond_0
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lj1/k0;->M:J

    iput-boolean v0, p0, Lj1/k0;->N:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lj1/k0;->I:Z

    iput-wide v1, p0, Lj1/k0;->L:J

    iput v0, p0, Lj1/k0;->O:I

    iget-object p1, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v1, p1

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    invoke-virtual {v2}, Lj1/p0;->V()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lj1/k0;->v:Lj1/u$a;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/u$a;

    invoke-interface {p1, p0}, Lj1/r0$a;->i(Lj1/r0;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private Z()V
    .locals 2

    iget-object v0, p0, Lj1/k0;->u:Landroid/os/Handler;

    new-instance v1, Lj1/h0;

    invoke-direct {v1, p0}, Lj1/h0;-><init>(Lj1/k0;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private d0(Lj1/k0$d;)Lm0/e0;
    .locals 4

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v0, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lj1/k0;->y:[Lj1/k0$d;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Lj1/k0$d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object p1, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object p1, p1, v1

    return-object p1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lj1/k0;->m:Ld2/b;

    iget-object v2, p0, Lj1/k0;->h:Ll0/y;

    iget-object v3, p0, Lj1/k0;->k:Ll0/w$a;

    invoke-static {v1, v2, v3}, Lj1/p0;->k(Ld2/b;Ll0/y;Ll0/w$a;)Lj1/p0;

    move-result-object v1

    invoke-virtual {v1, p0}, Lj1/p0;->d0(Lj1/p0$d;)V

    iget-object v2, p0, Lj1/k0;->y:[Lj1/k0$d;

    add-int/lit8 v3, v0, 0x1

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lj1/k0$d;

    aput-object p1, v2, v0

    invoke-static {v2}, Le2/n0;->k([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lj1/k0$d;

    iput-object p1, p0, Lj1/k0;->y:[Lj1/k0$d;

    iget-object p1, p0, Lj1/k0;->x:[Lj1/p0;

    invoke-static {p1, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lj1/p0;

    aput-object v1, p1, v0

    invoke-static {p1}, Le2/n0;->k([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lj1/p0;

    iput-object p1, p0, Lj1/k0;->x:[Lj1/p0;

    return-object v1
.end method

.method private g0([ZJ)Z
    .locals 4

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    iget-object v3, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object v3, v3, v2

    invoke-virtual {v3, p2, p3, v1}, Lj1/p0;->Z(JZ)Z

    move-result v3

    if-nez v3, :cond_1

    aget-boolean v3, p1, v2

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lj1/k0;->B:Z

    if-nez v3, :cond_1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method private h0(Lm0/b0;)V
    .locals 6

    iget-object v0, p0, Lj1/k0;->w:Ld1/b;

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    if-nez v0, :cond_0

    move-object v0, p1

    goto :goto_0

    :cond_0
    new-instance v0, Lm0/b0$b;

    invoke-direct {v0, v1, v2}, Lm0/b0$b;-><init>(J)V

    :goto_0
    iput-object v0, p0, Lj1/k0;->D:Lm0/b0;

    invoke-interface {p1}, Lm0/b0;->j()J

    move-result-wide v3

    iput-wide v3, p0, Lj1/k0;->E:J

    iget-boolean v0, p0, Lj1/k0;->K:Z

    const/4 v3, 0x1

    if-nez v0, :cond_1

    invoke-interface {p1}, Lm0/b0;->j()J

    move-result-wide v4

    cmp-long v0, v4, v1

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, Lj1/k0;->F:Z

    if-eqz v0, :cond_2

    const/4 v3, 0x7

    :cond_2
    iput v3, p0, Lj1/k0;->G:I

    iget-object v0, p0, Lj1/k0;->l:Lj1/k0$b;

    iget-wide v1, p0, Lj1/k0;->E:J

    invoke-interface {p1}, Lm0/b0;->h()Z

    move-result p1

    iget-boolean v3, p0, Lj1/k0;->F:Z

    invoke-interface {v0, v1, v2, p1, v3}, Lj1/k0$b;->r(JZZ)V

    iget-boolean p1, p0, Lj1/k0;->A:Z

    if-nez p1, :cond_3

    invoke-direct {p0}, Lj1/k0;->U()V

    :cond_3
    return-void
.end method

.method private j0()V
    .locals 26

    move-object/from16 v7, p0

    new-instance v8, Lj1/k0$a;

    iget-object v2, v7, Lj1/k0;->f:Landroid/net/Uri;

    iget-object v3, v7, Lj1/k0;->g:Ld2/l;

    iget-object v4, v7, Lj1/k0;->q:Lj1/f0;

    iget-object v6, v7, Lj1/k0;->r:Le2/g;

    move-object v0, v8

    move-object/from16 v1, p0

    move-object/from16 v5, p0

    invoke-direct/range {v0 .. v6}, Lj1/k0$a;-><init>(Lj1/k0;Landroid/net/Uri;Ld2/l;Lj1/f0;Lm0/n;Le2/g;)V

    iget-boolean v0, v7, Lj1/k0;->A:Z

    if-eqz v0, :cond_2

    invoke-direct/range {p0 .. p0}, Lj1/k0;->P()Z

    move-result v0

    invoke-static {v0}, Le2/a;->f(Z)V

    iget-wide v0, v7, Lj1/k0;->E:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    iget-wide v4, v7, Lj1/k0;->M:J

    cmp-long v6, v4, v0

    if-lez v6, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, v7, Lj1/k0;->P:Z

    iput-wide v2, v7, Lj1/k0;->M:J

    return-void

    :cond_0
    iget-object v0, v7, Lj1/k0;->D:Lm0/b0;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lm0/b0;

    iget-wide v4, v7, Lj1/k0;->M:J

    invoke-interface {v0, v4, v5}, Lm0/b0;->i(J)Lm0/b0$a;

    move-result-object v0

    iget-object v0, v0, Lm0/b0$a;->a:Lm0/c0;

    iget-wide v0, v0, Lm0/c0;->b:J

    iget-wide v4, v7, Lj1/k0;->M:J

    invoke-static {v8, v0, v1, v4, v5}, Lj1/k0$a;->h(Lj1/k0$a;JJ)V

    iget-object v0, v7, Lj1/k0;->x:[Lj1/p0;

    array-length v1, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_1

    aget-object v5, v0, v4

    iget-wide v9, v7, Lj1/k0;->M:J

    invoke-virtual {v5, v9, v10}, Lj1/p0;->b0(J)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    iput-wide v2, v7, Lj1/k0;->M:J

    :cond_2
    invoke-direct/range {p0 .. p0}, Lj1/k0;->M()I

    move-result v0

    iput v0, v7, Lj1/k0;->O:I

    iget-object v0, v7, Lj1/k0;->p:Ld2/h0;

    iget-object v1, v7, Lj1/k0;->i:Ld2/g0;

    iget v2, v7, Lj1/k0;->G:I

    invoke-interface {v1, v2}, Ld2/g0;->d(I)I

    move-result v1

    invoke-virtual {v0, v8, v7, v1}, Ld2/h0;->n(Ld2/h0$e;Ld2/h0$b;I)J

    move-result-wide v13

    invoke-static {v8}, Lj1/k0$a;->f(Lj1/k0$a;)Ld2/p;

    move-result-object v12

    iget-object v15, v7, Lj1/k0;->j:Lj1/e0$a;

    new-instance v16, Lj1/q;

    invoke-static {v8}, Lj1/k0$a;->e(Lj1/k0$a;)J

    move-result-wide v10

    move-object/from16 v9, v16

    invoke-direct/range {v9 .. v14}, Lj1/q;-><init>(JLd2/p;J)V

    const/16 v17, 0x1

    const/16 v18, -0x1

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-static {v8}, Lj1/k0$a;->g(Lj1/k0$a;)J

    move-result-wide v22

    iget-wide v0, v7, Lj1/k0;->E:J

    move-wide/from16 v24, v0

    invoke-virtual/range {v15 .. v25}, Lj1/e0$a;->A(Lj1/q;IILh0/r1;ILjava/lang/Object;JJ)V

    return-void
.end method

.method private k0()Z
    .locals 1

    iget-boolean v0, p0, Lj1/k0;->I:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lj1/k0;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public static synthetic v(Lj1/k0;)V
    .locals 0

    invoke-direct {p0}, Lj1/k0;->U()V

    return-void
.end method

.method public static synthetic w(Lj1/k0;)V
    .locals 0

    invoke-direct {p0}, Lj1/k0;->S()V

    return-void
.end method

.method public static synthetic x(Lj1/k0;Lm0/b0;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/k0;->T(Lm0/b0;)V

    return-void
.end method

.method public static synthetic y(Lj1/k0;)V
    .locals 0

    invoke-direct {p0}, Lj1/k0;->R()V

    return-void
.end method

.method static synthetic z(Lj1/k0;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lj1/k0;->t:Ljava/lang/Runnable;

    return-object p0
.end method


# virtual methods
.method O()Lm0/e0;
    .locals 3

    new-instance v0, Lj1/k0$d;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lj1/k0$d;-><init>(IZ)V

    invoke-direct {p0, v0}, Lj1/k0;->d0(Lj1/k0$d;)Lm0/e0;

    move-result-object v0

    return-object v0
.end method

.method Q(I)Z
    .locals 1

    invoke-direct {p0}, Lj1/k0;->k0()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object p1, v0, p1

    iget-boolean v0, p0, Lj1/k0;->P:Z

    invoke-virtual {p1, v0}, Lj1/p0;->K(Z)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method X()V
    .locals 3

    iget-object v0, p0, Lj1/k0;->p:Ld2/h0;

    iget-object v1, p0, Lj1/k0;->i:Ld2/g0;

    iget v2, p0, Lj1/k0;->G:I

    invoke-interface {v1, v2}, Ld2/g0;->d(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ld2/h0;->k(I)V

    return-void
.end method

.method Y(I)V
    .locals 1

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object p1, v0, p1

    invoke-virtual {p1}, Lj1/p0;->N()V

    invoke-virtual {p0}, Lj1/k0;->X()V

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lj1/k0;->p:Ld2/h0;

    invoke-virtual {v0}, Ld2/h0;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lj1/k0;->r:Le2/g;

    invoke-virtual {v0}, Le2/g;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public a0(Lj1/k0$a;JJZ)V
    .locals 15

    move-object v0, p0

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->d(Lj1/k0$a;)Ld2/o0;

    move-result-object v1

    new-instance v14, Lj1/q;

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->e(Lj1/k0$a;)J

    move-result-wide v3

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->f(Lj1/k0$a;)Ld2/p;

    move-result-object v5

    invoke-virtual {v1}, Ld2/o0;->r()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v1}, Ld2/o0;->s()Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v1}, Ld2/o0;->q()J

    move-result-wide v12

    move-object v2, v14

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    invoke-direct/range {v2 .. v13}, Lj1/q;-><init>(JLd2/p;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    iget-object v1, v0, Lj1/k0;->i:Ld2/g0;

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->e(Lj1/k0$a;)J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Ld2/g0;->a(J)V

    iget-object v2, v0, Lj1/k0;->j:Lj1/e0$a;

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->g(Lj1/k0$a;)J

    move-result-wide v9

    iget-wide v11, v0, Lj1/k0;->E:J

    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v14

    invoke-virtual/range {v2 .. v12}, Lj1/e0$a;->r(Lj1/q;IILh0/r1;ILjava/lang/Object;JJ)V

    if-nez p6, :cond_1

    iget-object v1, v0, Lj1/k0;->x:[Lj1/p0;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v1, v3

    invoke-virtual {v4}, Lj1/p0;->V()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    iget v1, v0, Lj1/k0;->J:I

    if-lez v1, :cond_1

    iget-object v1, v0, Lj1/k0;->v:Lj1/u$a;

    invoke-static {v1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/u$a;

    invoke-interface {v1, p0}, Lj1/r0$a;->i(Lj1/r0;)V

    :cond_1
    return-void
.end method

.method public b(Lh0/r1;)V
    .locals 1

    iget-object p1, p0, Lj1/k0;->u:Landroid/os/Handler;

    iget-object v0, p0, Lj1/k0;->s:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public b0(Lj1/k0$a;JJ)V
    .locals 16

    move-object/from16 v0, p0

    iget-wide v1, v0, Lj1/k0;->E:J

    const/4 v3, 0x1

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v6, v1, v4

    if-nez v6, :cond_1

    iget-object v1, v0, Lj1/k0;->D:Lm0/b0;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lm0/b0;->h()Z

    move-result v1

    invoke-direct {v0, v3}, Lj1/k0;->N(Z)J

    move-result-wide v4

    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v2, v4, v6

    if-nez v2, :cond_0

    const-wide/16 v4, 0x0

    goto :goto_0

    :cond_0
    const-wide/16 v6, 0x2710

    add-long/2addr v4, v6

    :goto_0
    iput-wide v4, v0, Lj1/k0;->E:J

    iget-object v2, v0, Lj1/k0;->l:Lj1/k0$b;

    iget-boolean v6, v0, Lj1/k0;->F:Z

    invoke-interface {v2, v4, v5, v1, v6}, Lj1/k0$b;->r(JZZ)V

    :cond_1
    invoke-static/range {p1 .. p1}, Lj1/k0$a;->d(Lj1/k0$a;)Ld2/o0;

    move-result-object v1

    new-instance v2, Lj1/q;

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->e(Lj1/k0$a;)J

    move-result-wide v5

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->f(Lj1/k0$a;)Ld2/p;

    move-result-object v7

    invoke-virtual {v1}, Ld2/o0;->r()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v1}, Ld2/o0;->s()Ljava/util/Map;

    move-result-object v9

    invoke-virtual {v1}, Ld2/o0;->q()J

    move-result-wide v14

    move-object v4, v2

    move-wide/from16 v10, p2

    move-wide/from16 v12, p4

    invoke-direct/range {v4 .. v15}, Lj1/q;-><init>(JLd2/p;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    iget-object v1, v0, Lj1/k0;->i:Ld2/g0;

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->e(Lj1/k0$a;)J

    move-result-wide v4

    invoke-interface {v1, v4, v5}, Ld2/g0;->a(J)V

    iget-object v4, v0, Lj1/k0;->j:Lj1/e0$a;

    const/4 v6, 0x1

    const/4 v7, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->g(Lj1/k0$a;)J

    move-result-wide v11

    iget-wide v13, v0, Lj1/k0;->E:J

    move-object v5, v2

    invoke-virtual/range {v4 .. v14}, Lj1/e0$a;->u(Lj1/q;IILh0/r1;ILjava/lang/Object;JJ)V

    iput-boolean v3, v0, Lj1/k0;->P:Z

    iget-object v1, v0, Lj1/k0;->v:Lj1/u$a;

    invoke-static {v1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/u$a;

    invoke-interface {v1, v0}, Lj1/r0$a;->i(Lj1/r0;)V

    return-void
.end method

.method public c(JLh0/u3;)J
    .locals 9

    invoke-direct {p0}, Lj1/k0;->J()V

    iget-object v0, p0, Lj1/k0;->D:Lm0/b0;

    invoke-interface {v0}, Lm0/b0;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 p1, 0x0

    return-wide p1

    :cond_0
    iget-object v0, p0, Lj1/k0;->D:Lm0/b0;

    invoke-interface {v0, p1, p2}, Lm0/b0;->i(J)Lm0/b0$a;

    move-result-object v0

    iget-object v1, v0, Lm0/b0$a;->a:Lm0/c0;

    iget-wide v5, v1, Lm0/c0;->a:J

    iget-object v0, v0, Lm0/b0$a;->b:Lm0/c0;

    iget-wide v7, v0, Lm0/c0;->a:J

    move-object v2, p3

    move-wide v3, p1

    invoke-virtual/range {v2 .. v8}, Lh0/u3;->a(JJJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public c0(Lj1/k0$a;JJLjava/io/IOException;I)Ld2/h0$c;
    .locals 25

    move-object/from16 v0, p0

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->d(Lj1/k0$a;)Ld2/o0;

    move-result-object v1

    new-instance v14, Lj1/q;

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->e(Lj1/k0$a;)J

    move-result-wide v3

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->f(Lj1/k0$a;)Ld2/p;

    move-result-object v5

    invoke-virtual {v1}, Ld2/o0;->r()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v1}, Ld2/o0;->s()Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v1}, Ld2/o0;->q()J

    move-result-wide v12

    move-object v2, v14

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    invoke-direct/range {v2 .. v13}, Lj1/q;-><init>(JLd2/p;Landroid/net/Uri;Ljava/util/Map;JJJ)V

    new-instance v1, Lj1/t;

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->g(Lj1/k0$a;)J

    move-result-wide v2

    invoke-static {v2, v3}, Le2/n0;->Z0(J)J

    move-result-wide v21

    iget-wide v2, v0, Lj1/k0;->E:J

    invoke-static {v2, v3}, Le2/n0;->Z0(J)J

    move-result-wide v23

    const/16 v16, 0x1

    const/16 v17, -0x1

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object v15, v1

    invoke-direct/range {v15 .. v24}, Lj1/t;-><init>(IILh0/r1;ILjava/lang/Object;JJ)V

    iget-object v2, v0, Lj1/k0;->i:Ld2/g0;

    new-instance v3, Ld2/g0$c;

    move-object/from16 v13, p6

    move/from16 v4, p7

    invoke-direct {v3, v14, v1, v13, v4}, Ld2/g0$c;-><init>(Lj1/q;Lj1/t;Ljava/io/IOException;I)V

    invoke-interface {v2, v3}, Ld2/g0;->c(Ld2/g0$c;)J

    move-result-wide v1

    const/4 v3, 0x1

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v6, v1, v4

    if-nez v6, :cond_0

    sget-object v1, Ld2/h0;->g:Ld2/h0$c;

    move-object/from16 v15, p1

    goto :goto_1

    :cond_0
    invoke-direct/range {p0 .. p0}, Lj1/k0;->M()I

    move-result v4

    iget v5, v0, Lj1/k0;->O:I

    if-le v4, v5, :cond_1

    move-object/from16 v15, p1

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    move-object/from16 v15, p1

    :goto_0
    invoke-direct {v0, v15, v4}, Lj1/k0;->K(Lj1/k0$a;I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v5, v1, v2}, Ld2/h0;->h(ZJ)Ld2/h0$c;

    move-result-object v1

    goto :goto_1

    :cond_2
    sget-object v1, Ld2/h0;->f:Ld2/h0$c;

    :goto_1
    invoke-virtual {v1}, Ld2/h0$c;->c()Z

    move-result v2

    xor-int/lit8 v16, v2, 0x1

    iget-object v2, v0, Lj1/k0;->j:Lj1/e0$a;

    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->g(Lj1/k0$a;)J

    move-result-wide v9

    iget-wide v11, v0, Lj1/k0;->E:J

    move-object v3, v14

    move-object/from16 v13, p6

    move/from16 v14, v16

    invoke-virtual/range {v2 .. v14}, Lj1/e0$a;->w(Lj1/q;IILh0/r1;ILjava/lang/Object;JJLjava/io/IOException;Z)V

    if-eqz v16, :cond_3

    iget-object v2, v0, Lj1/k0;->i:Ld2/g0;

    invoke-static/range {p1 .. p1}, Lj1/k0$a;->e(Lj1/k0$a;)J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Ld2/g0;->a(J)V

    :cond_3
    return-object v1
.end method

.method public d()J
    .locals 2

    invoke-virtual {p0}, Lj1/k0;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public e(II)Lm0/e0;
    .locals 1

    new-instance p2, Lj1/k0$d;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lj1/k0$d;-><init>(IZ)V

    invoke-direct {p0, p2}, Lj1/k0;->d0(Lj1/k0$d;)Lm0/e0;

    move-result-object p1

    return-object p1
.end method

.method e0(ILh0/s1;Lk0/g;I)I
    .locals 3

    invoke-direct {p0}, Lj1/k0;->k0()Z

    move-result v0

    const/4 v1, -0x3

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-direct {p0, p1}, Lj1/k0;->V(I)V

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object v0, v0, p1

    iget-boolean v2, p0, Lj1/k0;->P:Z

    invoke-virtual {v0, p2, p3, p4, v2}, Lj1/p0;->S(Lh0/s1;Lk0/g;IZ)I

    move-result p2

    if-ne p2, v1, :cond_1

    invoke-direct {p0, p1}, Lj1/k0;->W(I)V

    :cond_1
    return p2
.end method

.method public f()J
    .locals 11

    invoke-direct {p0}, Lj1/k0;->J()V

    iget-boolean v0, p0, Lj1/k0;->P:Z

    const-wide/high16 v1, -0x8000000000000000L

    if-nez v0, :cond_7

    iget v0, p0, Lj1/k0;->J:I

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-direct {p0}, Lj1/k0;->P()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lj1/k0;->M:J

    return-wide v0

    :cond_1
    iget-boolean v0, p0, Lj1/k0;->B:Z

    const/4 v3, 0x0

    const-wide v4, 0x7fffffffffffffffL

    if-eqz v0, :cond_3

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v0, v0

    move-wide v7, v4

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v0, :cond_4

    iget-object v9, p0, Lj1/k0;->C:Lj1/k0$e;

    iget-object v10, v9, Lj1/k0$e;->b:[Z

    aget-boolean v10, v10, v6

    if-eqz v10, :cond_2

    iget-object v9, v9, Lj1/k0$e;->c:[Z

    aget-boolean v9, v9, v6

    if-eqz v9, :cond_2

    iget-object v9, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object v9, v9, v6

    invoke-virtual {v9}, Lj1/p0;->J()Z

    move-result v9

    if-nez v9, :cond_2

    iget-object v9, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object v9, v9, v6

    invoke-virtual {v9}, Lj1/p0;->z()J

    move-result-wide v9

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_3
    move-wide v7, v4

    :cond_4
    cmp-long v0, v7, v4

    if-nez v0, :cond_5

    invoke-direct {p0, v3}, Lj1/k0;->N(Z)J

    move-result-wide v7

    :cond_5
    cmp-long v0, v7, v1

    if-nez v0, :cond_6

    iget-wide v7, p0, Lj1/k0;->L:J

    :cond_6
    return-wide v7

    :cond_7
    :goto_1
    return-wide v1
.end method

.method public f0()V
    .locals 4

    iget-boolean v0, p0, Lj1/k0;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3}, Lj1/p0;->R()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lj1/k0;->p:Ld2/h0;

    invoke-virtual {v0, p0}, Ld2/h0;->m(Ld2/h0$f;)V

    iget-object v0, p0, Lj1/k0;->u:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iput-object v1, p0, Lj1/k0;->v:Lj1/u$a;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lj1/k0;->Q:Z

    return-void
.end method

.method public g(J)Z
    .locals 0

    iget-boolean p1, p0, Lj1/k0;->P:Z

    if-nez p1, :cond_2

    iget-object p1, p0, Lj1/k0;->p:Ld2/h0;

    invoke-virtual {p1}, Ld2/h0;->i()Z

    move-result p1

    if-nez p1, :cond_2

    iget-boolean p1, p0, Lj1/k0;->N:Z

    if-nez p1, :cond_2

    iget-boolean p1, p0, Lj1/k0;->A:Z

    if-eqz p1, :cond_0

    iget p1, p0, Lj1/k0;->J:I

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lj1/k0;->r:Le2/g;

    invoke-virtual {p1}, Le2/g;->e()Z

    move-result p1

    iget-object p2, p0, Lj1/k0;->p:Ld2/h0;

    invoke-virtual {p2}, Ld2/h0;->j()Z

    move-result p2

    if-nez p2, :cond_1

    invoke-direct {p0}, Lj1/k0;->j0()V

    const/4 p1, 0x1

    :cond_1
    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public h(J)V
    .locals 0

    return-void
.end method

.method public i()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lj1/k0;->z:Z

    iget-object v0, p0, Lj1/k0;->u:Landroid/os/Handler;

    iget-object v1, p0, Lj1/k0;->s:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method i0(IJ)I
    .locals 2

    invoke-direct {p0}, Lj1/k0;->k0()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-direct {p0, p1}, Lj1/k0;->V(I)V

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object v0, v0, p1

    iget-boolean v1, p0, Lj1/k0;->P:Z

    invoke-virtual {v0, p2, p3, v1}, Lj1/p0;->E(JZ)I

    move-result p2

    invoke-virtual {v0, p2}, Lj1/p0;->e0(I)V

    if-nez p2, :cond_1

    invoke-direct {p0, p1}, Lj1/k0;->W(I)V

    :cond_1
    return p2
.end method

.method public j()V
    .locals 4

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3}, Lj1/p0;->T()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lj1/k0;->q:Lj1/f0;

    invoke-interface {v0}, Lj1/f0;->a()V

    return-void
.end method

.method public k(Lm0/b0;)V
    .locals 2

    iget-object v0, p0, Lj1/k0;->u:Landroid/os/Handler;

    new-instance v1, Lj1/j0;

    invoke-direct {v1, p0, p1}, Lj1/j0;-><init>(Lj1/k0;Lm0/b0;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public bridge synthetic l(Ld2/h0$e;JJLjava/io/IOException;I)Ld2/h0$c;
    .locals 0

    check-cast p1, Lj1/k0$a;

    invoke-virtual/range {p0 .. p7}, Lj1/k0;->c0(Lj1/k0$a;JJLjava/io/IOException;I)Ld2/h0$c;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic m(Ld2/h0$e;JJZ)V
    .locals 0

    check-cast p1, Lj1/k0$a;

    invoke-virtual/range {p0 .. p6}, Lj1/k0;->a0(Lj1/k0$a;JJZ)V

    return-void
.end method

.method public n()J
    .locals 2

    iget-boolean v0, p0, Lj1/k0;->I:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lj1/k0;->P:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lj1/k0;->M()I

    move-result v0

    iget v1, p0, Lj1/k0;->O:I

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lj1/k0;->I:Z

    iget-wide v0, p0, Lj1/k0;->L:J

    return-wide v0

    :cond_1
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    return-wide v0
.end method

.method public o()Lj1/z0;
    .locals 1

    invoke-direct {p0}, Lj1/k0;->J()V

    iget-object v0, p0, Lj1/k0;->C:Lj1/k0$e;

    iget-object v0, v0, Lj1/k0$e;->a:Lj1/z0;

    return-object v0
.end method

.method public p(Lj1/u$a;J)V
    .locals 0

    iput-object p1, p0, Lj1/k0;->v:Lj1/u$a;

    iget-object p1, p0, Lj1/k0;->r:Le2/g;

    invoke-virtual {p1}, Le2/g;->e()Z

    invoke-direct {p0}, Lj1/k0;->j0()V

    return-void
.end method

.method public q()V
    .locals 2

    invoke-virtual {p0}, Lj1/k0;->X()V

    iget-boolean v0, p0, Lj1/k0;->P:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lj1/k0;->A:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const-string v1, "Loading finished before preparation is complete."

    invoke-static {v1, v0}, Lh0/y2;->a(Ljava/lang/String;Ljava/lang/Throwable;)Lh0/y2;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method public r(JZ)V
    .locals 5

    invoke-direct {p0}, Lj1/k0;->J()V

    invoke-direct {p0}, Lj1/k0;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lj1/k0;->C:Lj1/k0$e;

    iget-object v0, v0, Lj1/k0$e;->c:[Z

    iget-object v1, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v1, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v3, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object v3, v3, v2

    aget-boolean v4, v0, v2

    invoke-virtual {v3, p1, p2, p3, v4}, Lj1/p0;->q(JZZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public s(J)J
    .locals 4

    invoke-direct {p0}, Lj1/k0;->J()V

    iget-object v0, p0, Lj1/k0;->C:Lj1/k0$e;

    iget-object v0, v0, Lj1/k0$e;->b:[Z

    iget-object v1, p0, Lj1/k0;->D:Lm0/b0;

    invoke-interface {v1}, Lm0/b0;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 p1, 0x0

    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lj1/k0;->I:Z

    iput-wide p1, p0, Lj1/k0;->L:J

    invoke-direct {p0}, Lj1/k0;->P()Z

    move-result v2

    if-eqz v2, :cond_1

    iput-wide p1, p0, Lj1/k0;->M:J

    return-wide p1

    :cond_1
    iget v2, p0, Lj1/k0;->G:I

    const/4 v3, 0x7

    if-eq v2, v3, :cond_2

    invoke-direct {p0, v0, p1, p2}, Lj1/k0;->g0([ZJ)Z

    move-result v0

    if-eqz v0, :cond_2

    return-wide p1

    :cond_2
    iput-boolean v1, p0, Lj1/k0;->N:Z

    iput-wide p1, p0, Lj1/k0;->M:J

    iput-boolean v1, p0, Lj1/k0;->P:Z

    iget-object v0, p0, Lj1/k0;->p:Ld2/h0;

    invoke-virtual {v0}, Ld2/h0;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v2, v0

    :goto_1
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    invoke-virtual {v3}, Lj1/p0;->r()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lj1/k0;->p:Ld2/h0;

    invoke-virtual {v0}, Ld2/h0;->f()V

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lj1/k0;->p:Ld2/h0;

    invoke-virtual {v0}, Ld2/h0;->g()V

    iget-object v0, p0, Lj1/k0;->x:[Lj1/p0;

    array-length v2, v0

    :goto_2
    if-ge v1, v2, :cond_5

    aget-object v3, v0, v1

    invoke-virtual {v3}, Lj1/p0;->V()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    :goto_3
    return-wide p1
.end method

.method public bridge synthetic t(Ld2/h0$e;JJ)V
    .locals 0

    check-cast p1, Lj1/k0$a;

    invoke-virtual/range {p0 .. p5}, Lj1/k0;->b0(Lj1/k0$a;JJ)V

    return-void
.end method

.method public u([Lc2/t;[Z[Lj1/q0;[ZJ)J
    .locals 8

    invoke-direct {p0}, Lj1/k0;->J()V

    iget-object v0, p0, Lj1/k0;->C:Lj1/k0$e;

    iget-object v1, v0, Lj1/k0$e;->a:Lj1/z0;

    iget-object v0, v0, Lj1/k0$e;->c:[Z

    iget v2, p0, Lj1/k0;->J:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    array-length v5, p1

    const/4 v6, 0x1

    if-ge v4, v5, :cond_2

    aget-object v5, p3, v4

    if-eqz v5, :cond_1

    aget-object v5, p1, v4

    if-eqz v5, :cond_0

    aget-boolean v5, p2, v4

    if-nez v5, :cond_1

    :cond_0
    aget-object v5, p3, v4

    check-cast v5, Lj1/k0$c;

    invoke-static {v5}, Lj1/k0$c;->a(Lj1/k0$c;)I

    move-result v5

    aget-boolean v7, v0, v5

    invoke-static {v7}, Le2/a;->f(Z)V

    iget v7, p0, Lj1/k0;->J:I

    sub-int/2addr v7, v6

    iput v7, p0, Lj1/k0;->J:I

    aput-boolean v3, v0, v5

    const/4 v5, 0x0

    aput-object v5, p3, v4

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    iget-boolean p2, p0, Lj1/k0;->H:Z

    if-eqz p2, :cond_3

    if-nez v2, :cond_4

    goto :goto_1

    :cond_3
    const-wide/16 v4, 0x0

    cmp-long p2, p5, v4

    if-eqz p2, :cond_4

    :goto_1
    const/4 p2, 0x1

    goto :goto_2

    :cond_4
    const/4 p2, 0x0

    :goto_2
    const/4 v2, 0x0

    :goto_3
    array-length v4, p1

    if-ge v2, v4, :cond_9

    aget-object v4, p3, v2

    if-nez v4, :cond_8

    aget-object v4, p1, v2

    if-eqz v4, :cond_8

    aget-object v4, p1, v2

    invoke-interface {v4}, Lc2/w;->length()I

    move-result v5

    if-ne v5, v6, :cond_5

    const/4 v5, 0x1

    goto :goto_4

    :cond_5
    const/4 v5, 0x0

    :goto_4
    invoke-static {v5}, Le2/a;->f(Z)V

    invoke-interface {v4, v3}, Lc2/w;->b(I)I

    move-result v5

    if-nez v5, :cond_6

    const/4 v5, 0x1

    goto :goto_5

    :cond_6
    const/4 v5, 0x0

    :goto_5
    invoke-static {v5}, Le2/a;->f(Z)V

    invoke-interface {v4}, Lc2/w;->c()Lj1/x0;

    move-result-object v4

    invoke-virtual {v1, v4}, Lj1/z0;->c(Lj1/x0;)I

    move-result v4

    aget-boolean v5, v0, v4

    xor-int/2addr v5, v6

    invoke-static {v5}, Le2/a;->f(Z)V

    iget v5, p0, Lj1/k0;->J:I

    add-int/2addr v5, v6

    iput v5, p0, Lj1/k0;->J:I

    aput-boolean v6, v0, v4

    new-instance v5, Lj1/k0$c;

    invoke-direct {v5, p0, v4}, Lj1/k0$c;-><init>(Lj1/k0;I)V

    aput-object v5, p3, v2

    aput-boolean v6, p4, v2

    if-nez p2, :cond_8

    iget-object p2, p0, Lj1/k0;->x:[Lj1/p0;

    aget-object p2, p2, v4

    invoke-virtual {p2, p5, p6, v6}, Lj1/p0;->Z(JZ)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-virtual {p2}, Lj1/p0;->C()I

    move-result p2

    if-eqz p2, :cond_7

    const/4 p2, 0x1

    goto :goto_6

    :cond_7
    const/4 p2, 0x0

    :cond_8
    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_9
    iget p1, p0, Lj1/k0;->J:I

    if-nez p1, :cond_c

    iput-boolean v3, p0, Lj1/k0;->N:Z

    iput-boolean v3, p0, Lj1/k0;->I:Z

    iget-object p1, p0, Lj1/k0;->p:Ld2/h0;

    invoke-virtual {p1}, Ld2/h0;->j()Z

    move-result p1

    if-eqz p1, :cond_b

    iget-object p1, p0, Lj1/k0;->x:[Lj1/p0;

    array-length p2, p1

    :goto_7
    if-ge v3, p2, :cond_a

    aget-object p3, p1, v3

    invoke-virtual {p3}, Lj1/p0;->r()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_a
    iget-object p1, p0, Lj1/k0;->p:Ld2/h0;

    invoke-virtual {p1}, Ld2/h0;->f()V

    goto :goto_a

    :cond_b
    iget-object p1, p0, Lj1/k0;->x:[Lj1/p0;

    array-length p2, p1

    :goto_8
    if-ge v3, p2, :cond_e

    aget-object p3, p1, v3

    invoke-virtual {p3}, Lj1/p0;->V()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_c
    if-eqz p2, :cond_e

    invoke-virtual {p0, p5, p6}, Lj1/k0;->s(J)J

    move-result-wide p5

    :goto_9
    array-length p1, p3

    if-ge v3, p1, :cond_e

    aget-object p1, p3, v3

    if-eqz p1, :cond_d

    aput-boolean v6, p4, v3

    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_e
    :goto_a
    iput-boolean v6, p0, Lj1/k0;->H:Z

    return-wide p5
.end method
