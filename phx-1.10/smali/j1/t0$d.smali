.class final Lj1/t0$d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lj1/q0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj1/t0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "d"
.end annotation


# instance fields
.field private final f:J

.field private g:Z

.field private h:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1, p2}, Lj1/t0;->H(J)J

    move-result-wide p1

    iput-wide p1, p0, Lj1/t0$d;->f:J

    const-wide/16 p1, 0x0

    invoke-virtual {p0, p1, p2}, Lj1/t0$d;->a(J)V

    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 6

    invoke-static {p1, p2}, Lj1/t0;->H(J)J

    move-result-wide v0

    iget-wide v4, p0, Lj1/t0$d;->f:J

    const-wide/16 v2, 0x0

    invoke-static/range {v0 .. v5}, Le2/n0;->r(JJJ)J

    move-result-wide p1

    iput-wide p1, p0, Lj1/t0$d;->h:J

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public e(Lh0/s1;Lk0/g;I)I
    .locals 8

    iget-boolean v0, p0, Lj1/t0$d;->g:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-wide v2, p0, Lj1/t0$d;->f:J

    iget-wide v4, p0, Lj1/t0$d;->h:J

    sub-long/2addr v2, v4

    const-wide/16 v6, 0x0

    const/4 p1, -0x4

    cmp-long v0, v2, v6

    if-nez v0, :cond_1

    const/4 p3, 0x4

    invoke-virtual {p2, p3}, Lk0/a;->e(I)V

    return p1

    :cond_1
    invoke-static {v4, v5}, Lj1/t0;->I(J)J

    move-result-wide v4

    iput-wide v4, p2, Lk0/g;->j:J

    invoke-virtual {p2, v1}, Lk0/a;->e(I)V

    invoke-static {}, Lj1/t0;->J()[B

    move-result-object v0

    array-length v0, v0

    int-to-long v4, v0

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v0, v2

    and-int/lit8 v2, p3, 0x4

    if-nez v2, :cond_2

    invoke-virtual {p2, v0}, Lk0/g;->p(I)V

    iget-object p2, p2, Lk0/g;->h:Ljava/nio/ByteBuffer;

    invoke-static {}, Lj1/t0;->J()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    :cond_2
    and-int/lit8 p2, p3, 0x1

    if-nez p2, :cond_3

    iget-wide p2, p0, Lj1/t0$d;->h:J

    int-to-long v0, v0

    add-long/2addr p2, v0

    iput-wide p2, p0, Lj1/t0$d;->h:J

    :cond_3
    return p1

    :cond_4
    :goto_0
    invoke-static {}, Lj1/t0;->G()Lh0/r1;

    move-result-object p2

    iput-object p2, p1, Lh0/s1;->b:Lh0/r1;

    iput-boolean v1, p0, Lj1/t0$d;->g:Z

    const/4 p1, -0x5

    return p1
.end method

.method public i(J)I
    .locals 2

    iget-wide v0, p0, Lj1/t0$d;->h:J

    invoke-virtual {p0, p1, p2}, Lj1/t0$d;->a(J)V

    iget-wide p1, p0, Lj1/t0$d;->h:J

    sub-long/2addr p1, v0

    invoke-static {}, Lj1/t0;->J()[B

    move-result-object v0

    array-length v0, v0

    int-to-long v0, v0

    div-long/2addr p1, v0

    long-to-int p2, p1

    return p2
.end method

.method public k()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
