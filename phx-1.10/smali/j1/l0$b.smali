.class public final Lj1/l0$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lj1/x$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj1/l0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field private final a:Ld2/l$a;

.field private b:Lj1/f0$a;

.field private c:Ll0/b0;

.field private d:Ld2/g0;

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ld2/l$a;)V
    .locals 1

    new-instance v0, Lm0/i;

    invoke-direct {v0}, Lm0/i;-><init>()V

    invoke-direct {p0, p1, v0}, Lj1/l0$b;-><init>(Ld2/l$a;Lm0/r;)V

    return-void
.end method

.method public constructor <init>(Ld2/l$a;Lj1/f0$a;)V
    .locals 6

    new-instance v3, Ll0/l;

    invoke-direct {v3}, Ll0/l;-><init>()V

    new-instance v4, Ld2/x;

    invoke-direct {v4}, Ld2/x;-><init>()V

    const/high16 v5, 0x100000

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lj1/l0$b;-><init>(Ld2/l$a;Lj1/f0$a;Ll0/b0;Ld2/g0;I)V

    return-void
.end method

.method public constructor <init>(Ld2/l$a;Lj1/f0$a;Ll0/b0;Ld2/g0;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lj1/l0$b;->a:Ld2/l$a;

    iput-object p2, p0, Lj1/l0$b;->b:Lj1/f0$a;

    iput-object p3, p0, Lj1/l0$b;->c:Ll0/b0;

    iput-object p4, p0, Lj1/l0$b;->d:Ld2/g0;

    iput p5, p0, Lj1/l0$b;->e:I

    return-void
.end method

.method public constructor <init>(Ld2/l$a;Lm0/r;)V
    .locals 1

    new-instance v0, Lj1/m0;

    invoke-direct {v0, p2}, Lj1/m0;-><init>(Lm0/r;)V

    invoke-direct {p0, p1, v0}, Lj1/l0$b;-><init>(Ld2/l$a;Lj1/f0$a;)V

    return-void
.end method

.method public static synthetic a(Lm0/r;Li0/u1;)Lj1/f0;
    .locals 0

    invoke-static {p0, p1}, Lj1/l0$b;->c(Lm0/r;Li0/u1;)Lj1/f0;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic c(Lm0/r;Li0/u1;)Lj1/f0;
    .locals 0

    new-instance p1, Lj1/c;

    invoke-direct {p1, p0}, Lj1/c;-><init>(Lm0/r;)V

    return-object p1
.end method


# virtual methods
.method public b(Lh0/z1;)Lj1/l0;
    .locals 8

    iget-object v0, p1, Lh0/z1;->g:Lh0/z1$h;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lh0/z1;->g:Lh0/z1$h;

    iget-object v1, v0, Lh0/z1$h;->i:Ljava/lang/Object;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    iget-object v1, p0, Lj1/l0$b;->g:Ljava/lang/Object;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget-object v0, v0, Lh0/z1$h;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lj1/l0$b;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lh0/z1;->b()Lh0/z1$c;

    move-result-object p1

    iget-object v0, p0, Lj1/l0$b;->g:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Lh0/z1$c;->e(Ljava/lang/Object;)Lh0/z1$c;

    move-result-object p1

    :goto_2
    iget-object v0, p0, Lj1/l0$b;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lh0/z1$c;->b(Ljava/lang/String;)Lh0/z1$c;

    move-result-object p1

    goto :goto_3

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lh0/z1;->b()Lh0/z1$c;

    move-result-object p1

    iget-object v0, p0, Lj1/l0$b;->g:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Lh0/z1$c;->e(Ljava/lang/Object;)Lh0/z1$c;

    move-result-object p1

    :goto_3
    invoke-virtual {p1}, Lh0/z1$c;->a()Lh0/z1;

    move-result-object p1

    goto :goto_4

    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lh0/z1;->b()Lh0/z1$c;

    move-result-object p1

    goto :goto_2

    :cond_4
    :goto_4
    move-object v1, p1

    new-instance p1, Lj1/l0;

    iget-object v2, p0, Lj1/l0$b;->a:Ld2/l$a;

    iget-object v3, p0, Lj1/l0$b;->b:Lj1/f0$a;

    iget-object v0, p0, Lj1/l0$b;->c:Ll0/b0;

    invoke-interface {v0, v1}, Ll0/b0;->a(Lh0/z1;)Ll0/y;

    move-result-object v4

    iget-object v5, p0, Lj1/l0$b;->d:Ld2/g0;

    iget v6, p0, Lj1/l0$b;->e:I

    const/4 v7, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v7}, Lj1/l0;-><init>(Lh0/z1;Ld2/l$a;Lj1/f0$a;Ll0/y;Ld2/g0;ILj1/l0$a;)V

    return-object p1
.end method
