.class final Lj1/d$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lj1/q0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj1/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field public final f:Lj1/q0;

.field private g:Z

.field final synthetic h:Lj1/d;


# direct methods
.method public constructor <init>(Lj1/d;Lj1/q0;)V
    .locals 0

    iput-object p1, p0, Lj1/d$a;->h:Lj1/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lj1/d$a;->f:Lj1/q0;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lj1/d$a;->g:Z

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lj1/d$a;->f:Lj1/q0;

    invoke-interface {v0}, Lj1/q0;->b()V

    return-void
.end method

.method public e(Lh0/s1;Lk0/g;I)I
    .locals 10

    iget-object v0, p0, Lj1/d$a;->h:Lj1/d;

    invoke-virtual {v0}, Lj1/d;->l()Z

    move-result v0

    const/4 v1, -0x3

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-boolean v0, p0, Lj1/d$a;->g:Z

    const/4 v2, 0x4

    const/4 v3, -0x4

    if-eqz v0, :cond_1

    invoke-virtual {p2, v2}, Lk0/a;->n(I)V

    return v3

    :cond_1
    iget-object v0, p0, Lj1/d$a;->f:Lj1/q0;

    invoke-interface {v0, p1, p2, p3}, Lj1/q0;->e(Lh0/s1;Lk0/g;I)I

    move-result p3

    const/4 v0, -0x5

    const-wide/high16 v4, -0x8000000000000000L

    if-ne p3, v0, :cond_6

    iget-object p2, p1, Lh0/s1;->b:Lh0/r1;

    invoke-static {p2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lh0/r1;

    iget p3, p2, Lh0/r1;->G:I

    if-nez p3, :cond_2

    iget v1, p2, Lh0/r1;->H:I

    if-eqz v1, :cond_5

    :cond_2
    iget-object v1, p0, Lj1/d$a;->h:Lj1/d;

    iget-wide v2, v1, Lj1/d;->j:J

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    cmp-long v9, v2, v6

    if-eqz v9, :cond_3

    const/4 p3, 0x0

    :cond_3
    iget-wide v1, v1, Lj1/d;->k:J

    cmp-long v3, v1, v4

    if-eqz v3, :cond_4

    goto :goto_0

    :cond_4
    iget v8, p2, Lh0/r1;->H:I

    :goto_0
    invoke-virtual {p2}, Lh0/r1;->b()Lh0/r1$b;

    move-result-object p2

    invoke-virtual {p2, p3}, Lh0/r1$b;->P(I)Lh0/r1$b;

    move-result-object p2

    invoke-virtual {p2, v8}, Lh0/r1$b;->Q(I)Lh0/r1$b;

    move-result-object p2

    invoke-virtual {p2}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object p2

    iput-object p2, p1, Lh0/s1;->b:Lh0/r1;

    :cond_5
    return v0

    :cond_6
    iget-object p1, p0, Lj1/d$a;->h:Lj1/d;

    iget-wide v6, p1, Lj1/d;->k:J

    cmp-long v0, v6, v4

    if-eqz v0, :cond_9

    if-ne p3, v3, :cond_7

    iget-wide v8, p2, Lk0/g;->j:J

    cmp-long v0, v8, v6

    if-gez v0, :cond_8

    :cond_7
    if-ne p3, v1, :cond_9

    invoke-virtual {p1}, Lj1/d;->f()J

    move-result-wide v0

    cmp-long p1, v0, v4

    if-nez p1, :cond_9

    iget-boolean p1, p2, Lk0/g;->i:Z

    if-nez p1, :cond_9

    :cond_8
    invoke-virtual {p2}, Lk0/g;->f()V

    invoke-virtual {p2, v2}, Lk0/a;->n(I)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lj1/d$a;->g:Z

    return v3

    :cond_9
    return p3
.end method

.method public i(J)I
    .locals 1

    iget-object v0, p0, Lj1/d$a;->h:Lj1/d;

    invoke-virtual {v0}, Lj1/d;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, -0x3

    return p1

    :cond_0
    iget-object v0, p0, Lj1/d$a;->f:Lj1/q0;

    invoke-interface {v0, p1, p2}, Lj1/q0;->i(J)I

    move-result p1

    return p1
.end method

.method public k()Z
    .locals 1

    iget-object v0, p0, Lj1/d$a;->h:Lj1/d;

    invoke-virtual {v0}, Lj1/d;->l()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lj1/d$a;->f:Lj1/q0;

    invoke-interface {v0}, Lj1/q0;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
