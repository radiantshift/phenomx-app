.class public final Lj1/e;
.super Lj1/b1;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj1/e$a;,
        Lj1/e$b;
    }
.end annotation


# instance fields
.field private A:J

.field private B:J

.field private final r:J

.field private final s:J

.field private final t:Z

.field private final u:Z

.field private final v:Z

.field private final w:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lj1/d;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Lh0/c4$d;

.field private y:Lj1/e$a;

.field private z:Lj1/e$b;


# direct methods
.method public constructor <init>(Lj1/x;JJ)V
    .locals 9

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-direct/range {v0 .. v8}, Lj1/e;-><init>(Lj1/x;JJZZZ)V

    return-void
.end method

.method public constructor <init>(Lj1/x;JJZZZ)V
    .locals 2

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/x;

    invoke-direct {p0, p1}, Lj1/b1;-><init>(Lj1/x;)V

    const-wide/16 v0, 0x0

    cmp-long p1, p2, v0

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Le2/a;->a(Z)V

    iput-wide p2, p0, Lj1/e;->r:J

    iput-wide p4, p0, Lj1/e;->s:J

    iput-boolean p6, p0, Lj1/e;->t:Z

    iput-boolean p7, p0, Lj1/e;->u:Z

    iput-boolean p8, p0, Lj1/e;->v:Z

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lj1/e;->w:Ljava/util/ArrayList;

    new-instance p1, Lh0/c4$d;

    invoke-direct {p1}, Lh0/c4$d;-><init>()V

    iput-object p1, p0, Lj1/e;->x:Lh0/c4$d;

    return-void
.end method

.method private Z(Lh0/c4;)V
    .locals 15

    move-object v1, p0

    iget-object v0, v1, Lj1/e;->x:Lh0/c4$d;

    const/4 v2, 0x0

    move-object/from16 v4, p1

    invoke-virtual {v4, v2, v0}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    iget-object v0, v1, Lj1/e;->x:Lh0/c4$d;

    invoke-virtual {v0}, Lh0/c4$d;->g()J

    move-result-wide v5

    iget-object v0, v1, Lj1/e;->y:Lj1/e$a;

    const-wide/high16 v7, -0x8000000000000000L

    if-eqz v0, :cond_2

    iget-object v0, v1, Lj1/e;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, v1, Lj1/e;->u:Z

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-wide v9, v1, Lj1/e;->A:J

    sub-long/2addr v9, v5

    iget-wide v11, v1, Lj1/e;->s:J

    cmp-long v0, v11, v7

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    iget-wide v7, v1, Lj1/e;->B:J

    sub-long/2addr v7, v5

    :goto_0
    move-wide v5, v9

    goto :goto_4

    :cond_2
    :goto_1
    iget-wide v9, v1, Lj1/e;->r:J

    iget-wide v11, v1, Lj1/e;->s:J

    iget-boolean v0, v1, Lj1/e;->v:Z

    if-eqz v0, :cond_3

    iget-object v0, v1, Lj1/e;->x:Lh0/c4$d;

    invoke-virtual {v0}, Lh0/c4$d;->e()J

    move-result-wide v13

    add-long/2addr v9, v13

    add-long/2addr v11, v13

    :cond_3
    add-long v13, v5, v9

    iput-wide v13, v1, Lj1/e;->A:J

    iget-wide v13, v1, Lj1/e;->s:J

    cmp-long v0, v13, v7

    if-nez v0, :cond_4

    goto :goto_2

    :cond_4
    add-long v7, v5, v11

    :goto_2
    iput-wide v7, v1, Lj1/e;->B:J

    iget-object v0, v1, Lj1/e;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v0, :cond_5

    iget-object v5, v1, Lj1/e;->w:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lj1/d;

    iget-wide v6, v1, Lj1/e;->A:J

    iget-wide v13, v1, Lj1/e;->B:J

    invoke-virtual {v5, v6, v7, v13, v14}, Lj1/d;->w(JJ)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_5
    move-wide v5, v9

    move-wide v7, v11

    :goto_4
    :try_start_0
    new-instance v0, Lj1/e$a;

    move-object v3, v0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lj1/e$a;-><init>(Lh0/c4;JJ)V

    iput-object v0, v1, Lj1/e;->y:Lj1/e$a;
    :try_end_0
    .catch Lj1/e$b; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p0, v0}, Lj1/a;->D(Lh0/c4;)V

    return-void

    :catch_0
    move-exception v0

    iput-object v0, v1, Lj1/e;->z:Lj1/e$b;

    :goto_5
    iget-object v0, v1, Lj1/e;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    iget-object v0, v1, Lj1/e;->w:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/d;

    iget-object v3, v1, Lj1/e;->z:Lj1/e$b;

    invoke-virtual {v0, v3}, Lj1/d;->t(Lj1/e$b;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    return-void
.end method


# virtual methods
.method protected E()V
    .locals 1

    invoke-super {p0}, Lj1/g;->E()V

    const/4 v0, 0x0

    iput-object v0, p0, Lj1/e;->z:Lj1/e$b;

    iput-object v0, p0, Lj1/e;->y:Lj1/e$a;

    return-void
.end method

.method protected V(Lh0/c4;)V
    .locals 1

    iget-object v0, p0, Lj1/e;->z:Lj1/e$b;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lj1/e;->Z(Lh0/c4;)V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lj1/e;->z:Lj1/e$b;

    if-nez v0, :cond_0

    invoke-super {p0}, Lj1/g;->d()V

    return-void

    :cond_0
    throw v0
.end method

.method public i(Lj1/u;)V
    .locals 1

    iget-object v0, p0, Lj1/e;->w:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Le2/a;->f(Z)V

    iget-object v0, p0, Lj1/b1;->p:Lj1/x;

    check-cast p1, Lj1/d;

    iget-object p1, p1, Lj1/d;->f:Lj1/u;

    invoke-interface {v0, p1}, Lj1/x;->i(Lj1/u;)V

    iget-object p1, p0, Lj1/e;->w:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-boolean p1, p0, Lj1/e;->u:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lj1/e;->y:Lj1/e$a;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/e$a;

    iget-object p1, p1, Lj1/o;->k:Lh0/c4;

    invoke-direct {p0, p1}, Lj1/e;->Z(Lh0/c4;)V

    :cond_0
    return-void
.end method

.method public j(Lj1/x$b;Ld2/b;J)Lj1/u;
    .locals 8

    new-instance v7, Lj1/d;

    iget-object v0, p0, Lj1/b1;->p:Lj1/x;

    invoke-interface {v0, p1, p2, p3, p4}, Lj1/x;->j(Lj1/x$b;Ld2/b;J)Lj1/u;

    move-result-object v1

    iget-boolean v2, p0, Lj1/e;->t:Z

    iget-wide v3, p0, Lj1/e;->A:J

    iget-wide v5, p0, Lj1/e;->B:J

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lj1/d;-><init>(Lj1/u;ZJJ)V

    iget-object p1, p0, Lj1/e;->w:Ljava/util/ArrayList;

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v7
.end method
