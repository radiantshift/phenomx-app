.class final Lj1/k$b;
.super Lh0/a;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj1/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field private final n:I

.field private final o:I

.field private final p:[I

.field private final q:[I

.field private final r:[Lh0/c4;

.field private final s:[Ljava/lang/Object;

.field private final t:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;Lj1/s0;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lj1/k$e;",
            ">;",
            "Lj1/s0;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0, p3, p2}, Lh0/a;-><init>(ZLj1/s0;)V

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p2

    new-array p3, p2, [I

    iput-object p3, p0, Lj1/k$b;->p:[I

    new-array p3, p2, [I

    iput-object p3, p0, Lj1/k$b;->q:[I

    new-array p3, p2, [Lh0/c4;

    iput-object p3, p0, Lj1/k$b;->r:[Lh0/c4;

    new-array p2, p2, [Ljava/lang/Object;

    iput-object p2, p0, Lj1/k$b;->s:[Ljava/lang/Object;

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    iput-object p2, p0, Lj1/k$b;->t:Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/k$e;

    iget-object v2, p0, Lj1/k$b;->r:[Lh0/c4;

    iget-object v3, v1, Lj1/k$e;->a:Lj1/s;

    invoke-virtual {v3}, Lj1/s;->c0()Lh0/c4;

    move-result-object v3

    aput-object v3, v2, v0

    iget-object v2, p0, Lj1/k$b;->q:[I

    aput p2, v2, v0

    iget-object v2, p0, Lj1/k$b;->p:[I

    aput p3, v2, v0

    iget-object v2, p0, Lj1/k$b;->r:[Lh0/c4;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lh0/c4;->t()I

    move-result v2

    add-int/2addr p2, v2

    iget-object v2, p0, Lj1/k$b;->r:[Lh0/c4;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lh0/c4;->m()I

    move-result v2

    add-int/2addr p3, v2

    iget-object v2, p0, Lj1/k$b;->s:[Ljava/lang/Object;

    iget-object v1, v1, Lj1/k$e;->b:Ljava/lang/Object;

    aput-object v1, v2, v0

    iget-object v1, p0, Lj1/k$b;->t:Ljava/util/HashMap;

    aget-object v2, v2, v0

    add-int/lit8 v3, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v3

    goto :goto_0

    :cond_0
    iput p2, p0, Lj1/k$b;->n:I

    iput p3, p0, Lj1/k$b;->o:I

    return-void
.end method


# virtual methods
.method protected B(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lj1/k$b;->s:[Ljava/lang/Object;

    aget-object p1, v0, p1

    return-object p1
.end method

.method protected D(I)I
    .locals 1

    iget-object v0, p0, Lj1/k$b;->p:[I

    aget p1, v0, p1

    return p1
.end method

.method protected E(I)I
    .locals 1

    iget-object v0, p0, Lj1/k$b;->q:[I

    aget p1, v0, p1

    return p1
.end method

.method protected H(I)Lh0/c4;
    .locals 1

    iget-object v0, p0, Lj1/k$b;->r:[Lh0/c4;

    aget-object p1, v0, p1

    return-object p1
.end method

.method public m()I
    .locals 1

    iget v0, p0, Lj1/k$b;->o:I

    return v0
.end method

.method public t()I
    .locals 1

    iget v0, p0, Lj1/k$b;->n:I

    return v0
.end method

.method protected w(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lj1/k$b;->t:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-nez p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :goto_0
    return p1
.end method

.method protected x(I)I
    .locals 2

    iget-object v0, p0, Lj1/k$b;->p:[I

    add-int/lit8 p1, p1, 0x1

    const/4 v1, 0x0

    invoke-static {v0, p1, v1, v1}, Le2/n0;->h([IIZZ)I

    move-result p1

    return p1
.end method

.method protected y(I)I
    .locals 2

    iget-object v0, p0, Lj1/k$b;->q:[I

    add-int/lit8 p1, p1, 0x1

    const/4 v1, 0x0

    invoke-static {v0, p1, v1, v1}, Le2/n0;->h([IIZZ)I

    move-result p1

    return p1
.end method
