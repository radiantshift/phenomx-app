.class public Lj1/e0$a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj1/e0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj1/e0$a$a;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:Lj1/x$b;

.field private final c:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lj1/e0$a$a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:J


# direct methods
.method public constructor <init>()V
    .locals 6

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lj1/e0$a;-><init>(Ljava/util/concurrent/CopyOnWriteArrayList;ILj1/x$b;J)V

    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/CopyOnWriteArrayList;ILj1/x$b;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lj1/e0$a$a;",
            ">;I",
            "Lj1/x$b;",
            "J)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lj1/e0$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput p2, p0, Lj1/e0$a;->a:I

    iput-object p3, p0, Lj1/e0$a;->b:Lj1/x$b;

    iput-wide p4, p0, Lj1/e0$a;->d:J

    return-void
.end method

.method public static synthetic a(Lj1/e0$a;Lj1/e0;Lj1/x$b;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lj1/e0$a;->p(Lj1/e0;Lj1/x$b;Lj1/t;)V

    return-void
.end method

.method public static synthetic b(Lj1/e0$a;Lj1/e0;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lj1/e0$a;->n(Lj1/e0;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V

    return-void
.end method

.method public static synthetic c(Lj1/e0$a;Lj1/e0;Lj1/q;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lj1/e0$a;->m(Lj1/e0;Lj1/q;Lj1/t;)V

    return-void
.end method

.method public static synthetic d(Lj1/e0$a;Lj1/e0;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/e0$a;->k(Lj1/e0;Lj1/t;)V

    return-void
.end method

.method public static synthetic e(Lj1/e0$a;Lj1/e0;Lj1/q;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lj1/e0$a;->o(Lj1/e0;Lj1/q;Lj1/t;)V

    return-void
.end method

.method public static synthetic f(Lj1/e0$a;Lj1/e0;Lj1/q;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lj1/e0$a;->l(Lj1/e0;Lj1/q;Lj1/t;)V

    return-void
.end method

.method private h(J)J
    .locals 3

    invoke-static {p1, p2}, Le2/n0;->Z0(J)J

    move-result-wide p1

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    iget-wide v0, p0, Lj1/e0$a;->d:J

    add-long/2addr v0, p1

    :goto_0
    return-wide v0
.end method

.method private synthetic k(Lj1/e0;Lj1/t;)V
    .locals 2

    iget v0, p0, Lj1/e0$a;->a:I

    iget-object v1, p0, Lj1/e0$a;->b:Lj1/x$b;

    invoke-interface {p1, v0, v1, p2}, Lj1/e0;->I(ILj1/x$b;Lj1/t;)V

    return-void
.end method

.method private synthetic l(Lj1/e0;Lj1/q;Lj1/t;)V
    .locals 2

    iget v0, p0, Lj1/e0$a;->a:I

    iget-object v1, p0, Lj1/e0$a;->b:Lj1/x$b;

    invoke-interface {p1, v0, v1, p2, p3}, Lj1/e0;->H(ILj1/x$b;Lj1/q;Lj1/t;)V

    return-void
.end method

.method private synthetic m(Lj1/e0;Lj1/q;Lj1/t;)V
    .locals 2

    iget v0, p0, Lj1/e0$a;->a:I

    iget-object v1, p0, Lj1/e0$a;->b:Lj1/x$b;

    invoke-interface {p1, v0, v1, p2, p3}, Lj1/e0;->d0(ILj1/x$b;Lj1/q;Lj1/t;)V

    return-void
.end method

.method private synthetic n(Lj1/e0;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V
    .locals 7

    iget v1, p0, Lj1/e0$a;->a:I

    iget-object v2, p0, Lj1/e0$a;->b:Lj1/x$b;

    move-object v0, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Lj1/e0;->Q(ILj1/x$b;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V

    return-void
.end method

.method private synthetic o(Lj1/e0;Lj1/q;Lj1/t;)V
    .locals 2

    iget v0, p0, Lj1/e0$a;->a:I

    iget-object v1, p0, Lj1/e0$a;->b:Lj1/x$b;

    invoke-interface {p1, v0, v1, p2, p3}, Lj1/e0;->U(ILj1/x$b;Lj1/q;Lj1/t;)V

    return-void
.end method

.method private synthetic p(Lj1/e0;Lj1/x$b;Lj1/t;)V
    .locals 1

    iget v0, p0, Lj1/e0$a;->a:I

    invoke-interface {p1, v0, p2, p3}, Lj1/e0;->K(ILj1/x$b;Lj1/t;)V

    return-void
.end method


# virtual methods
.method public A(Lj1/q;IILh0/r1;ILjava/lang/Object;JJ)V
    .locals 12

    move-object v0, p0

    new-instance v11, Lj1/t;

    move-wide/from16 v1, p7

    invoke-direct {p0, v1, v2}, Lj1/e0$a;->h(J)J

    move-result-wide v7

    move-wide/from16 v1, p9

    invoke-direct {p0, v1, v2}, Lj1/e0$a;->h(J)J

    move-result-wide v9

    move-object v1, v11

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v1 .. v10}, Lj1/t;-><init>(IILh0/r1;ILjava/lang/Object;JJ)V

    move-object v1, p1

    invoke-virtual {p0, p1, v11}, Lj1/e0$a;->B(Lj1/q;Lj1/t;)V

    return-void
.end method

.method public B(Lj1/q;Lj1/t;)V
    .locals 4

    iget-object v0, p0, Lj1/e0$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/e0$a$a;

    iget-object v2, v1, Lj1/e0$a$a;->b:Lj1/e0;

    iget-object v1, v1, Lj1/e0$a$a;->a:Landroid/os/Handler;

    new-instance v3, Lj1/z;

    invoke-direct {v3, p0, v2, p1, p2}, Lj1/z;-><init>(Lj1/e0$a;Lj1/e0;Lj1/q;Lj1/t;)V

    invoke-static {v1, v3}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public C(Lj1/e0;)V
    .locals 3

    iget-object v0, p0, Lj1/e0$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/e0$a$a;

    iget-object v2, v1, Lj1/e0$a$a;->b:Lj1/e0;

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lj1/e0$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public D(IJJ)V
    .locals 12

    move-object v0, p0

    new-instance v11, Lj1/t;

    move-wide v1, p2

    invoke-direct {p0, p2, p3}, Lj1/e0$a;->h(J)J

    move-result-wide v7

    move-wide/from16 v1, p4

    invoke-direct {p0, v1, v2}, Lj1/e0$a;->h(J)J

    move-result-wide v9

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object v1, v11

    move v3, p1

    invoke-direct/range {v1 .. v10}, Lj1/t;-><init>(IILh0/r1;ILjava/lang/Object;JJ)V

    invoke-virtual {p0, v11}, Lj1/e0$a;->E(Lj1/t;)V

    return-void
.end method

.method public E(Lj1/t;)V
    .locals 5

    iget-object v0, p0, Lj1/e0$a;->b:Lj1/x$b;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/x$b;

    iget-object v1, p0, Lj1/e0$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lj1/e0$a$a;

    iget-object v3, v2, Lj1/e0$a$a;->b:Lj1/e0;

    iget-object v2, v2, Lj1/e0$a$a;->a:Landroid/os/Handler;

    new-instance v4, Lj1/d0;

    invoke-direct {v4, p0, v3, v0, p1}, Lj1/d0;-><init>(Lj1/e0$a;Lj1/e0;Lj1/x$b;Lj1/t;)V

    invoke-static {v2, v4}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public F(ILj1/x$b;J)Lj1/e0$a;
    .locals 7

    new-instance v6, Lj1/e0$a;

    iget-object v1, p0, Lj1/e0$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    move-object v0, v6

    move v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lj1/e0$a;-><init>(Ljava/util/concurrent/CopyOnWriteArrayList;ILj1/x$b;J)V

    return-object v6
.end method

.method public g(Landroid/os/Handler;Lj1/e0;)V
    .locals 2

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lj1/e0$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lj1/e0$a$a;

    invoke-direct {v1, p1, p2}, Lj1/e0$a$a;-><init>(Landroid/os/Handler;Lj1/e0;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public i(ILh0/r1;ILjava/lang/Object;J)V
    .locals 12

    move-object v0, p0

    new-instance v11, Lj1/t;

    move-wide/from16 v1, p5

    invoke-direct {p0, v1, v2}, Lj1/e0$a;->h(J)J

    move-result-wide v7

    const/4 v2, 0x1

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    move-object v1, v11

    move v3, p1

    move-object v4, p2

    move v5, p3

    move-object/from16 v6, p4

    invoke-direct/range {v1 .. v10}, Lj1/t;-><init>(IILh0/r1;ILjava/lang/Object;JJ)V

    invoke-virtual {p0, v11}, Lj1/e0$a;->j(Lj1/t;)V

    return-void
.end method

.method public j(Lj1/t;)V
    .locals 4

    iget-object v0, p0, Lj1/e0$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/e0$a$a;

    iget-object v2, v1, Lj1/e0$a$a;->b:Lj1/e0;

    iget-object v1, v1, Lj1/e0$a$a;->a:Landroid/os/Handler;

    new-instance v3, Lj1/c0;

    invoke-direct {v3, p0, v2, p1}, Lj1/c0;-><init>(Lj1/e0$a;Lj1/e0;Lj1/t;)V

    invoke-static {v1, v3}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public q(Lj1/q;I)V
    .locals 11

    const/4 v3, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v10}, Lj1/e0$a;->r(Lj1/q;IILh0/r1;ILjava/lang/Object;JJ)V

    return-void
.end method

.method public r(Lj1/q;IILh0/r1;ILjava/lang/Object;JJ)V
    .locals 12

    move-object v0, p0

    new-instance v11, Lj1/t;

    move-wide/from16 v1, p7

    invoke-direct {p0, v1, v2}, Lj1/e0$a;->h(J)J

    move-result-wide v7

    move-wide/from16 v1, p9

    invoke-direct {p0, v1, v2}, Lj1/e0$a;->h(J)J

    move-result-wide v9

    move-object v1, v11

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v1 .. v10}, Lj1/t;-><init>(IILh0/r1;ILjava/lang/Object;JJ)V

    move-object v1, p1

    invoke-virtual {p0, p1, v11}, Lj1/e0$a;->s(Lj1/q;Lj1/t;)V

    return-void
.end method

.method public s(Lj1/q;Lj1/t;)V
    .locals 4

    iget-object v0, p0, Lj1/e0$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/e0$a$a;

    iget-object v2, v1, Lj1/e0$a$a;->b:Lj1/e0;

    iget-object v1, v1, Lj1/e0$a$a;->a:Landroid/os/Handler;

    new-instance v3, Lj1/a0;

    invoke-direct {v3, p0, v2, p1, p2}, Lj1/a0;-><init>(Lj1/e0$a;Lj1/e0;Lj1/q;Lj1/t;)V

    invoke-static {v1, v3}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public t(Lj1/q;I)V
    .locals 11

    const/4 v3, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v10}, Lj1/e0$a;->u(Lj1/q;IILh0/r1;ILjava/lang/Object;JJ)V

    return-void
.end method

.method public u(Lj1/q;IILh0/r1;ILjava/lang/Object;JJ)V
    .locals 12

    move-object v0, p0

    new-instance v11, Lj1/t;

    move-wide/from16 v1, p7

    invoke-direct {p0, v1, v2}, Lj1/e0$a;->h(J)J

    move-result-wide v7

    move-wide/from16 v1, p9

    invoke-direct {p0, v1, v2}, Lj1/e0$a;->h(J)J

    move-result-wide v9

    move-object v1, v11

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v1 .. v10}, Lj1/t;-><init>(IILh0/r1;ILjava/lang/Object;JJ)V

    move-object v1, p1

    invoke-virtual {p0, p1, v11}, Lj1/e0$a;->v(Lj1/q;Lj1/t;)V

    return-void
.end method

.method public v(Lj1/q;Lj1/t;)V
    .locals 4

    iget-object v0, p0, Lj1/e0$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/e0$a$a;

    iget-object v2, v1, Lj1/e0$a$a;->b:Lj1/e0;

    iget-object v1, v1, Lj1/e0$a$a;->a:Landroid/os/Handler;

    new-instance v3, Lj1/y;

    invoke-direct {v3, p0, v2, p1, p2}, Lj1/y;-><init>(Lj1/e0$a;Lj1/e0;Lj1/q;Lj1/t;)V

    invoke-static {v1, v3}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public w(Lj1/q;IILh0/r1;ILjava/lang/Object;JJLjava/io/IOException;Z)V
    .locals 12

    move-object v0, p0

    new-instance v11, Lj1/t;

    move-wide/from16 v1, p7

    invoke-direct {p0, v1, v2}, Lj1/e0$a;->h(J)J

    move-result-wide v7

    move-wide/from16 v1, p9

    invoke-direct {p0, v1, v2}, Lj1/e0$a;->h(J)J

    move-result-wide v9

    move-object v1, v11

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v1 .. v10}, Lj1/t;-><init>(IILh0/r1;ILjava/lang/Object;JJ)V

    move-object v1, p1

    move-object/from16 v2, p11

    move/from16 v3, p12

    invoke-virtual {p0, p1, v11, v2, v3}, Lj1/e0$a;->y(Lj1/q;Lj1/t;Ljava/io/IOException;Z)V

    return-void
.end method

.method public x(Lj1/q;ILjava/io/IOException;Z)V
    .locals 13

    const/4 v3, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object/from16 v11, p3

    move/from16 v12, p4

    invoke-virtual/range {v0 .. v12}, Lj1/e0$a;->w(Lj1/q;IILh0/r1;ILjava/lang/Object;JJLjava/io/IOException;Z)V

    return-void
.end method

.method public y(Lj1/q;Lj1/t;Ljava/io/IOException;Z)V
    .locals 10

    iget-object v0, p0, Lj1/e0$a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/e0$a$a;

    iget-object v4, v1, Lj1/e0$a$a;->b:Lj1/e0;

    iget-object v1, v1, Lj1/e0$a$a;->a:Landroid/os/Handler;

    new-instance v9, Lj1/b0;

    move-object v2, v9

    move-object v3, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move v8, p4

    invoke-direct/range {v2 .. v8}, Lj1/b0;-><init>(Lj1/e0$a;Lj1/e0;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V

    invoke-static {v1, v9}, Le2/n0;->L0(Landroid/os/Handler;Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public z(Lj1/q;I)V
    .locals 11

    const/4 v3, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v10}, Lj1/e0$a;->A(Lj1/q;IILh0/r1;ILjava/lang/Object;JJ)V

    return-void
.end method
