.class public abstract Lj1/g;
.super Lj1/a;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj1/g$a;,
        Lj1/g$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lj1/a;"
    }
.end annotation


# instance fields
.field private final m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "TT;",
            "Lj1/g$b<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field private n:Landroid/os/Handler;

.field private o:Ld2/p0;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lj1/a;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lj1/g;->m:Ljava/util/HashMap;

    return-void
.end method

.method public static synthetic F(Lj1/g;Ljava/lang/Object;Lj1/x;Lh0/c4;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lj1/g;->L(Ljava/lang/Object;Lj1/x;Lh0/c4;)V

    return-void
.end method

.method private synthetic L(Ljava/lang/Object;Lj1/x;Lh0/c4;)V
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Lj1/g;->M(Ljava/lang/Object;Lj1/x;Lh0/c4;)V

    return-void
.end method


# virtual methods
.method protected C(Ld2/p0;)V
    .locals 0

    iput-object p1, p0, Lj1/g;->o:Ld2/p0;

    invoke-static {}, Le2/n0;->w()Landroid/os/Handler;

    move-result-object p1

    iput-object p1, p0, Lj1/g;->n:Landroid/os/Handler;

    return-void
.end method

.method protected E()V
    .locals 4

    iget-object v0, p0, Lj1/g;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/g$b;

    iget-object v2, v1, Lj1/g$b;->a:Lj1/x;

    iget-object v3, v1, Lj1/g$b;->b:Lj1/x$c;

    invoke-interface {v2, v3}, Lj1/x;->k(Lj1/x$c;)V

    iget-object v2, v1, Lj1/g$b;->a:Lj1/x;

    iget-object v3, v1, Lj1/g$b;->c:Lj1/g$a;

    invoke-interface {v2, v3}, Lj1/x;->p(Lj1/e0;)V

    iget-object v2, v1, Lj1/g$b;->a:Lj1/x;

    iget-object v1, v1, Lj1/g$b;->c:Lj1/g$a;

    invoke-interface {v2, v1}, Lj1/x;->o(Ll0/w;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lj1/g;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method protected final G(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lj1/g;->m:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/g$b;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/g$b;

    iget-object v0, p1, Lj1/g$b;->a:Lj1/x;

    iget-object p1, p1, Lj1/g$b;->b:Lj1/x$c;

    invoke-interface {v0, p1}, Lj1/x;->c(Lj1/x$c;)V

    return-void
.end method

.method protected final H(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lj1/g;->m:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/g$b;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/g$b;

    iget-object v0, p1, Lj1/g$b;->a:Lj1/x;

    iget-object p1, p1, Lj1/g$b;->b:Lj1/x$c;

    invoke-interface {v0, p1}, Lj1/x;->n(Lj1/x$c;)V

    return-void
.end method

.method protected abstract I(Ljava/lang/Object;Lj1/x$b;)Lj1/x$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lj1/x$b;",
            ")",
            "Lj1/x$b;"
        }
    .end annotation
.end method

.method protected J(Ljava/lang/Object;J)J
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;J)J"
        }
    .end annotation

    return-wide p2
.end method

.method protected abstract K(Ljava/lang/Object;I)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)I"
        }
    .end annotation
.end method

.method protected abstract M(Ljava/lang/Object;Lj1/x;Lh0/c4;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lj1/x;",
            "Lh0/c4;",
            ")V"
        }
    .end annotation
.end method

.method protected final N(Ljava/lang/Object;Lj1/x;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lj1/x;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lj1/g;->m:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Le2/a;->a(Z)V

    new-instance v0, Lj1/f;

    invoke-direct {v0, p0, p1}, Lj1/f;-><init>(Lj1/g;Ljava/lang/Object;)V

    new-instance v1, Lj1/g$a;

    invoke-direct {v1, p0, p1}, Lj1/g$a;-><init>(Lj1/g;Ljava/lang/Object;)V

    iget-object v2, p0, Lj1/g;->m:Ljava/util/HashMap;

    new-instance v3, Lj1/g$b;

    invoke-direct {v3, p2, v0, v1}, Lj1/g$b;-><init>(Lj1/x;Lj1/x$c;Lj1/g$a;)V

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lj1/g;->n:Landroid/os/Handler;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/Handler;

    invoke-interface {p2, p1, v1}, Lj1/x;->f(Landroid/os/Handler;Lj1/e0;)V

    iget-object p1, p0, Lj1/g;->n:Landroid/os/Handler;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/Handler;

    invoke-interface {p2, p1, v1}, Lj1/x;->b(Landroid/os/Handler;Ll0/w;)V

    iget-object p1, p0, Lj1/g;->o:Ld2/p0;

    invoke-virtual {p0}, Lj1/a;->A()Li0/u1;

    move-result-object v1

    invoke-interface {p2, v0, p1, v1}, Lj1/x;->q(Lj1/x$c;Ld2/p0;Li0/u1;)V

    invoke-virtual {p0}, Lj1/a;->B()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-interface {p2, v0}, Lj1/x;->c(Lj1/x$c;)V

    :cond_0
    return-void
.end method

.method protected final O(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lj1/g;->m:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/g$b;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/g$b;

    iget-object v0, p1, Lj1/g$b;->a:Lj1/x;

    iget-object v1, p1, Lj1/g$b;->b:Lj1/x$c;

    invoke-interface {v0, v1}, Lj1/x;->k(Lj1/x$c;)V

    iget-object v0, p1, Lj1/g$b;->a:Lj1/x;

    iget-object v1, p1, Lj1/g$b;->c:Lj1/g$a;

    invoke-interface {v0, v1}, Lj1/x;->p(Lj1/e0;)V

    iget-object v0, p1, Lj1/g$b;->a:Lj1/x;

    iget-object p1, p1, Lj1/g$b;->c:Lj1/g$a;

    invoke-interface {v0, p1}, Lj1/x;->o(Ll0/w;)V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lj1/g;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/g$b;

    iget-object v1, v1, Lj1/g$b;->a:Lj1/x;

    invoke-interface {v1}, Lj1/x;->d()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected y()V
    .locals 3

    iget-object v0, p0, Lj1/g;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/g$b;

    iget-object v2, v1, Lj1/g$b;->a:Lj1/x;

    iget-object v1, v1, Lj1/g$b;->b:Lj1/x$c;

    invoke-interface {v2, v1}, Lj1/x;->c(Lj1/x$c;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected z()V
    .locals 3

    iget-object v0, p0, Lj1/g;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/g$b;

    iget-object v2, v1, Lj1/g$b;->a:Lj1/x;

    iget-object v1, v1, Lj1/g$b;->b:Lj1/x$c;

    invoke-interface {v2, v1}, Lj1/x;->n(Lj1/x$c;)V

    goto :goto_0

    :cond_0
    return-void
.end method
