.class final Lj1/s$a;
.super Lj1/o;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj1/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# static fields
.field public static final n:Ljava/lang/Object;


# instance fields
.field private final l:Ljava/lang/Object;

.field private final m:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lj1/s$a;->n:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Lh0/c4;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/o;-><init>(Lh0/c4;)V

    iput-object p2, p0, Lj1/s$a;->l:Ljava/lang/Object;

    iput-object p3, p0, Lj1/s$a;->m:Ljava/lang/Object;

    return-void
.end method

.method static synthetic w(Lj1/s$a;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lj1/s$a;->m:Ljava/lang/Object;

    return-object p0
.end method

.method public static y(Lh0/z1;)Lj1/s$a;
    .locals 3

    new-instance v0, Lj1/s$a;

    new-instance v1, Lj1/s$b;

    invoke-direct {v1, p0}, Lj1/s$b;-><init>(Lh0/z1;)V

    sget-object p0, Lh0/c4$d;->w:Ljava/lang/Object;

    sget-object v2, Lj1/s$a;->n:Ljava/lang/Object;

    invoke-direct {v0, v1, p0, v2}, Lj1/s$a;-><init>(Lh0/c4;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static z(Lh0/c4;Ljava/lang/Object;Ljava/lang/Object;)Lj1/s$a;
    .locals 1

    new-instance v0, Lj1/s$a;

    invoke-direct {v0, p0, p1, p2}, Lj1/s$a;-><init>(Lh0/c4;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public f(Ljava/lang/Object;)I
    .locals 2

    iget-object v0, p0, Lj1/o;->k:Lh0/c4;

    sget-object v1, Lj1/s$a;->n:Ljava/lang/Object;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lj1/s$a;->m:Ljava/lang/Object;

    if-eqz v1, :cond_0

    move-object p1, v1

    :cond_0
    invoke-virtual {v0, p1}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public k(ILh0/c4$b;Z)Lh0/c4$b;
    .locals 1

    iget-object v0, p0, Lj1/o;->k:Lh0/c4;

    invoke-virtual {v0, p1, p2, p3}, Lh0/c4;->k(ILh0/c4$b;Z)Lh0/c4$b;

    iget-object p1, p2, Lh0/c4$b;->g:Ljava/lang/Object;

    iget-object v0, p0, Lj1/s$a;->m:Ljava/lang/Object;

    invoke-static {p1, v0}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    sget-object p1, Lj1/s$a;->n:Ljava/lang/Object;

    iput-object p1, p2, Lh0/c4$b;->g:Ljava/lang/Object;

    :cond_0
    return-object p2
.end method

.method public q(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lj1/o;->k:Lh0/c4;

    invoke-virtual {v0, p1}, Lh0/c4;->q(I)Ljava/lang/Object;

    move-result-object p1

    iget-object v0, p0, Lj1/s$a;->m:Ljava/lang/Object;

    invoke-static {p1, v0}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lj1/s$a;->n:Ljava/lang/Object;

    :cond_0
    return-object p1
.end method

.method public s(ILh0/c4$d;J)Lh0/c4$d;
    .locals 1

    iget-object v0, p0, Lj1/o;->k:Lh0/c4;

    invoke-virtual {v0, p1, p2, p3, p4}, Lh0/c4;->s(ILh0/c4$d;J)Lh0/c4$d;

    iget-object p1, p2, Lh0/c4$d;->f:Ljava/lang/Object;

    iget-object p3, p0, Lj1/s$a;->l:Ljava/lang/Object;

    invoke-static {p1, p3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lh0/c4$d;->w:Ljava/lang/Object;

    iput-object p1, p2, Lh0/c4$d;->f:Ljava/lang/Object;

    :cond_0
    return-object p2
.end method

.method public x(Lh0/c4;)Lj1/s$a;
    .locals 3

    new-instance v0, Lj1/s$a;

    iget-object v1, p0, Lj1/s$a;->l:Ljava/lang/Object;

    iget-object v2, p0, Lj1/s$a;->m:Ljava/lang/Object;

    invoke-direct {v0, p1, v1, v2}, Lj1/s$a;-><init>(Lh0/c4;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
