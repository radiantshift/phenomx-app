.class public abstract Lj1/b1;
.super Lj1/g;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lj1/g<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final q:Ljava/lang/Void;


# instance fields
.field protected final p:Lj1/x;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method protected constructor <init>(Lj1/x;)V
    .locals 0

    invoke-direct {p0}, Lj1/g;-><init>()V

    iput-object p1, p0, Lj1/b1;->p:Lj1/x;

    return-void
.end method


# virtual methods
.method protected final C(Ld2/p0;)V
    .locals 0

    invoke-super {p0, p1}, Lj1/g;->C(Ld2/p0;)V

    invoke-virtual {p0}, Lj1/b1;->Y()V

    return-void
.end method

.method protected bridge synthetic I(Ljava/lang/Object;Lj1/x$b;)Lj1/x$b;
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lj1/b1;->Q(Ljava/lang/Void;Lj1/x$b;)Lj1/x$b;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic J(Ljava/lang/Object;J)J
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2, p3}, Lj1/b1;->S(Ljava/lang/Void;J)J

    move-result-wide p1

    return-wide p1
.end method

.method protected bridge synthetic K(Ljava/lang/Object;I)I
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lj1/b1;->U(Ljava/lang/Void;I)I

    move-result p1

    return p1
.end method

.method protected bridge synthetic M(Ljava/lang/Object;Lj1/x;Lh0/c4;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2, p3}, Lj1/b1;->W(Ljava/lang/Void;Lj1/x;Lh0/c4;)V

    return-void
.end method

.method protected P(Lj1/x$b;)Lj1/x$b;
    .locals 0

    return-object p1
.end method

.method protected final Q(Ljava/lang/Void;Lj1/x$b;)Lj1/x$b;
    .locals 0

    invoke-virtual {p0, p2}, Lj1/b1;->P(Lj1/x$b;)Lj1/x$b;

    move-result-object p1

    return-object p1
.end method

.method protected R(J)J
    .locals 0

    return-wide p1
.end method

.method protected final S(Ljava/lang/Void;J)J
    .locals 0

    invoke-virtual {p0, p2, p3}, Lj1/b1;->R(J)J

    move-result-wide p1

    return-wide p1
.end method

.method protected T(I)I
    .locals 0

    return p1
.end method

.method protected final U(Ljava/lang/Void;I)I
    .locals 0

    invoke-virtual {p0, p2}, Lj1/b1;->T(I)I

    move-result p1

    return p1
.end method

.method protected abstract V(Lh0/c4;)V
.end method

.method protected final W(Ljava/lang/Void;Lj1/x;Lh0/c4;)V
    .locals 0

    invoke-virtual {p0, p3}, Lj1/b1;->V(Lh0/c4;)V

    return-void
.end method

.method protected final X()V
    .locals 2

    sget-object v0, Lj1/b1;->q:Ljava/lang/Void;

    iget-object v1, p0, Lj1/b1;->p:Lj1/x;

    invoke-virtual {p0, v0, v1}, Lj1/g;->N(Ljava/lang/Object;Lj1/x;)V

    return-void
.end method

.method protected Y()V
    .locals 0

    invoke-virtual {p0}, Lj1/b1;->X()V

    return-void
.end method

.method public a()Lh0/z1;
    .locals 1

    iget-object v0, p0, Lj1/b1;->p:Lj1/x;

    invoke-interface {v0}, Lj1/x;->a()Lh0/z1;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lj1/b1;->p:Lj1/x;

    invoke-interface {v0}, Lj1/x;->e()Z

    move-result v0

    return v0
.end method

.method public g()Lh0/c4;
    .locals 1

    iget-object v0, p0, Lj1/b1;->p:Lj1/x;

    invoke-interface {v0}, Lj1/x;->g()Lh0/c4;

    move-result-object v0

    return-object v0
.end method
