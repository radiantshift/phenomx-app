.class public final Lj1/k;
.super Lj1/g;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj1/k$d;,
        Lj1/k$c;,
        Lj1/k$b;,
        Lj1/k$f;,
        Lj1/k$e;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lj1/g<",
        "Lj1/k$e;",
        ">;"
    }
.end annotation


# static fields
.field private static final B:Lh0/z1;


# instance fields
.field private A:Lj1/s0;

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lj1/k$e;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lj1/k$d;",
            ">;"
        }
    .end annotation
.end field

.field private r:Landroid/os/Handler;

.field private final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lj1/k$e;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/util/IdentityHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/IdentityHashMap<",
            "Lj1/u;",
            "Lj1/k$e;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Lj1/k$e;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lj1/k$e;",
            ">;"
        }
    .end annotation
.end field

.field private final w:Z

.field private final x:Z

.field private y:Z

.field private z:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lj1/k$d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lh0/z1$c;

    invoke-direct {v0}, Lh0/z1$c;-><init>()V

    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lh0/z1$c;->f(Landroid/net/Uri;)Lh0/z1$c;

    move-result-object v0

    invoke-virtual {v0}, Lh0/z1$c;->a()Lh0/z1;

    move-result-object v0

    sput-object v0, Lj1/k;->B:Lh0/z1;

    return-void
.end method

.method public varargs constructor <init>(ZLj1/s0;[Lj1/x;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lj1/k;-><init>(ZZLj1/s0;[Lj1/x;)V

    return-void
.end method

.method public varargs constructor <init>(ZZLj1/s0;[Lj1/x;)V
    .locals 3

    invoke-direct {p0}, Lj1/g;-><init>()V

    array-length v0, p4

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p4, v1

    invoke-static {v2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {p3}, Lj1/s0;->a()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {p3}, Lj1/s0;->h()Lj1/s0;

    move-result-object p3

    :cond_1
    iput-object p3, p0, Lj1/k;->A:Lj1/s0;

    new-instance p3, Ljava/util/IdentityHashMap;

    invoke-direct {p3}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object p3, p0, Lj1/k;->t:Ljava/util/IdentityHashMap;

    new-instance p3, Ljava/util/HashMap;

    invoke-direct {p3}, Ljava/util/HashMap;-><init>()V

    iput-object p3, p0, Lj1/k;->u:Ljava/util/Map;

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    iput-object p3, p0, Lj1/k;->p:Ljava/util/List;

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    iput-object p3, p0, Lj1/k;->s:Ljava/util/List;

    new-instance p3, Ljava/util/HashSet;

    invoke-direct {p3}, Ljava/util/HashSet;-><init>()V

    iput-object p3, p0, Lj1/k;->z:Ljava/util/Set;

    new-instance p3, Ljava/util/HashSet;

    invoke-direct {p3}, Ljava/util/HashSet;-><init>()V

    iput-object p3, p0, Lj1/k;->q:Ljava/util/Set;

    new-instance p3, Ljava/util/HashSet;

    invoke-direct {p3}, Ljava/util/HashSet;-><init>()V

    iput-object p3, p0, Lj1/k;->v:Ljava/util/Set;

    iput-boolean p1, p0, Lj1/k;->w:Z

    iput-boolean p2, p0, Lj1/k;->x:Z

    invoke-static {p4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lj1/k;->T(Ljava/util/Collection;)V

    return-void
.end method

.method public varargs constructor <init>(Z[Lj1/x;)V
    .locals 2

    new-instance v0, Lj1/s0$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lj1/s0$a;-><init>(I)V

    invoke-direct {p0, p1, v0, p2}, Lj1/k;-><init>(ZLj1/s0;[Lj1/x;)V

    return-void
.end method

.method public varargs constructor <init>([Lj1/x;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lj1/k;-><init>(Z[Lj1/x;)V

    return-void
.end method

.method public static synthetic P(Lj1/k;Landroid/os/Message;)Z
    .locals 0

    invoke-direct {p0, p1}, Lj1/k;->i0(Landroid/os/Message;)Z

    move-result p0

    return p0
.end method

.method static synthetic Q()Lh0/z1;
    .locals 1

    sget-object v0, Lj1/k;->B:Lh0/z1;

    return-object v0
.end method

.method private R(ILj1/k$e;)V
    .locals 2

    if-lez p1, :cond_0

    iget-object v0, p0, Lj1/k;->s:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/k$e;

    iget-object v1, v0, Lj1/k$e;->a:Lj1/s;

    invoke-virtual {v1}, Lj1/s;->c0()Lh0/c4;

    move-result-object v1

    iget v0, v0, Lj1/k$e;->e:I

    invoke-virtual {v1}, Lh0/c4;->t()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2, p1, v0}, Lj1/k$e;->a(II)V

    iget-object v0, p2, Lj1/k$e;->a:Lj1/s;

    invoke-virtual {v0}, Lj1/s;->c0()Lh0/c4;

    move-result-object v0

    invoke-virtual {v0}, Lh0/c4;->t()I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1, v0}, Lj1/k;->W(III)V

    iget-object v0, p0, Lj1/k;->s:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object p1, p0, Lj1/k;->u:Ljava/util/Map;

    iget-object v0, p2, Lj1/k$e;->b:Ljava/lang/Object;

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p2, Lj1/k$e;->a:Lj1/s;

    invoke-virtual {p0, p2, p1}, Lj1/g;->N(Ljava/lang/Object;Lj1/x;)V

    invoke-virtual {p0}, Lj1/a;->B()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lj1/k;->t:Ljava/util/IdentityHashMap;

    invoke-virtual {p1}, Ljava/util/IdentityHashMap;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lj1/k;->v:Ljava/util/Set;

    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p2}, Lj1/g;->G(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method private U(ILjava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection<",
            "Lj1/k$e;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/k$e;

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, p1, v0}, Lj1/k;->R(ILj1/k$e;)V

    move p1, v1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private V(ILjava/util/Collection;Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection<",
            "Lj1/x;",
            ">;",
            "Landroid/os/Handler;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez p4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-ne v2, v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    invoke-static {v0}, Le2/a;->a(Z)V

    iget-object v0, p0, Lj1/k;->r:Landroid/os/Handler;

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lj1/x;

    invoke-static {v3}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lj1/x;

    new-instance v5, Lj1/k$e;

    iget-boolean v6, p0, Lj1/k;->x:Z

    invoke-direct {v5, v4, v6}, Lj1/k$e;-><init>(Lj1/x;Z)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_4
    iget-object v3, p0, Lj1/k;->p:Ljava/util/List;

    invoke-interface {v3, p1, v2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    if-eqz v0, :cond_5

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_5

    invoke-direct {p0, p3, p4}, Lj1/k;->X(Landroid/os/Handler;Ljava/lang/Runnable;)Lj1/k$d;

    move-result-object p2

    new-instance p3, Lj1/k$f;

    invoke-direct {p3, p1, v2, p2}, Lj1/k$f;-><init>(ILjava/lang/Object;Lj1/k$d;)V

    invoke-virtual {v0, v1, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_5

    :cond_5
    if-eqz p4, :cond_6

    if-eqz p3, :cond_6

    invoke-virtual {p3, p4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_6
    :goto_5
    return-void
.end method

.method private W(III)V
    .locals 2

    :goto_0
    iget-object v0, p0, Lj1/k;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lj1/k;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/k$e;

    iget v1, v0, Lj1/k$e;->d:I

    add-int/2addr v1, p2

    iput v1, v0, Lj1/k$e;->d:I

    iget v1, v0, Lj1/k$e;->e:I

    add-int/2addr v1, p3

    iput v1, v0, Lj1/k$e;->e:I

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private X(Landroid/os/Handler;Ljava/lang/Runnable;)Lj1/k$d;
    .locals 1

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lj1/k$d;

    invoke-direct {v0, p1, p2}, Lj1/k$d;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iget-object p1, p0, Lj1/k;->q:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object v0

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private Y()V
    .locals 3

    iget-object v0, p0, Lj1/k;->v:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/k$e;

    iget-object v2, v1, Lj1/k$e;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Lj1/g;->G(Ljava/lang/Object;)V

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private declared-synchronized Z(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lj1/k$d;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/k$d;

    invoke-virtual {v1}, Lj1/k$d;->a()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lj1/k;->q:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private a0(Lj1/k$e;)V
    .locals 1

    iget-object v0, p0, Lj1/k;->v:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1}, Lj1/g;->H(Ljava/lang/Object;)V

    return-void
.end method

.method private static b0(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0}, Lh0/a;->z(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private static d0(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0}, Lh0/a;->A(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private static e0(Lj1/k$e;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lj1/k$e;->b:Ljava/lang/Object;

    invoke-static {p0, p1}, Lh0/a;->C(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private f0()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lj1/k;->r:Landroid/os/Handler;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    return-object v0
.end method

.method private i0(Landroid/os/Message;)Z
    .locals 4

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eqz v0, :cond_6

    if-eq v0, v1, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    const/4 v2, 0x5

    if-ne v0, v2, :cond_0

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {p1}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    invoke-direct {p0, p1}, Lj1/k;->Z(Ljava/util/Set;)V

    goto/16 :goto_3

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_1
    invoke-direct {p0}, Lj1/k;->w0()V

    goto/16 :goto_3

    :cond_2
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {p1}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/k$f;

    iget-object v0, p1, Lj1/k$f;->b:Ljava/lang/Object;

    check-cast v0, Lj1/s0;

    iput-object v0, p0, Lj1/k;->A:Lj1/s0;

    goto/16 :goto_2

    :cond_3
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {p1}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/k$f;

    iget-object v0, p0, Lj1/k;->A:Lj1/s0;

    iget v2, p1, Lj1/k$f;->a:I

    add-int/lit8 v3, v2, 0x1

    invoke-interface {v0, v2, v3}, Lj1/s0;->b(II)Lj1/s0;

    move-result-object v0

    iput-object v0, p0, Lj1/k;->A:Lj1/s0;

    iget-object v2, p1, Lj1/k$f;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2, v1}, Lj1/s0;->d(II)Lj1/s0;

    move-result-object v0

    iput-object v0, p0, Lj1/k;->A:Lj1/s0;

    iget v0, p1, Lj1/k$f;->a:I

    iget-object v2, p1, Lj1/k$f;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v2}, Lj1/k;->l0(II)V

    goto :goto_2

    :cond_4
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {p1}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/k$f;

    iget v0, p1, Lj1/k$f;->a:I

    iget-object v2, p1, Lj1/k$f;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v0, :cond_5

    iget-object v3, p0, Lj1/k;->A:Lj1/s0;

    invoke-interface {v3}, Lj1/s0;->a()I

    move-result v3

    if-ne v2, v3, :cond_5

    iget-object v3, p0, Lj1/k;->A:Lj1/s0;

    invoke-interface {v3}, Lj1/s0;->h()Lj1/s0;

    move-result-object v3

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lj1/k;->A:Lj1/s0;

    invoke-interface {v3, v0, v2}, Lj1/s0;->b(II)Lj1/s0;

    move-result-object v3

    :goto_0
    iput-object v3, p0, Lj1/k;->A:Lj1/s0;

    sub-int/2addr v2, v1

    :goto_1
    if-lt v2, v0, :cond_7

    invoke-direct {p0, v2}, Lj1/k;->o0(I)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_6
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {p1}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/k$f;

    iget-object v0, p0, Lj1/k;->A:Lj1/s0;

    iget v2, p1, Lj1/k$f;->a:I

    iget-object v3, p1, Lj1/k$f;->b:Ljava/lang/Object;

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-interface {v0, v2, v3}, Lj1/s0;->d(II)Lj1/s0;

    move-result-object v0

    iput-object v0, p0, Lj1/k;->A:Lj1/s0;

    iget v0, p1, Lj1/k$f;->a:I

    iget-object v2, p1, Lj1/k$f;->b:Ljava/lang/Object;

    check-cast v2, Ljava/util/Collection;

    invoke-direct {p0, v0, v2}, Lj1/k;->U(ILjava/util/Collection;)V

    :cond_7
    :goto_2
    iget-object p1, p1, Lj1/k$f;->c:Lj1/k$d;

    invoke-direct {p0, p1}, Lj1/k;->s0(Lj1/k$d;)V

    :goto_3
    return v1
.end method

.method private j0(Lj1/k$e;)V
    .locals 1

    iget-boolean v0, p1, Lj1/k$e;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lj1/k$e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lj1/k;->v:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1}, Lj1/g;->O(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private l0(II)V
    .locals 4

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lj1/k;->s:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lj1/k$e;

    iget v2, v2, Lj1/k$e;->e:I

    iget-object v3, p0, Lj1/k;->s:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/k$e;

    invoke-interface {v3, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_0
    if-gt v0, v1, :cond_0

    iget-object p1, p0, Lj1/k;->s:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/k$e;

    iput v0, p1, Lj1/k$e;->d:I

    iput v2, p1, Lj1/k$e;->e:I

    iget-object p1, p1, Lj1/k$e;->a:Lj1/s;

    invoke-virtual {p1}, Lj1/s;->c0()Lh0/c4;

    move-result-object p1

    invoke-virtual {p1}, Lh0/c4;->t()I

    move-result p1

    add-int/2addr v2, p1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private m0(IILandroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez p4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-ne v2, v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    invoke-static {v0}, Le2/a;->a(Z)V

    iget-object v0, p0, Lj1/k;->r:Landroid/os/Handler;

    iget-object v1, p0, Lj1/k;->p:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lj1/k$e;

    invoke-interface {v1, p2, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    if-eqz v0, :cond_3

    invoke-direct {p0, p3, p4}, Lj1/k;->X(Landroid/os/Handler;Ljava/lang/Runnable;)Lj1/k$d;

    move-result-object p3

    const/4 p4, 0x2

    new-instance v1, Lj1/k$f;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-direct {v1, p1, p2, p3}, Lj1/k$f;-><init>(ILjava/lang/Object;Lj1/k$d;)V

    invoke-virtual {v0, p4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_3

    :cond_3
    if-eqz p4, :cond_4

    if-eqz p3, :cond_4

    invoke-virtual {p3, p4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_4
    :goto_3
    return-void
.end method

.method private o0(I)V
    .locals 3

    iget-object v0, p0, Lj1/k;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/k$e;

    iget-object v1, p0, Lj1/k;->u:Ljava/util/Map;

    iget-object v2, v0, Lj1/k$e;->b:Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lj1/k$e;->a:Lj1/s;

    invoke-virtual {v1}, Lj1/s;->c0()Lh0/c4;

    move-result-object v1

    invoke-virtual {v1}, Lh0/c4;->t()I

    move-result v1

    neg-int v1, v1

    const/4 v2, -0x1

    invoke-direct {p0, p1, v2, v1}, Lj1/k;->W(III)V

    const/4 p1, 0x1

    iput-boolean p1, v0, Lj1/k$e;->f:Z

    invoke-direct {p0, v0}, Lj1/k;->j0(Lj1/k$e;)V

    return-void
.end method

.method private q0(IILandroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez p4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-ne v2, v3, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-static {v0}, Le2/a;->a(Z)V

    iget-object v0, p0, Lj1/k;->r:Landroid/os/Handler;

    iget-object v2, p0, Lj1/k;->p:Ljava/util/List;

    invoke-static {v2, p1, p2}, Le2/n0;->N0(Ljava/util/List;II)V

    if-eqz v0, :cond_3

    invoke-direct {p0, p3, p4}, Lj1/k;->X(Landroid/os/Handler;Ljava/lang/Runnable;)Lj1/k$d;

    move-result-object p3

    new-instance p4, Lj1/k$f;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-direct {p4, p1, p2, p3}, Lj1/k$f;-><init>(ILjava/lang/Object;Lj1/k$d;)V

    invoke-virtual {v0, v1, p4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_2

    :cond_3
    if-eqz p4, :cond_4

    if-eqz p3, :cond_4

    invoke-virtual {p3, p4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_4
    :goto_2
    return-void
.end method

.method private r0()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lj1/k;->s0(Lj1/k$d;)V

    return-void
.end method

.method private s0(Lj1/k$d;)V
    .locals 2

    iget-boolean v0, p0, Lj1/k;->y:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lj1/k;->f0()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lj1/k;->y:Z

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lj1/k;->z:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method private t0(Lj1/s0;Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez p3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-ne v2, v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    invoke-static {v0}, Le2/a;->a(Z)V

    iget-object v0, p0, Lj1/k;->r:Landroid/os/Handler;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lj1/k;->g0()I

    move-result v2

    invoke-interface {p1}, Lj1/s0;->a()I

    move-result v3

    if-eq v3, v2, :cond_3

    invoke-interface {p1}, Lj1/s0;->h()Lj1/s0;

    move-result-object p1

    invoke-interface {p1, v1, v2}, Lj1/s0;->d(II)Lj1/s0;

    move-result-object p1

    :cond_3
    invoke-direct {p0, p2, p3}, Lj1/k;->X(Landroid/os/Handler;Ljava/lang/Runnable;)Lj1/k$d;

    move-result-object p2

    const/4 p3, 0x3

    new-instance v2, Lj1/k$f;

    invoke-direct {v2, v1, p1, p2}, Lj1/k$f;-><init>(ILjava/lang/Object;Lj1/k$d;)V

    invoke-virtual {v0, p3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_3

    :cond_4
    invoke-interface {p1}, Lj1/s0;->a()I

    move-result v0

    if-lez v0, :cond_5

    invoke-interface {p1}, Lj1/s0;->h()Lj1/s0;

    move-result-object p1

    :cond_5
    iput-object p1, p0, Lj1/k;->A:Lj1/s0;

    if-eqz p3, :cond_6

    if-eqz p2, :cond_6

    invoke-virtual {p2, p3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_6
    :goto_3
    return-void
.end method

.method private v0(Lj1/k$e;Lh0/c4;)V
    .locals 2

    iget v0, p1, Lj1/k$e;->d:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lj1/k;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lj1/k;->s:Ljava/util/List;

    iget v1, p1, Lj1/k$e;->d:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/k$e;

    invoke-virtual {p2}, Lh0/c4;->t()I

    move-result p2

    iget v0, v0, Lj1/k$e;->e:I

    iget v1, p1, Lj1/k$e;->e:I

    sub-int/2addr v0, v1

    sub-int/2addr p2, v0

    if-eqz p2, :cond_0

    iget p1, p1, Lj1/k$e;->d:I

    add-int/lit8 p1, p1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lj1/k;->W(III)V

    :cond_0
    invoke-direct {p0}, Lj1/k;->r0()V

    return-void
.end method

.method private w0()V
    .locals 5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lj1/k;->y:Z

    iget-object v0, p0, Lj1/k;->z:Ljava/util/Set;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lj1/k;->z:Ljava/util/Set;

    new-instance v1, Lj1/k$b;

    iget-object v2, p0, Lj1/k;->s:Ljava/util/List;

    iget-object v3, p0, Lj1/k;->A:Lj1/s0;

    iget-boolean v4, p0, Lj1/k;->w:Z

    invoke-direct {v1, v2, v3, v4}, Lj1/k$b;-><init>(Ljava/util/Collection;Lj1/s0;Z)V

    invoke-virtual {p0, v1}, Lj1/a;->D(Lh0/c4;)V

    invoke-direct {p0}, Lj1/k;->f0()Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method


# virtual methods
.method protected declared-synchronized C(Ld2/p0;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lj1/g;->C(Ld2/p0;)V

    new-instance p1, Landroid/os/Handler;

    new-instance v0, Lj1/j;

    invoke-direct {v0, p0}, Lj1/j;-><init>(Lj1/k;)V

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object p1, p0, Lj1/k;->r:Landroid/os/Handler;

    iget-object p1, p0, Lj1/k;->p:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lj1/k;->w0()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lj1/k;->A:Lj1/s0;

    iget-object v0, p0, Lj1/k;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    invoke-interface {p1, v1, v0}, Lj1/s0;->d(II)Lj1/s0;

    move-result-object p1

    iput-object p1, p0, Lj1/k;->A:Lj1/s0;

    iget-object p1, p0, Lj1/k;->p:Ljava/util/List;

    invoke-direct {p0, v1, p1}, Lj1/k;->U(ILjava/util/Collection;)V

    invoke-direct {p0}, Lj1/k;->r0()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected declared-synchronized E()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lj1/g;->E()V

    iget-object v0, p0, Lj1/k;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lj1/k;->v:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lj1/k;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lj1/k;->A:Lj1/s0;

    invoke-interface {v0}, Lj1/s0;->h()Lj1/s0;

    move-result-object v0

    iput-object v0, p0, Lj1/k;->A:Lj1/s0;

    iget-object v0, p0, Lj1/k;->r:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iput-object v1, p0, Lj1/k;->r:Landroid/os/Handler;

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lj1/k;->y:Z

    iget-object v0, p0, Lj1/k;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lj1/k;->q:Ljava/util/Set;

    invoke-direct {p0, v0}, Lj1/k;->Z(Ljava/util/Set;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected bridge synthetic I(Ljava/lang/Object;Lj1/x$b;)Lj1/x$b;
    .locals 0

    check-cast p1, Lj1/k$e;

    invoke-virtual {p0, p1, p2}, Lj1/k;->c0(Lj1/k$e;Lj1/x$b;)Lj1/x$b;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic K(Ljava/lang/Object;I)I
    .locals 0

    check-cast p1, Lj1/k$e;

    invoke-virtual {p0, p1, p2}, Lj1/k;->h0(Lj1/k$e;I)I

    move-result p1

    return p1
.end method

.method protected bridge synthetic M(Ljava/lang/Object;Lj1/x;Lh0/c4;)V
    .locals 0

    check-cast p1, Lj1/k$e;

    invoke-virtual {p0, p1, p2, p3}, Lj1/k;->n0(Lj1/k$e;Lj1/x;Lh0/c4;)V

    return-void
.end method

.method public declared-synchronized S(ILjava/util/Collection;Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection<",
            "Lj1/x;",
            ">;",
            "Landroid/os/Handler;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, Lj1/k;->V(ILjava/util/Collection;Landroid/os/Handler;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized T(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lj1/x;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lj1/k;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1, v1}, Lj1/k;->V(ILjava/util/Collection;Landroid/os/Handler;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public a()Lh0/z1;
    .locals 1

    sget-object v0, Lj1/k;->B:Lh0/z1;

    return-object v0
.end method

.method protected c0(Lj1/k$e;Lj1/x$b;)Lj1/x$b;
    .locals 6

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, Lj1/k$e;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p1, Lj1/k$e;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/x$b;

    iget-wide v1, v1, Lj1/v;->d:J

    iget-wide v3, p2, Lj1/v;->d:J

    cmp-long v5, v1, v3

    if-nez v5, :cond_0

    iget-object v0, p2, Lj1/v;->a:Ljava/lang/Object;

    invoke-static {p1, v0}, Lj1/k;->e0(Lj1/k$e;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p2, p1}, Lj1/x$b;->c(Ljava/lang/Object;)Lj1/x$b;

    move-result-object p1

    return-object p1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized g()Lh0/c4;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lj1/k;->A:Lj1/s0;

    invoke-interface {v0}, Lj1/s0;->a()I

    move-result v0

    iget-object v1, p0, Lj1/k;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lj1/k;->A:Lj1/s0;

    invoke-interface {v0}, Lj1/s0;->h()Lj1/s0;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lj1/k;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lj1/s0;->d(II)Lj1/s0;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lj1/k;->A:Lj1/s0;

    :goto_0
    new-instance v1, Lj1/k$b;

    iget-object v2, p0, Lj1/k;->p:Ljava/util/List;

    iget-boolean v3, p0, Lj1/k;->w:Z

    invoke-direct {v1, v2, v0, v3}, Lj1/k$b;-><init>(Ljava/util/Collection;Lj1/s0;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g0()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lj1/k;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected h0(Lj1/k$e;I)I
    .locals 0

    iget p1, p1, Lj1/k$e;->e:I

    add-int/2addr p2, p1

    return p2
.end method

.method public i(Lj1/u;)V
    .locals 2

    iget-object v0, p0, Lj1/k;->t:Ljava/util/IdentityHashMap;

    invoke-virtual {v0, p1}, Ljava/util/IdentityHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/k$e;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/k$e;

    iget-object v1, v0, Lj1/k$e;->a:Lj1/s;

    invoke-virtual {v1, p1}, Lj1/s;->i(Lj1/u;)V

    iget-object v1, v0, Lj1/k$e;->c:Ljava/util/List;

    check-cast p1, Lj1/r;

    iget-object p1, p1, Lj1/r;->f:Lj1/x$b;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object p1, p0, Lj1/k;->t:Ljava/util/IdentityHashMap;

    invoke-virtual {p1}, Ljava/util/IdentityHashMap;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-direct {p0}, Lj1/k;->Y()V

    :cond_0
    invoke-direct {p0, v0}, Lj1/k;->j0(Lj1/k$e;)V

    return-void
.end method

.method public j(Lj1/x$b;Ld2/b;J)Lj1/u;
    .locals 3

    iget-object v0, p1, Lj1/v;->a:Ljava/lang/Object;

    invoke-static {v0}, Lj1/k;->d0(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p1, Lj1/v;->a:Ljava/lang/Object;

    invoke-static {v1}, Lj1/k;->b0(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Lj1/x$b;->c(Ljava/lang/Object;)Lj1/x$b;

    move-result-object p1

    iget-object v1, p0, Lj1/k;->u:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/k$e;

    if-nez v0, :cond_0

    new-instance v0, Lj1/k$e;

    new-instance v1, Lj1/k$c;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lj1/k$c;-><init>(Lj1/k$a;)V

    iget-boolean v2, p0, Lj1/k;->x:Z

    invoke-direct {v0, v1, v2}, Lj1/k$e;-><init>(Lj1/x;Z)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lj1/k$e;->f:Z

    iget-object v1, v0, Lj1/k$e;->a:Lj1/s;

    invoke-virtual {p0, v0, v1}, Lj1/g;->N(Ljava/lang/Object;Lj1/x;)V

    :cond_0
    invoke-direct {p0, v0}, Lj1/k;->a0(Lj1/k$e;)V

    iget-object v1, v0, Lj1/k$e;->c:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lj1/k$e;->a:Lj1/s;

    invoke-virtual {v1, p1, p2, p3, p4}, Lj1/s;->Z(Lj1/x$b;Ld2/b;J)Lj1/r;

    move-result-object p1

    iget-object p2, p0, Lj1/k;->t:Ljava/util/IdentityHashMap;

    invoke-virtual {p2, p1, v0}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lj1/k;->Y()V

    return-object p1
.end method

.method public declared-synchronized k0(IILandroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, Lj1/k;->m0(IILandroid/os/Handler;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected n0(Lj1/k$e;Lj1/x;Lh0/c4;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lj1/k;->v0(Lj1/k$e;Lh0/c4;)V

    return-void
.end method

.method public declared-synchronized p0(IILandroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, Lj1/k;->q0(IILandroid/os/Handler;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized u0(Lj1/s0;)V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0, v0}, Lj1/k;->t0(Lj1/s0;Landroid/os/Handler;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected y()V
    .locals 1

    invoke-super {p0}, Lj1/g;->y()V

    iget-object v0, p0, Lj1/k;->v:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method protected z()V
    .locals 0

    return-void
.end method
