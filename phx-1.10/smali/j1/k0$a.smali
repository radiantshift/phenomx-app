.class final Lj1/k0$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ld2/h0$e;
.implements Lj1/p$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj1/k0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation


# instance fields
.field private final a:J

.field private final b:Landroid/net/Uri;

.field private final c:Ld2/o0;

.field private final d:Lj1/f0;

.field private final e:Lm0/n;

.field private final f:Le2/g;

.field private final g:Lm0/a0;

.field private volatile h:Z

.field private i:Z

.field private j:J

.field private k:Ld2/p;

.field private l:Lm0/e0;

.field private m:Z

.field final synthetic n:Lj1/k0;


# direct methods
.method public constructor <init>(Lj1/k0;Landroid/net/Uri;Ld2/l;Lj1/f0;Lm0/n;Le2/g;)V
    .locals 0

    iput-object p1, p0, Lj1/k0$a;->n:Lj1/k0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lj1/k0$a;->b:Landroid/net/Uri;

    new-instance p1, Ld2/o0;

    invoke-direct {p1, p3}, Ld2/o0;-><init>(Ld2/l;)V

    iput-object p1, p0, Lj1/k0$a;->c:Ld2/o0;

    iput-object p4, p0, Lj1/k0$a;->d:Lj1/f0;

    iput-object p5, p0, Lj1/k0$a;->e:Lm0/n;

    iput-object p6, p0, Lj1/k0$a;->f:Le2/g;

    new-instance p1, Lm0/a0;

    invoke-direct {p1}, Lm0/a0;-><init>()V

    iput-object p1, p0, Lj1/k0$a;->g:Lm0/a0;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lj1/k0$a;->i:Z

    invoke-static {}, Lj1/q;->a()J

    move-result-wide p1

    iput-wide p1, p0, Lj1/k0$a;->a:J

    const-wide/16 p1, 0x0

    invoke-direct {p0, p1, p2}, Lj1/k0$a;->i(J)Ld2/p;

    move-result-object p1

    iput-object p1, p0, Lj1/k0$a;->k:Ld2/p;

    return-void
.end method

.method static synthetic d(Lj1/k0$a;)Ld2/o0;
    .locals 0

    iget-object p0, p0, Lj1/k0$a;->c:Ld2/o0;

    return-object p0
.end method

.method static synthetic e(Lj1/k0$a;)J
    .locals 2

    iget-wide v0, p0, Lj1/k0$a;->a:J

    return-wide v0
.end method

.method static synthetic f(Lj1/k0$a;)Ld2/p;
    .locals 0

    iget-object p0, p0, Lj1/k0$a;->k:Ld2/p;

    return-object p0
.end method

.method static synthetic g(Lj1/k0$a;)J
    .locals 2

    iget-wide v0, p0, Lj1/k0$a;->j:J

    return-wide v0
.end method

.method static synthetic h(Lj1/k0$a;JJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lj1/k0$a;->j(JJ)V

    return-void
.end method

.method private i(J)Ld2/p;
    .locals 2

    new-instance v0, Ld2/p$b;

    invoke-direct {v0}, Ld2/p$b;-><init>()V

    iget-object v1, p0, Lj1/k0$a;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ld2/p$b;->i(Landroid/net/Uri;)Ld2/p$b;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ld2/p$b;->h(J)Ld2/p$b;

    move-result-object p1

    iget-object p2, p0, Lj1/k0$a;->n:Lj1/k0;

    invoke-static {p2}, Lj1/k0;->D(Lj1/k0;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ld2/p$b;->f(Ljava/lang/String;)Ld2/p$b;

    move-result-object p1

    const/4 p2, 0x6

    invoke-virtual {p1, p2}, Ld2/p$b;->b(I)Ld2/p$b;

    move-result-object p1

    invoke-static {}, Lj1/k0;->C()Ljava/util/Map;

    move-result-object p2

    invoke-virtual {p1, p2}, Ld2/p$b;->e(Ljava/util/Map;)Ld2/p$b;

    move-result-object p1

    invoke-virtual {p1}, Ld2/p$b;->a()Ld2/p;

    move-result-object p1

    return-object p1
.end method

.method private j(JJ)V
    .locals 1

    iget-object v0, p0, Lj1/k0$a;->g:Lm0/a0;

    iput-wide p1, v0, Lm0/a0;->a:J

    iput-wide p3, p0, Lj1/k0$a;->j:J

    const/4 p1, 0x1

    iput-boolean p1, p0, Lj1/k0$a;->i:Z

    const/4 p1, 0x0

    iput-boolean p1, p0, Lj1/k0$a;->m:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 17

    move-object/from16 v1, p0

    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_9

    iget-boolean v3, v1, Lj1/k0$a;->h:Z

    if-nez v3, :cond_9

    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    :try_start_0
    iget-object v6, v1, Lj1/k0$a;->g:Lm0/a0;

    iget-wide v13, v6, Lm0/a0;->a:J

    invoke-direct {v1, v13, v14}, Lj1/k0$a;->i(J)Ld2/p;

    move-result-object v6

    iput-object v6, v1, Lj1/k0$a;->k:Ld2/p;

    iget-object v7, v1, Lj1/k0$a;->c:Ld2/o0;

    invoke-virtual {v7, v6}, Ld2/o0;->b(Ld2/p;)J

    move-result-wide v6

    cmp-long v8, v6, v4

    if-eqz v8, :cond_0

    add-long/2addr v6, v13

    iget-object v8, v1, Lj1/k0$a;->n:Lj1/k0;

    invoke-static {v8}, Lj1/k0;->E(Lj1/k0;)V

    :cond_0
    move-wide v15, v6

    iget-object v6, v1, Lj1/k0$a;->n:Lj1/k0;

    iget-object v7, v1, Lj1/k0$a;->c:Ld2/o0;

    invoke-virtual {v7}, Ld2/o0;->f()Ljava/util/Map;

    move-result-object v7

    invoke-static {v7}, Ld1/b;->d(Ljava/util/Map;)Ld1/b;

    move-result-object v7

    invoke-static {v6, v7}, Lj1/k0;->G(Lj1/k0;Ld1/b;)Ld1/b;

    iget-object v6, v1, Lj1/k0$a;->c:Ld2/o0;

    iget-object v7, v1, Lj1/k0$a;->n:Lj1/k0;

    invoke-static {v7}, Lj1/k0;->F(Lj1/k0;)Ld1/b;

    move-result-object v7

    if-eqz v7, :cond_1

    iget-object v7, v1, Lj1/k0$a;->n:Lj1/k0;

    invoke-static {v7}, Lj1/k0;->F(Lj1/k0;)Ld1/b;

    move-result-object v7

    iget v7, v7, Ld1/b;->k:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_1

    new-instance v6, Lj1/p;

    iget-object v7, v1, Lj1/k0$a;->c:Ld2/o0;

    iget-object v8, v1, Lj1/k0$a;->n:Lj1/k0;

    invoke-static {v8}, Lj1/k0;->F(Lj1/k0;)Ld1/b;

    move-result-object v8

    iget v8, v8, Ld1/b;->k:I

    invoke-direct {v6, v7, v8, v1}, Lj1/p;-><init>(Ld2/l;ILj1/p$a;)V

    iget-object v7, v1, Lj1/k0$a;->n:Lj1/k0;

    invoke-virtual {v7}, Lj1/k0;->O()Lm0/e0;

    move-result-object v7

    iput-object v7, v1, Lj1/k0$a;->l:Lm0/e0;

    invoke-static {}, Lj1/k0;->H()Lh0/r1;

    move-result-object v8

    invoke-interface {v7, v8}, Lm0/e0;->e(Lh0/r1;)V

    :cond_1
    move-object v8, v6

    iget-object v7, v1, Lj1/k0$a;->d:Lj1/f0;

    iget-object v9, v1, Lj1/k0$a;->b:Landroid/net/Uri;

    iget-object v6, v1, Lj1/k0$a;->c:Ld2/o0;

    invoke-virtual {v6}, Ld2/o0;->f()Ljava/util/Map;

    move-result-object v10

    iget-object v6, v1, Lj1/k0$a;->e:Lm0/n;

    move-wide v11, v13

    move-wide v4, v13

    move-wide v13, v15

    move-object v15, v6

    invoke-interface/range {v7 .. v15}, Lj1/f0;->d(Ld2/i;Landroid/net/Uri;Ljava/util/Map;JJLm0/n;)V

    iget-object v6, v1, Lj1/k0$a;->n:Lj1/k0;

    invoke-static {v6}, Lj1/k0;->F(Lj1/k0;)Ld1/b;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, v1, Lj1/k0$a;->d:Lj1/f0;

    invoke-interface {v6}, Lj1/f0;->f()V

    :cond_2
    iget-boolean v6, v1, Lj1/k0$a;->i:Z

    if-eqz v6, :cond_3

    iget-object v6, v1, Lj1/k0$a;->d:Lj1/f0;

    iget-wide v7, v1, Lj1/k0$a;->j:J

    invoke-interface {v6, v4, v5, v7, v8}, Lj1/f0;->b(JJ)V

    iput-boolean v0, v1, Lj1/k0$a;->i:Z

    :cond_3
    :goto_1
    move-wide v13, v4

    :cond_4
    if-nez v2, :cond_5

    iget-boolean v4, v1, Lj1/k0$a;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_5

    :try_start_1
    iget-object v4, v1, Lj1/k0$a;->f:Le2/g;

    invoke-virtual {v4}, Le2/g;->a()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v4, v1, Lj1/k0$a;->d:Lj1/f0;

    iget-object v5, v1, Lj1/k0$a;->g:Lm0/a0;

    invoke-interface {v4, v5}, Lj1/f0;->c(Lm0/a0;)I

    move-result v2

    iget-object v4, v1, Lj1/k0$a;->d:Lj1/f0;

    invoke-interface {v4}, Lj1/f0;->e()J

    move-result-wide v4

    iget-object v6, v1, Lj1/k0$a;->n:Lj1/k0;

    invoke-static {v6}, Lj1/k0;->I(Lj1/k0;)J

    move-result-wide v6

    add-long/2addr v6, v13

    cmp-long v8, v4, v6

    if-lez v8, :cond_4

    iget-object v6, v1, Lj1/k0$a;->f:Le2/g;

    invoke-virtual {v6}, Le2/g;->c()Z

    iget-object v6, v1, Lj1/k0$a;->n:Lj1/k0;

    invoke-static {v6}, Lj1/k0;->A(Lj1/k0;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, v1, Lj1/k0$a;->n:Lj1/k0;

    invoke-static {v7}, Lj1/k0;->z(Lj1/k0;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :catch_0
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_5
    if-ne v2, v3, :cond_6

    const/4 v2, 0x0

    goto :goto_2

    :cond_6
    iget-object v3, v1, Lj1/k0$a;->d:Lj1/f0;

    invoke-interface {v3}, Lj1/f0;->e()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v7, v3, v5

    if-eqz v7, :cond_7

    iget-object v3, v1, Lj1/k0$a;->g:Lm0/a0;

    iget-object v4, v1, Lj1/k0$a;->d:Lj1/f0;

    invoke-interface {v4}, Lj1/f0;->e()J

    move-result-wide v4

    iput-wide v4, v3, Lm0/a0;->a:J

    :cond_7
    :goto_2
    iget-object v3, v1, Lj1/k0$a;->c:Ld2/o0;

    invoke-static {v3}, Ld2/o;->a(Ld2/l;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eq v2, v3, :cond_8

    iget-object v2, v1, Lj1/k0$a;->d:Lj1/f0;

    invoke-interface {v2}, Lj1/f0;->e()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v6, v2, v4

    if-eqz v6, :cond_8

    iget-object v2, v1, Lj1/k0$a;->g:Lm0/a0;

    iget-object v3, v1, Lj1/k0$a;->d:Lj1/f0;

    invoke-interface {v3}, Lj1/f0;->e()J

    move-result-wide v3

    iput-wide v3, v2, Lm0/a0;->a:J

    :cond_8
    iget-object v2, v1, Lj1/k0$a;->c:Ld2/o0;

    invoke-static {v2}, Ld2/o;->a(Ld2/l;)V

    throw v0

    :cond_9
    return-void
.end method

.method public b(Le2/a0;)V
    .locals 11

    iget-boolean v0, p0, Lj1/k0$a;->m:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    iget-wide v2, p0, Lj1/k0$a;->j:J

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lj1/k0$a;->n:Lj1/k0;

    invoke-static {v0, v1}, Lj1/k0;->B(Lj1/k0;Z)J

    move-result-wide v2

    iget-wide v4, p0, Lj1/k0$a;->j:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    :goto_0
    move-wide v5, v2

    invoke-virtual {p1}, Le2/a0;->a()I

    move-result v8

    iget-object v0, p0, Lj1/k0$a;->l:Lm0/e0;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lm0/e0;

    invoke-interface {v4, p1, v8}, Lm0/e0;->a(Le2/a0;I)V

    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-interface/range {v4 .. v10}, Lm0/e0;->f(JIIILm0/e0$a;)V

    iput-boolean v1, p0, Lj1/k0$a;->m:Z

    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lj1/k0$a;->h:Z

    return-void
.end method
