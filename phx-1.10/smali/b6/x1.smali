.class public Lb6/x1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lb6/q1;
.implements Lb6/s;
.implements Lb6/e2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb6/x1$b;,
        Lb6/x1$a;
    }
.end annotation


# static fields
.field private static final synthetic f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;


# instance fields
.field private volatile synthetic _parentHandle:Ljava/lang/Object;

.field private volatile synthetic _state:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lb6/x1;

    const-class v1, Ljava/lang/Object;

    const-string v2, "_state"

    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, Lb6/x1;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    invoke-static {}, Lb6/y1;->c()Lb6/a1;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Lb6/y1;->d()Lb6/a1;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lb6/x1;->_state:Ljava/lang/Object;

    const/4 p1, 0x0

    iput-object p1, p0, Lb6/x1;->_parentHandle:Ljava/lang/Object;

    return-void
.end method

.method public static final synthetic A(Lb6/x1;Lb6/x1$b;Lb6/r;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lb6/x1;->M(Lb6/x1$b;Lb6/r;Ljava/lang/Object;)V

    return-void
.end method

.method private final C(Ljava/lang/Object;Lb6/b2;Lb6/w1;)Z
    .locals 2

    new-instance v0, Lb6/x1$c;

    invoke-direct {v0, p3, p0, p1}, Lb6/x1$c;-><init>(Lkotlinx/coroutines/internal/r;Lb6/x1;Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p2}, Lkotlinx/coroutines/internal/r;->q()Lkotlinx/coroutines/internal/r;

    move-result-object p1

    invoke-virtual {p1, p3, p2, v0}, Lkotlinx/coroutines/internal/r;->x(Lkotlinx/coroutines/internal/r;Lkotlinx/coroutines/internal/r;Lkotlinx/coroutines/internal/r$a;)I

    move-result p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    return v1
.end method

.method private final D(Ljava/lang/Throwable;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    return-void

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/util/IdentityHashMap;

    invoke-direct {v1, v0}, Ljava/util/IdentityHashMap;-><init>(I)V

    invoke-static {v1}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    invoke-static {}, Lb6/o0;->d()Z

    move-result v1

    if-nez v1, :cond_1

    move-object v1, p1

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lkotlinx/coroutines/internal/d0;->n(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    invoke-static {}, Lb6/o0;->d()Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_2

    :cond_3
    invoke-static {v2}, Lkotlinx/coroutines/internal/d0;->n(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v2

    :goto_2
    if-eq v2, p1, :cond_2

    if-eq v2, v1, :cond_2

    instance-of v3, v2, Ljava/util/concurrent/CancellationException;

    if-nez v3, :cond_2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p1, v2}, Lk5/a;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_4
    return-void
.end method

.method private final H(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    :cond_0
    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lb6/l1;

    if-eqz v1, :cond_2

    instance-of v1, v0, Lb6/x1$b;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lb6/x1$b;

    invoke-virtual {v1}, Lb6/x1$b;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-instance v1, Lb6/v;

    invoke-direct {p0, p1}, Lb6/x1;->N(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lb6/v;-><init>(Ljava/lang/Throwable;ZILkotlin/jvm/internal/e;)V

    invoke-direct {p0, v0, v1}, Lb6/x1;->x0(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {}, Lb6/y1;->b()Lkotlinx/coroutines/internal/e0;

    move-result-object v1

    if-eq v0, v1, :cond_0

    return-object v0

    :cond_2
    :goto_0
    invoke-static {}, Lb6/y1;->a()Lkotlinx/coroutines/internal/e0;

    move-result-object p1

    return-object p1
.end method

.method private final I(Ljava/lang/Throwable;)Z
    .locals 4

    invoke-virtual {p0}, Lb6/x1;->b0()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    invoke-virtual {p0}, Lb6/x1;->V()Lb6/q;

    move-result-object v2

    if-eqz v2, :cond_4

    sget-object v3, Lb6/c2;->f:Lb6/c2;

    if-ne v2, v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-interface {v2, p1}, Lb6/q;->f(Ljava/lang/Throwable;)Z

    move-result p1

    if-nez p1, :cond_3

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_0
    return v1

    :cond_4
    :goto_1
    return v0
.end method

.method private final L(Lb6/l1;Ljava/lang/Object;)V
    .locals 3

    invoke-virtual {p0}, Lb6/x1;->V()Lb6/q;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lb6/z0;->b()V

    sget-object v0, Lb6/c2;->f:Lb6/c2;

    invoke-virtual {p0, v0}, Lb6/x1;->p0(Lb6/q;)V

    :cond_0
    instance-of v0, p2, Lb6/v;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    check-cast p2, Lb6/v;

    goto :goto_0

    :cond_1
    move-object p2, v1

    :goto_0
    if-eqz p2, :cond_2

    iget-object v1, p2, Lb6/v;->a:Ljava/lang/Throwable;

    :cond_2
    instance-of p2, p1, Lb6/w1;

    if-eqz p2, :cond_3

    :try_start_0
    move-object p2, p1

    check-cast p2, Lb6/w1;

    invoke-virtual {p2, v1}, Lb6/x;->y(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p2

    new-instance v0, Lb6/y;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception in completion handler "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " for "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, p2}, Lb6/y;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0, v0}, Lb6/x1;->Y(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    invoke-interface {p1}, Lb6/l1;->h()Lb6/b2;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-direct {p0, p1, v1}, Lb6/x1;->i0(Lb6/b2;Ljava/lang/Throwable;)V

    :cond_4
    :goto_1
    return-void
.end method

.method private final M(Lb6/x1$b;Lb6/r;Ljava/lang/Object;)V
    .locals 1

    invoke-static {}, Lb6/o0;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_2
    :goto_1
    invoke-direct {p0, p2}, Lb6/x1;->g0(Lkotlinx/coroutines/internal/r;)Lb6/r;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-direct {p0, p1, p2, p3}, Lb6/x1;->z0(Lb6/x1$b;Lb6/r;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    return-void

    :cond_3
    invoke-direct {p0, p1, p3}, Lb6/x1;->O(Lb6/x1$b;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Lb6/x1;->E(Ljava/lang/Object;)V

    return-void
.end method

.method private final N(Ljava/lang/Object;)Ljava/lang/Throwable;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    instance-of v0, p1, Ljava/lang/Throwable;

    :goto_0
    if-eqz v0, :cond_1

    check-cast p1, Ljava/lang/Throwable;

    if-nez p1, :cond_2

    const/4 p1, 0x0

    new-instance v0, Lb6/r1;

    invoke-static {p0}, Lb6/x1;->z(Lb6/x1;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0}, Lb6/r1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lb6/q1;)V

    move-object p1, v0

    goto :goto_1

    :cond_1
    const-string v0, "null cannot be cast to non-null type kotlinx.coroutines.ParentJob"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Lb6/e2;

    invoke-interface {p1}, Lb6/e2;->s()Ljava/util/concurrent/CancellationException;

    move-result-object p1

    :cond_2
    :goto_1
    return-object p1
.end method

.method private final O(Lb6/x1$b;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    invoke-static {}, Lb6/o0;->a()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_2
    :goto_1
    invoke-static {}, Lb6/o0;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lb6/x1$b;->i()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_4
    :goto_2
    invoke-static {}, Lb6/o0;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lb6/x1$b;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_3

    :cond_5
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_6
    :goto_3
    instance-of v0, p2, Lb6/v;

    const/4 v3, 0x0

    if-eqz v0, :cond_7

    move-object v0, p2

    check-cast v0, Lb6/v;

    goto :goto_4

    :cond_7
    move-object v0, v3

    :goto_4
    if-eqz v0, :cond_8

    iget-object v0, v0, Lb6/v;->a:Ljava/lang/Throwable;

    goto :goto_5

    :cond_8
    move-object v0, v3

    :goto_5
    monitor-enter p1

    :try_start_0
    invoke-virtual {p1}, Lb6/x1$b;->f()Z

    move-result v4

    invoke-virtual {p1, v0}, Lb6/x1$b;->j(Ljava/lang/Throwable;)Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, p1, v5}, Lb6/x1;->R(Lb6/x1$b;Ljava/util/List;)Ljava/lang/Throwable;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-direct {p0, v6, v5}, Lb6/x1;->D(Ljava/lang/Throwable;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_9
    monitor-exit p1

    if-nez v6, :cond_a

    goto :goto_6

    :cond_a
    if-ne v6, v0, :cond_b

    goto :goto_6

    :cond_b
    new-instance p2, Lb6/v;

    const/4 v0, 0x2

    invoke-direct {p2, v6, v2, v0, v3}, Lb6/v;-><init>(Ljava/lang/Throwable;ZILkotlin/jvm/internal/e;)V

    :goto_6
    if-eqz v6, :cond_e

    invoke-direct {p0, v6}, Lb6/x1;->I(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p0, v6}, Lb6/x1;->X(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_c

    goto :goto_7

    :cond_c
    const/4 v1, 0x0

    :cond_d
    :goto_7
    if-eqz v1, :cond_e

    const-string v0, "null cannot be cast to non-null type kotlinx.coroutines.CompletedExceptionally"

    invoke-static {p2, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-object v0, p2

    check-cast v0, Lb6/v;

    invoke-virtual {v0}, Lb6/v;->b()Z

    :cond_e
    if-nez v4, :cond_f

    invoke-virtual {p0, v6}, Lb6/x1;->j0(Ljava/lang/Throwable;)V

    :cond_f
    invoke-virtual {p0, p2}, Lb6/x1;->k0(Ljava/lang/Object;)V

    sget-object v0, Lb6/x1;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {p2}, Lb6/y1;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, p0, p1, v1}, Landroidx/concurrent/futures/b;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {}, Lb6/o0;->a()Z

    move-result v1

    if-eqz v1, :cond_11

    if-eqz v0, :cond_10

    goto :goto_8

    :cond_10
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_11
    :goto_8
    invoke-direct {p0, p1, p2}, Lb6/x1;->L(Lb6/l1;Ljava/lang/Object;)V

    return-object p2

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2
.end method

.method private final P(Lb6/l1;)Lb6/r;
    .locals 2

    instance-of v0, p1, Lb6/r;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lb6/r;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    invoke-interface {p1}, Lb6/l1;->h()Lb6/b2;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-direct {p0, p1}, Lb6/x1;->g0(Lkotlinx/coroutines/internal/r;)Lb6/r;

    move-result-object v1

    goto :goto_1

    :cond_1
    move-object v1, v0

    :cond_2
    :goto_1
    return-object v1
.end method

.method private final Q(Ljava/lang/Object;)Ljava/lang/Throwable;
    .locals 2

    instance-of v0, p1, Lb6/v;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p1, Lb6/v;

    goto :goto_0

    :cond_0
    move-object p1, v1

    :goto_0
    if-eqz p1, :cond_1

    iget-object v1, p1, Lb6/v;->a:Ljava/lang/Throwable;

    :cond_1
    return-object v1
.end method

.method private final R(Lb6/x1$b;Ljava/util/List;)Ljava/lang/Throwable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb6/x1$b;",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/Throwable;",
            ">;)",
            "Ljava/lang/Throwable;"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lb6/x1$b;->f()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lb6/r1;

    invoke-static {p0}, Lb6/x1;->z(Lb6/x1;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, v1, p0}, Lb6/r1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lb6/q1;)V

    return-object p1

    :cond_0
    return-object v1

    :cond_1
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/Throwable;

    instance-of v2, v2, Ljava/util/concurrent/CancellationException;

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    move-object v1, v0

    :cond_3
    check-cast v1, Ljava/lang/Throwable;

    if-eqz v1, :cond_4

    return-object v1

    :cond_4
    const/4 p1, 0x0

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    return-object p1
.end method

.method private final U(Lb6/l1;)Lb6/b2;
    .locals 3

    invoke-interface {p1}, Lb6/l1;->h()Lb6/b2;

    move-result-object v0

    if-nez v0, :cond_2

    instance-of v0, p1, Lb6/a1;

    if-eqz v0, :cond_0

    new-instance v0, Lb6/b2;

    invoke-direct {v0}, Lb6/b2;-><init>()V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lb6/w1;

    if-eqz v0, :cond_1

    check-cast p1, Lb6/w1;

    invoke-direct {p0, p1}, Lb6/x1;->n0(Lb6/w1;)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "State should have list: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    return-object v0
.end method

.method private final c0(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    const/4 v0, 0x0

    move-object v1, v0

    :cond_0
    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lb6/x1$b;

    if-eqz v3, :cond_7

    monitor-enter v2

    :try_start_0
    move-object v3, v2

    check-cast v3, Lb6/x1$b;

    invoke-virtual {v3}, Lb6/x1$b;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lb6/y1;->f()Lkotlinx/coroutines/internal/e0;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-object p1

    :cond_1
    :try_start_1
    move-object v3, v2

    check-cast v3, Lb6/x1$b;

    invoke-virtual {v3}, Lb6/x1$b;->f()Z

    move-result v3

    if-nez p1, :cond_2

    if-nez v3, :cond_4

    :cond_2
    if-nez v1, :cond_3

    invoke-direct {p0, p1}, Lb6/x1;->N(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v1

    :cond_3
    move-object p1, v2

    check-cast p1, Lb6/x1$b;

    invoke-virtual {p1, v1}, Lb6/x1$b;->b(Ljava/lang/Throwable;)V

    :cond_4
    move-object p1, v2

    check-cast p1, Lb6/x1$b;

    invoke-virtual {p1}, Lb6/x1$b;->e()Ljava/lang/Throwable;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    xor-int/lit8 v1, v3, 0x1

    if-eqz v1, :cond_5

    move-object v0, p1

    :cond_5
    monitor-exit v2

    if-eqz v0, :cond_6

    check-cast v2, Lb6/x1$b;

    invoke-virtual {v2}, Lb6/x1$b;->h()Lb6/b2;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lb6/x1;->h0(Lb6/b2;Ljava/lang/Throwable;)V

    :cond_6
    invoke-static {}, Lb6/y1;->a()Lkotlinx/coroutines/internal/e0;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v2

    throw p1

    :cond_7
    instance-of v3, v2, Lb6/l1;

    if-eqz v3, :cond_b

    if-nez v1, :cond_8

    invoke-direct {p0, p1}, Lb6/x1;->N(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v1

    :cond_8
    move-object v3, v2

    check-cast v3, Lb6/l1;

    invoke-interface {v3}, Lb6/l1;->a()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-direct {p0, v3, v1}, Lb6/x1;->w0(Lb6/l1;Ljava/lang/Throwable;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lb6/y1;->a()Lkotlinx/coroutines/internal/e0;

    move-result-object p1

    return-object p1

    :cond_9
    new-instance v3, Lb6/v;

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-direct {v3, v1, v4, v5, v0}, Lb6/v;-><init>(Ljava/lang/Throwable;ZILkotlin/jvm/internal/e;)V

    invoke-direct {p0, v2, v3}, Lb6/x1;->x0(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {}, Lb6/y1;->a()Lkotlinx/coroutines/internal/e0;

    move-result-object v4

    if-eq v3, v4, :cond_a

    invoke-static {}, Lb6/y1;->b()Lkotlinx/coroutines/internal/e0;

    move-result-object v2

    if-eq v3, v2, :cond_0

    return-object v3

    :cond_a
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot happen in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_b
    invoke-static {}, Lb6/y1;->f()Lkotlinx/coroutines/internal/e0;

    move-result-object p1

    return-object p1
.end method

.method private final e0(Lu5/l;Z)Lb6/w1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu5/l<",
            "-",
            "Ljava/lang/Throwable;",
            "Lk5/s;",
            ">;Z)",
            "Lb6/w1;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    instance-of p2, p1, Lb6/s1;

    if-eqz p2, :cond_0

    move-object v0, p1

    check-cast v0, Lb6/s1;

    :cond_0
    if-nez v0, :cond_5

    new-instance v0, Lb6/o1;

    invoke-direct {v0, p1}, Lb6/o1;-><init>(Lu5/l;)V

    goto :goto_0

    :cond_1
    instance-of p2, p1, Lb6/w1;

    if-eqz p2, :cond_2

    move-object v0, p1

    check-cast v0, Lb6/w1;

    :cond_2
    if-eqz v0, :cond_4

    invoke-static {}, Lb6/o0;->a()Z

    move-result p1

    if-eqz p1, :cond_5

    instance-of p1, v0, Lb6/s1;

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_4
    new-instance v0, Lb6/p1;

    invoke-direct {v0, p1}, Lb6/p1;-><init>(Lu5/l;)V

    :cond_5
    :goto_0
    invoke-virtual {v0, p0}, Lb6/w1;->A(Lb6/x1;)V

    return-object v0
.end method

.method private final g0(Lkotlinx/coroutines/internal/r;)Lb6/r;
    .locals 1

    :goto_0
    invoke-virtual {p1}, Lkotlinx/coroutines/internal/r;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/r;->q()Lkotlinx/coroutines/internal/r;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lkotlinx/coroutines/internal/r;->p()Lkotlinx/coroutines/internal/r;

    move-result-object p1

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/r;->t()Z

    move-result v0

    if-nez v0, :cond_0

    instance-of v0, p1, Lb6/r;

    if-eqz v0, :cond_1

    check-cast p1, Lb6/r;

    return-object p1

    :cond_1
    instance-of v0, p1, Lb6/b2;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1
.end method

.method private final h0(Lb6/b2;Ljava/lang/Throwable;)V
    .locals 6

    invoke-virtual {p0, p2}, Lb6/x1;->j0(Ljava/lang/Throwable;)V

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/r;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlinx/coroutines/internal/r;

    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, p1}, Lkotlin/jvm/internal/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    instance-of v2, v0, Lb6/s1;

    if-eqz v2, :cond_1

    move-object v2, v0

    check-cast v2, Lb6/w1;

    :try_start_0
    invoke-virtual {v2, p2}, Lb6/x;->y(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    if-eqz v1, :cond_0

    invoke-static {v1, v3}, Lk5/a;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    new-instance v1, Lb6/y;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception in completion handler "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " for "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Lb6/y;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    sget-object v2, Lk5/s;->a:Lk5/s;

    :cond_1
    :goto_1
    invoke-virtual {v0}, Lkotlinx/coroutines/internal/r;->p()Lkotlinx/coroutines/internal/r;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {p0, v1}, Lb6/x1;->Y(Ljava/lang/Throwable;)V

    :cond_3
    invoke-direct {p0, p2}, Lb6/x1;->I(Ljava/lang/Throwable;)Z

    return-void
.end method

.method private final i0(Lb6/b2;Ljava/lang/Throwable;)V
    .locals 6

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/r;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlinx/coroutines/internal/r;

    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, p1}, Lkotlin/jvm/internal/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    instance-of v2, v0, Lb6/w1;

    if-eqz v2, :cond_1

    move-object v2, v0

    check-cast v2, Lb6/w1;

    :try_start_0
    invoke-virtual {v2, p2}, Lb6/x;->y(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    if-eqz v1, :cond_0

    invoke-static {v1, v3}, Lk5/a;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    new-instance v1, Lb6/y;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception in completion handler "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " for "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Lb6/y;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    sget-object v2, Lk5/s;->a:Lk5/s;

    :cond_1
    :goto_1
    invoke-virtual {v0}, Lkotlinx/coroutines/internal/r;->p()Lkotlinx/coroutines/internal/r;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {p0, v1}, Lb6/x1;->Y(Ljava/lang/Throwable;)V

    :cond_3
    return-void
.end method

.method private final m0(Lb6/a1;)V
    .locals 2

    new-instance v0, Lb6/b2;

    invoke-direct {v0}, Lb6/b2;-><init>()V

    invoke-virtual {p1}, Lb6/a1;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lb6/k1;

    invoke-direct {v1, v0}, Lb6/k1;-><init>(Lb6/b2;)V

    move-object v0, v1

    :goto_0
    sget-object v1, Lb6/x1;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v1, p0, p1, v0}, Landroidx/concurrent/futures/b;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    return-void
.end method

.method private final n0(Lb6/w1;)V
    .locals 2

    new-instance v0, Lb6/b2;

    invoke-direct {v0}, Lb6/b2;-><init>()V

    invoke-virtual {p1, v0}, Lkotlinx/coroutines/internal/r;->k(Lkotlinx/coroutines/internal/r;)Z

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/r;->p()Lkotlinx/coroutines/internal/r;

    move-result-object v0

    sget-object v1, Lb6/x1;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v1, p0, p1, v0}, Landroidx/concurrent/futures/b;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    return-void
.end method

.method private final q0(Ljava/lang/Object;)I
    .locals 4

    instance-of v0, p1, Lb6/a1;

    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lb6/a1;

    invoke-virtual {v0}, Lb6/a1;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    return v3

    :cond_0
    sget-object v0, Lb6/x1;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {}, Lb6/y1;->c()Lb6/a1;

    move-result-object v3

    invoke-static {v0, p0, p1, v3}, Landroidx/concurrent/futures/b;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Lb6/x1;->l0()V

    return v2

    :cond_2
    instance-of v0, p1, Lb6/k1;

    if-eqz v0, :cond_4

    sget-object v0, Lb6/x1;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-object v3, p1

    check-cast v3, Lb6/k1;

    invoke-virtual {v3}, Lb6/k1;->h()Lb6/b2;

    move-result-object v3

    invoke-static {v0, p0, p1, v3}, Landroidx/concurrent/futures/b;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    return v1

    :cond_3
    invoke-virtual {p0}, Lb6/x1;->l0()V

    return v2

    :cond_4
    return v3
.end method

.method private final r0(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    instance-of v0, p1, Lb6/x1$b;

    const-string v1, "Active"

    if-eqz v0, :cond_1

    check-cast p1, Lb6/x1$b;

    invoke-virtual {p1}, Lb6/x1$b;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "Cancelling"

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lb6/x1$b;->g()Z

    move-result p1

    if-eqz p1, :cond_5

    const-string v1, "Completing"

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lb6/l1;

    if-eqz v0, :cond_3

    check-cast p1, Lb6/l1;

    invoke-interface {p1}, Lb6/l1;->a()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const-string v1, "New"

    goto :goto_0

    :cond_3
    instance-of p1, p1, Lb6/v;

    if-eqz p1, :cond_4

    const-string v1, "Cancelled"

    goto :goto_0

    :cond_4
    const-string v1, "Completed"

    :cond_5
    :goto_0
    return-object v1
.end method

.method public static synthetic t0(Lb6/x1;Ljava/lang/Throwable;Ljava/lang/String;ILjava/lang/Object;)Ljava/util/concurrent/CancellationException;
    .locals 0

    if-nez p4, :cond_1

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lb6/x1;->s0(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/util/concurrent/CancellationException;

    move-result-object p0

    return-object p0

    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: toCancellationException"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private final v0(Lb6/l1;Ljava/lang/Object;)Z
    .locals 4

    invoke-static {}, Lb6/o0;->a()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_3

    instance-of v0, p1, Lb6/a1;

    if-nez v0, :cond_1

    instance-of v0, p1, Lb6/w1;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_3
    :goto_2
    invoke-static {}, Lb6/o0;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    instance-of v0, p2, Lb6/v;

    xor-int/2addr v0, v2

    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_5
    :goto_3
    sget-object v0, Lb6/x1;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {p2}, Lb6/y1;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, p0, p1, v3}, Landroidx/concurrent/futures/b;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    return v1

    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lb6/x1;->j0(Ljava/lang/Throwable;)V

    invoke-virtual {p0, p2}, Lb6/x1;->k0(Ljava/lang/Object;)V

    invoke-direct {p0, p1, p2}, Lb6/x1;->L(Lb6/l1;Ljava/lang/Object;)V

    return v2
.end method

.method private final w0(Lb6/l1;Ljava/lang/Throwable;)Z
    .locals 5

    invoke-static {}, Lb6/o0;->a()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    instance-of v0, p1, Lb6/x1$b;

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_1
    :goto_0
    invoke-static {}, Lb6/o0;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Lb6/l1;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_3
    :goto_1
    invoke-direct {p0, p1}, Lb6/x1;->U(Lb6/l1;)Lb6/b2;

    move-result-object v0

    const/4 v2, 0x0

    if-nez v0, :cond_4

    return v2

    :cond_4
    new-instance v3, Lb6/x1$b;

    invoke-direct {v3, v0, v2, p2}, Lb6/x1$b;-><init>(Lb6/b2;ZLjava/lang/Throwable;)V

    sget-object v4, Lb6/x1;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v4, p0, p1, v3}, Landroidx/concurrent/futures/b;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    return v2

    :cond_5
    invoke-direct {p0, v0, p2}, Lb6/x1;->h0(Lb6/b2;Ljava/lang/Throwable;)V

    return v1
.end method

.method private final x0(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    instance-of v0, p1, Lb6/l1;

    if-nez v0, :cond_0

    invoke-static {}, Lb6/y1;->a()Lkotlinx/coroutines/internal/e0;

    move-result-object p1

    return-object p1

    :cond_0
    instance-of v0, p1, Lb6/a1;

    if-nez v0, :cond_1

    instance-of v0, p1, Lb6/w1;

    if-eqz v0, :cond_3

    :cond_1
    instance-of v0, p1, Lb6/r;

    if-nez v0, :cond_3

    instance-of v0, p2, Lb6/v;

    if-nez v0, :cond_3

    check-cast p1, Lb6/l1;

    invoke-direct {p0, p1, p2}, Lb6/x1;->v0(Lb6/l1;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    return-object p2

    :cond_2
    invoke-static {}, Lb6/y1;->b()Lkotlinx/coroutines/internal/e0;

    move-result-object p1

    return-object p1

    :cond_3
    check-cast p1, Lb6/l1;

    invoke-direct {p0, p1, p2}, Lb6/x1;->y0(Lb6/l1;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method private final y0(Lb6/l1;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    invoke-direct {p0, p1}, Lb6/x1;->U(Lb6/l1;)Lb6/b2;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lb6/y1;->b()Lkotlinx/coroutines/internal/e0;

    move-result-object p1

    return-object p1

    :cond_0
    instance-of v1, p1, Lb6/x1$b;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    move-object v1, p1

    check-cast v1, Lb6/x1$b;

    goto :goto_0

    :cond_1
    move-object v1, v2

    :goto_0
    const/4 v3, 0x0

    if-nez v1, :cond_2

    new-instance v1, Lb6/x1$b;

    invoke-direct {v1, v0, v3, v2}, Lb6/x1$b;-><init>(Lb6/b2;ZLjava/lang/Throwable;)V

    :cond_2
    new-instance v4, Lkotlin/jvm/internal/o;

    invoke-direct {v4}, Lkotlin/jvm/internal/o;-><init>()V

    monitor-enter v1

    :try_start_0
    invoke-virtual {v1}, Lb6/x1$b;->g()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {}, Lb6/y1;->a()Lkotlinx/coroutines/internal/e0;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object p1

    :cond_3
    const/4 v5, 0x1

    :try_start_1
    invoke-virtual {v1, v5}, Lb6/x1$b;->k(Z)V

    if-eq v1, p1, :cond_4

    sget-object v6, Lb6/x1;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v6, p0, p1, v1}, Landroidx/concurrent/futures/b;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-static {}, Lb6/y1;->b()Lkotlinx/coroutines/internal/e0;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object p1

    :cond_4
    :try_start_2
    invoke-static {}, Lb6/o0;->a()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v1}, Lb6/x1$b;->i()Z

    move-result v6

    xor-int/2addr v6, v5

    if-eqz v6, :cond_5

    goto :goto_1

    :cond_5
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_6
    :goto_1
    invoke-virtual {v1}, Lb6/x1$b;->f()Z

    move-result v6

    instance-of v7, p2, Lb6/v;

    if-eqz v7, :cond_7

    move-object v7, p2

    check-cast v7, Lb6/v;

    goto :goto_2

    :cond_7
    move-object v7, v2

    :goto_2
    if-eqz v7, :cond_8

    iget-object v7, v7, Lb6/v;->a:Ljava/lang/Throwable;

    invoke-virtual {v1, v7}, Lb6/x1$b;->b(Ljava/lang/Throwable;)V

    :cond_8
    invoke-virtual {v1}, Lb6/x1$b;->e()Ljava/lang/Throwable;

    move-result-object v7

    if-nez v6, :cond_9

    const/4 v3, 0x1

    :cond_9
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_a

    move-object v2, v7

    :cond_a
    iput-object v2, v4, Lkotlin/jvm/internal/o;->f:Ljava/lang/Object;

    sget-object v3, Lk5/s;->a:Lk5/s;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    move-object v3, v2

    check-cast v3, Ljava/lang/Throwable;

    if-eqz v2, :cond_b

    invoke-direct {p0, v0, v2}, Lb6/x1;->h0(Lb6/b2;Ljava/lang/Throwable;)V

    :cond_b
    invoke-direct {p0, p1}, Lb6/x1;->P(Lb6/l1;)Lb6/r;

    move-result-object p1

    if-eqz p1, :cond_c

    invoke-direct {p0, v1, p1, p2}, Lb6/x1;->z0(Lb6/x1$b;Lb6/r;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_c

    sget-object p1, Lb6/y1;->b:Lkotlinx/coroutines/internal/e0;

    return-object p1

    :cond_c
    invoke-direct {p0, v1, p2}, Lb6/x1;->O(Lb6/x1$b;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v1

    throw p1
.end method

.method public static final synthetic z(Lb6/x1;)Ljava/lang/String;
    .locals 0

    invoke-virtual {p0}, Lb6/x1;->J()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final z0(Lb6/x1$b;Lb6/r;Ljava/lang/Object;)Z
    .locals 6

    :cond_0
    iget-object v0, p2, Lb6/r;->j:Lb6/s;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Lb6/x1$a;

    invoke-direct {v3, p0, p1, p2, p3}, Lb6/x1$a;-><init>(Lb6/x1;Lb6/x1$b;Lb6/r;Ljava/lang/Object;)V

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lb6/q1$a;->d(Lb6/q1;ZZLu5/l;ILjava/lang/Object;)Lb6/z0;

    move-result-object v0

    sget-object v1, Lb6/c2;->f:Lb6/c2;

    if-eq v0, v1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    invoke-direct {p0, p2}, Lb6/x1;->g0(Lkotlinx/coroutines/internal/r;)Lb6/r;

    move-result-object p2

    if-nez p2, :cond_0

    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method protected E(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public final F(Ljava/lang/Object;)Z
    .locals 3

    invoke-static {}, Lb6/y1;->a()Lkotlinx/coroutines/internal/e0;

    move-result-object v0

    invoke-virtual {p0}, Lb6/x1;->T()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lb6/x1;->H(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lb6/y1;->b:Lkotlinx/coroutines/internal/e0;

    if-ne v0, v1, :cond_0

    return v2

    :cond_0
    invoke-static {}, Lb6/y1;->a()Lkotlinx/coroutines/internal/e0;

    move-result-object v1

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, Lb6/x1;->c0(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :cond_1
    invoke-static {}, Lb6/y1;->a()Lkotlinx/coroutines/internal/e0;

    move-result-object p1

    if-ne v0, p1, :cond_2

    goto :goto_0

    :cond_2
    sget-object p1, Lb6/y1;->b:Lkotlinx/coroutines/internal/e0;

    if-ne v0, p1, :cond_3

    goto :goto_0

    :cond_3
    invoke-static {}, Lb6/y1;->f()Lkotlinx/coroutines/internal/e0;

    move-result-object p1

    if-ne v0, p1, :cond_4

    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v0}, Lb6/x1;->E(Ljava/lang/Object;)V

    :goto_0
    return v2
.end method

.method public G(Ljava/lang/Throwable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lb6/x1;->F(Ljava/lang/Object;)Z

    return-void
.end method

.method protected J()Ljava/lang/String;
    .locals 1

    const-string v0, "Job was cancelled"

    return-object v0
.end method

.method public K(Ljava/lang/Throwable;)Z
    .locals 2

    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0, p1}, Lb6/x1;->F(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lb6/x1;->S()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public S()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public T()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final V()Lb6/q;
    .locals 1

    iget-object v0, p0, Lb6/x1;->_parentHandle:Ljava/lang/Object;

    check-cast v0, Lb6/q;

    return-object v0
.end method

.method public final W()Ljava/lang/Object;
    .locals 2

    :goto_0
    iget-object v0, p0, Lb6/x1;->_state:Ljava/lang/Object;

    instance-of v1, v0, Lkotlinx/coroutines/internal/z;

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    check-cast v0, Lkotlinx/coroutines/internal/z;

    invoke-virtual {v0, p0}, Lkotlinx/coroutines/internal/z;->c(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected X(Ljava/lang/Throwable;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public Y(Ljava/lang/Throwable;)V
    .locals 0

    throw p1
.end method

.method protected final Z(Lb6/q1;)V
    .locals 1

    invoke-static {}, Lb6/o0;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lb6/x1;->V()Lb6/q;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_2
    :goto_1
    if-nez p1, :cond_3

    sget-object p1, Lb6/c2;->f:Lb6/c2;

    invoke-virtual {p0, p1}, Lb6/x1;->p0(Lb6/q;)V

    return-void

    :cond_3
    invoke-interface {p1}, Lb6/q1;->d()Z

    invoke-interface {p1, p0}, Lb6/q1;->t(Lb6/s;)Lb6/q;

    move-result-object p1

    invoke-virtual {p0, p1}, Lb6/x1;->p0(Lb6/q;)V

    invoke-virtual {p0}, Lb6/x1;->a0()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Lb6/z0;->b()V

    sget-object p1, Lb6/c2;->f:Lb6/c2;

    invoke-virtual {p0, p1}, Lb6/x1;->p0(Lb6/q;)V

    :cond_4
    return-void
.end method

.method public a()Z
    .locals 2

    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lb6/l1;

    if-eqz v1, :cond_0

    check-cast v0, Lb6/l1;

    invoke-interface {v0}, Lb6/l1;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final a0()Z
    .locals 1

    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lb6/l1;

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public b(Ln5/g$c;)Ln5/g$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ln5/g$b;",
            ">(",
            "Ln5/g$c<",
            "TE;>;)TE;"
        }
    .end annotation

    invoke-static {p0, p1}, Lb6/q1$a;->c(Lb6/q1;Ln5/g$c;)Ln5/g$b;

    move-result-object p1

    return-object p1
.end method

.method protected b0()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(Ln5/g;)Ln5/g;
    .locals 0

    invoke-static {p0, p1}, Lb6/q1$a;->f(Lb6/q1;Ln5/g;)Ln5/g;

    move-result-object p1

    return-object p1
.end method

.method public final d()Z
    .locals 2

    :goto_0
    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lb6/x1;->q0(Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public final d0(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    :cond_0
    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lb6/x1;->x0(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {}, Lb6/y1;->a()Lkotlinx/coroutines/internal/e0;

    move-result-object v1

    if-eq v0, v1, :cond_1

    invoke-static {}, Lb6/y1;->b()Lkotlinx/coroutines/internal/e0;

    move-result-object v1

    if-eq v0, v1, :cond_0

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Job "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " is already complete or completing, but is being completed with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, Lb6/x1;->Q(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public f0()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lb6/p0;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getKey()Ln5/g$c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ln5/g$c<",
            "*>;"
        }
    .end annotation

    sget-object v0, Lb6/q1;->b:Lb6/q1$b;

    return-object v0
.end method

.method public i(Ln5/g$c;)Ln5/g;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/g$c<",
            "*>;)",
            "Ln5/g;"
        }
    .end annotation

    invoke-static {p0, p1}, Lb6/q1$a;->e(Lb6/q1;Ln5/g$c;)Ln5/g;

    move-result-object p1

    return-object p1
.end method

.method protected j0(Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method protected k0(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method protected l0()V
    .locals 0

    return-void
.end method

.method public o(Ljava/lang/Object;Lu5/p;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Lu5/p<",
            "-TR;-",
            "Ln5/g$b;",
            "+TR;>;)TR;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Lb6/q1$a;->b(Lb6/q1;Ljava/lang/Object;Lu5/p;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final o0(Lb6/w1;)V
    .locals 3

    :cond_0
    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lb6/w1;

    if-eqz v1, :cond_2

    if-eq v0, p1, :cond_1

    return-void

    :cond_1
    sget-object v1, Lb6/x1;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {}, Lb6/y1;->c()Lb6/a1;

    move-result-object v2

    invoke-static {v1, p0, v0, v2}, Landroidx/concurrent/futures/b;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_2
    instance-of v1, v0, Lb6/l1;

    if-eqz v1, :cond_3

    check-cast v0, Lb6/l1;

    invoke-interface {v0}, Lb6/l1;->h()Lb6/b2;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/r;->u()Z

    :cond_3
    return-void
.end method

.method public final p0(Lb6/q;)V
    .locals 0

    iput-object p1, p0, Lb6/x1;->_parentHandle:Ljava/lang/Object;

    return-void
.end method

.method public final r(ZZLu5/l;)Lb6/z0;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lu5/l<",
            "-",
            "Ljava/lang/Throwable;",
            "Lk5/s;",
            ">;)",
            "Lb6/z0;"
        }
    .end annotation

    invoke-direct {p0, p3, p1}, Lb6/x1;->e0(Lu5/l;Z)Lb6/w1;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lb6/a1;

    if-eqz v2, :cond_2

    move-object v2, v1

    check-cast v2, Lb6/a1;

    invoke-virtual {v2}, Lb6/a1;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v2, Lb6/x1;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v2, p0, v1, v0}, Landroidx/concurrent/futures/b;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    invoke-direct {p0, v2}, Lb6/x1;->m0(Lb6/a1;)V

    goto :goto_0

    :cond_2
    instance-of v2, v1, Lb6/l1;

    const/4 v3, 0x0

    if-eqz v2, :cond_b

    move-object v2, v1

    check-cast v2, Lb6/l1;

    invoke-interface {v2}, Lb6/l1;->h()Lb6/b2;

    move-result-object v2

    if-nez v2, :cond_3

    const-string v2, "null cannot be cast to non-null type kotlinx.coroutines.JobNode"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v1, Lb6/w1;

    invoke-direct {p0, v1}, Lb6/x1;->n0(Lb6/w1;)V

    goto :goto_0

    :cond_3
    sget-object v4, Lb6/c2;->f:Lb6/c2;

    if-eqz p1, :cond_8

    instance-of v5, v1, Lb6/x1$b;

    if-eqz v5, :cond_8

    monitor-enter v1

    :try_start_0
    move-object v3, v1

    check-cast v3, Lb6/x1$b;

    invoke-virtual {v3}, Lb6/x1$b;->e()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_4

    instance-of v5, p3, Lb6/r;

    if-eqz v5, :cond_7

    move-object v5, v1

    check-cast v5, Lb6/x1$b;

    invoke-virtual {v5}, Lb6/x1$b;->g()Z

    move-result v5

    if-nez v5, :cond_7

    :cond_4
    invoke-direct {p0, v1, v2, v0}, Lb6/x1;->C(Ljava/lang/Object;Lb6/b2;Lb6/w1;)Z

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_5

    monitor-exit v1

    goto :goto_0

    :cond_5
    if-nez v3, :cond_6

    monitor-exit v1

    return-object v0

    :cond_6
    move-object v4, v0

    :cond_7
    :try_start_1
    sget-object v5, Lk5/s;->a:Lk5/s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v1

    throw p1

    :cond_8
    :goto_1
    if-eqz v3, :cond_a

    if-eqz p2, :cond_9

    invoke-interface {p3, v3}, Lu5/l;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    return-object v4

    :cond_a
    invoke-direct {p0, v1, v2, v0}, Lb6/x1;->C(Ljava/lang/Object;Lb6/b2;Lb6/w1;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_b
    if-eqz p2, :cond_e

    instance-of p1, v1, Lb6/v;

    if-eqz p1, :cond_c

    check-cast v1, Lb6/v;

    goto :goto_2

    :cond_c
    move-object v1, v3

    :goto_2
    if-eqz v1, :cond_d

    iget-object v3, v1, Lb6/v;->a:Ljava/lang/Throwable;

    :cond_d
    invoke-interface {p3, v3}, Lu5/l;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_e
    sget-object p1, Lb6/c2;->f:Lb6/c2;

    return-object p1
.end method

.method public s()Ljava/util/concurrent/CancellationException;
    .locals 5

    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lb6/x1$b;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lb6/x1$b;

    invoke-virtual {v1}, Lb6/x1$b;->e()Ljava/lang/Throwable;

    move-result-object v1

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lb6/v;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lb6/v;

    iget-object v1, v1, Lb6/v;->a:Ljava/lang/Throwable;

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lb6/l1;

    if-nez v1, :cond_4

    move-object v1, v2

    :goto_0
    instance-of v3, v1, Ljava/util/concurrent/CancellationException;

    if-eqz v3, :cond_2

    move-object v2, v1

    check-cast v2, Ljava/util/concurrent/CancellationException;

    :cond_2
    if-nez v2, :cond_3

    new-instance v2, Lb6/r1;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Parent job is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lb6/x1;->r0(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1, p0}, Lb6/r1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lb6/q1;)V

    :cond_3
    return-object v2

    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot be cancelling child in this state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected final s0(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/util/concurrent/CancellationException;
    .locals 1

    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/util/concurrent/CancellationException;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Lb6/r1;

    if-nez p2, :cond_1

    invoke-static {p0}, Lb6/x1;->z(Lb6/x1;)Ljava/lang/String;

    move-result-object p2

    :cond_1
    invoke-direct {v0, p2, p1, p0}, Lb6/r1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lb6/q1;)V

    :cond_2
    return-object v0
.end method

.method public final t(Lb6/s;)Lb6/q;
    .locals 6

    new-instance v3, Lb6/r;

    invoke-direct {v3, p1}, Lb6/r;-><init>(Lb6/s;)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lb6/q1$a;->d(Lb6/q1;ZZLu5/l;ILjava/lang/Object;)Lb6/z0;

    move-result-object p1

    check-cast p1, Lb6/q;

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lb6/x1;->u0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lb6/p0;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/util/concurrent/CancellationException;
    .locals 4

    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lb6/x1$b;

    const-string v2, "Job is still new or active: "

    if-eqz v1, :cond_1

    check-cast v0, Lb6/x1$b;

    invoke-virtual {v0}, Lb6/x1$b;->e()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lb6/p0;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " is cancelling"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lb6/x1;->s0(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/util/concurrent/CancellationException;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    instance-of v1, v0, Lb6/l1;

    if-nez v1, :cond_3

    instance-of v1, v0, Lb6/v;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast v0, Lb6/v;

    iget-object v0, v0, Lb6/v;->a:Ljava/lang/Throwable;

    const/4 v1, 0x1

    invoke-static {p0, v0, v2, v1, v2}, Lb6/x1;->t0(Lb6/x1;Ljava/lang/Throwable;Ljava/lang/String;ILjava/lang/Object;)Ljava/util/concurrent/CancellationException;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Lb6/r1;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lb6/p0;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " has completed normally"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2, p0}, Lb6/r1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lb6/q1;)V

    :goto_0
    return-object v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final u0()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lb6/x1;->f0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lb6/x1;->W()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, Lb6/x1;->r0(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v(Lb6/e2;)V
    .locals 0

    invoke-virtual {p0, p1}, Lb6/x1;->F(Ljava/lang/Object;)Z

    return-void
.end method

.method public x(Ljava/util/concurrent/CancellationException;)V
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    new-instance v0, Lb6/r1;

    invoke-static {p0}, Lb6/x1;->z(Lb6/x1;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0}, Lb6/r1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lb6/q1;)V

    move-object p1, v0

    :cond_0
    invoke-virtual {p0, p1}, Lb6/x1;->G(Ljava/lang/Throwable;)V

    return-void
.end method
