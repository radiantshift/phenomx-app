.class public abstract Lb6/a;
.super Lb6/x1;
.source ""

# interfaces
.implements Ln5/d;
.implements Lb6/k0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lb6/x1;",
        "Ln5/d<",
        "TT;>;",
        "Lb6/k0;"
    }
.end annotation


# instance fields
.field private final g:Ln5/g;


# direct methods
.method public constructor <init>(Ln5/g;ZZ)V
    .locals 0

    invoke-direct {p0, p3}, Lb6/x1;-><init>(Z)V

    if-eqz p2, :cond_0

    sget-object p2, Lb6/q1;->b:Lb6/q1$b;

    invoke-interface {p1, p2}, Ln5/g;->b(Ln5/g$c;)Ln5/g$b;

    move-result-object p2

    check-cast p2, Lb6/q1;

    invoke-virtual {p0, p2}, Lb6/x1;->Z(Lb6/q1;)V

    :cond_0
    invoke-interface {p1, p0}, Ln5/g;->c(Ln5/g;)Ln5/g;

    move-result-object p1

    iput-object p1, p0, Lb6/a;->g:Ln5/g;

    return-void
.end method


# virtual methods
.method protected A0(Ljava/lang/Object;)V
    .locals 0

    invoke-virtual {p0, p1}, Lb6/x1;->E(Ljava/lang/Object;)V

    return-void
.end method

.method protected B0(Ljava/lang/Throwable;Z)V
    .locals 0

    return-void
.end method

.method protected C0(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    return-void
.end method

.method public final D0(Lb6/m0;Ljava/lang/Object;Lu5/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lb6/m0;",
            "TR;",
            "Lu5/p<",
            "-TR;-",
            "Ln5/d<",
            "-TT;>;+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1, p3, p2, p0}, Lb6/m0;->b(Lu5/p;Ljava/lang/Object;Ln5/d;)V

    return-void
.end method

.method protected J()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lb6/p0;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " was cancelled"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Y(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lb6/a;->g:Ln5/g;

    invoke-static {v0, p1}, Lb6/g0;->a(Ln5/g;Ljava/lang/Throwable;)V

    return-void
.end method

.method public a()Z
    .locals 1

    invoke-super {p0}, Lb6/x1;->a()Z

    move-result v0

    return v0
.end method

.method public f0()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lb6/a;->g:Ln5/g;

    invoke-static {v0}, Lb6/c0;->b(Ln5/g;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lb6/x1;->f0()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x22

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-super {p0}, Lb6/x1;->f0()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Ln5/g;
    .locals 1

    iget-object v0, p0, Lb6/a;->g:Ln5/g;

    return-object v0
.end method

.method public final getContext()Ln5/g;
    .locals 1

    iget-object v0, p0, Lb6/a;->g:Ln5/g;

    return-object v0
.end method

.method protected final k0(Ljava/lang/Object;)V
    .locals 1

    instance-of v0, p1, Lb6/v;

    if-eqz v0, :cond_0

    check-cast p1, Lb6/v;

    iget-object v0, p1, Lb6/v;->a:Ljava/lang/Throwable;

    invoke-virtual {p1}, Lb6/v;->a()Z

    move-result p1

    invoke-virtual {p0, v0, p1}, Lb6/a;->B0(Ljava/lang/Throwable;Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lb6/a;->C0(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public final resumeWith(Ljava/lang/Object;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1, v0}, Lb6/z;->d(Ljava/lang/Object;Lu5/l;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Lb6/x1;->d0(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    sget-object v0, Lb6/y1;->b:Lkotlinx/coroutines/internal/e0;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lb6/a;->A0(Ljava/lang/Object;)V

    return-void
.end method
