.class final synthetic Lb6/v1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static final a(Lb6/q1;)Lb6/t;
    .locals 1

    new-instance v0, Lb6/t1;

    invoke-direct {v0, p0}, Lb6/t1;-><init>(Lb6/q1;)V

    return-object v0
.end method

.method public static synthetic b(Lb6/q1;ILjava/lang/Object;)Lb6/t;
    .locals 0

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    const/4 p0, 0x0

    :cond_0
    invoke-static {p0}, Lb6/u1;->a(Lb6/q1;)Lb6/t;

    move-result-object p0

    return-object p0
.end method

.method public static final c(Ln5/g;Ljava/util/concurrent/CancellationException;)V
    .locals 1

    sget-object v0, Lb6/q1;->b:Lb6/q1$b;

    invoke-interface {p0, v0}, Ln5/g;->b(Ln5/g$c;)Ln5/g$b;

    move-result-object p0

    check-cast p0, Lb6/q1;

    if-eqz p0, :cond_0

    invoke-interface {p0, p1}, Lb6/q1;->x(Ljava/util/concurrent/CancellationException;)V

    :cond_0
    return-void
.end method

.method public static synthetic d(Ln5/g;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-static {p0, p1}, Lb6/u1;->c(Ln5/g;Ljava/util/concurrent/CancellationException;)V

    return-void
.end method

.method public static final e(Lb6/q1;)V
    .locals 1

    invoke-interface {p0}, Lb6/q1;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-interface {p0}, Lb6/q1;->u()Ljava/util/concurrent/CancellationException;

    move-result-object p0

    throw p0
.end method

.method public static final f(Ln5/g;)V
    .locals 1

    sget-object v0, Lb6/q1;->b:Lb6/q1$b;

    invoke-interface {p0, v0}, Ln5/g;->b(Ln5/g$c;)Ln5/g$b;

    move-result-object p0

    check-cast p0, Lb6/q1;

    if-eqz p0, :cond_0

    invoke-static {p0}, Lb6/u1;->e(Lb6/q1;)V

    :cond_0
    return-void
.end method
