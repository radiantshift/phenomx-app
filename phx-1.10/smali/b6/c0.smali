.class public final Lb6/c0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private static final a(Ln5/g;Ln5/g;Z)Ln5/g;
    .locals 3

    invoke-static {p0}, Lb6/c0;->c(Ln5/g;)Z

    move-result v0

    invoke-static {p1}, Lb6/c0;->c(Ln5/g;)Z

    move-result v1

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    invoke-interface {p0, p1}, Ln5/g;->c(Ln5/g;)Ln5/g;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance v0, Lkotlin/jvm/internal/o;

    invoke-direct {v0}, Lkotlin/jvm/internal/o;-><init>()V

    iput-object p1, v0, Lkotlin/jvm/internal/o;->f:Ljava/lang/Object;

    sget-object p1, Ln5/h;->f:Ln5/h;

    new-instance v2, Lb6/c0$b;

    invoke-direct {v2, v0, p2}, Lb6/c0$b;-><init>(Lkotlin/jvm/internal/o;Z)V

    invoke-interface {p0, p1, v2}, Ln5/g;->o(Ljava/lang/Object;Lu5/p;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ln5/g;

    if-eqz v1, :cond_1

    iget-object p2, v0, Lkotlin/jvm/internal/o;->f:Ljava/lang/Object;

    check-cast p2, Ln5/g;

    sget-object v1, Lb6/c0$a;->f:Lb6/c0$a;

    invoke-interface {p2, p1, v1}, Ln5/g;->o(Ljava/lang/Object;Lu5/p;)Ljava/lang/Object;

    move-result-object p1

    iput-object p1, v0, Lkotlin/jvm/internal/o;->f:Ljava/lang/Object;

    :cond_1
    iget-object p1, v0, Lkotlin/jvm/internal/o;->f:Ljava/lang/Object;

    check-cast p1, Ln5/g;

    invoke-interface {p0, p1}, Ln5/g;->c(Ln5/g;)Ln5/g;

    move-result-object p0

    return-object p0
.end method

.method public static final b(Ln5/g;)Ljava/lang/String;
    .locals 4

    invoke-static {}, Lb6/o0;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    sget-object v0, Lb6/i0;->h:Lb6/i0$a;

    invoke-interface {p0, v0}, Ln5/g;->b(Ln5/g$c;)Ln5/g$b;

    move-result-object v0

    check-cast v0, Lb6/i0;

    if-nez v0, :cond_1

    return-object v1

    :cond_1
    sget-object v1, Lb6/j0;->h:Lb6/j0$a;

    invoke-interface {p0, v1}, Ln5/g;->b(Ln5/g$c;)Ln5/g$b;

    move-result-object p0

    check-cast p0, Lb6/j0;

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lb6/j0;->D()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_3

    :cond_2
    const-string p0, "coroutine"

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p0, 0x23

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lb6/i0;->D()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final c(Ln5/g;)Z
    .locals 2

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sget-object v1, Lb6/c0$c;->f:Lb6/c0$c;

    invoke-interface {p0, v0, v1}, Ln5/g;->o(Ljava/lang/Object;Lu5/p;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public static final d(Lb6/k0;Ln5/g;)Ln5/g;
    .locals 2

    invoke-interface {p0}, Lb6/k0;->g()Ln5/g;

    move-result-object p0

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lb6/c0;->a(Ln5/g;Ln5/g;Z)Ln5/g;

    move-result-object p0

    invoke-static {}, Lb6/o0;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lb6/i0;

    invoke-static {}, Lb6/o0;->b()Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    invoke-direct {p1, v0, v1}, Lb6/i0;-><init>(J)V

    invoke-interface {p0, p1}, Ln5/g;->c(Ln5/g;)Ln5/g;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, p0

    :goto_0
    invoke-static {}, Lb6/y0;->a()Lb6/d0;

    move-result-object v0

    if-eq p0, v0, :cond_1

    sget-object v0, Ln5/e;->e:Ln5/e$b;

    invoke-interface {p0, v0}, Ln5/g;->b(Ln5/g$c;)Ln5/g$b;

    move-result-object p0

    if-nez p0, :cond_1

    invoke-static {}, Lb6/y0;->a()Lb6/d0;

    move-result-object p0

    invoke-interface {p1, p0}, Ln5/g;->c(Ln5/g;)Ln5/g;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method public static final e(Lkotlin/coroutines/jvm/internal/e;)Lb6/j2;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/jvm/internal/e;",
            ")",
            "Lb6/j2<",
            "*>;"
        }
    .end annotation

    :cond_0
    instance-of v0, p0, Lb6/u0;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    return-object v1

    :cond_1
    invoke-interface {p0}, Lkotlin/coroutines/jvm/internal/e;->getCallerFrame()Lkotlin/coroutines/jvm/internal/e;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v1

    :cond_2
    instance-of v0, p0, Lb6/j2;

    if-eqz v0, :cond_0

    check-cast p0, Lb6/j2;

    return-object p0
.end method

.method public static final f(Ln5/d;Ln5/g;Ljava/lang/Object;)Lb6/j2;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/d<",
            "*>;",
            "Ln5/g;",
            "Ljava/lang/Object;",
            ")",
            "Lb6/j2<",
            "*>;"
        }
    .end annotation

    instance-of v0, p0, Lkotlin/coroutines/jvm/internal/e;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    sget-object v0, Lb6/k2;->f:Lb6/k2;

    invoke-interface {p1, v0}, Ln5/g;->b(Ln5/g$c;)Ln5/g$b;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    return-object v1

    :cond_2
    check-cast p0, Lkotlin/coroutines/jvm/internal/e;

    invoke-static {p0}, Lb6/c0;->e(Lkotlin/coroutines/jvm/internal/e;)Lb6/j2;

    move-result-object p0

    if-eqz p0, :cond_3

    invoke-virtual {p0, p1, p2}, Lb6/j2;->G0(Ln5/g;Ljava/lang/Object;)V

    :cond_3
    return-object p0
.end method
