.class public final Lb6/j2;
.super Lkotlinx/coroutines/internal/c0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lkotlinx/coroutines/internal/c0<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private i:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lk5/l<",
            "Ln5/g;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# virtual methods
.method protected A0(Ljava/lang/Object;)V
    .locals 5

    iget-object v0, p0, Lb6/j2;->i:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lk5/l;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lk5/l;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ln5/g;

    invoke-virtual {v0}, Lk5/l;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2, v0}, Lkotlinx/coroutines/internal/i0;->a(Ln5/g;Ljava/lang/Object;)V

    iget-object v0, p0, Lb6/j2;->i:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lkotlinx/coroutines/internal/c0;->h:Ln5/d;

    invoke-static {p1, v0}, Lb6/z;->a(Ljava/lang/Object;Ln5/d;)Ljava/lang/Object;

    move-result-object p1

    iget-object v0, p0, Lkotlinx/coroutines/internal/c0;->h:Ln5/d;

    invoke-interface {v0}, Ln5/d;->getContext()Ln5/g;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlinx/coroutines/internal/i0;->c(Ln5/g;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    sget-object v4, Lkotlinx/coroutines/internal/i0;->a:Lkotlinx/coroutines/internal/e0;

    if-eq v3, v4, :cond_1

    invoke-static {v0, v2, v3}, Lb6/c0;->f(Ln5/d;Ln5/g;Ljava/lang/Object;)Lb6/j2;

    move-result-object v1

    :cond_1
    :try_start_0
    iget-object v0, p0, Lkotlinx/coroutines/internal/c0;->h:Ln5/d;

    invoke-interface {v0, p1}, Ln5/d;->resumeWith(Ljava/lang/Object;)V

    sget-object p1, Lk5/s;->a:Lk5/s;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lb6/j2;->F0()Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_2
    invoke-static {v2, v3}, Lkotlinx/coroutines/internal/i0;->a(Ln5/g;Ljava/lang/Object;)V

    :cond_3
    return-void

    :catchall_0
    move-exception p1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lb6/j2;->F0()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    invoke-static {v2, v3}, Lkotlinx/coroutines/internal/i0;->a(Ln5/g;Ljava/lang/Object;)V

    :cond_5
    throw p1
.end method

.method public final F0()Z
    .locals 2

    iget-object v0, p0, Lb6/j2;->i:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lb6/j2;->i:Ljava/lang/ThreadLocal;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final G0(Ln5/g;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lb6/j2;->i:Ljava/lang/ThreadLocal;

    invoke-static {p1, p2}, Lk5/p;->a(Ljava/lang/Object;Ljava/lang/Object;)Lk5/l;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    return-void
.end method
