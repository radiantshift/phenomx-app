.class public final Lb6/i2;
.super Lb6/d0;
.source ""


# static fields
.field public static final h:Lb6/i2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lb6/i2;

    invoke-direct {v0}, Lb6/i2;-><init>()V

    sput-object v0, Lb6/i2;->h:Lb6/i2;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lb6/d0;-><init>()V

    return-void
.end method


# virtual methods
.method public D(Ln5/g;Ljava/lang/Runnable;)V
    .locals 0

    sget-object p2, Lb6/l2;->h:Lb6/l2$a;

    invoke-interface {p1, p2}, Ln5/g;->b(Ln5/g$c;)Ln5/g$b;

    move-result-object p1

    check-cast p1, Lb6/l2;

    if-eqz p1, :cond_0

    const/4 p2, 0x1

    iput-boolean p2, p1, Lb6/l2;->g:Z

    return-void

    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Dispatchers.Unconfined.dispatch function can only be used by the yield function. If you wrap Unconfined dispatcher in your code, make sure you properly delegate isDispatchNeeded and dispatch calls."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public E(Ln5/g;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "Dispatchers.Unconfined"

    return-object v0
.end method
