.class final Lb6/o1;
.super Lb6/s1;
.source ""


# static fields
.field private static final synthetic k:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;


# instance fields
.field private volatile synthetic _invoked:I

.field private final j:Lu5/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lu5/l<",
            "Ljava/lang/Throwable;",
            "Lk5/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lb6/o1;

    const-string v1, "_invoked"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, Lb6/o1;->k:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    return-void
.end method

.method public constructor <init>(Lu5/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu5/l<",
            "-",
            "Ljava/lang/Throwable;",
            "Lk5/s;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lb6/s1;-><init>()V

    iput-object p1, p0, Lb6/o1;->j:Lu5/l;

    const/4 p1, 0x0

    iput p1, p0, Lb6/o1;->_invoked:I

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lb6/o1;->y(Ljava/lang/Throwable;)V

    sget-object p1, Lk5/s;->a:Lk5/s;

    return-object p1
.end method

.method public y(Ljava/lang/Throwable;)V
    .locals 3

    sget-object v0, Lb6/o1;->k:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb6/o1;->j:Lu5/l;

    invoke-interface {v0, p1}, Lu5/l;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
