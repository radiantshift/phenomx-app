.class public abstract Lb6/d0;
.super Ln5/a;
.source ""

# interfaces
.implements Ln5/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb6/d0$a;
    }
.end annotation


# static fields
.field public static final g:Lb6/d0$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lb6/d0$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lb6/d0$a;-><init>(Lkotlin/jvm/internal/e;)V

    sput-object v0, Lb6/d0;->g:Lb6/d0$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Ln5/e;->e:Ln5/e$b;

    invoke-direct {p0, v0}, Ln5/a;-><init>(Ln5/g$c;)V

    return-void
.end method


# virtual methods
.method public abstract D(Ln5/g;Ljava/lang/Runnable;)V
.end method

.method public E(Ln5/g;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public F(I)Lb6/d0;
    .locals 1

    invoke-static {p1}, Lkotlinx/coroutines/internal/o;->a(I)V

    new-instance v0, Lkotlinx/coroutines/internal/n;

    invoke-direct {v0, p0, p1}, Lkotlinx/coroutines/internal/n;-><init>(Lb6/d0;I)V

    return-object v0
.end method

.method public b(Ln5/g$c;)Ln5/g$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ln5/g$b;",
            ">(",
            "Ln5/g$c<",
            "TE;>;)TE;"
        }
    .end annotation

    invoke-static {p0, p1}, Ln5/e$a;->a(Ln5/e;Ln5/g$c;)Ln5/g$b;

    move-result-object p1

    return-object p1
.end method

.method public final h(Ln5/d;)Ln5/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ln5/d<",
            "-TT;>;)",
            "Ln5/d<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lkotlinx/coroutines/internal/h;

    invoke-direct {v0, p0, p1}, Lkotlinx/coroutines/internal/h;-><init>(Lb6/d0;Ln5/d;)V

    return-object v0
.end method

.method public i(Ln5/g$c;)Ln5/g;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/g$c<",
            "*>;)",
            "Ln5/g;"
        }
    .end annotation

    invoke-static {p0, p1}, Ln5/e$a;->b(Ln5/e;Ln5/g$c;)Ln5/g;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lb6/p0;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lb6/p0;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final y(Ln5/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/d<",
            "*>;)V"
        }
    .end annotation

    check-cast p1, Lkotlinx/coroutines/internal/h;

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/h;->r()V

    return-void
.end method
