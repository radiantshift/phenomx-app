.class public abstract Ly0/o;
.super Lh0/f;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ly0/o$a;,
        Ly0/o$c;,
        Ly0/o$b;
    }
.end annotation


# static fields
.field private static final H0:[B


# instance fields
.field private final A:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private A0:Z

.field private final B:Landroid/media/MediaCodec$BufferInfo;

.field private B0:Z

.field private final C:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Ly0/o$c;",
            ">;"
        }
    .end annotation
.end field

.field private C0:Lh0/q;

.field private D:Lh0/r1;

.field protected D0:Lk0/e;

.field private E:Lh0/r1;

.field private E0:Ly0/o$c;

.field private F:Ll0/o;

.field private F0:J

.field private G:Ll0/o;

.field private G0:Z

.field private H:Landroid/media/MediaCrypto;

.field private I:Z

.field private J:J

.field private K:F

.field private L:F

.field private M:Ly0/l;

.field private N:Lh0/r1;

.field private O:Landroid/media/MediaFormat;

.field private P:Z

.field private Q:F

.field private R:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Ly0/n;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ly0/o$b;

.field private T:Ly0/n;

.field private U:I

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private a0:Z

.field private b0:Z

.field private c0:Z

.field private d0:Z

.field private e0:Z

.field private f0:Ly0/i;

.field private g0:J

.field private h0:I

.field private i0:I

.field private j0:Ljava/nio/ByteBuffer;

.field private k0:Z

.field private l0:Z

.field private m0:Z

.field private n0:Z

.field private o0:Z

.field private p0:Z

.field private q0:I

.field private r0:I

.field private final s:Ly0/l$b;

.field private s0:I

.field private final t:Ly0/q;

.field private t0:Z

.field private final u:Z

.field private u0:Z

.field private final v:F

.field private v0:Z

.field private final w:Lk0/g;

.field private w0:J

.field private final x:Lk0/g;

.field private x0:J

.field private final y:Lk0/g;

.field private y0:Z

.field private final z:Ly0/h;

.field private z0:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x26

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Ly0/o;->H0:[B

    return-void

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x1t
        0x67t
        0x42t
        -0x40t
        0xbt
        -0x26t
        0x25t
        -0x70t
        0x0t
        0x0t
        0x1t
        0x68t
        -0x32t
        0xft
        0x13t
        0x20t
        0x0t
        0x0t
        0x1t
        0x65t
        -0x78t
        -0x7ct
        0xdt
        -0x32t
        0x71t
        0x18t
        -0x60t
        0x0t
        0x2ft
        -0x41t
        0x1ct
        0x31t
        -0x3dt
        0x27t
        0x5dt
        0x78t
    .end array-data
.end method

.method public constructor <init>(ILy0/l$b;Ly0/q;ZF)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/f;-><init>(I)V

    iput-object p2, p0, Ly0/o;->s:Ly0/l$b;

    invoke-static {p3}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ly0/q;

    iput-object p1, p0, Ly0/o;->t:Ly0/q;

    iput-boolean p4, p0, Ly0/o;->u:Z

    iput p5, p0, Ly0/o;->v:F

    invoke-static {}, Lk0/g;->s()Lk0/g;

    move-result-object p1

    iput-object p1, p0, Ly0/o;->w:Lk0/g;

    new-instance p1, Lk0/g;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lk0/g;-><init>(I)V

    iput-object p1, p0, Ly0/o;->x:Lk0/g;

    new-instance p1, Lk0/g;

    const/4 p3, 0x2

    invoke-direct {p1, p3}, Lk0/g;-><init>(I)V

    iput-object p1, p0, Ly0/o;->y:Lk0/g;

    new-instance p1, Ly0/h;

    invoke-direct {p1}, Ly0/h;-><init>()V

    iput-object p1, p0, Ly0/o;->z:Ly0/h;

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    iput-object p3, p0, Ly0/o;->A:Ljava/util/ArrayList;

    new-instance p3, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {p3}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object p3, p0, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    const/high16 p3, 0x3f800000    # 1.0f

    iput p3, p0, Ly0/o;->K:F

    iput p3, p0, Ly0/o;->L:F

    const-wide p3, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide p3, p0, Ly0/o;->J:J

    new-instance p5, Ljava/util/ArrayDeque;

    invoke-direct {p5}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p5, p0, Ly0/o;->C:Ljava/util/ArrayDeque;

    sget-object p5, Ly0/o$c;->e:Ly0/o$c;

    invoke-direct {p0, p5}, Ly0/o;->h1(Ly0/o$c;)V

    invoke-virtual {p1, p2}, Lk0/g;->p(I)V

    iget-object p1, p1, Lk0/g;->h:Ljava/nio/ByteBuffer;

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object p5

    invoke-virtual {p1, p5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    const/high16 p1, -0x40800000    # -1.0f

    iput p1, p0, Ly0/o;->Q:F

    iput p2, p0, Ly0/o;->U:I

    iput p2, p0, Ly0/o;->q0:I

    const/4 p1, -0x1

    iput p1, p0, Ly0/o;->h0:I

    iput p1, p0, Ly0/o;->i0:I

    iput-wide p3, p0, Ly0/o;->g0:J

    iput-wide p3, p0, Ly0/o;->w0:J

    iput-wide p3, p0, Ly0/o;->x0:J

    iput-wide p3, p0, Ly0/o;->F0:J

    iput p2, p0, Ly0/o;->r0:I

    iput p2, p0, Ly0/o;->s0:I

    return-void
.end method

.method private D0()Z
    .locals 1

    iget v0, p0, Ly0/o;->i0:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private E0(Lh0/r1;)V
    .locals 2

    invoke-direct {p0}, Ly0/o;->h0()V

    iget-object p1, p1, Lh0/r1;->q:Ljava/lang/String;

    const-string v0, "audio/mp4a-latm"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const-string v0, "audio/mpeg"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "audio/opus"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Ly0/o;->z:Ly0/h;

    invoke-virtual {p1, v1}, Ly0/h;->A(I)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Ly0/o;->z:Ly0/h;

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ly0/h;->A(I)V

    :goto_0
    iput-boolean v1, p0, Ly0/o;->m0:Z

    return-void
.end method

.method private F0(Ly0/n;Landroid/media/MediaCrypto;)V
    .locals 12

    iget-object v1, p1, Ly0/n;->a:Ljava/lang/String;

    sget v0, Le2/n0;->a:I

    const/high16 v2, -0x40800000    # -1.0f

    const/16 v3, 0x17

    if-ge v0, v3, :cond_0

    const/high16 v3, -0x40800000    # -1.0f

    goto :goto_0

    :cond_0
    iget v3, p0, Ly0/o;->L:F

    iget-object v4, p0, Ly0/o;->D:Lh0/r1;

    invoke-virtual {p0}, Lh0/f;->I()[Lh0/r1;

    move-result-object v5

    invoke-virtual {p0, v3, v4, v5}, Ly0/o;->v0(FLh0/r1;[Lh0/r1;)F

    move-result v3

    :goto_0
    iget v4, p0, Ly0/o;->v:F

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iget-object v5, p0, Ly0/o;->D:Lh0/r1;

    invoke-virtual {p0, p1, v5, p2, v2}, Ly0/o;->z0(Ly0/n;Lh0/r1;Landroid/media/MediaCrypto;F)Ly0/l$a;

    move-result-object p2

    const/16 v5, 0x1f

    if-lt v0, v5, :cond_2

    invoke-virtual {p0}, Lh0/f;->H()Li0/u1;

    move-result-object v0

    invoke-static {p2, v0}, Ly0/o$a;->a(Ly0/l$a;Li0/u1;)V

    :cond_2
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createCodec:"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Le2/k0;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ly0/o;->s:Ly0/l$b;

    invoke-interface {v0, p2}, Ly0/l$b;->a(Ly0/l$a;)Ly0/l;

    move-result-object v0

    iput-object v0, p0, Ly0/o;->M:Ly0/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Le2/k0;->c()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-object v0, p0, Ly0/o;->D:Lh0/r1;

    invoke-virtual {p1, v0}, Ly0/n;->o(Lh0/r1;)Z

    move-result v0

    const/4 v7, 0x2

    const/4 v8, 0x0

    const/4 v9, 0x1

    if-nez v0, :cond_3

    new-array v0, v7, [Ljava/lang/Object;

    iget-object v10, p0, Ly0/o;->D:Lh0/r1;

    invoke-static {v10}, Lh0/r1;->i(Lh0/r1;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v0, v8

    aput-object v1, v0, v9

    const-string v10, "Format exceeds selected codec\'s capabilities [%s, %s]"

    invoke-static {v10, v0}, Le2/n0;->C(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v10, "MediaCodecRenderer"

    invoke-static {v10, v0}, Le2/r;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iput-object p1, p0, Ly0/o;->T:Ly0/n;

    iput v2, p0, Ly0/o;->Q:F

    iget-object v0, p0, Ly0/o;->D:Lh0/r1;

    iput-object v0, p0, Ly0/o;->N:Lh0/r1;

    invoke-direct {p0, v1}, Ly0/o;->X(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ly0/o;->U:I

    iget-object v0, p0, Ly0/o;->N:Lh0/r1;

    invoke-static {v1, v0}, Ly0/o;->Y(Ljava/lang/String;Lh0/r1;)Z

    move-result v0

    iput-boolean v0, p0, Ly0/o;->V:Z

    invoke-static {v1}, Ly0/o;->d0(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ly0/o;->W:Z

    invoke-static {v1}, Ly0/o;->f0(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ly0/o;->X:Z

    invoke-static {v1}, Ly0/o;->a0(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ly0/o;->Y:Z

    invoke-static {v1}, Ly0/o;->b0(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ly0/o;->Z:Z

    invoke-static {v1}, Ly0/o;->Z(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ly0/o;->a0:Z

    iget-object v0, p0, Ly0/o;->N:Lh0/r1;

    invoke-static {v1, v0}, Ly0/o;->e0(Ljava/lang/String;Lh0/r1;)Z

    move-result v0

    iput-boolean v0, p0, Ly0/o;->b0:Z

    invoke-static {p1}, Ly0/o;->c0(Ly0/n;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Ly0/o;->u0()Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    :cond_5
    :goto_2
    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Ly0/o;->e0:Z

    iget-object v0, p0, Ly0/o;->M:Ly0/l;

    invoke-interface {v0}, Ly0/l;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    iput-boolean v9, p0, Ly0/o;->p0:Z

    iput v9, p0, Ly0/o;->q0:I

    iget v0, p0, Ly0/o;->U:I

    if-eqz v0, :cond_6

    const/4 v8, 0x1

    :cond_6
    iput-boolean v8, p0, Ly0/o;->c0:Z

    :cond_7
    iget-object p1, p1, Ly0/n;->a:Ljava/lang/String;

    const-string v0, "c2.android.mp3.decoder"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    new-instance p1, Ly0/i;

    invoke-direct {p1}, Ly0/i;-><init>()V

    iput-object p1, p0, Ly0/o;->f0:Ly0/i;

    :cond_8
    invoke-virtual {p0}, Lh0/f;->getState()I

    move-result p1

    if-ne p1, v7, :cond_9

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    const-wide/16 v10, 0x3e8

    add-long/2addr v7, v10

    iput-wide v7, p0, Ly0/o;->g0:J

    :cond_9
    iget-object p1, p0, Ly0/o;->D0:Lk0/e;

    iget v0, p1, Lk0/e;->a:I

    add-int/2addr v0, v9

    iput v0, p1, Lk0/e;->a:I

    sub-long v7, v5, v3

    move-object v0, p0

    move-object v2, p2

    move-wide v3, v5

    move-wide v5, v7

    invoke-virtual/range {v0 .. v6}, Ly0/o;->N0(Ljava/lang/String;Ly0/l$a;JJ)V

    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Le2/k0;->c()V

    throw p1
.end method

.method private G0(J)Z
    .locals 6

    iget-object v0, p0, Ly0/o;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    iget-object v3, p0, Ly0/o;->A:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v5, v3, p1

    if-nez v5, :cond_0

    iget-object p1, p0, Ly0/o;->A:Ljava/util/ArrayList;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private static H0(Ljava/lang/IllegalStateException;)Z
    .locals 3

    sget v0, Le2/n0;->a:I

    const/4 v1, 0x1

    const/16 v2, 0x15

    if-lt v0, v2, :cond_0

    invoke-static {p0}, Ly0/o;->I0(Ljava/lang/IllegalStateException;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object p0

    array-length v0, p0

    const/4 v2, 0x0

    if-lez v0, :cond_1

    aget-object p0, p0, v2

    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object p0

    const-string v0, "android.media.MediaCodec"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private static I0(Ljava/lang/IllegalStateException;)Z
    .locals 0

    instance-of p0, p0, Landroid/media/MediaCodec$CodecException;

    return p0
.end method

.method private static J0(Ljava/lang/IllegalStateException;)Z
    .locals 1

    instance-of v0, p0, Landroid/media/MediaCodec$CodecException;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/media/MediaCodec$CodecException;

    invoke-virtual {p0}, Landroid/media/MediaCodec$CodecException;->isRecoverable()Z

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method private L0(Landroid/media/MediaCrypto;Z)V
    .locals 7

    iget-object v0, p0, Ly0/o;->R:Ljava/util/ArrayDeque;

    const/4 v1, 0x0

    if-nez v0, :cond_2

    :try_start_0
    invoke-direct {p0, p2}, Ly0/o;->r0(Z)Ljava/util/List;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayDeque;

    invoke-direct {v2}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v2, p0, Ly0/o;->R:Ljava/util/ArrayDeque;

    iget-boolean v3, p0, Ly0/o;->u:Z

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/util/ArrayDeque;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Ly0/o;->R:Ljava/util/ArrayDeque;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly0/n;

    invoke-virtual {v2, v0}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    iput-object v1, p0, Ly0/o;->S:Ly0/o$b;
    :try_end_0
    .catch Ly0/v$c; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    new-instance v0, Ly0/o$b;

    iget-object v1, p0, Ly0/o;->D:Lh0/r1;

    const v2, -0xc34e

    invoke-direct {v0, v1, p1, p2, v2}, Ly0/o$b;-><init>(Lh0/r1;Ljava/lang/Throwable;ZI)V

    throw v0

    :cond_2
    :goto_1
    iget-object v0, p0, Ly0/o;->R:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Ly0/o;->R:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->peekFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly0/n;

    :goto_2
    iget-object v2, p0, Ly0/o;->M:Ly0/l;

    if-nez v2, :cond_7

    iget-object v2, p0, Ly0/o;->R:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->peekFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ly0/n;

    invoke-virtual {p0, v2}, Ly0/o;->m1(Ly0/n;)Z

    move-result v3

    if-nez v3, :cond_3

    return-void

    :cond_3
    :try_start_1
    invoke-direct {p0, v2, p1}, Ly0/o;->F0(Ly0/n;Landroid/media/MediaCrypto;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v3

    const-string v4, "MediaCodecRenderer"

    if-ne v2, v0, :cond_4

    :try_start_2
    const-string v3, "Preferred decoder instantiation failed. Sleeping for 50ms then retrying."

    invoke-static {v4, v3}, Le2/r;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v5, 0x32

    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V

    invoke-direct {p0, v2, p1}, Ly0/o;->F0(Ly0/n;Landroid/media/MediaCrypto;)V

    goto :goto_2

    :cond_4
    throw v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to initialize decoder: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Le2/r;->j(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v4, p0, Ly0/o;->R:Ljava/util/ArrayDeque;

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    new-instance v4, Ly0/o$b;

    iget-object v5, p0, Ly0/o;->D:Lh0/r1;

    invoke-direct {v4, v5, v3, p2, v2}, Ly0/o$b;-><init>(Lh0/r1;Ljava/lang/Throwable;ZLy0/n;)V

    invoke-virtual {p0, v4}, Ly0/o;->M0(Ljava/lang/Exception;)V

    iget-object v2, p0, Ly0/o;->S:Ly0/o$b;

    if-nez v2, :cond_5

    iput-object v4, p0, Ly0/o;->S:Ly0/o$b;

    goto :goto_3

    :cond_5
    invoke-static {v2, v4}, Ly0/o$b;->a(Ly0/o$b;Ly0/o$b;)Ly0/o$b;

    move-result-object v2

    iput-object v2, p0, Ly0/o;->S:Ly0/o$b;

    :goto_3
    iget-object v2, p0, Ly0/o;->R:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    goto :goto_2

    :cond_6
    iget-object p1, p0, Ly0/o;->S:Ly0/o$b;

    throw p1

    :cond_7
    iput-object v1, p0, Ly0/o;->R:Ljava/util/ArrayDeque;

    return-void

    :cond_8
    new-instance p1, Ly0/o$b;

    iget-object v0, p0, Ly0/o;->D:Lh0/r1;

    const v2, -0xc34f

    invoke-direct {p1, v0, v1, p2, v2}, Ly0/o$b;-><init>(Lh0/r1;Ljava/lang/Throwable;ZI)V

    throw p1
.end method

.method private U()V
    .locals 5

    iget-boolean v0, p0, Ly0/o;->y0:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-static {v0}, Le2/a;->f(Z)V

    invoke-virtual {p0}, Lh0/f;->F()Lh0/s1;

    move-result-object v0

    iget-object v2, p0, Ly0/o;->y:Lk0/g;

    invoke-virtual {v2}, Lk0/g;->f()V

    :cond_0
    iget-object v2, p0, Ly0/o;->y:Lk0/g;

    invoke-virtual {v2}, Lk0/g;->f()V

    iget-object v2, p0, Ly0/o;->y:Lk0/g;

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lh0/f;->R(Lh0/s1;Lk0/g;I)I

    move-result v2

    const/4 v4, -0x5

    if-eq v2, v4, :cond_5

    const/4 v4, -0x4

    if-eq v2, v4, :cond_2

    const/4 v0, -0x3

    if-ne v2, v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    iget-object v2, p0, Ly0/o;->y:Lk0/g;

    invoke-virtual {v2}, Lk0/a;->k()Z

    move-result v2

    if-eqz v2, :cond_3

    iput-boolean v1, p0, Ly0/o;->y0:Z

    return-void

    :cond_3
    iget-boolean v2, p0, Ly0/o;->A0:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Ly0/o;->D:Lh0/r1;

    invoke-static {v2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lh0/r1;

    iput-object v2, p0, Ly0/o;->E:Lh0/r1;

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4}, Ly0/o;->Q0(Lh0/r1;Landroid/media/MediaFormat;)V

    iput-boolean v3, p0, Ly0/o;->A0:Z

    :cond_4
    iget-object v2, p0, Ly0/o;->y:Lk0/g;

    invoke-virtual {v2}, Lk0/g;->q()V

    iget-object v2, p0, Ly0/o;->z:Ly0/h;

    iget-object v3, p0, Ly0/o;->y:Lk0/g;

    invoke-virtual {v2, v3}, Ly0/h;->u(Lk0/g;)Z

    move-result v2

    if-nez v2, :cond_0

    iput-boolean v1, p0, Ly0/o;->n0:Z

    return-void

    :cond_5
    invoke-virtual {p0, v0}, Ly0/o;->P0(Lh0/s1;)Lk0/i;

    return-void
.end method

.method private V(JJ)Z
    .locals 18

    move-object/from16 v15, p0

    iget-boolean v0, v15, Ly0/o;->z0:Z

    const/4 v14, 0x1

    xor-int/2addr v0, v14

    invoke-static {v0}, Le2/a;->f(Z)V

    iget-object v0, v15, Ly0/o;->z:Ly0/h;

    invoke-virtual {v0}, Ly0/h;->z()Z

    move-result v0

    const/4 v13, 0x0

    if-eqz v0, :cond_1

    const/4 v5, 0x0

    iget-object v0, v15, Ly0/o;->z:Ly0/h;

    iget-object v6, v0, Lk0/g;->h:Ljava/nio/ByteBuffer;

    iget v7, v15, Ly0/o;->i0:I

    const/4 v8, 0x0

    invoke-virtual {v0}, Ly0/h;->y()I

    move-result v9

    iget-object v0, v15, Ly0/o;->z:Ly0/h;

    invoke-virtual {v0}, Ly0/h;->w()J

    move-result-wide v10

    iget-object v0, v15, Ly0/o;->z:Ly0/h;

    invoke-virtual {v0}, Lk0/a;->j()Z

    move-result v12

    iget-object v0, v15, Ly0/o;->z:Ly0/h;

    invoke-virtual {v0}, Lk0/a;->k()Z

    move-result v16

    iget-object v3, v15, Ly0/o;->E:Lh0/r1;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-object/from16 v17, v3

    move-wide/from16 v3, p3

    move/from16 v13, v16

    move-object/from16 v14, v17

    invoke-virtual/range {v0 .. v14}, Ly0/o;->W0(JJLy0/l;Ljava/nio/ByteBuffer;IIIJZZLh0/r1;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v15, Ly0/o;->z:Ly0/h;

    invoke-virtual {v0}, Ly0/h;->x()J

    move-result-wide v0

    invoke-virtual {v15, v0, v1}, Ly0/o;->S0(J)V

    iget-object v0, v15, Ly0/o;->z:Ly0/h;

    invoke-virtual {v0}, Ly0/h;->f()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    iget-boolean v1, v15, Ly0/o;->y0:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    iput-boolean v1, v15, Ly0/o;->z0:Z

    return v0

    :cond_2
    const/4 v1, 0x1

    iget-boolean v2, v15, Ly0/o;->n0:Z

    if-eqz v2, :cond_3

    iget-object v2, v15, Ly0/o;->z:Ly0/h;

    iget-object v3, v15, Ly0/o;->y:Lk0/g;

    invoke-virtual {v2, v3}, Ly0/h;->u(Lk0/g;)Z

    move-result v2

    invoke-static {v2}, Le2/a;->f(Z)V

    iput-boolean v0, v15, Ly0/o;->n0:Z

    :cond_3
    iget-boolean v2, v15, Ly0/o;->o0:Z

    if-eqz v2, :cond_5

    iget-object v2, v15, Ly0/o;->z:Ly0/h;

    invoke-virtual {v2}, Ly0/h;->z()Z

    move-result v2

    if-eqz v2, :cond_4

    return v1

    :cond_4
    invoke-direct/range {p0 .. p0}, Ly0/o;->h0()V

    iput-boolean v0, v15, Ly0/o;->o0:Z

    invoke-virtual/range {p0 .. p0}, Ly0/o;->K0()V

    iget-boolean v2, v15, Ly0/o;->m0:Z

    if-nez v2, :cond_5

    return v0

    :cond_5
    invoke-direct/range {p0 .. p0}, Ly0/o;->U()V

    iget-object v2, v15, Ly0/o;->z:Ly0/h;

    invoke-virtual {v2}, Ly0/h;->z()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, v15, Ly0/o;->z:Ly0/h;

    invoke-virtual {v2}, Lk0/g;->q()V

    :cond_6
    iget-object v2, v15, Ly0/o;->z:Ly0/h;

    invoke-virtual {v2}, Ly0/h;->z()Z

    move-result v2

    if-nez v2, :cond_8

    iget-boolean v2, v15, Ly0/o;->y0:Z

    if-nez v2, :cond_8

    iget-boolean v2, v15, Ly0/o;->o0:Z

    if-eqz v2, :cond_7

    goto :goto_1

    :cond_7
    const/4 v14, 0x0

    goto :goto_2

    :cond_8
    :goto_1
    const/4 v14, 0x1

    :goto_2
    return v14
.end method

.method private V0()V
    .locals 3

    iget v0, p0, Ly0/o;->s0:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    iput-boolean v1, p0, Ly0/o;->z0:Z

    invoke-virtual {p0}, Ly0/o;->b1()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Ly0/o;->Z0()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Ly0/o;->o0()V

    invoke-direct {p0}, Ly0/o;->s1()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Ly0/o;->o0()V

    :goto_0
    return-void
.end method

.method private X(Ljava/lang/String;)I
    .locals 3

    sget v0, Le2/n0;->a:I

    const/16 v1, 0x19

    if-gt v0, v1, :cond_1

    const-string v1, "OMX.Exynos.avc.dec.secure"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Le2/n0;->d:Ljava/lang/String;

    const-string v2, "SM-T585"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SM-A510"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SM-A520"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SM-J700"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 p1, 0x2

    return p1

    :cond_1
    const/16 v1, 0x18

    if-ge v0, v1, :cond_4

    const-string v0, "OMX.Nvidia.h264.decode"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "OMX.Nvidia.h264.decode.secure"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    :cond_2
    sget-object p1, Le2/n0;->b:Ljava/lang/String;

    const-string v0, "flounder"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "flounder_lte"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "grouper"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "tilapia"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    :cond_3
    const/4 p1, 0x1

    return p1

    :cond_4
    const/4 p1, 0x0

    return p1
.end method

.method private X0()V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Ly0/o;->v0:Z

    iget-object v1, p0, Ly0/o;->M:Ly0/l;

    invoke-interface {v1}, Ly0/l;->g()Landroid/media/MediaFormat;

    move-result-object v1

    iget v2, p0, Ly0/o;->U:I

    if-eqz v2, :cond_0

    const-string v2, "width"

    invoke-virtual {v1, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    const-string v2, "height"

    invoke-virtual {v1, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_0

    iput-boolean v0, p0, Ly0/o;->d0:Z

    return-void

    :cond_0
    iget-boolean v2, p0, Ly0/o;->b0:Z

    if-eqz v2, :cond_1

    const-string v2, "channel-count"

    invoke-virtual {v1, v2, v0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    :cond_1
    iput-object v1, p0, Ly0/o;->O:Landroid/media/MediaFormat;

    iput-boolean v0, p0, Ly0/o;->P:Z

    return-void
.end method

.method private static Y(Ljava/lang/String;Lh0/r1;)Z
    .locals 2

    sget v0, Le2/n0;->a:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    iget-object p1, p1, Lh0/r1;->s:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "OMX.MTK.VIDEO.DECODER.AVC"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private Y0(I)Z
    .locals 3

    invoke-virtual {p0}, Lh0/f;->F()Lh0/s1;

    move-result-object v0

    iget-object v1, p0, Ly0/o;->w:Lk0/g;

    invoke-virtual {v1}, Lk0/g;->f()V

    iget-object v1, p0, Ly0/o;->w:Lk0/g;

    or-int/lit8 p1, p1, 0x4

    invoke-virtual {p0, v0, v1, p1}, Lh0/f;->R(Lh0/s1;Lk0/g;I)I

    move-result p1

    const/4 v1, 0x1

    const/4 v2, -0x5

    if-ne p1, v2, :cond_0

    invoke-virtual {p0, v0}, Ly0/o;->P0(Lh0/s1;)Lk0/i;

    return v1

    :cond_0
    const/4 v0, -0x4

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Ly0/o;->w:Lk0/g;

    invoke-virtual {p1}, Lk0/a;->k()Z

    move-result p1

    if-eqz p1, :cond_1

    iput-boolean v1, p0, Ly0/o;->y0:Z

    invoke-direct {p0}, Ly0/o;->V0()V

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private static Z(Ljava/lang/String;)Z
    .locals 2

    sget v0, Le2/n0;->a:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    const-string v0, "OMX.SEC.mp3.dec"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    sget-object p0, Le2/n0;->c:Ljava/lang/String;

    const-string v0, "samsung"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    sget-object p0, Le2/n0;->b:Ljava/lang/String;

    const-string v0, "baffin"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "grand"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fortuna"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "gprimelte"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "j2y18lte"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ms01"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private Z0()V
    .locals 0

    invoke-virtual {p0}, Ly0/o;->a1()V

    invoke-virtual {p0}, Ly0/o;->K0()V

    return-void
.end method

.method private static a0(Ljava/lang/String;)Z
    .locals 2

    sget v0, Le2/n0;->a:I

    const/16 v1, 0x17

    if-gt v0, v1, :cond_0

    const-string v1, "OMX.google.vorbis.decoder"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/16 v1, 0x13

    if-gt v0, v1, :cond_3

    sget-object v0, Le2/n0;->b:Ljava/lang/String;

    const-string v1, "hb2000"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "stvm8"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const-string v0, "OMX.amlogic.avc.decoder.awesome"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "OMX.amlogic.avc.decoder.awesome.secure"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    :cond_2
    const/4 p0, 0x1

    goto :goto_0

    :cond_3
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static b0(Ljava/lang/String;)Z
    .locals 2

    sget v0, Le2/n0;->a:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    const-string v0, "OMX.google.aac.decoder"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static c0(Ly0/n;)Z
    .locals 3

    iget-object v0, p0, Ly0/n;->a:Ljava/lang/String;

    sget v1, Le2/n0;->a:I

    const/16 v2, 0x19

    if-gt v1, v2, :cond_0

    const-string v2, "OMX.rk.video_decoder.avc"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_0
    const/16 v2, 0x11

    if-gt v1, v2, :cond_1

    const-string v2, "OMX.allwinner.video.decoder.avc"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    const/16 v2, 0x1d

    if-gt v1, v2, :cond_2

    const-string v1, "OMX.broadcom.video_decoder.tunnel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "OMX.broadcom.video_decoder.tunnel.secure"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "OMX.bcm.vdec.avc.tunnel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "OMX.bcm.vdec.avc.tunnel.secure"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "OMX.bcm.vdec.hevc.tunnel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "OMX.bcm.vdec.hevc.tunnel.secure"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    sget-object v0, Le2/n0;->c:Ljava/lang/String;

    const-string v1, "Amazon"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Le2/n0;->d:Ljava/lang/String;

    const-string v1, "AFTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean p0, p0, Ly0/n;->g:Z

    if-eqz p0, :cond_4

    :cond_3
    const/4 p0, 0x1

    goto :goto_0

    :cond_4
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static d0(Ljava/lang/String;)Z
    .locals 2

    sget v0, Le2/n0;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_2

    if-ne v0, v1, :cond_0

    const-string v1, "OMX.SEC.avc.dec"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "OMX.SEC.avc.dec.secure"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/16 v1, 0x13

    if-ne v0, v1, :cond_1

    sget-object v0, Le2/n0;->d:Ljava/lang/String;

    const-string v1, "SM-G800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "OMX.Exynos.avc.dec"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "OMX.Exynos.avc.dec.secure"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static e0(Ljava/lang/String;Lh0/r1;)Z
    .locals 3

    sget v0, Le2/n0;->a:I

    const/4 v1, 0x1

    const/16 v2, 0x12

    if-gt v0, v2, :cond_0

    iget p1, p1, Lh0/r1;->D:I

    if-ne p1, v1, :cond_0

    const-string p1, "OMX.MTK.AUDIO.DECODER.MP3"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private e1()V
    .locals 2

    const/4 v0, -0x1

    iput v0, p0, Ly0/o;->h0:I

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    const/4 v1, 0x0

    iput-object v1, v0, Lk0/g;->h:Ljava/nio/ByteBuffer;

    return-void
.end method

.method private static f0(Ljava/lang/String;)Z
    .locals 2

    sget v0, Le2/n0;->a:I

    const/16 v1, 0x1d

    if-ne v0, v1, :cond_0

    const-string v0, "c2.android.aac.decoder"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private f1()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Ly0/o;->i0:I

    const/4 v0, 0x0

    iput-object v0, p0, Ly0/o;->j0:Ljava/nio/ByteBuffer;

    return-void
.end method

.method private g1(Ll0/o;)V
    .locals 1

    iget-object v0, p0, Ly0/o;->F:Ll0/o;

    invoke-static {v0, p1}, Ll0/n;->a(Ll0/o;Ll0/o;)V

    iput-object p1, p0, Ly0/o;->F:Ll0/o;

    return-void
.end method

.method private h0()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Ly0/o;->o0:Z

    iget-object v1, p0, Ly0/o;->z:Ly0/h;

    invoke-virtual {v1}, Ly0/h;->f()V

    iget-object v1, p0, Ly0/o;->y:Lk0/g;

    invoke-virtual {v1}, Lk0/g;->f()V

    iput-boolean v0, p0, Ly0/o;->n0:Z

    iput-boolean v0, p0, Ly0/o;->m0:Z

    return-void
.end method

.method private h1(Ly0/o$c;)V
    .locals 4

    iput-object p1, p0, Ly0/o;->E0:Ly0/o$c;

    iget-wide v0, p1, Ly0/o$c;->c:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p1, v0, v2

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Ly0/o;->G0:Z

    invoke-virtual {p0, v0, v1}, Ly0/o;->R0(J)V

    :cond_0
    return-void
.end method

.method private i0()Z
    .locals 2

    iget-boolean v0, p0, Ly0/o;->t0:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    iput v1, p0, Ly0/o;->r0:I

    iget-boolean v0, p0, Ly0/o;->W:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Ly0/o;->Y:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iput v1, p0, Ly0/o;->s0:I

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x3

    iput v0, p0, Ly0/o;->s0:I

    const/4 v0, 0x0

    return v0

    :cond_2
    :goto_1
    return v1
.end method

.method private j0()V
    .locals 1

    iget-boolean v0, p0, Ly0/o;->t0:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput v0, p0, Ly0/o;->r0:I

    const/4 v0, 0x3

    iput v0, p0, Ly0/o;->s0:I

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Ly0/o;->Z0()V

    :goto_0
    return-void
.end method

.method private k0()Z
    .locals 2

    iget-boolean v0, p0, Ly0/o;->t0:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    iput v1, p0, Ly0/o;->r0:I

    iget-boolean v0, p0, Ly0/o;->W:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Ly0/o;->Y:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Ly0/o;->s0:I

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x3

    iput v0, p0, Ly0/o;->s0:I

    const/4 v0, 0x0

    return v0

    :cond_2
    invoke-direct {p0}, Ly0/o;->s1()V

    :goto_1
    return v1
.end method

.method private k1(Ll0/o;)V
    .locals 1

    iget-object v0, p0, Ly0/o;->G:Ll0/o;

    invoke-static {v0, p1}, Ll0/n;->a(Ll0/o;Ll0/o;)V

    iput-object p1, p0, Ly0/o;->G:Ll0/o;

    return-void
.end method

.method private l0(JJ)Z
    .locals 19

    move-object/from16 v15, p0

    invoke-direct/range {p0 .. p0}, Ly0/o;->D0()Z

    move-result v0

    const/16 v16, 0x1

    const/4 v14, 0x0

    if-nez v0, :cond_b

    iget-boolean v0, v15, Ly0/o;->Z:Z

    if-eqz v0, :cond_1

    iget-boolean v0, v15, Ly0/o;->u0:Z

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, v15, Ly0/o;->M:Ly0/l;

    iget-object v1, v15, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    invoke-interface {v0, v1}, Ly0/l;->b(Landroid/media/MediaCodec$BufferInfo;)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    invoke-direct/range {p0 .. p0}, Ly0/o;->V0()V

    iget-boolean v0, v15, Ly0/o;->z0:Z

    if-eqz v0, :cond_0

    invoke-virtual/range {p0 .. p0}, Ly0/o;->a1()V

    :cond_0
    return v14

    :cond_1
    iget-object v0, v15, Ly0/o;->M:Ly0/l;

    iget-object v1, v15, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    invoke-interface {v0, v1}, Ly0/l;->b(Landroid/media/MediaCodec$BufferInfo;)I

    move-result v0

    :goto_0
    if-gez v0, :cond_5

    const/4 v1, -0x2

    if-ne v0, v1, :cond_2

    invoke-direct/range {p0 .. p0}, Ly0/o;->X0()V

    return v16

    :cond_2
    iget-boolean v0, v15, Ly0/o;->e0:Z

    if-eqz v0, :cond_4

    iget-boolean v0, v15, Ly0/o;->y0:Z

    if-nez v0, :cond_3

    iget v0, v15, Ly0/o;->r0:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-direct/range {p0 .. p0}, Ly0/o;->V0()V

    :cond_4
    return v14

    :cond_5
    iget-boolean v1, v15, Ly0/o;->d0:Z

    if-eqz v1, :cond_6

    iput-boolean v14, v15, Ly0/o;->d0:Z

    iget-object v1, v15, Ly0/o;->M:Ly0/l;

    invoke-interface {v1, v0, v14}, Ly0/l;->d(IZ)V

    return v16

    :cond_6
    iget-object v1, v15, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    iget v2, v1, Landroid/media/MediaCodec$BufferInfo;->size:I

    if-nez v2, :cond_7

    iget v1, v1, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    invoke-direct/range {p0 .. p0}, Ly0/o;->V0()V

    return v14

    :cond_7
    iput v0, v15, Ly0/o;->i0:I

    iget-object v1, v15, Ly0/o;->M:Ly0/l;

    invoke-interface {v1, v0}, Ly0/l;->l(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, v15, Ly0/o;->j0:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_8

    iget-object v1, v15, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    iget v1, v1, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, v15, Ly0/o;->j0:Ljava/nio/ByteBuffer;

    iget-object v1, v15, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    iget v2, v1, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v1, v1, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v2, v1

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    :cond_8
    iget-boolean v0, v15, Ly0/o;->a0:Z

    if-eqz v0, :cond_9

    iget-object v0, v15, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v1, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-nez v5, :cond_9

    iget v1, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_9

    iget-wide v1, v15, Ly0/o;->w0:J

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v5, v1, v3

    if-eqz v5, :cond_9

    iput-wide v1, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    :cond_9
    iget-object v0, v15, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v0, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-direct {v15, v0, v1}, Ly0/o;->G0(J)Z

    move-result v0

    iput-boolean v0, v15, Ly0/o;->k0:Z

    iget-wide v0, v15, Ly0/o;->x0:J

    iget-object v2, v15, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v2, v2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_a

    const/4 v0, 0x1

    goto :goto_1

    :cond_a
    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, v15, Ly0/o;->l0:Z

    invoke-virtual {v15, v2, v3}, Ly0/o;->t1(J)V

    :cond_b
    iget-boolean v0, v15, Ly0/o;->Z:Z

    if-eqz v0, :cond_d

    iget-boolean v0, v15, Ly0/o;->u0:Z

    if-eqz v0, :cond_d

    :try_start_1
    iget-object v5, v15, Ly0/o;->M:Ly0/l;

    iget-object v6, v15, Ly0/o;->j0:Ljava/nio/ByteBuffer;

    iget v7, v15, Ly0/o;->i0:I

    iget-object v0, v15, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    iget v8, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    const/4 v9, 0x1

    iget-wide v10, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iget-boolean v12, v15, Ly0/o;->k0:Z

    iget-boolean v13, v15, Ly0/o;->l0:Z

    iget-object v3, v15, Ly0/o;->E:Lh0/r1;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-object/from16 v17, v3

    move-wide/from16 v3, p3

    const/16 v18, 0x0

    move-object/from16 v14, v17

    :try_start_2
    invoke-virtual/range {v0 .. v14}, Ly0/o;->W0(JJLy0/l;Ljava/nio/ByteBuffer;IIIJZZLh0/r1;)Z

    move-result v0
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    :catch_1
    nop

    goto :goto_2

    :catch_2
    const/16 v18, 0x0

    :goto_2
    invoke-direct/range {p0 .. p0}, Ly0/o;->V0()V

    iget-boolean v0, v15, Ly0/o;->z0:Z

    if-eqz v0, :cond_c

    invoke-virtual/range {p0 .. p0}, Ly0/o;->a1()V

    :cond_c
    return v18

    :cond_d
    const/16 v18, 0x0

    iget-object v5, v15, Ly0/o;->M:Ly0/l;

    iget-object v6, v15, Ly0/o;->j0:Ljava/nio/ByteBuffer;

    iget v7, v15, Ly0/o;->i0:I

    iget-object v0, v15, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    iget v8, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    const/4 v9, 0x1

    iget-wide v10, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iget-boolean v12, v15, Ly0/o;->k0:Z

    iget-boolean v13, v15, Ly0/o;->l0:Z

    iget-object v14, v15, Ly0/o;->E:Lh0/r1;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    invoke-virtual/range {v0 .. v14}, Ly0/o;->W0(JJLy0/l;Ljava/nio/ByteBuffer;IIIJZZLh0/r1;)Z

    move-result v0

    :goto_3
    if-eqz v0, :cond_10

    iget-object v0, v15, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v0, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-virtual {v15, v0, v1}, Ly0/o;->S0(J)V

    iget-object v0, v15, Ly0/o;->B:Landroid/media/MediaCodec$BufferInfo;

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_e

    const/4 v14, 0x1

    goto :goto_4

    :cond_e
    const/4 v14, 0x0

    :goto_4
    invoke-direct/range {p0 .. p0}, Ly0/o;->f1()V

    if-nez v14, :cond_f

    return v16

    :cond_f
    invoke-direct/range {p0 .. p0}, Ly0/o;->V0()V

    :cond_10
    return v18
.end method

.method private l1(J)Z
    .locals 5

    iget-wide v0, p0, Ly0/o;->J:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, p1

    iget-wide p1, p0, Ly0/o;->J:J

    cmp-long v2, v0, p1

    if-gez v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private m0(Ly0/n;Lh0/r1;Ll0/o;Ll0/o;)Z
    .locals 4

    const/4 v0, 0x0

    if-ne p3, p4, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x1

    if-eqz p4, :cond_8

    if-nez p3, :cond_1

    goto :goto_1

    :cond_1
    invoke-interface {p4}, Ll0/o;->e()Ljava/util/UUID;

    move-result-object v2

    invoke-interface {p3}, Ll0/o;->e()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    :cond_2
    sget v2, Le2/n0;->a:I

    const/16 v3, 0x17

    if-ge v2, v3, :cond_3

    return v1

    :cond_3
    sget-object v2, Lh0/i;->e:Ljava/util/UUID;

    invoke-interface {p3}, Ll0/o;->e()Ljava/util/UUID;

    move-result-object p3

    invoke-virtual {v2, p3}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_8

    invoke-interface {p4}, Ll0/o;->e()Ljava/util/UUID;

    move-result-object p3

    invoke-virtual {v2, p3}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_4

    goto :goto_1

    :cond_4
    invoke-direct {p0, p4}, Ly0/o;->y0(Ll0/o;)Ll0/h0;

    move-result-object p3

    if-nez p3, :cond_5

    return v1

    :cond_5
    iget-boolean p3, p3, Ll0/h0;->c:Z

    if-eqz p3, :cond_6

    const/4 p2, 0x0

    goto :goto_0

    :cond_6
    iget-object p2, p2, Lh0/r1;->q:Ljava/lang/String;

    invoke-interface {p4, p2}, Ll0/o;->f(Ljava/lang/String;)Z

    move-result p2

    :goto_0
    iget-boolean p1, p1, Ly0/n;->g:Z

    if-nez p1, :cond_7

    if-eqz p2, :cond_7

    return v1

    :cond_7
    return v0

    :cond_8
    :goto_1
    return v1
.end method

.method private n0()Z
    .locals 15

    iget-object v0, p0, Ly0/o;->M:Ly0/l;

    const/4 v1, 0x0

    if-eqz v0, :cond_1c

    iget v0, p0, Ly0/o;->r0:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1c

    iget-boolean v3, p0, Ly0/o;->y0:Z

    if-eqz v3, :cond_0

    goto/16 :goto_5

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Ly0/o;->n1()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Ly0/o;->j0()V

    :cond_1
    iget v0, p0, Ly0/o;->h0:I

    if-gez v0, :cond_3

    iget-object v0, p0, Ly0/o;->M:Ly0/l;

    invoke-interface {v0}, Ly0/l;->o()I

    move-result v0

    iput v0, p0, Ly0/o;->h0:I

    if-gez v0, :cond_2

    return v1

    :cond_2
    iget-object v3, p0, Ly0/o;->x:Lk0/g;

    iget-object v4, p0, Ly0/o;->M:Ly0/l;

    invoke-interface {v4, v0}, Ly0/l;->h(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, v3, Lk0/g;->h:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {v0}, Lk0/g;->f()V

    :cond_3
    iget v0, p0, Ly0/o;->r0:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_5

    iget-boolean v0, p0, Ly0/o;->e0:Z

    if-eqz v0, :cond_4

    goto :goto_0

    :cond_4
    iput-boolean v3, p0, Ly0/o;->u0:Z

    iget-object v4, p0, Ly0/o;->M:Ly0/l;

    iget v5, p0, Ly0/o;->h0:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x4

    invoke-interface/range {v4 .. v10}, Ly0/l;->j(IIIJI)V

    invoke-direct {p0}, Ly0/o;->e1()V

    :goto_0
    iput v2, p0, Ly0/o;->r0:I

    return v1

    :cond_5
    iget-boolean v0, p0, Ly0/o;->c0:Z

    if-eqz v0, :cond_6

    iput-boolean v1, p0, Ly0/o;->c0:Z

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    iget-object v0, v0, Lk0/g;->h:Ljava/nio/ByteBuffer;

    sget-object v1, Ly0/o;->H0:[B

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    iget-object v4, p0, Ly0/o;->M:Ly0/l;

    iget v5, p0, Ly0/o;->h0:I

    const/4 v6, 0x0

    array-length v7, v1

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    invoke-interface/range {v4 .. v10}, Ly0/l;->j(IIIJI)V

    invoke-direct {p0}, Ly0/o;->e1()V

    iput-boolean v3, p0, Ly0/o;->t0:Z

    return v3

    :cond_6
    iget v0, p0, Ly0/o;->q0:I

    if-ne v0, v3, :cond_8

    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Ly0/o;->N:Lh0/r1;

    iget-object v4, v4, Lh0/r1;->s:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_7

    iget-object v4, p0, Ly0/o;->N:Lh0/r1;

    iget-object v4, v4, Lh0/r1;->s:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    iget-object v5, p0, Ly0/o;->x:Lk0/g;

    iget-object v5, v5, Lk0/g;->h:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    iput v2, p0, Ly0/o;->q0:I

    :cond_8
    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    iget-object v0, v0, Lk0/g;->h:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    invoke-virtual {p0}, Lh0/f;->F()Lh0/s1;

    move-result-object v4

    :try_start_0
    iget-object v5, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {p0, v4, v5, v1}, Lh0/f;->R(Lh0/s1;Lk0/g;I)I

    move-result v5
    :try_end_0
    .catch Lk0/g$a; {:try_start_0 .. :try_end_0} :catch_2

    invoke-virtual {p0}, Lh0/f;->n()Z

    move-result v6

    if-eqz v6, :cond_9

    iget-wide v6, p0, Ly0/o;->w0:J

    iput-wide v6, p0, Ly0/o;->x0:J

    :cond_9
    const/4 v6, -0x3

    if-ne v5, v6, :cond_a

    return v1

    :cond_a
    const/4 v6, -0x5

    if-ne v5, v6, :cond_c

    iget v0, p0, Ly0/o;->q0:I

    if-ne v0, v2, :cond_b

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {v0}, Lk0/g;->f()V

    iput v3, p0, Ly0/o;->q0:I

    :cond_b
    invoke-virtual {p0, v4}, Ly0/o;->P0(Lh0/s1;)Lk0/i;

    return v3

    :cond_c
    iget-object v4, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {v4}, Lk0/a;->k()Z

    move-result v4

    if-eqz v4, :cond_10

    iget v0, p0, Ly0/o;->q0:I

    if-ne v0, v2, :cond_d

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {v0}, Lk0/g;->f()V

    iput v3, p0, Ly0/o;->q0:I

    :cond_d
    iput-boolean v3, p0, Ly0/o;->y0:Z

    iget-boolean v0, p0, Ly0/o;->t0:Z

    if-nez v0, :cond_e

    invoke-direct {p0}, Ly0/o;->V0()V

    return v1

    :cond_e
    :try_start_1
    iget-boolean v0, p0, Ly0/o;->e0:Z

    if-eqz v0, :cond_f

    goto :goto_2

    :cond_f
    iput-boolean v3, p0, Ly0/o;->u0:Z

    iget-object v4, p0, Ly0/o;->M:Ly0/l;

    iget v5, p0, Ly0/o;->h0:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x4

    invoke-interface/range {v4 .. v10}, Ly0/l;->j(IIIJI)V

    invoke-direct {p0}, Ly0/o;->e1()V
    :try_end_1
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    return v1

    :catch_0
    move-exception v0

    iget-object v1, p0, Ly0/o;->D:Lh0/r1;

    invoke-virtual {v0}, Landroid/media/MediaCodec$CryptoException;->getErrorCode()I

    move-result v2

    invoke-static {v2}, Le2/n0;->U(I)I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lh0/f;->C(Ljava/lang/Throwable;Lh0/r1;I)Lh0/q;

    move-result-object v0

    throw v0

    :cond_10
    iget-boolean v4, p0, Ly0/o;->t0:Z

    if-nez v4, :cond_12

    iget-object v4, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {v4}, Lk0/a;->m()Z

    move-result v4

    if-nez v4, :cond_12

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {v0}, Lk0/g;->f()V

    iget v0, p0, Ly0/o;->q0:I

    if-ne v0, v2, :cond_11

    iput v3, p0, Ly0/o;->q0:I

    :cond_11
    return v3

    :cond_12
    iget-object v2, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {v2}, Lk0/g;->r()Z

    move-result v2

    if-eqz v2, :cond_13

    iget-object v4, p0, Ly0/o;->x:Lk0/g;

    iget-object v4, v4, Lk0/g;->g:Lk0/c;

    invoke-virtual {v4, v0}, Lk0/c;->b(I)V

    :cond_13
    iget-boolean v0, p0, Ly0/o;->V:Z

    if-eqz v0, :cond_15

    if-nez v2, :cond_15

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    iget-object v0, v0, Lk0/g;->h:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Le2/w;->b(Ljava/nio/ByteBuffer;)V

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    iget-object v0, v0, Lk0/g;->h:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-nez v0, :cond_14

    return v3

    :cond_14
    iput-boolean v1, p0, Ly0/o;->V:Z

    :cond_15
    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    iget-wide v4, v0, Lk0/g;->j:J

    iget-object v6, p0, Ly0/o;->f0:Ly0/i;

    if-eqz v6, :cond_16

    iget-object v4, p0, Ly0/o;->D:Lh0/r1;

    invoke-virtual {v6, v4, v0}, Ly0/i;->d(Lh0/r1;Lk0/g;)J

    move-result-wide v4

    iget-wide v6, p0, Ly0/o;->w0:J

    iget-object v0, p0, Ly0/o;->f0:Ly0/i;

    iget-object v8, p0, Ly0/o;->D:Lh0/r1;

    invoke-virtual {v0, v8}, Ly0/i;->b(Lh0/r1;)J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    iput-wide v6, p0, Ly0/o;->w0:J

    :cond_16
    move-wide v12, v4

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {v0}, Lk0/a;->j()Z

    move-result v0

    if-eqz v0, :cond_17

    iget-object v0, p0, Ly0/o;->A:Ljava/util/ArrayList;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_17
    iget-boolean v0, p0, Ly0/o;->A0:Z

    if-eqz v0, :cond_19

    iget-object v0, p0, Ly0/o;->C:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_18

    iget-object v0, p0, Ly0/o;->C:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->peekLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly0/o$c;

    goto :goto_3

    :cond_18
    iget-object v0, p0, Ly0/o;->E0:Ly0/o$c;

    :goto_3
    iget-object v0, v0, Ly0/o$c;->d:Le2/i0;

    iget-object v4, p0, Ly0/o;->D:Lh0/r1;

    invoke-virtual {v0, v12, v13, v4}, Le2/i0;->a(JLjava/lang/Object;)V

    iput-boolean v1, p0, Ly0/o;->A0:Z

    :cond_19
    iget-wide v4, p0, Ly0/o;->w0:J

    invoke-static {v4, v5, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Ly0/o;->w0:J

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {v0}, Lk0/g;->q()V

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {v0}, Lk0/a;->i()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {p0, v0}, Ly0/o;->C0(Lk0/g;)V

    :cond_1a
    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    invoke-virtual {p0, v0}, Ly0/o;->U0(Lk0/g;)V

    if-eqz v2, :cond_1b

    :try_start_2
    iget-object v8, p0, Ly0/o;->M:Ly0/l;

    iget v9, p0, Ly0/o;->h0:I

    const/4 v10, 0x0

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    iget-object v11, v0, Lk0/g;->g:Lk0/c;

    const/4 v14, 0x0

    invoke-interface/range {v8 .. v14}, Ly0/l;->e(IILk0/c;JI)V

    goto :goto_4

    :cond_1b
    iget-object v8, p0, Ly0/o;->M:Ly0/l;

    iget v9, p0, Ly0/o;->h0:I

    const/4 v10, 0x0

    iget-object v0, p0, Ly0/o;->x:Lk0/g;

    iget-object v0, v0, Lk0/g;->h:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v11

    const/4 v14, 0x0

    invoke-interface/range {v8 .. v14}, Ly0/l;->j(IIIJI)V
    :try_end_2
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_4
    invoke-direct {p0}, Ly0/o;->e1()V

    iput-boolean v3, p0, Ly0/o;->t0:Z

    iput v1, p0, Ly0/o;->q0:I

    iget-object v0, p0, Ly0/o;->D0:Lk0/e;

    iget v1, v0, Lk0/e;->c:I

    add-int/2addr v1, v3

    iput v1, v0, Lk0/e;->c:I

    return v3

    :catch_1
    move-exception v0

    iget-object v1, p0, Ly0/o;->D:Lh0/r1;

    invoke-virtual {v0}, Landroid/media/MediaCodec$CryptoException;->getErrorCode()I

    move-result v2

    invoke-static {v2}, Le2/n0;->U(I)I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lh0/f;->C(Ljava/lang/Throwable;Lh0/r1;I)Lh0/q;

    move-result-object v0

    throw v0

    :catch_2
    move-exception v0

    invoke-virtual {p0, v0}, Ly0/o;->M0(Ljava/lang/Exception;)V

    invoke-direct {p0, v1}, Ly0/o;->Y0(I)Z

    invoke-direct {p0}, Ly0/o;->o0()V

    return v3

    :cond_1c
    :goto_5
    return v1
.end method

.method private o0()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Ly0/o;->M:Ly0/l;

    invoke-interface {v0}, Ly0/l;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Ly0/o;->c1()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Ly0/o;->c1()V

    throw v0
.end method

.method protected static q1(Lh0/r1;)Z
    .locals 1

    iget p0, p0, Lh0/r1;->L:I

    if-eqz p0, :cond_1

    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private r0(Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Ly0/n;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Ly0/o;->t:Ly0/q;

    iget-object v1, p0, Ly0/o;->D:Lh0/r1;

    invoke-virtual {p0, v0, v1, p1}, Ly0/o;->x0(Ly0/q;Lh0/r1;Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    iget-object p1, p0, Ly0/o;->t:Ly0/q;

    iget-object v0, p0, Ly0/o;->D:Lh0/r1;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Ly0/o;->x0(Ly0/q;Lh0/r1;Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Drm session requires secure decoder for "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ly0/o;->D:Lh0/r1;

    iget-object v1, v1, Lh0/r1;->q:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", but no secure decoder available. Trying to proceed with "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "MediaCodecRenderer"

    invoke-static {v1, p1}, Le2/r;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method private r1(Lh0/r1;)Z
    .locals 4

    sget v0, Le2/n0;->a:I

    const/4 v1, 0x1

    const/16 v2, 0x17

    if-ge v0, v2, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Ly0/o;->M:Ly0/l;

    if-eqz v0, :cond_6

    iget v0, p0, Ly0/o;->s0:I

    const/4 v2, 0x3

    if-eq v0, v2, :cond_6

    invoke-virtual {p0}, Lh0/f;->getState()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    iget v0, p0, Ly0/o;->L:F

    invoke-virtual {p0}, Lh0/f;->I()[Lh0/r1;

    move-result-object v2

    invoke-virtual {p0, v0, p1, v2}, Ly0/o;->v0(FLh0/r1;[Lh0/r1;)F

    move-result p1

    iget v0, p0, Ly0/o;->Q:F

    cmpl-float v2, v0, p1

    if-nez v2, :cond_2

    return v1

    :cond_2
    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v3, p1, v2

    if-nez v3, :cond_3

    invoke-direct {p0}, Ly0/o;->j0()V

    const/4 p1, 0x0

    return p1

    :cond_3
    cmpl-float v0, v0, v2

    if-nez v0, :cond_5

    iget v0, p0, Ly0/o;->v:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_4

    goto :goto_0

    :cond_4
    return v1

    :cond_5
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "operating-rate"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    iget-object v2, p0, Ly0/o;->M:Ly0/l;

    invoke-interface {v2, v0}, Ly0/l;->k(Landroid/os/Bundle;)V

    iput p1, p0, Ly0/o;->Q:F

    :cond_6
    :goto_1
    return v1
.end method

.method private s1()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Ly0/o;->H:Landroid/media/MediaCrypto;

    iget-object v1, p0, Ly0/o;->G:Ll0/o;

    invoke-direct {p0, v1}, Ly0/o;->y0(Ll0/o;)Ll0/h0;

    move-result-object v1

    iget-object v1, v1, Ll0/h0;->b:[B

    invoke-virtual {v0, v1}, Landroid/media/MediaCrypto;->setMediaDrmSession([B)V
    :try_end_0
    .catch Landroid/media/MediaCryptoException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Ly0/o;->G:Ll0/o;

    invoke-direct {p0, v0}, Ly0/o;->g1(Ll0/o;)V

    const/4 v0, 0x0

    iput v0, p0, Ly0/o;->r0:I

    iput v0, p0, Ly0/o;->s0:I

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Ly0/o;->D:Lh0/r1;

    const/16 v2, 0x1776

    invoke-virtual {p0, v0, v1, v2}, Lh0/f;->C(Ljava/lang/Throwable;Lh0/r1;I)Lh0/q;

    move-result-object v0

    throw v0
.end method

.method private y0(Ll0/o;)Ll0/h0;
    .locals 3

    invoke-interface {p1}, Ll0/o;->h()Lk0/b;

    move-result-object p1

    if-eqz p1, :cond_1

    instance-of v0, p1, Ll0/h0;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expecting FrameworkCryptoConfig but found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    iget-object p1, p0, Ly0/o;->D:Lh0/r1;

    const/16 v1, 0x1771

    invoke-virtual {p0, v0, p1, v1}, Lh0/f;->C(Ljava/lang/Throwable;Lh0/r1;I)Lh0/q;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    check-cast p1, Ll0/h0;

    return-object p1
.end method


# virtual methods
.method protected final A0()J
    .locals 2

    iget-object v0, p0, Ly0/o;->E0:Ly0/o$c;

    iget-wide v0, v0, Ly0/o$c;->c:J

    return-wide v0
.end method

.method public B(FF)V
    .locals 0

    iput p1, p0, Ly0/o;->K:F

    iput p2, p0, Ly0/o;->L:F

    iget-object p1, p0, Ly0/o;->N:Lh0/r1;

    invoke-direct {p0, p1}, Ly0/o;->r1(Lh0/r1;)Z

    return-void
.end method

.method protected B0()F
    .locals 1

    iget v0, p0, Ly0/o;->K:F

    return v0
.end method

.method protected C0(Lk0/g;)V
    .locals 0

    return-void
.end method

.method protected K()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Ly0/o;->D:Lh0/r1;

    sget-object v0, Ly0/o$c;->e:Ly0/o$c;

    invoke-direct {p0, v0}, Ly0/o;->h1(Ly0/o$c;)V

    iget-object v0, p0, Ly0/o;->C:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    invoke-virtual {p0}, Ly0/o;->q0()Z

    return-void
.end method

.method protected final K0()V
    .locals 6

    iget-object v0, p0, Ly0/o;->M:Ly0/l;

    if-nez v0, :cond_8

    iget-boolean v0, p0, Ly0/o;->m0:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Ly0/o;->D:Lh0/r1;

    if-nez v0, :cond_0

    goto/16 :goto_2

    :cond_0
    iget-object v1, p0, Ly0/o;->G:Ll0/o;

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Ly0/o;->o1(Lh0/r1;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ly0/o;->D:Lh0/r1;

    invoke-direct {p0, v0}, Ly0/o;->E0(Lh0/r1;)V

    return-void

    :cond_1
    iget-object v0, p0, Ly0/o;->G:Ll0/o;

    invoke-direct {p0, v0}, Ly0/o;->g1(Ll0/o;)V

    iget-object v0, p0, Ly0/o;->D:Lh0/r1;

    iget-object v0, v0, Lh0/r1;->q:Ljava/lang/String;

    iget-object v1, p0, Ly0/o;->F:Ll0/o;

    if-eqz v1, :cond_7

    iget-object v2, p0, Ly0/o;->H:Landroid/media/MediaCrypto;

    const/4 v3, 0x1

    if-nez v2, :cond_5

    invoke-direct {p0, v1}, Ly0/o;->y0(Ll0/o;)Ll0/h0;

    move-result-object v1

    if-nez v1, :cond_3

    iget-object v0, p0, Ly0/o;->F:Ll0/o;

    invoke-interface {v0}, Ll0/o;->g()Ll0/o$a;

    move-result-object v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    return-void

    :cond_3
    :try_start_0
    new-instance v2, Landroid/media/MediaCrypto;

    iget-object v4, v1, Ll0/h0;->a:Ljava/util/UUID;

    iget-object v5, v1, Ll0/h0;->b:[B

    invoke-direct {v2, v4, v5}, Landroid/media/MediaCrypto;-><init>(Ljava/util/UUID;[B)V

    iput-object v2, p0, Ly0/o;->H:Landroid/media/MediaCrypto;
    :try_end_0
    .catch Landroid/media/MediaCryptoException; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v1, v1, Ll0/h0;->c:Z

    if-nez v1, :cond_4

    invoke-virtual {v2, v0}, Landroid/media/MediaCrypto;->requiresSecureDecoderComponent(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Ly0/o;->I:Z

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v1, p0, Ly0/o;->D:Lh0/r1;

    const/16 v2, 0x1776

    invoke-virtual {p0, v0, v1, v2}, Lh0/f;->C(Ljava/lang/Throwable;Lh0/r1;I)Lh0/q;

    move-result-object v0

    throw v0

    :cond_5
    :goto_1
    sget-boolean v0, Ll0/h0;->d:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Ly0/o;->F:Ll0/o;

    invoke-interface {v0}, Ll0/o;->getState()I

    move-result v0

    if-eq v0, v3, :cond_6

    const/4 v1, 0x4

    if-eq v0, v1, :cond_7

    return-void

    :cond_6
    iget-object v0, p0, Ly0/o;->F:Ll0/o;

    invoke-interface {v0}, Ll0/o;->g()Ll0/o$a;

    move-result-object v0

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll0/o$a;

    iget-object v1, p0, Ly0/o;->D:Lh0/r1;

    iget v2, v0, Ll0/o$a;->f:I

    invoke-virtual {p0, v0, v1, v2}, Lh0/f;->C(Ljava/lang/Throwable;Lh0/r1;I)Lh0/q;

    move-result-object v0

    throw v0

    :cond_7
    :try_start_1
    iget-object v0, p0, Ly0/o;->H:Landroid/media/MediaCrypto;

    iget-boolean v1, p0, Ly0/o;->I:Z

    invoke-direct {p0, v0, v1}, Ly0/o;->L0(Landroid/media/MediaCrypto;Z)V
    :try_end_1
    .catch Ly0/o$b; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    :catch_1
    move-exception v0

    iget-object v1, p0, Ly0/o;->D:Lh0/r1;

    const/16 v2, 0xfa1

    invoke-virtual {p0, v0, v1, v2}, Lh0/f;->C(Ljava/lang/Throwable;Lh0/r1;I)Lh0/q;

    move-result-object v0

    throw v0

    :cond_8
    :goto_2
    return-void
.end method

.method protected L(ZZ)V
    .locals 0

    new-instance p1, Lk0/e;

    invoke-direct {p1}, Lk0/e;-><init>()V

    iput-object p1, p0, Ly0/o;->D0:Lk0/e;

    return-void
.end method

.method protected M(JZ)V
    .locals 0

    const/4 p1, 0x0

    iput-boolean p1, p0, Ly0/o;->y0:Z

    iput-boolean p1, p0, Ly0/o;->z0:Z

    iput-boolean p1, p0, Ly0/o;->B0:Z

    iget-boolean p2, p0, Ly0/o;->m0:Z

    if-eqz p2, :cond_0

    iget-object p2, p0, Ly0/o;->z:Ly0/h;

    invoke-virtual {p2}, Ly0/h;->f()V

    iget-object p2, p0, Ly0/o;->y:Lk0/g;

    invoke-virtual {p2}, Lk0/g;->f()V

    iput-boolean p1, p0, Ly0/o;->n0:Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ly0/o;->p0()Z

    :goto_0
    iget-object p1, p0, Ly0/o;->E0:Ly0/o$c;

    iget-object p1, p1, Ly0/o$c;->d:Le2/i0;

    invoke-virtual {p1}, Le2/i0;->k()I

    move-result p1

    if-lez p1, :cond_1

    const/4 p1, 0x1

    iput-boolean p1, p0, Ly0/o;->A0:Z

    :cond_1
    iget-object p1, p0, Ly0/o;->E0:Ly0/o$c;

    iget-object p1, p1, Ly0/o$c;->d:Le2/i0;

    invoke-virtual {p1}, Le2/i0;->c()V

    iget-object p1, p0, Ly0/o;->C:Ljava/util/ArrayDeque;

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->clear()V

    return-void
.end method

.method protected abstract M0(Ljava/lang/Exception;)V
.end method

.method protected N()V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Ly0/o;->h0()V

    invoke-virtual {p0}, Ly0/o;->a1()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, v0}, Ly0/o;->k1(Ll0/o;)V

    return-void

    :catchall_0
    move-exception v1

    invoke-direct {p0, v0}, Ly0/o;->k1(Ll0/o;)V

    throw v1
.end method

.method protected abstract N0(Ljava/lang/String;Ly0/l$a;JJ)V
.end method

.method protected O()V
    .locals 0

    return-void
.end method

.method protected abstract O0(Ljava/lang/String;)V
.end method

.method protected P()V
    .locals 0

    return-void
.end method

.method protected P0(Lh0/s1;)Lk0/i;
    .locals 11

    const/4 v0, 0x1

    iput-boolean v0, p0, Ly0/o;->A0:Z

    iget-object v1, p1, Lh0/s1;->b:Lh0/r1;

    invoke-static {v1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lh0/r1;

    iget-object v1, v5, Lh0/r1;->q:Ljava/lang/String;

    if-eqz v1, :cond_13

    iget-object p1, p1, Lh0/s1;->a:Ll0/o;

    invoke-direct {p0, p1}, Ly0/o;->k1(Ll0/o;)V

    iput-object v5, p0, Ly0/o;->D:Lh0/r1;

    iget-boolean p1, p0, Ly0/o;->m0:Z

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iput-boolean v0, p0, Ly0/o;->o0:Z

    return-object v1

    :cond_0
    iget-object p1, p0, Ly0/o;->M:Ly0/l;

    if-nez p1, :cond_1

    iput-object v1, p0, Ly0/o;->R:Ljava/util/ArrayDeque;

    invoke-virtual {p0}, Ly0/o;->K0()V

    return-object v1

    :cond_1
    iget-object v1, p0, Ly0/o;->T:Ly0/n;

    iget-object v4, p0, Ly0/o;->N:Lh0/r1;

    iget-object v2, p0, Ly0/o;->F:Ll0/o;

    iget-object v3, p0, Ly0/o;->G:Ll0/o;

    invoke-direct {p0, v1, v5, v2, v3}, Ly0/o;->m0(Ly0/n;Lh0/r1;Ll0/o;Ll0/o;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Ly0/o;->j0()V

    new-instance p1, Lk0/i;

    iget-object v3, v1, Ly0/n;->a:Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v7, 0x80

    move-object v2, p1

    invoke-direct/range {v2 .. v7}, Lk0/i;-><init>(Ljava/lang/String;Lh0/r1;Lh0/r1;II)V

    return-object p1

    :cond_2
    iget-object v2, p0, Ly0/o;->G:Ll0/o;

    iget-object v3, p0, Ly0/o;->F:Ll0/o;

    const/4 v6, 0x0

    if-eq v2, v3, :cond_3

    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_5

    sget v3, Le2/n0;->a:I

    const/16 v7, 0x17

    if-lt v3, v7, :cond_4

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    :cond_5
    :goto_1
    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, Le2/a;->f(Z)V

    invoke-virtual {p0, v1, v4, v5}, Ly0/o;->W(Ly0/n;Lh0/r1;Lh0/r1;)Lk0/i;

    move-result-object v3

    iget v7, v3, Lk0/i;->d:I

    const/4 v8, 0x3

    const/16 v9, 0x10

    const/4 v10, 0x2

    if-eqz v7, :cond_f

    if-eq v7, v0, :cond_c

    if-eq v7, v10, :cond_8

    if-ne v7, v8, :cond_7

    invoke-direct {p0, v5}, Ly0/o;->r1(Lh0/r1;)Z

    move-result v0

    if-nez v0, :cond_6

    goto :goto_4

    :cond_6
    iput-object v5, p0, Ly0/o;->N:Lh0/r1;

    if-eqz v2, :cond_10

    invoke-direct {p0}, Ly0/o;->k0()Z

    move-result v0

    if-nez v0, :cond_10

    goto :goto_5

    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_8
    invoke-direct {p0, v5}, Ly0/o;->r1(Lh0/r1;)Z

    move-result v7

    if-nez v7, :cond_9

    goto :goto_4

    :cond_9
    iput-boolean v0, p0, Ly0/o;->p0:Z

    iput v0, p0, Ly0/o;->q0:I

    iget v7, p0, Ly0/o;->U:I

    if-eq v7, v10, :cond_b

    if-ne v7, v0, :cond_a

    iget v7, v5, Lh0/r1;->v:I

    iget v9, v4, Lh0/r1;->v:I

    if-ne v7, v9, :cond_a

    iget v7, v5, Lh0/r1;->w:I

    iget v9, v4, Lh0/r1;->w:I

    if-ne v7, v9, :cond_a

    goto :goto_3

    :cond_a
    const/4 v0, 0x0

    :cond_b
    :goto_3
    iput-boolean v0, p0, Ly0/o;->c0:Z

    iput-object v5, p0, Ly0/o;->N:Lh0/r1;

    if-eqz v2, :cond_10

    invoke-direct {p0}, Ly0/o;->k0()Z

    move-result v0

    if-nez v0, :cond_10

    goto :goto_5

    :cond_c
    invoke-direct {p0, v5}, Ly0/o;->r1(Lh0/r1;)Z

    move-result v0

    if-nez v0, :cond_d

    :goto_4
    const/16 v7, 0x10

    goto :goto_6

    :cond_d
    iput-object v5, p0, Ly0/o;->N:Lh0/r1;

    if-eqz v2, :cond_e

    invoke-direct {p0}, Ly0/o;->k0()Z

    move-result v0

    if-nez v0, :cond_10

    goto :goto_5

    :cond_e
    invoke-direct {p0}, Ly0/o;->i0()Z

    move-result v0

    if-nez v0, :cond_10

    :goto_5
    const/4 v7, 0x2

    goto :goto_6

    :cond_f
    invoke-direct {p0}, Ly0/o;->j0()V

    :cond_10
    const/4 v7, 0x0

    :goto_6
    iget v0, v3, Lk0/i;->d:I

    if-eqz v0, :cond_12

    iget-object v0, p0, Ly0/o;->M:Ly0/l;

    if-ne v0, p1, :cond_11

    iget p1, p0, Ly0/o;->s0:I

    if-ne p1, v8, :cond_12

    :cond_11
    new-instance p1, Lk0/i;

    iget-object v3, v1, Ly0/n;->a:Ljava/lang/String;

    const/4 v6, 0x0

    move-object v2, p1

    invoke-direct/range {v2 .. v7}, Lk0/i;-><init>(Ljava/lang/String;Lh0/r1;Lh0/r1;II)V

    return-object p1

    :cond_12
    return-object v3

    :cond_13
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    const/16 v0, 0xfa5

    invoke-virtual {p0, p1, v5, v0}, Lh0/f;->C(Ljava/lang/Throwable;Lh0/r1;I)Lh0/q;

    move-result-object p1

    throw p1
.end method

.method protected Q([Lh0/r1;JJ)V
    .locals 15

    move-object v0, p0

    iget-object v1, v0, Ly0/o;->E0:Ly0/o$c;

    iget-wide v1, v1, Ly0/o$c;->c:J

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v5, v1, v3

    if-nez v5, :cond_0

    new-instance v1, Ly0/o$c;

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    move-object v6, v1

    move-wide/from16 v9, p2

    move-wide/from16 v11, p4

    invoke-direct/range {v6 .. v12}, Ly0/o$c;-><init>(JJJ)V

    invoke-direct {p0, v1}, Ly0/o;->h1(Ly0/o$c;)V

    goto :goto_0

    :cond_0
    iget-object v1, v0, Ly0/o;->C:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-wide v1, v0, Ly0/o;->w0:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1

    iget-wide v5, v0, Ly0/o;->F0:J

    cmp-long v7, v5, v3

    if-eqz v7, :cond_2

    cmp-long v7, v5, v1

    if-ltz v7, :cond_2

    :cond_1
    new-instance v1, Ly0/o$c;

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    move-object v8, v1

    move-wide/from16 v11, p2

    move-wide/from16 v13, p4

    invoke-direct/range {v8 .. v14}, Ly0/o$c;-><init>(JJJ)V

    invoke-direct {p0, v1}, Ly0/o;->h1(Ly0/o$c;)V

    iget-object v1, v0, Ly0/o;->E0:Ly0/o$c;

    iget-wide v1, v1, Ly0/o$c;->c:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Ly0/o;->T0()V

    goto :goto_0

    :cond_2
    iget-object v1, v0, Ly0/o;->C:Ljava/util/ArrayDeque;

    new-instance v9, Ly0/o$c;

    iget-wide v3, v0, Ly0/o;->w0:J

    move-object v2, v9

    move-wide/from16 v5, p2

    move-wide/from16 v7, p4

    invoke-direct/range {v2 .. v8}, Ly0/o$c;-><init>(JJJ)V

    invoke-virtual {v1, v9}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_0
    return-void
.end method

.method protected abstract Q0(Lh0/r1;Landroid/media/MediaFormat;)V
.end method

.method protected R0(J)V
    .locals 0

    return-void
.end method

.method protected S0(J)V
    .locals 3

    iput-wide p1, p0, Ly0/o;->F0:J

    :goto_0
    iget-object v0, p0, Ly0/o;->C:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ly0/o;->C:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly0/o$c;

    iget-wide v0, v0, Ly0/o$c;->a:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    iget-object v0, p0, Ly0/o;->C:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly0/o$c;

    invoke-direct {p0, v0}, Ly0/o;->h1(Ly0/o$c;)V

    invoke-virtual {p0}, Ly0/o;->T0()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected T0()V
    .locals 0

    return-void
.end method

.method protected abstract U0(Lk0/g;)V
.end method

.method protected abstract W(Ly0/n;Lh0/r1;Lh0/r1;)Lk0/i;
.end method

.method protected abstract W0(JJLy0/l;Ljava/nio/ByteBuffer;IIIJZZLh0/r1;)Z
.end method

.method public final a(Lh0/r1;)I
    .locals 2

    :try_start_0
    iget-object v0, p0, Ly0/o;->t:Ly0/q;

    invoke-virtual {p0, v0, p1}, Ly0/o;->p1(Ly0/q;Lh0/r1;)I

    move-result p1
    :try_end_0
    .catch Ly0/v$c; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception v0

    const/16 v1, 0xfa2

    invoke-virtual {p0, v0, p1, v1}, Lh0/f;->C(Ljava/lang/Throwable;Lh0/r1;I)Lh0/q;

    move-result-object p1

    throw p1
.end method

.method protected a1()V
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Ly0/o;->M:Ly0/l;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ly0/l;->a()V

    iget-object v1, p0, Ly0/o;->D0:Lk0/e;

    iget v2, v1, Lk0/e;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lk0/e;->b:I

    iget-object v1, p0, Ly0/o;->T:Ly0/n;

    iget-object v1, v1, Ly0/n;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ly0/o;->O0(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_0
    iput-object v0, p0, Ly0/o;->M:Ly0/l;

    :try_start_1
    iget-object v1, p0, Ly0/o;->H:Landroid/media/MediaCrypto;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/media/MediaCrypto;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    iput-object v0, p0, Ly0/o;->H:Landroid/media/MediaCrypto;

    invoke-direct {p0, v0}, Ly0/o;->g1(Ll0/o;)V

    invoke-virtual {p0}, Ly0/o;->d1()V

    return-void

    :catchall_0
    move-exception v1

    iput-object v0, p0, Ly0/o;->H:Landroid/media/MediaCrypto;

    invoke-direct {p0, v0}, Ly0/o;->g1(Ll0/o;)V

    invoke-virtual {p0}, Ly0/o;->d1()V

    throw v1

    :catchall_1
    move-exception v1

    iput-object v0, p0, Ly0/o;->M:Ly0/l;

    :try_start_2
    iget-object v2, p0, Ly0/o;->H:Landroid/media/MediaCrypto;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/media/MediaCrypto;->release()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :cond_2
    iput-object v0, p0, Ly0/o;->H:Landroid/media/MediaCrypto;

    invoke-direct {p0, v0}, Ly0/o;->g1(Ll0/o;)V

    invoke-virtual {p0}, Ly0/o;->d1()V

    throw v1

    :catchall_2
    move-exception v1

    iput-object v0, p0, Ly0/o;->H:Landroid/media/MediaCrypto;

    invoke-direct {p0, v0}, Ly0/o;->g1(Ll0/o;)V

    invoke-virtual {p0}, Ly0/o;->d1()V

    throw v1
.end method

.method protected b1()V
    .locals 0

    return-void
.end method

.method protected c1()V
    .locals 4

    invoke-direct {p0}, Ly0/o;->e1()V

    invoke-direct {p0}, Ly0/o;->f1()V

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Ly0/o;->g0:J

    const/4 v2, 0x0

    iput-boolean v2, p0, Ly0/o;->u0:Z

    iput-boolean v2, p0, Ly0/o;->t0:Z

    iput-boolean v2, p0, Ly0/o;->c0:Z

    iput-boolean v2, p0, Ly0/o;->d0:Z

    iput-boolean v2, p0, Ly0/o;->k0:Z

    iput-boolean v2, p0, Ly0/o;->l0:Z

    iget-object v3, p0, Ly0/o;->A:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    iput-wide v0, p0, Ly0/o;->w0:J

    iput-wide v0, p0, Ly0/o;->x0:J

    iput-wide v0, p0, Ly0/o;->F0:J

    iget-object v0, p0, Ly0/o;->f0:Ly0/i;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ly0/i;->c()V

    :cond_0
    iput v2, p0, Ly0/o;->r0:I

    iput v2, p0, Ly0/o;->s0:I

    iget-boolean v0, p0, Ly0/o;->p0:Z

    iput v0, p0, Ly0/o;->q0:I

    return-void
.end method

.method protected d1()V
    .locals 2

    invoke-virtual {p0}, Ly0/o;->c1()V

    const/4 v0, 0x0

    iput-object v0, p0, Ly0/o;->C0:Lh0/q;

    iput-object v0, p0, Ly0/o;->f0:Ly0/i;

    iput-object v0, p0, Ly0/o;->R:Ljava/util/ArrayDeque;

    iput-object v0, p0, Ly0/o;->T:Ly0/n;

    iput-object v0, p0, Ly0/o;->N:Lh0/r1;

    iput-object v0, p0, Ly0/o;->O:Landroid/media/MediaFormat;

    const/4 v0, 0x0

    iput-boolean v0, p0, Ly0/o;->P:Z

    iput-boolean v0, p0, Ly0/o;->v0:Z

    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Ly0/o;->Q:F

    iput v0, p0, Ly0/o;->U:I

    iput-boolean v0, p0, Ly0/o;->V:Z

    iput-boolean v0, p0, Ly0/o;->W:Z

    iput-boolean v0, p0, Ly0/o;->X:Z

    iput-boolean v0, p0, Ly0/o;->Y:Z

    iput-boolean v0, p0, Ly0/o;->Z:Z

    iput-boolean v0, p0, Ly0/o;->a0:Z

    iput-boolean v0, p0, Ly0/o;->b0:Z

    iput-boolean v0, p0, Ly0/o;->e0:Z

    iput-boolean v0, p0, Ly0/o;->p0:Z

    iput v0, p0, Ly0/o;->q0:I

    iput-boolean v0, p0, Ly0/o;->I:Z

    return-void
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Ly0/o;->z0:Z

    return v0
.end method

.method protected g0(Ljava/lang/Throwable;Ly0/n;)Ly0/m;
    .locals 1

    new-instance v0, Ly0/m;

    invoke-direct {v0, p1, p2}, Ly0/m;-><init>(Ljava/lang/Throwable;Ly0/n;)V

    return-object v0
.end method

.method protected final i1()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ly0/o;->B0:Z

    return-void
.end method

.method protected final j1(Lh0/q;)V
    .locals 0

    iput-object p1, p0, Ly0/o;->C0:Lh0/q;

    return-void
.end method

.method public k()Z
    .locals 5

    iget-object v0, p0, Ly0/o;->D:Lh0/r1;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lh0/f;->J()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Ly0/o;->D0()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Ly0/o;->g0:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Ly0/o;->g0:J

    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected m1(Ly0/n;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method protected n1()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final o()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method protected o1(Lh0/r1;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public p(JJ)V
    .locals 5

    iget-boolean v0, p0, Ly0/o;->B0:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Ly0/o;->B0:Z

    invoke-direct {p0}, Ly0/o;->V0()V

    :cond_0
    iget-object v0, p0, Ly0/o;->C0:Lh0/q;

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :try_start_0
    iget-boolean v2, p0, Ly0/o;->z0:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Ly0/o;->b1()V

    return-void

    :cond_1
    iget-object v2, p0, Ly0/o;->D:Lh0/r1;

    if-nez v2, :cond_2

    const/4 v2, 0x2

    invoke-direct {p0, v2}, Ly0/o;->Y0(I)Z

    move-result v2

    if-nez v2, :cond_2

    return-void

    :cond_2
    invoke-virtual {p0}, Ly0/o;->K0()V

    iget-boolean v2, p0, Ly0/o;->m0:Z

    if-eqz v2, :cond_4

    const-string v2, "bypassRender"

    invoke-static {v2}, Le2/k0;->a(Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0, p1, p2, p3, p4}, Ly0/o;->V(JJ)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0

    :cond_3
    invoke-static {}, Le2/k0;->c()V

    goto :goto_3

    :cond_4
    iget-object v2, p0, Ly0/o;->M:Ly0/l;

    if-eqz v2, :cond_6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-string v4, "drainAndFeed"

    invoke-static {v4}, Le2/k0;->a(Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0, p1, p2, p3, p4}, Ly0/o;->l0(JJ)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-direct {p0, v2, v3}, Ly0/o;->l1(J)Z

    move-result v4

    if-eqz v4, :cond_5

    goto :goto_1

    :cond_5
    :goto_2
    invoke-direct {p0}, Ly0/o;->n0()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-direct {p0, v2, v3}, Ly0/o;->l1(J)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_2

    :cond_6
    iget-object p3, p0, Ly0/o;->D0:Lk0/e;

    iget p4, p3, Lk0/e;->d:I

    invoke-virtual {p0, p1, p2}, Lh0/f;->T(J)I

    move-result p1

    add-int/2addr p4, p1

    iput p4, p3, Lk0/e;->d:I

    invoke-direct {p0, v0}, Ly0/o;->Y0(I)Z

    :goto_3
    iget-object p1, p0, Ly0/o;->D0:Lk0/e;

    invoke-virtual {p1}, Lk0/e;->c()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Ly0/o;->H0(Ljava/lang/IllegalStateException;)Z

    move-result p2

    if-eqz p2, :cond_9

    invoke-virtual {p0, p1}, Ly0/o;->M0(Ljava/lang/Exception;)V

    sget p2, Le2/n0;->a:I

    const/16 p3, 0x15

    if-lt p2, p3, :cond_7

    invoke-static {p1}, Ly0/o;->J0(Ljava/lang/IllegalStateException;)Z

    move-result p2

    if-eqz p2, :cond_7

    const/4 v1, 0x1

    :cond_7
    if-eqz v1, :cond_8

    invoke-virtual {p0}, Ly0/o;->a1()V

    :cond_8
    invoke-virtual {p0}, Ly0/o;->t0()Ly0/n;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Ly0/o;->g0(Ljava/lang/Throwable;Ly0/n;)Ly0/m;

    move-result-object p1

    iget-object p2, p0, Ly0/o;->D:Lh0/r1;

    const/16 p3, 0xfa3

    invoke-virtual {p0, p1, p2, v1, p3}, Lh0/f;->D(Ljava/lang/Throwable;Lh0/r1;ZI)Lh0/q;

    move-result-object p1

    throw p1

    :cond_9
    throw p1

    :cond_a
    const/4 p1, 0x0

    iput-object p1, p0, Ly0/o;->C0:Lh0/q;

    throw v0
.end method

.method protected final p0()Z
    .locals 1

    invoke-virtual {p0}, Ly0/o;->q0()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ly0/o;->K0()V

    :cond_0
    return v0
.end method

.method protected abstract p1(Ly0/q;Lh0/r1;)I
.end method

.method protected q0()Z
    .locals 5

    iget-object v0, p0, Ly0/o;->M:Ly0/l;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget v0, p0, Ly0/o;->s0:I

    const/4 v2, 0x3

    const/4 v3, 0x1

    if-eq v0, v2, :cond_5

    iget-boolean v2, p0, Ly0/o;->W:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, Ly0/o;->X:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Ly0/o;->v0:Z

    if-eqz v2, :cond_5

    :cond_1
    iget-boolean v2, p0, Ly0/o;->Y:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Ly0/o;->u0:Z

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    sget v0, Le2/n0;->a:I

    const/16 v2, 0x17

    if-lt v0, v2, :cond_3

    const/4 v4, 0x1

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    :goto_0
    invoke-static {v4}, Le2/a;->f(Z)V

    if-lt v0, v2, :cond_4

    :try_start_0
    invoke-direct {p0}, Ly0/o;->s1()V
    :try_end_0
    .catch Lh0/q; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "MediaCodecRenderer"

    const-string v2, "Failed to update the DRM session, releasing the codec instead."

    invoke-static {v1, v2, v0}, Le2/r;->j(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Ly0/o;->a1()V

    return v3

    :cond_4
    :goto_1
    invoke-direct {p0}, Ly0/o;->o0()V

    return v1

    :cond_5
    :goto_2
    invoke-virtual {p0}, Ly0/o;->a1()V

    return v3
.end method

.method protected final s0()Ly0/l;
    .locals 1

    iget-object v0, p0, Ly0/o;->M:Ly0/l;

    return-object v0
.end method

.method protected final t0()Ly0/n;
    .locals 1

    iget-object v0, p0, Ly0/o;->T:Ly0/n;

    return-object v0
.end method

.method protected final t1(J)V
    .locals 1

    iget-object v0, p0, Ly0/o;->E0:Ly0/o$c;

    iget-object v0, v0, Ly0/o$c;->d:Le2/i0;

    invoke-virtual {v0, p1, p2}, Le2/i0;->i(J)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh0/r1;

    if-nez p1, :cond_0

    iget-boolean p2, p0, Ly0/o;->G0:Z

    if-eqz p2, :cond_0

    iget-object p2, p0, Ly0/o;->O:Landroid/media/MediaFormat;

    if-eqz p2, :cond_0

    iget-object p1, p0, Ly0/o;->E0:Ly0/o$c;

    iget-object p1, p1, Ly0/o$c;->d:Le2/i0;

    invoke-virtual {p1}, Le2/i0;->h()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh0/r1;

    :cond_0
    const/4 p2, 0x0

    if-eqz p1, :cond_1

    iput-object p1, p0, Ly0/o;->E:Lh0/r1;

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_2

    iget-boolean p1, p0, Ly0/o;->P:Z

    if-eqz p1, :cond_3

    iget-object p1, p0, Ly0/o;->E:Lh0/r1;

    if-eqz p1, :cond_3

    :cond_2
    iget-object p1, p0, Ly0/o;->E:Lh0/r1;

    iget-object v0, p0, Ly0/o;->O:Landroid/media/MediaFormat;

    invoke-virtual {p0, p1, v0}, Ly0/o;->Q0(Lh0/r1;Landroid/media/MediaFormat;)V

    iput-boolean p2, p0, Ly0/o;->P:Z

    iput-boolean p2, p0, Ly0/o;->G0:Z

    :cond_3
    return-void
.end method

.method protected u0()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract v0(FLh0/r1;[Lh0/r1;)F
.end method

.method protected final w0()Landroid/media/MediaFormat;
    .locals 1

    iget-object v0, p0, Ly0/o;->O:Landroid/media/MediaFormat;

    return-object v0
.end method

.method protected abstract x0(Ly0/q;Lh0/r1;Z)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ly0/q;",
            "Lh0/r1;",
            "Z)",
            "Ljava/util/List<",
            "Ly0/n;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract z0(Ly0/n;Lh0/r1;Landroid/media/MediaCrypto;F)Ly0/l$a;
.end method
