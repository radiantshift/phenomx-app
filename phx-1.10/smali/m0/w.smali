.class public Lm0/w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lm0/m;


# instance fields
.field private final a:Lm0/m;


# direct methods
.method public constructor <init>(Lm0/m;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lm0/w;->a:Lm0/m;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0}, Lm0/m;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public c(I)I
    .locals 1

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0, p1}, Lm0/m;->c(I)I

    move-result p1

    return p1
.end method

.method public d([BIIZ)Z
    .locals 1

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0, p1, p2, p3, p4}, Lm0/m;->d([BIIZ)Z

    move-result p1

    return p1
.end method

.method public e([BII)I
    .locals 1

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0, p1, p2, p3}, Lm0/m;->e([BII)I

    move-result p1

    return p1
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0}, Lm0/m;->g()V

    return-void
.end method

.method public h(I)V
    .locals 1

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0, p1}, Lm0/m;->h(I)V

    return-void
.end method

.method public j(IZ)Z
    .locals 1

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0, p1, p2}, Lm0/m;->j(IZ)Z

    move-result p1

    return p1
.end method

.method public l([BIIZ)Z
    .locals 1

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0, p1, p2, p3, p4}, Lm0/m;->l([BIIZ)Z

    move-result p1

    return p1
.end method

.method public m()J
    .locals 2

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0}, Lm0/m;->m()J

    move-result-wide v0

    return-wide v0
.end method

.method public n([BII)V
    .locals 1

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0, p1, p2, p3}, Lm0/m;->n([BII)V

    return-void
.end method

.method public o(I)V
    .locals 1

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0, p1}, Lm0/m;->o(I)V

    return-void
.end method

.method public p()J
    .locals 2

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0}, Lm0/m;->p()J

    move-result-wide v0

    return-wide v0
.end method

.method public read([BII)I
    .locals 1

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0, p1, p2, p3}, Lm0/m;->read([BII)I

    move-result p1

    return p1
.end method

.method public readFully([BII)V
    .locals 1

    iget-object v0, p0, Lm0/w;->a:Lm0/m;

    invoke-interface {v0, p1, p2, p3}, Lm0/m;->readFully([BII)V

    return-void
.end method
