.class public final Lg6/a;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private static final a(Ln5/d;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/d<",
            "*>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    sget-object v0, Lk5/m;->f:Lk5/m$a;

    invoke-static {p1}, Lk5/n;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lk5/m;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0}, Ln5/d;->resumeWith(Ljava/lang/Object;)V

    throw p1
.end method

.method public static final b(Ln5/d;Ln5/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln5/d<",
            "-",
            "Lk5/s;",
            ">;",
            "Ln5/d<",
            "*>;)V"
        }
    .end annotation

    :try_start_0
    invoke-static {p0}, Lo5/b;->b(Ln5/d;)Ln5/d;

    move-result-object p0

    sget-object v0, Lk5/m;->f:Lk5/m$a;

    sget-object v0, Lk5/s;->a:Lk5/s;

    invoke-static {v0}, Lk5/m;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p0, v0, v2, v1, v2}, Lkotlinx/coroutines/internal/i;->c(Ln5/d;Ljava/lang/Object;Lu5/l;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p1, p0}, Lg6/a;->a(Ln5/d;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public static final c(Lu5/p;Ljava/lang/Object;Ln5/d;Lu5/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "Lu5/p<",
            "-TR;-",
            "Ln5/d<",
            "-TT;>;+",
            "Ljava/lang/Object;",
            ">;TR;",
            "Ln5/d<",
            "-TT;>;",
            "Lu5/l<",
            "-",
            "Ljava/lang/Throwable;",
            "Lk5/s;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    invoke-static {p0, p1, p2}, Lo5/b;->a(Lu5/p;Ljava/lang/Object;Ln5/d;)Ln5/d;

    move-result-object p0

    invoke-static {p0}, Lo5/b;->b(Ln5/d;)Ln5/d;

    move-result-object p0

    sget-object p1, Lk5/m;->f:Lk5/m$a;

    sget-object p1, Lk5/s;->a:Lk5/s;

    invoke-static {p1}, Lk5/m;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p0, p1, p3}, Lkotlinx/coroutines/internal/i;->b(Ln5/d;Ljava/lang/Object;Lu5/l;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p2, p0}, Lg6/a;->a(Ln5/d;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public static synthetic d(Lu5/p;Ljava/lang/Object;Ln5/d;Lu5/l;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lg6/a;->c(Lu5/p;Ljava/lang/Object;Ln5/d;Lu5/l;)V

    return-void
.end method
