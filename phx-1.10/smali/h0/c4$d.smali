.class public final Lh0/c4$d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh0/c4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# static fields
.field private static final A:Ljava/lang/String;

.field private static final B:Ljava/lang/String;

.field private static final C:Ljava/lang/String;

.field private static final D:Ljava/lang/String;

.field private static final E:Ljava/lang/String;

.field private static final F:Ljava/lang/String;

.field private static final G:Ljava/lang/String;

.field private static final H:Ljava/lang/String;

.field private static final I:Ljava/lang/String;

.field private static final J:Ljava/lang/String;

.field private static final K:Ljava/lang/String;

.field private static final L:Ljava/lang/String;

.field public static final M:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Lh0/c4$d;",
            ">;"
        }
    .end annotation
.end field

.field public static final w:Ljava/lang/Object;

.field private static final x:Ljava/lang/Object;

.field private static final y:Lh0/z1;

.field private static final z:Ljava/lang/String;


# instance fields
.field public f:Ljava/lang/Object;

.field public g:Ljava/lang/Object;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public h:Lh0/z1;

.field public i:Ljava/lang/Object;

.field public j:J

.field public k:J

.field public l:J

.field public m:Z

.field public n:Z

.field public o:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public p:Lh0/z1$g;

.field public q:Z

.field public r:J

.field public s:J

.field public t:I

.field public u:I

.field public v:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lh0/c4$d;->w:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lh0/c4$d;->x:Ljava/lang/Object;

    new-instance v0, Lh0/z1$c;

    invoke-direct {v0}, Lh0/z1$c;-><init>()V

    const-string v1, "com.google.android.exoplayer2.Timeline"

    invoke-virtual {v0, v1}, Lh0/z1$c;->c(Ljava/lang/String;)Lh0/z1$c;

    move-result-object v0

    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lh0/z1$c;->f(Landroid/net/Uri;)Lh0/z1$c;

    move-result-object v0

    invoke-virtual {v0}, Lh0/z1$c;->a()Lh0/z1;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->y:Lh0/z1;

    const/4 v0, 0x1

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->z:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->A:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->B:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->C:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->D:Ljava/lang/String;

    const/4 v0, 0x6

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->E:Ljava/lang/String;

    const/4 v0, 0x7

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->F:Ljava/lang/String;

    const/16 v0, 0x8

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->G:Ljava/lang/String;

    const/16 v0, 0x9

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->H:Ljava/lang/String;

    const/16 v0, 0xa

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->I:Ljava/lang/String;

    const/16 v0, 0xb

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->J:Ljava/lang/String;

    const/16 v0, 0xc

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->K:Ljava/lang/String;

    const/16 v0, 0xd

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/c4$d;->L:Ljava/lang/String;

    sget-object v0, Lh0/e4;->a:Lh0/e4;

    sput-object v0, Lh0/c4$d;->M:Lh0/h$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lh0/c4$d;->w:Ljava/lang/Object;

    iput-object v0, p0, Lh0/c4$d;->f:Ljava/lang/Object;

    sget-object v0, Lh0/c4$d;->y:Lh0/z1;

    iput-object v0, p0, Lh0/c4$d;->h:Lh0/z1;

    return-void
.end method

.method public static synthetic a(Landroid/os/Bundle;)Lh0/c4$d;
    .locals 0

    invoke-static {p0}, Lh0/c4$d;->b(Landroid/os/Bundle;)Lh0/c4$d;

    move-result-object p0

    return-object p0
.end method

.method private static b(Landroid/os/Bundle;)Lh0/c4$d;
    .locals 25

    move-object/from16 v0, p0

    sget-object v1, Lh0/c4$d;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lh0/z1;->t:Lh0/h$a;

    invoke-interface {v2, v1}, Lh0/h$a;->a(Landroid/os/Bundle;)Lh0/h;

    move-result-object v1

    check-cast v1, Lh0/z1;

    goto :goto_0

    :cond_0
    sget-object v1, Lh0/z1;->n:Lh0/z1;

    :goto_0
    move-object v4, v1

    sget-object v1, Lh0/c4$d;->A:Ljava/lang/String;

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    sget-object v1, Lh0/c4$d;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    sget-object v1, Lh0/c4$d;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    sget-object v1, Lh0/c4$d;->D:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    sget-object v1, Lh0/c4$d;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    sget-object v1, Lh0/c4$d;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v14, Lh0/z1$g;->q:Lh0/h$a;

    invoke-interface {v14, v1}, Lh0/h$a;->a(Landroid/os/Bundle;)Lh0/h;

    move-result-object v1

    check-cast v1, Lh0/z1$g;

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    move-object v14, v1

    sget-object v1, Lh0/c4$d;->G:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sget-object v15, Lh0/c4$d;->H:Ljava/lang/String;

    move-wide/from16 v23, v6

    const-wide/16 v5, 0x0

    invoke-virtual {v0, v15, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v15

    sget-object v7, Lh0/c4$d;->I:Ljava/lang/String;

    invoke-virtual {v0, v7, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v17

    sget-object v2, Lh0/c4$d;->J:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v19

    sget-object v2, Lh0/c4$d;->K:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v20

    sget-object v2, Lh0/c4$d;->L:Ljava/lang/String;

    invoke-virtual {v0, v2, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v21

    new-instance v0, Lh0/c4$d;

    move-object v2, v0

    invoke-direct {v0}, Lh0/c4$d;-><init>()V

    sget-object v3, Lh0/c4$d;->x:Ljava/lang/Object;

    const/4 v5, 0x0

    move-wide/from16 v6, v23

    invoke-virtual/range {v2 .. v22}, Lh0/c4$d;->i(Ljava/lang/Object;Lh0/z1;Ljava/lang/Object;JJJZZLh0/z1$g;JJIIJ)Lh0/c4$d;

    iput-boolean v1, v0, Lh0/c4$d;->q:Z

    return-object v0
.end method


# virtual methods
.method public c()J
    .locals 2

    iget-wide v0, p0, Lh0/c4$d;->l:J

    invoke-static {v0, v1}, Le2/n0;->a0(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lh0/c4$d;->r:J

    invoke-static {v0, v1}, Le2/n0;->Z0(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public e()J
    .locals 2

    iget-wide v0, p0, Lh0/c4$d;->r:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const-class v2, Lh0/c4$d;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    goto/16 :goto_1

    :cond_1
    check-cast p1, Lh0/c4$d;

    iget-object v2, p0, Lh0/c4$d;->f:Ljava/lang/Object;

    iget-object v3, p1, Lh0/c4$d;->f:Ljava/lang/Object;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/c4$d;->h:Lh0/z1;

    iget-object v3, p1, Lh0/c4$d;->h:Lh0/z1;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/c4$d;->i:Ljava/lang/Object;

    iget-object v3, p1, Lh0/c4$d;->i:Ljava/lang/Object;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/c4$d;->p:Lh0/z1$g;

    iget-object v3, p1, Lh0/c4$d;->p:Lh0/z1$g;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lh0/c4$d;->j:J

    iget-wide v4, p1, Lh0/c4$d;->j:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    iget-wide v2, p0, Lh0/c4$d;->k:J

    iget-wide v4, p1, Lh0/c4$d;->k:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    iget-wide v2, p0, Lh0/c4$d;->l:J

    iget-wide v4, p1, Lh0/c4$d;->l:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    iget-boolean v2, p0, Lh0/c4$d;->m:Z

    iget-boolean v3, p1, Lh0/c4$d;->m:Z

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lh0/c4$d;->n:Z

    iget-boolean v3, p1, Lh0/c4$d;->n:Z

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lh0/c4$d;->q:Z

    iget-boolean v3, p1, Lh0/c4$d;->q:Z

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Lh0/c4$d;->r:J

    iget-wide v4, p1, Lh0/c4$d;->r:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    iget-wide v2, p0, Lh0/c4$d;->s:J

    iget-wide v4, p1, Lh0/c4$d;->s:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    iget v2, p0, Lh0/c4$d;->t:I

    iget v3, p1, Lh0/c4$d;->t:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lh0/c4$d;->u:I

    iget v3, p1, Lh0/c4$d;->u:I

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Lh0/c4$d;->v:J

    iget-wide v4, p1, Lh0/c4$d;->v:J

    cmp-long p1, v2, v4

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public f()J
    .locals 2

    iget-wide v0, p0, Lh0/c4$d;->s:J

    invoke-static {v0, v1}, Le2/n0;->Z0(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()J
    .locals 2

    iget-wide v0, p0, Lh0/c4$d;->v:J

    return-wide v0
.end method

.method public h()Z
    .locals 4

    iget-boolean v0, p0, Lh0/c4$d;->o:Z

    iget-object v1, p0, Lh0/c4$d;->p:Lh0/z1$g;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Le2/a;->f(Z)V

    iget-object v0, p0, Lh0/c4$d;->p:Lh0/z1$g;

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    return v2
.end method

.method public hashCode()I
    .locals 6

    iget-object v0, p0, Lh0/c4$d;->f:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const/16 v1, 0xd9

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lh0/c4$d;->h:Lh0/z1;

    invoke-virtual {v0}, Lh0/z1;->hashCode()I

    move-result v0

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lh0/c4$d;->i:Ljava/lang/Object;

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v0, p0, Lh0/c4$d;->p:Lh0/z1$g;

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lh0/z1$g;->hashCode()I

    move-result v2

    :goto_1
    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lh0/c4$d;->j:J

    const/16 v0, 0x20

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v3, v2

    add-int/2addr v1, v3

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lh0/c4$d;->k:J

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v3, v2

    add-int/2addr v1, v3

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lh0/c4$d;->l:J

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v3, v2

    add-int/2addr v1, v3

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v2, p0, Lh0/c4$d;->m:Z

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v2, p0, Lh0/c4$d;->n:Z

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v2, p0, Lh0/c4$d;->q:Z

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lh0/c4$d;->r:J

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v3, v2

    add-int/2addr v1, v3

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lh0/c4$d;->s:J

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v3, v2

    add-int/2addr v1, v3

    mul-int/lit8 v1, v1, 0x1f

    iget v2, p0, Lh0/c4$d;->t:I

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget v2, p0, Lh0/c4$d;->u:I

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v2, p0, Lh0/c4$d;->v:J

    ushr-long v4, v2, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/2addr v1, v0

    return v1
.end method

.method public i(Ljava/lang/Object;Lh0/z1;Ljava/lang/Object;JJJZZLh0/z1$g;JJIIJ)Lh0/c4$d;
    .locals 5

    move-object v0, p0

    move-object v1, p2

    move-object/from16 v2, p12

    move-object v3, p1

    iput-object v3, v0, Lh0/c4$d;->f:Ljava/lang/Object;

    if-eqz v1, :cond_0

    move-object v3, v1

    goto :goto_0

    :cond_0
    sget-object v3, Lh0/c4$d;->y:Lh0/z1;

    :goto_0
    iput-object v3, v0, Lh0/c4$d;->h:Lh0/z1;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lh0/z1;->g:Lh0/z1$h;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lh0/z1$h;->i:Ljava/lang/Object;

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    iput-object v1, v0, Lh0/c4$d;->g:Ljava/lang/Object;

    move-object v1, p3

    iput-object v1, v0, Lh0/c4$d;->i:Ljava/lang/Object;

    move-wide v3, p4

    iput-wide v3, v0, Lh0/c4$d;->j:J

    move-wide v3, p6

    iput-wide v3, v0, Lh0/c4$d;->k:J

    move-wide v3, p8

    iput-wide v3, v0, Lh0/c4$d;->l:J

    move v1, p10

    iput-boolean v1, v0, Lh0/c4$d;->m:Z

    move/from16 v1, p11

    iput-boolean v1, v0, Lh0/c4$d;->n:Z

    const/4 v1, 0x0

    if-eqz v2, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_2
    iput-boolean v3, v0, Lh0/c4$d;->o:Z

    iput-object v2, v0, Lh0/c4$d;->p:Lh0/z1$g;

    move-wide/from16 v2, p13

    iput-wide v2, v0, Lh0/c4$d;->r:J

    move-wide/from16 v2, p15

    iput-wide v2, v0, Lh0/c4$d;->s:J

    move/from16 v2, p17

    iput v2, v0, Lh0/c4$d;->t:I

    move/from16 v2, p18

    iput v2, v0, Lh0/c4$d;->u:I

    move-wide/from16 v2, p19

    iput-wide v2, v0, Lh0/c4$d;->v:J

    iput-boolean v1, v0, Lh0/c4$d;->q:Z

    return-object v0
.end method
