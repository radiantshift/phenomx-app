.class public abstract Lh0/n3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# static fields
.field static final f:Ljava/lang/String;

.field public static final g:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Lh0/n3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/n3;->f:Ljava/lang/String;

    sget-object v0, Lh0/m3;->a:Lh0/m3;

    sput-object v0, Lh0/n3;->g:Lh0/h$a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Landroid/os/Bundle;)Lh0/n3;
    .locals 0

    invoke-static {p0}, Lh0/n3;->b(Landroid/os/Bundle;)Lh0/n3;

    move-result-object p0

    return-object p0
.end method

.method private static b(Landroid/os/Bundle;)Lh0/n3;
    .locals 3

    sget-object v0, Lh0/n3;->f:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    sget-object v0, Lh0/a4;->l:Lh0/h$a;

    :goto_0
    invoke-interface {v0, p0}, Lh0/h$a;->a(Landroid/os/Bundle;)Lh0/h;

    move-result-object p0

    check-cast p0, Lh0/n3;

    return-object p0

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown RatingType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    sget-object v0, Lh0/w3;->l:Lh0/h$a;

    goto :goto_0

    :cond_2
    sget-object v0, Lh0/a3;->j:Lh0/h$a;

    goto :goto_0

    :cond_3
    sget-object v0, Lh0/u1;->l:Lh0/h$a;

    goto :goto_0
.end method
