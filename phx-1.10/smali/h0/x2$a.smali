.class final Lh0/x2$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lj1/e0;
.implements Ll0/w;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh0/x2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field private final f:Lh0/x2$c;

.field final synthetic g:Lh0/x2;


# direct methods
.method public constructor <init>(Lh0/x2;Lh0/x2$c;)V
    .locals 0

    iput-object p1, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lh0/x2$a;->f:Lh0/x2$c;

    return-void
.end method

.method public static synthetic A(Lh0/x2$a;Landroid/util/Pair;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lh0/x2$a;->Z(Landroid/util/Pair;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V

    return-void
.end method

.method public static synthetic B(Lh0/x2$a;Landroid/util/Pair;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/x2$a;->R(Landroid/util/Pair;)V

    return-void
.end method

.method public static synthetic D(Lh0/x2$a;Landroid/util/Pair;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->T(Landroid/util/Pair;I)V

    return-void
.end method

.method public static synthetic E(Lh0/x2$a;Landroid/util/Pair;Lj1/q;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lh0/x2$a;->X(Landroid/util/Pair;Lj1/q;Lj1/t;)V

    return-void
.end method

.method public static synthetic J(Lh0/x2$a;Landroid/util/Pair;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->V(Landroid/util/Pair;Ljava/lang/Exception;)V

    return-void
.end method

.method private M(ILj1/x$b;)Landroid/util/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lj1/x$b;",
            ")",
            "Landroid/util/Pair<",
            "Ljava/lang/Integer;",
            "Lj1/x$b;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    iget-object v1, p0, Lh0/x2$a;->f:Lh0/x2$c;

    invoke-static {v1, p2}, Lh0/x2;->c(Lh0/x2$c;Lj1/x$b;)Lj1/x$b;

    move-result-object p2

    if-nez p2, :cond_0

    return-object v0

    :cond_0
    move-object v0, p2

    :cond_1
    iget-object p2, p0, Lh0/x2$a;->f:Lh0/x2$c;

    invoke-static {p2, p1}, Lh0/x2;->d(Lh0/x2$c;I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p1

    return-object p1
.end method

.method private synthetic N(Landroid/util/Pair;Lj1/t;)V
    .locals 2

    iget-object v0, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {v0}, Lh0/x2;->e(Lh0/x2;)Li0/a;

    move-result-object v0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lj1/x$b;

    invoke-interface {v0, v1, p1, p2}, Lj1/e0;->I(ILj1/x$b;Lj1/t;)V

    return-void
.end method

.method private synthetic O(Landroid/util/Pair;)V
    .locals 2

    iget-object v0, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {v0}, Lh0/x2;->e(Lh0/x2;)Li0/a;

    move-result-object v0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lj1/x$b;

    invoke-interface {v0, v1, p1}, Ll0/w;->h0(ILj1/x$b;)V

    return-void
.end method

.method private synthetic P(Landroid/util/Pair;)V
    .locals 2

    iget-object v0, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {v0}, Lh0/x2;->e(Lh0/x2;)Li0/a;

    move-result-object v0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lj1/x$b;

    invoke-interface {v0, v1, p1}, Ll0/w;->e0(ILj1/x$b;)V

    return-void
.end method

.method private synthetic R(Landroid/util/Pair;)V
    .locals 2

    iget-object v0, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {v0}, Lh0/x2;->e(Lh0/x2;)Li0/a;

    move-result-object v0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lj1/x$b;

    invoke-interface {v0, v1, p1}, Ll0/w;->F(ILj1/x$b;)V

    return-void
.end method

.method private synthetic T(Landroid/util/Pair;I)V
    .locals 2

    iget-object v0, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {v0}, Lh0/x2;->e(Lh0/x2;)Li0/a;

    move-result-object v0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lj1/x$b;

    invoke-interface {v0, v1, p1, p2}, Ll0/w;->C(ILj1/x$b;I)V

    return-void
.end method

.method private synthetic V(Landroid/util/Pair;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {v0}, Lh0/x2;->e(Lh0/x2;)Li0/a;

    move-result-object v0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lj1/x$b;

    invoke-interface {v0, v1, p1, p2}, Ll0/w;->m0(ILj1/x$b;Ljava/lang/Exception;)V

    return-void
.end method

.method private synthetic W(Landroid/util/Pair;)V
    .locals 2

    iget-object v0, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {v0}, Lh0/x2;->e(Lh0/x2;)Li0/a;

    move-result-object v0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lj1/x$b;

    invoke-interface {v0, v1, p1}, Ll0/w;->L(ILj1/x$b;)V

    return-void
.end method

.method private synthetic X(Landroid/util/Pair;Lj1/q;Lj1/t;)V
    .locals 2

    iget-object v0, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {v0}, Lh0/x2;->e(Lh0/x2;)Li0/a;

    move-result-object v0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lj1/x$b;

    invoke-interface {v0, v1, p1, p2, p3}, Lj1/e0;->H(ILj1/x$b;Lj1/q;Lj1/t;)V

    return-void
.end method

.method private synthetic Y(Landroid/util/Pair;Lj1/q;Lj1/t;)V
    .locals 2

    iget-object v0, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {v0}, Lh0/x2;->e(Lh0/x2;)Li0/a;

    move-result-object v0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lj1/x$b;

    invoke-interface {v0, v1, p1, p2, p3}, Lj1/e0;->d0(ILj1/x$b;Lj1/q;Lj1/t;)V

    return-void
.end method

.method private synthetic Z(Landroid/util/Pair;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V
    .locals 8

    iget-object v0, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {v0}, Lh0/x2;->e(Lh0/x2;)Li0/a;

    move-result-object v1

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object v3, p1

    check-cast v3, Lj1/x$b;

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move v7, p5

    invoke-interface/range {v1 .. v7}, Lj1/e0;->Q(ILj1/x$b;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V

    return-void
.end method

.method private synthetic a0(Landroid/util/Pair;Lj1/q;Lj1/t;)V
    .locals 2

    iget-object v0, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {v0}, Lh0/x2;->e(Lh0/x2;)Li0/a;

    move-result-object v0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lj1/x$b;

    invoke-interface {v0, v1, p1, p2, p3}, Lj1/e0;->U(ILj1/x$b;Lj1/q;Lj1/t;)V

    return-void
.end method

.method public static synthetic b(Lh0/x2$a;Landroid/util/Pair;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->N(Landroid/util/Pair;Lj1/t;)V

    return-void
.end method

.method private synthetic b0(Landroid/util/Pair;Lj1/t;)V
    .locals 2

    iget-object v0, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {v0}, Lh0/x2;->e(Lh0/x2;)Li0/a;

    move-result-object v0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lj1/x$b;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lj1/x$b;

    invoke-interface {v0, v1, p1, p2}, Lj1/e0;->K(ILj1/x$b;Lj1/t;)V

    return-void
.end method

.method public static synthetic h(Lh0/x2$a;Landroid/util/Pair;Lj1/q;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lh0/x2$a;->a0(Landroid/util/Pair;Lj1/q;Lj1/t;)V

    return-void
.end method

.method public static synthetic i(Lh0/x2$a;Landroid/util/Pair;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/x2$a;->W(Landroid/util/Pair;)V

    return-void
.end method

.method public static synthetic j(Lh0/x2$a;Landroid/util/Pair;Lj1/q;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lh0/x2$a;->Y(Landroid/util/Pair;Lj1/q;Lj1/t;)V

    return-void
.end method

.method public static synthetic l(Lh0/x2$a;Landroid/util/Pair;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/x2$a;->O(Landroid/util/Pair;)V

    return-void
.end method

.method public static synthetic w(Lh0/x2$a;Landroid/util/Pair;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/x2$a;->P(Landroid/util/Pair;)V

    return-void
.end method

.method public static synthetic x(Lh0/x2$a;Landroid/util/Pair;Lj1/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->b0(Landroid/util/Pair;Lj1/t;)V

    return-void
.end method


# virtual methods
.method public C(ILj1/x$b;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->M(ILj1/x$b;)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {p2}, Lh0/x2;->b(Lh0/x2;)Le2/n;

    move-result-object p2

    new-instance v0, Lh0/r2;

    invoke-direct {v0, p0, p1, p3}, Lh0/r2;-><init>(Lh0/x2$a;Landroid/util/Pair;I)V

    invoke-interface {p2, v0}, Le2/n;->k(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public F(ILj1/x$b;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->M(ILj1/x$b;)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {p2}, Lh0/x2;->b(Lh0/x2;)Le2/n;

    move-result-object p2

    new-instance v0, Lh0/q2;

    invoke-direct {v0, p0, p1}, Lh0/q2;-><init>(Lh0/x2$a;Landroid/util/Pair;)V

    invoke-interface {p2, v0}, Le2/n;->k(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public H(ILj1/x$b;Lj1/q;Lj1/t;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->M(ILj1/x$b;)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {p2}, Lh0/x2;->b(Lh0/x2;)Le2/n;

    move-result-object p2

    new-instance v0, Lh0/u2;

    invoke-direct {v0, p0, p1, p3, p4}, Lh0/u2;-><init>(Lh0/x2$a;Landroid/util/Pair;Lj1/q;Lj1/t;)V

    invoke-interface {p2, v0}, Le2/n;->k(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public I(ILj1/x$b;Lj1/t;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->M(ILj1/x$b;)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {p2}, Lh0/x2;->b(Lh0/x2;)Le2/n;

    move-result-object p2

    new-instance v0, Lh0/w2;

    invoke-direct {v0, p0, p1, p3}, Lh0/w2;-><init>(Lh0/x2$a;Landroid/util/Pair;Lj1/t;)V

    invoke-interface {p2, v0}, Le2/n;->k(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public K(ILj1/x$b;Lj1/t;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->M(ILj1/x$b;)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {p2}, Lh0/x2;->b(Lh0/x2;)Le2/n;

    move-result-object p2

    new-instance v0, Lh0/m2;

    invoke-direct {v0, p0, p1, p3}, Lh0/m2;-><init>(Lh0/x2$a;Landroid/util/Pair;Lj1/t;)V

    invoke-interface {p2, v0}, Le2/n;->k(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public L(ILj1/x$b;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->M(ILj1/x$b;)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {p2}, Lh0/x2;->b(Lh0/x2;)Le2/n;

    move-result-object p2

    new-instance v0, Lh0/l2;

    invoke-direct {v0, p0, p1}, Lh0/l2;-><init>(Lh0/x2$a;Landroid/util/Pair;)V

    invoke-interface {p2, v0}, Le2/n;->k(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public Q(ILj1/x$b;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V
    .locals 7

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->M(ILj1/x$b;)Landroid/util/Pair;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object p1, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {p1}, Lh0/x2;->b(Lh0/x2;)Le2/n;

    move-result-object p1

    new-instance p2, Lh0/v2;

    move-object v0, p2

    move-object v1, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lh0/v2;-><init>(Lh0/x2$a;Landroid/util/Pair;Lj1/q;Lj1/t;Ljava/io/IOException;Z)V

    invoke-interface {p1, p2}, Le2/n;->k(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public U(ILj1/x$b;Lj1/q;Lj1/t;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->M(ILj1/x$b;)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {p2}, Lh0/x2;->b(Lh0/x2;)Le2/n;

    move-result-object p2

    new-instance v0, Lh0/s2;

    invoke-direct {v0, p0, p1, p3, p4}, Lh0/s2;-><init>(Lh0/x2$a;Landroid/util/Pair;Lj1/q;Lj1/t;)V

    invoke-interface {p2, v0}, Le2/n;->k(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public d0(ILj1/x$b;Lj1/q;Lj1/t;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->M(ILj1/x$b;)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {p2}, Lh0/x2;->b(Lh0/x2;)Le2/n;

    move-result-object p2

    new-instance v0, Lh0/t2;

    invoke-direct {v0, p0, p1, p3, p4}, Lh0/t2;-><init>(Lh0/x2$a;Landroid/util/Pair;Lj1/q;Lj1/t;)V

    invoke-interface {p2, v0}, Le2/n;->k(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public e0(ILj1/x$b;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->M(ILj1/x$b;)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {p2}, Lh0/x2;->b(Lh0/x2;)Le2/n;

    move-result-object p2

    new-instance v0, Lh0/p2;

    invoke-direct {v0, p0, p1}, Lh0/p2;-><init>(Lh0/x2$a;Landroid/util/Pair;)V

    invoke-interface {p2, v0}, Le2/n;->k(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public synthetic g0(ILj1/x$b;)V
    .locals 0

    invoke-static {p0, p1, p2}, Ll0/p;->a(Ll0/w;ILj1/x$b;)V

    return-void
.end method

.method public h0(ILj1/x$b;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->M(ILj1/x$b;)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {p2}, Lh0/x2;->b(Lh0/x2;)Le2/n;

    move-result-object p2

    new-instance v0, Lh0/o2;

    invoke-direct {v0, p0, p1}, Lh0/o2;-><init>(Lh0/x2$a;Landroid/util/Pair;)V

    invoke-interface {p2, v0}, Le2/n;->k(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public m0(ILj1/x$b;Ljava/lang/Exception;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lh0/x2$a;->M(ILj1/x$b;)Landroid/util/Pair;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lh0/x2$a;->g:Lh0/x2;

    invoke-static {p2}, Lh0/x2;->b(Lh0/x2;)Le2/n;

    move-result-object p2

    new-instance v0, Lh0/n2;

    invoke-direct {v0, p0, p1, p3}, Lh0/n2;-><init>(Lh0/x2$a;Landroid/util/Pair;Ljava/lang/Exception;)V

    invoke-interface {p2, v0}, Le2/n;->k(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
