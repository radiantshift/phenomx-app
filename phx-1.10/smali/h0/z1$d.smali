.class public Lh0/z1$d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh0/z1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh0/z1$d$a;
    }
.end annotation


# static fields
.field public static final k:Lh0/z1$d;

.field private static final l:Ljava/lang/String;

.field private static final m:Ljava/lang/String;

.field private static final n:Ljava/lang/String;

.field private static final o:Ljava/lang/String;

.field private static final p:Ljava/lang/String;

.field public static final q:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Lh0/z1$e;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final f:J

.field public final g:J

.field public final h:Z

.field public final i:Z

.field public final j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lh0/z1$d$a;

    invoke-direct {v0}, Lh0/z1$d$a;-><init>()V

    invoke-virtual {v0}, Lh0/z1$d$a;->f()Lh0/z1$d;

    move-result-object v0

    sput-object v0, Lh0/z1$d;->k:Lh0/z1$d;

    const/4 v0, 0x0

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/z1$d;->l:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/z1$d;->m:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/z1$d;->n:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/z1$d;->o:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/z1$d;->p:Ljava/lang/String;

    sget-object v0, Lh0/a2;->a:Lh0/a2;

    sput-object v0, Lh0/z1$d;->q:Lh0/h$a;

    return-void
.end method

.method private constructor <init>(Lh0/z1$d$a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lh0/z1$d$a;->a(Lh0/z1$d$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lh0/z1$d;->f:J

    invoke-static {p1}, Lh0/z1$d$a;->b(Lh0/z1$d$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lh0/z1$d;->g:J

    invoke-static {p1}, Lh0/z1$d$a;->c(Lh0/z1$d$a;)Z

    move-result v0

    iput-boolean v0, p0, Lh0/z1$d;->h:Z

    invoke-static {p1}, Lh0/z1$d$a;->d(Lh0/z1$d$a;)Z

    move-result v0

    iput-boolean v0, p0, Lh0/z1$d;->i:Z

    invoke-static {p1}, Lh0/z1$d$a;->e(Lh0/z1$d$a;)Z

    move-result p1

    iput-boolean p1, p0, Lh0/z1$d;->j:Z

    return-void
.end method

.method synthetic constructor <init>(Lh0/z1$d$a;Lh0/z1$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/z1$d;-><init>(Lh0/z1$d$a;)V

    return-void
.end method

.method public static synthetic a(Landroid/os/Bundle;)Lh0/z1$e;
    .locals 0

    invoke-static {p0}, Lh0/z1$d;->c(Landroid/os/Bundle;)Lh0/z1$e;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic c(Landroid/os/Bundle;)Lh0/z1$e;
    .locals 5

    new-instance v0, Lh0/z1$d$a;

    invoke-direct {v0}, Lh0/z1$d$a;-><init>()V

    sget-object v1, Lh0/z1$d;->l:Ljava/lang/String;

    sget-object v2, Lh0/z1$d;->k:Lh0/z1$d;

    iget-wide v3, v2, Lh0/z1$d;->f:J

    invoke-virtual {p0, v1, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lh0/z1$d$a;->k(J)Lh0/z1$d$a;

    move-result-object v0

    sget-object v1, Lh0/z1$d;->m:Ljava/lang/String;

    iget-wide v3, v2, Lh0/z1$d;->g:J

    invoke-virtual {p0, v1, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lh0/z1$d$a;->h(J)Lh0/z1$d$a;

    move-result-object v0

    sget-object v1, Lh0/z1$d;->n:Ljava/lang/String;

    iget-boolean v3, v2, Lh0/z1$d;->h:Z

    invoke-virtual {p0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lh0/z1$d$a;->j(Z)Lh0/z1$d$a;

    move-result-object v0

    sget-object v1, Lh0/z1$d;->o:Ljava/lang/String;

    iget-boolean v3, v2, Lh0/z1$d;->i:Z

    invoke-virtual {p0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lh0/z1$d$a;->i(Z)Lh0/z1$d$a;

    move-result-object v0

    sget-object v1, Lh0/z1$d;->p:Ljava/lang/String;

    iget-boolean v2, v2, Lh0/z1$d;->j:Z

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p0

    invoke-virtual {v0, p0}, Lh0/z1$d$a;->l(Z)Lh0/z1$d$a;

    move-result-object p0

    invoke-virtual {p0}, Lh0/z1$d$a;->g()Lh0/z1$e;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public b()Lh0/z1$d$a;
    .locals 2

    new-instance v0, Lh0/z1$d$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lh0/z1$d$a;-><init>(Lh0/z1$d;Lh0/z1$a;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lh0/z1$d;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lh0/z1$d;

    iget-wide v3, p0, Lh0/z1$d;->f:J

    iget-wide v5, p1, Lh0/z1$d;->f:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_2

    iget-wide v3, p0, Lh0/z1$d;->g:J

    iget-wide v5, p1, Lh0/z1$d;->g:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lh0/z1$d;->h:Z

    iget-boolean v3, p1, Lh0/z1$d;->h:Z

    if-ne v1, v3, :cond_2

    iget-boolean v1, p0, Lh0/z1$d;->i:Z

    iget-boolean v3, p1, Lh0/z1$d;->i:Z

    if-ne v1, v3, :cond_2

    iget-boolean v1, p0, Lh0/z1$d;->j:Z

    iget-boolean p1, p1, Lh0/z1$d;->j:Z

    if-ne v1, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 7

    iget-wide v0, p0, Lh0/z1$d;->f:J

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-wide v3, p0, Lh0/z1$d;->g:J

    ushr-long v5, v3, v2

    xor-long v2, v3, v5

    long-to-int v0, v2

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v0, p0, Lh0/z1$d;->h:Z

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v0, p0, Lh0/z1$d;->i:Z

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget-boolean v0, p0, Lh0/z1$d;->j:Z

    add-int/2addr v1, v0

    return v1
.end method
