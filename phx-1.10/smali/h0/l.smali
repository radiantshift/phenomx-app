.class final Lh0/l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Le2/t;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh0/l$a;
    }
.end annotation


# instance fields
.field private final f:Le2/f0;

.field private final g:Lh0/l$a;

.field private h:Lh0/p3;

.field private i:Le2/t;

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Lh0/l$a;Le2/d;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lh0/l;->g:Lh0/l$a;

    new-instance p1, Le2/f0;

    invoke-direct {p1, p2}, Le2/f0;-><init>(Le2/d;)V

    iput-object p1, p0, Lh0/l;->f:Le2/f0;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lh0/l;->j:Z

    return-void
.end method

.method private d(Z)Z
    .locals 1

    iget-object v0, p0, Lh0/l;->h:Lh0/p3;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lh0/p3;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lh0/l;->h:Lh0/p3;

    invoke-interface {v0}, Lh0/p3;->k()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    iget-object p1, p0, Lh0/l;->h:Lh0/p3;

    invoke-interface {p1}, Lh0/p3;->n()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private j(Z)V
    .locals 5

    invoke-direct {p0, p1}, Lh0/l;->d(Z)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    iput-boolean p1, p0, Lh0/l;->j:Z

    iget-boolean p1, p0, Lh0/l;->k:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {p1}, Le2/f0;->b()V

    :cond_0
    return-void

    :cond_1
    iget-object p1, p0, Lh0/l;->i:Le2/t;

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Le2/t;

    invoke-interface {p1}, Le2/t;->A()J

    move-result-wide v0

    iget-boolean v2, p0, Lh0/l;->j:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {v2}, Le2/f0;->A()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_2

    iget-object p1, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {p1}, Le2/f0;->c()V

    return-void

    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lh0/l;->j:Z

    iget-boolean v2, p0, Lh0/l;->k:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {v2}, Le2/f0;->b()V

    :cond_3
    iget-object v2, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {v2, v0, v1}, Le2/f0;->a(J)V

    invoke-interface {p1}, Le2/t;->g()Lh0/f3;

    move-result-object p1

    iget-object v0, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {v0}, Le2/f0;->g()Lh0/f3;

    move-result-object v0

    invoke-virtual {p1, v0}, Lh0/f3;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {v0, p1}, Le2/f0;->h(Lh0/f3;)V

    iget-object v0, p0, Lh0/l;->g:Lh0/l$a;

    invoke-interface {v0, p1}, Lh0/l$a;->h(Lh0/f3;)V

    :cond_4
    return-void
.end method


# virtual methods
.method public A()J
    .locals 2

    iget-boolean v0, p0, Lh0/l;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {v0}, Le2/f0;->A()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lh0/l;->i:Le2/t;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le2/t;

    invoke-interface {v0}, Le2/t;->A()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public a(Lh0/p3;)V
    .locals 1

    iget-object v0, p0, Lh0/l;->h:Lh0/p3;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lh0/l;->i:Le2/t;

    iput-object p1, p0, Lh0/l;->h:Lh0/p3;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lh0/l;->j:Z

    :cond_0
    return-void
.end method

.method public b(Lh0/p3;)V
    .locals 2

    invoke-interface {p1}, Lh0/p3;->w()Le2/t;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lh0/l;->i:Le2/t;

    if-eq v0, v1, :cond_1

    if-nez v1, :cond_0

    iput-object v0, p0, Lh0/l;->i:Le2/t;

    iput-object p1, p0, Lh0/l;->h:Lh0/p3;

    iget-object p1, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {p1}, Le2/f0;->g()Lh0/f3;

    move-result-object p1

    invoke-interface {v0, p1}, Le2/t;->h(Lh0/f3;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Multiple renderer media clocks enabled."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lh0/q;->h(Ljava/lang/RuntimeException;)Lh0/q;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public c(J)V
    .locals 1

    iget-object v0, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {v0, p1, p2}, Le2/f0;->a(J)V

    return-void
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lh0/l;->k:Z

    iget-object v0, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {v0}, Le2/f0;->b()V

    return-void
.end method

.method public f()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lh0/l;->k:Z

    iget-object v0, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {v0}, Le2/f0;->c()V

    return-void
.end method

.method public g()Lh0/f3;
    .locals 1

    iget-object v0, p0, Lh0/l;->i:Le2/t;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Le2/t;->g()Lh0/f3;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {v0}, Le2/f0;->g()Lh0/f3;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public h(Lh0/f3;)V
    .locals 1

    iget-object v0, p0, Lh0/l;->i:Le2/t;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Le2/t;->h(Lh0/f3;)V

    iget-object p1, p0, Lh0/l;->i:Le2/t;

    invoke-interface {p1}, Le2/t;->g()Lh0/f3;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lh0/l;->f:Le2/f0;

    invoke-virtual {v0, p1}, Le2/f0;->h(Lh0/f3;)V

    return-void
.end method

.method public i(Z)J
    .locals 2

    invoke-direct {p0, p1}, Lh0/l;->j(Z)V

    invoke-virtual {p0}, Lh0/l;->A()J

    move-result-wide v0

    return-wide v0
.end method
