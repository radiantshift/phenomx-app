.class public final Lh0/r1$b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh0/r1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Lz0/a;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[B>;"
        }
    .end annotation
.end field

.field private n:Ll0/m;

.field private o:J

.field private p:I

.field private q:I

.field private r:F

.field private s:I

.field private t:F

.field private u:[B

.field private v:I

.field private w:Lf2/c;

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lh0/r1$b;->f:I

    iput v0, p0, Lh0/r1$b;->g:I

    iput v0, p0, Lh0/r1$b;->l:I

    const-wide v1, 0x7fffffffffffffffL

    iput-wide v1, p0, Lh0/r1$b;->o:J

    iput v0, p0, Lh0/r1$b;->p:I

    iput v0, p0, Lh0/r1$b;->q:I

    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lh0/r1$b;->r:F

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lh0/r1$b;->t:F

    iput v0, p0, Lh0/r1$b;->v:I

    iput v0, p0, Lh0/r1$b;->x:I

    iput v0, p0, Lh0/r1$b;->y:I

    iput v0, p0, Lh0/r1$b;->z:I

    iput v0, p0, Lh0/r1$b;->C:I

    iput v0, p0, Lh0/r1$b;->D:I

    iput v0, p0, Lh0/r1$b;->E:I

    const/4 v0, 0x0

    iput v0, p0, Lh0/r1$b;->F:I

    return-void
.end method

.method private constructor <init>(Lh0/r1;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lh0/r1;->f:Ljava/lang/String;

    iput-object v0, p0, Lh0/r1$b;->a:Ljava/lang/String;

    iget-object v0, p1, Lh0/r1;->g:Ljava/lang/String;

    iput-object v0, p0, Lh0/r1$b;->b:Ljava/lang/String;

    iget-object v0, p1, Lh0/r1;->h:Ljava/lang/String;

    iput-object v0, p0, Lh0/r1$b;->c:Ljava/lang/String;

    iget v0, p1, Lh0/r1;->i:I

    iput v0, p0, Lh0/r1$b;->d:I

    iget v0, p1, Lh0/r1;->j:I

    iput v0, p0, Lh0/r1$b;->e:I

    iget v0, p1, Lh0/r1;->k:I

    iput v0, p0, Lh0/r1$b;->f:I

    iget v0, p1, Lh0/r1;->l:I

    iput v0, p0, Lh0/r1$b;->g:I

    iget-object v0, p1, Lh0/r1;->n:Ljava/lang/String;

    iput-object v0, p0, Lh0/r1$b;->h:Ljava/lang/String;

    iget-object v0, p1, Lh0/r1;->o:Lz0/a;

    iput-object v0, p0, Lh0/r1$b;->i:Lz0/a;

    iget-object v0, p1, Lh0/r1;->p:Ljava/lang/String;

    iput-object v0, p0, Lh0/r1$b;->j:Ljava/lang/String;

    iget-object v0, p1, Lh0/r1;->q:Ljava/lang/String;

    iput-object v0, p0, Lh0/r1$b;->k:Ljava/lang/String;

    iget v0, p1, Lh0/r1;->r:I

    iput v0, p0, Lh0/r1$b;->l:I

    iget-object v0, p1, Lh0/r1;->s:Ljava/util/List;

    iput-object v0, p0, Lh0/r1$b;->m:Ljava/util/List;

    iget-object v0, p1, Lh0/r1;->t:Ll0/m;

    iput-object v0, p0, Lh0/r1$b;->n:Ll0/m;

    iget-wide v0, p1, Lh0/r1;->u:J

    iput-wide v0, p0, Lh0/r1$b;->o:J

    iget v0, p1, Lh0/r1;->v:I

    iput v0, p0, Lh0/r1$b;->p:I

    iget v0, p1, Lh0/r1;->w:I

    iput v0, p0, Lh0/r1$b;->q:I

    iget v0, p1, Lh0/r1;->x:F

    iput v0, p0, Lh0/r1$b;->r:F

    iget v0, p1, Lh0/r1;->y:I

    iput v0, p0, Lh0/r1$b;->s:I

    iget v0, p1, Lh0/r1;->z:F

    iput v0, p0, Lh0/r1$b;->t:F

    iget-object v0, p1, Lh0/r1;->A:[B

    iput-object v0, p0, Lh0/r1$b;->u:[B

    iget v0, p1, Lh0/r1;->B:I

    iput v0, p0, Lh0/r1$b;->v:I

    iget-object v0, p1, Lh0/r1;->C:Lf2/c;

    iput-object v0, p0, Lh0/r1$b;->w:Lf2/c;

    iget v0, p1, Lh0/r1;->D:I

    iput v0, p0, Lh0/r1$b;->x:I

    iget v0, p1, Lh0/r1;->E:I

    iput v0, p0, Lh0/r1$b;->y:I

    iget v0, p1, Lh0/r1;->F:I

    iput v0, p0, Lh0/r1$b;->z:I

    iget v0, p1, Lh0/r1;->G:I

    iput v0, p0, Lh0/r1$b;->A:I

    iget v0, p1, Lh0/r1;->H:I

    iput v0, p0, Lh0/r1$b;->B:I

    iget v0, p1, Lh0/r1;->I:I

    iput v0, p0, Lh0/r1$b;->C:I

    iget v0, p1, Lh0/r1;->J:I

    iput v0, p0, Lh0/r1$b;->D:I

    iget v0, p1, Lh0/r1;->K:I

    iput v0, p0, Lh0/r1$b;->E:I

    iget p1, p1, Lh0/r1;->L:I

    iput p1, p0, Lh0/r1$b;->F:I

    return-void
.end method

.method synthetic constructor <init>(Lh0/r1;Lh0/r1$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/r1$b;-><init>(Lh0/r1;)V

    return-void
.end method

.method static synthetic A(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->d:I

    return p0
.end method

.method static synthetic B(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->e:I

    return p0
.end method

.method static synthetic C(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->f:I

    return p0
.end method

.method static synthetic D(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->g:I

    return p0
.end method

.method static synthetic E(Lh0/r1$b;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lh0/r1$b;->h:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic F(Lh0/r1$b;)Lz0/a;
    .locals 0

    iget-object p0, p0, Lh0/r1$b;->i:Lz0/a;

    return-object p0
.end method

.method static synthetic a(Lh0/r1$b;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lh0/r1$b;->a:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lh0/r1$b;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lh0/r1$b;->j:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lh0/r1$b;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lh0/r1$b;->k:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->l:I

    return p0
.end method

.method static synthetic e(Lh0/r1$b;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lh0/r1$b;->m:Ljava/util/List;

    return-object p0
.end method

.method static synthetic f(Lh0/r1$b;)Ll0/m;
    .locals 0

    iget-object p0, p0, Lh0/r1$b;->n:Ll0/m;

    return-object p0
.end method

.method static synthetic g(Lh0/r1$b;)J
    .locals 2

    iget-wide v0, p0, Lh0/r1$b;->o:J

    return-wide v0
.end method

.method static synthetic h(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->p:I

    return p0
.end method

.method static synthetic i(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->q:I

    return p0
.end method

.method static synthetic j(Lh0/r1$b;)F
    .locals 0

    iget p0, p0, Lh0/r1$b;->r:F

    return p0
.end method

.method static synthetic k(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->s:I

    return p0
.end method

.method static synthetic l(Lh0/r1$b;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lh0/r1$b;->b:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic m(Lh0/r1$b;)F
    .locals 0

    iget p0, p0, Lh0/r1$b;->t:F

    return p0
.end method

.method static synthetic n(Lh0/r1$b;)[B
    .locals 0

    iget-object p0, p0, Lh0/r1$b;->u:[B

    return-object p0
.end method

.method static synthetic o(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->v:I

    return p0
.end method

.method static synthetic p(Lh0/r1$b;)Lf2/c;
    .locals 0

    iget-object p0, p0, Lh0/r1$b;->w:Lf2/c;

    return-object p0
.end method

.method static synthetic q(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->x:I

    return p0
.end method

.method static synthetic r(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->y:I

    return p0
.end method

.method static synthetic s(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->z:I

    return p0
.end method

.method static synthetic t(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->A:I

    return p0
.end method

.method static synthetic u(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->B:I

    return p0
.end method

.method static synthetic v(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->C:I

    return p0
.end method

.method static synthetic w(Lh0/r1$b;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lh0/r1$b;->c:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic x(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->D:I

    return p0
.end method

.method static synthetic y(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->E:I

    return p0
.end method

.method static synthetic z(Lh0/r1$b;)I
    .locals 0

    iget p0, p0, Lh0/r1$b;->F:I

    return p0
.end method


# virtual methods
.method public G()Lh0/r1;
    .locals 2

    new-instance v0, Lh0/r1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lh0/r1;-><init>(Lh0/r1$b;Lh0/r1$a;)V

    return-object v0
.end method

.method public H(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->C:I

    return-object p0
.end method

.method public I(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->f:I

    return-object p0
.end method

.method public J(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->x:I

    return-object p0
.end method

.method public K(Ljava/lang/String;)Lh0/r1$b;
    .locals 0

    iput-object p1, p0, Lh0/r1$b;->h:Ljava/lang/String;

    return-object p0
.end method

.method public L(Lf2/c;)Lh0/r1$b;
    .locals 0

    iput-object p1, p0, Lh0/r1$b;->w:Lf2/c;

    return-object p0
.end method

.method public M(Ljava/lang/String;)Lh0/r1$b;
    .locals 0

    iput-object p1, p0, Lh0/r1$b;->j:Ljava/lang/String;

    return-object p0
.end method

.method public N(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->F:I

    return-object p0
.end method

.method public O(Ll0/m;)Lh0/r1$b;
    .locals 0

    iput-object p1, p0, Lh0/r1$b;->n:Ll0/m;

    return-object p0
.end method

.method public P(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->A:I

    return-object p0
.end method

.method public Q(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->B:I

    return-object p0
.end method

.method public R(F)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->r:F

    return-object p0
.end method

.method public S(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->q:I

    return-object p0
.end method

.method public T(I)Lh0/r1$b;
    .locals 0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lh0/r1$b;->a:Ljava/lang/String;

    return-object p0
.end method

.method public U(Ljava/lang/String;)Lh0/r1$b;
    .locals 0

    iput-object p1, p0, Lh0/r1$b;->a:Ljava/lang/String;

    return-object p0
.end method

.method public V(Ljava/util/List;)Lh0/r1$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;)",
            "Lh0/r1$b;"
        }
    .end annotation

    iput-object p1, p0, Lh0/r1$b;->m:Ljava/util/List;

    return-object p0
.end method

.method public W(Ljava/lang/String;)Lh0/r1$b;
    .locals 0

    iput-object p1, p0, Lh0/r1$b;->b:Ljava/lang/String;

    return-object p0
.end method

.method public X(Ljava/lang/String;)Lh0/r1$b;
    .locals 0

    iput-object p1, p0, Lh0/r1$b;->c:Ljava/lang/String;

    return-object p0
.end method

.method public Y(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->l:I

    return-object p0
.end method

.method public Z(Lz0/a;)Lh0/r1$b;
    .locals 0

    iput-object p1, p0, Lh0/r1$b;->i:Lz0/a;

    return-object p0
.end method

.method public a0(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->z:I

    return-object p0
.end method

.method public b0(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->g:I

    return-object p0
.end method

.method public c0(F)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->t:F

    return-object p0
.end method

.method public d0([B)Lh0/r1$b;
    .locals 0

    iput-object p1, p0, Lh0/r1$b;->u:[B

    return-object p0
.end method

.method public e0(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->e:I

    return-object p0
.end method

.method public f0(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->s:I

    return-object p0
.end method

.method public g0(Ljava/lang/String;)Lh0/r1$b;
    .locals 0

    iput-object p1, p0, Lh0/r1$b;->k:Ljava/lang/String;

    return-object p0
.end method

.method public h0(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->y:I

    return-object p0
.end method

.method public i0(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->d:I

    return-object p0
.end method

.method public j0(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->v:I

    return-object p0
.end method

.method public k0(J)Lh0/r1$b;
    .locals 0

    iput-wide p1, p0, Lh0/r1$b;->o:J

    return-object p0
.end method

.method public l0(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->D:I

    return-object p0
.end method

.method public m0(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->E:I

    return-object p0
.end method

.method public n0(I)Lh0/r1$b;
    .locals 0

    iput p1, p0, Lh0/r1$b;->p:I

    return-object p0
.end method
