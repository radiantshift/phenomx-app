.class public abstract Lh0/f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/p3;
.implements Lh0/r3;


# instance fields
.field private final f:I

.field private final g:Lh0/s1;

.field private h:Lh0/s3;

.field private i:I

.field private j:Li0/u1;

.field private k:I

.field private l:Lj1/q0;

.field private m:[Lh0/r1;

.field private n:J

.field private o:J

.field private p:J

.field private q:Z

.field private r:Z


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lh0/f;->f:I

    new-instance p1, Lh0/s1;

    invoke-direct {p1}, Lh0/s1;-><init>()V

    iput-object p1, p0, Lh0/f;->g:Lh0/s1;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lh0/f;->p:J

    return-void
.end method

.method private S(JZ)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lh0/f;->q:Z

    iput-wide p1, p0, Lh0/f;->o:J

    iput-wide p1, p0, Lh0/f;->p:J

    invoke-virtual {p0, p1, p2, p3}, Lh0/f;->M(JZ)V

    return-void
.end method


# virtual methods
.method public synthetic B(FF)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/o3;->a(Lh0/p3;FF)V

    return-void
.end method

.method protected final C(Ljava/lang/Throwable;Lh0/r1;I)Lh0/q;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lh0/f;->D(Ljava/lang/Throwable;Lh0/r1;ZI)Lh0/q;

    move-result-object p1

    return-object p1
.end method

.method protected final D(Ljava/lang/Throwable;Lh0/r1;ZI)Lh0/q;
    .locals 9

    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lh0/f;->r:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lh0/f;->r:Z

    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p0, p2}, Lh0/r3;->a(Lh0/r1;)I

    move-result v1

    invoke-static {v1}, Lh0/q3;->f(I)I

    move-result v1
    :try_end_0
    .catch Lh0/q; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v0, p0, Lh0/f;->r:Z

    move v6, v1

    goto :goto_0

    :catchall_0
    move-exception p1

    iput-boolean v0, p0, Lh0/f;->r:Z

    throw p1

    :catch_0
    iput-boolean v0, p0, Lh0/f;->r:Z

    :cond_0
    const/4 v1, 0x4

    const/4 v6, 0x4

    :goto_0
    invoke-interface {p0}, Lh0/p3;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lh0/f;->G()I

    move-result v4

    move-object v2, p1

    move-object v5, p2

    move v7, p3

    move v8, p4

    invoke-static/range {v2 .. v8}, Lh0/q;->f(Ljava/lang/Throwable;Ljava/lang/String;ILh0/r1;IZI)Lh0/q;

    move-result-object p1

    return-object p1
.end method

.method protected final E()Lh0/s3;
    .locals 1

    iget-object v0, p0, Lh0/f;->h:Lh0/s3;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/s3;

    return-object v0
.end method

.method protected final F()Lh0/s1;
    .locals 1

    iget-object v0, p0, Lh0/f;->g:Lh0/s1;

    invoke-virtual {v0}, Lh0/s1;->a()V

    iget-object v0, p0, Lh0/f;->g:Lh0/s1;

    return-object v0
.end method

.method protected final G()I
    .locals 1

    iget v0, p0, Lh0/f;->i:I

    return v0
.end method

.method protected final H()Li0/u1;
    .locals 1

    iget-object v0, p0, Lh0/f;->j:Li0/u1;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Li0/u1;

    return-object v0
.end method

.method protected final I()[Lh0/r1;
    .locals 1

    iget-object v0, p0, Lh0/f;->m:[Lh0/r1;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lh0/r1;

    return-object v0
.end method

.method protected final J()Z
    .locals 1

    invoke-virtual {p0}, Lh0/f;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lh0/f;->q:Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lh0/f;->l:Lj1/q0;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/q0;

    invoke-interface {v0}, Lj1/q0;->k()Z

    move-result v0

    :goto_0
    return v0
.end method

.method protected abstract K()V
.end method

.method protected L(ZZ)V
    .locals 0

    return-void
.end method

.method protected abstract M(JZ)V
.end method

.method protected N()V
    .locals 0

    return-void
.end method

.method protected O()V
    .locals 0

    return-void
.end method

.method protected P()V
    .locals 0

    return-void
.end method

.method protected abstract Q([Lh0/r1;JJ)V
.end method

.method protected final R(Lh0/s1;Lk0/g;I)I
    .locals 5

    iget-object v0, p0, Lh0/f;->l:Lj1/q0;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/q0;

    invoke-interface {v0, p1, p2, p3}, Lj1/q0;->e(Lh0/s1;Lk0/g;I)I

    move-result p3

    const/4 v0, -0x4

    if-ne p3, v0, :cond_2

    invoke-virtual {p2}, Lk0/a;->k()Z

    move-result p1

    if-eqz p1, :cond_1

    const-wide/high16 p1, -0x8000000000000000L

    iput-wide p1, p0, Lh0/f;->p:J

    iget-boolean p1, p0, Lh0/f;->q:Z

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, -0x3

    :goto_0
    return v0

    :cond_1
    iget-wide v0, p2, Lk0/g;->j:J

    iget-wide v2, p0, Lh0/f;->n:J

    add-long/2addr v0, v2

    iput-wide v0, p2, Lk0/g;->j:J

    iget-wide p1, p0, Lh0/f;->p:J

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    iput-wide p1, p0, Lh0/f;->p:J

    goto :goto_1

    :cond_2
    const/4 p2, -0x5

    if-ne p3, p2, :cond_3

    iget-object p2, p1, Lh0/s1;->b:Lh0/r1;

    invoke-static {p2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lh0/r1;

    iget-wide v0, p2, Lh0/r1;->u:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v4, v0, v2

    if-eqz v4, :cond_3

    invoke-virtual {p2}, Lh0/r1;->b()Lh0/r1$b;

    move-result-object v0

    iget-wide v1, p2, Lh0/r1;->u:J

    iget-wide v3, p0, Lh0/f;->n:J

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lh0/r1$b;->k0(J)Lh0/r1$b;

    move-result-object p2

    invoke-virtual {p2}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object p2

    iput-object p2, p1, Lh0/s1;->b:Lh0/r1;

    :cond_3
    :goto_1
    return p3
.end method

.method protected T(J)I
    .locals 3

    iget-object v0, p0, Lh0/f;->l:Lj1/q0;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/q0;

    iget-wide v1, p0, Lh0/f;->n:J

    sub-long/2addr p1, v1

    invoke-interface {v0, p1, p2}, Lj1/q0;->i(J)I

    move-result p1

    return p1
.end method

.method public final b()V
    .locals 3

    iget v0, p0, Lh0/f;->k:I

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->f(Z)V

    iput v1, p0, Lh0/f;->k:I

    invoke-virtual {p0}, Lh0/f;->P()V

    return-void
.end method

.method public final c()V
    .locals 1

    iget v0, p0, Lh0/f;->k:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->f(Z)V

    iget-object v0, p0, Lh0/f;->g:Lh0/s1;

    invoke-virtual {v0}, Lh0/s1;->a()V

    invoke-virtual {p0}, Lh0/f;->N()V

    return-void
.end method

.method public final d()V
    .locals 2

    iget v0, p0, Lh0/f;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Le2/a;->f(Z)V

    const/4 v0, 0x2

    iput v0, p0, Lh0/f;->k:I

    invoke-virtual {p0}, Lh0/f;->O()V

    return-void
.end method

.method public final getState()I
    .locals 1

    iget v0, p0, Lh0/f;->k:I

    return v0
.end method

.method public final i()V
    .locals 3

    iget v0, p0, Lh0/f;->k:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Le2/a;->f(Z)V

    iget-object v0, p0, Lh0/f;->g:Lh0/s1;

    invoke-virtual {v0}, Lh0/s1;->a()V

    iput v2, p0, Lh0/f;->k:I

    const/4 v0, 0x0

    iput-object v0, p0, Lh0/f;->l:Lj1/q0;

    iput-object v0, p0, Lh0/f;->m:[Lh0/r1;

    iput-boolean v2, p0, Lh0/f;->q:Z

    invoke-virtual {p0}, Lh0/f;->K()V

    return-void
.end method

.method public final j()Lj1/q0;
    .locals 1

    iget-object v0, p0, Lh0/f;->l:Lj1/q0;

    return-object v0
.end method

.method public final l()I
    .locals 1

    iget v0, p0, Lh0/f;->f:I

    return v0
.end method

.method public final m(Lh0/s3;[Lh0/r1;Lj1/q0;JZZJJ)V
    .locals 9

    move-object v7, p0

    move v8, p6

    iget v0, v7, Lh0/f;->k:I

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->f(Z)V

    move-object v0, p1

    iput-object v0, v7, Lh0/f;->h:Lh0/s3;

    iput v1, v7, Lh0/f;->k:I

    move/from16 v0, p7

    invoke-virtual {p0, p6, v0}, Lh0/f;->L(ZZ)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-wide/from16 v3, p8

    move-wide/from16 v5, p10

    invoke-virtual/range {v0 .. v6}, Lh0/f;->y([Lh0/r1;Lj1/q0;JJ)V

    move-wide v0, p4

    invoke-direct {p0, p4, p5, p6}, Lh0/f;->S(JZ)V

    return-void
.end method

.method public final n()Z
    .locals 5

    iget-wide v0, p0, Lh0/f;->p:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public o()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public q(ILjava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public final r()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lh0/f;->q:Z

    return-void
.end method

.method public final s()V
    .locals 1

    iget-object v0, p0, Lh0/f;->l:Lj1/q0;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj1/q0;

    invoke-interface {v0}, Lj1/q0;->b()V

    return-void
.end method

.method public final t()J
    .locals 2

    iget-wide v0, p0, Lh0/f;->p:J

    return-wide v0
.end method

.method public final u(J)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lh0/f;->S(JZ)V

    return-void
.end method

.method public final v()Z
    .locals 1

    iget-boolean v0, p0, Lh0/f;->q:Z

    return v0
.end method

.method public w()Le2/t;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final x(ILi0/u1;)V
    .locals 0

    iput p1, p0, Lh0/f;->i:I

    iput-object p2, p0, Lh0/f;->j:Li0/u1;

    return-void
.end method

.method public final y([Lh0/r1;Lj1/q0;JJ)V
    .locals 6

    iget-boolean v0, p0, Lh0/f;->q:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Le2/a;->f(Z)V

    iput-object p2, p0, Lh0/f;->l:Lj1/q0;

    iget-wide v0, p0, Lh0/f;->p:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long p2, v0, v2

    if-nez p2, :cond_0

    iput-wide p3, p0, Lh0/f;->p:J

    :cond_0
    iput-object p1, p0, Lh0/f;->m:[Lh0/r1;

    iput-wide p5, p0, Lh0/f;->n:J

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p3

    move-wide v4, p5

    invoke-virtual/range {v0 .. v5}, Lh0/f;->Q([Lh0/r1;JJ)V

    return-void
.end method

.method public final z()Lh0/r3;
    .locals 0

    return-object p0
.end method
