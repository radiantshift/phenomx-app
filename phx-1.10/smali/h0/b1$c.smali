.class final Lh0/b1$c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lf2/x;
.implements Lj0/t;
.implements Ls1/n;
.implements Lz0/f;
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Lg2/f$a;
.implements Lh0/d$b;
.implements Lh0/b$b;
.implements Lh0/x3$b;
.implements Lh0/s$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh0/b1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic f:Lh0/b1;


# direct methods
.method private constructor <init>(Lh0/b1;)V
    .locals 0

    iput-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lh0/b1;Lh0/b1$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/b1$c;-><init>(Lh0/b1;)V

    return-void
.end method

.method public static synthetic I(Lz0/a;Lh0/g3$d;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/b1$c;->T(Lz0/a;Lh0/g3$d;)V

    return-void
.end method

.method public static synthetic J(Lf2/z;Lh0/g3$d;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/b1$c;->X(Lf2/z;Lh0/g3$d;)V

    return-void
.end method

.method public static synthetic K(IZLh0/g3$d;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lh0/b1$c;->W(IZLh0/g3$d;)V

    return-void
.end method

.method public static synthetic L(Ls1/e;Lh0/g3$d;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/b1$c;->R(Ls1/e;Lh0/g3$d;)V

    return-void
.end method

.method public static synthetic M(Ljava/util/List;Lh0/g3$d;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/b1$c;->Q(Ljava/util/List;Lh0/g3$d;)V

    return-void
.end method

.method public static synthetic N(Lh0/b1$c;Lh0/g3$d;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/b1$c;->S(Lh0/g3$d;)V

    return-void
.end method

.method public static synthetic O(Lh0/o;Lh0/g3$d;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/b1$c;->V(Lh0/o;Lh0/g3$d;)V

    return-void
.end method

.method public static synthetic P(ZLh0/g3$d;)V
    .locals 0

    invoke-static {p0, p1}, Lh0/b1$c;->U(ZLh0/g3$d;)V

    return-void
.end method

.method private static synthetic Q(Ljava/util/List;Lh0/g3$d;)V
    .locals 0

    invoke-interface {p1, p0}, Lh0/g3$d;->j(Ljava/util/List;)V

    return-void
.end method

.method private static synthetic R(Ls1/e;Lh0/g3$d;)V
    .locals 0

    invoke-interface {p1, p0}, Lh0/g3$d;->x(Ls1/e;)V

    return-void
.end method

.method private synthetic S(Lh0/g3$d;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->H0(Lh0/b1;)Lh0/e2;

    move-result-object v0

    invoke-interface {p1, v0}, Lh0/g3$d;->l0(Lh0/e2;)V

    return-void
.end method

.method private static synthetic T(Lz0/a;Lh0/g3$d;)V
    .locals 0

    invoke-interface {p1, p0}, Lh0/g3$d;->w(Lz0/a;)V

    return-void
.end method

.method private static synthetic U(ZLh0/g3$d;)V
    .locals 0

    invoke-interface {p1, p0}, Lh0/g3$d;->b(Z)V

    return-void
.end method

.method private static synthetic V(Lh0/o;Lh0/g3$d;)V
    .locals 0

    invoke-interface {p1, p0}, Lh0/g3$d;->T(Lh0/o;)V

    return-void
.end method

.method private static synthetic W(IZLh0/g3$d;)V
    .locals 0

    invoke-interface {p2, p0, p1}, Lh0/g3$d;->p0(IZ)V

    return-void
.end method

.method private static synthetic X(Lf2/z;Lh0/g3$d;)V
    .locals 0

    invoke-interface {p1, p0}, Lh0/g3$d;->l(Lf2/z;)V

    return-void
.end method


# virtual methods
.method public synthetic A(Lh0/r1;)V
    .locals 0

    invoke-static {p0, p1}, Lf2/m;->a(Lf2/x;Lh0/r1;)V

    return-void
.end method

.method public synthetic B(Lh0/r1;)V
    .locals 0

    invoke-static {p0, p1}, Lj0/i;->a(Lj0/t;Lh0/r1;)V

    return-void
.end method

.method public C(I)V
    .locals 3

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {p1}, Lh0/b1;->Q0(Lh0/b1;)Lh0/x3;

    move-result-object p1

    invoke-static {p1}, Lh0/b1;->R0(Lh0/x3;)Lh0/o;

    move-result-object p1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->S0(Lh0/b1;)Lh0/o;

    move-result-object v0

    invoke-virtual {p1, v0}, Lh0/o;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0, p1}, Lh0/b1;->T0(Lh0/b1;Lh0/o;)Lh0/o;

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->Z0(Lh0/b1;)Le2/q;

    move-result-object v0

    const/16 v1, 0x1d

    new-instance v2, Lh0/e1;

    invoke-direct {v2, p1}, Lh0/e1;-><init>(Lh0/o;)V

    invoke-virtual {v0, v1, v2}, Le2/q;->k(ILe2/q$a;)V

    :cond_0
    return-void
.end method

.method public synthetic D(Z)V
    .locals 0

    invoke-static {p0, p1}, Lh0/r;->b(Lh0/s$a;Z)V

    return-void
.end method

.method public synthetic E(Z)V
    .locals 0

    invoke-static {p0, p1}, Lh0/r;->a(Lh0/s$a;Z)V

    return-void
.end method

.method public F()V
    .locals 4

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3}, Lh0/b1;->P0(Lh0/b1;ZII)V

    return-void
.end method

.method public G(Z)V
    .locals 0

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {p1}, Lh0/b1;->U0(Lh0/b1;)V

    return-void
.end method

.method public H(F)V
    .locals 0

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {p1}, Lh0/b1;->N0(Lh0/b1;)V

    return-void
.end method

.method public a(I)V
    .locals 3

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-virtual {v0}, Lh0/b1;->s()Z

    move-result v0

    iget-object v1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0, p1}, Lh0/b1;->O0(ZI)I

    move-result v2

    invoke-static {v1, v0, p1, v2}, Lh0/b1;->P0(Lh0/b1;ZII)V

    return-void
.end method

.method public b(Z)V
    .locals 3

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->B0(Lh0/b1;)Z

    move-result v0

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0, p1}, Lh0/b1;->C0(Lh0/b1;Z)Z

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->Z0(Lh0/b1;)Le2/q;

    move-result-object v0

    const/16 v1, 0x17

    new-instance v2, Lh0/j1;

    invoke-direct {v2, p1}, Lh0/j1;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Le2/q;->k(ILe2/q$a;)V

    return-void
.end method

.method public c(Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1}, Li0/a;->c(Ljava/lang/Exception;)V

    return-void
.end method

.method public d(Lk0/e;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1}, Li0/a;->d(Lk0/e;)V

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lh0/b1;->A0(Lh0/b1;Lh0/r1;)Lh0/r1;

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {p1, v0}, Lh0/b1;->b1(Lh0/b1;Lk0/e;)Lk0/e;

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1}, Li0/a;->e(Ljava/lang/String;)V

    return-void
.end method

.method public f(Ljava/lang/Object;J)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Li0/a;->f(Ljava/lang/Object;J)V

    iget-object p2, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {p2}, Lh0/b1;->a1(Lh0/b1;)Ljava/lang/Object;

    move-result-object p2

    if-ne p2, p1, :cond_0

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {p1}, Lh0/b1;->Z0(Lh0/b1;)Le2/q;

    move-result-object p1

    const/16 p2, 0x1a

    sget-object p3, Lh0/k1;->a:Lh0/k1;

    invoke-virtual {p1, p2, p3}, Le2/q;->k(ILe2/q$a;)V

    :cond_0
    return-void
.end method

.method public g(Ljava/lang/String;JJ)V
    .locals 7

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v1

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-interface/range {v1 .. v6}, Li0/a;->g(Ljava/lang/String;JJ)V

    return-void
.end method

.method public h(Landroid/view/Surface;)V
    .locals 1

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lh0/b1;->K0(Lh0/b1;Ljava/lang/Object;)V

    return-void
.end method

.method public i(IZ)V
    .locals 2

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->Z0(Lh0/b1;)Le2/q;

    move-result-object v0

    new-instance v1, Lh0/c1;

    invoke-direct {v1, p1, p2}, Lh0/c1;-><init>(IZ)V

    const/16 p1, 0x1e

    invoke-virtual {v0, p1, v1}, Le2/q;->k(ILe2/q$a;)V

    return-void
.end method

.method public j(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ls1/b;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->Z0(Lh0/b1;)Le2/q;

    move-result-object v0

    new-instance v1, Lh0/g1;

    invoke-direct {v1, p1}, Lh0/g1;-><init>(Ljava/util/List;)V

    const/16 p1, 0x1b

    invoke-virtual {v0, p1, v1}, Le2/q;->k(ILe2/q$a;)V

    return-void
.end method

.method public k(Lh0/r1;Lk0/i;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0, p1}, Lh0/b1;->X0(Lh0/b1;Lh0/r1;)Lh0/r1;

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Li0/a;->k(Lh0/r1;Lk0/i;)V

    return-void
.end method

.method public l(Lf2/z;)V
    .locals 2

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0, p1}, Lh0/b1;->Y0(Lh0/b1;Lf2/z;)Lf2/z;

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->Z0(Lh0/b1;)Le2/q;

    move-result-object v0

    new-instance v1, Lh0/d1;

    invoke-direct {v1, p1}, Lh0/d1;-><init>(Lf2/z;)V

    const/16 p1, 0x19

    invoke-virtual {v0, p1, v1}, Le2/q;->k(ILe2/q$a;)V

    return-void
.end method

.method public m(J)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Li0/a;->m(J)V

    return-void
.end method

.method public n(Lk0/e;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0, p1}, Lh0/b1;->V0(Lh0/b1;Lk0/e;)Lk0/e;

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1}, Li0/a;->n(Lk0/e;)V

    return-void
.end method

.method public o(Lk0/e;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1}, Li0/a;->o(Lk0/e;)V

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lh0/b1;->X0(Lh0/b1;Lh0/r1;)Lh0/r1;

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {p1, v0}, Lh0/b1;->V0(Lh0/b1;Lk0/e;)Lk0/e;

    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0, p1}, Lh0/b1;->M0(Lh0/b1;Landroid/graphics/SurfaceTexture;)V

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {p1, p2, p3}, Lh0/b1;->L0(Lh0/b1;II)V

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lh0/b1;->K0(Lh0/b1;Ljava/lang/Object;)V

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    const/4 v0, 0x0

    invoke-static {p1, v0, v0}, Lh0/b1;->L0(Lh0/b1;II)V

    const/4 p1, 0x1

    return p1
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {p1, p2, p3}, Lh0/b1;->L0(Lh0/b1;II)V

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method

.method public p(Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1}, Li0/a;->p(Ljava/lang/Exception;)V

    return-void
.end method

.method public q(Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1}, Li0/a;->q(Ljava/lang/Exception;)V

    return-void
.end method

.method public r(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1}, Li0/a;->r(Ljava/lang/String;)V

    return-void
.end method

.method public s(Ljava/lang/String;JJ)V
    .locals 7

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v1

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-interface/range {v1 .. v6}, Li0/a;->s(Ljava/lang/String;JJ)V

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {p1, p3, p4}, Lh0/b1;->L0(Lh0/b1;II)V

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->J0(Lh0/b1;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object p1

    invoke-static {v0, p1}, Lh0/b1;->K0(Lh0/b1;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {p1}, Lh0/b1;->J0(Lh0/b1;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lh0/b1;->K0(Lh0/b1;Ljava/lang/Object;)V

    :cond_0
    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    const/4 v0, 0x0

    invoke-static {p1, v0, v0}, Lh0/b1;->L0(Lh0/b1;II)V

    return-void
.end method

.method public t(Lh0/r1;Lk0/i;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0, p1}, Lh0/b1;->A0(Lh0/b1;Lh0/r1;)Lh0/r1;

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Li0/a;->t(Lh0/r1;Lk0/i;)V

    return-void
.end method

.method public u(IJJ)V
    .locals 7

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v1

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-interface/range {v1 .. v6}, Li0/a;->u(IJJ)V

    return-void
.end method

.method public v(IJ)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Li0/a;->v(IJ)V

    return-void
.end method

.method public w(Lz0/a;)V
    .locals 3

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->E0(Lh0/b1;)Lh0/e2;

    move-result-object v1

    invoke-virtual {v1}, Lh0/e2;->b()Lh0/e2$b;

    move-result-object v1

    invoke-virtual {v1, p1}, Lh0/e2$b;->L(Lz0/a;)Lh0/e2$b;

    move-result-object v1

    invoke-virtual {v1}, Lh0/e2$b;->H()Lh0/e2;

    move-result-object v1

    invoke-static {v0, v1}, Lh0/b1;->F0(Lh0/b1;Lh0/e2;)Lh0/e2;

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->G0(Lh0/b1;)Lh0/e2;

    move-result-object v0

    iget-object v1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v1}, Lh0/b1;->H0(Lh0/b1;)Lh0/e2;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v1, v0}, Lh0/b1;->I0(Lh0/b1;Lh0/e2;)Lh0/e2;

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->Z0(Lh0/b1;)Le2/q;

    move-result-object v0

    const/16 v1, 0xe

    new-instance v2, Lh0/f1;

    invoke-direct {v2, p0}, Lh0/f1;-><init>(Lh0/b1$c;)V

    invoke-virtual {v0, v1, v2}, Le2/q;->i(ILe2/q$a;)V

    :cond_0
    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->Z0(Lh0/b1;)Le2/q;

    move-result-object v0

    const/16 v1, 0x1c

    new-instance v2, Lh0/i1;

    invoke-direct {v2, p1}, Lh0/i1;-><init>(Lz0/a;)V

    invoke-virtual {v0, v1, v2}, Le2/q;->i(ILe2/q$a;)V

    iget-object p1, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {p1}, Lh0/b1;->Z0(Lh0/b1;)Le2/q;

    move-result-object p1

    invoke-virtual {p1}, Le2/q;->f()V

    return-void
.end method

.method public x(Ls1/e;)V
    .locals 2

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0, p1}, Lh0/b1;->D0(Lh0/b1;Ls1/e;)Ls1/e;

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->Z0(Lh0/b1;)Le2/q;

    move-result-object v0

    new-instance v1, Lh0/h1;

    invoke-direct {v1, p1}, Lh0/h1;-><init>(Ls1/e;)V

    const/16 p1, 0x1b

    invoke-virtual {v0, p1, v1}, Le2/q;->k(ILe2/q$a;)V

    return-void
.end method

.method public y(JI)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Li0/a;->y(JI)V

    return-void
.end method

.method public z(Lk0/e;)V
    .locals 1

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0, p1}, Lh0/b1;->b1(Lh0/b1;Lk0/e;)Lk0/e;

    iget-object v0, p0, Lh0/b1$c;->f:Lh0/b1;

    invoke-static {v0}, Lh0/b1;->W0(Lh0/b1;)Li0/a;

    move-result-object v0

    invoke-interface {v0, p1}, Li0/a;->z(Lk0/e;)V

    return-void
.end method
