.class public final Lh0/z1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh0/z1$j;,
        Lh0/z1$e;,
        Lh0/z1$d;,
        Lh0/z1$k;,
        Lh0/z1$l;,
        Lh0/z1$g;,
        Lh0/z1$i;,
        Lh0/z1$h;,
        Lh0/z1$b;,
        Lh0/z1$f;,
        Lh0/z1$c;
    }
.end annotation


# static fields
.field public static final n:Lh0/z1;

.field private static final o:Ljava/lang/String;

.field private static final p:Ljava/lang/String;

.field private static final q:Ljava/lang/String;

.field private static final r:Ljava/lang/String;

.field private static final s:Ljava/lang/String;

.field public static final t:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Lh0/z1;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final f:Ljava/lang/String;

.field public final g:Lh0/z1$h;

.field public final h:Lh0/z1$i;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final i:Lh0/z1$g;

.field public final j:Lh0/e2;

.field public final k:Lh0/z1$d;

.field public final l:Lh0/z1$e;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final m:Lh0/z1$j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lh0/z1$c;

    invoke-direct {v0}, Lh0/z1$c;-><init>()V

    invoke-virtual {v0}, Lh0/z1$c;->a()Lh0/z1;

    move-result-object v0

    sput-object v0, Lh0/z1;->n:Lh0/z1;

    const/4 v0, 0x0

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/z1;->o:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/z1;->p:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/z1;->q:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/z1;->r:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/z1;->s:Ljava/lang/String;

    sget-object v0, Lh0/y1;->a:Lh0/y1;

    sput-object v0, Lh0/z1;->t:Lh0/h$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lh0/z1$e;Lh0/z1$i;Lh0/z1$g;Lh0/e2;Lh0/z1$j;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lh0/z1;->f:Ljava/lang/String;

    iput-object p3, p0, Lh0/z1;->g:Lh0/z1$h;

    iput-object p3, p0, Lh0/z1;->h:Lh0/z1$i;

    iput-object p4, p0, Lh0/z1;->i:Lh0/z1$g;

    iput-object p5, p0, Lh0/z1;->j:Lh0/e2;

    iput-object p2, p0, Lh0/z1;->k:Lh0/z1$d;

    iput-object p2, p0, Lh0/z1;->l:Lh0/z1$e;

    iput-object p6, p0, Lh0/z1;->m:Lh0/z1$j;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lh0/z1$e;Lh0/z1$i;Lh0/z1$g;Lh0/e2;Lh0/z1$j;Lh0/z1$a;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lh0/z1;-><init>(Ljava/lang/String;Lh0/z1$e;Lh0/z1$i;Lh0/z1$g;Lh0/e2;Lh0/z1$j;)V

    return-void
.end method

.method public static synthetic a(Landroid/os/Bundle;)Lh0/z1;
    .locals 0

    invoke-static {p0}, Lh0/z1;->c(Landroid/os/Bundle;)Lh0/z1;

    move-result-object p0

    return-object p0
.end method

.method private static c(Landroid/os/Bundle;)Lh0/z1;
    .locals 8

    sget-object v0, Lh0/z1;->o:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    sget-object v0, Lh0/z1;->p:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lh0/z1$g;->k:Lh0/z1$g;

    goto :goto_0

    :cond_0
    sget-object v1, Lh0/z1$g;->q:Lh0/h$a;

    invoke-interface {v1, v0}, Lh0/h$a;->a(Landroid/os/Bundle;)Lh0/h;

    move-result-object v0

    check-cast v0, Lh0/z1$g;

    :goto_0
    move-object v5, v0

    sget-object v0, Lh0/z1;->q:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lh0/e2;->N:Lh0/e2;

    goto :goto_1

    :cond_1
    sget-object v1, Lh0/e2;->v0:Lh0/h$a;

    invoke-interface {v1, v0}, Lh0/h$a;->a(Landroid/os/Bundle;)Lh0/h;

    move-result-object v0

    check-cast v0, Lh0/e2;

    :goto_1
    move-object v6, v0

    sget-object v0, Lh0/z1;->r:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lh0/z1$e;->r:Lh0/z1$e;

    goto :goto_2

    :cond_2
    sget-object v1, Lh0/z1$d;->q:Lh0/h$a;

    invoke-interface {v1, v0}, Lh0/h$a;->a(Landroid/os/Bundle;)Lh0/h;

    move-result-object v0

    check-cast v0, Lh0/z1$e;

    :goto_2
    move-object v3, v0

    sget-object v0, Lh0/z1;->s:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p0

    if-nez p0, :cond_3

    sget-object p0, Lh0/z1$j;->i:Lh0/z1$j;

    goto :goto_3

    :cond_3
    sget-object v0, Lh0/z1$j;->m:Lh0/h$a;

    invoke-interface {v0, p0}, Lh0/h$a;->a(Landroid/os/Bundle;)Lh0/h;

    move-result-object p0

    check-cast p0, Lh0/z1$j;

    :goto_3
    move-object v7, p0

    new-instance p0, Lh0/z1;

    const/4 v4, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lh0/z1;-><init>(Ljava/lang/String;Lh0/z1$e;Lh0/z1$i;Lh0/z1$g;Lh0/e2;Lh0/z1$j;)V

    return-object p0
.end method

.method public static d(Landroid/net/Uri;)Lh0/z1;
    .locals 1

    new-instance v0, Lh0/z1$c;

    invoke-direct {v0}, Lh0/z1$c;-><init>()V

    invoke-virtual {v0, p0}, Lh0/z1$c;->f(Landroid/net/Uri;)Lh0/z1$c;

    move-result-object p0

    invoke-virtual {p0}, Lh0/z1$c;->a()Lh0/z1;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public b()Lh0/z1$c;
    .locals 2

    new-instance v0, Lh0/z1$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lh0/z1$c;-><init>(Lh0/z1;Lh0/z1$a;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lh0/z1;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lh0/z1;

    iget-object v1, p0, Lh0/z1;->f:Ljava/lang/String;

    iget-object v3, p1, Lh0/z1;->f:Ljava/lang/String;

    invoke-static {v1, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lh0/z1;->k:Lh0/z1$d;

    iget-object v3, p1, Lh0/z1;->k:Lh0/z1$d;

    invoke-virtual {v1, v3}, Lh0/z1$d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lh0/z1;->g:Lh0/z1$h;

    iget-object v3, p1, Lh0/z1;->g:Lh0/z1$h;

    invoke-static {v1, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lh0/z1;->i:Lh0/z1$g;

    iget-object v3, p1, Lh0/z1;->i:Lh0/z1$g;

    invoke-static {v1, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lh0/z1;->j:Lh0/e2;

    iget-object v3, p1, Lh0/z1;->j:Lh0/e2;

    invoke-static {v1, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lh0/z1;->m:Lh0/z1$j;

    iget-object p1, p1, Lh0/z1;->m:Lh0/z1$j;

    invoke-static {v1, p1}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lh0/z1;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lh0/z1;->g:Lh0/z1$h;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lh0/z1$h;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lh0/z1;->i:Lh0/z1$g;

    invoke-virtual {v1}, Lh0/z1$g;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lh0/z1;->k:Lh0/z1$d;

    invoke-virtual {v1}, Lh0/z1$d;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lh0/z1;->j:Lh0/e2;

    invoke-virtual {v1}, Lh0/e2;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lh0/z1;->m:Lh0/z1$j;

    invoke-virtual {v1}, Lh0/z1$j;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
