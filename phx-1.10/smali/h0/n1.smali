.class final Lh0/n1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lj1/u$a;
.implements Lc2/c0$a;
.implements Lh0/x2$d;
.implements Lh0/l$a;
.implements Lh0/k3$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh0/n1$c;,
        Lh0/n1$b;,
        Lh0/n1$d;,
        Lh0/n1$g;,
        Lh0/n1$h;,
        Lh0/n1$f;,
        Lh0/n1$e;
    }
.end annotation


# instance fields
.field private final A:J

.field private B:Lh0/u3;

.field private C:Lh0/d3;

.field private D:Lh0/n1$e;

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:I

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:I

.field private P:Lh0/n1$h;

.field private Q:J

.field private R:I

.field private S:Z

.field private T:Lh0/q;

.field private U:J

.field private V:J

.field private final f:[Lh0/p3;

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lh0/p3;",
            ">;"
        }
    .end annotation
.end field

.field private final h:[Lh0/r3;

.field private final i:Lc2/c0;

.field private final j:Lc2/d0;

.field private final k:Lh0/x1;

.field private final l:Ld2/f;

.field private final m:Le2/n;

.field private final n:Landroid/os/HandlerThread;

.field private final o:Landroid/os/Looper;

.field private final p:Lh0/c4$d;

.field private final q:Lh0/c4$b;

.field private final r:J

.field private final s:Z

.field private final t:Lh0/l;

.field private final u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lh0/n1$d;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Le2/d;

.field private final w:Lh0/n1$f;

.field private final x:Lh0/i2;

.field private final y:Lh0/x2;

.field private final z:Lh0/w1;


# direct methods
.method public constructor <init>([Lh0/p3;Lc2/c0;Lc2/d0;Lh0/x1;Ld2/f;IZLi0/a;Lh0/u3;Lh0/w1;JZLandroid/os/Looper;Le2/d;Lh0/n1$f;Li0/u1;Landroid/os/Looper;)V
    .locals 13

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p5

    move-object/from16 v4, p8

    move-wide/from16 v5, p11

    move-object/from16 v7, p15

    move-object/from16 v8, p17

    move-object/from16 v9, p18

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v10, p16

    iput-object v10, v0, Lh0/n1;->w:Lh0/n1$f;

    iput-object v1, v0, Lh0/n1;->f:[Lh0/p3;

    iput-object v2, v0, Lh0/n1;->i:Lc2/c0;

    move-object/from16 v10, p3

    iput-object v10, v0, Lh0/n1;->j:Lc2/d0;

    move-object/from16 v11, p4

    iput-object v11, v0, Lh0/n1;->k:Lh0/x1;

    iput-object v3, v0, Lh0/n1;->l:Ld2/f;

    move/from16 v12, p6

    iput v12, v0, Lh0/n1;->J:I

    move/from16 v12, p7

    iput-boolean v12, v0, Lh0/n1;->K:Z

    move-object/from16 v12, p9

    iput-object v12, v0, Lh0/n1;->B:Lh0/u3;

    move-object/from16 v12, p10

    iput-object v12, v0, Lh0/n1;->z:Lh0/w1;

    iput-wide v5, v0, Lh0/n1;->A:J

    iput-wide v5, v0, Lh0/n1;->U:J

    move/from16 v5, p13

    iput-boolean v5, v0, Lh0/n1;->F:Z

    iput-object v7, v0, Lh0/n1;->v:Le2/d;

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v5, v0, Lh0/n1;->V:J

    invoke-interface/range {p4 .. p4}, Lh0/x1;->j()J

    move-result-wide v5

    iput-wide v5, v0, Lh0/n1;->r:J

    invoke-interface/range {p4 .. p4}, Lh0/x1;->c()Z

    move-result v5

    iput-boolean v5, v0, Lh0/n1;->s:Z

    invoke-static/range {p3 .. p3}, Lh0/d3;->j(Lc2/d0;)Lh0/d3;

    move-result-object v5

    iput-object v5, v0, Lh0/n1;->C:Lh0/d3;

    new-instance v6, Lh0/n1$e;

    invoke-direct {v6, v5}, Lh0/n1$e;-><init>(Lh0/d3;)V

    iput-object v6, v0, Lh0/n1;->D:Lh0/n1$e;

    array-length v5, v1

    new-array v5, v5, [Lh0/r3;

    iput-object v5, v0, Lh0/n1;->h:[Lh0/r3;

    const/4 v5, 0x0

    :goto_0
    array-length v6, v1

    if-ge v5, v6, :cond_0

    aget-object v6, v1, v5

    invoke-interface {v6, v5, v8}, Lh0/p3;->x(ILi0/u1;)V

    iget-object v6, v0, Lh0/n1;->h:[Lh0/r3;

    aget-object v10, v1, v5

    invoke-interface {v10}, Lh0/p3;->z()Lh0/r3;

    move-result-object v10

    aput-object v10, v6, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Lh0/l;

    invoke-direct {v1, p0, v7}, Lh0/l;-><init>(Lh0/l$a;Le2/d;)V

    iput-object v1, v0, Lh0/n1;->t:Lh0/l;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-static {}, Lp2/p0;->h()Ljava/util/Set;

    move-result-object v1

    iput-object v1, v0, Lh0/n1;->g:Ljava/util/Set;

    new-instance v1, Lh0/c4$d;

    invoke-direct {v1}, Lh0/c4$d;-><init>()V

    iput-object v1, v0, Lh0/n1;->p:Lh0/c4$d;

    new-instance v1, Lh0/c4$b;

    invoke-direct {v1}, Lh0/c4$b;-><init>()V

    iput-object v1, v0, Lh0/n1;->q:Lh0/c4$b;

    invoke-virtual {p2, p0, v3}, Lc2/c0;->b(Lc2/c0$a;Ld2/f;)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lh0/n1;->S:Z

    const/4 v1, 0x0

    move-object/from16 v2, p14

    invoke-interface {v7, v2, v1}, Le2/d;->b(Landroid/os/Looper;Landroid/os/Handler$Callback;)Le2/n;

    move-result-object v2

    new-instance v3, Lh0/i2;

    invoke-direct {v3, v4, v2}, Lh0/i2;-><init>(Li0/a;Le2/n;)V

    iput-object v3, v0, Lh0/n1;->x:Lh0/i2;

    new-instance v3, Lh0/x2;

    invoke-direct {v3, p0, v4, v2, v8}, Lh0/x2;-><init>(Lh0/x2$d;Li0/a;Le2/n;Li0/u1;)V

    iput-object v3, v0, Lh0/n1;->y:Lh0/x2;

    if-eqz v9, :cond_1

    iput-object v1, v0, Lh0/n1;->n:Landroid/os/HandlerThread;

    iput-object v9, v0, Lh0/n1;->o:Landroid/os/Looper;

    goto :goto_1

    :cond_1
    new-instance v1, Landroid/os/HandlerThread;

    const/16 v2, -0x10

    const-string v3, "ExoPlayer:Playback"

    invoke-direct {v1, v3, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v1, v0, Lh0/n1;->n:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, v0, Lh0/n1;->o:Landroid/os/Looper;

    :goto_1
    iget-object v1, v0, Lh0/n1;->o:Landroid/os/Looper;

    invoke-interface {v7, v1, p0}, Le2/d;->b(Landroid/os/Looper;Landroid/os/Handler$Callback;)Le2/n;

    move-result-object v1

    iput-object v1, v0, Lh0/n1;->m:Le2/n;

    return-void
.end method

.method private A(Lh0/c4;Ljava/lang/Object;J)J
    .locals 4

    iget-object v0, p0, Lh0/n1;->q:Lh0/c4$b;

    invoke-virtual {p1, p2, v0}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object p2

    iget p2, p2, Lh0/c4$b;->h:I

    iget-object v0, p0, Lh0/n1;->p:Lh0/c4$d;

    invoke-virtual {p1, p2, v0}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    iget-object p1, p0, Lh0/n1;->p:Lh0/c4$d;

    iget-wide v0, p1, Lh0/c4$d;->k:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p2, v0, v2

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lh0/c4$d;->h()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lh0/n1;->p:Lh0/c4$d;

    iget-boolean p2, p1, Lh0/c4$d;->n:Z

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lh0/c4$d;->c()J

    move-result-wide p1

    iget-object v0, p0, Lh0/n1;->p:Lh0/c4$d;

    iget-wide v0, v0, Lh0/c4$d;->k:J

    sub-long/2addr p1, v0

    invoke-static {p1, p2}, Le2/n0;->C0(J)J

    move-result-wide p1

    iget-object v0, p0, Lh0/n1;->q:Lh0/c4$b;

    invoke-virtual {v0}, Lh0/c4$b;->q()J

    move-result-wide v0

    add-long/2addr p3, v0

    sub-long/2addr p1, p3

    return-wide p1

    :cond_1
    :goto_0
    return-wide v2
.end method

.method static A0(Lh0/c4$d;Lh0/c4$b;IZLjava/lang/Object;Lh0/c4;Lh0/c4;)Ljava/lang/Object;
    .locals 9

    invoke-virtual {p5, p4}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result p4

    invoke-virtual {p5}, Lh0/c4;->m()I

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x0

    move v4, p4

    const/4 p4, -0x1

    :goto_0
    if-ge v2, v0, :cond_1

    if-ne p4, v1, :cond_1

    move-object v3, p5

    move-object v5, p1

    move-object v6, p0

    move v7, p2

    move v8, p3

    invoke-virtual/range {v3 .. v8}, Lh0/c4;->h(ILh0/c4$b;Lh0/c4$d;IZ)I

    move-result v4

    if-ne v4, v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p5, v4}, Lh0/c4;->q(I)Ljava/lang/Object;

    move-result-object p4

    invoke-virtual {p6, p4}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result p4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-ne p4, v1, :cond_2

    const/4 p0, 0x0

    goto :goto_2

    :cond_2
    invoke-virtual {p6, p4}, Lh0/c4;->q(I)Ljava/lang/Object;

    move-result-object p0

    :goto_2
    return-object p0
.end method

.method private B()J
    .locals 9

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->q()Lh0/f2;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    return-wide v0

    :cond_0
    invoke-virtual {v0}, Lh0/f2;->l()J

    move-result-wide v1

    iget-boolean v3, v0, Lh0/f2;->d:Z

    if-nez v3, :cond_1

    return-wide v1

    :cond_1
    const/4 v3, 0x0

    :goto_0
    iget-object v4, p0, Lh0/n1;->f:[Lh0/p3;

    array-length v5, v4

    if-ge v3, v5, :cond_5

    aget-object v4, v4, v3

    invoke-static {v4}, Lh0/n1;->S(Lh0/p3;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lh0/n1;->f:[Lh0/p3;

    aget-object v4, v4, v3

    invoke-interface {v4}, Lh0/p3;->j()Lj1/q0;

    move-result-object v4

    iget-object v5, v0, Lh0/f2;->c:[Lj1/q0;

    aget-object v5, v5, v3

    if-eq v4, v5, :cond_2

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lh0/n1;->f:[Lh0/p3;

    aget-object v4, v4, v3

    invoke-interface {v4}, Lh0/p3;->t()J

    move-result-wide v4

    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v8, v4, v6

    if-nez v8, :cond_3

    return-wide v6

    :cond_3
    invoke-static {v4, v5, v1, v2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    :cond_4
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    return-wide v1
.end method

.method private B0(JJ)V
    .locals 1

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    add-long/2addr p1, p3

    const/4 p3, 0x2

    invoke-interface {v0, p3, p1, p2}, Le2/n;->e(IJ)Z

    return-void
.end method

.method private C(Lh0/c4;)Landroid/util/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lh0/c4;",
            ")",
            "Landroid/util/Pair<",
            "Lj1/x$b;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lh0/c4;->u()Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    invoke-static {}, Lh0/d3;->k()Lj1/x$b;

    move-result-object p1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p1

    return-object p1

    :cond_0
    iget-boolean v0, p0, Lh0/n1;->K:Z

    invoke-virtual {p1, v0}, Lh0/c4;->e(Z)I

    move-result v6

    iget-object v4, p0, Lh0/n1;->p:Lh0/c4$d;

    iget-object v5, p0, Lh0/n1;->q:Lh0/c4$b;

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Lh0/c4;->n(Lh0/c4$d;Lh0/c4$b;IJ)Landroid/util/Pair;

    move-result-object v0

    iget-object v3, p0, Lh0/n1;->x:Lh0/i2;

    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v3, p1, v4, v1, v2}, Lh0/i2;->B(Lh0/c4;Ljava/lang/Object;J)Lj1/x$b;

    move-result-object v3

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Lj1/v;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v3, Lj1/v;->a:Ljava/lang/Object;

    iget-object v4, p0, Lh0/n1;->q:Lh0/c4$b;

    invoke-virtual {p1, v0, v4}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    iget p1, v3, Lj1/v;->c:I

    iget-object v0, p0, Lh0/n1;->q:Lh0/c4$b;

    iget v4, v3, Lj1/v;->b:I

    invoke-virtual {v0, v4}, Lh0/c4$b;->n(I)I

    move-result v0

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lh0/n1;->q:Lh0/c4$b;

    invoke-virtual {p1}, Lh0/c4$b;->j()J

    move-result-wide v1

    :cond_1
    move-wide v4, v1

    :cond_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {v3, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p1

    return-object p1
.end method

.method private D0(Z)V
    .locals 11

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    iget-object v0, v0, Lh0/f2;->f:Lh0/g2;

    iget-object v0, v0, Lh0/g2;->a:Lj1/x$b;

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    iget-wide v3, v1, Lh0/d3;->r:J

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, v0

    invoke-direct/range {v1 .. v6}, Lh0/n1;->G0(Lj1/x$b;JZZ)J

    move-result-wide v3

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    iget-wide v1, v1, Lh0/d3;->r:J

    cmp-long v5, v3, v1

    if-eqz v5, :cond_0

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    iget-wide v5, v1, Lh0/d3;->c:J

    iget-wide v7, v1, Lh0/d3;->d:J

    const/4 v10, 0x5

    move-object v1, p0

    move-object v2, v0

    move v9, p1

    invoke-direct/range {v1 .. v10}, Lh0/n1;->N(Lj1/x$b;JJJZI)Lh0/d3;

    move-result-object p1

    iput-object p1, p0, Lh0/n1;->C:Lh0/d3;

    :cond_0
    return-void
.end method

.method private E()J
    .locals 2

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-wide v0, v0, Lh0/d3;->p:J

    invoke-direct {p0, v0, v1}, Lh0/n1;->F(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private E0(Lh0/n1$h;)V
    .locals 19

    move-object/from16 v11, p0

    move-object/from16 v0, p1

    iget-object v1, v11, Lh0/n1;->D:Lh0/n1$e;

    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Lh0/n1$e;->b(I)V

    iget-object v1, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v1, Lh0/d3;->a:Lh0/c4;

    iget v4, v11, Lh0/n1;->J:I

    iget-boolean v5, v11, Lh0/n1;->K:Z

    iget-object v6, v11, Lh0/n1;->p:Lh0/c4$d;

    iget-object v7, v11, Lh0/n1;->q:Lh0/c4$b;

    const/4 v3, 0x1

    move-object/from16 v2, p1

    invoke-static/range {v1 .. v7}, Lh0/n1;->z0(Lh0/c4;Lh0/n1$h;ZIZLh0/c4$d;Lh0/c4$b;)Landroid/util/Pair;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v6, 0x0

    if-nez v1, :cond_0

    iget-object v7, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v7, v7, Lh0/d3;->a:Lh0/c4;

    invoke-direct {v11, v7}, Lh0/n1;->C(Lh0/c4;)Landroid/util/Pair;

    move-result-object v7

    iget-object v9, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v9, Lj1/x$b;

    iget-object v7, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    iget-object v7, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v7, v7, Lh0/d3;->a:Lh0/c4;

    invoke-virtual {v7}, Lh0/c4;->u()Z

    move-result v7

    xor-int/2addr v7, v8

    move v10, v7

    move-wide/from16 v17, v4

    :goto_0
    move-wide v4, v12

    move-wide/from16 v12, v17

    goto :goto_4

    :cond_0
    iget-object v7, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v9, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    iget-wide v9, v0, Lh0/n1$h;->c:J

    cmp-long v14, v9, v4

    if-nez v14, :cond_1

    move-wide v9, v4

    goto :goto_1

    :cond_1
    move-wide v9, v12

    :goto_1
    iget-object v14, v11, Lh0/n1;->x:Lh0/i2;

    iget-object v15, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v15, v15, Lh0/d3;->a:Lh0/c4;

    invoke-virtual {v14, v15, v7, v12, v13}, Lh0/i2;->B(Lh0/c4;Ljava/lang/Object;J)Lj1/x$b;

    move-result-object v7

    invoke-virtual {v7}, Lj1/v;->b()Z

    move-result v14

    if-eqz v14, :cond_3

    iget-object v4, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v4, v4, Lh0/d3;->a:Lh0/c4;

    iget-object v5, v7, Lj1/v;->a:Ljava/lang/Object;

    iget-object v12, v11, Lh0/n1;->q:Lh0/c4$b;

    invoke-virtual {v4, v5, v12}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    iget-object v4, v11, Lh0/n1;->q:Lh0/c4$b;

    iget v5, v7, Lj1/v;->b:I

    invoke-virtual {v4, v5}, Lh0/c4$b;->n(I)I

    move-result v4

    iget v5, v7, Lj1/v;->c:I

    if-ne v4, v5, :cond_2

    iget-object v4, v11, Lh0/n1;->q:Lh0/c4$b;

    invoke-virtual {v4}, Lh0/c4$b;->j()J

    move-result-wide v4

    move-wide v12, v4

    goto :goto_2

    :cond_2
    move-wide v12, v2

    :goto_2
    move-wide v4, v12

    move-wide v12, v9

    const/4 v10, 0x1

    move-object v9, v7

    goto :goto_4

    :cond_3
    iget-wide v14, v0, Lh0/n1$h;->c:J

    cmp-long v16, v14, v4

    if-nez v16, :cond_4

    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :goto_3
    move-wide/from16 v17, v9

    move v10, v4

    move-object v9, v7

    goto :goto_0

    :goto_4
    :try_start_0
    iget-object v7, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v7, v7, Lh0/d3;->a:Lh0/c4;

    invoke-virtual {v7}, Lh0/c4;->u()Z

    move-result v7

    if-eqz v7, :cond_5

    iput-object v0, v11, Lh0/n1;->P:Lh0/n1$h;

    goto :goto_5

    :cond_5
    const/4 v0, 0x4

    if-nez v1, :cond_7

    iget-object v1, v11, Lh0/n1;->C:Lh0/d3;

    iget v1, v1, Lh0/d3;->e:I

    if-eq v1, v8, :cond_6

    invoke-direct {v11, v0}, Lh0/n1;->c1(I)V

    :cond_6
    invoke-direct {v11, v6, v8, v6, v8}, Lh0/n1;->s0(ZZZZ)V

    :goto_5
    move-wide v7, v4

    goto/16 :goto_9

    :cond_7
    iget-object v1, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v1, Lh0/d3;->b:Lj1/x$b;

    invoke-virtual {v9, v1}, Lj1/v;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, v11, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v1}, Lh0/i2;->p()Lh0/f2;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-boolean v7, v1, Lh0/f2;->d:Z

    if-eqz v7, :cond_8

    cmp-long v7, v4, v2

    if-eqz v7, :cond_8

    iget-object v1, v1, Lh0/f2;->a:Lj1/u;

    iget-object v2, v11, Lh0/n1;->B:Lh0/u3;

    invoke-interface {v1, v4, v5, v2}, Lj1/u;->c(JLh0/u3;)J

    move-result-wide v1

    goto :goto_6

    :cond_8
    move-wide v1, v4

    :goto_6
    invoke-static {v1, v2}, Le2/n0;->Z0(J)J

    move-result-wide v14

    iget-object v3, v11, Lh0/n1;->C:Lh0/d3;

    iget-wide v6, v3, Lh0/d3;->r:J

    invoke-static {v6, v7}, Le2/n0;->Z0(J)J

    move-result-wide v6

    cmp-long v3, v14, v6

    if-nez v3, :cond_b

    iget-object v3, v11, Lh0/n1;->C:Lh0/d3;

    iget v6, v3, Lh0/d3;->e:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_9

    const/4 v7, 0x3

    if-ne v6, v7, :cond_b

    :cond_9
    iget-wide v7, v3, Lh0/d3;->r:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x2

    move-object/from16 v1, p0

    move-object v2, v9

    move-wide v3, v7

    move-wide v5, v12

    move v9, v10

    move v10, v0

    invoke-direct/range {v1 .. v10}, Lh0/n1;->N(Lj1/x$b;JJJZI)Lh0/d3;

    move-result-object v0

    iput-object v0, v11, Lh0/n1;->C:Lh0/d3;

    return-void

    :cond_a
    move-wide v1, v4

    :cond_b
    :try_start_1
    iget-object v3, v11, Lh0/n1;->C:Lh0/d3;

    iget v3, v3, Lh0/d3;->e:I

    if-ne v3, v0, :cond_c

    const/4 v0, 0x1

    goto :goto_7

    :cond_c
    const/4 v0, 0x0

    :goto_7
    invoke-direct {v11, v9, v1, v2, v0}, Lh0/n1;->F0(Lj1/x$b;JZ)J

    move-result-wide v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    cmp-long v0, v4, v14

    if-eqz v0, :cond_d

    goto :goto_8

    :cond_d
    const/4 v8, 0x0

    :goto_8
    or-int/2addr v10, v8

    :try_start_2
    iget-object v0, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v4, v0, Lh0/d3;->a:Lh0/c4;

    iget-object v5, v0, Lh0/d3;->b:Lj1/x$b;

    const/4 v8, 0x1

    move-object/from16 v1, p0

    move-object v2, v4

    move-object v3, v9

    move-wide v6, v12

    invoke-direct/range {v1 .. v8}, Lh0/n1;->q1(Lh0/c4;Lj1/x$b;Lh0/c4;Lj1/x$b;JZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-wide v7, v14

    :goto_9
    const/4 v0, 0x2

    move-object/from16 v1, p0

    move-object v2, v9

    move-wide v3, v7

    move-wide v5, v12

    move v9, v10

    move v10, v0

    invoke-direct/range {v1 .. v10}, Lh0/n1;->N(Lj1/x$b;JJJZI)Lh0/d3;

    move-result-object v0

    iput-object v0, v11, Lh0/n1;->C:Lh0/d3;

    return-void

    :catchall_0
    move-exception v0

    move-wide v7, v14

    goto :goto_a

    :catchall_1
    move-exception v0

    move-wide v7, v4

    :goto_a
    const/4 v14, 0x2

    move-object/from16 v1, p0

    move-object v2, v9

    move-wide v3, v7

    move-wide v5, v12

    move v9, v10

    move v10, v14

    invoke-direct/range {v1 .. v10}, Lh0/n1;->N(Lj1/x$b;JJJZI)Lh0/d3;

    move-result-object v1

    iput-object v1, v11, Lh0/n1;->C:Lh0/d3;

    throw v0
.end method

.method private F(J)J
    .locals 5

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->j()Lh0/f2;

    move-result-object v0

    const-wide/16 v1, 0x0

    if-nez v0, :cond_0

    return-wide v1

    :cond_0
    iget-wide v3, p0, Lh0/n1;->Q:J

    invoke-virtual {v0, v3, v4}, Lh0/f2;->y(J)J

    move-result-wide v3

    sub-long/2addr p1, v3

    invoke-static {v1, v2, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method private F0(Lj1/x$b;JZ)J
    .locals 7

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    iget-object v1, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v1}, Lh0/i2;->q()Lh0/f2;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v5, 0x0

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lh0/n1;->G0(Lj1/x$b;JZZ)J

    move-result-wide p1

    return-wide p1
.end method

.method private G(Lj1/u;)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0, p1}, Lh0/i2;->v(Lj1/u;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lh0/n1;->x:Lh0/i2;

    iget-wide v0, p0, Lh0/n1;->Q:J

    invoke-virtual {p1, v0, v1}, Lh0/i2;->y(J)V

    invoke-direct {p0}, Lh0/n1;->X()V

    return-void
.end method

.method private G0(Lj1/x$b;JZZ)J
    .locals 5

    invoke-direct {p0}, Lh0/n1;->l1()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lh0/n1;->H:Z

    const/4 v1, 0x2

    if-nez p5, :cond_0

    iget-object p5, p0, Lh0/n1;->C:Lh0/d3;

    iget p5, p5, Lh0/d3;->e:I

    const/4 v2, 0x3

    if-ne p5, v2, :cond_1

    :cond_0
    invoke-direct {p0, v1}, Lh0/n1;->c1(I)V

    :cond_1
    iget-object p5, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {p5}, Lh0/i2;->p()Lh0/f2;

    move-result-object p5

    move-object v2, p5

    :goto_0
    if-eqz v2, :cond_3

    iget-object v3, v2, Lh0/f2;->f:Lh0/g2;

    iget-object v3, v3, Lh0/g2;->a:Lj1/x$b;

    invoke-virtual {p1, v3}, Lj1/v;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Lh0/f2;->j()Lh0/f2;

    move-result-object v2

    goto :goto_0

    :cond_3
    :goto_1
    if-nez p4, :cond_4

    if-ne p5, v2, :cond_4

    if-eqz v2, :cond_7

    invoke-virtual {v2, p2, p3}, Lh0/f2;->z(J)J

    move-result-wide p4

    const-wide/16 v3, 0x0

    cmp-long p1, p4, v3

    if-gez p1, :cond_7

    :cond_4
    iget-object p1, p0, Lh0/n1;->f:[Lh0/p3;

    array-length p4, p1

    const/4 p5, 0x0

    :goto_2
    if-ge p5, p4, :cond_5

    aget-object v3, p1, p5

    invoke-direct {p0, v3}, Lh0/n1;->p(Lh0/p3;)V

    add-int/lit8 p5, p5, 0x1

    goto :goto_2

    :cond_5
    if-eqz v2, :cond_7

    :goto_3
    iget-object p1, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {p1}, Lh0/i2;->p()Lh0/f2;

    move-result-object p1

    if-eq p1, v2, :cond_6

    iget-object p1, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {p1}, Lh0/i2;->b()Lh0/f2;

    goto :goto_3

    :cond_6
    iget-object p1, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {p1, v2}, Lh0/i2;->z(Lh0/f2;)Z

    const-wide p4, 0xe8d4a51000L

    invoke-virtual {v2, p4, p5}, Lh0/f2;->x(J)V

    invoke-direct {p0}, Lh0/n1;->s()V

    :cond_7
    iget-object p1, p0, Lh0/n1;->x:Lh0/i2;

    if-eqz v2, :cond_a

    invoke-virtual {p1, v2}, Lh0/i2;->z(Lh0/f2;)Z

    iget-boolean p1, v2, Lh0/f2;->d:Z

    if-nez p1, :cond_8

    iget-object p1, v2, Lh0/f2;->f:Lh0/g2;

    invoke-virtual {p1, p2, p3}, Lh0/g2;->b(J)Lh0/g2;

    move-result-object p1

    iput-object p1, v2, Lh0/f2;->f:Lh0/g2;

    goto :goto_4

    :cond_8
    iget-boolean p1, v2, Lh0/f2;->e:Z

    if-eqz p1, :cond_9

    iget-object p1, v2, Lh0/f2;->a:Lj1/u;

    invoke-interface {p1, p2, p3}, Lj1/u;->s(J)J

    move-result-wide p1

    iget-object p3, v2, Lh0/f2;->a:Lj1/u;

    iget-wide p4, p0, Lh0/n1;->r:J

    sub-long p4, p1, p4

    iget-boolean v2, p0, Lh0/n1;->s:Z

    invoke-interface {p3, p4, p5, v2}, Lj1/u;->r(JZ)V

    move-wide p2, p1

    :cond_9
    :goto_4
    invoke-direct {p0, p2, p3}, Lh0/n1;->u0(J)V

    invoke-direct {p0}, Lh0/n1;->X()V

    goto :goto_5

    :cond_a
    invoke-virtual {p1}, Lh0/i2;->f()V

    invoke-direct {p0, p2, p3}, Lh0/n1;->u0(J)V

    :goto_5
    invoke-direct {p0, v0}, Lh0/n1;->I(Z)V

    iget-object p1, p0, Lh0/n1;->m:Le2/n;

    invoke-interface {p1, v1}, Le2/n;->c(I)Z

    return-wide p2
.end method

.method private H(Ljava/io/IOException;I)V
    .locals 1

    invoke-static {p1, p2}, Lh0/q;->g(Ljava/io/IOException;I)Lh0/q;

    move-result-object p1

    iget-object p2, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {p2}, Lh0/i2;->p()Lh0/f2;

    move-result-object p2

    if-eqz p2, :cond_0

    iget-object p2, p2, Lh0/f2;->f:Lh0/g2;

    iget-object p2, p2, Lh0/g2;->a:Lj1/x$b;

    invoke-virtual {p1, p2}, Lh0/q;->e(Lj1/v;)Lh0/q;

    move-result-object p1

    :cond_0
    const-string p2, "ExoPlayerImplInternal"

    const-string v0, "Playback error"

    invoke-static {p2, v0, p1}, Le2/r;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 p2, 0x0

    invoke-direct {p0, p2, p2}, Lh0/n1;->k1(ZZ)V

    iget-object p2, p0, Lh0/n1;->C:Lh0/d3;

    invoke-virtual {p2, p1}, Lh0/d3;->e(Lh0/q;)Lh0/d3;

    move-result-object p1

    iput-object p1, p0, Lh0/n1;->C:Lh0/d3;

    return-void
.end method

.method private H0(Lh0/k3;)V
    .locals 9

    invoke-virtual {p1}, Lh0/k3;->f()J

    move-result-wide v0

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    invoke-direct {p0, p1}, Lh0/n1;->I0(Lh0/k3;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v0, v0, Lh0/d3;->a:Lh0/c4;

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    new-instance v1, Lh0/n1$d;

    invoke-direct {v1, p1}, Lh0/n1$d;-><init>(Lh0/k3;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Lh0/n1$d;

    invoke-direct {v0, p1}, Lh0/n1$d;-><init>(Lh0/k3;)V

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v4, v1, Lh0/d3;->a:Lh0/c4;

    iget v5, p0, Lh0/n1;->J:I

    iget-boolean v6, p0, Lh0/n1;->K:Z

    iget-object v7, p0, Lh0/n1;->p:Lh0/c4$d;

    iget-object v8, p0, Lh0/n1;->q:Lh0/c4$b;

    move-object v2, v0

    move-object v3, v4

    invoke-static/range {v2 .. v8}, Lh0/n1;->w0(Lh0/n1$d;Lh0/c4;Lh0/c4;IZLh0/c4$d;Lh0/c4$b;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object p1, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lh0/k3;->k(Z)V

    :goto_0
    return-void
.end method

.method private I(Z)V
    .locals 5

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->j()Lh0/f2;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v1, Lh0/d3;->b:Lj1/x$b;

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lh0/f2;->f:Lh0/g2;

    iget-object v1, v1, Lh0/g2;->a:Lj1/x$b;

    :goto_0
    iget-object v2, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v2, v2, Lh0/d3;->k:Lj1/x$b;

    invoke-virtual {v2, v1}, Lj1/v;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    iget-object v3, p0, Lh0/n1;->C:Lh0/d3;

    invoke-virtual {v3, v1}, Lh0/d3;->b(Lj1/x$b;)Lh0/d3;

    move-result-object v1

    iput-object v1, p0, Lh0/n1;->C:Lh0/d3;

    :cond_1
    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    if-nez v0, :cond_2

    iget-wide v3, v1, Lh0/d3;->r:J

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lh0/f2;->i()J

    move-result-wide v3

    :goto_1
    iput-wide v3, v1, Lh0/d3;->p:J

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    invoke-direct {p0}, Lh0/n1;->E()J

    move-result-wide v3

    iput-wide v3, v1, Lh0/d3;->q:J

    if-nez v2, :cond_3

    if-eqz p1, :cond_4

    :cond_3
    if-eqz v0, :cond_4

    iget-boolean p1, v0, Lh0/f2;->d:Z

    if-eqz p1, :cond_4

    invoke-virtual {v0}, Lh0/f2;->n()Lj1/z0;

    move-result-object p1

    invoke-virtual {v0}, Lh0/f2;->o()Lc2/d0;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lh0/n1;->n1(Lj1/z0;Lc2/d0;)V

    :cond_4
    return-void
.end method

.method private I0(Lh0/k3;)V
    .locals 2

    invoke-virtual {p1}, Lh0/k3;->c()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lh0/n1;->o:Landroid/os/Looper;

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, Lh0/n1;->o(Lh0/k3;)V

    iget-object p1, p0, Lh0/n1;->C:Lh0/d3;

    iget p1, p1, Lh0/d3;->e:I

    const/4 v0, 0x3

    const/4 v1, 0x2

    if-eq p1, v0, :cond_0

    if-ne p1, v1, :cond_2

    :cond_0
    iget-object p1, p0, Lh0/n1;->m:Le2/n;

    invoke-interface {p1, v1}, Le2/n;->c(I)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/16 v1, 0xf

    invoke-interface {v0, v1, p1}, Le2/n;->h(ILjava/lang/Object;)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V

    :cond_2
    :goto_0
    return-void
.end method

.method private J(Lh0/c4;Z)V
    .locals 27

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    iget-object v2, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v3, v11, Lh0/n1;->P:Lh0/n1$h;

    iget-object v4, v11, Lh0/n1;->x:Lh0/i2;

    iget v5, v11, Lh0/n1;->J:I

    iget-boolean v6, v11, Lh0/n1;->K:Z

    iget-object v7, v11, Lh0/n1;->p:Lh0/c4$d;

    iget-object v8, v11, Lh0/n1;->q:Lh0/c4$b;

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v8}, Lh0/n1;->y0(Lh0/c4;Lh0/d3;Lh0/n1$h;Lh0/i2;IZLh0/c4$d;Lh0/c4$b;)Lh0/n1$g;

    move-result-object v7

    iget-object v9, v7, Lh0/n1$g;->a:Lj1/x$b;

    iget-wide v13, v7, Lh0/n1$g;->c:J

    iget-boolean v0, v7, Lh0/n1$g;->d:Z

    iget-wide v5, v7, Lh0/n1$g;->b:J

    iget-object v1, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v1, Lh0/d3;->b:Lj1/x$b;

    invoke-virtual {v1, v9}, Lj1/v;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v10, 0x1

    const/4 v15, 0x0

    if-eqz v1, :cond_1

    iget-object v1, v11, Lh0/n1;->C:Lh0/d3;

    iget-wide v1, v1, Lh0/d3;->r:J

    cmp-long v3, v5, v1

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const/16 v16, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/16 v16, 0x1

    :goto_1
    const/16 v17, 0x3

    const/4 v8, 0x0

    const-wide v18, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v4, 0x4

    :try_start_0
    iget-boolean v1, v7, Lh0/n1$g;->e:Z

    if-eqz v1, :cond_3

    iget-object v1, v11, Lh0/n1;->C:Lh0/d3;

    iget v1, v1, Lh0/d3;->e:I

    if-eq v1, v10, :cond_2

    invoke-direct {v11, v4}, Lh0/n1;->c1(I)V

    :cond_2
    invoke-direct {v11, v15, v15, v15, v10}, Lh0/n1;->s0(ZZZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    :cond_3
    if-nez v16, :cond_4

    :try_start_1
    iget-object v1, v11, Lh0/n1;->x:Lh0/i2;

    iget-wide v3, v11, Lh0/n1;->Q:J

    invoke-direct/range {p0 .. p0}, Lh0/n1;->B()J

    move-result-wide v22
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v2, p1

    const/4 v10, -0x1

    const/16 v20, 0x4

    move-wide/from16 v25, v5

    move-wide/from16 v5, v22

    :try_start_2
    invoke-virtual/range {v1 .. v6}, Lh0/i2;->F(Lh0/c4;JJ)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {v11, v15}, Lh0/n1;->D0(Z)V

    goto :goto_4

    :catchall_0
    move-exception v0

    const/4 v10, -0x1

    const/16 v20, 0x4

    :goto_2
    move-object v15, v8

    goto/16 :goto_9

    :cond_4
    move-wide/from16 v25, v5

    const/4 v10, -0x1

    const/16 v20, 0x4

    invoke-virtual/range {p1 .. p1}, Lh0/c4;->u()Z

    move-result v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    if-nez v1, :cond_7

    :try_start_3
    iget-object v1, v11, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v1}, Lh0/i2;->p()Lh0/f2;

    move-result-object v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :goto_3
    if-eqz v1, :cond_6

    :try_start_4
    iget-object v2, v1, Lh0/f2;->f:Lh0/g2;

    iget-object v2, v2, Lh0/g2;->a:Lj1/x$b;

    invoke-virtual {v2, v9}, Lj1/v;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, v11, Lh0/n1;->x:Lh0/i2;

    iget-object v3, v1, Lh0/f2;->f:Lh0/g2;

    invoke-virtual {v2, v12, v3}, Lh0/i2;->r(Lh0/c4;Lh0/g2;)Lh0/g2;

    move-result-object v2

    iput-object v2, v1, Lh0/f2;->f:Lh0/g2;

    invoke-virtual {v1}, Lh0/f2;->A()V

    :cond_5
    invoke-virtual {v1}, Lh0/f2;->j()Lh0/f2;

    move-result-object v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_3

    :cond_6
    move-wide/from16 v5, v25

    :try_start_5
    invoke-direct {v11, v9, v5, v6, v0}, Lh0/n1;->F0(Lj1/x$b;JZ)J

    move-result-wide v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-wide/from16 v21, v0

    goto :goto_5

    :catchall_1
    move-exception v0

    goto :goto_2

    :catchall_2
    move-exception v0

    move-wide/from16 v5, v25

    goto :goto_2

    :cond_7
    :goto_4
    move-wide/from16 v5, v25

    move-wide/from16 v21, v5

    :goto_5
    iget-object v0, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v4, v0, Lh0/d3;->a:Lh0/c4;

    iget-object v5, v0, Lh0/d3;->b:Lj1/x$b;

    iget-boolean v0, v7, Lh0/n1$g;->f:Z

    if-eqz v0, :cond_8

    move-wide/from16 v6, v21

    goto :goto_6

    :cond_8
    move-wide/from16 v6, v18

    :goto_6
    const/4 v0, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object v3, v9

    move-object v15, v8

    move v8, v0

    invoke-direct/range {v1 .. v8}, Lh0/n1;->q1(Lh0/c4;Lj1/x$b;Lh0/c4;Lj1/x$b;JZ)V

    if-nez v16, :cond_9

    iget-object v0, v11, Lh0/n1;->C:Lh0/d3;

    iget-wide v0, v0, Lh0/d3;->c:J

    cmp-long v2, v13, v0

    if-eqz v2, :cond_c

    :cond_9
    iget-object v0, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v0, Lh0/d3;->b:Lj1/x$b;

    iget-object v1, v1, Lj1/v;->a:Ljava/lang/Object;

    iget-object v0, v0, Lh0/d3;->a:Lh0/c4;

    if-eqz v16, :cond_a

    if-eqz p2, :cond_a

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, v11, Lh0/n1;->q:Lh0/c4$b;

    invoke-virtual {v0, v1, v2}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v0

    iget-boolean v0, v0, Lh0/c4$b;->k:Z

    if-nez v0, :cond_a

    const/16 v24, 0x1

    goto :goto_7

    :cond_a
    const/16 v24, 0x0

    :goto_7
    iget-object v0, v11, Lh0/n1;->C:Lh0/d3;

    iget-wide v7, v0, Lh0/d3;->d:J

    invoke-virtual {v12, v1}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result v0

    if-ne v0, v10, :cond_b

    const/4 v10, 0x4

    goto :goto_8

    :cond_b
    const/4 v10, 0x3

    :goto_8
    move-object/from16 v1, p0

    move-object v2, v9

    move-wide/from16 v3, v21

    move-wide v5, v13

    move/from16 v9, v24

    invoke-direct/range {v1 .. v10}, Lh0/n1;->N(Lj1/x$b;JJJZI)Lh0/d3;

    move-result-object v0

    iput-object v0, v11, Lh0/n1;->C:Lh0/d3;

    :cond_c
    invoke-direct/range {p0 .. p0}, Lh0/n1;->t0()V

    iget-object v0, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v0, v0, Lh0/d3;->a:Lh0/c4;

    invoke-direct {v11, v12, v0}, Lh0/n1;->x0(Lh0/c4;Lh0/c4;)V

    iget-object v0, v11, Lh0/n1;->C:Lh0/d3;

    invoke-virtual {v0, v12}, Lh0/d3;->i(Lh0/c4;)Lh0/d3;

    move-result-object v0

    iput-object v0, v11, Lh0/n1;->C:Lh0/d3;

    invoke-virtual/range {p1 .. p1}, Lh0/c4;->u()Z

    move-result v0

    if-nez v0, :cond_d

    iput-object v15, v11, Lh0/n1;->P:Lh0/n1$h;

    :cond_d
    const/4 v1, 0x0

    invoke-direct {v11, v1}, Lh0/n1;->I(Z)V

    return-void

    :catchall_3
    move-exception v0

    move-object v15, v8

    move-wide/from16 v5, v25

    goto :goto_9

    :catchall_4
    move-exception v0

    move-object v15, v8

    const/4 v10, -0x1

    const/16 v20, 0x4

    :goto_9
    iget-object v1, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v4, v1, Lh0/d3;->a:Lh0/c4;

    iget-object v8, v1, Lh0/d3;->b:Lj1/x$b;

    iget-boolean v1, v7, Lh0/n1$g;->f:Z

    if-eqz v1, :cond_e

    move-wide/from16 v18, v5

    :cond_e
    const/16 v21, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object v3, v9

    move-wide/from16 v25, v5

    move-object v5, v8

    move-wide/from16 v6, v18

    move/from16 v8, v21

    invoke-direct/range {v1 .. v8}, Lh0/n1;->q1(Lh0/c4;Lj1/x$b;Lh0/c4;Lj1/x$b;JZ)V

    if-nez v16, :cond_f

    iget-object v1, v11, Lh0/n1;->C:Lh0/d3;

    iget-wide v1, v1, Lh0/d3;->c:J

    cmp-long v3, v13, v1

    if-eqz v3, :cond_12

    :cond_f
    iget-object v1, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v2, v1, Lh0/d3;->b:Lj1/x$b;

    iget-object v2, v2, Lj1/v;->a:Ljava/lang/Object;

    iget-object v1, v1, Lh0/d3;->a:Lh0/c4;

    if-eqz v16, :cond_10

    if-eqz p2, :cond_10

    invoke-virtual {v1}, Lh0/c4;->u()Z

    move-result v3

    if-nez v3, :cond_10

    iget-object v3, v11, Lh0/n1;->q:Lh0/c4$b;

    invoke-virtual {v1, v2, v3}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v1

    iget-boolean v1, v1, Lh0/c4$b;->k:Z

    if-nez v1, :cond_10

    const/16 v24, 0x1

    goto :goto_a

    :cond_10
    const/16 v24, 0x0

    :goto_a
    iget-object v1, v11, Lh0/n1;->C:Lh0/d3;

    iget-wide v7, v1, Lh0/d3;->d:J

    invoke-virtual {v12, v2}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result v1

    if-ne v1, v10, :cond_11

    const/4 v10, 0x4

    goto :goto_b

    :cond_11
    const/4 v10, 0x3

    :goto_b
    move-object/from16 v1, p0

    move-object v2, v9

    move-wide/from16 v3, v25

    move-wide v5, v13

    move/from16 v9, v24

    invoke-direct/range {v1 .. v10}, Lh0/n1;->N(Lj1/x$b;JJJZI)Lh0/d3;

    move-result-object v1

    iput-object v1, v11, Lh0/n1;->C:Lh0/d3;

    :cond_12
    invoke-direct/range {p0 .. p0}, Lh0/n1;->t0()V

    iget-object v1, v11, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v1, Lh0/d3;->a:Lh0/c4;

    invoke-direct {v11, v12, v1}, Lh0/n1;->x0(Lh0/c4;Lh0/c4;)V

    iget-object v1, v11, Lh0/n1;->C:Lh0/d3;

    invoke-virtual {v1, v12}, Lh0/d3;->i(Lh0/c4;)Lh0/d3;

    move-result-object v1

    iput-object v1, v11, Lh0/n1;->C:Lh0/d3;

    invoke-virtual/range {p1 .. p1}, Lh0/c4;->u()Z

    move-result v1

    if-nez v1, :cond_13

    iput-object v15, v11, Lh0/n1;->P:Lh0/n1$h;

    :cond_13
    const/4 v1, 0x0

    invoke-direct {v11, v1}, Lh0/n1;->I(Z)V

    throw v0
.end method

.method private J0(Lh0/k3;)V
    .locals 3

    invoke-virtual {p1}, Lh0/k3;->c()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "TAG"

    const-string v1, "Trying to send message on a dead thread."

    invoke-static {v0, v1}, Le2/r;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lh0/k3;->k(Z)V

    return-void

    :cond_0
    iget-object v1, p0, Lh0/n1;->v:Le2/d;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Le2/d;->b(Landroid/os/Looper;Landroid/os/Handler$Callback;)Le2/n;

    move-result-object v0

    new-instance v1, Lh0/l1;

    invoke-direct {v1, p0, p1}, Lh0/l1;-><init>(Lh0/n1;Lh0/k3;)V

    invoke-interface {v0, v1}, Le2/n;->k(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private K(Lj1/u;)V
    .locals 11

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0, p1}, Lh0/i2;->v(Lj1/u;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {p1}, Lh0/i2;->j()Lh0/f2;

    move-result-object p1

    iget-object v0, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v0}, Lh0/l;->g()Lh0/f3;

    move-result-object v0

    iget v0, v0, Lh0/f3;->f:F

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v1, Lh0/d3;->a:Lh0/c4;

    invoke-virtual {p1, v0, v1}, Lh0/f2;->p(FLh0/c4;)V

    invoke-virtual {p1}, Lh0/f2;->n()Lj1/z0;

    move-result-object v0

    invoke-virtual {p1}, Lh0/f2;->o()Lc2/d0;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lh0/n1;->n1(Lj1/z0;Lc2/d0;)V

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    if-ne p1, v0, :cond_1

    iget-object v0, p1, Lh0/f2;->f:Lh0/g2;

    iget-wide v0, v0, Lh0/g2;->b:J

    invoke-direct {p0, v0, v1}, Lh0/n1;->u0(J)V

    invoke-direct {p0}, Lh0/n1;->s()V

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v2, v0, Lh0/d3;->b:Lj1/x$b;

    iget-object p1, p1, Lh0/f2;->f:Lh0/g2;

    iget-wide v7, p1, Lh0/g2;->b:J

    iget-wide v5, v0, Lh0/d3;->c:J

    const/4 v9, 0x0

    const/4 v10, 0x5

    move-object v1, p0

    move-wide v3, v7

    invoke-direct/range {v1 .. v10}, Lh0/n1;->N(Lj1/x$b;JJJZI)Lh0/d3;

    move-result-object p1

    iput-object p1, p0, Lh0/n1;->C:Lh0/d3;

    :cond_1
    invoke-direct {p0}, Lh0/n1;->X()V

    return-void
.end method

.method private K0(J)V
    .locals 5

    iget-object v0, p0, Lh0/n1;->f:[Lh0/p3;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    invoke-interface {v3}, Lh0/p3;->j()Lj1/q0;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v3, p1, p2}, Lh0/n1;->L0(Lh0/p3;J)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private L(Lh0/f3;FZZ)V
    .locals 3

    if-eqz p3, :cond_1

    if-eqz p4, :cond_0

    iget-object p3, p0, Lh0/n1;->D:Lh0/n1$e;

    const/4 p4, 0x1

    invoke-virtual {p3, p4}, Lh0/n1$e;->b(I)V

    :cond_0
    iget-object p3, p0, Lh0/n1;->C:Lh0/d3;

    invoke-virtual {p3, p1}, Lh0/d3;->f(Lh0/f3;)Lh0/d3;

    move-result-object p3

    iput-object p3, p0, Lh0/n1;->C:Lh0/d3;

    :cond_1
    iget p3, p1, Lh0/f3;->f:F

    invoke-direct {p0, p3}, Lh0/n1;->r1(F)V

    iget-object p3, p0, Lh0/n1;->f:[Lh0/p3;

    array-length p4, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p4, :cond_3

    aget-object v1, p3, v0

    if-eqz v1, :cond_2

    iget v2, p1, Lh0/f3;->f:F

    invoke-interface {v1, p2, v2}, Lh0/p3;->B(FF)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private L0(Lh0/p3;J)V
    .locals 1

    invoke-interface {p1}, Lh0/p3;->r()V

    instance-of v0, p1, Ls1/o;

    if-eqz v0, :cond_0

    check-cast p1, Ls1/o;

    invoke-virtual {p1, p2, p3}, Ls1/o;->e0(J)V

    :cond_0
    return-void
.end method

.method private M(Lh0/f3;Z)V
    .locals 2

    iget v0, p1, Lh0/f3;->f:F

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1, p2}, Lh0/n1;->L(Lh0/f3;FZZ)V

    return-void
.end method

.method private M0(ZLjava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 4

    iget-boolean v0, p0, Lh0/n1;->L:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lh0/n1;->L:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lh0/n1;->f:[Lh0/p3;

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p1, v1

    invoke-static {v2}, Lh0/n1;->S(Lh0/p3;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lh0/n1;->g:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Lh0/p3;->c()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    monitor-enter p0

    const/4 p1, 0x1

    :try_start_0
    invoke-virtual {p2, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_2
    :goto_1
    return-void
.end method

.method private N(Lj1/x$b;JJJZI)Lh0/d3;
    .locals 14

    move-object v0, p0

    move-object v2, p1

    move-wide/from16 v5, p4

    iget-boolean v1, v0, Lh0/n1;->S:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lh0/n1;->C:Lh0/d3;

    iget-wide v3, v1, Lh0/d3;->r:J

    cmp-long v1, p2, v3

    if-nez v1, :cond_1

    iget-object v1, v0, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v1, Lh0/d3;->b:Lj1/x$b;

    invoke-virtual {p1, v1}, Lj1/v;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, v0, Lh0/n1;->S:Z

    invoke-direct {p0}, Lh0/n1;->t0()V

    iget-object v1, v0, Lh0/n1;->C:Lh0/d3;

    iget-object v3, v1, Lh0/d3;->h:Lj1/z0;

    iget-object v4, v1, Lh0/d3;->i:Lc2/d0;

    iget-object v1, v1, Lh0/d3;->j:Ljava/util/List;

    iget-object v7, v0, Lh0/n1;->y:Lh0/x2;

    invoke-virtual {v7}, Lh0/x2;->s()Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v1, v0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v1}, Lh0/i2;->p()Lh0/f2;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v3, Lj1/z0;->i:Lj1/z0;

    goto :goto_2

    :cond_2
    invoke-virtual {v1}, Lh0/f2;->n()Lj1/z0;

    move-result-object v3

    :goto_2
    if-nez v1, :cond_3

    iget-object v4, v0, Lh0/n1;->j:Lc2/d0;

    goto :goto_3

    :cond_3
    invoke-virtual {v1}, Lh0/f2;->o()Lc2/d0;

    move-result-object v4

    :goto_3
    iget-object v7, v4, Lc2/d0;->c:[Lc2/t;

    invoke-direct {p0, v7}, Lh0/n1;->x([Lc2/t;)Lp2/q;

    move-result-object v7

    if-eqz v1, :cond_4

    iget-object v8, v1, Lh0/f2;->f:Lh0/g2;

    iget-wide v9, v8, Lh0/g2;->c:J

    cmp-long v11, v9, v5

    if-eqz v11, :cond_4

    invoke-virtual {v8, v5, v6}, Lh0/g2;->a(J)Lh0/g2;

    move-result-object v8

    iput-object v8, v1, Lh0/f2;->f:Lh0/g2;

    :cond_4
    move-object v11, v3

    move-object v12, v4

    move-object v13, v7

    goto :goto_4

    :cond_5
    iget-object v7, v0, Lh0/n1;->C:Lh0/d3;

    iget-object v7, v7, Lh0/d3;->b:Lj1/x$b;

    invoke-virtual {p1, v7}, Lj1/v;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    sget-object v1, Lj1/z0;->i:Lj1/z0;

    iget-object v3, v0, Lh0/n1;->j:Lc2/d0;

    invoke-static {}, Lp2/q;->q()Lp2/q;

    move-result-object v4

    move-object v11, v1

    move-object v12, v3

    move-object v13, v4

    goto :goto_4

    :cond_6
    move-object v13, v1

    move-object v11, v3

    move-object v12, v4

    :goto_4
    if-eqz p8, :cond_7

    iget-object v1, v0, Lh0/n1;->D:Lh0/n1$e;

    move/from16 v3, p9

    invoke-virtual {v1, v3}, Lh0/n1$e;->e(I)V

    :cond_7
    iget-object v1, v0, Lh0/n1;->C:Lh0/d3;

    invoke-direct {p0}, Lh0/n1;->E()J

    move-result-wide v9

    move-object v2, p1

    move-wide/from16 v3, p2

    move-wide/from16 v5, p4

    move-wide/from16 v7, p6

    invoke-virtual/range {v1 .. v13}, Lh0/d3;->c(Lj1/x$b;JJJJLj1/z0;Lc2/d0;Ljava/util/List;)Lh0/d3;

    move-result-object v1

    return-object v1
.end method

.method private N0(Lh0/f3;)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/16 v1, 0x10

    invoke-interface {v0, v1}, Le2/n;->f(I)V

    iget-object v0, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v0, p1}, Lh0/l;->h(Lh0/f3;)V

    return-void
.end method

.method private O(Lh0/p3;Lh0/f2;)Z
    .locals 3

    invoke-virtual {p2}, Lh0/f2;->j()Lh0/f2;

    move-result-object v0

    iget-object p2, p2, Lh0/f2;->f:Lh0/g2;

    iget-boolean p2, p2, Lh0/g2;->f:Z

    if-eqz p2, :cond_1

    iget-boolean p2, v0, Lh0/f2;->d:Z

    if-eqz p2, :cond_1

    instance-of p2, p1, Ls1/o;

    if-nez p2, :cond_0

    instance-of p2, p1, Lz0/g;

    if-nez p2, :cond_0

    invoke-interface {p1}, Lh0/p3;->t()J

    move-result-wide p1

    invoke-virtual {v0}, Lh0/f2;->m()J

    move-result-wide v0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private O0(Lh0/n1$b;)V
    .locals 5

    iget-object v0, p0, Lh0/n1;->D:Lh0/n1$e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lh0/n1$e;->b(I)V

    invoke-static {p1}, Lh0/n1$b;->a(Lh0/n1$b;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Lh0/n1$h;

    new-instance v1, Lh0/l3;

    invoke-static {p1}, Lh0/n1$b;->b(Lh0/n1$b;)Ljava/util/List;

    move-result-object v2

    invoke-static {p1}, Lh0/n1$b;->c(Lh0/n1$b;)Lj1/s0;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lh0/l3;-><init>(Ljava/util/Collection;Lj1/s0;)V

    invoke-static {p1}, Lh0/n1$b;->a(Lh0/n1$b;)I

    move-result v2

    invoke-static {p1}, Lh0/n1$b;->d(Lh0/n1$b;)J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lh0/n1$h;-><init>(Lh0/c4;IJ)V

    iput-object v0, p0, Lh0/n1;->P:Lh0/n1$h;

    :cond_0
    iget-object v0, p0, Lh0/n1;->y:Lh0/x2;

    invoke-static {p1}, Lh0/n1$b;->b(Lh0/n1$b;)Ljava/util/List;

    move-result-object v1

    invoke-static {p1}, Lh0/n1$b;->c(Lh0/n1$b;)Lj1/s0;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lh0/x2;->C(Ljava/util/List;Lj1/s0;)Lh0/c4;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lh0/n1;->J(Lh0/c4;Z)V

    return-void
.end method

.method private P()Z
    .locals 6

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->q()Lh0/f2;

    move-result-object v0

    iget-boolean v1, v0, Lh0/f2;->d:Z

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lh0/n1;->f:[Lh0/p3;

    array-length v4, v3

    if-ge v1, v4, :cond_3

    aget-object v3, v3, v1

    iget-object v4, v0, Lh0/f2;->c:[Lj1/q0;

    aget-object v4, v4, v1

    invoke-interface {v3}, Lh0/p3;->j()Lj1/q0;

    move-result-object v5

    if-ne v5, v4, :cond_2

    if-eqz v4, :cond_1

    invoke-interface {v3}, Lh0/p3;->n()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-direct {p0, v3, v0}, Lh0/n1;->O(Lh0/p3;Lh0/f2;)Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v2

    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method private static Q(ZLj1/x$b;JLj1/x$b;Lh0/c4$b;J)Z
    .locals 1

    const/4 v0, 0x0

    if-nez p0, :cond_3

    cmp-long p0, p2, p6

    if-nez p0, :cond_3

    iget-object p0, p1, Lj1/v;->a:Ljava/lang/Object;

    iget-object p2, p4, Lj1/v;->a:Ljava/lang/Object;

    invoke-virtual {p0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lj1/v;->b()Z

    move-result p0

    const/4 p2, 0x1

    if-eqz p0, :cond_2

    iget p0, p1, Lj1/v;->b:I

    invoke-virtual {p5, p0}, Lh0/c4$b;->t(I)Z

    move-result p0

    if-eqz p0, :cond_2

    iget p0, p1, Lj1/v;->b:I

    iget p3, p1, Lj1/v;->c:I

    invoke-virtual {p5, p0, p3}, Lh0/c4$b;->k(II)I

    move-result p0

    const/4 p3, 0x4

    if-eq p0, p3, :cond_1

    iget p0, p1, Lj1/v;->b:I

    iget p1, p1, Lj1/v;->c:I

    invoke-virtual {p5, p0, p1}, Lh0/c4$b;->k(II)I

    move-result p0

    const/4 p1, 0x2

    if-eq p0, p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    :cond_2
    invoke-virtual {p4}, Lj1/v;->b()Z

    move-result p0

    if-eqz p0, :cond_3

    iget p0, p4, Lj1/v;->b:I

    invoke-virtual {p5, p0}, Lh0/c4$b;->t(I)Z

    move-result p0

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    :cond_3
    :goto_0
    return v0
.end method

.method private Q0(Z)V
    .locals 1

    iget-boolean v0, p0, Lh0/n1;->N:Z

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Lh0/n1;->N:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lh0/n1;->C:Lh0/d3;

    iget-boolean p1, p1, Lh0/d3;->o:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lh0/n1;->m:Le2/n;

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Le2/n;->c(I)Z

    :cond_1
    return-void
.end method

.method private R()Z
    .locals 6

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->j()Lh0/f2;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {v0}, Lh0/f2;->k()J

    move-result-wide v2

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private R0(Z)V
    .locals 1

    iput-boolean p1, p0, Lh0/n1;->F:Z

    invoke-direct {p0}, Lh0/n1;->t0()V

    iget-boolean p1, p0, Lh0/n1;->G:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {p1}, Lh0/i2;->q()Lh0/f2;

    move-result-object p1

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lh0/n1;->D0(Z)V

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lh0/n1;->I(Z)V

    :cond_0
    return-void
.end method

.method private static S(Lh0/p3;)Z
    .locals 0

    invoke-interface {p0}, Lh0/p3;->getState()I

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private T()Z
    .locals 5

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    iget-object v1, v0, Lh0/f2;->f:Lh0/g2;

    iget-wide v1, v1, Lh0/g2;->e:J

    iget-boolean v0, v0, Lh0/f2;->d:Z

    if-eqz v0, :cond_1

    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v1, v3

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-wide v3, v0, Lh0/d3;->r:J

    cmp-long v0, v3, v1

    if-ltz v0, :cond_0

    invoke-direct {p0}, Lh0/n1;->f1()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private T0(ZIZI)V
    .locals 1

    iget-object v0, p0, Lh0/n1;->D:Lh0/n1$e;

    invoke-virtual {v0, p3}, Lh0/n1$e;->b(I)V

    iget-object p3, p0, Lh0/n1;->D:Lh0/n1$e;

    invoke-virtual {p3, p4}, Lh0/n1$e;->c(I)V

    iget-object p3, p0, Lh0/n1;->C:Lh0/d3;

    invoke-virtual {p3, p1, p2}, Lh0/d3;->d(ZI)Lh0/d3;

    move-result-object p2

    iput-object p2, p0, Lh0/n1;->C:Lh0/d3;

    const/4 p2, 0x0

    iput-boolean p2, p0, Lh0/n1;->H:Z

    invoke-direct {p0, p1}, Lh0/n1;->h0(Z)V

    invoke-direct {p0}, Lh0/n1;->f1()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-direct {p0}, Lh0/n1;->l1()V

    invoke-direct {p0}, Lh0/n1;->p1()V

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lh0/n1;->C:Lh0/d3;

    iget p1, p1, Lh0/d3;->e:I

    const/4 p2, 0x3

    const/4 p3, 0x2

    if-ne p1, p2, :cond_1

    invoke-direct {p0}, Lh0/n1;->i1()V

    :goto_0
    iget-object p1, p0, Lh0/n1;->m:Le2/n;

    invoke-interface {p1, p3}, Le2/n;->c(I)Z

    goto :goto_1

    :cond_1
    if-ne p1, p3, :cond_2

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method private static U(Lh0/d3;Lh0/c4$b;)Z
    .locals 2

    iget-object v0, p0, Lh0/d3;->b:Lj1/x$b;

    iget-object p0, p0, Lh0/d3;->a:Lh0/c4;

    invoke-virtual {p0}, Lh0/c4;->u()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, v0, Lj1/v;->a:Ljava/lang/Object;

    invoke-virtual {p0, v0, p1}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object p0

    iget-boolean p0, p0, Lh0/c4$b;->k:Z

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private synthetic V()Ljava/lang/Boolean;
    .locals 1

    iget-boolean v0, p0, Lh0/n1;->E:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private V0(Lh0/f3;)V
    .locals 1

    invoke-direct {p0, p1}, Lh0/n1;->N0(Lh0/f3;)V

    iget-object p1, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {p1}, Lh0/l;->g()Lh0/f3;

    move-result-object p1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lh0/n1;->M(Lh0/f3;Z)V

    return-void
.end method

.method private synthetic W(Lh0/k3;)V
    .locals 2

    :try_start_0
    invoke-direct {p0, p1}, Lh0/n1;->o(Lh0/k3;)V
    :try_end_0
    .catch Lh0/q; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    const-string v0, "ExoPlayerImplInternal"

    const-string v1, "Unexpected error delivering message on external thread."

    invoke-static {v0, v1, p1}, Le2/r;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private X()V
    .locals 3

    invoke-direct {p0}, Lh0/n1;->e1()Z

    move-result v0

    iput-boolean v0, p0, Lh0/n1;->I:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->j()Lh0/f2;

    move-result-object v0

    iget-wide v1, p0, Lh0/n1;->Q:J

    invoke-virtual {v0, v1, v2}, Lh0/f2;->d(J)V

    :cond_0
    invoke-direct {p0}, Lh0/n1;->m1()V

    return-void
.end method

.method private X0(I)V
    .locals 2

    iput p1, p0, Lh0/n1;->J:I

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v1, Lh0/d3;->a:Lh0/c4;

    invoke-virtual {v0, v1, p1}, Lh0/i2;->G(Lh0/c4;I)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lh0/n1;->D0(Z)V

    :cond_0
    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lh0/n1;->I(Z)V

    return-void
.end method

.method private Y()V
    .locals 2

    iget-object v0, p0, Lh0/n1;->D:Lh0/n1$e;

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    invoke-virtual {v0, v1}, Lh0/n1$e;->d(Lh0/d3;)V

    iget-object v0, p0, Lh0/n1;->D:Lh0/n1$e;

    invoke-static {v0}, Lh0/n1$e;->a(Lh0/n1$e;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh0/n1;->w:Lh0/n1$f;

    iget-object v1, p0, Lh0/n1;->D:Lh0/n1$e;

    invoke-interface {v0, v1}, Lh0/n1$f;->a(Lh0/n1$e;)V

    new-instance v0, Lh0/n1$e;

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    invoke-direct {v0, v1}, Lh0/n1$e;-><init>(Lh0/d3;)V

    iput-object v0, p0, Lh0/n1;->D:Lh0/n1$e;

    :cond_0
    return-void
.end method

.method private Y0(Lh0/u3;)V
    .locals 0

    iput-object p1, p0, Lh0/n1;->B:Lh0/u3;

    return-void
.end method

.method private Z(JJ)V
    .locals 7

    iget-object v0, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v0, v0, Lh0/d3;->b:Lj1/x$b;

    invoke-virtual {v0}, Lj1/v;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_7

    :cond_0
    iget-boolean v0, p0, Lh0/n1;->S:Z

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x1

    sub-long/2addr p1, v0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lh0/n1;->S:Z

    :cond_1
    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v0, Lh0/d3;->a:Lh0/c4;

    iget-object v0, v0, Lh0/d3;->b:Lj1/x$b;

    iget-object v0, v0, Lj1/v;->a:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result v0

    iget v1, p0, Lh0/n1;->R:I

    iget-object v2, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v2, 0x0

    if-lez v1, :cond_2

    :goto_0
    iget-object v3, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lh0/n1$d;

    goto :goto_1

    :cond_2
    move-object v3, v2

    :goto_1
    if-eqz v3, :cond_4

    iget v4, v3, Lh0/n1$d;->g:I

    if-gt v4, v0, :cond_3

    if-ne v4, v0, :cond_4

    iget-wide v3, v3, Lh0/n1$d;->h:J

    cmp-long v5, v3, p1

    if-lez v5, :cond_4

    :cond_3
    add-int/lit8 v1, v1, -0x1

    if-lez v1, :cond_2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_5

    :goto_2
    iget-object v3, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lh0/n1$d;

    goto :goto_3

    :cond_5
    move-object v3, v2

    :goto_3
    if-eqz v3, :cond_7

    iget-object v4, v3, Lh0/n1$d;->i:Ljava/lang/Object;

    if-eqz v4, :cond_7

    iget v4, v3, Lh0/n1$d;->g:I

    if-lt v4, v0, :cond_6

    if-ne v4, v0, :cond_7

    iget-wide v4, v3, Lh0/n1$d;->h:J

    cmp-long v6, v4, p1

    if-gtz v6, :cond_7

    :cond_6
    add-int/lit8 v1, v1, 0x1

    iget-object v3, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_5

    goto :goto_2

    :cond_7
    :goto_4
    if-eqz v3, :cond_d

    iget-object v4, v3, Lh0/n1$d;->i:Ljava/lang/Object;

    if-eqz v4, :cond_d

    iget v4, v3, Lh0/n1$d;->g:I

    if-ne v4, v0, :cond_d

    iget-wide v4, v3, Lh0/n1$d;->h:J

    cmp-long v6, v4, p1

    if-lez v6, :cond_d

    cmp-long v6, v4, p3

    if-gtz v6, :cond_d

    :try_start_0
    iget-object v4, v3, Lh0/n1$d;->f:Lh0/k3;

    invoke-direct {p0, v4}, Lh0/n1;->I0(Lh0/k3;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v4, v3, Lh0/n1$d;->f:Lh0/k3;

    invoke-virtual {v4}, Lh0/k3;->b()Z

    move-result v4

    if-nez v4, :cond_9

    iget-object v3, v3, Lh0/n1$d;->f:Lh0/k3;

    invoke-virtual {v3}, Lh0/k3;->j()Z

    move-result v3

    if-eqz v3, :cond_8

    goto :goto_5

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_9
    :goto_5
    iget-object v3, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_6
    iget-object v3, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_a

    iget-object v3, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lh0/n1$d;

    goto :goto_4

    :cond_a
    move-object v3, v2

    goto :goto_4

    :catchall_0
    move-exception p1

    iget-object p2, v3, Lh0/n1$d;->f:Lh0/k3;

    invoke-virtual {p2}, Lh0/k3;->b()Z

    move-result p2

    if-nez p2, :cond_b

    iget-object p2, v3, Lh0/n1$d;->f:Lh0/k3;

    invoke-virtual {p2}, Lh0/k3;->j()Z

    move-result p2

    if-eqz p2, :cond_c

    :cond_b
    iget-object p2, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_c
    throw p1

    :cond_d
    iput v1, p0, Lh0/n1;->R:I

    :cond_e
    :goto_7
    return-void
.end method

.method private a0()V
    .locals 11

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    iget-wide v1, p0, Lh0/n1;->Q:J

    invoke-virtual {v0, v1, v2}, Lh0/i2;->y(J)V

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    iget-wide v1, p0, Lh0/n1;->Q:J

    iget-object v3, p0, Lh0/n1;->C:Lh0/d3;

    invoke-virtual {v0, v1, v2, v3}, Lh0/i2;->o(JLh0/d3;)Lh0/g2;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v4, p0, Lh0/n1;->x:Lh0/i2;

    iget-object v5, p0, Lh0/n1;->h:[Lh0/r3;

    iget-object v6, p0, Lh0/n1;->i:Lc2/c0;

    iget-object v1, p0, Lh0/n1;->k:Lh0/x1;

    invoke-interface {v1}, Lh0/x1;->h()Ld2/b;

    move-result-object v7

    iget-object v8, p0, Lh0/n1;->y:Lh0/x2;

    iget-object v10, p0, Lh0/n1;->j:Lc2/d0;

    move-object v9, v0

    invoke-virtual/range {v4 .. v10}, Lh0/i2;->g([Lh0/r3;Lc2/c0;Ld2/b;Lh0/x2;Lh0/g2;Lc2/d0;)Lh0/f2;

    move-result-object v1

    iget-object v2, v1, Lh0/f2;->a:Lj1/u;

    iget-wide v3, v0, Lh0/g2;->b:J

    invoke-interface {v2, p0, v3, v4}, Lj1/u;->p(Lj1/u$a;J)V

    iget-object v2, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v2}, Lh0/i2;->p()Lh0/f2;

    move-result-object v2

    if-ne v2, v1, :cond_0

    iget-wide v0, v0, Lh0/g2;->b:J

    invoke-direct {p0, v0, v1}, Lh0/n1;->u0(J)V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lh0/n1;->I(Z)V

    :cond_1
    iget-boolean v0, p0, Lh0/n1;->I:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lh0/n1;->R()Z

    move-result v0

    iput-boolean v0, p0, Lh0/n1;->I:Z

    invoke-direct {p0}, Lh0/n1;->m1()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lh0/n1;->X()V

    :goto_0
    return-void
.end method

.method private a1(Z)V
    .locals 2

    iput-boolean p1, p0, Lh0/n1;->K:Z

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v1, Lh0/d3;->a:Lh0/c4;

    invoke-virtual {v0, v1, p1}, Lh0/i2;->H(Lh0/c4;Z)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lh0/n1;->D0(Z)V

    :cond_0
    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lh0/n1;->I(Z)V

    return-void
.end method

.method private b0()V
    .locals 14

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    :goto_0
    invoke-direct {p0}, Lh0/n1;->d1()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lh0/n1;->Y()V

    :cond_0
    iget-object v2, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v2}, Lh0/i2;->b()Lh0/f2;

    move-result-object v2

    invoke-static {v2}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lh0/f2;

    iget-object v3, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v3, v3, Lh0/d3;->b:Lj1/x$b;

    iget-object v3, v3, Lj1/v;->a:Ljava/lang/Object;

    iget-object v4, v2, Lh0/f2;->f:Lh0/g2;

    iget-object v4, v4, Lh0/g2;->a:Lj1/x$b;

    iget-object v4, v4, Lj1/v;->a:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v3, v3, Lh0/d3;->b:Lj1/x$b;

    iget v4, v3, Lj1/v;->b:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    iget-object v4, v2, Lh0/f2;->f:Lh0/g2;

    iget-object v4, v4, Lh0/g2;->a:Lj1/x$b;

    iget v6, v4, Lj1/v;->b:I

    if-ne v6, v5, :cond_1

    iget v3, v3, Lj1/v;->e:I

    iget v4, v4, Lj1/v;->e:I

    if-eq v3, v4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    iget-object v2, v2, Lh0/f2;->f:Lh0/g2;

    iget-object v5, v2, Lh0/g2;->a:Lj1/x$b;

    iget-wide v10, v2, Lh0/g2;->b:J

    iget-wide v8, v2, Lh0/g2;->c:J

    xor-int/lit8 v12, v3, 0x1

    const/4 v13, 0x0

    move-object v4, p0

    move-wide v6, v10

    invoke-direct/range {v4 .. v13}, Lh0/n1;->N(Lj1/x$b;JJJZI)Lh0/d3;

    move-result-object v2

    iput-object v2, p0, Lh0/n1;->C:Lh0/d3;

    invoke-direct {p0}, Lh0/n1;->t0()V

    invoke-direct {p0}, Lh0/n1;->p1()V

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private b1(Lj1/s0;)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->D:Lh0/n1$e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lh0/n1$e;->b(I)V

    iget-object v0, p0, Lh0/n1;->y:Lh0/x2;

    invoke-virtual {v0, p1}, Lh0/x2;->D(Lj1/s0;)Lh0/c4;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lh0/n1;->J(Lh0/c4;Z)V

    return-void
.end method

.method private c0()V
    .locals 14

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->q()Lh0/f2;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Lh0/f2;->j()Lh0/f2;

    move-result-object v1

    const-wide v8, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v10, 0x0

    if-eqz v1, :cond_9

    iget-boolean v1, p0, Lh0/n1;->G:Z

    if-eqz v1, :cond_1

    goto/16 :goto_2

    :cond_1
    invoke-direct {p0}, Lh0/n1;->P()Z

    move-result v1

    if-nez v1, :cond_2

    return-void

    :cond_2
    invoke-virtual {v0}, Lh0/f2;->j()Lh0/f2;

    move-result-object v1

    iget-boolean v1, v1, Lh0/f2;->d:Z

    if-nez v1, :cond_3

    iget-wide v1, p0, Lh0/n1;->Q:J

    invoke-virtual {v0}, Lh0/f2;->j()Lh0/f2;

    move-result-object v3

    invoke-virtual {v3}, Lh0/f2;->m()J

    move-result-wide v3

    cmp-long v5, v1, v3

    if-gez v5, :cond_3

    return-void

    :cond_3
    invoke-virtual {v0}, Lh0/f2;->o()Lc2/d0;

    move-result-object v11

    iget-object v1, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v1}, Lh0/i2;->c()Lh0/f2;

    move-result-object v12

    invoke-virtual {v12}, Lh0/f2;->o()Lc2/d0;

    move-result-object v13

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v3, v1, Lh0/d3;->a:Lh0/c4;

    iget-object v1, v12, Lh0/f2;->f:Lh0/g2;

    iget-object v2, v1, Lh0/g2;->a:Lj1/x$b;

    iget-object v0, v0, Lh0/f2;->f:Lh0/g2;

    iget-object v4, v0, Lh0/g2;->a:Lj1/x$b;

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, v3

    invoke-direct/range {v0 .. v7}, Lh0/n1;->q1(Lh0/c4;Lj1/x$b;Lh0/c4;Lj1/x$b;JZ)V

    iget-boolean v0, v12, Lh0/f2;->d:Z

    if-eqz v0, :cond_4

    iget-object v0, v12, Lh0/f2;->a:Lj1/u;

    invoke-interface {v0}, Lj1/u;->n()J

    move-result-wide v0

    cmp-long v2, v0, v8

    if-eqz v2, :cond_4

    invoke-virtual {v12}, Lh0/f2;->m()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lh0/n1;->K0(J)V

    return-void

    :cond_4
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lh0/n1;->f:[Lh0/p3;

    array-length v1, v1

    if-ge v0, v1, :cond_8

    invoke-virtual {v11, v0}, Lc2/d0;->c(I)Z

    move-result v1

    invoke-virtual {v13, v0}, Lc2/d0;->c(I)Z

    move-result v2

    if-eqz v1, :cond_7

    iget-object v1, p0, Lh0/n1;->f:[Lh0/p3;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lh0/p3;->v()Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lh0/n1;->h:[Lh0/r3;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lh0/r3;->l()I

    move-result v1

    const/4 v3, -0x2

    if-ne v1, v3, :cond_5

    const/4 v1, 0x1

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    :goto_1
    iget-object v3, v11, Lc2/d0;->b:[Lh0/s3;

    aget-object v3, v3, v0

    iget-object v4, v13, Lc2/d0;->b:[Lh0/s3;

    aget-object v4, v4, v0

    if-eqz v2, :cond_6

    invoke-virtual {v4, v3}, Lh0/s3;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    if-eqz v1, :cond_7

    :cond_6
    iget-object v1, p0, Lh0/n1;->f:[Lh0/p3;

    aget-object v1, v1, v0

    invoke-virtual {v12}, Lh0/f2;->m()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lh0/n1;->L0(Lh0/p3;J)V

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_8
    return-void

    :cond_9
    :goto_2
    iget-object v1, v0, Lh0/f2;->f:Lh0/g2;

    iget-boolean v1, v1, Lh0/g2;->i:Z

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lh0/n1;->G:Z

    if-eqz v1, :cond_d

    :cond_a
    :goto_3
    iget-object v1, p0, Lh0/n1;->f:[Lh0/p3;

    array-length v2, v1

    if-ge v10, v2, :cond_d

    aget-object v1, v1, v10

    iget-object v2, v0, Lh0/f2;->c:[Lj1/q0;

    aget-object v2, v2, v10

    if-eqz v2, :cond_c

    invoke-interface {v1}, Lh0/p3;->j()Lj1/q0;

    move-result-object v3

    if-ne v3, v2, :cond_c

    invoke-interface {v1}, Lh0/p3;->n()Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, v0, Lh0/f2;->f:Lh0/g2;

    iget-wide v2, v2, Lh0/g2;->e:J

    cmp-long v4, v2, v8

    if-eqz v4, :cond_b

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v6, v2, v4

    if-eqz v6, :cond_b

    invoke-virtual {v0}, Lh0/f2;->l()J

    move-result-wide v2

    iget-object v4, v0, Lh0/f2;->f:Lh0/g2;

    iget-wide v4, v4, Lh0/g2;->e:J

    add-long/2addr v2, v4

    goto :goto_4

    :cond_b
    move-wide v2, v8

    :goto_4
    invoke-direct {p0, v1, v2, v3}, Lh0/n1;->L0(Lh0/p3;J)V

    :cond_c
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_d
    return-void
.end method

.method private c1(I)V
    .locals 3

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget v1, v0, Lh0/d3;->e:I

    if-eq v1, p1, :cond_1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v1, p0, Lh0/n1;->V:J

    :cond_0
    invoke-virtual {v0, p1}, Lh0/d3;->g(I)Lh0/d3;

    move-result-object p1

    iput-object p1, p0, Lh0/n1;->C:Lh0/d3;

    :cond_1
    return-void
.end method

.method private d0()V
    .locals 2

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->q()Lh0/f2;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v1}, Lh0/i2;->p()Lh0/f2;

    move-result-object v1

    if-eq v1, v0, :cond_1

    iget-boolean v0, v0, Lh0/f2;->g:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lh0/n1;->q0()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lh0/n1;->s()V

    :cond_1
    :goto_0
    return-void
.end method

.method private d1()Z
    .locals 7

    invoke-direct {p0}, Lh0/n1;->f1()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-boolean v0, p0, Lh0/n1;->G:Z

    if-eqz v0, :cond_1

    return v1

    :cond_1
    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    if-nez v0, :cond_2

    return v1

    :cond_2
    invoke-virtual {v0}, Lh0/f2;->j()Lh0/f2;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-wide v2, p0, Lh0/n1;->Q:J

    invoke-virtual {v0}, Lh0/f2;->m()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-ltz v6, :cond_3

    iget-boolean v0, v0, Lh0/f2;->g:Z

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method private e0()V
    .locals 2

    iget-object v0, p0, Lh0/n1;->y:Lh0/x2;

    invoke-virtual {v0}, Lh0/x2;->i()Lh0/c4;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lh0/n1;->J(Lh0/c4;Z)V

    return-void
.end method

.method private e1()Z
    .locals 12

    invoke-direct {p0}, Lh0/n1;->R()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->j()Lh0/f2;

    move-result-object v0

    invoke-virtual {v0}, Lh0/f2;->k()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lh0/n1;->F(J)J

    move-result-wide v2

    iget-object v4, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v4}, Lh0/i2;->p()Lh0/f2;

    move-result-object v4

    if-ne v0, v4, :cond_1

    iget-wide v4, p0, Lh0/n1;->Q:J

    invoke-virtual {v0, v4, v5}, Lh0/f2;->y(J)J

    move-result-wide v4

    goto :goto_0

    :cond_1
    iget-wide v4, p0, Lh0/n1;->Q:J

    invoke-virtual {v0, v4, v5}, Lh0/f2;->y(J)J

    move-result-wide v4

    iget-object v0, v0, Lh0/f2;->f:Lh0/g2;

    iget-wide v6, v0, Lh0/g2;->b:J

    sub-long/2addr v4, v6

    :goto_0
    move-wide v10, v4

    iget-object v4, p0, Lh0/n1;->k:Lh0/x1;

    iget-object v0, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v0}, Lh0/l;->g()Lh0/f3;

    move-result-object v0

    iget v9, v0, Lh0/f3;->f:F

    move-wide v5, v10

    move-wide v7, v2

    invoke-interface/range {v4 .. v9}, Lh0/x1;->g(JJF)Z

    move-result v0

    if-nez v0, :cond_3

    const-wide/32 v4, 0x7a120

    cmp-long v6, v2, v4

    if-gez v6, :cond_3

    iget-wide v4, p0, Lh0/n1;->r:J

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-gtz v8, :cond_2

    iget-boolean v4, p0, Lh0/n1;->s:Z

    if-eqz v4, :cond_3

    :cond_2
    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    iget-object v0, v0, Lh0/f2;->a:Lj1/u;

    iget-object v4, p0, Lh0/n1;->C:Lh0/d3;

    iget-wide v4, v4, Lh0/d3;->r:J

    invoke-interface {v0, v4, v5, v1}, Lj1/u;->r(JZ)V

    iget-object v4, p0, Lh0/n1;->k:Lh0/x1;

    iget-object v0, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v0}, Lh0/l;->g()Lh0/f3;

    move-result-object v0

    iget v9, v0, Lh0/f3;->f:F

    move-wide v5, v10

    move-wide v7, v2

    invoke-interface/range {v4 .. v9}, Lh0/x1;->g(JJF)Z

    move-result v0

    :cond_3
    return v0
.end method

.method public static synthetic f(Lh0/n1;)Ljava/lang/Boolean;
    .locals 0

    invoke-direct {p0}, Lh0/n1;->V()Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private f0(Lh0/n1$c;)V
    .locals 4

    iget-object v0, p0, Lh0/n1;->D:Lh0/n1$e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lh0/n1$e;->b(I)V

    iget-object v0, p0, Lh0/n1;->y:Lh0/x2;

    iget v1, p1, Lh0/n1$c;->a:I

    iget v2, p1, Lh0/n1$c;->b:I

    iget v3, p1, Lh0/n1$c;->c:I

    iget-object p1, p1, Lh0/n1$c;->d:Lj1/s0;

    invoke-virtual {v0, v1, v2, v3, p1}, Lh0/x2;->v(IIILj1/s0;)Lh0/c4;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lh0/n1;->J(Lh0/c4;Z)V

    return-void
.end method

.method private f1()Z
    .locals 2

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-boolean v1, v0, Lh0/d3;->l:Z

    if-eqz v1, :cond_0

    iget v0, v0, Lh0/d3;->m:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic g(Lh0/n1;Lh0/k3;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/n1;->W(Lh0/k3;)V

    return-void
.end method

.method private g0()V
    .locals 5

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lh0/f2;->o()Lc2/d0;

    move-result-object v1

    iget-object v1, v1, Lc2/d0;->c:[Lc2/t;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    if-eqz v4, :cond_0

    invoke-interface {v4}, Lc2/t;->v()V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lh0/f2;->j()Lh0/f2;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private g1(Z)Z
    .locals 12

    iget v0, p0, Lh0/n1;->O:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lh0/n1;->T()Z

    move-result p1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    return v0

    :cond_1
    iget-object p1, p0, Lh0/n1;->C:Lh0/d3;

    iget-boolean v1, p1, Lh0/d3;->g:Z

    const/4 v2, 0x1

    if-nez v1, :cond_2

    return v2

    :cond_2
    iget-object p1, p1, Lh0/d3;->a:Lh0/c4;

    iget-object v1, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v1}, Lh0/i2;->p()Lh0/f2;

    move-result-object v1

    iget-object v1, v1, Lh0/f2;->f:Lh0/g2;

    iget-object v1, v1, Lh0/g2;->a:Lj1/x$b;

    invoke-direct {p0, p1, v1}, Lh0/n1;->h1(Lh0/c4;Lj1/x$b;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lh0/n1;->z:Lh0/w1;

    invoke-interface {p1}, Lh0/w1;->e()J

    move-result-wide v3

    goto :goto_0

    :cond_3
    const-wide v3, -0x7fffffffffffffffL    # -4.9E-324

    :goto_0
    move-wide v10, v3

    iget-object p1, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {p1}, Lh0/i2;->j()Lh0/f2;

    move-result-object p1

    invoke-virtual {p1}, Lh0/f2;->q()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p1, Lh0/f2;->f:Lh0/g2;

    iget-boolean v1, v1, Lh0/g2;->i:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    :goto_1
    iget-object v3, p1, Lh0/f2;->f:Lh0/g2;

    iget-object v3, v3, Lh0/g2;->a:Lj1/x$b;

    invoke-virtual {v3}, Lj1/v;->b()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-boolean p1, p1, Lh0/f2;->d:Z

    if-nez p1, :cond_5

    const/4 p1, 0x1

    goto :goto_2

    :cond_5
    const/4 p1, 0x0

    :goto_2
    if-nez v1, :cond_6

    if-nez p1, :cond_6

    iget-object v5, p0, Lh0/n1;->k:Lh0/x1;

    invoke-direct {p0}, Lh0/n1;->E()J

    move-result-wide v6

    iget-object p1, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {p1}, Lh0/l;->g()Lh0/f3;

    move-result-object p1

    iget v8, p1, Lh0/f3;->f:F

    iget-boolean v9, p0, Lh0/n1;->H:Z

    invoke-interface/range {v5 .. v11}, Lh0/x1;->e(JFZJ)Z

    move-result p1

    if-eqz p1, :cond_7

    :cond_6
    const/4 v0, 0x1

    :cond_7
    return v0
.end method

.method private h0(Z)V
    .locals 5

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lh0/f2;->o()Lc2/d0;

    move-result-object v1

    iget-object v1, v1, Lc2/d0;->c:[Lc2/t;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    if-eqz v4, :cond_0

    invoke-interface {v4, p1}, Lc2/t;->l(Z)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lh0/f2;->j()Lh0/f2;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private h1(Lh0/c4;Lj1/x$b;)Z
    .locals 4

    invoke-virtual {p2}, Lj1/v;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lh0/c4;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p2, p2, Lj1/v;->a:Ljava/lang/Object;

    iget-object v0, p0, Lh0/n1;->q:Lh0/c4$b;

    invoke-virtual {p1, p2, v0}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object p2

    iget p2, p2, Lh0/c4$b;->h:I

    iget-object v0, p0, Lh0/n1;->p:Lh0/c4$d;

    invoke-virtual {p1, p2, v0}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    iget-object p1, p0, Lh0/n1;->p:Lh0/c4$d;

    invoke-virtual {p1}, Lh0/c4$d;->h()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lh0/n1;->p:Lh0/c4$d;

    iget-boolean p2, p1, Lh0/c4$d;->n:Z

    if-eqz p2, :cond_1

    iget-wide p1, p1, Lh0/c4$d;->k:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, p1, v2

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    :goto_0
    return v1
.end method

.method private i0()V
    .locals 5

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lh0/f2;->o()Lc2/d0;

    move-result-object v1

    iget-object v1, v1, Lc2/d0;->c:[Lc2/t;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    if-eqz v4, :cond_0

    invoke-interface {v4}, Lc2/t;->x()V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lh0/f2;->j()Lh0/f2;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private i1()V
    .locals 5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lh0/n1;->H:Z

    iget-object v1, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v1}, Lh0/l;->e()V

    iget-object v1, p0, Lh0/n1;->f:[Lh0/p3;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    invoke-static {v3}, Lh0/n1;->S(Lh0/p3;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Lh0/p3;->d()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic k(Lh0/n1;Z)Z
    .locals 0

    iput-boolean p1, p0, Lh0/n1;->M:Z

    return p1
.end method

.method private k1(ZZ)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lh0/n1;->L:Z

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    invoke-direct {p0, p1, v0, v1, v0}, Lh0/n1;->s0(ZZZZ)V

    iget-object p1, p0, Lh0/n1;->D:Lh0/n1$e;

    invoke-virtual {p1, p2}, Lh0/n1$e;->b(I)V

    iget-object p1, p0, Lh0/n1;->k:Lh0/x1;

    invoke-interface {p1}, Lh0/x1;->i()V

    invoke-direct {p0, v1}, Lh0/n1;->c1(I)V

    return-void
.end method

.method static synthetic l(Lh0/n1;)Le2/n;
    .locals 0

    iget-object p0, p0, Lh0/n1;->m:Le2/n;

    return-object p0
.end method

.method private l0()V
    .locals 3

    iget-object v0, p0, Lh0/n1;->D:Lh0/n1$e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lh0/n1$e;->b(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, v0, v1}, Lh0/n1;->s0(ZZZZ)V

    iget-object v0, p0, Lh0/n1;->k:Lh0/x1;

    invoke-interface {v0}, Lh0/x1;->b()V

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v0, v0, Lh0/d3;->a:Lh0/c4;

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v0

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    :goto_0
    invoke-direct {p0, v0}, Lh0/n1;->c1(I)V

    iget-object v0, p0, Lh0/n1;->y:Lh0/x2;

    iget-object v2, p0, Lh0/n1;->l:Ld2/f;

    invoke-interface {v2}, Ld2/f;->b()Ld2/p0;

    move-result-object v2

    invoke-virtual {v0, v2}, Lh0/x2;->w(Ld2/p0;)V

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    invoke-interface {v0, v1}, Le2/n;->c(I)Z

    return-void
.end method

.method private l1()V
    .locals 5

    iget-object v0, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v0}, Lh0/l;->f()V

    iget-object v0, p0, Lh0/n1;->f:[Lh0/p3;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    invoke-static {v3}, Lh0/n1;->S(Lh0/p3;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v3}, Lh0/n1;->u(Lh0/p3;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private m(Lh0/n1$b;I)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->D:Lh0/n1$e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lh0/n1$e;->b(I)V

    iget-object v0, p0, Lh0/n1;->y:Lh0/x2;

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    invoke-virtual {v0}, Lh0/x2;->q()I

    move-result p2

    :cond_0
    invoke-static {p1}, Lh0/n1$b;->b(Lh0/n1$b;)Ljava/util/List;

    move-result-object v1

    invoke-static {p1}, Lh0/n1$b;->c(Lh0/n1$b;)Lj1/s0;

    move-result-object p1

    invoke-virtual {v0, p2, v1, p1}, Lh0/x2;->f(ILjava/util/List;Lj1/s0;)Lh0/c4;

    move-result-object p1

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2}, Lh0/n1;->J(Lh0/c4;Z)V

    return-void
.end method

.method private m1()V
    .locals 3

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->j()Lh0/f2;

    move-result-object v0

    iget-boolean v1, p0, Lh0/n1;->I:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    iget-object v0, v0, Lh0/f2;->a:Lj1/u;

    invoke-interface {v0}, Lj1/u;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    iget-boolean v2, v1, Lh0/d3;->g:Z

    if-eq v0, v2, :cond_2

    invoke-virtual {v1, v0}, Lh0/d3;->a(Z)Lh0/d3;

    move-result-object v0

    iput-object v0, p0, Lh0/n1;->C:Lh0/d3;

    :cond_2
    return-void
.end method

.method private n()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lh0/n1;->D0(Z)V

    return-void
.end method

.method private n0()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v0, v1}, Lh0/n1;->s0(ZZZZ)V

    iget-object v1, p0, Lh0/n1;->k:Lh0/x1;

    invoke-interface {v1}, Lh0/x1;->d()V

    invoke-direct {p0, v0}, Lh0/n1;->c1(I)V

    iget-object v1, p0, Lh0/n1;->n:Landroid/os/HandlerThread;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    :cond_0
    monitor-enter p0

    :try_start_0
    iput-boolean v0, p0, Lh0/n1;->E:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private n1(Lj1/z0;Lc2/d0;)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->k:Lh0/x1;

    iget-object v1, p0, Lh0/n1;->f:[Lh0/p3;

    iget-object p2, p2, Lc2/d0;->c:[Lc2/t;

    invoke-interface {v0, v1, p1, p2}, Lh0/x1;->f([Lh0/p3;Lj1/z0;[Lc2/t;)V

    return-void
.end method

.method private o(Lh0/k3;)V
    .locals 4

    invoke-virtual {p1}, Lh0/k3;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1}, Lh0/k3;->g()Lh0/k3$b;

    move-result-object v1

    invoke-virtual {p1}, Lh0/k3;->i()I

    move-result v2

    invoke-virtual {p1}, Lh0/k3;->e()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lh0/k3$b;->q(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1, v0}, Lh0/k3;->k(Z)V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {p1, v0}, Lh0/k3;->k(Z)V

    throw v1
.end method

.method private o0(IILj1/s0;)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->D:Lh0/n1$e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lh0/n1$e;->b(I)V

    iget-object v0, p0, Lh0/n1;->y:Lh0/x2;

    invoke-virtual {v0, p1, p2, p3}, Lh0/x2;->A(IILj1/s0;)Lh0/c4;

    move-result-object p1

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2}, Lh0/n1;->J(Lh0/c4;Z)V

    return-void
.end method

.method private o1()V
    .locals 1

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v0, v0, Lh0/d3;->a:Lh0/c4;

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lh0/n1;->y:Lh0/x2;

    invoke-virtual {v0}, Lh0/x2;->s()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lh0/n1;->a0()V

    invoke-direct {p0}, Lh0/n1;->c0()V

    invoke-direct {p0}, Lh0/n1;->d0()V

    invoke-direct {p0}, Lh0/n1;->b0()V

    :cond_1
    :goto_0
    return-void
.end method

.method private p(Lh0/p3;)V
    .locals 1

    invoke-static {p1}, Lh0/n1;->S(Lh0/p3;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v0, p1}, Lh0/l;->a(Lh0/p3;)V

    invoke-direct {p0, p1}, Lh0/n1;->u(Lh0/p3;)V

    invoke-interface {p1}, Lh0/p3;->i()V

    iget p1, p0, Lh0/n1;->O:I

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lh0/n1;->O:I

    return-void
.end method

.method private p1()V
    .locals 11

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-boolean v1, v0, Lh0/f2;->d:Z

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    if-eqz v1, :cond_1

    iget-object v1, v0, Lh0/f2;->a:Lj1/u;

    invoke-interface {v1}, Lj1/u;->n()J

    move-result-wide v4

    move-wide v6, v4

    goto :goto_0

    :cond_1
    move-wide v6, v2

    :goto_0
    const/4 v10, 0x0

    cmp-long v1, v6, v2

    if-eqz v1, :cond_2

    invoke-direct {p0, v6, v7}, Lh0/n1;->u0(J)V

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-wide v0, v0, Lh0/d3;->r:J

    cmp-long v2, v6, v0

    if-eqz v2, :cond_4

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v0, Lh0/d3;->b:Lj1/x$b;

    iget-wide v4, v0, Lh0/d3;->c:J

    const/4 v8, 0x1

    const/4 v9, 0x5

    move-object v0, p0

    move-wide v2, v6

    invoke-direct/range {v0 .. v9}, Lh0/n1;->N(Lj1/x$b;JJJZI)Lh0/d3;

    move-result-object v0

    iput-object v0, p0, Lh0/n1;->C:Lh0/d3;

    goto :goto_2

    :cond_2
    iget-object v1, p0, Lh0/n1;->t:Lh0/l;

    iget-object v2, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v2}, Lh0/i2;->q()Lh0/f2;

    move-result-object v2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v2}, Lh0/l;->i(Z)J

    move-result-wide v1

    iput-wide v1, p0, Lh0/n1;->Q:J

    invoke-virtual {v0, v1, v2}, Lh0/f2;->y(J)J

    move-result-wide v0

    iget-object v2, p0, Lh0/n1;->C:Lh0/d3;

    iget-wide v2, v2, Lh0/d3;->r:J

    invoke-direct {p0, v2, v3, v0, v1}, Lh0/n1;->Z(JJ)V

    iget-object v2, p0, Lh0/n1;->C:Lh0/d3;

    iput-wide v0, v2, Lh0/d3;->r:J

    :cond_4
    :goto_2
    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->j()Lh0/f2;

    move-result-object v0

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    invoke-virtual {v0}, Lh0/f2;->i()J

    move-result-wide v2

    iput-wide v2, v1, Lh0/d3;->p:J

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    invoke-direct {p0}, Lh0/n1;->E()J

    move-result-wide v1

    iput-wide v1, v0, Lh0/d3;->q:J

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-boolean v1, v0, Lh0/d3;->l:Z

    if-eqz v1, :cond_5

    iget v1, v0, Lh0/d3;->e:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    iget-object v1, v0, Lh0/d3;->a:Lh0/c4;

    iget-object v0, v0, Lh0/d3;->b:Lj1/x$b;

    invoke-direct {p0, v1, v0}, Lh0/n1;->h1(Lh0/c4;Lj1/x$b;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v0, v0, Lh0/d3;->n:Lh0/f3;

    iget v0, v0, Lh0/f3;->f:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_5

    iget-object v0, p0, Lh0/n1;->z:Lh0/w1;

    invoke-direct {p0}, Lh0/n1;->y()J

    move-result-wide v1

    invoke-direct {p0}, Lh0/n1;->E()J

    move-result-wide v3

    invoke-interface {v0, v1, v2, v3, v4}, Lh0/w1;->c(JJ)F

    move-result v0

    iget-object v1, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v1}, Lh0/l;->g()Lh0/f3;

    move-result-object v1

    iget v1, v1, Lh0/f3;->f:F

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_5

    iget-object v1, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v1, Lh0/d3;->n:Lh0/f3;

    invoke-virtual {v1, v0}, Lh0/f3;->d(F)Lh0/f3;

    move-result-object v0

    invoke-direct {p0, v0}, Lh0/n1;->N0(Lh0/f3;)V

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v0, v0, Lh0/d3;->n:Lh0/f3;

    iget-object v1, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v1}, Lh0/l;->g()Lh0/f3;

    move-result-object v1

    iget v1, v1, Lh0/f3;->f:F

    invoke-direct {p0, v0, v1, v10, v10}, Lh0/n1;->L(Lh0/f3;FZZ)V

    :cond_5
    return-void
.end method

.method private q()V
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lh0/n1;->v:Le2/d;

    invoke-interface {v1}, Le2/d;->a()J

    move-result-wide v1

    iget-object v3, v0, Lh0/n1;->m:Le2/n;

    const/4 v4, 0x2

    invoke-interface {v3, v4}, Le2/n;->f(I)V

    invoke-direct/range {p0 .. p0}, Lh0/n1;->o1()V

    iget-object v3, v0, Lh0/n1;->C:Lh0/d3;

    iget v3, v3, Lh0/d3;->e:I

    const/4 v5, 0x1

    if-eq v3, v5, :cond_22

    const/4 v6, 0x4

    if-ne v3, v6, :cond_0

    goto/16 :goto_13

    :cond_0
    iget-object v3, v0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v3}, Lh0/i2;->p()Lh0/f2;

    move-result-object v3

    const-wide/16 v7, 0xa

    if-nez v3, :cond_1

    invoke-direct {v0, v1, v2, v7, v8}, Lh0/n1;->B0(JJ)V

    return-void

    :cond_1
    const-string v9, "doSomeWork"

    invoke-static {v9}, Le2/k0;->a(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lh0/n1;->p1()V

    iget-boolean v9, v3, Lh0/f2;->d:Z

    const-wide/16 v10, 0x3e8

    const/4 v12, 0x0

    if-eqz v9, :cond_a

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v13

    mul-long v13, v13, v10

    iget-object v9, v3, Lh0/f2;->a:Lj1/u;

    iget-object v15, v0, Lh0/n1;->C:Lh0/d3;

    iget-wide v7, v15, Lh0/d3;->r:J

    iget-wide v10, v0, Lh0/n1;->r:J

    sub-long/2addr v7, v10

    iget-boolean v10, v0, Lh0/n1;->s:Z

    invoke-interface {v9, v7, v8, v10}, Lj1/u;->r(JZ)V

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x1

    :goto_0
    iget-object v10, v0, Lh0/n1;->f:[Lh0/p3;

    array-length v11, v10

    if-ge v7, v11, :cond_b

    aget-object v10, v10, v7

    invoke-static {v10}, Lh0/n1;->S(Lh0/p3;)Z

    move-result v11

    if-nez v11, :cond_2

    goto :goto_7

    :cond_2
    iget-wide v4, v0, Lh0/n1;->Q:J

    invoke-interface {v10, v4, v5, v13, v14}, Lh0/p3;->p(JJ)V

    if-eqz v8, :cond_3

    invoke-interface {v10}, Lh0/p3;->e()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v8, 0x1

    goto :goto_1

    :cond_3
    const/4 v8, 0x0

    :goto_1
    iget-object v4, v3, Lh0/f2;->c:[Lj1/q0;

    aget-object v4, v4, v7

    invoke-interface {v10}, Lh0/p3;->j()Lj1/q0;

    move-result-object v5

    if-eq v4, v5, :cond_4

    const/4 v4, 0x1

    goto :goto_2

    :cond_4
    const/4 v4, 0x0

    :goto_2
    if-nez v4, :cond_5

    invoke-interface {v10}, Lh0/p3;->n()Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    :goto_3
    if-nez v4, :cond_7

    if-nez v5, :cond_7

    invoke-interface {v10}, Lh0/p3;->k()Z

    move-result v4

    if-nez v4, :cond_7

    invoke-interface {v10}, Lh0/p3;->e()Z

    move-result v4

    if-eqz v4, :cond_6

    goto :goto_4

    :cond_6
    const/4 v4, 0x0

    goto :goto_5

    :cond_7
    :goto_4
    const/4 v4, 0x1

    :goto_5
    if-eqz v9, :cond_8

    if-eqz v4, :cond_8

    const/4 v9, 0x1

    goto :goto_6

    :cond_8
    const/4 v9, 0x0

    :goto_6
    if-nez v4, :cond_9

    invoke-interface {v10}, Lh0/p3;->s()V

    :cond_9
    :goto_7
    add-int/lit8 v7, v7, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x1

    goto :goto_0

    :cond_a
    iget-object v4, v3, Lh0/f2;->a:Lj1/u;

    invoke-interface {v4}, Lj1/u;->q()V

    const/4 v8, 0x1

    const/4 v9, 0x1

    :cond_b
    iget-object v4, v3, Lh0/f2;->f:Lh0/g2;

    iget-wide v4, v4, Lh0/g2;->e:J

    const-wide v13, -0x7fffffffffffffffL    # -4.9E-324

    if-eqz v8, :cond_d

    iget-boolean v7, v3, Lh0/f2;->d:Z

    if-eqz v7, :cond_d

    cmp-long v7, v4, v13

    if-eqz v7, :cond_c

    iget-object v7, v0, Lh0/n1;->C:Lh0/d3;

    iget-wide v7, v7, Lh0/d3;->r:J

    cmp-long v10, v4, v7

    if-gtz v10, :cond_d

    :cond_c
    const/4 v4, 0x1

    goto :goto_8

    :cond_d
    const/4 v4, 0x0

    :goto_8
    if-eqz v4, :cond_e

    iget-boolean v5, v0, Lh0/n1;->G:Z

    if-eqz v5, :cond_e

    iput-boolean v12, v0, Lh0/n1;->G:Z

    iget-object v5, v0, Lh0/n1;->C:Lh0/d3;

    iget v5, v5, Lh0/d3;->m:I

    const/4 v7, 0x5

    invoke-direct {v0, v12, v5, v12, v7}, Lh0/n1;->T0(ZIZI)V

    :cond_e
    const/4 v5, 0x3

    if-eqz v4, :cond_10

    iget-object v4, v3, Lh0/f2;->f:Lh0/g2;

    iget-boolean v4, v4, Lh0/g2;->i:Z

    if-eqz v4, :cond_10

    invoke-direct {v0, v6}, Lh0/n1;->c1(I)V

    :cond_f
    :goto_9
    invoke-direct/range {p0 .. p0}, Lh0/n1;->l1()V

    goto :goto_a

    :cond_10
    iget-object v4, v0, Lh0/n1;->C:Lh0/d3;

    iget v4, v4, Lh0/d3;->e:I

    const/4 v7, 0x2

    if-ne v4, v7, :cond_11

    invoke-direct {v0, v9}, Lh0/n1;->g1(Z)Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-direct {v0, v5}, Lh0/n1;->c1(I)V

    const/4 v4, 0x0

    iput-object v4, v0, Lh0/n1;->T:Lh0/q;

    invoke-direct/range {p0 .. p0}, Lh0/n1;->f1()Z

    move-result v4

    if-eqz v4, :cond_14

    invoke-direct/range {p0 .. p0}, Lh0/n1;->i1()V

    goto :goto_a

    :cond_11
    iget-object v4, v0, Lh0/n1;->C:Lh0/d3;

    iget v4, v4, Lh0/d3;->e:I

    if-ne v4, v5, :cond_14

    iget v4, v0, Lh0/n1;->O:I

    if-nez v4, :cond_12

    invoke-direct/range {p0 .. p0}, Lh0/n1;->T()Z

    move-result v4

    if-eqz v4, :cond_13

    goto :goto_a

    :cond_12
    if-nez v9, :cond_14

    :cond_13
    invoke-direct/range {p0 .. p0}, Lh0/n1;->f1()Z

    move-result v4

    iput-boolean v4, v0, Lh0/n1;->H:Z

    const/4 v4, 0x2

    invoke-direct {v0, v4}, Lh0/n1;->c1(I)V

    iget-boolean v4, v0, Lh0/n1;->H:Z

    if-eqz v4, :cond_f

    invoke-direct/range {p0 .. p0}, Lh0/n1;->i0()V

    iget-object v4, v0, Lh0/n1;->z:Lh0/w1;

    invoke-interface {v4}, Lh0/w1;->a()V

    goto :goto_9

    :cond_14
    :goto_a
    iget-object v4, v0, Lh0/n1;->C:Lh0/d3;

    iget v4, v4, Lh0/d3;->e:I

    const/4 v7, 0x2

    if-ne v4, v7, :cond_17

    const/4 v4, 0x0

    :goto_b
    iget-object v7, v0, Lh0/n1;->f:[Lh0/p3;

    array-length v8, v7

    if-ge v4, v8, :cond_16

    aget-object v7, v7, v4

    invoke-static {v7}, Lh0/n1;->S(Lh0/p3;)Z

    move-result v7

    if-eqz v7, :cond_15

    iget-object v7, v0, Lh0/n1;->f:[Lh0/p3;

    aget-object v7, v7, v4

    invoke-interface {v7}, Lh0/p3;->j()Lj1/q0;

    move-result-object v7

    iget-object v8, v3, Lh0/f2;->c:[Lj1/q0;

    aget-object v8, v8, v4

    if-ne v7, v8, :cond_15

    iget-object v7, v0, Lh0/n1;->f:[Lh0/p3;

    aget-object v7, v7, v4

    invoke-interface {v7}, Lh0/p3;->s()V

    :cond_15
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    :cond_16
    iget-object v3, v0, Lh0/n1;->C:Lh0/d3;

    iget-boolean v4, v3, Lh0/d3;->g:Z

    if-nez v4, :cond_17

    iget-wide v3, v3, Lh0/d3;->q:J

    const-wide/32 v7, 0x7a120

    cmp-long v9, v3, v7

    if-gez v9, :cond_17

    invoke-direct/range {p0 .. p0}, Lh0/n1;->R()Z

    move-result v3

    if-eqz v3, :cond_17

    const/4 v3, 0x1

    goto :goto_c

    :cond_17
    const/4 v3, 0x0

    :goto_c
    if-nez v3, :cond_18

    iput-wide v13, v0, Lh0/n1;->V:J

    goto :goto_d

    :cond_18
    iget-wide v3, v0, Lh0/n1;->V:J

    cmp-long v7, v3, v13

    iget-object v3, v0, Lh0/n1;->v:Le2/d;

    invoke-interface {v3}, Le2/d;->d()J

    move-result-wide v3

    if-nez v7, :cond_19

    iput-wide v3, v0, Lh0/n1;->V:J

    goto :goto_d

    :cond_19
    iget-wide v7, v0, Lh0/n1;->V:J

    sub-long/2addr v3, v7

    const-wide/16 v7, 0xfa0

    cmp-long v9, v3, v7

    if-gez v9, :cond_21

    :goto_d
    invoke-direct/range {p0 .. p0}, Lh0/n1;->f1()Z

    move-result v3

    if-eqz v3, :cond_1a

    iget-object v3, v0, Lh0/n1;->C:Lh0/d3;

    iget v3, v3, Lh0/d3;->e:I

    if-ne v3, v5, :cond_1a

    const/4 v3, 0x1

    goto :goto_e

    :cond_1a
    const/4 v3, 0x0

    :goto_e
    iget-boolean v4, v0, Lh0/n1;->N:Z

    if-eqz v4, :cond_1b

    iget-boolean v4, v0, Lh0/n1;->M:Z

    if-eqz v4, :cond_1b

    if-eqz v3, :cond_1b

    const/4 v15, 0x1

    goto :goto_f

    :cond_1b
    const/4 v15, 0x0

    :goto_f
    iget-object v4, v0, Lh0/n1;->C:Lh0/d3;

    iget-boolean v7, v4, Lh0/d3;->o:Z

    if-eq v7, v15, :cond_1c

    invoke-virtual {v4, v15}, Lh0/d3;->h(Z)Lh0/d3;

    move-result-object v4

    iput-object v4, v0, Lh0/n1;->C:Lh0/d3;

    :cond_1c
    iput-boolean v12, v0, Lh0/n1;->M:Z

    if-nez v15, :cond_20

    iget-object v4, v0, Lh0/n1;->C:Lh0/d3;

    iget v4, v4, Lh0/d3;->e:I

    if-ne v4, v6, :cond_1d

    goto :goto_12

    :cond_1d
    if-nez v3, :cond_1f

    const/4 v3, 0x2

    if-ne v4, v3, :cond_1e

    goto :goto_10

    :cond_1e
    if-ne v4, v5, :cond_20

    iget v3, v0, Lh0/n1;->O:I

    if-eqz v3, :cond_20

    const-wide/16 v3, 0x3e8

    goto :goto_11

    :cond_1f
    :goto_10
    const-wide/16 v3, 0xa

    :goto_11
    invoke-direct {v0, v1, v2, v3, v4}, Lh0/n1;->B0(JJ)V

    :cond_20
    :goto_12
    invoke-static {}, Le2/k0;->c()V

    return-void

    :cond_21
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Playback stuck buffering and not loading"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_22
    :goto_13
    return-void
.end method

.method private q0()Z
    .locals 15

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->q()Lh0/f2;

    move-result-object v0

    invoke-virtual {v0}, Lh0/f2;->o()Lc2/d0;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    iget-object v5, p0, Lh0/n1;->f:[Lh0/p3;

    array-length v6, v5

    const/4 v7, 0x1

    if-ge v3, v6, :cond_5

    aget-object v8, v5, v3

    invoke-static {v8}, Lh0/n1;->S(Lh0/p3;)Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_2

    :cond_0
    invoke-interface {v8}, Lh0/p3;->j()Lj1/q0;

    move-result-object v5

    iget-object v6, v0, Lh0/f2;->c:[Lj1/q0;

    aget-object v6, v6, v3

    if-eq v5, v6, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v1, v3}, Lc2/d0;->c(I)Z

    move-result v6

    if-eqz v6, :cond_2

    if-nez v5, :cond_2

    goto :goto_2

    :cond_2
    invoke-interface {v8}, Lh0/p3;->v()Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, v1, Lc2/d0;->c:[Lc2/t;

    aget-object v5, v5, v3

    invoke-static {v5}, Lh0/n1;->z(Lc2/t;)[Lh0/r1;

    move-result-object v9

    iget-object v5, v0, Lh0/f2;->c:[Lj1/q0;

    aget-object v10, v5, v3

    invoke-virtual {v0}, Lh0/f2;->m()J

    move-result-wide v11

    invoke-virtual {v0}, Lh0/f2;->l()J

    move-result-wide v13

    invoke-interface/range {v8 .. v14}, Lh0/p3;->y([Lh0/r1;Lj1/q0;JJ)V

    goto :goto_2

    :cond_3
    invoke-interface {v8}, Lh0/p3;->e()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-direct {p0, v8}, Lh0/n1;->p(Lh0/p3;)V

    goto :goto_2

    :cond_4
    const/4 v4, 0x1

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    xor-int/lit8 v0, v4, 0x1

    return v0
.end method

.method private q1(Lh0/c4;Lj1/x$b;Lh0/c4;Lj1/x$b;JZ)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lh0/n1;->h1(Lh0/c4;Lj1/x$b;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Lj1/v;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lh0/f3;->i:Lh0/f3;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lh0/n1;->C:Lh0/d3;

    iget-object p1, p1, Lh0/d3;->n:Lh0/f3;

    :goto_0
    iget-object p2, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {p2}, Lh0/l;->g()Lh0/f3;

    move-result-object p2

    invoke-virtual {p2, p1}, Lh0/f3;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_1

    invoke-direct {p0, p1}, Lh0/n1;->N0(Lh0/f3;)V

    iget-object p2, p0, Lh0/n1;->C:Lh0/d3;

    iget-object p2, p2, Lh0/d3;->n:Lh0/f3;

    iget p1, p1, Lh0/f3;->f:F

    const/4 p3, 0x0

    invoke-direct {p0, p2, p1, p3, p3}, Lh0/n1;->L(Lh0/f3;FZZ)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p2, Lj1/v;->a:Ljava/lang/Object;

    iget-object v1, p0, Lh0/n1;->q:Lh0/c4$b;

    invoke-virtual {p1, v0, v1}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v0

    iget v0, v0, Lh0/c4$b;->h:I

    iget-object v1, p0, Lh0/n1;->p:Lh0/c4$d;

    invoke-virtual {p1, v0, v1}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    iget-object v0, p0, Lh0/n1;->z:Lh0/w1;

    iget-object v1, p0, Lh0/n1;->p:Lh0/c4$d;

    iget-object v1, v1, Lh0/c4$d;->p:Lh0/z1$g;

    invoke-static {v1}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lh0/z1$g;

    invoke-interface {v0, v1}, Lh0/w1;->b(Lh0/z1$g;)V

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v2, p5, v0

    if-eqz v2, :cond_3

    iget-object p3, p0, Lh0/n1;->z:Lh0/w1;

    iget-object p2, p2, Lj1/v;->a:Ljava/lang/Object;

    invoke-direct {p0, p1, p2, p5, p6}, Lh0/n1;->A(Lh0/c4;Ljava/lang/Object;J)J

    move-result-wide p1

    invoke-interface {p3, p1, p2}, Lh0/w1;->d(J)V

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lh0/n1;->p:Lh0/c4$d;

    iget-object p1, p1, Lh0/c4$d;->f:Ljava/lang/Object;

    const/4 p2, 0x0

    invoke-virtual {p3}, Lh0/c4;->u()Z

    move-result p5

    if-nez p5, :cond_4

    iget-object p2, p4, Lj1/v;->a:Ljava/lang/Object;

    iget-object p4, p0, Lh0/n1;->q:Lh0/c4$b;

    invoke-virtual {p3, p2, p4}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object p2

    iget p2, p2, Lh0/c4$b;->h:I

    iget-object p4, p0, Lh0/n1;->p:Lh0/c4$d;

    invoke-virtual {p3, p2, p4}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    move-result-object p2

    iget-object p2, p2, Lh0/c4$d;->f:Ljava/lang/Object;

    :cond_4
    invoke-static {p2, p1}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    if-eqz p7, :cond_6

    :cond_5
    iget-object p1, p0, Lh0/n1;->z:Lh0/w1;

    invoke-interface {p1, v0, v1}, Lh0/w1;->d(J)V

    :cond_6
    :goto_1
    return-void
.end method

.method private r(IZ)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lh0/n1;->f:[Lh0/p3;

    aget-object v1, v1, p1

    invoke-static {v1}, Lh0/n1;->S(Lh0/p3;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    iget-object v2, v0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v2}, Lh0/i2;->q()Lh0/f2;

    move-result-object v2

    iget-object v3, v0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v3}, Lh0/i2;->p()Lh0/f2;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ne v2, v3, :cond_1

    const/4 v9, 0x1

    goto :goto_0

    :cond_1
    const/4 v9, 0x0

    :goto_0
    invoke-virtual {v2}, Lh0/f2;->o()Lc2/d0;

    move-result-object v3

    iget-object v6, v3, Lc2/d0;->b:[Lh0/s3;

    aget-object v6, v6, p1

    iget-object v3, v3, Lc2/d0;->c:[Lc2/t;

    aget-object v3, v3, p1

    invoke-static {v3}, Lh0/n1;->z(Lc2/t;)[Lh0/r1;

    move-result-object v7

    invoke-direct/range {p0 .. p0}, Lh0/n1;->f1()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, Lh0/n1;->C:Lh0/d3;

    iget v3, v3, Lh0/d3;->e:I

    const/4 v8, 0x3

    if-ne v3, v8, :cond_2

    const/4 v14, 0x1

    goto :goto_1

    :cond_2
    const/4 v14, 0x0

    :goto_1
    if-nez p2, :cond_3

    if-eqz v14, :cond_3

    const/4 v8, 0x1

    goto :goto_2

    :cond_3
    const/4 v8, 0x0

    :goto_2
    iget v3, v0, Lh0/n1;->O:I

    add-int/2addr v3, v5

    iput v3, v0, Lh0/n1;->O:I

    iget-object v3, v0, Lh0/n1;->g:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, v2, Lh0/f2;->c:[Lj1/q0;

    aget-object v5, v3, p1

    iget-wide v10, v0, Lh0/n1;->Q:J

    invoke-virtual {v2}, Lh0/f2;->m()J

    move-result-wide v12

    invoke-virtual {v2}, Lh0/f2;->l()J

    move-result-wide v15

    move-object v2, v1

    move-object v3, v6

    move-object v4, v7

    move-wide v6, v10

    move-wide v10, v12

    move-wide v12, v15

    invoke-interface/range {v2 .. v13}, Lh0/p3;->m(Lh0/s3;[Lh0/r1;Lj1/q0;JZZJJ)V

    const/16 v2, 0xb

    new-instance v3, Lh0/n1$a;

    invoke-direct {v3, v0}, Lh0/n1$a;-><init>(Lh0/n1;)V

    invoke-interface {v1, v2, v3}, Lh0/k3$b;->q(ILjava/lang/Object;)V

    iget-object v2, v0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v2, v1}, Lh0/l;->b(Lh0/p3;)V

    if-eqz v14, :cond_4

    invoke-interface {v1}, Lh0/p3;->d()V

    :cond_4
    return-void
.end method

.method private r0()V
    .locals 19

    move-object/from16 v10, p0

    iget-object v0, v10, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v0}, Lh0/l;->g()Lh0/f3;

    move-result-object v0

    iget v0, v0, Lh0/f3;->f:F

    iget-object v1, v10, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v1}, Lh0/i2;->p()Lh0/f2;

    move-result-object v1

    iget-object v2, v10, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v2}, Lh0/i2;->q()Lh0/f2;

    move-result-object v2

    const/4 v3, 0x1

    :goto_0
    if-eqz v1, :cond_b

    iget-boolean v4, v1, Lh0/f2;->d:Z

    if-nez v4, :cond_0

    goto/16 :goto_5

    :cond_0
    iget-object v4, v10, Lh0/n1;->C:Lh0/d3;

    iget-object v4, v4, Lh0/d3;->a:Lh0/c4;

    invoke-virtual {v1, v0, v4}, Lh0/f2;->v(FLh0/c4;)Lc2/d0;

    move-result-object v13

    invoke-virtual {v1}, Lh0/f2;->o()Lc2/d0;

    move-result-object v4

    invoke-virtual {v13, v4}, Lc2/d0;->a(Lc2/d0;)Z

    move-result v4

    const/4 v9, 0x0

    if-nez v4, :cond_9

    const/4 v8, 0x4

    iget-object v0, v10, Lh0/n1;->x:Lh0/i2;

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v6

    iget-object v0, v10, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0, v6}, Lh0/i2;->z(Lh0/f2;)Z

    move-result v16

    iget-object v0, v10, Lh0/n1;->f:[Lh0/p3;

    array-length v0, v0

    new-array v7, v0, [Z

    iget-object v0, v10, Lh0/n1;->C:Lh0/d3;

    iget-wide v14, v0, Lh0/d3;->r:J

    move-object v12, v6

    move-object/from16 v17, v7

    invoke-virtual/range {v12 .. v17}, Lh0/f2;->b(Lc2/d0;JZ[Z)J

    move-result-wide v12

    iget-object v0, v10, Lh0/n1;->C:Lh0/d3;

    iget v1, v0, Lh0/d3;->e:I

    if-eq v1, v8, :cond_1

    iget-wide v0, v0, Lh0/d3;->r:J

    cmp-long v2, v12, v0

    if-eqz v2, :cond_1

    const/4 v14, 0x1

    goto :goto_1

    :cond_1
    const/4 v14, 0x0

    :goto_1
    iget-object v0, v10, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v0, Lh0/d3;->b:Lj1/x$b;

    iget-wide v4, v0, Lh0/d3;->c:J

    iget-wide v2, v0, Lh0/d3;->d:J

    const/4 v15, 0x5

    move-object/from16 v0, p0

    move-wide/from16 v16, v2

    move-wide v2, v12

    move-object v11, v6

    move-object/from16 v18, v7

    move-wide/from16 v6, v16

    move v8, v14

    move v9, v15

    invoke-direct/range {v0 .. v9}, Lh0/n1;->N(Lj1/x$b;JJJZI)Lh0/d3;

    move-result-object v0

    iput-object v0, v10, Lh0/n1;->C:Lh0/d3;

    if-eqz v14, :cond_2

    invoke-direct {v10, v12, v13}, Lh0/n1;->u0(J)V

    :cond_2
    iget-object v0, v10, Lh0/n1;->f:[Lh0/p3;

    array-length v0, v0

    new-array v0, v0, [Z

    const/4 v9, 0x0

    :goto_2
    iget-object v1, v10, Lh0/n1;->f:[Lh0/p3;

    array-length v2, v1

    if-ge v9, v2, :cond_5

    aget-object v1, v1, v9

    invoke-static {v1}, Lh0/n1;->S(Lh0/p3;)Z

    move-result v2

    aput-boolean v2, v0, v9

    iget-object v2, v11, Lh0/f2;->c:[Lj1/q0;

    aget-object v2, v2, v9

    aget-boolean v3, v0, v9

    if-eqz v3, :cond_4

    invoke-interface {v1}, Lh0/p3;->j()Lj1/q0;

    move-result-object v3

    if-eq v2, v3, :cond_3

    invoke-direct {v10, v1}, Lh0/n1;->p(Lh0/p3;)V

    goto :goto_3

    :cond_3
    aget-boolean v2, v18, v9

    if-eqz v2, :cond_4

    iget-wide v2, v10, Lh0/n1;->Q:J

    invoke-interface {v1, v2, v3}, Lh0/p3;->u(J)V

    :cond_4
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_5
    invoke-direct {v10, v0}, Lh0/n1;->t([Z)V

    goto :goto_4

    :cond_6
    invoke-virtual {v0, v1}, Lh0/i2;->z(Lh0/f2;)Z

    iget-boolean v0, v1, Lh0/f2;->d:Z

    if-eqz v0, :cond_7

    iget-object v0, v1, Lh0/f2;->f:Lh0/g2;

    iget-wide v2, v0, Lh0/g2;->b:J

    iget-wide v4, v10, Lh0/n1;->Q:J

    invoke-virtual {v1, v4, v5}, Lh0/f2;->y(J)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-virtual {v1, v13, v2, v3, v4}, Lh0/f2;->a(Lc2/d0;JZ)J

    :cond_7
    :goto_4
    const/4 v5, 0x1

    invoke-direct {v10, v5}, Lh0/n1;->I(Z)V

    iget-object v0, v10, Lh0/n1;->C:Lh0/d3;

    iget v0, v0, Lh0/d3;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_8

    invoke-direct/range {p0 .. p0}, Lh0/n1;->X()V

    invoke-direct/range {p0 .. p0}, Lh0/n1;->p1()V

    iget-object v0, v10, Lh0/n1;->m:Le2/n;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Le2/n;->c(I)Z

    :cond_8
    return-void

    :cond_9
    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ne v1, v2, :cond_a

    const/4 v3, 0x0

    :cond_a
    invoke-virtual {v1}, Lh0/f2;->j()Lh0/f2;

    move-result-object v1

    goto/16 :goto_0

    :cond_b
    :goto_5
    return-void
.end method

.method private r1(F)V
    .locals 5

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lh0/f2;->o()Lc2/d0;

    move-result-object v1

    iget-object v1, v1, Lc2/d0;->c:[Lc2/t;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    if-eqz v4, :cond_0

    invoke-interface {v4, p1}, Lc2/t;->t(F)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lh0/f2;->j()Lh0/f2;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private s()V
    .locals 1

    iget-object v0, p0, Lh0/n1;->f:[Lh0/p3;

    array-length v0, v0

    new-array v0, v0, [Z

    invoke-direct {p0, v0}, Lh0/n1;->t([Z)V

    return-void
.end method

.method private s0(ZZZZ)V
    .locals 28

    move-object/from16 v1, p0

    iget-object v0, v1, Lh0/n1;->m:Le2/n;

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Le2/n;->f(I)V

    const/4 v2, 0x0

    iput-object v2, v1, Lh0/n1;->T:Lh0/q;

    const/4 v3, 0x0

    iput-boolean v3, v1, Lh0/n1;->H:Z

    iget-object v0, v1, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v0}, Lh0/l;->f()V

    const-wide v4, 0xe8d4a51000L

    iput-wide v4, v1, Lh0/n1;->Q:J

    iget-object v4, v1, Lh0/n1;->f:[Lh0/p3;

    array-length v5, v4

    const/4 v6, 0x0

    :goto_0
    const-string v7, "ExoPlayerImplInternal"

    if-ge v6, v5, :cond_0

    aget-object v0, v4, v6

    :try_start_0
    invoke-direct {v1, v0}, Lh0/n1;->p(Lh0/p3;)V
    :try_end_0
    .catch Lh0/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    :goto_1
    const-string v8, "Disable failed."

    invoke-static {v7, v8, v0}, Le2/r;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_2

    iget-object v4, v1, Lh0/n1;->f:[Lh0/p3;

    array-length v5, v4

    const/4 v6, 0x0

    :goto_3
    if-ge v6, v5, :cond_2

    aget-object v0, v4, v6

    iget-object v8, v1, Lh0/n1;->g:Ljava/util/Set;

    invoke-interface {v8, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    :try_start_1
    invoke-interface {v0}, Lh0/p3;->c()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v8, v0

    const-string v0, "Reset failed."

    invoke-static {v7, v0, v8}, Le2/r;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_2
    iput v3, v1, Lh0/n1;->O:I

    iget-object v0, v1, Lh0/n1;->C:Lh0/d3;

    iget-object v4, v0, Lh0/d3;->b:Lj1/x$b;

    iget-wide v5, v0, Lh0/d3;->r:J

    iget-object v0, v1, Lh0/n1;->C:Lh0/d3;

    iget-object v0, v0, Lh0/d3;->b:Lj1/x$b;

    invoke-virtual {v0}, Lj1/v;->b()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v1, Lh0/n1;->C:Lh0/d3;

    iget-object v7, v1, Lh0/n1;->q:Lh0/c4$b;

    invoke-static {v0, v7}, Lh0/n1;->U(Lh0/d3;Lh0/c4$b;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_5

    :cond_3
    iget-object v0, v1, Lh0/n1;->C:Lh0/d3;

    iget-wide v7, v0, Lh0/d3;->r:J

    goto :goto_6

    :cond_4
    :goto_5
    iget-object v0, v1, Lh0/n1;->C:Lh0/d3;

    iget-wide v7, v0, Lh0/d3;->c:J

    :goto_6
    if-eqz p2, :cond_5

    iput-object v2, v1, Lh0/n1;->P:Lh0/n1$h;

    iget-object v0, v1, Lh0/n1;->C:Lh0/d3;

    iget-object v0, v0, Lh0/d3;->a:Lh0/c4;

    invoke-direct {v1, v0}, Lh0/n1;->C(Lh0/c4;)Landroid/util/Pair;

    move-result-object v0

    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lj1/x$b;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    iget-object v0, v1, Lh0/n1;->C:Lh0/d3;

    iget-object v0, v0, Lh0/d3;->b:Lj1/x$b;

    invoke-virtual {v4, v0}, Lj1/v;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    move-object/from16 v17, v4

    move-wide/from16 v25, v5

    goto :goto_7

    :cond_5
    move-object/from16 v17, v4

    move-wide/from16 v25, v5

    const/4 v0, 0x0

    :goto_7
    iget-object v4, v1, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v4}, Lh0/i2;->f()V

    iput-boolean v3, v1, Lh0/n1;->I:Z

    new-instance v3, Lh0/d3;

    iget-object v4, v1, Lh0/n1;->C:Lh0/d3;

    iget-object v5, v4, Lh0/d3;->a:Lh0/c4;

    iget v11, v4, Lh0/d3;->e:I

    if-eqz p4, :cond_6

    goto :goto_8

    :cond_6
    iget-object v2, v4, Lh0/d3;->f:Lh0/q;

    :goto_8
    move-object v12, v2

    const/4 v13, 0x0

    if-eqz v0, :cond_7

    sget-object v2, Lj1/z0;->i:Lj1/z0;

    goto :goto_9

    :cond_7
    iget-object v2, v4, Lh0/d3;->h:Lj1/z0;

    :goto_9
    move-object v14, v2

    if-eqz v0, :cond_8

    iget-object v2, v1, Lh0/n1;->j:Lc2/d0;

    goto :goto_a

    :cond_8
    iget-object v2, v4, Lh0/d3;->i:Lc2/d0;

    :goto_a
    move-object v15, v2

    if-eqz v0, :cond_9

    invoke-static {}, Lp2/q;->q()Lp2/q;

    move-result-object v0

    goto :goto_b

    :cond_9
    iget-object v0, v4, Lh0/d3;->j:Ljava/util/List;

    :goto_b
    move-object/from16 v16, v0

    iget-object v0, v1, Lh0/n1;->C:Lh0/d3;

    iget-boolean v2, v0, Lh0/d3;->l:Z

    move/from16 v18, v2

    iget v2, v0, Lh0/d3;->m:I

    move/from16 v19, v2

    iget-object v0, v0, Lh0/d3;->n:Lh0/f3;

    move-object/from16 v20, v0

    const-wide/16 v23, 0x0

    const/16 v27, 0x0

    move-object v4, v3

    move-object/from16 v6, v17

    move-wide/from16 v9, v25

    move-wide/from16 v21, v25

    invoke-direct/range {v4 .. v27}, Lh0/d3;-><init>(Lh0/c4;Lj1/x$b;JJILh0/q;ZLj1/z0;Lc2/d0;Ljava/util/List;Lj1/x$b;ZILh0/f3;JJJZ)V

    iput-object v3, v1, Lh0/n1;->C:Lh0/d3;

    if-eqz p3, :cond_a

    iget-object v0, v1, Lh0/n1;->y:Lh0/x2;

    invoke-virtual {v0}, Lh0/x2;->y()V

    :cond_a
    return-void
.end method

.method private declared-synchronized s1(Lo2/p;J)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lo2/p<",
            "Ljava/lang/Boolean;",
            ">;J)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lh0/n1;->v:Le2/d;

    invoke-interface {v0}, Le2/d;->d()J

    move-result-wide v0

    add-long/2addr v0, p2

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p1}, Lo2/p;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    const-wide/16 v3, 0x0

    cmp-long v5, p2, v3

    if-lez v5, :cond_0

    :try_start_1
    iget-object v3, p0, Lh0/n1;->v:Le2/d;

    invoke-interface {v3}, Le2/d;->c()V

    invoke-virtual {p0, p2, p3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    const/4 p2, 0x1

    const/4 v2, 0x1

    :goto_1
    :try_start_2
    iget-object p2, p0, Lh0/n1;->v:Le2/d;

    invoke-interface {p2}, Le2/d;->d()J

    move-result-wide p2

    sub-long p2, v0, p2

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private t([Z)V
    .locals 6

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->q()Lh0/f2;

    move-result-object v0

    invoke-virtual {v0}, Lh0/f2;->o()Lc2/d0;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    iget-object v4, p0, Lh0/n1;->f:[Lh0/p3;

    array-length v4, v4

    if-ge v3, v4, :cond_1

    invoke-virtual {v1, v3}, Lc2/d0;->c(I)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lh0/n1;->g:Ljava/util/Set;

    iget-object v5, p0, Lh0/n1;->f:[Lh0/p3;

    aget-object v5, v5, v3

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lh0/n1;->f:[Lh0/p3;

    aget-object v4, v4, v3

    invoke-interface {v4}, Lh0/p3;->c()V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    iget-object v3, p0, Lh0/n1;->f:[Lh0/p3;

    array-length v3, v3

    if-ge v2, v3, :cond_3

    invoke-virtual {v1, v2}, Lc2/d0;->c(I)Z

    move-result v3

    if-eqz v3, :cond_2

    aget-boolean v3, p1, v2

    invoke-direct {p0, v2, v3}, Lh0/n1;->r(IZ)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    const/4 p1, 0x1

    iput-boolean p1, v0, Lh0/f2;->g:Z

    return-void
.end method

.method private t0()V
    .locals 1

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lh0/f2;->f:Lh0/g2;

    iget-boolean v0, v0, Lh0/g2;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lh0/n1;->F:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lh0/n1;->G:Z

    return-void
.end method

.method private u(Lh0/p3;)V
    .locals 2

    invoke-interface {p1}, Lh0/p3;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Lh0/p3;->b()V

    :cond_0
    return-void
.end method

.method private u0(J)V
    .locals 4

    iget-object v0, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v0}, Lh0/i2;->p()Lh0/f2;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide v0, 0xe8d4a51000L

    add-long/2addr p1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1, p2}, Lh0/f2;->z(J)J

    move-result-wide p1

    :goto_0
    iput-wide p1, p0, Lh0/n1;->Q:J

    iget-object v0, p0, Lh0/n1;->t:Lh0/l;

    invoke-virtual {v0, p1, p2}, Lh0/l;->c(J)V

    iget-object p1, p0, Lh0/n1;->f:[Lh0/p3;

    array-length p2, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p2, :cond_2

    aget-object v1, p1, v0

    invoke-static {v1}, Lh0/n1;->S(Lh0/p3;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lh0/n1;->Q:J

    invoke-interface {v1, v2, v3}, Lh0/p3;->u(J)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lh0/n1;->g0()V

    return-void
.end method

.method private static v0(Lh0/c4;Lh0/n1$d;Lh0/c4$d;Lh0/c4$b;)V
    .locals 4

    iget-object v0, p1, Lh0/n1$d;->i:Ljava/lang/Object;

    invoke-virtual {p0, v0, p3}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v0

    iget v0, v0, Lh0/c4$b;->h:I

    invoke-virtual {p0, v0, p2}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    move-result-object p2

    iget p2, p2, Lh0/c4$d;->u:I

    const/4 v0, 0x1

    invoke-virtual {p0, p2, p3, v0}, Lh0/c4;->k(ILh0/c4$b;Z)Lh0/c4$b;

    move-result-object p0

    iget-object p0, p0, Lh0/c4$b;->g:Ljava/lang/Object;

    iget-wide v0, p3, Lh0/c4$b;->i:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long p3, v0, v2

    if-eqz p3, :cond_0

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    goto :goto_0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    invoke-virtual {p1, p2, v0, v1, p0}, Lh0/n1$d;->b(IJLjava/lang/Object;)V

    return-void
.end method

.method private static w0(Lh0/n1$d;Lh0/c4;Lh0/c4;IZLh0/c4$d;Lh0/c4$b;)Z
    .locals 15

    move-object v0, p0

    move-object/from16 v8, p1

    move-object/from16 v1, p2

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    iget-object v2, v0, Lh0/n1$d;->i:Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x1

    const-wide/high16 v13, -0x8000000000000000L

    if-nez v2, :cond_3

    iget-object v1, v0, Lh0/n1$d;->f:Lh0/k3;

    invoke-virtual {v1}, Lh0/k3;->f()J

    move-result-wide v1

    cmp-long v3, v1, v13

    if-nez v3, :cond_0

    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lh0/n1$d;->f:Lh0/k3;

    invoke-virtual {v1}, Lh0/k3;->f()J

    move-result-wide v1

    invoke-static {v1, v2}, Le2/n0;->C0(J)J

    move-result-wide v1

    :goto_0
    new-instance v3, Lh0/n1$h;

    iget-object v4, v0, Lh0/n1$d;->f:Lh0/k3;

    invoke-virtual {v4}, Lh0/k3;->h()Lh0/c4;

    move-result-object v4

    iget-object v5, v0, Lh0/n1$d;->f:Lh0/k3;

    invoke-virtual {v5}, Lh0/k3;->d()I

    move-result v5

    invoke-direct {v3, v4, v5, v1, v2}, Lh0/n1$h;-><init>(Lh0/c4;IJ)V

    const/4 v4, 0x0

    move-object/from16 v1, p1

    move-object v2, v3

    move v3, v4

    move/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-static/range {v1 .. v7}, Lh0/n1;->z0(Lh0/c4;Lh0/n1$h;ZIZLh0/c4$d;Lh0/c4$b;)Landroid/util/Pair;

    move-result-object v1

    if-nez v1, :cond_1

    return v11

    :cond_1
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v8, v2}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v4, v1}, Lh0/n1$d;->b(IJLjava/lang/Object;)V

    iget-object v1, v0, Lh0/n1$d;->f:Lh0/k3;

    invoke-virtual {v1}, Lh0/k3;->f()J

    move-result-wide v1

    cmp-long v3, v1, v13

    if-nez v3, :cond_2

    invoke-static {v8, p0, v9, v10}, Lh0/n1;->v0(Lh0/c4;Lh0/n1$d;Lh0/c4$d;Lh0/c4$b;)V

    :cond_2
    return v12

    :cond_3
    invoke-virtual {v8, v2}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_4

    return v11

    :cond_4
    iget-object v3, v0, Lh0/n1$d;->f:Lh0/k3;

    invoke-virtual {v3}, Lh0/k3;->f()J

    move-result-wide v3

    cmp-long v5, v3, v13

    if-nez v5, :cond_5

    invoke-static {v8, p0, v9, v10}, Lh0/n1;->v0(Lh0/c4;Lh0/n1$d;Lh0/c4$d;Lh0/c4$b;)V

    return v12

    :cond_5
    iput v2, v0, Lh0/n1$d;->g:I

    iget-object v2, v0, Lh0/n1$d;->i:Ljava/lang/Object;

    invoke-virtual {v1, v2, v10}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    iget-boolean v2, v10, Lh0/c4$b;->k:Z

    if-eqz v2, :cond_6

    iget v2, v10, Lh0/c4$b;->h:I

    invoke-virtual {v1, v2, v9}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    move-result-object v2

    iget v2, v2, Lh0/c4$d;->t:I

    iget-object v3, v0, Lh0/n1$d;->i:Ljava/lang/Object;

    invoke-virtual {v1, v3}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result v1

    if-ne v2, v1, :cond_6

    iget-wide v1, v0, Lh0/n1$d;->h:J

    invoke-virtual/range {p6 .. p6}, Lh0/c4$b;->q()J

    move-result-wide v3

    add-long v5, v1, v3

    iget-object v1, v0, Lh0/n1$d;->i:Ljava/lang/Object;

    invoke-virtual {v8, v1, v10}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v1

    iget v4, v1, Lh0/c4$b;->h:I

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    invoke-virtual/range {v1 .. v6}, Lh0/c4;->n(Lh0/c4$d;Lh0/c4$b;IJ)Landroid/util/Pair;

    move-result-object v1

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v8, v2}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v4, v1}, Lh0/n1$d;->b(IJLjava/lang/Object;)V

    :cond_6
    return v12
.end method

.method private x([Lc2/t;)Lp2/q;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lc2/t;",
            ")",
            "Lp2/q<",
            "Lz0/a;",
            ">;"
        }
    .end annotation

    new-instance v0, Lp2/q$a;

    invoke-direct {v0}, Lp2/q$a;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v5, p1, v3

    if-eqz v5, :cond_1

    invoke-interface {v5, v2}, Lc2/w;->a(I)Lh0/r1;

    move-result-object v5

    iget-object v5, v5, Lh0/r1;->o:Lz0/a;

    if-nez v5, :cond_0

    new-instance v5, Lz0/a;

    new-array v6, v2, [Lz0/a$b;

    invoke-direct {v5, v6}, Lz0/a;-><init>([Lz0/a$b;)V

    invoke-virtual {v0, v5}, Lp2/q$a;->f(Ljava/lang/Object;)Lp2/q$a;

    goto :goto_1

    :cond_0
    invoke-virtual {v0, v5}, Lp2/q$a;->f(Ljava/lang/Object;)Lp2/q$a;

    const/4 v4, 0x1

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lp2/q$a;->h()Lp2/q;

    move-result-object p1

    goto :goto_2

    :cond_3
    invoke-static {}, Lp2/q;->q()Lp2/q;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method private x0(Lh0/c4;Lh0/c4;)V
    .locals 9

    invoke-virtual {p1}, Lh0/c4;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lh0/c4;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    iget-object v1, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lh0/n1$d;

    iget v5, p0, Lh0/n1;->J:I

    iget-boolean v6, p0, Lh0/n1;->K:Z

    iget-object v7, p0, Lh0/n1;->p:Lh0/c4$d;

    iget-object v8, p0, Lh0/n1;->q:Lh0/c4$b;

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v2 .. v8}, Lh0/n1;->w0(Lh0/n1$d;Lh0/c4;Lh0/c4;IZLh0/c4$d;Lh0/c4$b;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lh0/n1$d;

    iget-object v1, v1, Lh0/n1$d;->f:Lh0/k3;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lh0/k3;->k(Z)V

    iget-object v1, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lh0/n1;->u:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-void
.end method

.method private y()J
    .locals 5

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    iget-object v1, v0, Lh0/d3;->a:Lh0/c4;

    iget-object v2, v0, Lh0/d3;->b:Lj1/x$b;

    iget-object v2, v2, Lj1/v;->a:Ljava/lang/Object;

    iget-wide v3, v0, Lh0/d3;->r:J

    invoke-direct {p0, v1, v2, v3, v4}, Lh0/n1;->A(Lh0/c4;Ljava/lang/Object;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static y0(Lh0/c4;Lh0/d3;Lh0/n1$h;Lh0/i2;IZLh0/c4$d;Lh0/c4$b;)Lh0/n1$g;
    .locals 30

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move/from16 v10, p5

    move-object/from16 v11, p7

    invoke-virtual/range {p0 .. p0}, Lh0/c4;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lh0/n1$g;

    invoke-static {}, Lh0/d3;->k()Lj1/x$b;

    move-result-object v2

    const-wide/16 v3, 0x0

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lh0/n1$g;-><init>(Lj1/x$b;JJZZZ)V

    return-object v0

    :cond_0
    iget-object v14, v8, Lh0/d3;->b:Lj1/x$b;

    iget-object v12, v14, Lj1/v;->a:Ljava/lang/Object;

    invoke-static {v8, v11}, Lh0/n1;->U(Lh0/d3;Lh0/c4$b;)Z

    move-result v13

    iget-object v0, v8, Lh0/d3;->b:Lj1/x$b;

    invoke-virtual {v0}, Lj1/v;->b()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v13, :cond_1

    goto :goto_0

    :cond_1
    iget-wide v0, v8, Lh0/d3;->r:J

    goto :goto_1

    :cond_2
    :goto_0
    iget-wide v0, v8, Lh0/d3;->c:J

    :goto_1
    move-wide v15, v0

    const-wide v17, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v6, -0x1

    const/16 v19, 0x0

    const/16 v20, 0x1

    if-eqz v9, :cond_6

    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v3, p4

    move/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v21, v14

    const/4 v14, -0x1

    move-object/from16 v6, p7

    invoke-static/range {v0 .. v6}, Lh0/n1;->z0(Lh0/c4;Lh0/n1$h;ZIZLh0/c4$d;Lh0/c4$b;)Landroid/util/Pair;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-virtual {v7, v10}, Lh0/c4;->e(Z)I

    move-result v0

    move v6, v0

    move-wide v0, v15

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    goto :goto_4

    :cond_3
    iget-wide v1, v9, Lh0/n1$h;->c:J

    cmp-long v3, v1, v17

    if-nez v3, :cond_4

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v7, v0, v11}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v0

    iget v6, v0, Lh0/c4$b;->h:I

    move-wide v0, v15

    const/4 v2, 0x0

    goto :goto_2

    :cond_4
    iget-object v12, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const/4 v2, 0x1

    const/4 v6, -0x1

    :goto_2
    iget v3, v8, Lh0/d3;->e:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_5

    const/4 v3, 0x1

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    :goto_3
    const/4 v4, 0x0

    :goto_4
    move-object/from16 v9, p6

    move/from16 v29, v2

    move/from16 v27, v3

    move/from16 v28, v4

    move v3, v6

    move-object/from16 v6, v21

    goto/16 :goto_a

    :cond_6
    move-object/from16 v21, v14

    const/4 v14, -0x1

    iget-object v0, v8, Lh0/d3;->a:Lh0/c4;

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v7, v10}, Lh0/c4;->e(Z)I

    move-result v0

    :goto_5
    move-object/from16 v9, p6

    move v3, v0

    move-wide v0, v15

    move-object/from16 v6, v21

    :goto_6
    const/16 v27, 0x0

    const/16 v28, 0x0

    :goto_7
    const/16 v29, 0x0

    goto/16 :goto_a

    :cond_7
    invoke-virtual {v7, v12}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result v0

    if-ne v0, v14, :cond_9

    iget-object v5, v8, Lh0/d3;->a:Lh0/c4;

    move-object/from16 v0, p6

    move-object/from16 v1, p7

    move/from16 v2, p4

    move/from16 v3, p5

    move-object v4, v12

    move-object/from16 v6, p0

    invoke-static/range {v0 .. v6}, Lh0/n1;->A0(Lh0/c4$d;Lh0/c4$b;IZLjava/lang/Object;Lh0/c4;Lh0/c4;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_8

    invoke-virtual {v7, v10}, Lh0/c4;->e(Z)I

    move-result v0

    const/4 v4, 0x1

    goto :goto_8

    :cond_8
    invoke-virtual {v7, v0, v11}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v0

    iget v0, v0, Lh0/c4$b;->h:I

    const/4 v4, 0x0

    :goto_8
    move-object/from16 v9, p6

    move v3, v0

    move/from16 v28, v4

    move-wide v0, v15

    move-object/from16 v6, v21

    const/16 v27, 0x0

    goto :goto_7

    :cond_9
    cmp-long v0, v15, v17

    if-nez v0, :cond_a

    invoke-virtual {v7, v12, v11}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v0

    iget v0, v0, Lh0/c4$b;->h:I

    goto :goto_5

    :cond_a
    if-eqz v13, :cond_c

    iget-object v0, v8, Lh0/d3;->a:Lh0/c4;

    move-object/from16 v6, v21

    iget-object v1, v6, Lj1/v;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v11}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    iget-object v0, v8, Lh0/d3;->a:Lh0/c4;

    iget v1, v11, Lh0/c4$b;->h:I

    move-object/from16 v9, p6

    invoke-virtual {v0, v1, v9}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    move-result-object v0

    iget v0, v0, Lh0/c4$d;->t:I

    iget-object v1, v8, Lh0/d3;->a:Lh0/c4;

    iget-object v2, v6, Lj1/v;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result v1

    if-ne v0, v1, :cond_b

    invoke-virtual/range {p7 .. p7}, Lh0/c4$b;->q()J

    move-result-wide v0

    add-long v4, v15, v0

    invoke-virtual {v7, v12, v11}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v0

    iget v3, v0, Lh0/c4$b;->h:I

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p7

    invoke-virtual/range {v0 .. v5}, Lh0/c4;->n(Lh0/c4$d;Lh0/c4$b;IJ)Landroid/util/Pair;

    move-result-object v0

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object v12, v1

    move-wide v0, v2

    goto :goto_9

    :cond_b
    move-wide v0, v15

    :goto_9
    const/4 v3, -0x1

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x1

    goto :goto_a

    :cond_c
    move-object/from16 v9, p6

    move-object/from16 v6, v21

    move-wide v0, v15

    const/4 v3, -0x1

    goto/16 :goto_6

    :goto_a
    if-eq v3, v14, :cond_d

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p7

    invoke-virtual/range {v0 .. v5}, Lh0/c4;->n(Lh0/c4$d;Lh0/c4$b;IJ)Landroid/util/Pair;

    move-result-object v0

    iget-object v12, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    move-object/from16 v2, p3

    move-wide/from16 v25, v17

    goto :goto_b

    :cond_d
    move-object/from16 v2, p3

    move-wide/from16 v25, v0

    :goto_b
    invoke-virtual {v2, v7, v12, v0, v1}, Lh0/i2;->B(Lh0/c4;Ljava/lang/Object;J)Lj1/x$b;

    move-result-object v2

    iget v3, v2, Lj1/v;->e:I

    if-eq v3, v14, :cond_f

    iget v4, v6, Lj1/v;->e:I

    if-eq v4, v14, :cond_e

    if-lt v3, v4, :cond_e

    goto :goto_c

    :cond_e
    const/4 v3, 0x0

    goto :goto_d

    :cond_f
    :goto_c
    const/4 v3, 0x1

    :goto_d
    iget-object v4, v6, Lj1/v;->a:Ljava/lang/Object;

    invoke-virtual {v4, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-virtual {v6}, Lj1/v;->b()Z

    move-result v4

    if-nez v4, :cond_10

    invoke-virtual {v2}, Lj1/v;->b()Z

    move-result v4

    if-nez v4, :cond_10

    if-eqz v3, :cond_10

    goto :goto_e

    :cond_10
    const/16 v20, 0x0

    :goto_e
    invoke-virtual {v7, v12, v11}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v17

    move v12, v13

    move-object v13, v6

    move-object v3, v6

    move-wide v14, v15

    move-object/from16 v16, v2

    move-wide/from16 v18, v25

    invoke-static/range {v12 .. v19}, Lh0/n1;->Q(ZLj1/x$b;JLj1/x$b;Lh0/c4$b;J)Z

    move-result v4

    if-nez v20, :cond_11

    if-eqz v4, :cond_12

    :cond_11
    move-object v2, v3

    :cond_12
    invoke-virtual {v2}, Lj1/v;->b()Z

    move-result v4

    if-eqz v4, :cond_15

    invoke-virtual {v2, v3}, Lj1/v;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-wide v0, v8, Lh0/d3;->r:J

    goto :goto_f

    :cond_13
    iget-object v0, v2, Lj1/v;->a:Ljava/lang/Object;

    invoke-virtual {v7, v0, v11}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    iget v0, v2, Lj1/v;->c:I

    iget v1, v2, Lj1/v;->b:I

    invoke-virtual {v11, v1}, Lh0/c4$b;->n(I)I

    move-result v1

    if-ne v0, v1, :cond_14

    invoke-virtual/range {p7 .. p7}, Lh0/c4$b;->j()J

    move-result-wide v0

    goto :goto_f

    :cond_14
    const-wide/16 v0, 0x0

    :cond_15
    :goto_f
    move-wide/from16 v23, v0

    new-instance v0, Lh0/n1$g;

    move-object/from16 v21, v0

    move-object/from16 v22, v2

    invoke-direct/range {v21 .. v29}, Lh0/n1$g;-><init>(Lj1/x$b;JJZZZ)V

    return-object v0
.end method

.method private static z(Lc2/t;)[Lh0/r1;
    .locals 4

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lc2/w;->length()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    new-array v2, v1, [Lh0/r1;

    :goto_1
    if-ge v0, v1, :cond_1

    invoke-interface {p0, v0}, Lc2/w;->a(I)Lh0/r1;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-object v2
.end method

.method private static z0(Lh0/c4;Lh0/n1$h;ZIZLh0/c4$d;Lh0/c4$b;)Landroid/util/Pair;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lh0/c4;",
            "Lh0/n1$h;",
            "ZIZ",
            "Lh0/c4$d;",
            "Lh0/c4$b;",
            ")",
            "Landroid/util/Pair<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    move-object v7, p0

    move-object v0, p1

    move-object/from16 v8, p6

    iget-object v1, v0, Lh0/n1$h;->a:Lh0/c4;

    invoke-virtual {p0}, Lh0/c4;->u()Z

    move-result v2

    const/4 v9, 0x0

    if-eqz v2, :cond_0

    return-object v9

    :cond_0
    invoke-virtual {v1}, Lh0/c4;->u()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v10, v7

    goto :goto_0

    :cond_1
    move-object v10, v1

    :goto_0
    :try_start_0
    iget v4, v0, Lh0/n1$h;->b:I

    iget-wide v5, v0, Lh0/n1$h;->c:J

    move-object v1, v10

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    invoke-virtual/range {v1 .. v6}, Lh0/c4;->n(Lh0/c4$d;Lh0/c4$b;IJ)Landroid/util/Pair;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p0, v10}, Lh0/c4;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    return-object v1

    :cond_2
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {p0, v2}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v10, v2, v8}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v2

    iget-boolean v2, v2, Lh0/c4$b;->k:Z

    if-eqz v2, :cond_3

    iget v2, v8, Lh0/c4$b;->h:I

    move-object/from16 v11, p5

    invoke-virtual {v10, v2, v11}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    move-result-object v2

    iget v2, v2, Lh0/c4$d;->t:I

    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v10, v3}, Lh0/c4;->f(Ljava/lang/Object;)I

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {p0, v1, v8}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v1

    iget v3, v1, Lh0/c4$b;->h:I

    iget-wide v4, v0, Lh0/n1$h;->c:J

    move-object v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-virtual/range {v0 .. v5}, Lh0/c4;->n(Lh0/c4$d;Lh0/c4$b;IJ)Landroid/util/Pair;

    move-result-object v1

    :cond_3
    return-object v1

    :cond_4
    move-object/from16 v11, p5

    if-eqz p2, :cond_5

    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    move v2, p3

    move/from16 v3, p4

    move-object v5, v10

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lh0/n1;->A0(Lh0/c4$d;Lh0/c4$b;IZLjava/lang/Object;Lh0/c4;Lh0/c4;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, v0, v8}, Lh0/c4;->l(Ljava/lang/Object;Lh0/c4$b;)Lh0/c4$b;

    move-result-object v0

    iget v3, v0, Lh0/c4$b;->h:I

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    move-object v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-virtual/range {v0 .. v5}, Lh0/c4;->n(Lh0/c4$d;Lh0/c4$b;IJ)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :catch_0
    :cond_5
    return-object v9
.end method


# virtual methods
.method public C0(Lh0/c4;IJ)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    new-instance v1, Lh0/n1$h;

    invoke-direct {v1, p1, p2, p3, p4}, Lh0/n1$h;-><init>(Lh0/c4;IJ)V

    const/4 p1, 0x3

    invoke-interface {v0, p1, v1}, Le2/n;->h(ILjava/lang/Object;)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V

    return-void
.end method

.method public D()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lh0/n1;->o:Landroid/os/Looper;

    return-object v0
.end method

.method public P0(Ljava/util/List;IJLj1/s0;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lh0/x2$c;",
            ">;IJ",
            "Lj1/s0;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    new-instance v8, Lh0/n1$b;

    const/4 v7, 0x0

    move-object v1, v8

    move-object v2, p1

    move-object v3, p5

    move v4, p2

    move-wide v5, p3

    invoke-direct/range {v1 .. v7}, Lh0/n1$b;-><init>(Ljava/util/List;Lj1/s0;IJLh0/n1$a;)V

    const/16 p1, 0x11

    invoke-interface {v0, p1, v8}, Le2/n;->h(ILjava/lang/Object;)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V

    return-void
.end method

.method public S0(ZI)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/4 v1, 0x1

    invoke-interface {v0, v1, p1, p2}, Le2/n;->b(III)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V

    return-void
.end method

.method public U0(Lh0/f3;)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/4 v1, 0x4

    invoke-interface {v0, v1, p1}, Le2/n;->h(ILjava/lang/Object;)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V

    return-void
.end method

.method public W0(I)V
    .locals 3

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Le2/n;->b(III)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V

    return-void
.end method

.method public Z0(Z)V
    .locals 3

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/4 v1, 0x0

    const/16 v2, 0xc

    invoke-interface {v0, v2, p1, v1}, Le2/n;->b(III)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V

    return-void
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/16 v1, 0x16

    invoke-interface {v0, v1}, Le2/n;->c(I)Z

    return-void
.end method

.method public declared-synchronized c(Lh0/k3;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lh0/n1;->E:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lh0/n1;->o:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/16 v1, 0xe

    invoke-interface {v0, v1, p1}, Le2/n;->h(ILjava/lang/Object;)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :goto_0
    :try_start_1
    const-string v0, "ExoPlayerImplInternal"

    const-string v1, "Ignoring messages sent after release."

    invoke-static {v0, v1}, Le2/r;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lh0/k3;->k(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Le2/n;->c(I)Z

    return-void
.end method

.method public e(Lj1/u;)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/16 v1, 0x8

    invoke-interface {v0, v1, p1}, Le2/n;->h(ILjava/lang/Object;)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V

    return-void
.end method

.method public h(Lh0/f3;)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/16 v1, 0x10

    invoke-interface {v0, v1, p1}, Le2/n;->h(ILjava/lang/Object;)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 7

    const-string v0, "Playback error"

    const-string v1, "ExoPlayerImplInternal"

    const/16 v2, 0x3e8

    const/4 v3, 0x0

    const/4 v4, 0x1

    :try_start_0
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    return v3

    :pswitch_0
    invoke-direct {p0}, Lh0/n1;->n()V

    goto/16 :goto_8

    :pswitch_1
    iget p1, p1, Landroid/os/Message;->arg1:I

    if-ne p1, v4, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0, p1}, Lh0/n1;->Q0(Z)V

    goto/16 :goto_8

    :pswitch_2
    iget p1, p1, Landroid/os/Message;->arg1:I

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    invoke-direct {p0, p1}, Lh0/n1;->R0(Z)V

    goto/16 :goto_8

    :pswitch_3
    invoke-direct {p0}, Lh0/n1;->e0()V

    goto/16 :goto_8

    :pswitch_4
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lj1/s0;

    invoke-direct {p0, p1}, Lh0/n1;->b1(Lj1/s0;)V

    goto/16 :goto_8

    :pswitch_5
    iget v5, p1, Landroid/os/Message;->arg1:I

    iget v6, p1, Landroid/os/Message;->arg2:I

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lj1/s0;

    invoke-direct {p0, v5, v6, p1}, Lh0/n1;->o0(IILj1/s0;)V

    goto/16 :goto_8

    :pswitch_6
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lh0/n1$c;

    invoke-direct {p0, p1}, Lh0/n1;->f0(Lh0/n1$c;)V

    goto/16 :goto_8

    :pswitch_7
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lh0/n1$b;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v5, p1}, Lh0/n1;->m(Lh0/n1$b;I)V

    goto/16 :goto_8

    :pswitch_8
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lh0/n1$b;

    invoke-direct {p0, p1}, Lh0/n1;->O0(Lh0/n1$b;)V

    goto/16 :goto_8

    :pswitch_9
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lh0/f3;

    invoke-direct {p0, p1, v3}, Lh0/n1;->M(Lh0/f3;Z)V

    goto/16 :goto_8

    :pswitch_a
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lh0/k3;

    invoke-direct {p0, p1}, Lh0/n1;->J0(Lh0/k3;)V

    goto/16 :goto_8

    :pswitch_b
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lh0/k3;

    invoke-direct {p0, p1}, Lh0/n1;->H0(Lh0/k3;)V

    goto/16 :goto_8

    :pswitch_c
    iget v5, p1, Landroid/os/Message;->arg1:I

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :cond_2
    const/4 v5, 0x0

    :goto_2
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p0, v5, p1}, Lh0/n1;->M0(ZLjava/util/concurrent/atomic/AtomicBoolean;)V

    goto/16 :goto_8

    :pswitch_d
    iget p1, p1, Landroid/os/Message;->arg1:I

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    goto :goto_3

    :cond_3
    const/4 p1, 0x0

    :goto_3
    invoke-direct {p0, p1}, Lh0/n1;->a1(Z)V

    goto/16 :goto_8

    :pswitch_e
    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, p1}, Lh0/n1;->X0(I)V

    goto/16 :goto_8

    :pswitch_f
    invoke-direct {p0}, Lh0/n1;->r0()V

    goto/16 :goto_8

    :pswitch_10
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lj1/u;

    invoke-direct {p0, p1}, Lh0/n1;->G(Lj1/u;)V

    goto/16 :goto_8

    :pswitch_11
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lj1/u;

    invoke-direct {p0, p1}, Lh0/n1;->K(Lj1/u;)V

    goto/16 :goto_8

    :pswitch_12
    invoke-direct {p0}, Lh0/n1;->n0()V

    return v4

    :pswitch_13
    invoke-direct {p0, v3, v4}, Lh0/n1;->k1(ZZ)V

    goto/16 :goto_8

    :pswitch_14
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lh0/u3;

    invoke-direct {p0, p1}, Lh0/n1;->Y0(Lh0/u3;)V

    goto/16 :goto_8

    :pswitch_15
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lh0/f3;

    invoke-direct {p0, p1}, Lh0/n1;->V0(Lh0/f3;)V

    goto/16 :goto_8

    :pswitch_16
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lh0/n1$h;

    invoke-direct {p0, p1}, Lh0/n1;->E0(Lh0/n1$h;)V

    goto/16 :goto_8

    :pswitch_17
    invoke-direct {p0}, Lh0/n1;->q()V

    goto/16 :goto_8

    :pswitch_18
    iget v5, p1, Landroid/os/Message;->arg1:I

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    goto :goto_4

    :cond_4
    const/4 v5, 0x0

    :goto_4
    iget p1, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v5, p1, v4, v4}, Lh0/n1;->T0(ZIZI)V

    goto/16 :goto_8

    :pswitch_19
    invoke-direct {p0}, Lh0/n1;->l0()V
    :try_end_0
    .catch Lh0/q; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ll0/o$a; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lh0/y2; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ld2/m; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lj1/b; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_8

    :catch_0
    move-exception p1

    instance-of v5, p1, Ljava/lang/IllegalStateException;

    if-nez v5, :cond_5

    instance-of v5, p1, Ljava/lang/IllegalArgumentException;

    if-eqz v5, :cond_6

    :cond_5
    const/16 v2, 0x3ec

    :cond_6
    invoke-static {p1, v2}, Lh0/q;->i(Ljava/lang/RuntimeException;I)Lh0/q;

    move-result-object p1

    :cond_7
    :goto_5
    invoke-static {v1, v0, p1}, Le2/r;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v4, v3}, Lh0/n1;->k1(ZZ)V

    iget-object v0, p0, Lh0/n1;->C:Lh0/d3;

    invoke-virtual {v0, p1}, Lh0/d3;->e(Lh0/q;)Lh0/d3;

    move-result-object p1

    iput-object p1, p0, Lh0/n1;->C:Lh0/d3;

    goto/16 :goto_8

    :catch_1
    move-exception p1

    const/16 v0, 0x7d0

    goto :goto_7

    :catch_2
    move-exception p1

    const/16 v0, 0x3ea

    goto :goto_7

    :catch_3
    move-exception p1

    iget v0, p1, Ld2/m;->f:I

    goto :goto_7

    :catch_4
    move-exception p1

    iget v0, p1, Lh0/y2;->g:I

    if-ne v0, v4, :cond_9

    iget-boolean v0, p1, Lh0/y2;->f:Z

    if-eqz v0, :cond_8

    const/16 v0, 0xbb9

    const/16 v2, 0xbb9

    goto :goto_6

    :cond_8
    const/16 v0, 0xbbb

    const/16 v2, 0xbbb

    goto :goto_6

    :cond_9
    const/4 v1, 0x4

    if-ne v0, v1, :cond_b

    iget-boolean v0, p1, Lh0/y2;->f:Z

    if-eqz v0, :cond_a

    const/16 v0, 0xbba

    const/16 v2, 0xbba

    goto :goto_6

    :cond_a
    const/16 v0, 0xbbc

    const/16 v2, 0xbbc

    :cond_b
    :goto_6
    invoke-direct {p0, p1, v2}, Lh0/n1;->H(Ljava/io/IOException;I)V

    goto :goto_8

    :catch_5
    move-exception p1

    iget v0, p1, Ll0/o$a;->f:I

    :goto_7
    invoke-direct {p0, p1, v0}, Lh0/n1;->H(Ljava/io/IOException;I)V

    goto :goto_8

    :catch_6
    move-exception p1

    iget v2, p1, Lh0/q;->n:I

    if-ne v2, v4, :cond_c

    iget-object v2, p0, Lh0/n1;->x:Lh0/i2;

    invoke-virtual {v2}, Lh0/i2;->q()Lh0/f2;

    move-result-object v2

    if-eqz v2, :cond_c

    iget-object v2, v2, Lh0/f2;->f:Lh0/g2;

    iget-object v2, v2, Lh0/g2;->a:Lj1/x$b;

    invoke-virtual {p1, v2}, Lh0/q;->e(Lj1/v;)Lh0/q;

    move-result-object p1

    :cond_c
    iget-boolean v2, p1, Lh0/q;->t:Z

    if-eqz v2, :cond_d

    iget-object v2, p0, Lh0/n1;->T:Lh0/q;

    if-nez v2, :cond_d

    const-string v0, "Recoverable renderer error"

    invoke-static {v1, v0, p1}, Le2/r;->j(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object p1, p0, Lh0/n1;->T:Lh0/q;

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/16 v1, 0x19

    invoke-interface {v0, v1, p1}, Le2/n;->h(ILjava/lang/Object;)Le2/n$a;

    move-result-object p1

    invoke-interface {v0, p1}, Le2/n;->g(Le2/n$a;)Z

    goto :goto_8

    :cond_d
    iget-object v2, p0, Lh0/n1;->T:Lh0/q;

    if-eqz v2, :cond_7

    invoke-virtual {v2, p1}, Ljava/lang/Exception;->addSuppressed(Ljava/lang/Throwable;)V

    iget-object p1, p0, Lh0/n1;->T:Lh0/q;

    goto/16 :goto_5

    :goto_8
    invoke-direct {p0}, Lh0/n1;->Y()V

    return v4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic i(Lj1/r0;)V
    .locals 0

    check-cast p1, Lj1/u;

    invoke-virtual {p0, p1}, Lh0/n1;->j0(Lj1/u;)V

    return-void
.end method

.method public j0(Lj1/u;)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/16 v1, 0x9

    invoke-interface {v0, v1, p1}, Le2/n;->h(ILjava/lang/Object;)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V

    return-void
.end method

.method public j1()V
    .locals 2

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Le2/n;->l(I)Le2/n$a;

    move-result-object v0

    invoke-interface {v0}, Le2/n$a;->a()V

    return-void
.end method

.method public k0()V
    .locals 2

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Le2/n;->l(I)Le2/n$a;

    move-result-object v0

    invoke-interface {v0}, Le2/n$a;->a()V

    return-void
.end method

.method public declared-synchronized m0()Z
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lh0/n1;->E:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lh0/n1;->o:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Le2/n;->c(I)Z

    new-instance v0, Lh0/m1;

    invoke-direct {v0, p0}, Lh0/m1;-><init>(Lh0/n1;)V

    iget-wide v1, p0, Lh0/n1;->A:J

    invoke-direct {p0, v0, v1, v2}, Lh0/n1;->s1(Lo2/p;J)V

    iget-boolean v0, p0, Lh0/n1;->E:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public p0(IILj1/s0;)V
    .locals 2

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/16 v1, 0x14

    invoke-interface {v0, v1, p1, p2, p3}, Le2/n;->d(IIILjava/lang/Object;)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V

    return-void
.end method

.method public v(J)V
    .locals 0

    iput-wide p1, p0, Lh0/n1;->U:J

    return-void
.end method

.method public w(Z)V
    .locals 3

    iget-object v0, p0, Lh0/n1;->m:Le2/n;

    const/4 v1, 0x0

    const/16 v2, 0x18

    invoke-interface {v0, v2, p1, v1}, Le2/n;->b(III)Le2/n$a;

    move-result-object p1

    invoke-interface {p1}, Le2/n$a;->a()V

    return-void
.end method
