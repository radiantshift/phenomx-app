.class public final Lh0/e2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh0/e2$b;
    }
.end annotation


# static fields
.field public static final N:Lh0/e2;

.field private static final O:Ljava/lang/String;

.field private static final P:Ljava/lang/String;

.field private static final Q:Ljava/lang/String;

.field private static final R:Ljava/lang/String;

.field private static final S:Ljava/lang/String;

.field private static final T:Ljava/lang/String;

.field private static final U:Ljava/lang/String;

.field private static final V:Ljava/lang/String;

.field private static final W:Ljava/lang/String;

.field private static final X:Ljava/lang/String;

.field private static final Y:Ljava/lang/String;

.field private static final Z:Ljava/lang/String;

.field private static final a0:Ljava/lang/String;

.field private static final b0:Ljava/lang/String;

.field private static final c0:Ljava/lang/String;

.field private static final d0:Ljava/lang/String;

.field private static final e0:Ljava/lang/String;

.field private static final f0:Ljava/lang/String;

.field private static final g0:Ljava/lang/String;

.field private static final h0:Ljava/lang/String;

.field private static final i0:Ljava/lang/String;

.field private static final j0:Ljava/lang/String;

.field private static final k0:Ljava/lang/String;

.field private static final l0:Ljava/lang/String;

.field private static final m0:Ljava/lang/String;

.field private static final n0:Ljava/lang/String;

.field private static final o0:Ljava/lang/String;

.field private static final p0:Ljava/lang/String;

.field private static final q0:Ljava/lang/String;

.field private static final r0:Ljava/lang/String;

.field private static final s0:Ljava/lang/String;

.field private static final t0:Ljava/lang/String;

.field private static final u0:Ljava/lang/String;

.field public static final v0:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Lh0/e2;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Ljava/lang/Integer;

.field public final B:Ljava/lang/Integer;

.field public final C:Ljava/lang/Integer;

.field public final D:Ljava/lang/CharSequence;

.field public final E:Ljava/lang/CharSequence;

.field public final F:Ljava/lang/CharSequence;

.field public final G:Ljava/lang/Integer;

.field public final H:Ljava/lang/Integer;

.field public final I:Ljava/lang/CharSequence;

.field public final J:Ljava/lang/CharSequence;

.field public final K:Ljava/lang/CharSequence;

.field public final L:Ljava/lang/Integer;

.field public final M:Landroid/os/Bundle;

.field public final f:Ljava/lang/CharSequence;

.field public final g:Ljava/lang/CharSequence;

.field public final h:Ljava/lang/CharSequence;

.field public final i:Ljava/lang/CharSequence;

.field public final j:Ljava/lang/CharSequence;

.field public final k:Ljava/lang/CharSequence;

.field public final l:Ljava/lang/CharSequence;

.field public final m:Lh0/n3;

.field public final n:Lh0/n3;

.field public final o:[B

.field public final p:Ljava/lang/Integer;

.field public final q:Landroid/net/Uri;

.field public final r:Ljava/lang/Integer;

.field public final s:Ljava/lang/Integer;

.field public final t:Ljava/lang/Integer;

.field public final u:Ljava/lang/Boolean;

.field public final v:Ljava/lang/Boolean;

.field public final w:Ljava/lang/Integer;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final x:Ljava/lang/Integer;

.field public final y:Ljava/lang/Integer;

.field public final z:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lh0/e2$b;

    invoke-direct {v0}, Lh0/e2$b;-><init>()V

    invoke-virtual {v0}, Lh0/e2$b;->H()Lh0/e2;

    move-result-object v0

    sput-object v0, Lh0/e2;->N:Lh0/e2;

    const/4 v0, 0x0

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->O:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->P:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->Q:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->R:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->S:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->T:Ljava/lang/String;

    const/4 v0, 0x6

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->U:Ljava/lang/String;

    const/16 v0, 0x8

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->V:Ljava/lang/String;

    const/16 v0, 0x9

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->W:Ljava/lang/String;

    const/16 v0, 0xa

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->X:Ljava/lang/String;

    const/16 v0, 0xb

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->Y:Ljava/lang/String;

    const/16 v0, 0xc

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->Z:Ljava/lang/String;

    const/16 v0, 0xd

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->a0:Ljava/lang/String;

    const/16 v0, 0xe

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->b0:Ljava/lang/String;

    const/16 v0, 0xf

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->c0:Ljava/lang/String;

    const/16 v0, 0x10

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->d0:Ljava/lang/String;

    const/16 v0, 0x11

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->e0:Ljava/lang/String;

    const/16 v0, 0x12

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->f0:Ljava/lang/String;

    const/16 v0, 0x13

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->g0:Ljava/lang/String;

    const/16 v0, 0x14

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->h0:Ljava/lang/String;

    const/16 v0, 0x15

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->i0:Ljava/lang/String;

    const/16 v0, 0x16

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->j0:Ljava/lang/String;

    const/16 v0, 0x17

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->k0:Ljava/lang/String;

    const/16 v0, 0x18

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->l0:Ljava/lang/String;

    const/16 v0, 0x19

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->m0:Ljava/lang/String;

    const/16 v0, 0x1a

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->n0:Ljava/lang/String;

    const/16 v0, 0x1b

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->o0:Ljava/lang/String;

    const/16 v0, 0x1c

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->p0:Ljava/lang/String;

    const/16 v0, 0x1d

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->q0:Ljava/lang/String;

    const/16 v0, 0x1e

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->r0:Ljava/lang/String;

    const/16 v0, 0x1f

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->s0:Ljava/lang/String;

    const/16 v0, 0x20

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->t0:Ljava/lang/String;

    const/16 v0, 0x3e8

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/e2;->u0:Ljava/lang/String;

    sget-object v0, Lh0/d2;->a:Lh0/d2;

    sput-object v0, Lh0/e2;->v0:Lh0/h$a;

    return-void
.end method

.method private constructor <init>(Lh0/e2$b;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lh0/e2$b;->a(Lh0/e2$b;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1}, Lh0/e2$b;->l(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Lh0/e2$b;->w(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, -0x1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v4, :cond_5

    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lh0/e2;->d(I)I

    move-result v3

    :cond_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v4, :cond_4

    const/4 v3, 0x1

    :cond_4
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    if-nez v2, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lh0/e2;->e(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :cond_5
    :goto_0
    invoke-static {p1}, Lh0/e2$b;->B(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->f:Ljava/lang/CharSequence;

    invoke-static {p1}, Lh0/e2$b;->C(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->g:Ljava/lang/CharSequence;

    invoke-static {p1}, Lh0/e2$b;->D(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->h:Ljava/lang/CharSequence;

    invoke-static {p1}, Lh0/e2$b;->E(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->i:Ljava/lang/CharSequence;

    invoke-static {p1}, Lh0/e2$b;->F(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->j:Ljava/lang/CharSequence;

    invoke-static {p1}, Lh0/e2$b;->G(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->k:Ljava/lang/CharSequence;

    invoke-static {p1}, Lh0/e2$b;->b(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->l:Ljava/lang/CharSequence;

    invoke-static {p1}, Lh0/e2$b;->c(Lh0/e2$b;)Lh0/n3;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->m:Lh0/n3;

    invoke-static {p1}, Lh0/e2$b;->d(Lh0/e2$b;)Lh0/n3;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->n:Lh0/n3;

    invoke-static {p1}, Lh0/e2$b;->e(Lh0/e2$b;)[B

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->o:[B

    invoke-static {p1}, Lh0/e2$b;->f(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->p:Ljava/lang/Integer;

    invoke-static {p1}, Lh0/e2$b;->g(Lh0/e2$b;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->q:Landroid/net/Uri;

    invoke-static {p1}, Lh0/e2$b;->h(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->r:Ljava/lang/Integer;

    invoke-static {p1}, Lh0/e2$b;->i(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lh0/e2;->s:Ljava/lang/Integer;

    iput-object v1, p0, Lh0/e2;->t:Ljava/lang/Integer;

    iput-object v0, p0, Lh0/e2;->u:Ljava/lang/Boolean;

    invoke-static {p1}, Lh0/e2$b;->j(Lh0/e2$b;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->v:Ljava/lang/Boolean;

    invoke-static {p1}, Lh0/e2$b;->k(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->w:Ljava/lang/Integer;

    invoke-static {p1}, Lh0/e2$b;->k(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->x:Ljava/lang/Integer;

    invoke-static {p1}, Lh0/e2$b;->m(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->y:Ljava/lang/Integer;

    invoke-static {p1}, Lh0/e2$b;->n(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->z:Ljava/lang/Integer;

    invoke-static {p1}, Lh0/e2$b;->o(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->A:Ljava/lang/Integer;

    invoke-static {p1}, Lh0/e2$b;->p(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->B:Ljava/lang/Integer;

    invoke-static {p1}, Lh0/e2$b;->q(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->C:Ljava/lang/Integer;

    invoke-static {p1}, Lh0/e2$b;->r(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->D:Ljava/lang/CharSequence;

    invoke-static {p1}, Lh0/e2$b;->s(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->E:Ljava/lang/CharSequence;

    invoke-static {p1}, Lh0/e2$b;->t(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->F:Ljava/lang/CharSequence;

    invoke-static {p1}, Lh0/e2$b;->u(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->G:Ljava/lang/Integer;

    invoke-static {p1}, Lh0/e2$b;->v(Lh0/e2$b;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->H:Ljava/lang/Integer;

    invoke-static {p1}, Lh0/e2$b;->x(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->I:Ljava/lang/CharSequence;

    invoke-static {p1}, Lh0/e2$b;->y(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->J:Ljava/lang/CharSequence;

    invoke-static {p1}, Lh0/e2$b;->z(Lh0/e2$b;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lh0/e2;->K:Ljava/lang/CharSequence;

    iput-object v2, p0, Lh0/e2;->L:Ljava/lang/Integer;

    invoke-static {p1}, Lh0/e2$b;->A(Lh0/e2$b;)Landroid/os/Bundle;

    move-result-object p1

    iput-object p1, p0, Lh0/e2;->M:Landroid/os/Bundle;

    return-void
.end method

.method synthetic constructor <init>(Lh0/e2$b;Lh0/e2$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/e2;-><init>(Lh0/e2$b;)V

    return-void
.end method

.method public static synthetic a(Landroid/os/Bundle;)Lh0/e2;
    .locals 0

    invoke-static {p0}, Lh0/e2;->c(Landroid/os/Bundle;)Lh0/e2;

    move-result-object p0

    return-object p0
.end method

.method private static c(Landroid/os/Bundle;)Lh0/e2;
    .locals 5

    new-instance v0, Lh0/e2$b;

    invoke-direct {v0}, Lh0/e2$b;-><init>()V

    sget-object v1, Lh0/e2;->O:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->m0(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->P:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->O(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->Q:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->N(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->R:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->M(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->S:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->W(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->T:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->l0(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->U:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->U(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->X:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    sget-object v3, Lh0/e2;->q0:Ljava/lang/String;

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2, v3}, Lh0/e2$b;->P([BLjava/lang/Integer;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->Y:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lh0/e2$b;->Q(Landroid/net/Uri;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->j0:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->r0(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->k0:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->S(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->l0:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->T(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->o0:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->Z(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->p0:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->R(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->r0:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->k0(Ljava/lang/CharSequence;)Lh0/e2$b;

    move-result-object v1

    sget-object v2, Lh0/e2;->u0:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/e2$b;->X(Landroid/os/Bundle;)Lh0/e2$b;

    sget-object v1, Lh0/e2;->V:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v2, Lh0/n3;->g:Lh0/h$a;

    invoke-interface {v2, v1}, Lh0/h$a;->a(Landroid/os/Bundle;)Lh0/h;

    move-result-object v1

    check-cast v1, Lh0/n3;

    invoke-virtual {v0, v1}, Lh0/e2$b;->q0(Lh0/n3;)Lh0/e2$b;

    :cond_1
    sget-object v1, Lh0/e2;->W:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v2, Lh0/n3;->g:Lh0/h$a;

    invoke-interface {v2, v1}, Lh0/h$a;->a(Landroid/os/Bundle;)Lh0/h;

    move-result-object v1

    check-cast v1, Lh0/n3;

    invoke-virtual {v0, v1}, Lh0/e2$b;->d0(Lh0/n3;)Lh0/e2$b;

    :cond_2
    sget-object v1, Lh0/e2;->Z:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->p0(Ljava/lang/Integer;)Lh0/e2$b;

    :cond_3
    sget-object v1, Lh0/e2;->a0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->o0(Ljava/lang/Integer;)Lh0/e2$b;

    :cond_4
    sget-object v1, Lh0/e2;->b0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->Y(Ljava/lang/Integer;)Lh0/e2$b;

    :cond_5
    sget-object v1, Lh0/e2;->t0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->a0(Ljava/lang/Boolean;)Lh0/e2$b;

    :cond_6
    sget-object v1, Lh0/e2;->c0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->b0(Ljava/lang/Boolean;)Lh0/e2$b;

    :cond_7
    sget-object v1, Lh0/e2;->d0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->g0(Ljava/lang/Integer;)Lh0/e2$b;

    :cond_8
    sget-object v1, Lh0/e2;->e0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->f0(Ljava/lang/Integer;)Lh0/e2$b;

    :cond_9
    sget-object v1, Lh0/e2;->f0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->e0(Ljava/lang/Integer;)Lh0/e2$b;

    :cond_a
    sget-object v1, Lh0/e2;->g0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->j0(Ljava/lang/Integer;)Lh0/e2$b;

    :cond_b
    sget-object v1, Lh0/e2;->h0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->i0(Ljava/lang/Integer;)Lh0/e2$b;

    :cond_c
    sget-object v1, Lh0/e2;->i0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->h0(Ljava/lang/Integer;)Lh0/e2$b;

    :cond_d
    sget-object v1, Lh0/e2;->m0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->V(Ljava/lang/Integer;)Lh0/e2$b;

    :cond_e
    sget-object v1, Lh0/e2;->n0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lh0/e2$b;->n0(Ljava/lang/Integer;)Lh0/e2$b;

    :cond_f
    sget-object v1, Lh0/e2;->s0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Lh0/e2$b;->c0(Ljava/lang/Integer;)Lh0/e2$b;

    :cond_10
    invoke-virtual {v0}, Lh0/e2$b;->H()Lh0/e2;

    move-result-object p0

    return-object p0
.end method

.method private static d(I)I
    .locals 0

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 p0, 0x0

    return p0

    :pswitch_1
    const/4 p0, 0x6

    return p0

    :pswitch_2
    const/4 p0, 0x5

    return p0

    :pswitch_3
    const/4 p0, 0x4

    return p0

    :pswitch_4
    const/4 p0, 0x3

    return p0

    :pswitch_5
    const/4 p0, 0x2

    return p0

    :pswitch_6
    const/4 p0, 0x1

    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method private static e(I)I
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/16 p0, 0x14

    return p0

    :pswitch_0
    const/16 p0, 0x19

    return p0

    :pswitch_1
    const/16 p0, 0x18

    return p0

    :pswitch_2
    const/16 p0, 0x17

    return p0

    :pswitch_3
    const/16 p0, 0x16

    return p0

    :pswitch_4
    const/16 p0, 0x15

    return p0

    :pswitch_5
    const/4 p0, 0x0

    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public b()Lh0/e2$b;
    .locals 2

    new-instance v0, Lh0/e2$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lh0/e2$b;-><init>(Lh0/e2;Lh0/e2$a;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const-class v2, Lh0/e2;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_1

    :cond_1
    check-cast p1, Lh0/e2;

    iget-object v2, p0, Lh0/e2;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->f:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->g:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->g:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->h:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->h:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->i:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->i:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->j:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->j:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->k:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->k:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->l:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->l:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->m:Lh0/n3;

    iget-object v3, p1, Lh0/e2;->m:Lh0/n3;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->n:Lh0/n3;

    iget-object v3, p1, Lh0/e2;->n:Lh0/n3;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->o:[B

    iget-object v3, p1, Lh0/e2;->o:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->p:Ljava/lang/Integer;

    iget-object v3, p1, Lh0/e2;->p:Ljava/lang/Integer;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->q:Landroid/net/Uri;

    iget-object v3, p1, Lh0/e2;->q:Landroid/net/Uri;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->r:Ljava/lang/Integer;

    iget-object v3, p1, Lh0/e2;->r:Ljava/lang/Integer;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->s:Ljava/lang/Integer;

    iget-object v3, p1, Lh0/e2;->s:Ljava/lang/Integer;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->t:Ljava/lang/Integer;

    iget-object v3, p1, Lh0/e2;->t:Ljava/lang/Integer;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->u:Ljava/lang/Boolean;

    iget-object v3, p1, Lh0/e2;->u:Ljava/lang/Boolean;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->v:Ljava/lang/Boolean;

    iget-object v3, p1, Lh0/e2;->v:Ljava/lang/Boolean;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->x:Ljava/lang/Integer;

    iget-object v3, p1, Lh0/e2;->x:Ljava/lang/Integer;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->y:Ljava/lang/Integer;

    iget-object v3, p1, Lh0/e2;->y:Ljava/lang/Integer;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->z:Ljava/lang/Integer;

    iget-object v3, p1, Lh0/e2;->z:Ljava/lang/Integer;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->A:Ljava/lang/Integer;

    iget-object v3, p1, Lh0/e2;->A:Ljava/lang/Integer;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->B:Ljava/lang/Integer;

    iget-object v3, p1, Lh0/e2;->B:Ljava/lang/Integer;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->C:Ljava/lang/Integer;

    iget-object v3, p1, Lh0/e2;->C:Ljava/lang/Integer;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->D:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->D:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->E:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->E:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->F:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->F:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->G:Ljava/lang/Integer;

    iget-object v3, p1, Lh0/e2;->G:Ljava/lang/Integer;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->H:Ljava/lang/Integer;

    iget-object v3, p1, Lh0/e2;->H:Ljava/lang/Integer;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->I:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->I:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->J:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->J:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->K:Ljava/lang/CharSequence;

    iget-object v3, p1, Lh0/e2;->K:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lh0/e2;->L:Ljava/lang/Integer;

    iget-object p1, p1, Lh0/e2;->L:Ljava/lang/Integer;

    invoke-static {v2, p1}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lh0/e2;->f:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->g:Ljava/lang/CharSequence;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->h:Ljava/lang/CharSequence;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->i:Ljava/lang/CharSequence;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->j:Ljava/lang/CharSequence;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->k:Ljava/lang/CharSequence;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->l:Ljava/lang/CharSequence;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->m:Lh0/n3;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->n:Lh0/n3;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->o:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->p:Ljava/lang/Integer;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->q:Landroid/net/Uri;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->r:Ljava/lang/Integer;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->s:Ljava/lang/Integer;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->t:Ljava/lang/Integer;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->u:Ljava/lang/Boolean;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->v:Ljava/lang/Boolean;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->x:Ljava/lang/Integer;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->y:Ljava/lang/Integer;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->z:Ljava/lang/Integer;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->A:Ljava/lang/Integer;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->B:Ljava/lang/Integer;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->C:Ljava/lang/Integer;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->D:Ljava/lang/CharSequence;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->E:Ljava/lang/CharSequence;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->F:Ljava/lang/CharSequence;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->G:Ljava/lang/Integer;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->H:Ljava/lang/Integer;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->I:Ljava/lang/CharSequence;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->J:Ljava/lang/CharSequence;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->K:Ljava/lang/CharSequence;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    iget-object v1, p0, Lh0/e2;->L:Ljava/lang/Integer;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    invoke-static {v0}, Lo2/j;->b([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
