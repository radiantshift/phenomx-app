.class public final Lh0/s$b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh0/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field A:Z

.field B:Landroid/os/Looper;

.field C:Z

.field final a:Landroid/content/Context;

.field b:Le2/d;

.field c:J

.field d:Lo2/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lo2/p<",
            "Lh0/t3;",
            ">;"
        }
    .end annotation
.end field

.field e:Lo2/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lo2/p<",
            "Lj1/x$a;",
            ">;"
        }
    .end annotation
.end field

.field f:Lo2/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lo2/p<",
            "Lc2/c0;",
            ">;"
        }
    .end annotation
.end field

.field g:Lo2/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lo2/p<",
            "Lh0/x1;",
            ">;"
        }
    .end annotation
.end field

.field h:Lo2/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lo2/p<",
            "Ld2/f;",
            ">;"
        }
    .end annotation
.end field

.field i:Lo2/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lo2/f<",
            "Le2/d;",
            "Li0/a;",
            ">;"
        }
    .end annotation
.end field

.field j:Landroid/os/Looper;

.field k:Le2/c0;

.field l:Lj0/e;

.field m:Z

.field n:I

.field o:Z

.field p:Z

.field q:I

.field r:I

.field s:Z

.field t:Lh0/u3;

.field u:J

.field v:J

.field w:Lh0/w1;

.field x:J

.field y:J

.field z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Lh0/v;

    invoke-direct {v0, p1}, Lh0/v;-><init>(Landroid/content/Context;)V

    new-instance v1, Lh0/x;

    invoke-direct {v1, p1}, Lh0/x;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, v1}, Lh0/s$b;-><init>(Landroid/content/Context;Lo2/p;Lo2/p;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lo2/p;Lo2/p;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lo2/p<",
            "Lh0/t3;",
            ">;",
            "Lo2/p<",
            "Lj1/x$a;",
            ">;)V"
        }
    .end annotation

    new-instance v4, Lh0/w;

    invoke-direct {v4, p1}, Lh0/w;-><init>(Landroid/content/Context;)V

    sget-object v5, Lh0/a0;->f:Lh0/a0;

    new-instance v6, Lh0/u;

    invoke-direct {v6, p1}, Lh0/u;-><init>(Landroid/content/Context;)V

    sget-object v7, Lh0/t;->f:Lh0/t;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lh0/s$b;-><init>(Landroid/content/Context;Lo2/p;Lo2/p;Lo2/p;Lo2/p;Lo2/p;Lo2/f;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lo2/p;Lo2/p;Lo2/p;Lo2/p;Lo2/p;Lo2/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lo2/p<",
            "Lh0/t3;",
            ">;",
            "Lo2/p<",
            "Lj1/x$a;",
            ">;",
            "Lo2/p<",
            "Lc2/c0;",
            ">;",
            "Lo2/p<",
            "Lh0/x1;",
            ">;",
            "Lo2/p<",
            "Ld2/f;",
            ">;",
            "Lo2/f<",
            "Le2/d;",
            "Li0/a;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lh0/s$b;->a:Landroid/content/Context;

    iput-object p2, p0, Lh0/s$b;->d:Lo2/p;

    iput-object p3, p0, Lh0/s$b;->e:Lo2/p;

    iput-object p4, p0, Lh0/s$b;->f:Lo2/p;

    iput-object p5, p0, Lh0/s$b;->g:Lo2/p;

    iput-object p6, p0, Lh0/s$b;->h:Lo2/p;

    iput-object p7, p0, Lh0/s$b;->i:Lo2/f;

    invoke-static {}, Le2/n0;->Q()Landroid/os/Looper;

    move-result-object p1

    iput-object p1, p0, Lh0/s$b;->j:Landroid/os/Looper;

    sget-object p1, Lj0/e;->l:Lj0/e;

    iput-object p1, p0, Lh0/s$b;->l:Lj0/e;

    const/4 p1, 0x0

    iput p1, p0, Lh0/s$b;->n:I

    const/4 p2, 0x1

    iput p2, p0, Lh0/s$b;->q:I

    iput p1, p0, Lh0/s$b;->r:I

    iput-boolean p2, p0, Lh0/s$b;->s:Z

    sget-object p1, Lh0/u3;->g:Lh0/u3;

    iput-object p1, p0, Lh0/s$b;->t:Lh0/u3;

    const-wide/16 p3, 0x1388

    iput-wide p3, p0, Lh0/s$b;->u:J

    const-wide/16 p3, 0x3a98

    iput-wide p3, p0, Lh0/s$b;->v:J

    new-instance p1, Lh0/j$b;

    invoke-direct {p1}, Lh0/j$b;-><init>()V

    invoke-virtual {p1}, Lh0/j$b;->a()Lh0/j;

    move-result-object p1

    iput-object p1, p0, Lh0/s$b;->w:Lh0/w1;

    sget-object p1, Le2/d;->a:Le2/d;

    iput-object p1, p0, Lh0/s$b;->b:Le2/d;

    const-wide/16 p3, 0x1f4

    iput-wide p3, p0, Lh0/s$b;->x:J

    const-wide/16 p3, 0x7d0

    iput-wide p3, p0, Lh0/s$b;->y:J

    iput-boolean p2, p0, Lh0/s$b;->A:Z

    return-void
.end method

.method public static synthetic a(Landroid/content/Context;)Ld2/f;
    .locals 0

    invoke-static {p0}, Lh0/s$b;->k(Landroid/content/Context;)Ld2/f;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic b(Landroid/content/Context;)Lh0/t3;
    .locals 0

    invoke-static {p0}, Lh0/s$b;->h(Landroid/content/Context;)Lh0/t3;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic c(Lh0/t3;)Lh0/t3;
    .locals 0

    invoke-static {p0}, Lh0/s$b;->m(Lh0/t3;)Lh0/t3;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic d(Landroid/content/Context;)Lc2/c0;
    .locals 0

    invoke-static {p0}, Lh0/s$b;->j(Landroid/content/Context;)Lc2/c0;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic e(Landroid/content/Context;)Lj1/x$a;
    .locals 0

    invoke-static {p0}, Lh0/s$b;->i(Landroid/content/Context;)Lj1/x$a;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic f(Lh0/x1;)Lh0/x1;
    .locals 0

    invoke-static {p0}, Lh0/s$b;->l(Lh0/x1;)Lh0/x1;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic h(Landroid/content/Context;)Lh0/t3;
    .locals 1

    new-instance v0, Lh0/m;

    invoke-direct {v0, p0}, Lh0/m;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static synthetic i(Landroid/content/Context;)Lj1/x$a;
    .locals 2

    new-instance v0, Lj1/m;

    new-instance v1, Lm0/i;

    invoke-direct {v1}, Lm0/i;-><init>()V

    invoke-direct {v0, p0, v1}, Lj1/m;-><init>(Landroid/content/Context;Lm0/r;)V

    return-object v0
.end method

.method private static synthetic j(Landroid/content/Context;)Lc2/c0;
    .locals 1

    new-instance v0, Lc2/m;

    invoke-direct {v0, p0}, Lc2/m;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static synthetic k(Landroid/content/Context;)Ld2/f;
    .locals 0

    invoke-static {p0}, Ld2/s;->n(Landroid/content/Context;)Ld2/s;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic l(Lh0/x1;)Lh0/x1;
    .locals 0

    return-object p0
.end method

.method private static synthetic m(Lh0/t3;)Lh0/t3;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public g()Lh0/s;
    .locals 2

    iget-boolean v0, p0, Lh0/s$b;->C:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-static {v0}, Le2/a;->f(Z)V

    iput-boolean v1, p0, Lh0/s$b;->C:Z

    new-instance v0, Lh0/b1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lh0/b1;-><init>(Lh0/s$b;Lh0/g3;)V

    return-object v0
.end method

.method public n(Lh0/w1;)Lh0/s$b;
    .locals 1

    iget-boolean v0, p0, Lh0/s$b;->C:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Le2/a;->f(Z)V

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh0/w1;

    iput-object p1, p0, Lh0/s$b;->w:Lh0/w1;

    return-object p0
.end method

.method public o(Lh0/x1;)Lh0/s$b;
    .locals 1

    iget-boolean v0, p0, Lh0/s$b;->C:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Le2/a;->f(Z)V

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lh0/y;

    invoke-direct {v0, p1}, Lh0/y;-><init>(Lh0/x1;)V

    iput-object v0, p0, Lh0/s$b;->g:Lo2/p;

    return-object p0
.end method

.method public p(Lh0/t3;)Lh0/s$b;
    .locals 1

    iget-boolean v0, p0, Lh0/s$b;->C:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Le2/a;->f(Z)V

    invoke-static {p1}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lh0/z;

    invoke-direct {v0, p1}, Lh0/z;-><init>(Lh0/t3;)V

    iput-object v0, p0, Lh0/s$b;->d:Lo2/p;

    return-object p0
.end method
