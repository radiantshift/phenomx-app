.class Lh0/d$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh0/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private final f:Landroid/os/Handler;

.field final synthetic g:Lh0/d;


# direct methods
.method public constructor <init>(Lh0/d;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lh0/d$a;->g:Lh0/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lh0/d$a;->f:Landroid/os/Handler;

    return-void
.end method

.method public static synthetic a(Lh0/d$a;I)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/d$a;->b(I)V

    return-void
.end method

.method private synthetic b(I)V
    .locals 1

    iget-object v0, p0, Lh0/d$a;->g:Lh0/d;

    invoke-static {v0, p1}, Lh0/d;->d(Lh0/d;I)V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 2

    iget-object v0, p0, Lh0/d$a;->f:Landroid/os/Handler;

    new-instance v1, Lh0/c;

    invoke-direct {v1, p0, p1}, Lh0/c;-><init>(Lh0/d$a;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
