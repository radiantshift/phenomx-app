.class public final Lh0/o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# static fields
.field public static final i:Lh0/o;

.field private static final j:Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/lang/String;

.field public static final m:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Lh0/o;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final f:I

.field public final g:I

.field public final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lh0/o;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1}, Lh0/o;-><init>(III)V

    sput-object v0, Lh0/o;->i:Lh0/o;

    invoke-static {v1}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/o;->j:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/o;->k:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/o;->l:Ljava/lang/String;

    sget-object v0, Lh0/n;->a:Lh0/n;

    sput-object v0, Lh0/o;->m:Lh0/h$a;

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lh0/o;->f:I

    iput p2, p0, Lh0/o;->g:I

    iput p3, p0, Lh0/o;->h:I

    return-void
.end method

.method public static synthetic a(Landroid/os/Bundle;)Lh0/o;
    .locals 0

    invoke-static {p0}, Lh0/o;->b(Landroid/os/Bundle;)Lh0/o;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic b(Landroid/os/Bundle;)Lh0/o;
    .locals 4

    sget-object v0, Lh0/o;->j:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    sget-object v2, Lh0/o;->k:Ljava/lang/String;

    invoke-virtual {p0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    sget-object v3, Lh0/o;->l:Ljava/lang/String;

    invoke-virtual {p0, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p0

    new-instance v1, Lh0/o;

    invoke-direct {v1, v0, v2, p0}, Lh0/o;-><init>(III)V

    return-object v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lh0/o;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lh0/o;

    iget v1, p0, Lh0/o;->f:I

    iget v3, p1, Lh0/o;->f:I

    if-ne v1, v3, :cond_2

    iget v1, p0, Lh0/o;->g:I

    iget v3, p1, Lh0/o;->g:I

    if-ne v1, v3, :cond_2

    iget v1, p0, Lh0/o;->h:I

    iget p1, p1, Lh0/o;->h:I

    if-ne v1, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lh0/o;->f:I

    const/16 v1, 0x20f

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lh0/o;->g:I

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    iget v0, p0, Lh0/o;->h:I

    add-int/2addr v1, v0

    return v1
.end method
