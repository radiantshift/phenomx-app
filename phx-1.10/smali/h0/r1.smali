.class public final Lh0/r1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh0/r1$b;
    }
.end annotation


# static fields
.field private static final N:Lh0/r1;

.field private static final O:Ljava/lang/String;

.field private static final P:Ljava/lang/String;

.field private static final Q:Ljava/lang/String;

.field private static final R:Ljava/lang/String;

.field private static final S:Ljava/lang/String;

.field private static final T:Ljava/lang/String;

.field private static final U:Ljava/lang/String;

.field private static final V:Ljava/lang/String;

.field private static final W:Ljava/lang/String;

.field private static final X:Ljava/lang/String;

.field private static final Y:Ljava/lang/String;

.field private static final Z:Ljava/lang/String;

.field private static final a0:Ljava/lang/String;

.field private static final b0:Ljava/lang/String;

.field private static final c0:Ljava/lang/String;

.field private static final d0:Ljava/lang/String;

.field private static final e0:Ljava/lang/String;

.field private static final f0:Ljava/lang/String;

.field private static final g0:Ljava/lang/String;

.field private static final h0:Ljava/lang/String;

.field private static final i0:Ljava/lang/String;

.field private static final j0:Ljava/lang/String;

.field private static final k0:Ljava/lang/String;

.field private static final l0:Ljava/lang/String;

.field private static final m0:Ljava/lang/String;

.field private static final n0:Ljava/lang/String;

.field private static final o0:Ljava/lang/String;

.field private static final p0:Ljava/lang/String;

.field private static final q0:Ljava/lang/String;

.field private static final r0:Ljava/lang/String;

.field private static final s0:Ljava/lang/String;

.field private static final t0:Ljava/lang/String;

.field public static final u0:Lh0/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/h$a<",
            "Lh0/r1;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:[B

.field public final B:I

.field public final C:Lf2/c;

.field public final D:I

.field public final E:I

.field public final F:I

.field public final G:I

.field public final H:I

.field public final I:I

.field public final J:I

.field public final K:I

.field public final L:I

.field private M:I

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:Ljava/lang/String;

.field public final o:Lz0/a;

.field public final p:Ljava/lang/String;

.field public final q:Ljava/lang/String;

.field public final r:I

.field public final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[B>;"
        }
    .end annotation
.end field

.field public final t:Ll0/m;

.field public final u:J

.field public final v:I

.field public final w:I

.field public final x:F

.field public final y:I

.field public final z:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lh0/r1$b;

    invoke-direct {v0}, Lh0/r1$b;-><init>()V

    invoke-virtual {v0}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object v0

    sput-object v0, Lh0/r1;->N:Lh0/r1;

    const/4 v0, 0x0

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->O:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->P:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->Q:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->R:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->S:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->T:Ljava/lang/String;

    const/4 v0, 0x6

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->U:Ljava/lang/String;

    const/4 v0, 0x7

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->V:Ljava/lang/String;

    const/16 v0, 0x8

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->W:Ljava/lang/String;

    const/16 v0, 0x9

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->X:Ljava/lang/String;

    const/16 v0, 0xa

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->Y:Ljava/lang/String;

    const/16 v0, 0xb

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->Z:Ljava/lang/String;

    const/16 v0, 0xc

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->a0:Ljava/lang/String;

    const/16 v0, 0xd

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->b0:Ljava/lang/String;

    const/16 v0, 0xe

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->c0:Ljava/lang/String;

    const/16 v0, 0xf

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->d0:Ljava/lang/String;

    const/16 v0, 0x10

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->e0:Ljava/lang/String;

    const/16 v0, 0x11

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->f0:Ljava/lang/String;

    const/16 v0, 0x12

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->g0:Ljava/lang/String;

    const/16 v0, 0x13

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->h0:Ljava/lang/String;

    const/16 v0, 0x14

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->i0:Ljava/lang/String;

    const/16 v0, 0x15

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->j0:Ljava/lang/String;

    const/16 v0, 0x16

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->k0:Ljava/lang/String;

    const/16 v0, 0x17

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->l0:Ljava/lang/String;

    const/16 v0, 0x18

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->m0:Ljava/lang/String;

    const/16 v0, 0x19

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->n0:Ljava/lang/String;

    const/16 v0, 0x1a

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->o0:Ljava/lang/String;

    const/16 v0, 0x1b

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->p0:Ljava/lang/String;

    const/16 v0, 0x1c

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->q0:Ljava/lang/String;

    const/16 v0, 0x1d

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->r0:Ljava/lang/String;

    const/16 v0, 0x1e

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->s0:Ljava/lang/String;

    const/16 v0, 0x1f

    invoke-static {v0}, Le2/n0;->r0(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lh0/r1;->t0:Ljava/lang/String;

    sget-object v0, Lh0/q1;->a:Lh0/q1;

    sput-object v0, Lh0/r1;->u0:Lh0/h$a;

    return-void
.end method

.method private constructor <init>(Lh0/r1$b;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lh0/r1$b;->a(Lh0/r1$b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lh0/r1;->f:Ljava/lang/String;

    invoke-static {p1}, Lh0/r1$b;->l(Lh0/r1$b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lh0/r1;->g:Ljava/lang/String;

    invoke-static {p1}, Lh0/r1$b;->w(Lh0/r1$b;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Le2/n0;->E0(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lh0/r1;->h:Ljava/lang/String;

    invoke-static {p1}, Lh0/r1$b;->A(Lh0/r1$b;)I

    move-result v0

    iput v0, p0, Lh0/r1;->i:I

    invoke-static {p1}, Lh0/r1$b;->B(Lh0/r1$b;)I

    move-result v0

    iput v0, p0, Lh0/r1;->j:I

    invoke-static {p1}, Lh0/r1$b;->C(Lh0/r1$b;)I

    move-result v0

    iput v0, p0, Lh0/r1;->k:I

    invoke-static {p1}, Lh0/r1$b;->D(Lh0/r1$b;)I

    move-result v1

    iput v1, p0, Lh0/r1;->l:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    :cond_0
    iput v0, p0, Lh0/r1;->m:I

    invoke-static {p1}, Lh0/r1$b;->E(Lh0/r1$b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lh0/r1;->n:Ljava/lang/String;

    invoke-static {p1}, Lh0/r1$b;->F(Lh0/r1$b;)Lz0/a;

    move-result-object v0

    iput-object v0, p0, Lh0/r1;->o:Lz0/a;

    invoke-static {p1}, Lh0/r1$b;->b(Lh0/r1$b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lh0/r1;->p:Ljava/lang/String;

    invoke-static {p1}, Lh0/r1$b;->c(Lh0/r1$b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lh0/r1;->q:Ljava/lang/String;

    invoke-static {p1}, Lh0/r1$b;->d(Lh0/r1$b;)I

    move-result v0

    iput v0, p0, Lh0/r1;->r:I

    invoke-static {p1}, Lh0/r1$b;->e(Lh0/r1$b;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lh0/r1$b;->e(Lh0/r1$b;)Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lh0/r1;->s:Ljava/util/List;

    invoke-static {p1}, Lh0/r1$b;->f(Lh0/r1$b;)Ll0/m;

    move-result-object v0

    iput-object v0, p0, Lh0/r1;->t:Ll0/m;

    invoke-static {p1}, Lh0/r1$b;->g(Lh0/r1$b;)J

    move-result-wide v3

    iput-wide v3, p0, Lh0/r1;->u:J

    invoke-static {p1}, Lh0/r1$b;->h(Lh0/r1$b;)I

    move-result v1

    iput v1, p0, Lh0/r1;->v:I

    invoke-static {p1}, Lh0/r1$b;->i(Lh0/r1$b;)I

    move-result v1

    iput v1, p0, Lh0/r1;->w:I

    invoke-static {p1}, Lh0/r1$b;->j(Lh0/r1$b;)F

    move-result v1

    iput v1, p0, Lh0/r1;->x:F

    invoke-static {p1}, Lh0/r1$b;->k(Lh0/r1$b;)I

    move-result v1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_2

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    invoke-static {p1}, Lh0/r1$b;->k(Lh0/r1$b;)I

    move-result v1

    :goto_1
    iput v1, p0, Lh0/r1;->y:I

    invoke-static {p1}, Lh0/r1$b;->m(Lh0/r1$b;)F

    move-result v1

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v1, v1, v4

    if-nez v1, :cond_3

    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_3
    invoke-static {p1}, Lh0/r1$b;->m(Lh0/r1$b;)F

    move-result v1

    :goto_2
    iput v1, p0, Lh0/r1;->z:F

    invoke-static {p1}, Lh0/r1$b;->n(Lh0/r1$b;)[B

    move-result-object v1

    iput-object v1, p0, Lh0/r1;->A:[B

    invoke-static {p1}, Lh0/r1$b;->o(Lh0/r1$b;)I

    move-result v1

    iput v1, p0, Lh0/r1;->B:I

    invoke-static {p1}, Lh0/r1$b;->p(Lh0/r1$b;)Lf2/c;

    move-result-object v1

    iput-object v1, p0, Lh0/r1;->C:Lf2/c;

    invoke-static {p1}, Lh0/r1$b;->q(Lh0/r1$b;)I

    move-result v1

    iput v1, p0, Lh0/r1;->D:I

    invoke-static {p1}, Lh0/r1$b;->r(Lh0/r1$b;)I

    move-result v1

    iput v1, p0, Lh0/r1;->E:I

    invoke-static {p1}, Lh0/r1$b;->s(Lh0/r1$b;)I

    move-result v1

    iput v1, p0, Lh0/r1;->F:I

    invoke-static {p1}, Lh0/r1$b;->t(Lh0/r1$b;)I

    move-result v1

    if-ne v1, v2, :cond_4

    const/4 v1, 0x0

    goto :goto_3

    :cond_4
    invoke-static {p1}, Lh0/r1$b;->t(Lh0/r1$b;)I

    move-result v1

    :goto_3
    iput v1, p0, Lh0/r1;->G:I

    invoke-static {p1}, Lh0/r1$b;->u(Lh0/r1$b;)I

    move-result v1

    if-ne v1, v2, :cond_5

    goto :goto_4

    :cond_5
    invoke-static {p1}, Lh0/r1$b;->u(Lh0/r1$b;)I

    move-result v3

    :goto_4
    iput v3, p0, Lh0/r1;->H:I

    invoke-static {p1}, Lh0/r1$b;->v(Lh0/r1$b;)I

    move-result v1

    iput v1, p0, Lh0/r1;->I:I

    invoke-static {p1}, Lh0/r1$b;->x(Lh0/r1$b;)I

    move-result v1

    iput v1, p0, Lh0/r1;->J:I

    invoke-static {p1}, Lh0/r1$b;->y(Lh0/r1$b;)I

    move-result v1

    iput v1, p0, Lh0/r1;->K:I

    invoke-static {p1}, Lh0/r1$b;->z(Lh0/r1$b;)I

    move-result v1

    if-nez v1, :cond_6

    if-eqz v0, :cond_6

    const/4 p1, 0x1

    goto :goto_5

    :cond_6
    invoke-static {p1}, Lh0/r1$b;->z(Lh0/r1$b;)I

    move-result p1

    :goto_5
    iput p1, p0, Lh0/r1;->L:I

    return-void
.end method

.method synthetic constructor <init>(Lh0/r1$b;Lh0/r1$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/r1;-><init>(Lh0/r1$b;)V

    return-void
.end method

.method public static synthetic a(Landroid/os/Bundle;)Lh0/r1;
    .locals 0

    invoke-static {p0}, Lh0/r1;->e(Landroid/os/Bundle;)Lh0/r1;

    move-result-object p0

    return-object p0
.end method

.method private static d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)TT;"
        }
    .end annotation

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    move-object p0, p1

    :goto_0
    return-object p0
.end method

.method private static e(Landroid/os/Bundle;)Lh0/r1;
    .locals 6

    new-instance v0, Lh0/r1$b;

    invoke-direct {v0}, Lh0/r1$b;-><init>()V

    invoke-static {p0}, Le2/c;->a(Landroid/os/Bundle;)V

    sget-object v1, Lh0/r1;->O:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lh0/r1;->N:Lh0/r1;

    iget-object v3, v2, Lh0/r1;->f:Ljava/lang/String;

    invoke-static {v1, v3}, Lh0/r1;->d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lh0/r1$b;->U(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v1

    sget-object v3, Lh0/r1;->P:Ljava/lang/String;

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lh0/r1;->g:Ljava/lang/String;

    invoke-static {v3, v4}, Lh0/r1;->d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Lh0/r1$b;->W(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v1

    sget-object v3, Lh0/r1;->Q:Ljava/lang/String;

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lh0/r1;->h:Ljava/lang/String;

    invoke-static {v3, v4}, Lh0/r1;->d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Lh0/r1$b;->X(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v1

    sget-object v3, Lh0/r1;->R:Ljava/lang/String;

    iget v4, v2, Lh0/r1;->i:I

    invoke-virtual {p0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v3}, Lh0/r1$b;->i0(I)Lh0/r1$b;

    move-result-object v1

    sget-object v3, Lh0/r1;->S:Ljava/lang/String;

    iget v4, v2, Lh0/r1;->j:I

    invoke-virtual {p0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v3}, Lh0/r1$b;->e0(I)Lh0/r1$b;

    move-result-object v1

    sget-object v3, Lh0/r1;->T:Ljava/lang/String;

    iget v4, v2, Lh0/r1;->k:I

    invoke-virtual {p0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v3}, Lh0/r1$b;->I(I)Lh0/r1$b;

    move-result-object v1

    sget-object v3, Lh0/r1;->U:Ljava/lang/String;

    iget v4, v2, Lh0/r1;->l:I

    invoke-virtual {p0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v3}, Lh0/r1$b;->b0(I)Lh0/r1$b;

    move-result-object v1

    sget-object v3, Lh0/r1;->V:Ljava/lang/String;

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lh0/r1;->n:Ljava/lang/String;

    invoke-static {v3, v4}, Lh0/r1;->d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Lh0/r1$b;->K(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v1

    sget-object v3, Lh0/r1;->W:Ljava/lang/String;

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lz0/a;

    iget-object v4, v2, Lh0/r1;->o:Lz0/a;

    invoke-static {v3, v4}, Lh0/r1;->d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lz0/a;

    invoke-virtual {v1, v3}, Lh0/r1$b;->Z(Lz0/a;)Lh0/r1$b;

    move-result-object v1

    sget-object v3, Lh0/r1;->X:Ljava/lang/String;

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lh0/r1;->p:Ljava/lang/String;

    invoke-static {v3, v4}, Lh0/r1;->d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Lh0/r1$b;->M(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v1

    sget-object v3, Lh0/r1;->Y:Ljava/lang/String;

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lh0/r1;->q:Ljava/lang/String;

    invoke-static {v3, v4}, Lh0/r1;->d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Lh0/r1$b;->g0(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v1

    sget-object v3, Lh0/r1;->Z:Ljava/lang/String;

    iget v2, v2, Lh0/r1;->r:I

    invoke-virtual {p0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->Y(I)Lh0/r1$b;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Lh0/r1;->h(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    if-nez v3, :cond_1

    invoke-virtual {v0, v1}, Lh0/r1$b;->V(Ljava/util/List;)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->b0:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Ll0/m;

    invoke-virtual {v1, v2}, Lh0/r1$b;->O(Ll0/m;)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->c0:Ljava/lang/String;

    sget-object v3, Lh0/r1;->N:Lh0/r1;

    iget-wide v4, v3, Lh0/r1;->u:J

    invoke-virtual {p0, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lh0/r1$b;->k0(J)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->d0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->v:I

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->n0(I)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->e0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->w:I

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->S(I)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->f0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->x:F

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->R(F)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->g0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->y:I

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->f0(I)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->h0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->z:F

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->c0(F)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->i0:Ljava/lang/String;

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->d0([B)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->j0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->B:I

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->j0(I)Lh0/r1$b;

    sget-object v1, Lh0/r1;->k0:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lf2/c;->p:Lh0/h$a;

    invoke-interface {v2, v1}, Lh0/h$a;->a(Landroid/os/Bundle;)Lh0/h;

    move-result-object v1

    check-cast v1, Lf2/c;

    invoke-virtual {v0, v1}, Lh0/r1$b;->L(Lf2/c;)Lh0/r1$b;

    :cond_0
    sget-object v1, Lh0/r1;->l0:Ljava/lang/String;

    iget v2, v3, Lh0/r1;->D:I

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lh0/r1$b;->J(I)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->m0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->E:I

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->h0(I)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->n0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->F:I

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->a0(I)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->o0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->G:I

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->P(I)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->p0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->H:I

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->Q(I)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->q0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->I:I

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->H(I)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->s0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->J:I

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->l0(I)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->t0:Ljava/lang/String;

    iget v4, v3, Lh0/r1;->K:I

    invoke-virtual {p0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lh0/r1$b;->m0(I)Lh0/r1$b;

    move-result-object v1

    sget-object v2, Lh0/r1;->r0:Ljava/lang/String;

    iget v3, v3, Lh0/r1;->L:I

    invoke-virtual {p0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p0

    invoke-virtual {v1, p0}, Lh0/r1$b;->N(I)Lh0/r1$b;

    invoke-virtual {v0}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object p0

    return-object p0

    :cond_1
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method private static h(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lh0/r1;->a0:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x24

    invoke-static {p0, v1}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static i(Lh0/r1;)Ljava/lang/String;
    .locals 8

    if-nez p0, :cond_0

    const-string p0, "null"

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lh0/r1;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", mimeType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lh0/r1;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lh0/r1;->m:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const-string v1, ", bitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lh0/r1;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v1, p0, Lh0/r1;->n:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", codecs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lh0/r1;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v1, p0, Lh0/r1;->t:Ll0/m;

    const/16 v3, 0x2c

    if-eqz v1, :cond_9

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    const/4 v4, 0x0

    :goto_0
    iget-object v5, p0, Lh0/r1;->t:Ll0/m;

    iget v6, v5, Ll0/m;->i:I

    if-ge v4, v6, :cond_8

    invoke-virtual {v5, v4}, Ll0/m;->h(I)Ll0/m$b;

    move-result-object v5

    iget-object v5, v5, Ll0/m$b;->g:Ljava/util/UUID;

    sget-object v6, Lh0/i;->b:Ljava/util/UUID;

    invoke-virtual {v5, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v5, "cenc"

    :goto_1
    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    sget-object v6, Lh0/i;->c:Ljava/util/UUID;

    invoke-virtual {v5, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v5, "clearkey"

    goto :goto_1

    :cond_4
    sget-object v6, Lh0/i;->e:Ljava/util/UUID;

    invoke-virtual {v5, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v5, "playready"

    goto :goto_1

    :cond_5
    sget-object v6, Lh0/i;->d:Ljava/util/UUID;

    invoke-virtual {v5, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v5, "widevine"

    goto :goto_1

    :cond_6
    sget-object v6, Lh0/i;->a:Ljava/util/UUID;

    invoke-virtual {v5, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    const-string v5, "universal"

    goto :goto_1

    :cond_7
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unknown ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, ")"

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_8
    const-string v4, ", drm=["

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v3}, Lo2/g;->d(C)Lo2/g;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lo2/g;->b(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_9
    iget v1, p0, Lh0/r1;->v:I

    if-eq v1, v2, :cond_a

    iget v1, p0, Lh0/r1;->w:I

    if-eq v1, v2, :cond_a

    const-string v1, ", res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lh0/r1;->v:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lh0/r1;->w:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_a
    iget v1, p0, Lh0/r1;->x:F

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v1, v1, v4

    if-eqz v1, :cond_b

    const-string v1, ", fps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lh0/r1;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    :cond_b
    iget v1, p0, Lh0/r1;->D:I

    if-eq v1, v2, :cond_c

    const-string v1, ", channels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lh0/r1;->D:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_c
    iget v1, p0, Lh0/r1;->E:I

    if-eq v1, v2, :cond_d

    const-string v1, ", sample_rate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lh0/r1;->E:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_d
    iget-object v1, p0, Lh0/r1;->h:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", language="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lh0/r1;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_e
    iget-object v1, p0, Lh0/r1;->g:Ljava/lang/String;

    if-eqz v1, :cond_f

    const-string v1, ", label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lh0/r1;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    iget v1, p0, Lh0/r1;->i:I

    const-string v2, "]"

    if-eqz v1, :cond_13

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget v4, p0, Lh0/r1;->i:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_10

    const-string v4, "auto"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10
    iget v4, p0, Lh0/r1;->i:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_11

    const-string v4, "default"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_11
    iget v4, p0, Lh0/r1;->i:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_12

    const-string v4, "forced"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_12
    const-string v4, ", selectionFlags=["

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v3}, Lo2/g;->d(C)Lo2/g;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lo2/g;->b(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_13
    iget v1, p0, Lh0/r1;->j:I

    if-eqz v1, :cond_23

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget v4, p0, Lh0/r1;->j:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_14

    const-string v4, "main"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_14
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_15

    const-string v4, "alt"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_15
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_16

    const-string v4, "supplementary"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_16
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_17

    const-string v4, "commentary"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_17
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_18

    const-string v4, "dub"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_18
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_19

    const-string v4, "emergency"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_19
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_1a

    const-string v4, "caption"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1a
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_1b

    const-string v4, "subtitle"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1b
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit16 v4, v4, 0x100

    if-eqz v4, :cond_1c

    const-string v4, "sign"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1c
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit16 v4, v4, 0x200

    if-eqz v4, :cond_1d

    const-string v4, "describes-video"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1d
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit16 v4, v4, 0x400

    if-eqz v4, :cond_1e

    const-string v4, "describes-music"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1e
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit16 v4, v4, 0x800

    if-eqz v4, :cond_1f

    const-string v4, "enhanced-intelligibility"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1f
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit16 v4, v4, 0x1000

    if-eqz v4, :cond_20

    const-string v4, "transcribes-dialog"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_20
    iget v4, p0, Lh0/r1;->j:I

    and-int/lit16 v4, v4, 0x2000

    if-eqz v4, :cond_21

    const-string v4, "easy-read"

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_21
    iget p0, p0, Lh0/r1;->j:I

    and-int/lit16 p0, p0, 0x4000

    if-eqz p0, :cond_22

    const-string p0, "trick-play"

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_22
    const-string p0, ", roleFlags=["

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v3}, Lo2/g;->d(C)Lo2/g;

    move-result-object p0

    invoke-virtual {p0, v0, v1}, Lo2/g;->b(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public b()Lh0/r1$b;
    .locals 2

    new-instance v0, Lh0/r1$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lh0/r1$b;-><init>(Lh0/r1;Lh0/r1$a;)V

    return-object v0
.end method

.method public c(I)Lh0/r1;
    .locals 1

    invoke-virtual {p0}, Lh0/r1;->b()Lh0/r1$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lh0/r1$b;->N(I)Lh0/r1$b;

    move-result-object p1

    invoke-virtual {p1}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object p1

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_4

    const-class v2, Lh0/r1;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_1

    :cond_1
    check-cast p1, Lh0/r1;

    iget v2, p0, Lh0/r1;->M:I

    if-eqz v2, :cond_2

    iget v3, p1, Lh0/r1;->M:I

    if-eqz v3, :cond_2

    if-eq v2, v3, :cond_2

    return v1

    :cond_2
    iget v2, p0, Lh0/r1;->i:I

    iget v3, p1, Lh0/r1;->i:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->j:I

    iget v3, p1, Lh0/r1;->j:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->k:I

    iget v3, p1, Lh0/r1;->k:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->l:I

    iget v3, p1, Lh0/r1;->l:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->r:I

    iget v3, p1, Lh0/r1;->r:I

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lh0/r1;->u:J

    iget-wide v4, p1, Lh0/r1;->u:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_3

    iget v2, p0, Lh0/r1;->v:I

    iget v3, p1, Lh0/r1;->v:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->w:I

    iget v3, p1, Lh0/r1;->w:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->y:I

    iget v3, p1, Lh0/r1;->y:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->B:I

    iget v3, p1, Lh0/r1;->B:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->D:I

    iget v3, p1, Lh0/r1;->D:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->E:I

    iget v3, p1, Lh0/r1;->E:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->F:I

    iget v3, p1, Lh0/r1;->F:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->G:I

    iget v3, p1, Lh0/r1;->G:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->H:I

    iget v3, p1, Lh0/r1;->H:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->I:I

    iget v3, p1, Lh0/r1;->I:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->J:I

    iget v3, p1, Lh0/r1;->J:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->K:I

    iget v3, p1, Lh0/r1;->K:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->L:I

    iget v3, p1, Lh0/r1;->L:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lh0/r1;->x:F

    iget v3, p1, Lh0/r1;->x:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_3

    iget v2, p0, Lh0/r1;->z:F

    iget v3, p1, Lh0/r1;->z:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lh0/r1;->f:Ljava/lang/String;

    iget-object v3, p1, Lh0/r1;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lh0/r1;->g:Ljava/lang/String;

    iget-object v3, p1, Lh0/r1;->g:Ljava/lang/String;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lh0/r1;->n:Ljava/lang/String;

    iget-object v3, p1, Lh0/r1;->n:Ljava/lang/String;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lh0/r1;->p:Ljava/lang/String;

    iget-object v3, p1, Lh0/r1;->p:Ljava/lang/String;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lh0/r1;->q:Ljava/lang/String;

    iget-object v3, p1, Lh0/r1;->q:Ljava/lang/String;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lh0/r1;->h:Ljava/lang/String;

    iget-object v3, p1, Lh0/r1;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lh0/r1;->A:[B

    iget-object v3, p1, Lh0/r1;->A:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lh0/r1;->o:Lz0/a;

    iget-object v3, p1, Lh0/r1;->o:Lz0/a;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lh0/r1;->C:Lf2/c;

    iget-object v3, p1, Lh0/r1;->C:Lf2/c;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lh0/r1;->t:Ll0/m;

    iget-object v3, p1, Lh0/r1;->t:Ll0/m;

    invoke-static {v2, v3}, Le2/n0;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0, p1}, Lh0/r1;->g(Lh0/r1;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_4
    :goto_1
    return v1
.end method

.method public f()I
    .locals 3

    iget v0, p0, Lh0/r1;->v:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget v2, p0, Lh0/r1;->w:I

    if-ne v2, v1, :cond_0

    goto :goto_0

    :cond_0
    mul-int v1, v0, v2

    :cond_1
    :goto_0
    return v1
.end method

.method public g(Lh0/r1;)Z
    .locals 4

    iget-object v0, p0, Lh0/r1;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p1, Lh0/r1;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    return v2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lh0/r1;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lh0/r1;->s:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iget-object v3, p1, Lh0/r1;->s:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    invoke-static {v1, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    return v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lh0/r1;->M:I

    if-nez v0, :cond_7

    const/16 v0, 0x20f

    iget-object v1, p0, Lh0/r1;->f:Ljava/lang/String;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lh0/r1;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lh0/r1;->h:Ljava/lang/String;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->i:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->j:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->k:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->l:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lh0/r1;->n:Ljava/lang/String;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    goto :goto_3

    :cond_3
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lh0/r1;->o:Lz0/a;

    if-nez v1, :cond_4

    const/4 v1, 0x0

    goto :goto_4

    :cond_4
    invoke-virtual {v1}, Lz0/a;->hashCode()I

    move-result v1

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lh0/r1;->p:Ljava/lang/String;

    if-nez v1, :cond_5

    const/4 v1, 0x0

    goto :goto_5

    :cond_5
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lh0/r1;->q:Ljava/lang/String;

    if-nez v1, :cond_6

    goto :goto_6

    :cond_6
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->r:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lh0/r1;->u:J

    long-to-int v2, v1

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->v:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->w:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->x:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->y:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->z:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->B:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->D:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->E:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->F:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->G:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->H:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->I:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->J:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->K:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lh0/r1;->L:I

    add-int/2addr v0, v1

    iput v0, p0, Lh0/r1;->M:I

    :cond_7
    iget v0, p0, Lh0/r1;->M:I

    return v0
.end method

.method public j(Lh0/r1;)Lh0/r1;
    .locals 11

    if-ne p0, p1, :cond_0

    return-object p0

    :cond_0
    iget-object v0, p0, Lh0/r1;->q:Ljava/lang/String;

    invoke-static {v0}, Le2/v;->k(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p1, Lh0/r1;->f:Ljava/lang/String;

    iget-object v2, p1, Lh0/r1;->g:Ljava/lang/String;

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lh0/r1;->g:Ljava/lang/String;

    :goto_0
    iget-object v3, p0, Lh0/r1;->h:Ljava/lang/String;

    const/4 v4, 0x3

    const/4 v5, 0x1

    if-eq v0, v4, :cond_2

    if-ne v0, v5, :cond_3

    :cond_2
    iget-object v4, p1, Lh0/r1;->h:Ljava/lang/String;

    if-eqz v4, :cond_3

    move-object v3, v4

    :cond_3
    iget v4, p0, Lh0/r1;->k:I

    const/4 v6, -0x1

    if-ne v4, v6, :cond_4

    iget v4, p1, Lh0/r1;->k:I

    :cond_4
    iget v7, p0, Lh0/r1;->l:I

    if-ne v7, v6, :cond_5

    iget v7, p1, Lh0/r1;->l:I

    :cond_5
    iget-object v6, p0, Lh0/r1;->n:Ljava/lang/String;

    if-nez v6, :cond_6

    iget-object v8, p1, Lh0/r1;->n:Ljava/lang/String;

    invoke-static {v8, v0}, Le2/n0;->L(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Le2/n0;->T0(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    if-ne v9, v5, :cond_6

    move-object v6, v8

    :cond_6
    iget-object v5, p0, Lh0/r1;->o:Lz0/a;

    if-nez v5, :cond_7

    iget-object v5, p1, Lh0/r1;->o:Lz0/a;

    goto :goto_1

    :cond_7
    iget-object v8, p1, Lh0/r1;->o:Lz0/a;

    invoke-virtual {v5, v8}, Lz0/a;->e(Lz0/a;)Lz0/a;

    move-result-object v5

    :goto_1
    iget v8, p0, Lh0/r1;->x:F

    const/high16 v9, -0x40800000    # -1.0f

    cmpl-float v9, v8, v9

    if-nez v9, :cond_8

    const/4 v9, 0x2

    if-ne v0, v9, :cond_8

    iget v8, p1, Lh0/r1;->x:F

    :cond_8
    iget v0, p0, Lh0/r1;->i:I

    iget v9, p1, Lh0/r1;->i:I

    or-int/2addr v0, v9

    iget v9, p0, Lh0/r1;->j:I

    iget v10, p1, Lh0/r1;->j:I

    or-int/2addr v9, v10

    iget-object p1, p1, Lh0/r1;->t:Ll0/m;

    iget-object v10, p0, Lh0/r1;->t:Ll0/m;

    invoke-static {p1, v10}, Ll0/m;->g(Ll0/m;Ll0/m;)Ll0/m;

    move-result-object p1

    invoke-virtual {p0}, Lh0/r1;->b()Lh0/r1$b;

    move-result-object v10

    invoke-virtual {v10, v1}, Lh0/r1$b;->U(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v1

    invoke-virtual {v1, v2}, Lh0/r1$b;->W(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v1

    invoke-virtual {v1, v3}, Lh0/r1$b;->X(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lh0/r1$b;->i0(I)Lh0/r1$b;

    move-result-object v0

    invoke-virtual {v0, v9}, Lh0/r1$b;->e0(I)Lh0/r1$b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lh0/r1$b;->I(I)Lh0/r1$b;

    move-result-object v0

    invoke-virtual {v0, v7}, Lh0/r1$b;->b0(I)Lh0/r1$b;

    move-result-object v0

    invoke-virtual {v0, v6}, Lh0/r1$b;->K(Ljava/lang/String;)Lh0/r1$b;

    move-result-object v0

    invoke-virtual {v0, v5}, Lh0/r1$b;->Z(Lz0/a;)Lh0/r1$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lh0/r1$b;->O(Ll0/m;)Lh0/r1$b;

    move-result-object p1

    invoke-virtual {p1, v8}, Lh0/r1$b;->R(F)Lh0/r1$b;

    move-result-object p1

    invoke-virtual {p1}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Format("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lh0/r1;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lh0/r1;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lh0/r1;->p:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lh0/r1;->q:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lh0/r1;->n:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lh0/r1;->m:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lh0/r1;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lh0/r1;->v:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lh0/r1;->w:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lh0/r1;->x:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, "], ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lh0/r1;->D:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lh0/r1;->E:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "])"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
