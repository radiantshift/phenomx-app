.class public final Lh0/j$b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh0/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field private a:F

.field private b:F

.field private c:J

.field private d:F

.field private e:J

.field private f:J

.field private g:F


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x3f7851ec    # 0.97f

    iput v0, p0, Lh0/j$b;->a:F

    const v0, 0x3f83d70a    # 1.03f

    iput v0, p0, Lh0/j$b;->b:F

    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lh0/j$b;->c:J

    const v0, 0x33d6bf95    # 1.0E-7f

    iput v0, p0, Lh0/j$b;->d:F

    const-wide/16 v0, 0x14

    invoke-static {v0, v1}, Le2/n0;->C0(J)J

    move-result-wide v0

    iput-wide v0, p0, Lh0/j$b;->e:J

    const-wide/16 v0, 0x1f4

    invoke-static {v0, v1}, Le2/n0;->C0(J)J

    move-result-wide v0

    iput-wide v0, p0, Lh0/j$b;->f:J

    const v0, 0x3f7fbe77    # 0.999f

    iput v0, p0, Lh0/j$b;->g:F

    return-void
.end method


# virtual methods
.method public a()Lh0/j;
    .locals 13

    new-instance v12, Lh0/j;

    iget v1, p0, Lh0/j$b;->a:F

    iget v2, p0, Lh0/j$b;->b:F

    iget-wide v3, p0, Lh0/j$b;->c:J

    iget v5, p0, Lh0/j$b;->d:F

    iget-wide v6, p0, Lh0/j$b;->e:J

    iget-wide v8, p0, Lh0/j$b;->f:J

    iget v10, p0, Lh0/j$b;->g:F

    const/4 v11, 0x0

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lh0/j;-><init>(FFJFJJFLh0/j$a;)V

    return-object v12
.end method

.method public b(F)Lh0/j$b;
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->a(Z)V

    iput p1, p0, Lh0/j$b;->b:F

    return-object p0
.end method

.method public c(F)Lh0/j$b;
    .locals 1

    const/4 v0, 0x0

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->a(Z)V

    iput p1, p0, Lh0/j$b;->a:F

    return-object p0
.end method

.method public d(J)Lh0/j$b;
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->a(Z)V

    invoke-static {p1, p2}, Le2/n0;->C0(J)J

    move-result-wide p1

    iput-wide p1, p0, Lh0/j$b;->e:J

    return-object p0
.end method

.method public e(F)Lh0/j$b;
    .locals 1

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->a(Z)V

    iput p1, p0, Lh0/j$b;->g:F

    return-object p0
.end method

.method public f(J)Lh0/j$b;
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->a(Z)V

    iput-wide p1, p0, Lh0/j$b;->c:J

    return-object p0
.end method

.method public g(F)Lh0/j$b;
    .locals 1

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->a(Z)V

    const v0, 0x49742400    # 1000000.0f

    div-float/2addr p1, v0

    iput p1, p0, Lh0/j$b;->d:F

    return-object p0
.end method

.method public h(J)Lh0/j$b;
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->a(Z)V

    invoke-static {p1, p2}, Le2/n0;->C0(J)J

    move-result-wide p1

    iput-wide p1, p0, Lh0/j$b;->f:J

    return-object p0
.end method
