.class public abstract Lh0/e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh0/g3;


# instance fields
.field protected final a:Lh0/c4$d;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lh0/c4$d;

    invoke-direct {v0}, Lh0/c4$d;-><init>()V

    iput-object v0, p0, Lh0/e;->a:Lh0/c4$d;

    return-void
.end method

.method private V()I
    .locals 2

    invoke-interface {p0}, Lh0/g3;->L()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method private W(I)V
    .locals 6

    invoke-interface {p0}, Lh0/g3;->E()I

    move-result v1

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v5, 0x1

    move-object v0, p0

    move v4, p1

    invoke-virtual/range {v0 .. v5}, Lh0/e;->X(IJIZ)V

    return-void
.end method

.method private Y(JI)V
    .locals 6

    invoke-interface {p0}, Lh0/g3;->E()I

    move-result v1

    const/4 v5, 0x0

    move-object v0, p0

    move-wide v2, p1

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lh0/e;->X(IJIZ)V

    return-void
.end method

.method private Z(II)V
    .locals 6

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lh0/e;->X(IJIZ)V

    return-void
.end method

.method private a0(I)V
    .locals 2

    invoke-virtual {p0}, Lh0/e;->e()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    invoke-interface {p0}, Lh0/g3;->E()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, Lh0/e;->W(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0, p1}, Lh0/e;->Z(II)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final B()Z
    .locals 2

    invoke-virtual {p0}, Lh0/e;->e()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final H()Z
    .locals 2

    invoke-virtual {p0}, Lh0/e;->U()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final J()Z
    .locals 3

    invoke-interface {p0}, Lh0/g3;->N()Lh0/c4;

    move-result-object v0

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p0}, Lh0/g3;->E()I

    move-result v1

    iget-object v2, p0, Lh0/e;->a:Lh0/c4$d;

    invoke-virtual {v0, v1, v2}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    move-result-object v0

    iget-boolean v0, v0, Lh0/c4$d;->n:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final P()I
    .locals 1

    invoke-interface {p0}, Lh0/g3;->N()Lh0/c4;

    move-result-object v0

    invoke-virtual {v0}, Lh0/c4;->t()I

    move-result v0

    return v0
.end method

.method public final R(J)V
    .locals 1

    const/4 v0, 0x5

    invoke-direct {p0, p1, p2, v0}, Lh0/e;->Y(JI)V

    return-void
.end method

.method public final T()Z
    .locals 3

    invoke-interface {p0}, Lh0/g3;->N()Lh0/c4;

    move-result-object v0

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p0}, Lh0/g3;->E()I

    move-result v1

    iget-object v2, p0, Lh0/e;->a:Lh0/c4$d;

    invoke-virtual {v0, v1, v2}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    move-result-object v0

    invoke-virtual {v0}, Lh0/c4$d;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final U()I
    .locals 4

    invoke-interface {p0}, Lh0/g3;->N()Lh0/c4;

    move-result-object v0

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Lh0/g3;->E()I

    move-result v1

    invoke-direct {p0}, Lh0/e;->V()I

    move-result v2

    invoke-interface {p0}, Lh0/g3;->Q()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lh0/c4;->p(IIZ)I

    move-result v0

    :goto_0
    return v0
.end method

.method public abstract X(IJIZ)V
.end method

.method public final d()J
    .locals 3

    invoke-interface {p0}, Lh0/g3;->N()Lh0/c4;

    move-result-object v0

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Lh0/g3;->E()I

    move-result v1

    iget-object v2, p0, Lh0/e;->a:Lh0/c4$d;

    invoke-virtual {v0, v1, v2}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    move-result-object v0

    invoke-virtual {v0}, Lh0/c4$d;->f()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public final e()I
    .locals 4

    invoke-interface {p0}, Lh0/g3;->N()Lh0/c4;

    move-result-object v0

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Lh0/g3;->E()I

    move-result v1

    invoke-direct {p0}, Lh0/e;->V()I

    move-result v2

    invoke-interface {p0}, Lh0/g3;->Q()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lh0/c4;->i(IIZ)I

    move-result v0

    :goto_0
    return v0
.end method

.method public final q(IJ)V
    .locals 6

    const/16 v4, 0xa

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    invoke-virtual/range {v0 .. v5}, Lh0/e;->X(IJIZ)V

    return-void
.end method

.method public final t()Z
    .locals 3

    invoke-interface {p0}, Lh0/g3;->N()Lh0/c4;

    move-result-object v0

    invoke-virtual {v0}, Lh0/c4;->u()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p0}, Lh0/g3;->E()I

    move-result v1

    iget-object v2, p0, Lh0/e;->a:Lh0/c4$d;

    invoke-virtual {v0, v1, v2}, Lh0/c4;->r(ILh0/c4$d;)Lh0/c4$d;

    move-result-object v0

    iget-boolean v0, v0, Lh0/c4$d;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final v()V
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lh0/e;->a0(I)V

    return-void
.end method
