.class final Lh0/b1$d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lf2/j;
.implements Lg2/a;
.implements Lh0/k3$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh0/b1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "d"
.end annotation


# instance fields
.field private f:Lf2/j;

.field private g:Lg2/a;

.field private h:Lf2/j;

.field private i:Lg2/a;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lh0/b1$a;)V
    .locals 0

    invoke-direct {p0}, Lh0/b1$d;-><init>()V

    return-void
.end method


# virtual methods
.method public a(J[F)V
    .locals 1

    iget-object v0, p0, Lh0/b1$d;->i:Lg2/a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lg2/a;->a(J[F)V

    :cond_0
    iget-object v0, p0, Lh0/b1$d;->g:Lg2/a;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2, p3}, Lg2/a;->a(J[F)V

    :cond_1
    return-void
.end method

.method public g(JJLh0/r1;Landroid/media/MediaFormat;)V
    .locals 8

    iget-object v0, p0, Lh0/b1$d;->h:Lf2/j;

    if-eqz v0, :cond_0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lf2/j;->g(JJLh0/r1;Landroid/media/MediaFormat;)V

    :cond_0
    iget-object v1, p0, Lh0/b1$d;->f:Lf2/j;

    if-eqz v1, :cond_1

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-interface/range {v1 .. v7}, Lf2/j;->g(JJLh0/r1;Landroid/media/MediaFormat;)V

    :cond_1
    return-void
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lh0/b1$d;->i:Lg2/a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lg2/a;->h()V

    :cond_0
    iget-object v0, p0, Lh0/b1$d;->g:Lg2/a;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lg2/a;->h()V

    :cond_1
    return-void
.end method

.method public q(ILjava/lang/Object;)V
    .locals 1

    const/4 v0, 0x7

    if-eq p1, v0, :cond_3

    const/16 v0, 0x8

    if-eq p1, v0, :cond_2

    const/16 v0, 0x2710

    if-eq p1, v0, :cond_0

    goto :goto_1

    :cond_0
    check-cast p2, Lg2/f;

    if-nez p2, :cond_1

    const/4 p1, 0x0

    iput-object p1, p0, Lh0/b1$d;->h:Lf2/j;

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Lg2/f;->getVideoFrameMetadataListener()Lf2/j;

    move-result-object p1

    iput-object p1, p0, Lh0/b1$d;->h:Lf2/j;

    invoke-virtual {p2}, Lg2/f;->getCameraMotionListener()Lg2/a;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lh0/b1$d;->i:Lg2/a;

    goto :goto_1

    :cond_2
    check-cast p2, Lg2/a;

    iput-object p2, p0, Lh0/b1$d;->g:Lg2/a;

    goto :goto_1

    :cond_3
    check-cast p2, Lf2/j;

    iput-object p2, p0, Lh0/b1$d;->f:Lf2/j;

    :goto_1
    return-void
.end method
