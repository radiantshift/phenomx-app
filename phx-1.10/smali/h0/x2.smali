.class final Lh0/x2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh0/x2$a;,
        Lh0/x2$b;,
        Lh0/x2$c;,
        Lh0/x2$d;
    }
.end annotation


# instance fields
.field private final a:Li0/u1;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lh0/x2$c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/IdentityHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/IdentityHashMap<",
            "Lj1/u;",
            "Lh0/x2$c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Lh0/x2$c;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lh0/x2$d;

.field private final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lh0/x2$c;",
            "Lh0/x2$b;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lh0/x2$c;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Li0/a;

.field private final i:Le2/n;

.field private j:Lj1/s0;

.field private k:Z

.field private l:Ld2/p0;


# direct methods
.method public constructor <init>(Lh0/x2$d;Li0/a;Le2/n;Li0/u1;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p4, p0, Lh0/x2;->a:Li0/u1;

    iput-object p1, p0, Lh0/x2;->e:Lh0/x2$d;

    new-instance p1, Lj1/s0$a;

    const/4 p4, 0x0

    invoke-direct {p1, p4}, Lj1/s0$a;-><init>(I)V

    iput-object p1, p0, Lh0/x2;->j:Lj1/s0;

    new-instance p1, Ljava/util/IdentityHashMap;

    invoke-direct {p1}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object p1, p0, Lh0/x2;->c:Ljava/util/IdentityHashMap;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lh0/x2;->d:Ljava/util/Map;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lh0/x2;->b:Ljava/util/List;

    iput-object p2, p0, Lh0/x2;->h:Li0/a;

    iput-object p3, p0, Lh0/x2;->i:Le2/n;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lh0/x2;->f:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lh0/x2;->g:Ljava/util/Set;

    return-void
.end method

.method private B(II)V
    .locals 4

    const/4 v0, 0x1

    sub-int/2addr p2, v0

    :goto_0
    if-lt p2, p1, :cond_1

    iget-object v1, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lh0/x2$c;

    iget-object v2, p0, Lh0/x2;->d:Ljava/util/Map;

    iget-object v3, v1, Lh0/x2$c;->b:Ljava/lang/Object;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lh0/x2$c;->a:Lj1/s;

    invoke-virtual {v2}, Lj1/s;->c0()Lh0/c4;

    move-result-object v2

    invoke-virtual {v2}, Lh0/c4;->t()I

    move-result v2

    neg-int v2, v2

    invoke-direct {p0, p2, v2}, Lh0/x2;->g(II)V

    iput-boolean v0, v1, Lh0/x2$c;->e:Z

    iget-boolean v2, p0, Lh0/x2;->k:Z

    if-eqz v2, :cond_0

    invoke-direct {p0, v1}, Lh0/x2;->u(Lh0/x2$c;)V

    :cond_0
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static synthetic a(Lh0/x2;Lj1/x;Lh0/c4;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lh0/x2;->t(Lj1/x;Lh0/c4;)V

    return-void
.end method

.method static synthetic b(Lh0/x2;)Le2/n;
    .locals 0

    iget-object p0, p0, Lh0/x2;->i:Le2/n;

    return-object p0
.end method

.method static synthetic c(Lh0/x2$c;Lj1/x$b;)Lj1/x$b;
    .locals 0

    invoke-static {p0, p1}, Lh0/x2;->n(Lh0/x2$c;Lj1/x$b;)Lj1/x$b;

    move-result-object p0

    return-object p0
.end method

.method static synthetic d(Lh0/x2$c;I)I
    .locals 0

    invoke-static {p0, p1}, Lh0/x2;->r(Lh0/x2$c;I)I

    move-result p0

    return p0
.end method

.method static synthetic e(Lh0/x2;)Li0/a;
    .locals 0

    iget-object p0, p0, Lh0/x2;->h:Li0/a;

    return-object p0
.end method

.method private g(II)V
    .locals 2

    :goto_0
    iget-object v0, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/x2$c;

    iget v1, v0, Lh0/x2$c;->d:I

    add-int/2addr v1, p2

    iput v1, v0, Lh0/x2$c;->d:I

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private j(Lh0/x2$c;)V
    .locals 1

    iget-object v0, p0, Lh0/x2;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh0/x2$b;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lh0/x2$b;->a:Lj1/x;

    iget-object p1, p1, Lh0/x2$b;->b:Lj1/x$c;

    invoke-interface {v0, p1}, Lj1/x;->c(Lj1/x$c;)V

    :cond_0
    return-void
.end method

.method private k()V
    .locals 3

    iget-object v0, p0, Lh0/x2;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lh0/x2$c;

    iget-object v2, v1, Lh0/x2$c;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v1}, Lh0/x2;->j(Lh0/x2$c;)V

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private l(Lh0/x2$c;)V
    .locals 1

    iget-object v0, p0, Lh0/x2;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lh0/x2;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh0/x2$b;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lh0/x2$b;->a:Lj1/x;

    iget-object p1, p1, Lh0/x2$b;->b:Lj1/x$c;

    invoke-interface {v0, p1}, Lj1/x;->n(Lj1/x$c;)V

    :cond_0
    return-void
.end method

.method private static m(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0}, Lh0/a;->z(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private static n(Lh0/x2$c;Lj1/x$b;)Lj1/x$b;
    .locals 6

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lh0/x2$c;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lh0/x2$c;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/x$b;

    iget-wide v1, v1, Lj1/v;->d:J

    iget-wide v3, p1, Lj1/v;->d:J

    cmp-long v5, v1, v3

    if-nez v5, :cond_0

    iget-object v0, p1, Lj1/v;->a:Ljava/lang/Object;

    invoke-static {p0, v0}, Lh0/x2;->p(Lh0/x2$c;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {p1, p0}, Lj1/x$b;->c(Ljava/lang/Object;)Lj1/x$b;

    move-result-object p0

    return-object p0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private static o(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0}, Lh0/a;->A(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private static p(Lh0/x2$c;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lh0/x2$c;->b:Ljava/lang/Object;

    invoke-static {p0, p1}, Lh0/a;->C(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private static r(Lh0/x2$c;I)I
    .locals 0

    iget p0, p0, Lh0/x2$c;->d:I

    add-int/2addr p1, p0

    return p1
.end method

.method private synthetic t(Lj1/x;Lh0/c4;)V
    .locals 0

    iget-object p1, p0, Lh0/x2;->e:Lh0/x2$d;

    invoke-interface {p1}, Lh0/x2$d;->a()V

    return-void
.end method

.method private u(Lh0/x2$c;)V
    .locals 3

    iget-boolean v0, p1, Lh0/x2$c;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lh0/x2$c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh0/x2;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/x2$b;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/x2$b;

    iget-object v1, v0, Lh0/x2$b;->a:Lj1/x;

    iget-object v2, v0, Lh0/x2$b;->b:Lj1/x$c;

    invoke-interface {v1, v2}, Lj1/x;->k(Lj1/x$c;)V

    iget-object v1, v0, Lh0/x2$b;->a:Lj1/x;

    iget-object v2, v0, Lh0/x2$b;->c:Lh0/x2$a;

    invoke-interface {v1, v2}, Lj1/x;->p(Lj1/e0;)V

    iget-object v1, v0, Lh0/x2$b;->a:Lj1/x;

    iget-object v0, v0, Lh0/x2$b;->c:Lh0/x2$a;

    invoke-interface {v1, v0}, Lj1/x;->o(Ll0/w;)V

    iget-object v0, p0, Lh0/x2;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private x(Lh0/x2$c;)V
    .locals 5

    iget-object v0, p1, Lh0/x2$c;->a:Lj1/s;

    new-instance v1, Lh0/k2;

    invoke-direct {v1, p0}, Lh0/k2;-><init>(Lh0/x2;)V

    new-instance v2, Lh0/x2$a;

    invoke-direct {v2, p0, p1}, Lh0/x2$a;-><init>(Lh0/x2;Lh0/x2$c;)V

    iget-object v3, p0, Lh0/x2;->f:Ljava/util/HashMap;

    new-instance v4, Lh0/x2$b;

    invoke-direct {v4, v0, v1, v2}, Lh0/x2$b;-><init>(Lj1/x;Lj1/x$c;Lh0/x2$a;)V

    invoke-virtual {v3, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Le2/n0;->y()Landroid/os/Handler;

    move-result-object p1

    invoke-interface {v0, p1, v2}, Lj1/x;->f(Landroid/os/Handler;Lj1/e0;)V

    invoke-static {}, Le2/n0;->y()Landroid/os/Handler;

    move-result-object p1

    invoke-interface {v0, p1, v2}, Lj1/x;->b(Landroid/os/Handler;Ll0/w;)V

    iget-object p1, p0, Lh0/x2;->l:Ld2/p0;

    iget-object v2, p0, Lh0/x2;->a:Li0/u1;

    invoke-interface {v0, v1, p1, v2}, Lj1/x;->q(Lj1/x$c;Ld2/p0;Li0/u1;)V

    return-void
.end method


# virtual methods
.method public A(IILj1/s0;)Lh0/c4;
    .locals 1

    if-ltz p1, :cond_0

    if-gt p1, p2, :cond_0

    invoke-virtual {p0}, Lh0/x2;->q()I

    move-result v0

    if-gt p2, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Le2/a;->a(Z)V

    iput-object p3, p0, Lh0/x2;->j:Lj1/s0;

    invoke-direct {p0, p1, p2}, Lh0/x2;->B(II)V

    invoke-virtual {p0}, Lh0/x2;->i()Lh0/c4;

    move-result-object p1

    return-object p1
.end method

.method public C(Ljava/util/List;Lj1/s0;)Lh0/c4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lh0/x2$c;",
            ">;",
            "Lj1/s0;",
            ")",
            "Lh0/c4;"
        }
    .end annotation

    iget-object v0, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lh0/x2;->B(II)V

    iget-object v0, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1, p2}, Lh0/x2;->f(ILjava/util/List;Lj1/s0;)Lh0/c4;

    move-result-object p1

    return-object p1
.end method

.method public D(Lj1/s0;)Lh0/c4;
    .locals 2

    invoke-virtual {p0}, Lh0/x2;->q()I

    move-result v0

    invoke-interface {p1}, Lj1/s0;->a()I

    move-result v1

    if-eq v1, v0, :cond_0

    invoke-interface {p1}, Lj1/s0;->h()Lj1/s0;

    move-result-object p1

    const/4 v1, 0x0

    invoke-interface {p1, v1, v0}, Lj1/s0;->d(II)Lj1/s0;

    move-result-object p1

    :cond_0
    iput-object p1, p0, Lh0/x2;->j:Lj1/s0;

    invoke-virtual {p0}, Lh0/x2;->i()Lh0/c4;

    move-result-object p1

    return-object p1
.end method

.method public f(ILjava/util/List;Lj1/s0;)Lh0/c4;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lh0/x2$c;",
            ">;",
            "Lj1/s0;",
            ")",
            "Lh0/c4;"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iput-object p3, p0, Lh0/x2;->j:Lj1/s0;

    move p3, p1

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, p1

    if-ge p3, v0, :cond_3

    sub-int v0, p3, p1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/x2$c;

    if-lez p3, :cond_0

    iget-object v1, p0, Lh0/x2;->b:Ljava/util/List;

    add-int/lit8 v2, p3, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lh0/x2$c;

    iget-object v2, v1, Lh0/x2$c;->a:Lj1/s;

    invoke-virtual {v2}, Lj1/s;->c0()Lh0/c4;

    move-result-object v2

    iget v1, v1, Lh0/x2$c;->d:I

    invoke-virtual {v2}, Lh0/c4;->t()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Lh0/x2$c;->c(I)V

    iget-object v1, v0, Lh0/x2$c;->a:Lj1/s;

    invoke-virtual {v1}, Lj1/s;->c0()Lh0/c4;

    move-result-object v1

    invoke-virtual {v1}, Lh0/c4;->t()I

    move-result v1

    invoke-direct {p0, p3, v1}, Lh0/x2;->g(II)V

    iget-object v1, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v1, p3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lh0/x2;->d:Ljava/util/Map;

    iget-object v2, v0, Lh0/x2$c;->b:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v1, p0, Lh0/x2;->k:Z

    if-eqz v1, :cond_2

    invoke-direct {p0, v0}, Lh0/x2;->x(Lh0/x2$c;)V

    iget-object v1, p0, Lh0/x2;->c:Ljava/util/IdentityHashMap;

    invoke-virtual {v1}, Ljava/util/IdentityHashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lh0/x2;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    invoke-direct {p0, v0}, Lh0/x2;->j(Lh0/x2$c;)V

    :cond_2
    :goto_2
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lh0/x2;->i()Lh0/c4;

    move-result-object p1

    return-object p1
.end method

.method public h(Lj1/x$b;Ld2/b;J)Lj1/u;
    .locals 2

    iget-object v0, p1, Lj1/v;->a:Ljava/lang/Object;

    invoke-static {v0}, Lh0/x2;->o(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p1, Lj1/v;->a:Ljava/lang/Object;

    invoke-static {v1}, Lh0/x2;->m(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Lj1/x$b;->c(Ljava/lang/Object;)Lj1/x$b;

    move-result-object p1

    iget-object v1, p0, Lh0/x2;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/x2$c;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/x2$c;

    invoke-direct {p0, v0}, Lh0/x2;->l(Lh0/x2$c;)V

    iget-object v1, v0, Lh0/x2$c;->c:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lh0/x2$c;->a:Lj1/s;

    invoke-virtual {v1, p1, p2, p3, p4}, Lj1/s;->Z(Lj1/x$b;Ld2/b;J)Lj1/r;

    move-result-object p1

    iget-object p2, p0, Lh0/x2;->c:Ljava/util/IdentityHashMap;

    invoke-virtual {p2, p1, v0}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lh0/x2;->k()V

    return-object p1
.end method

.method public i()Lh0/c4;
    .locals 3

    iget-object v0, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lh0/c4;->f:Lh0/c4;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lh0/x2$c;

    iput v1, v2, Lh0/x2$c;->d:I

    iget-object v2, v2, Lh0/x2$c;->a:Lj1/s;

    invoke-virtual {v2}, Lj1/s;->c0()Lh0/c4;

    move-result-object v2

    invoke-virtual {v2}, Lh0/c4;->t()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Lh0/l3;

    iget-object v1, p0, Lh0/x2;->b:Ljava/util/List;

    iget-object v2, p0, Lh0/x2;->j:Lj1/s0;

    invoke-direct {v0, v1, v2}, Lh0/l3;-><init>(Ljava/util/Collection;Lj1/s0;)V

    return-object v0
.end method

.method public q()I
    .locals 1

    iget-object v0, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public s()Z
    .locals 1

    iget-boolean v0, p0, Lh0/x2;->k:Z

    return v0
.end method

.method public v(IIILj1/s0;)Lh0/c4;
    .locals 3

    const/4 v0, 0x1

    if-ltz p1, :cond_0

    if-gt p1, p2, :cond_0

    invoke-virtual {p0}, Lh0/x2;->q()I

    move-result v1

    if-gt p2, v1, :cond_0

    if-ltz p3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Le2/a;->a(Z)V

    iput-object p4, p0, Lh0/x2;->j:Lj1/s0;

    if-eq p1, p2, :cond_3

    if-ne p1, p3, :cond_1

    goto :goto_2

    :cond_1
    invoke-static {p1, p3}, Ljava/lang/Math;->min(II)I

    move-result p4

    sub-int v1, p2, p1

    add-int/2addr v1, p3

    sub-int/2addr v1, v0

    add-int/lit8 v0, p2, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lh0/x2$c;

    iget v1, v1, Lh0/x2$c;->d:I

    iget-object v2, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-static {v2, p1, p2, p3}, Le2/n0;->B0(Ljava/util/List;III)V

    :goto_1
    if-gt p4, v0, :cond_2

    iget-object p1, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh0/x2$c;

    iput v1, p1, Lh0/x2$c;->d:I

    iget-object p1, p1, Lh0/x2$c;->a:Lj1/s;

    invoke-virtual {p1}, Lj1/s;->c0()Lh0/c4;

    move-result-object p1

    invoke-virtual {p1}, Lh0/c4;->t()I

    move-result p1

    add-int/2addr v1, p1

    add-int/lit8 p4, p4, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lh0/x2;->i()Lh0/c4;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_2
    invoke-virtual {p0}, Lh0/x2;->i()Lh0/c4;

    move-result-object p1

    return-object p1
.end method

.method public w(Ld2/p0;)V
    .locals 3

    iget-boolean v0, p0, Lh0/x2;->k:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-static {v0}, Le2/a;->f(Z)V

    iput-object p1, p0, Lh0/x2;->l:Ld2/p0;

    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lh0/x2;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/x2$c;

    invoke-direct {p0, v0}, Lh0/x2;->x(Lh0/x2$c;)V

    iget-object v2, p0, Lh0/x2;->g:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    iput-boolean v1, p0, Lh0/x2;->k:Z

    return-void
.end method

.method public y()V
    .locals 5

    iget-object v0, p0, Lh0/x2;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lh0/x2$b;

    :try_start_0
    iget-object v2, v1, Lh0/x2$b;->a:Lj1/x;

    iget-object v3, v1, Lh0/x2$b;->b:Lj1/x$c;

    invoke-interface {v2, v3}, Lj1/x;->k(Lj1/x$c;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v3, "MediaSourceList"

    const-string v4, "Failed to release child source."

    invoke-static {v3, v4, v2}, Le2/r;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    iget-object v2, v1, Lh0/x2$b;->a:Lj1/x;

    iget-object v3, v1, Lh0/x2$b;->c:Lh0/x2$a;

    invoke-interface {v2, v3}, Lj1/x;->p(Lj1/e0;)V

    iget-object v2, v1, Lh0/x2$b;->a:Lj1/x;

    iget-object v1, v1, Lh0/x2$b;->c:Lh0/x2$a;

    invoke-interface {v2, v1}, Lj1/x;->o(Ll0/w;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lh0/x2;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lh0/x2;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lh0/x2;->k:Z

    return-void
.end method

.method public z(Lj1/u;)V
    .locals 2

    iget-object v0, p0, Lh0/x2;->c:Ljava/util/IdentityHashMap;

    invoke-virtual {v0, p1}, Ljava/util/IdentityHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/x2$c;

    invoke-static {v0}, Le2/a;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lh0/x2$c;

    iget-object v1, v0, Lh0/x2$c;->a:Lj1/s;

    invoke-virtual {v1, p1}, Lj1/s;->i(Lj1/u;)V

    iget-object v1, v0, Lh0/x2$c;->c:Ljava/util/List;

    check-cast p1, Lj1/r;

    iget-object p1, p1, Lj1/r;->f:Lj1/x$b;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object p1, p0, Lh0/x2;->c:Ljava/util/IdentityHashMap;

    invoke-virtual {p1}, Ljava/util/IdentityHashMap;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-direct {p0}, Lh0/x2;->k()V

    :cond_0
    invoke-direct {p0, v0}, Lh0/x2;->u(Lh0/x2$c;)V

    return-void
.end method
