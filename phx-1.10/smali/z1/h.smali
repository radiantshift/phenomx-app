.class final Lz1/h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ls1/h;


# instance fields
.field private final f:Lz1/d;

.field private final g:[J

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lz1/g;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lz1/e;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lz1/d;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lz1/d;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lz1/g;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lz1/e;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lz1/h;->f:Lz1/d;

    iput-object p3, p0, Lz1/h;->i:Ljava/util/Map;

    iput-object p4, p0, Lz1/h;->j:Ljava/util/Map;

    if-eqz p2, :cond_0

    invoke-static {p2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p2

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p2

    :goto_0
    iput-object p2, p0, Lz1/h;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lz1/d;->j()[J

    move-result-object p1

    iput-object p1, p0, Lz1/h;->g:[J

    return-void
.end method


# virtual methods
.method public a(J)I
    .locals 2

    iget-object v0, p0, Lz1/h;->g:[J

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1, v1}, Le2/n0;->e([JJZZ)I

    move-result p1

    iget-object p2, p0, Lz1/h;->g:[J

    array-length p2, p2

    if-ge p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method public b(I)J
    .locals 3

    iget-object v0, p0, Lz1/h;->g:[J

    aget-wide v1, v0, p1

    return-wide v1
.end method

.method public c(J)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List<",
            "Ls1/b;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lz1/h;->f:Lz1/d;

    iget-object v3, p0, Lz1/h;->h:Ljava/util/Map;

    iget-object v4, p0, Lz1/h;->i:Ljava/util/Map;

    iget-object v5, p0, Lz1/h;->j:Ljava/util/Map;

    move-wide v1, p1

    invoke-virtual/range {v0 .. v5}, Lz1/d;->h(JLjava/util/Map;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lz1/h;->g:[J

    array-length v0, v0

    return v0
.end method
