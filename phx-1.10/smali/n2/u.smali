.class final Ln2/u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ln2/f;
.implements Ln2/e;
.implements Ln2/c;
.implements Ln2/f0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TResult:",
        "Ljava/lang/Object;",
        "TContinuationResult:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ln2/f<",
        "TTContinuationResult;>;",
        "Ln2/e;",
        "Ln2/c;",
        "Ln2/f0;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final b:Ln2/a;

.field private final c:Ln2/j0;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ln2/a;Ln2/j0;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ln2/u;->a:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Ln2/u;->b:Ln2/a;

    iput-object p3, p0, Ln2/u;->c:Ln2/j0;

    return-void
.end method

.method static bridge synthetic e(Ln2/u;)Ln2/a;
    .locals 0

    iget-object p0, p0, Ln2/u;->b:Ln2/a;

    return-object p0
.end method

.method static bridge synthetic f(Ln2/u;)Ln2/j0;
    .locals 0

    iget-object p0, p0, Ln2/u;->c:Ln2/j0;

    return-object p0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Ln2/u;->c:Ln2/j0;

    invoke-virtual {v0}, Ln2/j0;->q()Z

    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTContinuationResult;)V"
        }
    .end annotation

    iget-object v0, p0, Ln2/u;->c:Ln2/j0;

    invoke-virtual {v0, p1}, Ln2/j0;->p(Ljava/lang/Object;)V

    return-void
.end method

.method public final c(Ln2/i;)V
    .locals 2

    iget-object v0, p0, Ln2/u;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Ln2/t;

    invoke-direct {v1, p0, p1}, Ln2/t;-><init>(Ln2/u;Ln2/i;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final d(Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Ln2/u;->c:Ln2/j0;

    invoke-virtual {v0, p1}, Ln2/j0;->o(Ljava/lang/Exception;)V

    return-void
.end method
