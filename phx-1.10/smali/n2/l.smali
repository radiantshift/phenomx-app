.class public final Ln2/l;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(Ln2/i;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TResult:",
            "Ljava/lang/Object;",
            ">(",
            "Ln2/i<",
            "TTResult;>;)TTResult;"
        }
    .end annotation

    invoke-static {}, Lj2/b;->d()V

    const-string v0, "Task must not be null"

    invoke-static {p0, v0}, Lj2/b;->g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Ln2/i;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Ln2/l;->i(Ln2/i;)Ljava/lang/Object;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance v0, Ln2/o;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ln2/o;-><init>(Ln2/n;)V

    invoke-static {p0, v0}, Ln2/l;->j(Ln2/i;Ln2/p;)V

    invoke-virtual {v0}, Ln2/o;->c()V

    invoke-static {p0}, Ln2/l;->i(Ln2/i;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Ln2/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TResult:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Callable<",
            "TTResult;>;)",
            "Ln2/i<",
            "TTResult;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, "Executor must not be null"

    invoke-static {p0, v0}, Lj2/b;->g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Callback must not be null"

    invoke-static {p1, v0}, Lj2/b;->g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ln2/j0;

    invoke-direct {v0}, Ln2/j0;-><init>()V

    new-instance v1, Ln2/k0;

    invoke-direct {v1, v0, p1}, Ln2/k0;-><init>(Ln2/j0;Ljava/util/concurrent/Callable;)V

    invoke-interface {p0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-object v0
.end method

.method public static c(Ljava/lang/Exception;)Ln2/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TResult:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Exception;",
            ")",
            "Ln2/i<",
            "TTResult;>;"
        }
    .end annotation

    new-instance v0, Ln2/j0;

    invoke-direct {v0}, Ln2/j0;-><init>()V

    invoke-virtual {v0, p0}, Ln2/j0;->o(Ljava/lang/Exception;)V

    return-object v0
.end method

.method public static d(Ljava/lang/Object;)Ln2/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TResult:",
            "Ljava/lang/Object;",
            ">(TTResult;)",
            "Ln2/i<",
            "TTResult;>;"
        }
    .end annotation

    new-instance v0, Ln2/j0;

    invoke-direct {v0}, Ln2/j0;-><init>()V

    invoke-virtual {v0, p0}, Ln2/j0;->p(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static e(Ljava/util/Collection;)Ln2/i;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ln2/i<",
            "*>;>;)",
            "Ln2/i<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_3

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ln2/i;

    const-string v2, "null tasks are not accepted"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    new-instance v0, Ln2/j0;

    invoke-direct {v0}, Ln2/j0;-><init>()V

    new-instance v1, Ln2/q;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {v1, v2, v0}, Ln2/q;-><init>(ILn2/j0;)V

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ln2/i;

    invoke-static {v2, v1}, Ln2/l;->j(Ln2/i;Ln2/p;)V

    goto :goto_1

    :cond_2
    return-object v0

    :cond_3
    :goto_2
    const/4 p0, 0x0

    invoke-static {p0}, Ln2/l;->d(Ljava/lang/Object;)Ln2/i;

    move-result-object p0

    return-object p0
.end method

.method public static varargs f([Ln2/i;)Ln2/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ln2/i<",
            "*>;)",
            "Ln2/i<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_1

    array-length v0, p0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, Ln2/l;->e(Ljava/util/Collection;)Ln2/i;

    move-result-object p0

    return-object p0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    invoke-static {p0}, Ln2/l;->d(Ljava/lang/Object;)Ln2/i;

    move-result-object p0

    return-object p0
.end method

.method public static g(Ljava/util/Collection;)Ln2/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Ln2/i<",
            "*>;>;)",
            "Ln2/i<",
            "Ljava/util/List<",
            "Ln2/i<",
            "*>;>;>;"
        }
    .end annotation

    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Ln2/l;->e(Ljava/util/Collection;)Ln2/i;

    move-result-object v0

    new-instance v1, Ln2/m;

    invoke-direct {v1, p0}, Ln2/m;-><init>(Ljava/util/Collection;)V

    sget-object p0, Ln2/k;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, p0, v1}, Ln2/i;->g(Ljava/util/concurrent/Executor;Ln2/a;)Ln2/i;

    move-result-object p0

    return-object p0

    :cond_1
    :goto_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, Ln2/l;->d(Ljava/lang/Object;)Ln2/i;

    move-result-object p0

    return-object p0
.end method

.method public static varargs h([Ln2/i;)Ln2/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ln2/i<",
            "*>;)",
            "Ln2/i<",
            "Ljava/util/List<",
            "Ln2/i<",
            "*>;>;>;"
        }
    .end annotation

    if-eqz p0, :cond_1

    array-length v0, p0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, Ln2/l;->g(Ljava/util/Collection;)Ln2/i;

    move-result-object p0

    return-object p0

    :cond_1
    :goto_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, Ln2/l;->d(Ljava/lang/Object;)Ln2/i;

    move-result-object p0

    return-object p0
.end method

.method private static i(Ln2/i;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Ln2/i;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ln2/i;->i()Ljava/lang/Object;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-virtual {p0}, Ln2/i;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p0, Ljava/util/concurrent/CancellationException;

    const-string v0, "Task is already canceled"

    invoke-direct {p0, v0}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    new-instance v0, Ljava/util/concurrent/ExecutionException;

    invoke-virtual {p0}, Ln2/i;->h()Ljava/lang/Exception;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static j(Ln2/i;Ln2/p;)V
    .locals 1

    sget-object v0, Ln2/k;->b:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Ln2/i;->d(Ljava/util/concurrent/Executor;Ln2/f;)Ln2/i;

    invoke-virtual {p0, v0, p1}, Ln2/i;->c(Ljava/util/concurrent/Executor;Ln2/e;)Ln2/i;

    invoke-virtual {p0, v0, p1}, Ln2/i;->a(Ljava/util/concurrent/Executor;Ln2/c;)Ln2/i;

    return-void
.end method
