.class public Lkotlinx/coroutines/internal/c0;
.super Lb6/a;
.source ""

# interfaces
.implements Lkotlin/coroutines/jvm/internal/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lb6/a<",
        "TT;>;",
        "Lkotlin/coroutines/jvm/internal/e;"
    }
.end annotation


# instance fields
.field public final h:Ln5/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln5/d<",
            "TT;>;"
        }
    .end annotation
.end field


# virtual methods
.method protected A0(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lkotlinx/coroutines/internal/c0;->h:Ln5/d;

    invoke-static {p1, v0}, Lb6/z;->a(Ljava/lang/Object;Ln5/d;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v0, p1}, Ln5/d;->resumeWith(Ljava/lang/Object;)V

    return-void
.end method

.method protected E(Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lkotlinx/coroutines/internal/c0;->h:Ln5/d;

    invoke-static {v0}, Lo5/b;->b(Ln5/d;)Ln5/d;

    move-result-object v0

    iget-object v1, p0, Lkotlinx/coroutines/internal/c0;->h:Ln5/d;

    invoke-static {p1, v1}, Lb6/z;->a(Ljava/lang/Object;Ln5/d;)Ljava/lang/Object;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lkotlinx/coroutines/internal/i;->c(Ln5/d;Ljava/lang/Object;Lu5/l;ILjava/lang/Object;)V

    return-void
.end method

.method public final E0()Lb6/q1;
    .locals 1

    invoke-virtual {p0}, Lb6/x1;->V()Lb6/q;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lb6/q;->getParent()Lb6/q1;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method protected final b0()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final getCallerFrame()Lkotlin/coroutines/jvm/internal/e;
    .locals 2

    iget-object v0, p0, Lkotlinx/coroutines/internal/c0;->h:Ln5/d;

    instance-of v1, v0, Lkotlin/coroutines/jvm/internal/e;

    if-eqz v1, :cond_0

    check-cast v0, Lkotlin/coroutines/jvm/internal/e;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getStackTraceElement()Ljava/lang/StackTraceElement;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
