.class public final Lw0/v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lw0/b0;


# instance fields
.field private a:Lh0/r1;

.field private b:Le2/j0;

.field private c:Lm0/e0;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lh0/r1$b;

    invoke-direct {v0}, Lh0/r1$b;-><init>()V

    invoke-virtual {v0, p1}, Lh0/r1$b;->g0(Ljava/lang/String;)Lh0/r1$b;

    move-result-object p1

    invoke-virtual {p1}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object p1

    iput-object p1, p0, Lw0/v;->a:Lh0/r1;

    return-void
.end method

.method private a()V
    .locals 1
    .annotation runtime Lorg/checkerframework/checker/nullness/qual/EnsuresNonNull;
        value = {
            "timestampAdjuster",
            "output"
        }
    .end annotation

    iget-object v0, p0, Lw0/v;->b:Le2/j0;

    invoke-static {v0}, Le2/a;->h(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lw0/v;->c:Lm0/e0;

    invoke-static {v0}, Le2/n0;->j(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public b(Le2/a0;)V
    .locals 8

    invoke-direct {p0}, Lw0/v;->a()V

    iget-object v0, p0, Lw0/v;->b:Le2/j0;

    invoke-virtual {v0}, Le2/j0;->d()J

    move-result-wide v2

    iget-object v0, p0, Lw0/v;->b:Le2/j0;

    invoke-virtual {v0}, Le2/j0;->e()J

    move-result-wide v0

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v6, v2, v4

    if-eqz v6, :cond_2

    cmp-long v6, v0, v4

    if-nez v6, :cond_0

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lw0/v;->a:Lh0/r1;

    iget-wide v5, v4, Lh0/r1;->u:J

    cmp-long v7, v0, v5

    if-eqz v7, :cond_1

    invoke-virtual {v4}, Lh0/r1;->b()Lh0/r1$b;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lh0/r1$b;->k0(J)Lh0/r1$b;

    move-result-object v0

    invoke-virtual {v0}, Lh0/r1$b;->G()Lh0/r1;

    move-result-object v0

    iput-object v0, p0, Lw0/v;->a:Lh0/r1;

    iget-object v1, p0, Lw0/v;->c:Lm0/e0;

    invoke-interface {v1, v0}, Lm0/e0;->e(Lh0/r1;)V

    :cond_1
    invoke-virtual {p1}, Le2/a0;->a()I

    move-result v5

    iget-object v0, p0, Lw0/v;->c:Lm0/e0;

    invoke-interface {v0, p1, v5}, Lm0/e0;->a(Le2/a0;I)V

    iget-object v1, p0, Lw0/v;->c:Lm0/e0;

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lm0/e0;->f(JIIILm0/e0$a;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public c(Le2/j0;Lm0/n;Lw0/i0$d;)V
    .locals 0

    iput-object p1, p0, Lw0/v;->b:Le2/j0;

    invoke-virtual {p3}, Lw0/i0$d;->a()V

    invoke-virtual {p3}, Lw0/i0$d;->c()I

    move-result p1

    const/4 p3, 0x5

    invoke-interface {p2, p1, p3}, Lm0/n;->e(II)Lm0/e0;

    move-result-object p1

    iput-object p1, p0, Lw0/v;->c:Lm0/e0;

    iget-object p2, p0, Lw0/v;->a:Lh0/r1;

    invoke-interface {p1, p2}, Lm0/e0;->e(Lh0/r1;)V

    return-void
.end method
