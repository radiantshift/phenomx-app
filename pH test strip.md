
Phenomx app is capable of reading any type of color indicator based strip, such as  pH paper strip.

- [color chart](obsidian://open?vault=technology&file=test%2FpH-strip_2022-11-19_23-45-54.jpg)

Method:  
 - DFFT + filter + reverse DFFT to allow "zoom" robust algo
 - scale down 1D using Hilbert "curve" sampling
 - auto white balance
 - feed sample to a [ML][1] network for pH value extraction

[1]: https://inkforall.com/ai-writing-tools/ai-content-marketing/ai-content-marketing-strategy/ai-words/ai-model/