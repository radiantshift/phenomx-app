## Phenomx Health App

Menopause Nutrition App concept wireframes:

![[phenomx-app/drawings/2022-11-19 19.01 PhX SLC wireframe|2022-11-19 19.01 PhX SLC wireframe]]


# SLC version 1.10

# here is the extract of the app apk

- <https://gateway.ipfs.io/ipfs/f01701114a562bb1e874be980d4b293e83f89abef4d087938/>
- <https://ipfs.safewatch.care/ipfs/f01701114a562bb1e874be980d4b293e83f89abef4d087938/>


[1]: https://phenomx.netlify.com

