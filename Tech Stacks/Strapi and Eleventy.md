
## qmarks (FoxyTab): 
How Paradigma Digital switched from Wordpress to Strapi and Eleventy.
https://strapi.io/user-stories/paradigma-digital-brand

API Documentation - Strapi Developer Docs
https://docs.strapi.io/developer-docs/latest/plugins/documentation.html#installation

API Documentation Made Easy - Get Started | Swagger
https://swagger.io/solutions/api-documentation/

Documenting Your Existing APIs: API Documentation Made Easy with OpenAPI & Swagger
https://swagger.io/resources/articles/documenting-apis-with-swagger/

About Swagger Specification | Documentation | Swagger
https://swagger.io/docs/specification/about/

swagger-api/swagger-codegen: swagger-codegen contains a template-driven engine to generate documentation, API clients and server stubs in different languages by parsing your OpenAPI / Swagger definition.
https://github.com/swagger-api/swagger-codegen

Inspector
https://inspector.swagger.io/builder?url=https%3A%2F%2Fswapi.co%2Fapi%2Fpeople


