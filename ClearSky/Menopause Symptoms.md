__Physiologic symptoms__:

  **Brain health**:
  - Mood swings
	  - Moody
  - Depression 
  - Anxiety and/or panic attacks
  - Headaches
  - Brain fog 
  - Decreased memory
	  - Forgetful
	- Low libido
	  - No libido
		- Do not want to have sex

  **Gastrointestinal health**:
  - Bloating
  - Gas
	  - Stomach pain
  - Constipation
  - Diarrhea during menstruation
  - Dry mouth

  **Menstrual health**:
  - Sore breasts
  - Menstrual crampss
  - Vaginal dryness

  **Climacteric**:
  - Hot flashes
  - Night sweats
  - Cold flashes
  - Body odor
  - Heart palpitations

  Fatigue:
	- Tired
	- Low energy
	
  Sleep issues:
  - Sleep difficulties
	  - Cannot go to sleep
	  - Cannot stay asleep
	  - Wake up too early
	  - Cannot get back to sleep

  **Metabolic health**:
  - Weight gain
	  - Meno belly
	  - Abdominal weight gain
	  - Weight gain in mid section
	- High blood sugar

  Heart health:
  - Heart palpitations
	  - Fast heart rate

  Bone health:
  - Joint pain
	  - Bones hurt
	  - Body hurts

  Immune health:
  - Sick (frequently)
  - colds
  - Urinary tract infections

  Energy:
  - Low energy
  - fatigue

  Bladder health:
  - Urinary tract infections
	  - UTI
	  - Difficulty peeing
	  - Cannot pee
  - Bladder pain
	  - Bladder uncomfortable
  - Frequent urination
	  - Peeing alot
  - Incontinence

**Aesthetic symptoms**:
  Skin health:
  - Loss of skin elasticity
  - Skin sagging…Skin wrinkling
  - Age spots/melasma
  - Dry skin
	  - Itchy skin
  - Hair loss
  - Hair thinning
  - Change of hair texture
  - Skin rash
  - Atopic dermatitis
  - Eczema

![[symptoms.yml]]
```
Sore breasts
Bloating
Gas
Constipation
Menstrual cramps
Night sweats
Hot flashes
Cold flashes
Body odor
Mood swings
Depression
Headaches
Brain fog
Anxiety and/or panic attacks
Heart palpitations
Sleep difficulties
Joint pain
Low energy, fatigue
Weight gain
Frequent sickness - colds,…
Urinary tract infections
Bladder pain, frequent urin…
Incontinence
Loss of skin elasticity, sag…
Skin wrinkling
Age spots, melasma
Dry skin
Dry mouth
Vaginal dryness
Decreased memory
Hair loss, thinning, change…
None of the above
Increased skin issues(sem…
Diarrhea during menstrual…
skin rash and atopic derm…
