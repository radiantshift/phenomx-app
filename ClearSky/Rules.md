## clearSky AI RulesSet

In attachment I put a template for our rules. In brief:
every rule can match multiple words or combination of words. salutation: hello | hi  matches either hello or hi
you can combine rules with the prefix \$ or ?.
The dollar symbol is for mandatory matching, while the question mark for optional matching.
So for instance the rule $extremely bad  match all sentences that contain one alternative from the rule extremely  followed by the word bad, but does not match the word bad alone. ?extremely bad  instead matches also the word bad alone, as ?extremely  is optional
there is a special word <any>, that matches any  word

For completeness: I am adding also support for descriptions: for instance describe(id=eyes-symptoms,words=eyes,adj=watery&itchy)
will create a new rule `eyes-symptoms` that matches watery eyes  , my eyes are watery , my eyes are watery and itchy , etc.
As this is not well tested at the moment, it is not supported in the server yet.
So I would suggest you avoid to use it at the moment

```yaml
rules:
  # modifiers
  extremely: very | really | extremely
  memory: forgetful | forget <any>
  # mood
  awful: like the world is coming to an end | ?extremely terrible | ?extremely bad | ?extremely uncomfortable | ?extremely depressed | ?extremely anxious | ?extremely angry | ?extremely moody
  great: sunny | wonderful | fantastic | the best ever | $extremely excited | ?extremely cheerful 
  eyes-symptom-descr: describe(id=eyes-symptoms,words=eyes,adj=watery&itchy)

intents:
  - eyes-symptoms
  - awful
  - great
  - memory
```
