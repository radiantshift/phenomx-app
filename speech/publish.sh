#

find . -name '*~1' -delete
qm=$(ipfs add -Q -w ex-audio.htm ex-audio.js style.css ClearSky_Logo*_small.svg README.md)
echo qm: $qm
echo url: https://ipfs.safewatch.care/ipfs/$qm/ex-audio.htm
echo ipfs name publish --key=clearSpeech $qm
# QmXkByihhYiTezQXmUFs2zmXCxGo268naRgeatTZiHo1Rd
key=$(ipfs key list -l --ipns-base b58mh | grep -w clearSpeech | cut -d' ' -f1)
echo key: $key
echo url: https://ipfs.safewatch.care/ipns/$key/ex-audio.htm
echo url: http://localhost:8390/ipns/$key/ex-audio.htm
