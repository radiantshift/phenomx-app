  **Brain health**:
  - Mood swings
  - Depression 
  - Anxiety and/or panic attacks
  - Headaches
  - Brain fog 
  - Decreased memory
	  - Forgetful
   - Low libido (Do not want sex)
   - Stress

  **Gastrointestinal health**:
  - Bloating
  - Gas
	  - Stomach pain
  - Constipation
  - Diarrhea during menstruation
  - Dry mouth

  **Menstrual health**:
  - Sore breasts
  - Menstrual crampss
  - Vaginal dryness

  **Climacteric**:
  - Hot flashes
  - Night sweats
  - Cold flashes
  - Body odor
  - Heart palpitations

  **Fatigue:**
	- Tired / low energy
  - Sleep difficulties

  **Metabolic health**:
  - Weight gain
  - Bllood sugar control

  **Bone health:**
  - Joint & bone pain
	
  **Immune health:**
  - Sick (frequently)
  - colds
  - Urinary tract infections

  **Energy:**
  - Low energy
  - fatigue

  **Bladder health:**
  - Urinary tract infections
  - Bladder pain
  - Frequent urination
  - Incontinence

  **Skin health:**
  - Loss of skin elasticity
  - Skin sagging…Skin wrinkling
  - Age spots/melasma
  - Dry &/or itchy skin
  - Hair loss
  - Hair thinning
  - Change of hair texture
  - Skin rash
  - Atopic dermatitis
  - Eczema