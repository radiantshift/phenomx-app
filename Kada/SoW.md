
Date: Tue Dec 20 02:07:38 PM CET 2022

### Statement of Work for Phenomx Prototype App development proposal

Between PhenomX health (Phenomx), and KadaSolutions (Kada)

PhenomXhealth located at chemin du Bochet, 27; CH 1025 Saint Sulpice
is hiring KadaSolution located at 

__________________________________
to design and develop and nativeReact application running on both iOS, Android Sytems
for the estimate total cost of 12500.--CHF as outlined below:


### Background

PhX is a femtech, precision nutrition company empowering women to treat their health symptoms on their menopause journey. A phone application
provides users a frictionless way to track their diet and symptoms and receive immediate, personalized insights and recommendations. 

### Services to be rendered

- design, develop, test and deploy a mobile application (iOS and Android phone) for tracking symptoms and diet, providing insight recommendations.
- simple content management (git?) so PhX push/update content to be displayed on the app
- initial maintenance, early bug fixes, and support during one month following up the delivery

### Mutual cooperation
- Kada agrees to put their best efforts to create a professional collaboration. 
- PhenomX agree to collaborate in providing all necessary information for a smooth app development.

### The Work

 Core functionality :
   Capture tracking inputs from user.
   Compute score and display results.
   Recommendations provided.
   
Design theme according to PhenomX branding style.
 
Base application, backend setup.
Visual structure and design of various screen navigation and connecting business logic and external api. 
 
Scope:

  This is a prototype to assess market acceptance, and there it will have limited
  set of features 
  - symptom tracking
  - STT (speech to text) diet intake
  - insight with score, history and graphs 
  - recommendation results
  - learn more screen

  Development is based on the figma [mockup][1] and need to be responsive for mobile phones
  (tablet excluded)
  
  [1]: https://www.figma.com/file/BiYiMrq7mxrsnG3ThldnGo/PhenomX-Next-Phase?node-id

Timeline (temptative, relative to project start)

- W01 VPS,repository setup & docu exchange
- W02 basic visual structure, navigations, functional mockup, login
- W03 components and UI design
- W04 Symptom and diet tracking +insight related screens
- W05 diet learn screens
- W06 wrapup, deploy, test

Terms 
- First intermediate milestone (W03?) that equates to 5K workload to be evaluated and approved before continuing work-effort.
- Payment to be made at delivery of the app.

Plans Cancellation

Right to modify, reject, cancel or stop the process are ... to reimburse the
incured reasonable cost.

Liability and warranties.....

